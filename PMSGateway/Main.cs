﻿
using PMSGateway.common;
using PMSGateway.resources.utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using Wifi360.Data.Controllers;
using Wifi360.Data.Model;

namespace PMSGateway
{
    public partial class Main : Form
    {
        private Thread thread = null;
        
        TcpClient client = new TcpClient();
        DateTime lastRecieved = DateTime.MinValue;
        string ip = string.Empty;
        int port = 0;
        int interval = 0;
        int sequenceNumber = 10;
        NetworkStream stream = null;
        bool broken = false;
        int checkenumber = 1;
        List<Guess> guessList = new List<Guess>();
        TcpListener listener = null;

        public static string CleanInvalidXmlChars(string text)
        {
            // From xml spec valid chars:             
            // #x9 | #xA | #xD | [#x20-#xD7FF] | [#xE000-#xFFFD] | [#x10000-#x10FFFF]                 
            // any Unicode character, excluding the surrogate blocks, FFFE, and FFFF.             
            string re = @"[^x09x0Ax0Dx20-xD7FFxE000-xFFFDx10000-x10FFFF]";
            return Regex.Replace(text, re, "");
        }

        public Main()
        {
            try {
            
            InitializeComponent();

            Form.CheckForIllegalCrossThreadCalls = false;

            ip = textBoxIP.Text;
            port = Int32.Parse(textBoxPort.Text);
            interval = Int32.Parse(textBoxInterval.Text);

            timerTick.Interval = interval * 1000;
            timerTick.Tick += timerTick_Tick;
            timerTick.Start();

            string sencode = "Müller Dompè";
            sencode = Regex.Replace(sencode, @"[\p{L}-[a-zA-Z]]", m =>
                string.Join(string.Empty, m.Value.Select(c => string.Format("&#x{0:X};", Convert.ToInt32(c))).ToArray()));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            try
            {
                buttonStop.Enabled = true;
                timerTick.Start();

                string decode = WebUtility.HtmlDecode("M&#xFC;ller Domp&#xE8;");
                string encode = WebUtility.HtmlEncode("Müller Dompè");

                client = new TcpClient();
                client.Connect(ip, port);
                stream = client.GetStream();
                var content = new List<byte>();
                content.Add(2); // ASCII STX
                content.AddRange(Encoding.ASCII.GetBytes("<LinkDescription Date=\"" + DateTime.Now.ToString("yyMMdd") + "\" Time=\"" + DateTime.Now.ToString("HHmmss") + "\" VerNum=\"1.1\" />"));
                content.Add(3); // ASCII ETX
                byte[] buffer = content.ToArray();
                textBoxLogSend.Text += DateTime.Now.ToString() + " >> <LinkDescription Date=\"" + DateTime.Now.ToString("yyMMdd") + "\" Time=\"" + DateTime.Now.ToString("HHmmss") + "\" VerNum=\"1.1\" />";
                textBoxLogSend.Text += Environment.NewLine;
                stream.Write(buffer, 0, buffer.Length);

                if (stream.CanRead)
                {

                    byte[] myReadBuffer = new byte[1024];
                    StringBuilder myCompleteMessage = new StringBuilder();
                    int numberOfBytesRead = 0;

                    // Incoming message may be larger than the buffer size. 
                    do
                    {
                        numberOfBytesRead = stream.Read(myReadBuffer, 0, myReadBuffer.Length);

                        myCompleteMessage.AppendFormat("{0}", Encoding.ASCII.GetString(myReadBuffer, 0, numberOfBytesRead));

                    }
                    while (stream.DataAvailable);

                    string response = myCompleteMessage.ToString();
                    textBoxLogRecived.Text += DateTime.Now + " << " + response;
                    textBoxLogRecived.Text += Environment.NewLine;
                }

                lastRecieved = DateTime.Now;

                //IPAddress ipAddress = IPAddress.Parse("127.0.0.1");

                //listener = new TcpListener(ipAddress, 500);

                //listener.Start();

                //bool read = true;

                

                //while (read)
                //{
                //    // TcpClient client2 = listener.AcceptTcpClient();
                //    Socket client2 = listener.AcceptSocket();

                //    Thread childSocketThread = new Thread(() =>
                //    {
                        
                //        string serverResponse = string.Empty; ;
                //        Byte[] sendBytes = Encoding.UTF8.GetBytes(serverResponse);

                //        byte[] data = new byte[100];
                //        int size = client2.Receive(data);

                //        string recibe = System.Text.Encoding.UTF8.GetString(data, 0, data.Length);
                //        XDocument doc = XDocument.Parse(recibe.Trim('\0').Substring(1, recibe.Trim('\0').Length - 2), LoadOptions.None);

                //        string room = doc.Root.Attribute("room").Value;
                //        string surname = WebUtility.HtmlDecode(doc.Root.Attribute("name").Value);
                //        string amount = WebUtility.HtmlDecode(doc.Root.Attribute("amount").Value);

                //        guessList = new List<Guess>();
                //        stream = client.GetStream();

                //        string send = "<PostInquiry InquiryInformation=\"" + room + "\" MaximumReturnedMatches=\"16\" SequenceNumber=\"" + sequenceNumber + "\" RequestType=\"4\" PaymentMethod=\"16\" Date=\"" + DateTime.Now.ToString("yyMMdd") + "\" Time=\"" + DateTime.Now.ToString("HHmmss") + "\" RevenueCenter=\"1\" WorkstationId=\"POS1\" />";

                //        var contentOracle = new List<byte>();
                //        contentOracle.Add(2); // ASCII STX
                //        contentOracle.AddRange(Encoding.ASCII.GetBytes(send));
                //        contentOracle.Add(3); // ASCII ETX
                //        byte[] bufferOracle = contentOracle.ToArray();
                //        stream.Write(bufferOracle, 0, bufferOracle.Length);
                //        // Whatever else you want to do...

                //        if (stream.CanRead)
                //        {
                //            byte[] myReadBuffer = new byte[1024];
                //            StringBuilder myCompleteMessage = new StringBuilder();
                //            int numberOfBytesRead = 0;

                //            // Incoming message may be larger than the buffer size. 
                //            do
                //            {
                //                numberOfBytesRead = stream.Read(myReadBuffer, 0, myReadBuffer.Length);

                //                myCompleteMessage.AppendFormat("{0}", Encoding.ASCII.GetString(myReadBuffer, 0, numberOfBytesRead));

                //            }
                //            while (stream.DataAvailable);

                //            string response = myCompleteMessage.ToString();

                //            XDocument docResponse = XDocument.Parse(response.Substring(1, response.Length - 2), LoadOptions.None);

                //            sequenceNumber++;

                //            string responseValue = string.Empty; ;
                //            switch (docResponse.Root.Name.ToString().ToUpper())
                //            {
                //                case "POSTANSWER":// MessageBox.Show(docResponse.Root.Attribute("AnswerStatus").Value + " " + docResponse.Root.Attribute("ResponseText").Value);
                //                    responseValue = "<Response code=\"KO\" message=\"" + docResponse.Root.Attribute("AnswerStatus").Value + " " + docResponse.Root.Attribute("ResponseText").Value + "\" />";
                //                    sendBytes = Encoding.ASCII.GetBytes(responseValue);
                //                    break;
                //                case "POSTLIST":
                //                    //comboBoxClientes.Items.Clear();
                //                    //comboBoxClientes.Text = string.Empty;
                //                    var list = docResponse.Descendants("PostListItem");
                //                    int i = 0;
                //                    foreach (var item in list)
                //                    {
                //                        Guess g = new Guess();
                //                        g.Name = WebUtility.HtmlDecode(item.Attribute("LastName").Value);
                //                        g.Profile = item.Attribute("ProfileId").Value;
                //                        g.ReservationId = item.Attribute("ReservationId").Value;
                //                        g.CanPost = Int32.Parse(item.Attribute("NoPost").Value);
                //                        g.Credit = Double.Parse(item.Attribute("CreditLimit").Value);
                //                        g.MatchId = i + 1;

                //                        guessList.Add(g);
                //                        i++;
                //                    }

                //                    bool noGuess = true;
                //                    foreach (Guess g in guessList)
                //                    {
                //                        if (g.Name.ToUpper().Equals(surname.ToUpper()))
                //                        {
                //                            noGuess = false;
                //                            if (!g.CanPost.Equals(1) && (g.Credit > Double.Parse(amount))) //se puede hacer cargo
                //                            {
                //                                CheckNumberOracle checkNumberOracle = new CheckNumberOracle();
                //                                checkNumberOracle.Room = room;
                //                                checkNumberOracle.Name = g.Name;
                //                                checkNumberOracle.Amount = Double.Parse(amount);
                //                                checkNumberOracle.Site = 1;
                //                                checkNumberOracle.Date = DateTime.Now;
                //                                BillingController.SaveCheckNumberOracle(checkNumberOracle);

                //                                string send2 = "<PostRequest RoomNumber=\"" + room + "\" ReservationId=\"" + g.ReservationId + "\" ProfileId=\"" + g.Profile + "\" LastName=\"" + EncodeName(g.Name) + "\" HotelId=\"1\" RequestType=\"4\" InquiryInformation=\"" + room + "\" MatchfromPostList=\"" + g.MatchId + "\" SequenceNumber=\"" + sequenceNumber + "\" TotalAmount=\"" + amount.ToString().Trim('.') + "\" PaymentMethod=\"16\" CheckNumber=\"" + checkNumberOracle.IdCheck + "\" Date=\"" + DateTime.Now.ToString("yyMMdd") + "\" Time=\"" + DateTime.Now.ToString("HHmmss") + "\" WorkstationId=\"POS1\" />";

                //                                var contentOraclePost = new List<byte>();
                //                                contentOraclePost.Add(2); // ASCII STX
                //                                contentOraclePost.AddRange(Encoding.ASCII.GetBytes(send2));
                //                                contentOraclePost.Add(3); // ASCII ETX
                //                                byte[] bufferOraclePost = contentOraclePost.ToArray();
                //                                stream.Write(bufferOraclePost, 0, bufferOraclePost.Length);
                //                                // Whatever else you want to do...

                //                                if (stream.CanRead)
                //                                {
                //                                    byte[] myReadBufferPost = new byte[1024];
                //                                    StringBuilder myCompleteMessage2 = new StringBuilder();
                //                                    int numberOfBytesRead2 = 0;

                //                                    // Incoming message may be larger than the buffer size. 
                //                                    do
                //                                    {
                //                                        numberOfBytesRead2 = stream.Read(myReadBufferPost, 0, myReadBufferPost.Length);

                //                                        myCompleteMessage2.AppendFormat("{0}", Encoding.ASCII.GetString(myReadBufferPost, 0, numberOfBytesRead2));

                //                                    }
                //                                    while (stream.DataAvailable);

                //                                    string response2 = myCompleteMessage2.ToString();

                //                                    responseValue = "<Response code=\"OK\" message=\"" + response2 + "\" />";
                //                                    sendBytes = Encoding.ASCII.GetBytes(responseValue);
                //                                }
                //                                checkenumber++;
                //                            }
                //                            else if (g.CanPost.Equals(1))
                //                            {
                //                                responseValue = "<Response code=\"KO\" message=\"No se puede hacer el post, no lo tiene permitido\" />";
                //                                sendBytes = Encoding.ASCII.GetBytes(responseValue);
                //                            }
                //                            else
                //                            {
                //                                responseValue = "<Response code=\"KO\" message=\"No se puede hacer el post, el importe supera el máximo permitido\" />";
                //                                sendBytes = Encoding.ASCII.GetBytes(responseValue);
                //                            }
                //                        }
                //                    }
                //                    if (noGuess)
                //                    {
                //                        responseValue = "<Response code=\"KO\" message=\"El nombre no coincide con ninguno de la habitación\" />";
                //                        sendBytes = Encoding.ASCII.GetBytes(responseValue);
                //                    }
                //                    break;
                //            }

                //        }

                //        lastRecieved = DateTime.Now;
                //        sequenceNumber++;

                //        client2.Send(sendBytes);

                //        client2.Close();
                //        //read = false;

                //    });

                //    childSocketThread.Start();
                //}

                //listener.Stop();

                Thread listen = new Thread(DoWork);
                listen.Start();
                ;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
        }

        private void DoWork()
        {
            IPAddress ipAddress = IPAddress.Parse("127.0.0.1");

            listener = new TcpListener(ipAddress, 500);

            listener.Start();

            bool read = true;



            while (read)
            {
                // TcpClient client2 = listener.AcceptTcpClient();
                Socket client2 = listener.AcceptSocket();

                //Thread childSocketThread = new Thread(() =>
                //{

                    string serverResponse = string.Empty; ;
                    Byte[] sendBytes = Encoding.UTF8.GetBytes(serverResponse);

                    byte[] data = new byte[100];
                    int size = client2.Receive(data);

                    string recibe = System.Text.Encoding.UTF8.GetString(data, 0, data.Length);
                    XDocument doc = XDocument.Parse(recibe.Trim('\0').Substring(1, recibe.Trim('\0').Length - 2), LoadOptions.None);

                    string room = doc.Root.Attribute("room").Value;
                    string surname = WebUtility.HtmlDecode(doc.Root.Attribute("name").Value);
                    string amount = WebUtility.HtmlDecode(doc.Root.Attribute("amount").Value);

                    guessList = new List<Guess>();
                    stream = client.GetStream();

                    string send = "<PostInquiry InquiryInformation=\"" + room + "\" MaximumReturnedMatches=\"16\" SequenceNumber=\"" + sequenceNumber + "\" RequestType=\"4\" PaymentMethod=\"16\" Date=\"" + DateTime.Now.ToString("yyMMdd") + "\" Time=\"" + DateTime.Now.ToString("HHmmss") + "\" RevenueCenter=\"1\" WorkstationId=\"POS1\" />";
                    textBoxLogSend.Text += DateTime.Now.ToString() + " >> " + send + Environment.NewLine; ;
                    var contentOracle = new List<byte>();
                    contentOracle.Add(2); // ASCII STX
                    contentOracle.AddRange(Encoding.ASCII.GetBytes(send));
                    contentOracle.Add(3); // ASCII ETX
                    byte[] bufferOracle = contentOracle.ToArray();
                    stream.Write(bufferOracle, 0, bufferOracle.Length);
                    // Whatever else you want to do...

                    if (stream.CanRead)
                    {
                        byte[] myReadBuffer = new byte[1024];
                        StringBuilder myCompleteMessage = new StringBuilder();
                        int numberOfBytesRead = 0;

                        // Incoming message may be larger than the buffer size. 
                        do
                        {
                            numberOfBytesRead = stream.Read(myReadBuffer, 0, myReadBuffer.Length);

                            myCompleteMessage.AppendFormat("{0}", Encoding.ASCII.GetString(myReadBuffer, 0, numberOfBytesRead));

                        }
                        while (stream.DataAvailable);

                        string response = myCompleteMessage.ToString();
                        textBoxLogRecived.Text += DateTime.Now.ToString() + " >> " + response + Environment.NewLine;

                        XDocument docResponse = XDocument.Parse(response.Substring(1, response.Length - 2), LoadOptions.None);

                        sequenceNumber++;

                        string responseValue = string.Empty; ;
                        switch (docResponse.Root.Name.ToString().ToUpper())
                        {
                            case "POSTANSWER":// MessageBox.Show(docResponse.Root.Attribute("AnswerStatus").Value + " " + docResponse.Root.Attribute("ResponseText").Value);
                                responseValue = "<Response code=\"KO\" message=\"" + docResponse.Root.Attribute("AnswerStatus").Value + " " + docResponse.Root.Attribute("ResponseText").Value + "\" />";
                                sendBytes = Encoding.ASCII.GetBytes(responseValue);
                                break;
                            case "POSTLIST":
                                //comboBoxClientes.Items.Clear();
                                //comboBoxClientes.Text = string.Empty;
                                var list = docResponse.Descendants("PostListItem");
                                int i = 0;
                                foreach (var item in list)
                                {
                                    Guess g = new Guess();
                                    g.Name = WebUtility.HtmlDecode(item.Attribute("LastName").Value);
                                    g.Profile = item.Attribute("ProfileId").Value;
                                    g.ReservationId = item.Attribute("ReservationId").Value;
                                    g.CanPost = Int32.Parse(item.Attribute("NoPost").Value);
                                    g.Credit = Double.Parse(item.Attribute("CreditLimit").Value);
                                    g.MatchId = i + 1;

                                    guessList.Add(g);
                                    i++;
                                }

                                bool noGuess = true;
                                foreach (Guess g in guessList)
                                {
                                    if (g.Name.ToUpper().Equals(surname.ToUpper()))
                                    {
                                        noGuess = false;
                                        if (!g.CanPost.Equals(1) && (g.Credit > Double.Parse(amount))) //se puede hacer cargo
                                        {
                                            CheckNumberOracle checkNumberOracle = new CheckNumberOracle();
                                            checkNumberOracle.Room = room;
                                            checkNumberOracle.Name = g.Name;
                                            checkNumberOracle.Amount = Double.Parse(amount);
                                            checkNumberOracle.Site = 1;
                                            checkNumberOracle.Date = DateTime.Now;
                                            BillingController.SaveCheckNumberOracle(checkNumberOracle);

                                            string send2 = "<PostRequest RoomNumber=\"" + room + "\" ReservationId=\"" + g.ReservationId + "\" ProfileId=\"" + g.Profile + "\" LastName=\"" + EncodeName(g.Name) + "\" HotelId=\"1\" RequestType=\"4\" InquiryInformation=\"" + room + "\" MatchfromPostList=\"" + g.MatchId + "\" SequenceNumber=\"" + sequenceNumber + "\" TotalAmount=\"" + amount.ToString().Trim('.') + "\" PaymentMethod=\"16\" CheckNumber=\"" + checkNumberOracle.IdCheck + "\" Date=\"" + DateTime.Now.ToString("yyMMdd") + "\" Time=\"" + DateTime.Now.ToString("HHmmss") + "\" WorkstationId=\"POS1\" />";
                                            textBoxLogSend.Text += DateTime.Now.ToString() + " >> " + send2 + Environment.NewLine;
                                            var contentOraclePost = new List<byte>();
                                            contentOraclePost.Add(2); // ASCII STX
                                            contentOraclePost.AddRange(Encoding.ASCII.GetBytes(send2));
                                            contentOraclePost.Add(3); // ASCII ETX
                                            byte[] bufferOraclePost = contentOraclePost.ToArray();
                                            stream.Write(bufferOraclePost, 0, bufferOraclePost.Length);
                                            // Whatever else you want to do...

                                            if (stream.CanRead)
                                            {
                                                byte[] myReadBufferPost = new byte[1024];
                                                StringBuilder myCompleteMessage2 = new StringBuilder();
                                                int numberOfBytesRead2 = 0;

                                                // Incoming message may be larger than the buffer size. 
                                                do
                                                {
                                                    numberOfBytesRead2 = stream.Read(myReadBufferPost, 0, myReadBufferPost.Length);

                                                    myCompleteMessage2.AppendFormat("{0}", Encoding.ASCII.GetString(myReadBufferPost, 0, numberOfBytesRead2));

                                                }
                                                while (stream.DataAvailable);

                                                string response2 = myCompleteMessage2.ToString();
                                                textBoxLogRecived.Text += DateTime.Now.ToString() + " >> " + response2 + Environment.NewLine;

                                                responseValue = "<Response code=\"OK\" message=\"" + response2 + "\" />";
                                                sendBytes = Encoding.ASCII.GetBytes(responseValue);
                                            }
                                            checkenumber++;
                                        }
                                        else if (g.CanPost.Equals(1))
                                        {
                                            responseValue = "<Response code=\"KO\" message=\"No se puede hacer el post, no lo tiene permitido\" />";
                                            sendBytes = Encoding.ASCII.GetBytes(responseValue);
                                        }
                                        else
                                        {
                                            responseValue = "<Response code=\"KO\" message=\"No se puede hacer el post, el importe supera el máximo permitido\" />";
                                            sendBytes = Encoding.ASCII.GetBytes(responseValue);
                                        }
                                    }
                                }
                                if (noGuess)
                                {
                                    responseValue = "<Response code=\"KO\" message=\"El nombre no coincide con ninguno de la habitación\" />";
                                    sendBytes = Encoding.ASCII.GetBytes(responseValue);
                                }
                                break;
                        }

                    }

                    lastRecieved = DateTime.Now;
                    sequenceNumber++;

                    client2.Send(sendBytes);

                    client2.Close();
                    //read = false;

                //});

                //childSocketThread.Start();
            }

            listener.Stop();
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            client = new TcpClient();
            
            client.Close();
            timerTick.Stop();

            stream.Close();

            buttonStart.Enabled = true;
            buttonStop.Enabled = false;
        }

        private void timerTick_Tick(object sender, EventArgs e)
        {
            try {
            TimeSpan dif = (DateTime.Now - lastRecieved);
             if (dif.TotalSeconds + 1 > (timerTick.Interval/1000))
                {
                // Or some other encoding, of course...

                var content = new List<byte>();
                content.Add(2); // ASCII STX
                content.AddRange(Encoding.ASCII.GetBytes("<LinkStart Date=\"" + DateTime.Now.ToString("yyMMdd") + "\" Time=\"" + DateTime.Now.ToString("HHmmss") + "\" />"));
                content.Add(3); // ASCII ETX
                byte[] buffer = content.ToArray();
                textBoxLogSend.Text += DateTime.Now.ToString() + " >> <LinkStart Date=\"" + DateTime.Now.ToString("yyMMdd") + "\" Time=\"" + DateTime.Now.ToString("HHmmss") + "\" />";
                textBoxLogSend.Text += Environment.NewLine;
                stream.Write(buffer, 0, buffer.Length);
                // Whatever else you want to do...

                if (stream.CanRead)
                {
                    byte[] myReadBuffer = new byte[1024];
                    StringBuilder myCompleteMessage = new StringBuilder();
                    int numberOfBytesRead = 0;

                    // Incoming message may be larger than the buffer size. 
                    do
                    {
                        numberOfBytesRead = stream.Read(myReadBuffer, 0, myReadBuffer.Length);

                        myCompleteMessage.AppendFormat("{0}", Encoding.ASCII.GetString(myReadBuffer, 0, numberOfBytesRead));

                    }
                    while (stream.DataAvailable);

                    string response = myCompleteMessage.ToString();
                    textBoxLogRecived.Text += DateTime.Now + " << " + response;
                    textBoxLogRecived.Text += Environment.NewLine;
                }

                lastRecieved = DateTime.Now;
            }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                broken = true;
            }
            finally 
            {
                try
                {
                    TimeSpan dif = (DateTime.Now - lastRecieved);

                    if ((broken) && (dif.TotalSeconds > (timerTick.Interval / 1000)))
                    {
                        client = new TcpClient();
                        client.Connect(ip, port);
                        stream = client.GetStream();
                        var content = new List<byte>();
                        content.Add(2); // ASCII STX
                        content.AddRange(Encoding.ASCII.GetBytes("<LinkDescription Date=\"" + DateTime.Now.ToString("yyMMdd") + "\" Time=\"" + DateTime.Now.ToString("HHmmss") + "\" VerNum=\"1.1\" />"));
                        content.Add(3); // ASCII ETX
                        byte[] buffer = content.ToArray();
                        textBoxLogSend.Text += DateTime.Now.ToString() + " >> <LinkDescription Date=\"" + DateTime.Now.ToString("yyMMdd") + "\" Time=\"" + DateTime.Now.ToString("HHmmss") + "\" VerNum=\"1.1\" />";
                        textBoxLogSend.Text += Environment.NewLine;
                        stream.Write(buffer, 0, buffer.Length);

                        if (stream.CanRead)
                        {

                            byte[] myReadBuffer = new byte[1024];
                            StringBuilder myCompleteMessage = new StringBuilder();
                            int numberOfBytesRead = 0;

                            // Incoming message may be larger than the buffer size. 
                            do
                            {
                                numberOfBytesRead = stream.Read(myReadBuffer, 0, myReadBuffer.Length);

                                myCompleteMessage.AppendFormat("{0}", Encoding.ASCII.GetString(myReadBuffer, 0, numberOfBytesRead));

                            }
                            while (stream.DataAvailable);

                            string response = myCompleteMessage.ToString();
                            textBoxLogRecived.Text += DateTime.Now + " << " + response;
                            textBoxLogRecived.Text += Environment.NewLine;
                        }

                        lastRecieved = DateTime.Now;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void logToFile(string line)
        {
            try
            {
                string pathDirectory = string.Format(@"{0}\Logs",Application.StartupPath);

                if (!Directory.Exists(pathDirectory))
                    Directory.CreateDirectory(pathDirectory);

                string path = string.Format(@"{0}\Logs\{1}.txt",Application.StartupPath, DateTime.Today.ToString("yyMMdd"));
                if (!File.Exists(path))
                {
                    File.Create(path);
                }

                StreamWriter stream = new StreamWriter(path, true);
                stream.Write(line,stream);
                stream.Write(Environment.NewLine);

                stream.Close();
                

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void logToBox(string line)
        {
            textBoxLogSend.Text += line;
            textBoxLogSend.Text += Environment.NewLine;
        }

        private void buttonApply_Click(object sender, EventArgs e)
        {
            timerTick.Interval = Int32.Parse(textBoxInterval.Text) * 1000;
            ip = textBoxIP.Text;
            port = Int32.Parse(textBoxPort.Text);
        }

        private void buttonSend_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                
                string send = string.Empty;
                bool CanSend = true;
                switch (comboBoxAccion.SelectedItem.ToString().ToUpper())
                {
                    case "POSTINQUIRY":
                        send = "<PostInquiry InquiryInformation=\"" + textBoxRoom.Text + "\" MaximumReturnedMatches=\"16\" SequenceNumber=\"" + sequenceNumber + "\" RequestType=\"4\" PaymentMethod=\"16\" Date=\"" + DateTime.Now.ToString("yyMMdd") + "\" Time=\"" + DateTime.Now.ToString("HHmmss") + "\" RevenueCenter=\"1\" WorkstationId=\"POS1\" />";
                        labelCreditLimit.Text = string.Empty;
                        labelCanPost.Text = string.Empty;
                        labelRerservationID.Text = string.Empty;
                        labelProfile.Text = string.Empty;
                        break;
                    case "POSTREQUEST": 
                        string sencode = comboBoxClientes.SelectedItem.ToString();
                        sencode = Regex.Replace(sencode, @"[\p{L}-[a-zA-Z]]", m =>
                        string.Join(string.Empty, m.Value.Select(c => string.Format("&#x{0:X};", Convert.ToInt32(c))).ToArray()));

                        int matchPostList = comboBoxClientes.SelectedIndex + 1;
                        string reservationID = string.Empty;
                        string ProfileID = string.Empty;

                        string[] values3 = labelRerservationID.Text.Split(';');
                        foreach (string value in values3)
                        {
                            if (value.Contains(comboBoxClientes.SelectedItem.ToString()))
                            {
                                string[] post = value.Split('|');
                                reservationID = post[1];
                            }
                        }

                        string[] values4 = labelProfile.Text.Split(';');
                        foreach (string value in values4)
                        {
                            if (value.Contains(comboBoxClientes.SelectedItem.ToString()))
                            {
                                string[] post = value.Split('|');
                                ProfileID = post[1];
                            }
                        }

                        double amount = double.Parse(textBoxAmount.Text);
                        amount = amount * 100;
                        send = "<PostRequest RoomNumber=\"" + textBoxRoom.Text + "\" ReservationId=\"" + reservationID + "\" ProfileId=\"" + ProfileID + "\" LastName=\"" + sencode + "\" HotelId=\"1\" RequestType=\"4\" InquiryInformation=\"" + textBoxRoom.Text + "\" MatchfromPostList=\"" + matchPostList + "\" SequenceNumber=\"" + sequenceNumber + "\" TotalAmount=\"" + amount.ToString().Trim('.') + "\" PaymentMethod=\"16\" CheckNumber=\"12345678\" Date=\"" + DateTime.Now.ToString("yyMMdd") + "\" Time=\"" + DateTime.Now.ToString("HHmmss") + "\" WorkstationId=\"POS1\" />";
                        amount = amount / 100;

                        string[] values = labelCanPost.Text.Split(';');
                        foreach (string value in values)
                        {
                            if (value.Contains(comboBoxClientes.SelectedItem.ToString()))
                            {
                                string[] post = value.Split('|');
                                if (post[1].Equals("1"))
                                    CanSend = false;
                            }
                        }

                        string[] values2 = labelCreditLimit.Text.Split(';');
                        foreach (string value in values2)
                        {
                            if (value.Contains(comboBoxClientes.SelectedItem.ToString()))
                            {
                                string[] post = value.Split('|');
                                if (amount >= Double.Parse(post[1].Replace('.',',')))
                                    CanSend = false;
                            }
                        }

                        break;
                }

                if (CanSend)
                {
                    stream = client.GetStream();

                    var content = new List<byte>();
                    content.Add(2); // ASCII STX
                    content.AddRange(Encoding.ASCII.GetBytes(send));
                    content.Add(3); // ASCII ETX
                    byte[] buffer = content.ToArray();
                    textBoxLogSend.Text += DateTime.Now.ToString() + " >> " + send;
                    textBoxLogSend.Text += Environment.NewLine;
                    stream.Write(buffer, 0, buffer.Length);
                    // Whatever else you want to do...

                    if (stream.CanRead)
                    {
                        byte[] myReadBuffer = new byte[1024];
                        StringBuilder myCompleteMessage = new StringBuilder();
                        int numberOfBytesRead = 0;

                        // Incoming message may be larger than the buffer size. 
                        do
                        {
                            numberOfBytesRead = stream.Read(myReadBuffer, 0, myReadBuffer.Length);

                            myCompleteMessage.AppendFormat("{0}", Encoding.ASCII.GetString(myReadBuffer, 0, numberOfBytesRead));

                        }
                        while (stream.DataAvailable);

                        string response = myCompleteMessage.ToString();
                        textBoxLogRecived.Text += DateTime.Now + " << " + response;
                        textBoxLogRecived.Text += Environment.NewLine;

                        XDocument doc = XDocument.Parse(response.Substring(1, response.Length - 2), LoadOptions.None);

                        switch (doc.Root.Name.ToString().ToUpper())
                        {
                            case "POSTANSWER": MessageBox.Show(doc.Root.Attribute("AnswerStatus").Value + " " + doc.Root.Attribute("ResponseText").Value);
                                break;
                            case "POSTLIST":
                                comboBoxClientes.Items.Clear();
                                comboBoxClientes.Text = string.Empty;
                                var list = doc.Descendants("PostListItem");
                                foreach (var item in list)
                                {
                                    comboBoxClientes.Items.Add(WebUtility.HtmlDecode(item.Attribute("LastName").Value));
                                    labelCanPost.Text += string.Format("{0}|{1};", WebUtility.HtmlDecode(item.Attribute("LastName").Value), item.Attribute("NoPost").Value);
                                    labelCreditLimit.Text += string.Format("{0}|{1};", WebUtility.HtmlDecode(item.Attribute("LastName").Value),item.Attribute("CreditLimit").Value);
                                    labelRerservationID.Text += string.Format("{0}|{1};", WebUtility.HtmlDecode(item.Attribute("LastName").Value), item.Attribute("ReservationId").Value);
                                    labelProfile.Text += string.Format("{0}|{1};", WebUtility.HtmlDecode(item.Attribute("LastName").Value), item.Attribute("ProfileId").Value);

                                }
                                break;
                        }
                    }

                    lastRecieved = DateTime.Now;
                    sequenceNumber++;
                }
                else
                    MessageBox.Show("No se puede hacer el cargo");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            this.Cursor = Cursors.Default;
        }

        private string EncodeName(string name)
        {
            name = Regex.Replace(name, @"[\p{L}-[a-zA-Z]]", m =>
                        string.Join(string.Empty, m.Value.Select(c => string.Format("&#x{0:X};", Convert.ToInt32(c))).ToArray()));

            return name;
        }

    }
}
