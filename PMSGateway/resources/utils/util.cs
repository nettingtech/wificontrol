﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PMSGateway.resources.utils
{
    public class util
    {
        public static string CreateRandomPassword(int PasswordLength)
        {
            string _allowedChars = "23456789";
            Random randNum = new Random();
            char[] chars = new char[PasswordLength];
            int allowedCharCount = _allowedChars.Length;

            for (int i = 0; i < PasswordLength; i++)
            {
                chars[i] = _allowedChars[(int)((_allowedChars.Length) * randNum.NextDouble())];
            }

            return new string(chars);
        }

        public static string createRandomUserNumber(int Length)
        {
            string _allowedChars = "ABCDEFGHJKLMNPQRSTUVWXYZ23456789";
            Random randNum = new Random();
            char[] chars = new char[Length];
            int allowedCharCount = _allowedChars.Length;

            for (int i = 0; i < Length; i++)
            {
                chars[i] = _allowedChars[(int)((_allowedChars.Length) * randNum.NextDouble())];
            }

            return new string(chars);
        }

        public static void sendMailFrontDesk(string mail, DateTime billDate, int idBill, string room, double amount)
        {
            string body = "<table>";
            body += string.Format("<tr>Date<td></td><td>{0}<td></tr>", billDate.ToString("yy/MM/dd hh:mm:ss"));
            body += string.Format("<tr>Transaction ID<td></td><td>{0}<td></tr>", idBill);
            body += string.Format("<tr>Room<td></td><td>{0}<td></tr>", room);
            body += string.Format("<tr>Amount<td></td><td>{0} <td></tr>", amount);
            body += "</table>";
            
            Send(ConfigurationManager.AppSettings["SoporteMail"].ToString(), "NEW INTERNET MANUAL BILL", body);
        }

        public static void sendMail(string subject, string body)
        {
            Send(ConfigurationManager.AppSettings["SoporteMail"].ToString(), subject, body);
        }

        

        private static void Send(string to, string subject, string body)
        {
            string _smtpClient = string.Empty;
            int _smtpPort = 0;
            string _userName = string.Empty;
            string _password = string.Empty;
            string _from = string.Empty;
            string _to = string.Empty;
            bool _isBodyHtml = true;
            bool _sendAsync = true;
            bool _ssl = true;

            // Get values from configuration
            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SmtpClient"]))
                throw new Exception("SmtpClient");
            else
                _smtpClient = ConfigurationManager.AppSettings["SmtpClient"];


            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SmtpPort"]))
                throw new Exception("SmtpPort");
            else
            {
                if (!Int32.TryParse(ConfigurationManager.AppSettings["SmtpPort"], out _smtpPort))
                    throw new Exception(string.Format("smtpPort {0} not valid.", ConfigurationManager.AppSettings["SmtpPort"]));
            }

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["UserName"]))
                _userName = ConfigurationManager.AppSettings["UserName"];

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["Password"]))
                _password = ConfigurationManager.AppSettings["Password"];

            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SmtpFrom"]))
                throw new Exception("SmtpFrom");
            else
                _from = ConfigurationManager.AppSettings["SmtpFrom"];

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["SendAsync"]))
            {
                if (!Boolean.TryParse(ConfigurationManager.AppSettings["SendAsync"], out _sendAsync))
                    throw new Exception(string.Format("SendAsync {0} not valid.", ConfigurationManager.AppSettings["SendAsync"]));
            }

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["SmtpSSL"]))
            {
                if (!Boolean.TryParse(ConfigurationManager.AppSettings["SmtpSSL"], out _ssl))
                    throw new Exception(string.Format("SmtpSSL {0} not valid.", ConfigurationManager.AppSettings["SmtpSSL"]));
            }

            try
            {
                SmtpClient smtpClient = new SmtpClient(_smtpClient, _smtpPort);
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.EnableSsl = _ssl;

                if ((_userName != string.Empty) && (_password != string.Empty))
                {
                    NetworkCredential networkCredential = new NetworkCredential(_userName, _password);
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = networkCredential;
                }

                MailMessage mailMessage = new MailMessage(_from, to, subject, body);
                mailMessage.IsBodyHtml = true;

                if (_sendAsync)
                    smtpClient.SendAsync(mailMessage, null);
                else
                    smtpClient.Send(mailMessage);
            }
            catch
            {
                throw;
            }
        }
    }
}
