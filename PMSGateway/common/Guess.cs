﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMSGateway.common
{
    public class Guess
    {
        #region variables

        private string _name = string.Empty;
        private double _credit = 0;
        private int _canPost = 0;
        private string _profile = string.Empty;
        private string _reservationId = string.Empty;
        private int _matchId = 0;

        #endregion 

        #region Propierties

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public double Credit
        {
            get { return _credit; }
            set { _credit = value; }
        }

        public int CanPost
        {
            get { return _canPost; }
            set { _canPost = value; }
        }

        public string Profile
        {
            get { return _profile; }
            set { _profile = value; }
        }

        public string ReservationId
        {
            get { return _reservationId; }
            set { _reservationId = value; }
        }

        public int MatchId
        {
            get { return _matchId; }
            set { _matchId = value; }
        }


        #endregion
    }
}
