﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.IO;

namespace HttpResponseText
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            try
            {

                IPAddress ipAddress = IPAddress.Parse("127.0.0.1");

                textBox1.Text += "Starting TCP listener...";
                textBox1.Text += Environment.NewLine;

                TcpListener listener = new TcpListener(ipAddress, 500);

                listener.Start();

                bool read = true;

                while (read)
                {
                   // TcpClient client2 = listener.AcceptTcpClient();
                    Socket client = listener.AcceptSocket();
                    Console.WriteLine("Connection accepted.");

                    var childSocketThread = new Thread(() =>
                    {
                        byte[] data = new byte[100];
                        int size = client.Receive(data);
                        
                        textBox1.Text += "Recieved data: ";
                        for (int i = 0; i < size; i++)
                            textBox1.Text +=Convert.ToChar(data[i]);


                        textBox1.Text += Environment.NewLine;

                        string serverResponse = "Respuesta de prueba";
                        Byte[] sendBytes = Encoding.ASCII.GetBytes(serverResponse);

                        client.Send(sendBytes);
                        
                        client.Close();
                     //   read = false;


                    });
                    childSocketThread.Start();
                }

                listener.Stop();
            }
            catch (Exception e)
            {
                textBox1.Text += "Error: " + e.StackTrace;
                textBox1.Text += Environment.NewLine;
            }


        }
    }
}
