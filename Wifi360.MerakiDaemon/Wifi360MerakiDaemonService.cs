﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using RestSharp;
using RestSharp.Serializers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Xml;
using Wifi360.Data.Controllers;
using Wifi360.Data.Model;
using Wifi360.MerakiDaemon.Common;

namespace Wifi360.MerakiDaemon
{
    partial class Wifi360MerakiDaemonService : ServiceBase
    {
        #region Variables Privadas y Propiedades

        private bool firstRun;          // 'Flag' para controlar si es el primer 'tick' o no

        private System.Timers.Timer timerDatosMaestros = null;
        private System.Timers.Timer timerSplashLogins = null;
        private System.Timers.Timer timerAccounting = null;
        private System.Timers.Timer timerIdleTimeOut = null;

        private bool isExecutingDatosMaestros;
        private bool isExecutingThreadSplashLogin;
        private bool isExecutingThreadAccounting;
        private bool isExecutingThreadIdleTimeOut;
        
        //HACKATHON
        private System.Timers.Timer timerCamara = null;

        private bool isExecutingCamara;

        #endregion Variables Privadas y Propiedades

        #region metodos servicio y timer

        public Wifi360MerakiDaemonService()
        {
            InitializeComponent();

             Thread.Sleep(15000);

            firstRun = true;
            timerDatosMaestros = new System.Timers.Timer();
            timerDatosMaestros.Interval = Int32.Parse(ConfigurationManager.AppSettings["TimerInterval"].ToString()) * 1000;
            timerDatosMaestros.Elapsed += new ElapsedEventHandler(TimerElapsedDatosMaestros);

            timerSplashLogins = new System.Timers.Timer();
            timerSplashLogins.Interval = Int32.Parse(ConfigurationManager.AppSettings["TimerIntervalSplashLoginAttemps"].ToString()) * 1000;
            timerSplashLogins.Elapsed += new ElapsedEventHandler(TimerElapsedSplashLogins);

            timerAccounting = new System.Timers.Timer();
            timerAccounting.Interval = Int32.Parse(ConfigurationManager.AppSettings["TimerIntervalAccounting"].ToString()) * 1000;
            timerAccounting.Elapsed += new ElapsedEventHandler(TimerElapsedAccounting);

            timerIdleTimeOut = new System.Timers.Timer();
            timerIdleTimeOut.Interval = Int32.Parse(ConfigurationManager.AppSettings["TimerIntervalIdleTimeOut"].ToString()) * 1000;
            timerIdleTimeOut.Elapsed += new ElapsedEventHandler(TimerElapsedIdleTimeOut);

            timerCamara = new System.Timers.Timer();
            timerCamara.Interval = 120000;
            timerCamara.Elapsed += new ElapsedEventHandler(TimerCamara);        

            //HACKATHON


            //

            Context.Log = //new Log(Context.AppName, ELogDest.File);
                new Log(Context.AppName, AppDomain.CurrentDomain.BaseDirectory, ELogType.Debug);
        }

        protected override void OnStart(string[] args)
        {
            // TODO: agregar código aquí para iniciar el servicio.
            if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "Se inicia el servicio");
            if (firstRun)
            {
                firstRun = false;
                TimerElapsedDatosMaestros(null, null);
                //TimerElapsedSplashLogins(null, null);
                //TimerElapsedAccounting(null, null);
                //TimerElapsedIdleTimeOut(null, null);
                //TimerCamara(null, null);
            }

            timerDatosMaestros.Start();
            //timerSplashLogins.Start();
            //timerAccounting.Start();
            //timerIdleTimeOut.Start();

            //timerCamara.Start();

        }

        protected override void OnStop()
        {
            // TODO: agregar código aquí para realizar cualquier anulación necesaria para detener el servicio.
            if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "Se para el servicio");
            timerDatosMaestros.Stop();
            timerSplashLogins.Stop();
            timerAccounting.Stop();

        }

        private void TimerElapsedDatosMaestros(object sender, ElapsedEventArgs e)
        {
            // Para poder editar el app.config en caliente
            List<ParametrosConfiguracion> parametros = BillingController.GetParametrosConfiguracion(0);
            ParametrosConfiguracion key = (from t in parametros where t.Key.ToUpper().Equals("ENCRYPTIONKEY") select t).FirstOrDefault();
            try
            {
                ConfigurationManager.RefreshSection("connectionStrings");
                ConfigurationManager.RefreshSection("appSettings");

                Thread.Sleep(10000);

                if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "**************** Empieza el proceso Maestros **************************");
                if (!isExecutingDatosMaestros)
                {
                    isExecutingDatosMaestros = true;

                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                    ServicePointManager.ServerCertificateValidationCallback += (sender1, certificate, chain, sslPolicyErrors) =>
                    {
                        return true;
                    };

                    System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                    var client = new RestClient(Context.ApiUrl + "/organizations");
                    var request = new RestRequest(Method.GET);
                    request.AddHeader("Postman-Token", "7afdfed5-4243-4cda-8b95-ac9f2dc1b2f3");
                    request.AddHeader("cache-control", "no-cache");
                    request.AddHeader("X-Cisco-Meraki-API-Key", Context.ApiKey);
                    IRestResponse response = client.Execute(request);

                    DateTime update = DateTime.Now.ToUniversalTime();
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var organizations = JsonConvert.DeserializeObject<List<Organization>>(response.Content);
                        foreach (Organization obj in organizations)
                        {
                            ApiMerakiOrganizations amOrganization = BillingController.SelectApiMerakiOrganization(obj.id);
                            Misc.Map(obj, amOrganization);
                            amOrganization.LastUpdate = update;
                            BillingController.SaveApiMerakiOrganization(amOrganization);
                            

                            var client2 = new RestClient(Context.ApiUrl + "/organizations/" + obj.id + "/networks");
                            var request2 = new RestRequest(Method.GET);
                            request2.AddHeader("Postman-Token", "7afdfed5-4243-4cda-8b95-ac9f2dc1b2f3");
                            request2.AddHeader("cache-control", "no-cache");
                            request2.AddHeader("X-Cisco-Meraki-API-Key", Context.ApiKey);
                            IRestResponse response2 = client2.Execute(request2);

                            if (response2.StatusCode == HttpStatusCode.OK)
                            {
                                var networks = JsonConvert.DeserializeObject<List<Network>>(response2.Content);

                                foreach (Network net in networks)
                                {
                                    try
                                    {
                                        #region Para cada red

                                        ApiMerakiNetworks amNetwork = BillingController.SelectApiMerakiNetwork(net.id);
                                        Misc.Map(net, amNetwork);
                                        amNetwork.LastUpdate = update;
                                        amNetwork.IdOrganization = amOrganization.IdOrganization;
                                        BillingController.SaveApiMerakiNetwork(amNetwork);


                                        //Obtenemos los SSIDS por red
                                        var clientSSID = new RestClient(Context.ApiUrl + "/networks/" + net.id + "/ssids");
                                        var requestSSID = new RestRequest(Method.GET);
                                        requestSSID.AddHeader("Postman-Token", "7afdfed5-4243-4cda-8b95-ac9f2dc1b2f3");
                                        requestSSID.AddHeader("cache-control", "no-cache");
                                        requestSSID.AddHeader("X-Cisco-Meraki-API-Key", Context.ApiKey);
                                        IRestResponse responseSSID = clientSSID.Execute(requestSSID);

                                        List<ssid> ssids = null;
                                        if (responseSSID.StatusCode == HttpStatusCode.OK)
                                        {
                                            ssids = JsonConvert.DeserializeObject<List<ssid>>(responseSSID.Content);

                                        }


                                        //guardamos los dispositivos de la red
                                        var clientDevices = new RestClient(Context.ApiUrl + "/networks/" + net.id + "/devices");
                                        var requestDevices = new RestRequest(Method.GET);
                                        requestDevices.AddHeader("Postman-Token", "7afdfed5-4243-4cda-8b95-ac9f2dc1b2f3");
                                        requestDevices.AddHeader("cache-control", "no-cache");
                                        requestDevices.AddHeader("X-Cisco-Meraki-API-Key", Context.ApiKey);
                                        IRestResponse responseDevices = clientDevices.Execute(requestDevices);

                                        List<Device> devices = null;
                                        if (responseDevices.StatusCode == HttpStatusCode.OK)
                                        {
                                            devices = JsonConvert.DeserializeObject<List<Device>>(responseDevices.Content);

                                            foreach (Device device in devices)
                                            {
                                                ApiMerakiDevices amDevice = BillingController.SelectApiMerakiDevice(device.mac);
                                                Misc.Map(device, amDevice);
                                                if (amDevice.lanIp == null)
                                                    amDevice.lanIp = "0.0.0.0";
                                                amDevice.LastUpdate = update;
                                                BillingController.SaveApiMerakiDevice(amDevice);

                                            }
                                        }

                                        if ((ssids != null) && (devices != null))
                                        {
                                            foreach (ssid _ssid in ssids)
                                            {
                                                if (_ssid.enabled)
                                                {
                                                    foreach (Device dev in devices) {
                                                        var clientSplash = new RestClient(Context.ApiUrl + "/networks/" + net.id + "/ssids/" + _ssid.number + "/splashSettings");
                                                        var requestSplash = new RestRequest(Method.GET);
                                                        requestSplash.AddHeader("Postman-Token", "7afdfed5-4243-4cda-8b95-ac9f2dc1b2f3");
                                                        requestSplash.AddHeader("cache-control", "no-cache");
                                                        requestSplash.AddHeader("X-Cisco-Meraki-API-Key", Context.ApiKey);
                                                        IRestResponse responseSplash = clientSplash.Execute(requestSSID);

                                                        if (responseSplash.StatusCode == HttpStatusCode.OK)
                                                        {
                                                            splashSettings _splash = JsonConvert.DeserializeObject<splashSettings>(responseSplash.Content);
                                                            if (key != null)
                                                            {
                                                                string[] splashUrl = _splash.splashUrl.Split('?');
                                                                if (splashUrl.Length > 0) {

                                                                    string[] token = splashUrl[1].Split('=');
                                                                    if (token.Length > 0)
                                                                    {
                                                                        if (token[0].ToUpper().Equals("TOKEN"))
                                                                        {
                                                                            string token_decrypt = Wifi360.Data.Class.Misc.Decrypt(token[1], key.value);
                                                                            string[] token_parameters = token_decrypt.Split('|');
                                                                            int idhotel = 0, idlocation = 0;

                                                                            foreach(string parameter in token_parameters)
                                                                            {
                                                                                string[] parameter_value = parameter.Split('=');
                                                                                switch(parameter_value[0])
                                                                                {
                                                                                    case "nt_st": idhotel = int.Parse(parameter_value[1]); break;
                                                                                    case "nt_lt": idlocation = int.Parse(parameter_value[1]); break;
                                                                                }
                                                                            }

                                                                            //Hemos obtenido ya el idhotel y el idlocation, vamos a crear los datos en MerakiGPSLocation, 
                                                                            //actualizamos o creamos.
                                                                            string nas_Mac = string.Format("{0}:{1}", dev.mac.Replace(':', '-').ToUpper(), _ssid.name);
                                                                            MerakiLocationsGPS gpsLocation = BillingController.SelectMerakiLocationsGPS(nas_Mac);
                                                                            gpsLocation.IdSite = idhotel;
                                                                            gpsLocation.IdLocation = idlocation;
                                                                            gpsLocation.Latitude = dev.lat;
                                                                            gpsLocation.Longitude = dev.lng;
                                                                            gpsLocation.NAS_MAC = nas_Mac;
                                                                            gpsLocation.Description = dev.name;
                                                                            BillingController.SaveMerakiLocationsGPS(gpsLocation);
                                                                        }
                                                                        else
                                                                        {
                                                                            string[] tokenAll = splashUrl[1].Split('&');
                                                                            int idhotel = 0, idlocation = 0;
                                                                            foreach (string value_tokenAll in tokenAll)
                                                                            {
                                                                                string[] value = value_tokenAll.Split('=');
                                                                               
                                                                                switch (value[0])
                                                                                {
                                                                                    case "idhotel": idhotel = int.Parse(value[1]); break;
                                                                                    case "idlocation": idlocation = int.Parse(value[1]); break;
                                                                                }
                                                                            }

                                                                            string nas_Mac = string.Format("{0}:{1}", dev.mac.Replace(':', '-').ToUpper(), _ssid.name);
                                                                            MerakiLocationsGPS gpsLocation = BillingController.SelectMerakiLocationsGPS(nas_Mac);
                                                                            gpsLocation.IdSite = idhotel;
                                                                            gpsLocation.IdLocation = idlocation;
                                                                            gpsLocation.Latitude = dev.lat;
                                                                            gpsLocation.Longitude = dev.lng;
                                                                            gpsLocation.NAS_MAC = nas_Mac;
                                                                            gpsLocation.Description = dev.name;
                                                                            BillingController.SaveMerakiLocationsGPS(gpsLocation);


                                                                        }

                                                                    }

                                                                }
                                                            }


                                                        }
                                                    }
                                                }
                                            }
                                        }


                                        //las redes de tipo switch no tienen tipos de dispositivos.
                                        if (!net.type.ToUpper().Equals("SWITCH"))
                                        {
                                            //guardamos las group policies de la red
                                            var clientGruopPolicies = new RestClient(Context.ApiUrl + "/networks/" + net.id + "/groupPolicies");
                                            var requestGroupPolicies = new RestRequest(Method.GET);
                                            requestGroupPolicies.AddHeader("Postman-Token", "7afdfed5-4243-4cda-8b95-ac9f2dc1b2f3");
                                            requestGroupPolicies.AddHeader("cache-control", "no-cache");
                                            requestGroupPolicies.AddHeader("X-Cisco-Meraki-API-Key", Context.ApiKey);
                                            IRestResponse responseGroupPolicies = clientGruopPolicies.Execute(requestGroupPolicies);

                                            if (responseDevices.StatusCode == HttpStatusCode.OK)
                                            {
                                                var groupPolicies = JsonConvert.DeserializeObject<List<groupPolicies>>(responseGroupPolicies.Content);

                                                foreach (groupPolicies police in groupPolicies)
                                                {
                                                    ApiMerakiGroupPolicies amGroupPolice = BillingController.SelectApiMerakiGroupPolicy(net.id, police.groupPolicyId);
                                                    Misc.Map(police, amGroupPolice);
                                                    amGroupPolice.NetworkId = net.id;
                                                    amGroupPolice.LastUpdate = update;

                                                    BillingController.SaveApiMerakiGroupPolicies(amGroupPolice);
                                                }
                                            }
                                        }

                                        #endregion
                                    }
                                    catch(Exception ex)
                                    {
                                        Context.Log.WriteEntry(ELogType.Error, Context.AppName, "Maestros: Error en Red " + net.id + ": "  + ex.Message);
                                    }
                                }
                            }

                        }

                        //Eliminamos las Organizaciones, Redes y Dispositivos que no aparecieron en el ultimo bucle
                        List<ApiMerakiOrganizations> aMorganizations = BillingController.SelectApiMerakiOrganizations(update);
                        foreach(ApiMerakiOrganizations org in aMorganizations)
                        {
                            List<ApiMerakiNetworks> networks = BillingController.SelectApiMerakiNetworks(org.id);
                            foreach(ApiMerakiNetworks net in networks)
                            {
                                List<ApiMerakiDevices> devices = BillingController.SelectApiMerakiDevices(net.id);
                                foreach(ApiMerakiDevices dev in devices)
                                {
                                    BillingController.DeleteApiMerakiDevice(dev.Id);
                                }

                                List<ApiMerakiGroupPolicies> policies = BillingController.SelectApiMerakiGroupPolicies(net.id);
                                foreach(ApiMerakiGroupPolicies policy in policies)
                                {
                                    BillingController.DeleteApiMerakiGroupPolicies(policy.Id);
                                }

                                BillingController.DeleteApiMerakiNetwork(net.idNetwork);
                            }
                            BillingController.DeleteApiMerakiOrganization(org.IdOrganization);
                        }

                        //Eliminamos las redes que no hayan sido actualizadas y sus dispositivos
                        List<ApiMerakiNetworks> aMnetworks = BillingController.SelectApiMerakiNetworks(update);
                        foreach(ApiMerakiNetworks net in aMnetworks)
                        {
                            List<ApiMerakiDevices> devices = BillingController.SelectApiMerakiDevices(net.id);
                            foreach (ApiMerakiDevices dev in devices)
                            {
                                BillingController.DeleteApiMerakiDevice(dev.Id);
                            }

                            List<ApiMerakiGroupPolicies> policies = BillingController.SelectApiMerakiGroupPolicies(net.id);
                            foreach (ApiMerakiGroupPolicies policy in policies)
                            {
                                BillingController.DeleteApiMerakiGroupPolicies(policy.Id);
                            }

                            BillingController.DeleteApiMerakiNetwork(net.idNetwork);
                        }

                        //Eliminamos los dispositivos que no hayan sido actualizados
                        List<ApiMerakiDevices> aMdevices = BillingController.SelectApiMerakiDevices(update);
                        foreach (ApiMerakiDevices dev in aMdevices)
                        {
                            BillingController.DeleteApiMerakiDevice(dev.Id);
                        }

                        //Eliminamos todas las politicas que no hayan sido actualizadas
                        List<ApiMerakiGroupPolicies> aMGroupPolicies = BillingController.SelectApiMerakiGroupPolicies(update);
                        foreach(ApiMerakiGroupPolicies policy in aMGroupPolicies)
                        {
                            BillingController.DeleteApiMerakiGroupPolicies(policy.Id);
                        }
                    }

                }

                else
                   if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "**************** Ejecutando un proceso anterior de Maestros **************************");
            }
            catch (Exception ex)
            {
                Context.Log.WriteEntry(ELogType.Error, Context.AppName, ex.Message);
            }
            finally
            {
                isExecutingDatosMaestros = false;
                if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "***************** Fin el proceso Maestros **************************");
            }

        }

        private void TimerCamara(object sender, ElapsedEventArgs e)
        {
            // Para poder editar el app.config en caliente
            try
            {

                //        Thread.Sleep(10000);

                if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "**************** Empieza el proceso Maestros **************************");
                if (!isExecutingCamara)
                {
                    DateTime dateNow = DateTime.Now.ToUniversalTime();
                    dateNow = dateNow.AddSeconds(-120);
                    string s2 = dateNow.ToString("yyyy-MM-dd'T'HH:mm:ss.fffK", CultureInfo.InvariantCulture);
                    isExecutingCamara = true;

                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                    ServicePointManager.ServerCertificateValidationCallback += (sender1, certificate, chain, sslPolicyErrors) =>
                    {
                        return true;
                    };

                    System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                    var client = new RestClient(Context.ApiUrl + "/devices/Q2KD-5BTF-CA87/clients?timespan=120");
                    var request = new RestRequest(Method.GET);
                    request.AddHeader("Postman-Token", "f4883596-7cfa-4be7-89a1-0d9599cbf5a1");
                    request.AddHeader("cache-control", "no-cache");
                    request.AddHeader("X-Cisco-Meraki-API-Key", "093b24e85df15a3e66f1fc359f4c48493eaa1b73");
                    IRestResponse response = client.Execute(request);

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var clients = JsonConvert.DeserializeObject<List<client>>(response.Content);
                        client c = (from t in clients where t.mac.Equals("34:8a:7b:cf:75:2a") select t).FirstOrDefault();

                        if (c == null)
                        {

                            var clientSnapshot = new RestClient("https://api.meraki.com/api/v0/networks/L_646829496481101394/cameras/Q2EV-PAFW-7QAE/snapshot");
                            var requestSnapshot = new RestRequest(Method.POST);
                            requestSnapshot.AddHeader("Postman-Token", "c78cdefa-9818-403e-9ccd-a78ba8b361f8");
                            requestSnapshot.AddHeader("cache-control", "no-cache");
                            requestSnapshot.AddHeader("Content-Type", "application/json");
                            requestSnapshot.AddHeader("X-Cisco-Meraki-API-Key", "093b24e85df15a3e66f1fc359f4c48493eaa1b73");
                            requestSnapshot.AddParameter("undefined", "{\n\t\"timestamp\":\"" + s2 + "\"\n}", ParameterType.RequestBody);
                            IRestResponse responseSnapshot = clientSnapshot.Execute(requestSnapshot);

                            if (responseSnapshot.StatusDescription.Equals("Permanent Redirect"))
                            {
                                clientSnapshot = new RestClient(responseSnapshot.Headers[13].Value.ToString());
                                responseSnapshot = clientSnapshot.Execute(requestSnapshot);

                                if (responseSnapshot.StatusCode == HttpStatusCode.Accepted)
                                {
                                    var snapshot = JsonConvert.DeserializeObject<snapShot>(responseSnapshot.Content);
                                    string http = string.Empty;
                                    http = "<body>";
                                    http += string.Format("<b>Snapshot time</b>:{0} <br />", dateNow.ToString());
                                    http += string.Format("<img src=\"{0}\" />", snapshot.url);
                                    http += "</body>";

                                    Send("jarrocha@ilos.es", "Device out of range: 34:8a:7b:cf:75:2a", http);
                                }

                            }
                        }
                    }

                }

                else
                   if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "**************** Ejecutando un proceso anterior de Maestros **************************");
            }
            catch (Exception ex)
            {
                Context.Log.WriteEntry(ELogType.Error, Context.AppName, ex.Message);
            }
            finally
            {
                isExecutingCamara = false;
                if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "***************** Fin el proceso Maestros **************************");
            }

        }

        private void TimerElapsedAccounting(object sender, ElapsedEventArgs e)
        {
            // Para poder editar el app.config en caliente
            try
            {
                ConfigurationManager.RefreshSection("connectionStrings");
                ConfigurationManager.RefreshSection("appSettings");

                

                if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "**************************** Empieza el proceso de Accounting ********************");
                if (!isExecutingThreadAccounting)
                {
                    isExecutingThreadAccounting = true;
                    string accountingTime = ConfigurationManager.AppSettings["AccountingInterval"].ToString();

                    List<ApiMerakiSplashLogins> activeSessions = BillingController.SelectApiMerakiSplashLogins(true);
                    foreach(ApiMerakiSplashLogins actSession in activeSessions)
                    {
                        DateTime dateInserted = DateTime.Now;
                        long epoch = Misc.DateTimeToUnixTime(dateInserted) - actSession.loginAt;
                        if (epoch % Double.Parse(accountingTime) == 0)
                        {
                            ApiMerakiDevices amDevice = BillingController.SelectApiMerakiDevice(actSession.gatewayDeviceMac);

                            if (!amDevice.Id.Equals(0))
                            {
                                //UNA VEZ OBTENIDO EL DEVICE, OBTENEMOS SU ACTIVIDAD

                                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                                ServicePointManager.ServerCertificateValidationCallback += (sender1, certificate, chain, sslPolicyErrors) =>
                                {
                                    return true;
                                };

                                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                                var client = new RestClient(Context.ApiUrl + "/devices/" + amDevice.serial + "/clients?timespan=" + epoch);
                                var request = new RestRequest(Method.GET);
                                request.AddHeader("Postman-Token", "7afdfed5-4243-4cda-8b95-ac9f2dc1b2f3");
                                request.AddHeader("cache-control", "no-cache");
                                request.AddHeader("X-Cisco-Meraki-API-Key", Context.ApiKey);
                                IRestResponse response = client.Execute(request);

                                if (response.StatusCode == HttpStatusCode.OK)
                                {
                                    List<client> clients = JsonConvert.DeserializeObject<List<client>>(response.Content);
                                    client obj = (from t in clients where t.mac.Equals(actSession.clientMac) select t).FirstOrDefault();
                                    if (obj != null)
                                    {
                                        string NasMac = string.Format("{0}:{1}", actSession.gatewayDeviceMac.Replace(':', '-').ToUpper(), actSession.ssid);
                                        MerakiLocationsGPS merakilocation = BillingController.SelectMerakiLocationsGPS(NasMac);
                                        FDSLocationsIdentifier locationIdentifier = BillingController.GetFDSLocationIdentifier(merakilocation.IdLocation);

                                        ApiMerakiAccounting amAccounting = new ApiMerakiAccounting();
                                        amAccounting.IdHotel = merakilocation.IdSite;
                                        amAccounting.IdLocation = locationIdentifier.IdLocation;
                                        amAccounting.LocationIdentifier = locationIdentifier.Identifier;
                                        amAccounting.User = actSession.name;
                                        amAccounting.DateInserted = dateInserted.ToUniversalTime();
                                        amAccounting.CallerID = actSession.clientMac.Replace(':', '-').ToUpper();
                                        amAccounting.NAS_MAC = NasMac;
                                        amAccounting.AcctSessionId = string.Format("AM|{0}|{1}|{2}", actSession.gatewayDeviceMac, actSession.clientId, actSession.loginAt);
                                        amAccounting.SessionTime = int.Parse(epoch.ToString());
                                        amAccounting.BytesIn = Int64.Parse(Math.Round(obj.usage.recv * 1024, 0).ToString());
                                        amAccounting.BytesOut = Int64.Parse(Math.Round(obj.usage.sent * 1024, 0).ToString());

                                        BillingController.SaveApiMerakiAccounting(amAccounting);
                                    }

                                }
                            }
                            else
                                if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "**************************** No existe el Device con mac :" + actSession.gatewayDeviceMac);

                        }

                    }
                }

                else
                   if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "****************************  Está ejecutando un ciclo anterior Accounting **************************** ");
            }
            catch (Exception ex)
            {
                Context.Log.WriteEntry(ELogType.Error, Context.AppName, ex.Message);
            }
            finally
            {
                isExecutingThreadAccounting = false;
                if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, " ***************************Fin del proceso de Accounting * *******************");
            }

        }

        private void TimerElapsedIdleTimeOut(object sender, ElapsedEventArgs e)
        {
            // Para poder editar el app.config en caliente
            try
            {
                ConfigurationManager.RefreshSection("connectionStrings");
                ConfigurationManager.RefreshSection("appSettings");

                if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "**************************** Empieza el proceso de Idle TimeOut ********************");
                if (!isExecutingThreadIdleTimeOut)
                {
                    isExecutingThreadIdleTimeOut = true;
                    string idleTimeOut = ConfigurationManager.AppSettings["IdleTimeOut"].ToString();

                    //Obtenemos los usuarios que está activos
                    List<ApiMerakiSplashLogins> activeSessions = BillingController.SelectApiMerakiSplashLogins(true);
                    DateTime closedSessionDate = DateTime.Now.ToUniversalTime();
                    foreach (ApiMerakiSplashLogins actSession in activeSessions)
                    {
                        TimeSpan diff = (closedSessionDate - actSession.loginAtUTC);
                        List<ApiMerakiAccounting> accounting = BillingController.SelectApiMerakiAccounting(string.Format("AM|{0}|{1}|{2}", actSession.gatewayDeviceMac, actSession.clientId, actSession.loginAt), actSession.loginAtUTC);
                        //no hay entrada en los ultimos 15 minutos
                        if (accounting.Count.Equals(0) && (Math.Round(diff.TotalSeconds, 0) > int.Parse(idleTimeOut)))
                        {
                            ApiMerakiAccounting lastAccounting = BillingController.SelectApiMerakiAccountingLastbyActSessionId(string.Format("AM|{0}|{1}|{2}", actSession.gatewayDeviceMac, actSession.clientId, actSession.loginAt));
                            ActiveSessions active = BillingController.SelectActiveSessions(actSession.clientMac.Replace(':', '-').ToUpper(), string.Format("AM|{0}|{1}|{2}", actSession.gatewayDeviceMac, actSession.clientId, actSession.loginAt));
                            if (!lastAccounting.Id.Equals(0) && !active.IdActiveSession.Equals(0))
                            {
                                //creamos entrada en ClosedSessions
                                ClosedSessions2 closedSession = new ClosedSessions2();

                                closedSession.IdHotel = active.IdHotel;
                                closedSession.UserName = active.UserName;
                                closedSession.NAS = active.NAS;
                                closedSession.Port = active.Port;
                                closedSession.SessionStarted = active.SessionStarted;
                                closedSession.SessionTerminated = closedSessionDate;
                                closedSession.CallerID = active.CallerID;
                                closedSession.AcctSessionID = active.AcctSessionID;
                                closedSession.FramedIP = active.FramedIP;
                                closedSession.LocationIdentifier = active.LocationIdentifier;
                                closedSession.NAS_MAC = active.NAS_MAC;
                                closedSession.BytesIn = lastAccounting.BytesIn;
                                closedSession.BytesOut = lastAccounting.BytesOut;

                                BillingController.SaveClosedSessions(closedSession);
                                BillingController.DeleteActiveSession(active.IdActiveSession);

                            }

                            //eliminamos de active session
                            BillingController.DeleteApiMerakiAccounting(string.Format("AM|{0}|{1}|{2}", actSession.gatewayDeviceMac, actSession.clientId, actSession.loginAt));

                            //cambiamos el estado 
                            actSession.OnlineStatus = 0;
                            BillingController.SaveApiMerakiSplashLogin(actSession);
                        }
                        else
                        {
                            ApiMerakiAccounting first = accounting.First();
                            ApiMerakiAccounting last = (from t in accounting where t.SessionTime <= (first.SessionTime - Int32.Parse(idleTimeOut)) select t).FirstOrDefault();
                            if (last != null)
                            {
                                TimeSpan timeDifference = first.DateInserted - last.DateInserted;
                                if (Math.Round(timeDifference.TotalSeconds,0) >= Int32.Parse(idleTimeOut))
                                {
                                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                                    ServicePointManager.ServerCertificateValidationCallback += (sender1, certificate, chain, sslPolicyErrors) =>
                                    {
                                        return true;
                                    };

                                    ApiMerakiDevices amDevice = BillingController.SelectApiMerakiDevice(actSession.gatewayDeviceMac);
                                    System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                                    var client = new RestClient(Context.ApiUrl + "/networks/" + amDevice.networkId + "/clients/" + actSession.clientMac);
                                    var request = new RestRequest(Method.GET);
                                    request.AddHeader("Postman-Token", "7afdfed5-4243-4cda-8b95-ac9f2dc1b2f3");
                                    request.AddHeader("cache-control", "no-cache");
                                    request.AddHeader("X-Cisco-Meraki-API-Key", Context.ApiKey);
                                    IRestResponse response = client.Execute(request);

                                    if (response.StatusCode == HttpStatusCode.OK)
                                    {

                                    }

                                    if (first.BytesIn.Equals(last.BytesIn) && first.BytesOut.Equals(last.BytesOut))
                                    { // son iguales y hay que expulsarlo
                                        string actSessionID = string.Format("AM|{0}|{1}|{2}", actSession.gatewayDeviceMac, actSession.clientId, actSession.loginAt);
                                        ClosedSessions2 closedSession = new ClosedSessions2();

                                        //si el usuario está en la tabla de active sessions
                                        ActiveSessions active = BillingController.SelectActiveSessions(actSession.clientMac.Replace(':', '-').ToUpper(), actSessionID);
                                        if (!active.IdActiveSession.Equals(0))
                                        {
                                            closedSession.IdHotel = active.IdHotel;
                                            closedSession.UserName = active.UserName;
                                            closedSession.NAS = active.NAS;
                                            closedSession.Port = active.Port;
                                            closedSession.SessionStarted = active.SessionStarted;
                                            closedSession.SessionTerminated = closedSessionDate;
                                            closedSession.CallerID = active.CallerID;
                                            closedSession.AcctSessionID = active.AcctSessionID;
                                            closedSession.FramedIP = active.FramedIP;
                                            closedSession.LocationIdentifier = active.LocationIdentifier;
                                            closedSession.NAS_MAC = active.NAS_MAC;
                                            closedSession.BytesIn = last.BytesIn;
                                            closedSession.BytesOut = last.BytesOut;

                                            BillingController.DeleteActiveSession(active.IdActiveSession);
                                        }
                                        else
                                        {
                                            string NasMac = string.Format("{0}:{1}", actSession.gatewayDeviceMac.Replace(':', '-').ToUpper(), actSession.ssid);
                                            MerakiLocationsGPS merakilocation = BillingController.SelectMerakiLocationsGPS(NasMac);
                                            FDSLocationsIdentifier locationIdentifier = BillingController.GetFDSLocationIdentifier(merakilocation.IdLocation);

                                            closedSession.IdHotel = locationIdentifier.IdSite;
                                            closedSession.LocationIdentifier = locationIdentifier.Identifier;
                                            closedSession.SessionTerminated = closedSessionDate;
                                            closedSession.SessionStarted = actSession.loginAtUTC;
                                            closedSession.CallerID = actSession.clientMac.Replace(':', '-').ToUpper();
                                            closedSession.NAS_MAC = NasMac;
                                            closedSession.BytesIn = last.BytesIn;
                                            closedSession.BytesOut = last.BytesOut;
                                            closedSession.AcctSessionID = actSessionID;
                                            closedSession.Port = 0;
                                            closedSession.UserName = actSession.login;
                                        }

                                        BillingController.SaveClosedSessions(closedSession);

                                        BillingController.DeleteApiMerakiAccounting(actSessionID);

                                        actSession.OnlineStatus = 0;
                                        BillingController.SaveApiMerakiSplashLogin(actSession);

                                       
                                    }
                                }
                            }
                        }
                    }
                }

                else
                   if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "****************************  Está ejecutando un ciclo Idle TimeOut **************************** ");
            }
            catch (Exception ex)
            {
                Context.Log.WriteEntry(ELogType.Error, Context.AppName, ex.Message);
            }
            finally
            {
                isExecutingThreadIdleTimeOut = false;
                if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, " *************************** Fin del proceso de  Idle TimeOut  * *******************");
            }

        }

        private void TimerElapsedSplashLogins(object sender, ElapsedEventArgs e)
        {
            Thread tid1 = new Thread(new ThreadStart(ThreadSplashLogin));
            tid1.Start();
        }

        private void ThreadSplashLogin()
        {
            try
            {
                ConfigurationManager.RefreshSection("connectionStrings");
                ConfigurationManager.RefreshSection("appSettings");
                

                if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "************************** Empieza el proceso ThreadSplashLogin *****************");
                if (!isExecutingThreadSplashLogin)
                {
                    isExecutingThreadSplashLogin = true;

                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                    ServicePointManager.ServerCertificateValidationCallback += (sender1, certificate, chain, sslPolicyErrors) =>
                    {
                        return true;
                    };

                    System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                    //OBTENEMOS LAS ORGANIZACIONES EN LAS QUE EL TOKEN TIENE PERMISOS
                    if (Context.IsLog) Context.Log.WriteEntry(ELogType.Debug, Context.AppName, Context.ApiUrl + "/organizations");
                    var client = new RestClient(Context.ApiUrl + "/organizations");
                    var request = new RestRequest(Method.GET);
                    request.AddHeader("Postman-Token", "7afdfed5-4243-4cda-8b95-ac9f2dc1b2f3");
                    request.AddHeader("cache-control", "no-cache");
                    request.AddHeader("X-Cisco-Meraki-API-Key", Context.ApiKey);
                    IRestResponse response = client.Execute(request);

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        //PARA CADA ORGANIZACION, OBENTEMOS LA LISTA DE REDES DISPONIBLES
                        if (Context.IsLog) Context.Log.WriteEntry(ELogType.Debug, Context.AppName, response.Content);
                        var organizations = JsonConvert.DeserializeObject<List<Organization>>(response.Content);
                        foreach (Organization obj in organizations)
                        {

                            ApiMerakiOrganizations amOrganization = BillingController.SelectApiMerakiOrganization(obj.id);
                            if (!amOrganization.IdOrganization.Equals(0))
                            {
                                if (Context.IsLog) Context.Log.WriteEntry(ELogType.Debug, Context.AppName, "------------------------ ORGANIZACION " + obj.name + "-------------------------");
                                if (Context.IsLog) Context.Log.WriteEntry(ELogType.Debug, Context.AppName, Context.ApiUrl + "/organizations/" + obj.id + "/networks");
                                var client2 = new RestClient(Context.ApiUrl + "/organizations/" + obj.id + "/networks");
                                var request2 = new RestRequest(Method.GET);
                                request2.AddHeader("Postman-Token", "7afdfed5-4243-4cda-8b95-ac9f2dc1b2f3");
                                request2.AddHeader("cache-control", "no-cache");
                                request2.AddHeader("X-Cisco-Meraki-API-Key", Context.ApiKey);
                                IRestResponse response2 = client2.Execute(request2);

                                if (response2.StatusCode == HttpStatusCode.OK)
                                {
                                    //PARA CADA RED, OBTENEMOS LOS INTENTOS DE CONEXION
                                    if (Context.IsLog) Context.Log.WriteEntry(ELogType.Debug, Context.AppName, response2.Content);
                                    var networks = JsonConvert.DeserializeObject<List<Network>>(response2.Content);

                                    foreach (Network net in networks)
                                    {
                                        try
                                        {
                                            #region Para cada Red
                                            ApiMerakiNetworks amNetwork = BillingController.SelectApiMerakiNetwork(net.id);
                                            if (!amNetwork.idNetwork.Equals(0))
                                            {
                                                if (Context.IsLog) Context.Log.WriteEntry(ELogType.Debug, Context.AppName, "------------------------ RED " + net.name + "-------------------------");

                                                if (!amNetwork.LastUserLogin.HasValue ||(amNetwork.LastUserLogin.Value < Misc.DateTimeToUnixTime(DateTime.Now)))
                                                {

                                                    string timeSpan = string.Empty;
                                                    if (amNetwork.LastUserLogin.HasValue)
                                                        timeSpan = (Misc.DateTimeToUnixTime(DateTime.Now) - amNetwork.LastUserLogin.Value).ToString();
                                                    else
                                                        timeSpan = "84600";

                                                    if (Context.IsLog) Context.Log.WriteEntry(ELogType.Debug, Context.AppName, Context.ApiUrl + "/networks/" + net.id + "/splashLoginAttempts?timespan=" + timeSpan);
                                                    var clientSplashLoginAttemps = new RestClient(Context.ApiUrl + "/networks/" + net.id + "/splashLoginAttempts?timespan=" + timeSpan);
                                                    var requestSplashLoginAttemps = new RestRequest(Method.GET);
                                                    requestSplashLoginAttemps.AddHeader("Postman-Token", "7afdfed5-4243-4cda-8b95-ac9f2dc1b2f3");
                                                    requestSplashLoginAttemps.AddHeader("cache-control", "no-cache");
                                                    requestSplashLoginAttemps.AddHeader("X-Cisco-Meraki-API-Key", Context.ApiKey);
                                                    IRestResponse responseSplashLoginAttemps = clientSplashLoginAttemps.Execute(requestSplashLoginAttemps);

                                                    if (responseSplashLoginAttemps.StatusCode == HttpStatusCode.OK)
                                                    {
                                                        //PARA CADA INTENTO DE CONEXIÓN
                                                        if (Context.IsLog) Context.Log.WriteEntry(ELogType.Debug, Context.AppName, responseSplashLoginAttemps.Content);
                                                        var splashLogins = JsonConvert.DeserializeObject<List<splashLoginAttempts>>(responseSplashLoginAttemps.Content);

                                                        foreach (splashLoginAttempts splash in splashLogins)
                                                        {
                                                            if (Context.IsLog) Context.Log.WriteEntry(ELogType.Debug, Context.AppName, "------------------------ SPLASH LOGIN ATTEMP " + splash.login + "-------------------------");
                                                            //COMPROBAMOS QUE NO EXISTA YA EN LA TABLA CON LA COMBINACION CLIENTID-CLIENTMAC-LOGINAT
                                                            ApiMerakiSplashLogins splashLogin = BillingController.SelectApiMerakiSplashLogin(splash.clientId, splash.clientMac, splash.loginAt);
                                                            if (splashLogin.Id.Equals(0))
                                                            {
                                                                #region comprobar si existe un usuario anterior

                                                                ApiMerakiSplashLogins splashOnline = BillingController.SelectApiMerakiSplashLogin(splash.clientId, splash.clientMac, true);
                                                                if (!splashOnline.Id.Equals(0))
                                                                {
                                                                    splashOnline.OnlineStatus = 0;
                                                                    BillingController.SaveApiMerakiSplashLogin(splashOnline);

                                                                    ApiMerakiDevices amDevice = BillingController.SelectApiMerakiDevice(splash.gatewayDeviceMac);

                                                                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                                                                    ServicePointManager.ServerCertificateValidationCallback += (sender1, certificate, chain, sslPolicyErrors) =>
                                                                    {
                                                                        return true;
                                                                    };

                                                                    DateTime dateClosedSession = DateTime.Now.ToUniversalTime();
                                                                    string epoch = (Misc.DateTimeToUnixTime(dateClosedSession) - splashOnline.loginAt).ToString();

                                                                    System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                                                                    var clientOnline = new RestClient(Context.ApiUrl + "/devices/" + amDevice.serial + "/clients?timespan=" + epoch);
                                                                    var requestOnline = new RestRequest(Method.GET);
                                                                    requestOnline.AddHeader("Postman-Token", "7afdfed5-4243-4cda-8b95-ac9f2dc1b2f3");
                                                                    requestOnline.AddHeader("cache-control", "no-cache");
                                                                    requestOnline.AddHeader("X-Cisco-Meraki-API-Key", Context.ApiKey);
                                                                    IRestResponse responseOnline = clientOnline.Execute(request);

                                                                    if (responseOnline.StatusCode == HttpStatusCode.OK)
                                                                    {
                                                                        List<client> clients = JsonConvert.DeserializeObject<List<client>>(responseOnline.Content);
                                                                        client clientApi = (from t in clients where t.mac.Equals(splashOnline.clientMac) select t).FirstOrDefault();
                                                                        if (clientApi != null)
                                                                        {
                                                                            ClosedSessions2 closedSession = new ClosedSessions2();
                                                                            ActiveSessions active = BillingController.SelectActiveSessions(splashOnline.clientMac.Replace(':', '-').ToUpper(), string.Format("AM|{0}|{1}|{2}", splashOnline.gatewayDeviceMac, splashOnline.clientId, splashOnline.loginAt));
                                                                            if (!active.IdActiveSession.Equals(0))
                                                                            {
                                                                                closedSession.IdHotel = active.IdHotel;
                                                                                closedSession.UserName = active.UserName;
                                                                                closedSession.NAS = active.NAS;
                                                                                closedSession.Port = active.Port;
                                                                                closedSession.SessionStarted = active.SessionStarted;
                                                                                closedSession.SessionTerminated = dateClosedSession;
                                                                                closedSession.CallerID = active.CallerID;
                                                                                closedSession.AcctSessionID = active.AcctSessionID;
                                                                                closedSession.FramedIP = active.FramedIP;
                                                                                closedSession.LocationIdentifier = active.LocationIdentifier;
                                                                                closedSession.NAS_MAC = active.NAS_MAC;
                                                                                closedSession.BytesIn = Int64.Parse(Math.Round(clientApi.usage.recv * 1024, 0).ToString());
                                                                                closedSession.BytesOut = Int64.Parse(Math.Round(clientApi.usage.sent * 1024, 0).ToString());

                                                                                BillingController.DeleteActiveSession(active.IdActiveSession);
                                                                                BillingController.SaveClosedSessions(closedSession);

                                                                                //falta actualizar los valores del usuario y desactivalo si necesario
                                                                            }
                                                                        }
                                                                    }

                                                                }

                                                                #endregion

                                                                #region Introducir
                                                                //SI NO HA SIDO YA INTRODUCIDO, LO INTRODUCIMOS EN LA TABLA DE SPLASHLOGINS
                                                                Misc.Map(splash, splashLogin);
                                                                splashLogin.OnlineStatus = 1;
                                                                splashLogin.loginAtUTC = Misc.UnixTimeToDateTime(splashLogin.loginAt);
                                                                if (splash.authorization.Equals(Misc.SplashLoginResult.failure.ToString()))
                                                                {
                                                                    splashLogin.name = string.Empty;
                                                                    splashLogin.OnlineStatus = 0;
                                                                }


                                                                BillingController.SaveApiMerakiSplashLogin(splashLogin);
                                                                if (!amNetwork.LastUserLogin.HasValue || amNetwork.LastUserLogin > splashLogin.loginAt)
                                                                    amNetwork.LastUserLogin = splashLogin.loginAt;

                                                                if (splashLogin.authorization.Equals(Misc.SplashLoginResult.success.ToString()))
                                                                {
                                                                    //Hacemos la entrada en la tabla de ActiveSessions
                                                                    //creamos el identificador
                                                                    string NasMac = string.Format("{0}:{1}", splash.gatewayDeviceMac.Replace(':', '-').ToUpper(), splash.ssid);
                                                                    MerakiLocationsGPS merakilocation = BillingController.SelectMerakiLocationsGPS(NasMac);
                                                                    FDSLocationsIdentifier locationIdentifier = BillingController.GetFDSLocationIdentifier(merakilocation.IdLocation);
                                                                    Users merakiUser = BillingController.GetRadiusUser(splashLogin.name.ToUpper(), locationIdentifier.IdSite);

                                                                    ActiveSessions activeSession = new ActiveSessions();
                                                                    activeSession.IdHotel = merakilocation.IdSite;
                                                                    activeSession.LocationIdentifier = locationIdentifier.Identifier;
                                                                    activeSession.UserName = splashLogin.name;
                                                                    activeSession.SessionStarted = Misc.UnixTimeToDateTime(splashLogin.loginAt);
                                                                    activeSession.CallerID = splashLogin.clientMac.Replace(':', '-').ToUpper();
                                                                    activeSession.NAS_MAC = NasMac;
                                                                    activeSession.Port = 0;
                                                                    activeSession.AcctSessionID = string.Format("AM|{0}|{1}|{2}", splash.gatewayDeviceMac, splash.clientId, splash.loginAt);

                                                                    //OBTENEMOS LA LOS DETALLES DEL CLIENTE PARA OBTENER LA IP
                                                                    if (Context.IsLog) Context.Log.WriteEntry(ELogType.Debug, Context.AppName, Context.ApiUrl + "/networks/" + net.id + "/clients/" + splashLogin.clientId);
                                                                    var clientClientDetails = new RestClient(Context.ApiUrl + "/networks/" + net.id + "/clients/" + splashLogin.clientId);
                                                                    var requestClientDetails = new RestRequest(Method.GET);
                                                                    requestClientDetails.AddHeader("Postman-Token", "7afdfed5-4243-4cda-8b95-ac9f2dc1b2f3");
                                                                    requestClientDetails.AddHeader("cache-control", "no-cache");
                                                                    requestClientDetails.AddHeader("X-Cisco-Meraki-API-Key", Context.ApiKey);
                                                                    IRestResponse responseClientDetails = clientClientDetails.Execute(requestClientDetails);

                                                                    if (responseClientDetails.StatusCode == HttpStatusCode.OK)
                                                                    {
                                                                        if (Context.IsLog) Context.Log.WriteEntry(ELogType.Debug, Context.AppName, responseSplashLoginAttemps.Content);
                                                                        var clientDetails = JsonConvert.DeserializeObject<clientDetails>(responseClientDetails.Content);

                                                                        activeSession.FramedIP = clientDetails.ip;

                                                                    }

                                                                    BillingController.SaveActiveSession(activeSession);

                                                                    if (!merakiUser.IdUser.Equals(0))
                                                                    {
                                                                        //Actualizamos la politica del usuario
                                                                        Priorities priority = BillingController.GetPriority(merakiUser.Priority);
                                                                        ApiMerakiGroupPolicies groupPolicy = BillingController.SelectApiMerakiGroupPolicy(net.id, priority.Description);

                                                                        var clientPutPolicy = new RestClient(Context.ApiUrl + "/networks/" + net.id + "/clients/" + splash.clientMac + "/policy?timespan=86400&groupPolicyId=" + groupPolicy.groupPolicyId + "&devicePolicy=group");
                                                                        var requestPutPolicy = new RestRequest(Method.PUT);
                                                                        requestPutPolicy.AddHeader("Postman-Token", "dc563683-7abd-4a7f-832b-18df5eec0ea7");
                                                                        requestPutPolicy.AddHeader("cache-control", "no-cache");
                                                                        requestPutPolicy.AddHeader("Content-Type", "application/json");
                                                                        requestPutPolicy.AddHeader("X-Cisco-Meraki-API-Key", Context.ApiKey);
                                                                        requestPutPolicy.AddParameter("undefined", "{\n  \"devicePolicy\": \"group\",\n  \"groupPolicyId\": " + groupPolicy.groupPolicyId + "\n}", ParameterType.RequestBody);
                                                                        IRestResponse responsePutPolicy = clientPutPolicy.Execute(request);

                                                                        if (responsePutPolicy.StatusCode == HttpStatusCode.OK)
                                                                        {
                                                                            ;
                                                                        }
                                                                    }

                                                                    long timeSpanPolicy = Misc.DateTimeToUnixTime(DateTime.Now.ToUniversalTime()) - splash.loginAt;

                                                                    if (Context.IsLog) Context.Log.WriteEntry(ELogType.Debug, Context.AppName, Context.ApiUrl + Context.ApiUrl + "/networks/" + net.id + "/clients/" + splash.clientMac + "/policy?timespan=84200");
                                                                    var clientGroupPolicy = new RestClient(Context.ApiUrl + "/networks/" + net.id + "/clients/" + splash.clientMac + "/policy?timespan=84200");
                                                                    var requestGroupPolicy = new RestRequest(Method.GET);
                                                                    requestGroupPolicy.AddHeader("Postman-Token", "7afdfed5-4243-4cda-8b95-ac9f2dc1b2f3");
                                                                    requestGroupPolicy.AddHeader("cache-control", "no-cache");
                                                                    requestGroupPolicy.AddHeader("X-Cisco-Meraki-API-Key", Context.ApiKey);

                                                                    IRestResponse responseGroupPolicy = clientGroupPolicy.Execute(requestGroupPolicy);
                                                                    if (responseGroupPolicy.StatusCode == HttpStatusCode.OK)
                                                                    {
                                                                        var doc = JsonConvert.DeserializeXmlNode(responseGroupPolicy.Content, "root");
                                                                        //XmlDocument doc = JsonToXml(responseGroupPolicy.Content);

                                                                    }

                                                                    var clientPut = new RestClient(Context.ApiUrl + "/networks/" + net.id + "/clients/" + splash.clientMac + "/splashAuthorizationStatus");
                                                                    var requestPut = new RestRequest(Method.PUT);
                                                                    requestPut.RequestFormat = DataFormat.Json;
                                                                    requestPut.AddHeader("Postman-Token", "4ef6bd1f-387f-4a3d-b68c-3049a15582d4");
                                                                    requestPut.AddHeader("cache-control", "no-cache");
                                                                    requestPut.AddHeader("Content-Type", "application/json");
                                                                    requestPut.AddHeader("X-Cisco-Meraki-API-Key", Context.ApiKey);
                                                                    requestPut.AddParameter("application/json", "[{\n\"ssids\":{\"1\":{\"isAuthorized\": false}}}]", ParameterType.RequestBody);
                                                                    IRestResponse responsePut = clientPut.Execute(requestPut);

                                                                    if (responsePut.StatusDescription.Equals("Permanent Redirect"))
                                                                    {
                                                                        clientPut = new RestClient(responsePut.Headers[13].Value.ToString());
                                                                        responsePut = clientPut.Execute(requestPut);



                                                                    }


                                                                    if (Context.IsLog) Context.Log.WriteEntry(ELogType.Debug, Context.AppName, "Entrada en active Session --> " + splashLogin.clientMac + " : " + splashLogin.name);

                                                                }

                                                                #endregion

                                                                #region Comprobamos Status

                                                                if (Context.IsLog) Context.Log.WriteEntry(ELogType.Debug, Context.AppName, Context.ApiUrl + "/networks/" + net.id + "/clients/" + splash.clientMac + "/splashAuthorizationStatus");
                                                                var clientSplashAuthorizationStatus = new RestClient(Context.ApiUrl + "/networks/" + net.id + "/clients/" + splash.clientMac + "/splashAuthorizationStatus");
                                                                var requestSplashAuthorizationStatus = new RestRequest(Method.GET);
                                                                requestSplashAuthorizationStatus.AddHeader("Postman-Token", "7afdfed5-4243-4cda-8b95-ac9f2dc1b2f3");
                                                                requestSplashAuthorizationStatus.AddHeader("cache-control", "no-cache");
                                                                requestSplashAuthorizationStatus.AddHeader("X-Cisco-Meraki-API-Key", Context.ApiKey);
                                                                IRestResponse responseSplashAuthorizationStatus = clientSplashAuthorizationStatus.Execute(requestSplashAuthorizationStatus);

                                                                if (responseSplashAuthorizationStatus.StatusCode == HttpStatusCode.OK)
                                                                {
                                                                    Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "SPLASH LOGIN STATUS  ------> Red:" + net.name + " --- ClientMac: " + splash.clientMac + " --> " + responseSplashAuthorizationStatus.Content);
                                                                }
                                                                #endregion
                                                            }
                                                        }
                                                    }

                                                    BillingController.SaveApiMerakiNetwork(amNetwork);
                                                }
                                                else
                                                    if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "El tiempo de la ultima comprobación es superior al actual --> " + net.id + " : " + net.name);
                                            }
                                            else
                                                if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "La Red no se encuentra en la BBDD --> " + net.id + " : " + net.name);

                                            #endregion
                                        }
                                        catch(Exception ex)
                                        {
                                            Context.Log.WriteEntry(ELogType.Error, Context.AppName, "Error en red " + net.id + ": " + ex.Message);
                                        }
                                    }
                                }
                            }
                            else
                                if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "La organización no se encuentra en la BBDD --> " + obj.id +" : " + obj.name);

                    }
                }

                }

                else
                   if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "******************** Está ejecutando un ciclo anterior ThreadSplashLogin ********************");
            }
            catch (Exception ex)
            {
                Context.Log.WriteEntry(ELogType.Error, Context.AppName, ex.Message);
            }
            finally
            {
                isExecutingThreadSplashLogin = false;
                if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "************************** Fin del proceso ThreadSplashLogin *****************");
            }
        }

        private void SetServiceConfigKey(string KeyName, string value)
        {
            try
            {
                string configPath = string.Format(@"{0}\{1}.config", System.AppDomain.CurrentDomain.BaseDirectory, System.AppDomain.CurrentDomain.FriendlyName);
                ConfigLowLevel.SetAppSetting(configPath, KeyName, value);

            }
            catch (Exception ex)
            {
                Context.Log.WriteEntry(ELogType.Error, Context.AppName, ex.InnerException);
            }
        }

        private string GetAppSetting(string key)
        {
            try
            {
                string fileName = string.Format(@"{0}\{1}.config", System.AppDomain.CurrentDomain.BaseDirectory, System.AppDomain.CurrentDomain.FriendlyName);
                if (!File.Exists(fileName))
                    return string.Empty;

                XmlDocument doc = new XmlDocument();
                doc.Load(fileName);

                XmlElement root = doc.DocumentElement;
                foreach (XmlNode node in root.ChildNodes)
                {
                    if ((node.Name == "appSettings") && (node.HasChildNodes))
                    {
                        foreach (XmlNode childNode in node.ChildNodes)
                        {
                            if ((childNode.Name == "add") && (childNode.Attributes["key"].Value == key))
                            {
                                return childNode.Attributes["value"].Value;
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Context.Log.WriteEntry(ELogType.Error, Context.AppName, ex.InnerException);
                return string.Empty;
            }

            return string.Empty;

        }

        public static XmlDocument JsonToXml(string json)
        {
            XmlNode newNode = null;
            XmlNode appendToNode = null;
            XmlDocument returnXmlDoc = new XmlDocument();
            returnXmlDoc.LoadXml("<Document />");
            XmlNode rootNode = returnXmlDoc.SelectSingleNode("Document");
            appendToNode = rootNode;

            string[] arrElementData;
            string[] arrElements = json.Split('\r');
            foreach (string element in arrElements)
            {
                string processElement = element.Replace("\r", "").Replace("\n", "").Replace("\t", "").Trim();
                if ((processElement.IndexOf("}") > -1 || processElement.IndexOf("]") > -1) && appendToNode != rootNode)
                {
                    appendToNode = appendToNode.ParentNode;
                }
                else if (processElement.IndexOf("[") > -1)
                {
                    processElement = processElement.Replace(":", "").Replace("[", "").Replace("\"", "").Trim();
                    newNode = returnXmlDoc.CreateElement(processElement);
                    appendToNode.AppendChild(newNode);
                    appendToNode = newNode;
                }
                else if (processElement.IndexOf("{") > -1 && processElement.IndexOf(":") > -1)
                {
                    processElement = processElement.Replace(":", "").Replace("{", "").Replace("\"", "").Trim();
                    newNode = returnXmlDoc.CreateElement(processElement);
                    appendToNode.AppendChild(newNode);
                    appendToNode = newNode;
                }
                else
                {
                    if (processElement.IndexOf(":") > -1)
                    {
                        arrElementData = processElement.Replace(": \"", ":").Replace("\",", "").Replace("\"", "").Split(':');
                        newNode = returnXmlDoc.CreateElement(arrElementData[0]);
                        for (int i = 1; i < arrElementData.Length; i++)
                        {
                            newNode.InnerText += arrElementData[i];
                        }

                        appendToNode.AppendChild(newNode);
                    }
                }
            }

            return returnXmlDoc;
        }

        //HACKATHON 

        private static void Send(string to, string subject, string body)
        {
            string _smtpClient = "smtp.gmail.com";
            int _smtpPort = 587;
            string _userName = "hackathon.meraki@gmail.com";
            string _password = "ilovemeraki";
            string _from = "hackathon.meraki@gmail.com";
            string _to = "javier.arrocha@netting.tech; german.arrocha@netting.tech; juanf.moreda@fibratel.com";
            bool _isBodyHtml = true;
            bool _sendAsync = false;
            bool _ssl = true;

            try
            {
                SmtpClient smtpClient = new SmtpClient(_smtpClient, _smtpPort);
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.EnableSsl = _ssl;

                if ((_userName != string.Empty) && (_password != string.Empty))
                {
                    NetworkCredential networkCredential = new NetworkCredential(_userName, _password);
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = networkCredential;
                }

                string[] mails = _to.Split(';');

                foreach (string to_ in mails)
                {
                    MailMessage mailMessage = new MailMessage(_from, to_, subject, body);
                    mailMessage.IsBodyHtml = _isBodyHtml;

                    if (_sendAsync)
                        smtpClient.SendAsync(mailMessage, null);
                    else
                        smtpClient.Send(mailMessage);
                }
            }
            catch
            {
                throw;
            }
        }

        //

        #endregion
    }
}
