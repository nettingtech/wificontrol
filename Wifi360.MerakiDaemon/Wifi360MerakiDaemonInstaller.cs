﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;
using System.Threading.Tasks;

namespace Wifi360.MerakiDaemon
{
    [RunInstaller(true)]
    public partial class Wifi360MerakiDaemonInstaller : System.Configuration.Install.Installer
    {
        public Wifi360MerakiDaemonInstaller()
        {
            InitializeComponent();

            serviceProcessInstaller1.Account = ServiceAccount.LocalSystem;
            serviceProcessInstaller1.Username = null;
            serviceProcessInstaller1.Password = null;

            //# Service Information
            serviceInstaller1.DisplayName = "Wifi 360 Meraki Api Daemon Service";
            serviceInstaller1.Description = "Wifi 360 Meraki Api  Daemon Service";
            serviceInstaller1.StartType = ServiceStartMode.Manual;

            //# This must be identical to the WindowsService.ServiceBase name

            //# set in the constructor of MonitorService.cs
            serviceInstaller1.ServiceName = "Wifi360.MerakiDaemon";
        }
    }
}
