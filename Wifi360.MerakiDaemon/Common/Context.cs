﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wifi360.MerakiDaemon.Common
{
    public class Context
    {
        #region Variables

        private static Log _log = null;
        private static string _appName = string.Empty;
        private static string _apiURL = string.Empty;
        private static string _apiKey = string.Empty;
        private static int _intentos = 0;
        private static bool _isLog = false;
        private static bool _timeStamp = false;

        #endregion

        #region Properties

        public static Log Log
        {
            set { _log = value; }
            get { return _log; }
        }

        public static string AppName
        {
            set { _appName = value; }
            get
            {
                _appName = ConfigurationManager.AppSettings["AppName"].ToString();
                return _appName;
            }
        }

        public static string ApiUrl
        {
            set { _apiURL = value; }
            get
            {
                _apiURL = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                return _apiURL;
            }
        }

        public static string ApiKey
        {
            set { _apiKey = value; }
            get
            {
                _apiKey = ConfigurationManager.AppSettings["ApiKey"].ToString();
                return _apiKey;
            }
        }

        public static int Intentos
        {
            set { _intentos = value; }
            get
            {
                _intentos = Int32.Parse(ConfigurationManager.AppSettings["Attemps"].ToString());
                return _intentos;
            }
        }

        public static bool IsLog
        {
            set { _isLog = value; }
            get
            {
                _isLog = Boolean.Parse(ConfigurationManager.AppSettings["IsLogin"].ToString());
                return _isLog;
            }
        }

        public static bool TimeStamp
        {
            set { _timeStamp = value; }
            get
            {
                _timeStamp = Boolean.Parse(ConfigurationManager.AppSettings["TimeStamp"].ToString());
                return _timeStamp;
            }
        }

        #endregion
    }
}
