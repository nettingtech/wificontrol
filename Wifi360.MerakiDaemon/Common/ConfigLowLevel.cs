﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Wifi360.MerakiDaemon.Common
{
    public static class ConfigLowLevel
    {
        #region Métodos públicos ConnectionString

        /// <summary>
        /// Devuelve la cadena de conexión con nombre 'name' del archivo de configuración dado por 'fileName'
        /// </summary>
        /// <param name="fileName">Nombre del fichero xml de configuración (string)</param>
        /// <param name="name">Nombre de la cadena de conexión a buscar (string)</param>
        /// <returns>El valor de la cadena de conexión buscada (string)</returns>
        public static string GetConnectionString(string fileName, string name)
        {
            if (!File.Exists(fileName))
                return string.Empty;

            XmlDocument doc = new XmlDocument();
            doc.Load(fileName);

            XmlElement root = doc.DocumentElement;
            foreach (XmlNode node in root.ChildNodes)
            {
                if ((node.Name == "connectionStrings") && (node.HasChildNodes))
                {
                    foreach (XmlNode childNode in node.ChildNodes)
                    {
                        if ((childNode.Name == "add") && (childNode.Attributes["name"].Value == name))
                        {
                            return childNode.Attributes["connectionString"].Value;
                        }
                    }
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Guarda la cadena de conexión con nombre 'name' en el archivo de configuración dado por 'fileName'
        /// </summary>
        /// <param name="fileName">Nombre del fichero xml de configuración (string)</param>
        /// <param name="name">Nombre de la cadena de conexión a reescribir (string)</param>
        /// <param name="value">Nuevo valor de la cadena de conexión (string)</param>
        public static void SetConnectionString(string fileName, string name, string value)
        {
            if (!File.Exists(fileName))
                return;

            XmlDocument doc = new XmlDocument();
            doc.Load(fileName);

            XmlElement root = doc.DocumentElement;
            foreach (XmlNode node in root.ChildNodes)
            {
                if ((node.Name == "connectionStrings") && (node.HasChildNodes))
                {
                    foreach (XmlNode childNode in node.ChildNodes)
                    {
                        if ((childNode.Name == "add") && (childNode.Attributes["name"].Value == name))
                        {
                            childNode.Attributes["connectionString"].Value = value;
                            doc.Save(fileName);
                            return;
                        }
                    }
                }
            }
        }

        #endregion Métodos públicos ConnectionString

        #region Métodos públicos AppSettings

        /// <summary>
        /// Devuelve el valor de un appSetting con la clave 'key' del archivo de configuración dado por 'fileName'
        /// </summary>
        /// <param name="fileName">Nombre del fichero xml de configuración (string)</param>
        /// <param name="key">Clave del appSetting a buscar (string)</param>
        /// <returns>El valor del appSetting buscado (string)</returns>
        public static string GetAppSetting(string fileName, string key)
        {
            if (!File.Exists(fileName))
                return string.Empty;

            XmlDocument doc = new XmlDocument();
            doc.Load(fileName);

            XmlElement root = doc.DocumentElement;
            foreach (XmlNode node in root.ChildNodes)
            {
                if ((node.Name == "appSettings") && (node.HasChildNodes))
                {
                    foreach (XmlNode childNode in node.ChildNodes)
                    {
                        if ((childNode.Name == "add") && (childNode.Attributes["key"].Value == key))
                        {
                            return childNode.Attributes["value"].Value;
                        }
                    }
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Guarda el valor de un appSetting con la clave 'key' en el archivo de configuración dado por 'fileName'
        /// </summary>
        /// <param name="fileName">Nombre del fichero xml de configuración (string)</param>
        /// <param name="key">Clave del appSetting a reescribir (string)</param>
        /// <param name="value">Nuevo valor del appSetting (string)</param>
        public static void SetAppSetting(string fileName, string key, string value)
        {
            if (!File.Exists(fileName))
                return;

            XmlDocument doc = new XmlDocument();
            doc.Load(fileName);

            XmlElement root = doc.DocumentElement;
            foreach (XmlNode node in root.ChildNodes)
            {
                if ((node.Name == "appSettings") && (node.HasChildNodes))
                {
                    foreach (XmlNode childNode in node.ChildNodes)
                    {
                        if ((childNode.Name == "add") && (childNode.Attributes["key"].Value == key))
                        {
                            childNode.Attributes["value"].Value = value;
                            doc.Save(fileName);
                            return;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Guarda el valor de un appSetting con la clave 'key' en el archivo de configuración dado por 'fileName'
        /// </summary>
        /// <param name="fileName">Nombre del fichero xml de configuración (string)</param>
        /// <param name="key">Clave del appSetting a reescribir (string)</param>
        /// <param name="value">Nuevo valor del appSetting (string)</param>
        public static void SetAppSettingKey(string fileName, string key, string value)
        {

            if (!File.Exists(fileName))
                return;

            XmlDocument doc = new XmlDocument();
            doc.Load(fileName);

            XmlElement root = doc.DocumentElement;
            foreach (XmlNode node in root.ChildNodes)
            {
                if ((node.Name == "appSettings") && (node.HasChildNodes))
                {
                    foreach (XmlNode childNode in node.ChildNodes)
                    {
                        if ((childNode.Name == "add") && (childNode.Attributes["key"].Value == key))
                        {
                            childNode.Attributes["value"].Value = value;
                            doc.Save(fileName);
                            return;
                        }
                    }
                }
            }
        }

        #endregion Métodos públicos AppSettings

        #region Métodos públicos UserSettings

        /// <summary>
        /// Devuelve el valor del userSetting con nombre 'name' y espacio de nombres 'nameSpace'
        /// del archivo de configuración dado por 'fileName'
        /// </summary>
        /// <param name="fileName">Nombre del fichero xml de configuración (string)</param>
        /// <param name="nameSpace">Nombre del espacio de nombres en donde buscar (string)</param>
        /// <param name="name">Nombre del userSetting a buscar (string)</param>
        /// <returns>El valor del userSetting buscado (string)</returns>
        public static string GetUserSetting(string fileName, string nameSpace, string name)
        {
            if (!File.Exists(fileName))
                return string.Empty;

            XmlDocument doc = new XmlDocument();
            doc.Load(fileName);

            nameSpace += ".Properties.Settings";

            XmlElement root = doc.DocumentElement;
            foreach (XmlNode node in root.ChildNodes)
            {
                if ((node.Name == "userSettings") && (node.HasChildNodes))
                {
                    foreach (XmlNode childNode in node.ChildNodes)
                    {
                        if ((childNode.Name == nameSpace) && (childNode.HasChildNodes))
                        {
                            foreach (XmlNode grandChildNode in childNode.ChildNodes)
                            {
                                if ((grandChildNode.Name == "setting") && (grandChildNode.Attributes["name"].Value == name))
                                {
                                    return grandChildNode.FirstChild.InnerText;
                                }
                            }
                        }
                    }
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Guarda el valor del userSetting con nombre 'name' y espacio de nombres 'nameSpace'
        /// del archivo de configuración dado por 'fileName'
        /// </summary>
        /// <param name="fileName">Nombre del fichero xml de configuración (string)</param>
        /// <param name="nameSpace">Nombre del espacio de nombres en donde buscar (string)</param>
        /// <param name="name">Nombre del userSetting a buscar (string)</param>
        /// <param name="value">Nuevo valor del userSetting (string)</param>
        public static void SetUserSetting(string fileName, string nameSpace, string name, string value)
        {
            if (!File.Exists(fileName))
                return;

            XmlDocument doc = new XmlDocument();
            doc.Load(fileName);

            nameSpace += ".Properties.Settings";

            XmlElement root = doc.DocumentElement;
            foreach (XmlNode node in root.ChildNodes)
            {
                if ((node.Name == "userSettings") && (node.HasChildNodes))
                {
                    foreach (XmlNode childNode in node.ChildNodes)
                    {
                        if ((childNode.Name == nameSpace) && (childNode.HasChildNodes))
                        {
                            foreach (XmlNode grandChildNode in childNode.ChildNodes)
                            {
                                if ((grandChildNode.Name == "setting") && (grandChildNode.Attributes["name"].Value == name))
                                {
                                    grandChildNode.FirstChild.InnerText = value;
                                    doc.Save(fileName);
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }

        #endregion Métodos públicos UserSettings
    }
}
