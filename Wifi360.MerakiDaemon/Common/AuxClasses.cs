﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wifi360.MerakiDaemon.Common
{
    public class Organization: BusinessBase
    {
        private int _id;
        private string _name;

        public int id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string name
        {
            get { return _name; }
            set { _name = value; }
        }

    }

    public class Network: BusinessBase
    {
        public string id { get; set; }
        public string organizationId { get; set; }
        public string name { get; set; }
        public string timeZone { get; set; }
        public string tags { get; set; }
        public string type { get; set; }
        public bool disableMyMerakiCom { get; set; }
        public bool disableRemoteStatusPage { get; set; }

    }

    public class Device: BusinessBase
    {
        public string lanIp{ get; set; }
        public string serial { get; set; }
        public string mac { get; set; }
        public string lat { get; set; }
        public string lng { get; set; }
        public string address { get; set; }
        public string name { get; set; }
        public string model { get; set; }
        public string networkId { get; set; }

    }

    public class usage: BusinessBase
    {
        public Decimal sent { get; set; }
        public Decimal recv { get; set; }
    }

    public class client : BusinessBase
    {
        public string id { get; set; }
        public string description { get; set; }
        public string mdnsName { get; set; }
        public string dhcpHostname { get; set; }
        public string mac { get; set; }
        public string vlan { get; set; }
        public string switchport { get; set; }
        public usage usage { get; set; }
    }

    public class splashLoginAttempts : BusinessBase
    {
        public string name { get; set; }
        public string login { get; set; }
        public string  ssid { get; set; }
        public long loginAt { get; set; }
        public string gatewayDeviceMac { get; set; }
        public string clientMac { get; set; }
        public string clientId { get; set; }
        public string authorization { get; set; }


    }

    public class clientDetails : BusinessBase
    {
        public string id { get; set; }
        public string mac { get; set; }
        public string ip { get; set; }
        public string ip6 { get; set; }
        public string description { get; set; }
        public long firstSeen { get; set; }
        public long lastSeen { get; set; }
        public string manufacturer { get; set; }
        public string os { get; set; }
        public string user { get; set; }
        public string vlan { get; set; }
        public string ssid { get; set; }
        public string wirelessCapabilities { get; set; }
        public bool smInstalled { get; set; }
        public string recentDevideMac { get; set; }
        public string clientVpnConnections { get; set; }
        public string lldp { get; set; }
        public string cdp { get; set; }
    }

    public class groupPolicies : BusinessBase
    {
        public int groupPolicyId { get; set; }
        public string name { get; set; }

    }

    public class policyClient : BusinessBase
    {
        public string mac { get; set; }
        public string type { get; set; }
        public ssids ssids { get; set; }


    }

    public class ssids : BusinessBase
    {
        public List<policySsidClientDetail> number { get; set; }
    }

    public class policySsidClientDetail: BusinessBase
    {
        public string type { get; set; }
        public int group_number { get; set; }
        public string name { get; set; }
    }

    public class snapShot : BusinessBase
    {
        public string url { get; set; }
        public string expiry { get; set; }
    }

    public class splashSettings : BusinessBase
    {
        public int ssidNumber { get; set; }
        public string splashMethod { get; set; }
        public string splashUrl { get; set; }
        public bool useSplashUrl { get; set; }

    }

    public class ssid : BusinessBase
    {
        public int number { get; set; }
        public string name { get; set; }
        public bool enabled { get; set; }
        public string  splashPage { get; set; }
        public bool ssidAdminAccessible { get; set; }
        public string authMode { get; set; }

    }
}
