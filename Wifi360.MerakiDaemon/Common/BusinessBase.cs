﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wifi360.MerakiDaemon.Common
{
    [Serializable()]
    public class BusinessBase
    {
        private bool _isNew = true;
        private bool _isDeleted = false;
        private bool _isDirty = false;
        private Hashtable _rules = new Hashtable();

        public bool IsNew
        {
            get { return _isNew; }
            set { _isNew = value; }
        }

        public bool IsDeleted
        {
            get { return _isDeleted; }
        }

        public void MarkOld()
        {
            _isNew = false;

        }

        public void MakeClean()
        {
            _isDirty = false;

        }


        virtual public bool IsValid
        {
            get { return (_rules.Count == 0); }
        }

        virtual public bool IsDirty
        {
            get { return _isDirty; }

        }

        public Hashtable BrokenRules
        {
            get { return _rules; }
        }

        protected virtual void MarkNew()
        {
            _isNew = true;
            _isDeleted = false;
            MarkDirty();
        }

        protected void MarkDeleted()
        {
            _isDeleted = true;
            MarkDirty();
        }

        protected void MarkUndeleted()
        {
            _isDeleted = false;
            MarkDirty();
        }

        protected void MarkDirty()
        {
            _isDirty = true;
        }

        protected void MarkClean()
        {
            _isDirty = false;
        }

        protected void PropertyChange(string propertyName)
        {
            CheckRules(propertyName);
            MarkDirty();
        }

        protected void CheckRules()
        {
            CheckRules("");
        }

        protected virtual void CheckRules(string ruleName)
        {
            // Derived classes can override this method to check rules
        }

        public void CheckRule(bool condition, string name, string description)
        {
            if (condition)
            {
                if (!_rules.ContainsKey(name))
                    _rules.Add(name, description);
            }
            else
            {
                if (_rules.ContainsKey(name))
                    _rules.Remove(name);
            }
        }

        public void Save()
        {
            // Don't save the object if !IsValid
            if (!IsValid)
                throw new Exception("Object is not valid and can not be saved");

            // Save the object if IsDirty
            if (IsDirty)
                this.Business_Update();

        }

        public void Save(SqlTransaction transaction)
        {
            // Don't save the object if !IsValid
            if (!IsValid)
                throw new Exception("Object is not valid and can not be saved");

            // Save the object if IsDirty
            if (IsDirty)
                this.Business_Update(transaction);

        }

        public virtual void Delete()
        {
            MarkDeleted();
            CheckRules();
        }

        public virtual void Undelete()
        {
            MarkUndeleted();
            CheckRules();
        }

        protected virtual void Business_Create(object criteria)
        {
            throw new Exception("Invalid operation - create not allowed");
        }

        protected virtual void Business_Update()
        {
            throw new NotSupportedException("Invalid operation - update not allowed");
        }

        protected virtual void Business_Update(SqlTransaction transaction)
        {
            throw new NotSupportedException("Invalid operation - update not allowed");
        }

        protected virtual void Business_Fetch(object criteria)
        {
            throw new NotSupportedException("Invalid operation - fetch not allowed");
        }

        protected virtual void Business_Fetch(object criteria, SqlTransaction transaction)
        {
            throw new NotSupportedException("Invalid operation - fetch not allowed");
        }

        protected virtual void Business_Delete(object criteria)
        {
            throw new NotSupportedException("Invalid operation - fetch not allowed");
        }

        protected virtual void Business_Delete(object criteria, SqlTransaction transaction)
        {
            throw new NotSupportedException("Invalid operation - fetch not allowed");
        }
    }
}
