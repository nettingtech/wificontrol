﻿<%@ Page Title="" Language="C#" MasterPageFile="~/resources/master/MasterSecurityPage.Master" AutoEventWireup="true" CodeBehind="logout.aspx.cs" Inherits="PortalWIFI4EU_Meraki.logout" %>
    <asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server">
    <script>
        $(document).ready(function (e) {
            $("#logo").attr("src", "/resources/images/" + $("#idhotel").val() + "/" + $("#idlocation").val() + "-logo.png");

        });
    </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div class="container">
        <div class="row" id="row_wifi4eu">
            <div class="col-sm-12">
                <img id="wifi4eulogo">
            </div>
        </div>
        <div class="row" id="principal" style="margin-top: 10px;">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                         <p class="text-center"> <img src="#" id="logo" style="width:150px;" /></p>
                        <div class="alert alert-success text-center">
                          <p><b>Usted está desconectado</b></p>
                            <p>Por favor, espere unos minutos antes de conectarse de nuevo.</p>
                          <p>Gracias</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
