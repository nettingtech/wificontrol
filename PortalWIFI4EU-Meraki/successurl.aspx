﻿<%@ Page Title="" Language="C#" MasterPageFile="~/resources/master/MasterSecurityPage.Master" AutoEventWireup="true" CodeBehind="successurl.aspx.cs" Inherits="PortalWIFI4EU_Meraki.successurl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>        

        $(document).ready(function (e) {
            $("#logohead").html('<img id="logo" style="width:150px;" src="/resources/images/' + <%=nt_st%> + '/' + <%=nt_lt%> + '-logo.png" />');

        });
       
    </script>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1">
    <div class="container">
        <div class="row" id="row_wifi4eu">
            <div class="col-sm-12">
                <img id="wifi4eulogo">
            </div>
        </div>
        <div class="row" id="principal" style="margin-top: 10px;">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="text-center" id="logohead"> </div>
                        <div class="alert alert-success text-center">
                            <label id="labelusername">Código de Acceso: <span style='color:#004085;'><%= username %> </span></label><br />
                            <label id="labelvalidtill">Vádido hasta: <span style='color:#004085;'><%= validtill %> </span></label><br />
                            <label id="labeltimecredit">Crédito de tiempo: <span style='color:#004085;'><%= timecredit %> </span></label><br />
                            <label id="labelvolumedown">Crédito de volumen: <span style='color:#004085;'><%= volumecredit %> </span></label><br />
                            <label id="labelaccesstype">Tipo de acceso: <span style='color:#004085;'><%= accesstype %> </span></label><br />
                        </div>

                        <asp:Button runat="server" ID="logout_asp_button" CssClass="btn btn-danger btn-block" Text="Desconectar" OnClick="logout_asp_button_Click" /><br />
                        <asp:Button runat="server" ID="change_asp_button" CssClass="btn btn-warning btn-block" Text="Usar otro código de acceso" OnClick="change_asp_button_Click" /><br />
                        <a href="<%= continueurl %>" id="buttonContinue" target="_blank" class="btn btn-default btn-block">Continuar a internet</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function (e) {

            $('#footer').delay(2000).css('top', '1180px');// again 100 is the height of your footer
            $('#footer').css('display', 'block');
        });
    </script>
</asp:Content>
