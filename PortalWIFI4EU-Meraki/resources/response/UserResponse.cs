﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PortalWIFI4EU_Meraki.resources.response
{
    public class UserResponse
    {
        public int IdUser { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ValidTill { get; set; }
        public string Url { get; set; }
        public int allow { get; set; }
        public string VolumeUp { get; set; }
        public string VolumeDown { get; set; }
        public string TimeCredit { get; set; }
        public string AccessType { get; set; }
    }
}