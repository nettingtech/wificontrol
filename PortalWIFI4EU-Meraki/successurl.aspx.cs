﻿using PortalWIFI4EU_Meraki.resources.master;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Wifi360.Data.Class;
using Wifi360.Data.Controllers;
using Wifi360.Data.Model;

namespace PortalWIFI4EU_Meraki
{
    public partial class successurl : System.Web.UI.Page
    {
        protected string username = string.Empty;
        protected string validtill = string.Empty;
        protected string timecredit = string.Empty;
        protected string volumecredit = string.Empty;
        protected string accesstype = string.Empty;
        protected string continueurl = string.Empty;
        protected string logout_url = string.Empty;
        protected int nt_st = 0;
        protected int nt_lt = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var isLog = Boolean.Parse(ConfigurationManager.AppSettings["isLog"].ToString());
                var idproceso = Guid.NewGuid();

                HttpCookie meraki = Context.Request.Cookies["meraki"];
                HttpCookie merakiUser = Context.Request.Cookies["merakiuser"];

                if (Context.Request.Params["logout_url"] != null)
                    logout_url = Context.Request.Params["logout_url"].ToString();

                if (Context.Request.Params["username"] != null)
                    username = Context.Request.Params["username"].ToString();


                if (Context.Request.Params["continue_url"] != null)
                    continueurl = Context.Request.Params["continue_url"].ToString();

                MerakiLogoutUrl obj = new MerakiLogoutUrl();
                obj.Idhotel = int.Parse(((MasterSecurityPage)Master).idhotel);
                nt_st = obj.Idhotel;
                obj.Idlocation = int.Parse(((MasterSecurityPage)Master).idlocation);
                nt_lt = obj.Idlocation;
                obj.MAC = ((MasterSecurityPage)Master).mac;
                obj.Url = logout_url;
                obj.DateInserted = DateTime.Now.ToUniversalTime();
                obj.IP = Context.Request.UserHostAddress;
                obj.UserName = username;

             //   logout_url += "&continue_url=" + Context.Request.Url.Scheme + "://" + Context.Request.Url.Host + "/logout.aspx";

                BillingController.SaveMerakiLogoutUrl(obj);

                SitesGMT siteGMT = GetSiteGMt(obj.Idlocation, obj.Idhotel);

                Users user = BillingController.GetRadiusUser(obj.UserName, obj.Idhotel);
                if (user.IdUser != 0)
                {


                    bool little_joke = false;
                    var little_joke_info = "Intento fallido: ";
                    string Real_session_nas_mac = string.Empty;
                    //Tenemos mac obtenemos activesessions con GetActiveSessionMAC
                    ActiveSessions Real_session = new ActiveSessions();
                    //Real_session = BillingController.GetActiveSessionMAC(((MerakiSecureMaster)Master).mac);
                    Real_session = BillingController.GetActiveSession(((MasterSecurityPage)Master).mac, nt_st);
                    //con activesession obtenemos nasMAc
                    if (Real_session.AcctSessionID != null)
                    {
                        Real_session_nas_mac = Real_session.NAS_MAC;     // E0-CB-BC-35-D5-67:ON_SPOT  (idhotel 234) o tb hotspot_cunef_2019
                        Real_session_nas_mac.Split(':');
                        //con nasMAc location y site y comparamos con los encryptados (ya están en OBJ)

                        if (!(Real_session_nas_mac[0].Equals(((MasterSecurityPage)Master).ap_mac)) || !(Real_session_nas_mac[1].Equals(((MasterSecurityPage)Master).ap_ssid)))
                        {
                            little_joke = true;
                            little_joke_info += "NAS_MAC ";
                        }
                        //if (!Real_session.IdHotel.Equals(nt_st))
                        //{
                        //    little_joke = true;
                        //    little_joke_info += "SITE ";
                        //}
                        BillingController.GetLocation(nt_lt);
                        if (!Real_session.IdHotel.Equals(nt_st))
                        {
                            little_joke = true;
                            little_joke_info += " LOCATION.";
                        }
                    }

                    if (little_joke)
                    {
                        //POCA BROMA SARDINAS CUCA!
                        CaptivePortalLogAttack new_attack = new CaptivePortalLogAttack();
                        new_attack.idLog = 0;
                        new_attack.device_mac = obj.MAC;
                        new_attack.ip = obj.IP;
                        new_attack.info = little_joke_info;
                        new_attack.info += "ActiveSession ID: " + Real_session.IdActiveSession;
                        new_attack.nas_mac_correcta = Real_session_nas_mac[0].ToString() + ":" + Real_session_nas_mac[0].ToString();
                        new_attack.username = username;
                        new_attack.nas_mac_origen = ((MasterSecurityPage)Master).ap_mac + ":" + ((MasterSecurityPage)Master).ap_ssid;
                        new_attack.portal = "MERAKI WIFI4EU";
                        new_attack.date = DateTime.Now.ToUniversalTime();

                        BillingController.SaveCaptivePortalLogAttack(new_attack);

                        //redireccionamos a LOGOUT_URL (RADIUOS COA). Lo dejamos comentado porque para detecta atque mi MAC con otro usuario en otra RED

                        //   logout_url += "&continue_url=" + Context.Request.Url.Scheme + "://" + Context.Request.Url.Host + "/logout.aspx";
                        Response.Redirect(logout_url, false);
                        Response.End();
                    }

                    BillingTypes2 billingtype = BillingController.ObtainBillingType2(user.IdBillingType);
                    username = user.Name;

                    DateTime validTill = DateTime.MinValue;
                    List<ClosedSessions2> closedSessions = BillingController.GetClosedSession(obj.Idhotel, username, user.ValidSince, user.ValidTill);
                    if (closedSessions.Count > 0)
                    {
                        ClosedSessions2 closed = (from d in closedSessions orderby d.SessionTerminated descending select d).FirstOrDefault();
                        if (closed != null)
                            validTill = closed.SessionTerminated.Value.AddHours(billingtype.ValidAfterFirstUse);
                    }
                    else
                    {
                        ActiveSessions active = BillingController.GetActiveSession(obj.MAC, obj.Idhotel);
                        if (!active.IdActiveSession.Equals(0))
                            validTill = active.SessionStarted.Value.AddHours(billingtype.ValidAfterFirstUse);
                        else
                            validTill = DateTime.Now.ToUniversalTime().AddHours(billingtype.ValidAfterFirstUse);
                    }

                    if (validTill > user.ValidTill)
                        validtill = user.ValidTill.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                    else
                        validtill = validTill.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");

                    TimeSpan t = TimeSpan.FromSeconds(user.TimeCredit / user.MaxDevices);
                    string time = string.Empty;
                    if (user.TimeCredit >= 86400)
                        time = string.Format("{0} d: {1:D2} h: {2:D2} m: {3:D2} s", t.Days, t.Hours, t.Minutes, t.Seconds);
                    else
                        time = string.Format("{0:D2} h: {1:D2} m: {2:D2} s", t.Hours, t.Minutes, t.Seconds);

                    timecredit = time;
                    volumecredit = string.Format("{0:0.00} MB", Math.Round(double.Parse((user.VolumeDown / 1048576).ToString()), 2));
                    accesstype = billingtype.Description;

                    //continueurl = ((MasterSecurityPage)Master).urlDest;
                }
            }
            catch (Exception ex)
            {
                CaptivePortalLogInternal internallog = new CaptivePortalLogInternal();
                try
                {
                    internallog.IdHotel = int.Parse(((MasterSecurityPage)Master).idhotel);
                    internallog.IdLocation = int.Parse(((MasterSecurityPage)Master).idlocation);
                    internallog.CallerID = ((MasterSecurityPage)Master).mac;
                    internallog.NomadixResponse = ((MasterSecurityPage)Master).urlDest;
                    if (ex.StackTrace == null)
                    {
                        internallog.RadiusResponse = ex.InnerException.Message + " trace: " + ex.StackTrace;
                    }
                    internallog.UserName = ((MasterSecurityPage)Master).username;
                    internallog.Page = "Success.aspx";
                    internallog.NSEId = string.Empty;
                    internallog.Password = string.Empty;
                    BillingController.SaveCaptivePortalLogInternal(internallog);
                }
                catch {; }
            }
        }

        private static SitesGMT GetSiteGMt(int idlocation, int idhotel)
        {
            SitesGMT siteGMT = null;
            Locations2 location = null;
            Hotels hotel = null;
            try
            {
                if (idlocation != 0)
                {
                    location = BillingController.GetLocation(idlocation);
                    hotel = BillingController.GetHotel(location.IdHotel);
                    siteGMT = BillingController.SiteGMTSitebyLocation(location.IdLocation);
                    if (siteGMT.Id.Equals(0))
                        siteGMT = BillingController.SiteGMTSitebySite(location.IdHotel);
                }
                else
                {
                    siteGMT = BillingController.SiteGMTSitebySite(idhotel);
                }

                return siteGMT;
            }
            catch (Exception ex)
            {
                return null;
            }


        }

        protected void logout_asp_button_Click(object sender, EventArgs e)
        {
            try
            {
                logout_url = Context.Request.Params["logout_url"].ToString();
                logout_url += "&continue_url=" + Context.Request.Url.Scheme + "://" + Context.Request.Url.Host + "/logout.aspx";

                Response.Redirect(logout_url, false);
            }
            catch (Exception ex)
            {
                ;
            }

        }

        protected void change_asp_button_Click(object sender, EventArgs e)
        {
            try
            {
                string nt_token = Session["nt_crc"].ToString();
                List<ParametrosConfiguracion> parametros = BillingController.GetParametrosConfiguracion(0);
                ParametrosConfiguracion key = (from t in parametros where t.Key.ToUpper().Equals("ENCRYPTIONKEY") select t).FirstOrDefault();

                string[] nt_token_decrypt = Misc.Decrypt(nt_token, key.value).Split('|');

                string[] nt_token_decrypt_A = nt_token_decrypt[0].Split('=');
                string[] nt_token_decrypt_B = nt_token_decrypt[1].Split('=');
                string[] nt_token_decrypt_C = nt_token_decrypt[2].Split('=');
              
                int idhotel = int.Parse(nt_token_decrypt_A[1]);
                int idlocation = int.Parse(nt_token_decrypt_B[1]);
                string mac = nt_token_decrypt_C[1].Replace(':','-');

                BillingController.DeleteRememberMeTableUser(mac, idhotel);

                logout_url = Context.Request.Params["logout_url"].ToString();
                logout_url += "&continue_url=" + Context.Request.Url.Scheme + "://" + Context.Request.Url.Host + "/logout.aspx";
                Response.Redirect(logout_url, false);
            }
            catch (Exception ex)
            {
                ;
            }
        }
    }
}