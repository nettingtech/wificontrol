﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Wifi360.Data.Class;
using Wifi360.Data.Controllers;
using Wifi360.Data.Model;

namespace PortalWIFI4EU_Meraki
{
    public partial class landing : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            ParametrosConfiguracion Parametro_UrlSite = BillingController.GetParametroConfiguracion(HttpContext.Current.Request.Url.Host);
            if (Parametro_UrlSite.IdParameter != 0)
            {

                List<ParametrosConfiguracion> parametros = BillingController.GetParametrosConfiguracion(0);
                ParametrosConfiguracion key = (from t in parametros where t.Key.ToUpper().Equals("ENCRYPTIONKEY") select t).FirstOrDefault();
                try
                {
                    Locations2 location = BillingController.ObtainLocationDefault(Parametro_UrlSite.IdSite);

                    var ap_mac = Request.Params["ap_mac"];
                    var ap_ssid = Request.Params["ap_name"];
                    var macs = Request.Params["client_mac"];
                    var origin = Request.Params["login_url"].Split('&')[0];
                    var urlDest = Request.Params["continue_url"];
                    var error = Request.Params["error_message"];
                    var nt_crc_plaint = "nt_st=" + location.IdHotel +
                        "|nt_lt=" + location.IdLocation +
                        "|nt_client_mac=" + macs + 
                        "|nt_origin=" + origin + 
                        "|nt_urlDest=" + urlDest + 
                        "|nt_error=" + error + 
                        "|nt_ap_mac=" + ap_mac + 
                        "|nt_app_ssid=" + ap_ssid + 
                        "|epoch=" + DateTime.Now.ToUniversalTime();

                    var nt_crc = Misc.Encrypt(nt_crc_plaint, key.value);

                    Session["nt_crc"] = nt_crc;
                    Response.Redirect("~/welcome.aspx", false);

                }
                catch
                {
                    Response.Redirect("error.aspx");
                }
            }
            else
            {
                Response.Redirect("error.aspx");
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //83% of people not see this
        }
    }
}