﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Wifi360.Data.Controllers;
using Wifi360.Data.Model;

namespace ApiNetting
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código, en svc y en el archivo de configuración.
    // NOTE: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Service1.svc o Service1.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class ApiNetting : IApiNetting
    {
        public List<BillingTypesApi> GetBillingTypes(string token, int? location = 0)
        {
            List<BillingTypesApi> response = new List<BillingTypesApi>();
            ApiTokens ApiToken = BillingController.SelectAccessWithToken(token);
            if (!ApiToken.Id.Equals(0))
            {
                if (location.Value != 0)
                {
                    List<BillingTypes> billingTypes = BillingController.ObtainBillingTypesLocation(location.Value);
                }
                else
                {
                    //comprobamos si el usuario es de group, site o location
                    ;
                } 
                    
            }


            return response;
        }

        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }
    }
}
