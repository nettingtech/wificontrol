﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace ApiNetting
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IService1" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IApiNetting
    {

        [OperationContract]
        string GetData(int value);

        [OperationContract]
        CompositeType GetDataUsingDataContract(CompositeType composite);

        [OperationContract]
        List<BillingTypesApi> GetBillingTypes(string token, int? location=0); 

        // TODO: agregue aquí sus operaciones de servicio
    }

    [DataContract]
    public class BillingTypesApi
    {
        [DataMember]
        public int IdBillingType { get; set; }

        [DataMember]
        public int IdHotel{ get; set; }

        [DataMember]
        public string Description{ get; set; }

        [DataMember]
        public float Price { get; set; }

        [DataMember]
        public int TimeCredit { get; set; }

        [DataMember]
        public int MaxDevices { get; set; }

        [DataMember]
        public int ValidTill { get; set; }

        [DataMember]
        public long BWDown { get; set; }

        [DataMember]
        public long BWUp { get; set; }

        [DataMember]
        public int IdPriority { get; set; }

        [DataMember]
        public bool FreeAccess { get; set; }

        [DataMember]
        public bool Visible { get; set; }

        [DataMember]
        public int Order { get; set; }

        [DataMember]
        public bool Enabled { get; set; }

        [DataMember]
        public long VolumeUp { get; set; }

        [DataMember]
        public long VolumeDown { get; set; }

        [DataMember]
        public int IdLocation { get; set; }

        [DataMember]
        public string PayPalDescription { get; set; }

        [DataMember]
        public int IdBillingModule { get; set; }

        [DataMember]
        public bool DefaultFastTicket { get; set; }

        [DataMember]
        public string UrlLanding { get; set; }

        [DataMember]
        public int FilterId { get; set; }

        [DataMember]
        public bool IoT { get; set; }

        [DataMember]
        public int NextBillingType { get; set; }


    }
    // Utilice un contrato de datos, como se ilustra en el ejemplo siguiente, para agregar tipos compuestos a las operaciones de servicio.
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }
}
