﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Site.Master" AutoEventWireup="true" CodeBehind="ManageGroupDetails.aspx.cs" Inherits="Wifi360.ManageGroupDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

        <script src="js/plugins/chosen/chosen.jquery.js"></script>
        <script type="text/javascript">
           
            $(document).ready(function () {

                var parameters = getParameters();

                function getParameters() {
                    var searchString = window.location.search.substring(1)
                      , params = searchString.split("&")
                      , hash = {}
                    ;

                    for (var i = 0; i < params.length; i++) {
                        var val = params[i].split("=");
                        hash[unescape(val[0])] = unescape(val[1]);
                    }
                    return hash;
                }

                //FUnción de Precarga de elementos Select
                function bindSelect(id, valueMember, displayMember, source) {
                    $("#" + id).find('option').remove().end();
                    var cookie = readCookie("wifi360-language");
                    switch (cookie) {
                            case "es": $("#" + id).append('<option value="0">Seleccionar</option>'); break;
                            default: $("#" + id).append('<option value="0">Select</option>'); break;
                        }
                    $.each(source, function (index, item) {
                        $("#" + id).append('<option value="' + item[valueMember] + '"> ' + item[displayMember] + '</option>');
                    });
                };

                //PRECARGA DE CURRENCIES
                var data_currencies = 'action=Currencies';
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: data_currencies,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    //beforeSend: function () {
                    //    $("#myModalWait").modal('show');  //Barra de progreso Bootsrap
                    //},
                    //complete: function () {
                    //    $("#myModalWait").modal('hide');
                    //},
                    success: function (pReturn_currencies) {
                        var lReturn_currencies = JSON.parse(pReturn_currencies);
                        bindSelect('currencies', 'Code', 'Badge', lReturn_currencies.list);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#searchResult").html(xhr.statusText)
                    }
                });

                //Precarga de los SELECT & FORM
                var dataString = 'action=SELECT_ALL_GROUPS_SITE_AND_LOCATIONS&id=' + parameters.id;
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    beforeSend: function () {
                        $("#myModalWait").modal('show');
                    },        
                    success: function (pReturn) {

                        var lReturn = JSON.parse(pReturn);

                        if (parameters.id == undefined) {                     
                            $.each(lReturn.Group_list, function (index, item) {
                                $("#multiselectGROUPS").append('<option value="' + item["idGroup"] + '"> ' + item["Name"] + '</option>');
                            });

                            $.each(lReturn.Site_list, function (index, item) {
                                $("#multiselectSITES").append('<option value="' + item["IdHotel"] + '"> ' + item["Name"] + '</option>');
                            });

                            $.each(lReturn.Location_list, function (index, item) {
                                $("#multiselectLOCATIONS").append('<option value="' + item["Idlocation"] + '">' + item["Description"] + '</option>');
                            });
                        } else {

                            $("id").val(lReturn.id);
                            $("#dashboard_name").val(lReturn.Name);                            
                            $("#currencies").val((lReturn.Currencie).trim());
                            $("#latitude").val(lReturn.Latitude);
                            $("#longitude").val(lReturn.Longitude);
                            if (lReturn.Realmshare == true)
                                $("#realmshare").attr('checked', true);
                            if (lReturn.Family == true)
                                $("#family").attr('checked', true);

                            $.each(lReturn.Group_list, function (index, item) {
                                if (lReturn.Group_list_selected.find(x=> x.idGroup == item["idGroup"])){
                                    $("#multiselectGROUPS").append('<option selected value="' + item["idGroup"] + '"> ' + item["Name"] + '</option>');
                                }else{
                                    $("#multiselectGROUPS").append('<option value="' + item["idGroup"] + '"> ' + item["Name"] + '</option>');
                                }                                    
                            });
                            $.each(lReturn.Site_list, function (index, item) {
                                if (lReturn.Site_list_selected.find(x=> x.IdHotel == item["IdHotel"])) {
                                    $("#multiselectSITES").append('<option selected value="' + item["IdHotel"] + '"> ' + item["Name"] + '</option>');
                                }else{
                                    $("#multiselectSITES").append('<option value="' + item["IdHotel"] + '"> ' + item["Name"] + '</option>');
                                }
                            });

                            $.each(lReturn.Location_list, function (index, item) {
                                if (lReturn.Location_list_selected.find(x=> x.Idlocation == item["Idlocation"])) {
                                    $("#multiselectLOCATIONS").append('<option selected value="' + item["Idlocation"] + '">' + item["Description"] + '</option>');
                                }else{
                                    $("#multiselectLOCATIONS").append('<option value="' + item["Idlocation"] + '">' + item["Description"] + '</option>');
                                }
                            });
                        }
                        $("#multiselectGROUPS").trigger("chosen:updated");
                        $("#multiselectSITES").trigger("chosen:updated");
                        $("#multiselectLOCATIONS").trigger("chosen:updated");
                    },
                    complete: function () {
                        $("#myModalWait").modal('hide');
                    }
                });

                function Save_dashboard_group() {

                    //var parameters = getParameters();
                    var id = 0;
                    var data = {};
                    //tipo de acción
                    data["action"] = 'SAVE_DASHBOARD_GROUPS';
                    data["Name"] = $("#dashboard_name").val();
                    data["Currencie"] = $("#currencies").val();
                    data["latitude"] = $("#latitude").val();
                    data["longitude"] = $("#longitude").val();
                    data["realmshare"] = $("#realmshare").is(':checked');
                    data["family"] = $("#family").is(':checked');
                    
                    data["id"] = parameters.id;

                    if (!$.isNumeric(parameters.id))
                        data["id"] = "undefined";

                    //Las varables multiselect vienen como un array JSON.stringify deserializa la variable
                    var t_GROUPS = JSON.stringify($("#multiselectGROUPS").val());
                    var t_SITES = JSON.stringify($("#multiselectSITES").val());
                    var t_LOCATIONS = JSON.stringify($("#multiselectLOCATIONS").val());

                    //Quitamos los caracteres "sobrantes" con expresión regular                   
                    data["GROUPS"] = t_GROUPS.replace(/\"/g, ' ').replace('[', '').replace(']', '').replace(/\s/g, '');
                    data["SITES"] = t_SITES.replace(/\"/g, ' ').replace('[', '').replace(']', '').replace(/\s/g, '');
                    data["LOCATIONS"] = t_LOCATIONS.replace(/\"/g, ' ').replace('[', '').replace(']', '').replace(/\s/g, '');

                    //Llamada AJAX al HANDLER para el SAVE.
                    $.ajax({
                        url: "handlers/SMIHandler.ashx",
                        type: "POST",
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "text",
                        beforeSend: function () {
                            $("#myModalWait").modal('show');
                        },
                        complete: function () {
                            $("#myModalWait").modal('hide');
                        },
                        success: function (msg) {
                            if (msg.code == "OK") {
                                $("#message").removeClass().addClass("alert alert-success");
                                $("#message").html(GetMessage(msg.message));
                            }
                            else {
                                $("#message").removeClass().addClass("alert alert-danger");
                                $("#message").html(GetMessage(msg.message));
                            }
                            $("#myModalMessage").modal('show');

                            var lReturn = JSON.parse(msg);
                            if (lReturn.IdGroup != undefined) {
                                parameters.id = lReturn.IdGroup;
                            }

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#loading").html("We have a problem, please try again later.");
                            alert('error: ' + xhr.statusText);
                        }
                    });
                }

                $("#save_dashboard").click(function (e) {
                    e.preventDefault();
                    Save_dashboard_group();
                });

                $(function () {
                    $('.chosen-select').chosen();
                    $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
                });

                $('.chosen-select').chosen({ width: "100%" });
            });

            $("#back").on("click", function (e) {
                e.preventDefault();
                window.location = "ManageGroups.aspx";
            });
                        
        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2 id="headerpagemanageusers">Management Group Details</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="default.aspx">Dashboard</a>
                    </li>
                    <li id="breadpagefathersuperadmin">
                        <a>Super Admin</a>
                    </li>
                    <li class="active">
                        <strong id="breadlabelmanagesites">Management Group</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2"></div>
        </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-9">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5 id="labelboxheadermanagementusers"> Management Group Details</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="form-horizontal">
                            <div class="form-group"> <label class="control-label col-sm-3" id="labelnewuser_form_name">Name DashBoard Group</label>
                            <div class="col-sm-9">
                                <input type="text"  id="dashboard_name" class="form-control" />
                            </div>
                        </div>                           
                            <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformpaypalcurrency">Currency code</label>
                            <div class="col-sm-9"><select id="currencies" class="form-control"></select></div>
                        </div>
                            <div class="form-group"> <label class="control-label col-sm-3" id="labellocationsettingsformlatitude" >Latitude</label>
                            <div class="col-sm-9">
                                <input type="text"  id="latitude" class="form-control" />
                            </div>
                        </div>
                            <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformlongitude">Longitude</label>
                            <div class="col-sm-9">
                                <input type="text"  id="longitude" class="form-control" />
                            </div>
                        </div>
                            <div class="form-group"> <label class="control-label col-sm-3" id="form_Family">Family</label>
                            <div class="col-sm-2"><div class="onoffswitch">
                                <input type="checkbox" class="onoffswitch-checkbox" id="family"/>
                                <label class="onoffswitch-label" for="family">
                                    <span class="onoffswitch-inner"></span>
                                    <span class="onoffswitch-switch"></span>
                                </label>
                            </div></div>
                        </div>
                            <div class="form-group"> <label class="control-label col-sm-3" id="form_RealmShare">Realm Share</label>
                            <div class="col-sm-2"><div class="onoffswitch">
                                <input type="checkbox" class="onoffswitch-checkbox" id="realmshare"/>
                                <label class="onoffswitch-label" for="realmshare">
                                    <span class="onoffswitch-inner"></span>
                                    <span class="onoffswitch-switch"></span>
                                </label>
                            </div></div>
                        </div>                                             
                            <div class="form-group"><label class="control-label col-sm-3" id="labelnewgroups_form_name">Select Groups</label>
                            <div class="col-sm-9"><select data-placeholder="Groups..." class="chosen-select" multiple="multiple" style="display: none;" tabindex="-1" id="multiselectGROUPS"></select></div>         
                        </div> 
                            <div class="form-group"><label class="control-label col-sm-3" id="labelnewsites_form_name">Select Sites</label>
                            <div class="col-sm-9"><select data-placeholder="Sites..." class="chosen-select" multiple="multiple" style="display: none;" tabindex="-1" id="multiselectSITES"></select></div>         
                        </div>
                            <div class="form-group"><label class="control-label col-sm-3" id="labelnewlocations_form_name">Select Locations</label>
                            <div class="col-sm-9"><select data-placeholder="Locations..." class="chosen-select" multiple="multiple" style="display: none;" tabindex="-1" id="multiselectLOCATIONS"></select></div>         
                        </div>                       
                        </div>
                    </div>
                </div>
            </div>

            <!-- COLUMNA DCHA -->
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5 id="labelboxheaderbuttonsettings">Actions</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <p>
                        <a href="#" class="btn btn-w-m btn-lg btn-primary btn-block" id="save_dashboard"><i class="fa fa-save"></i> <span id="label_createG_form_buttonclone"> Save Group</span></a>
                        <a href="ManageGroups.aspx" class="btn btn-w-m btn-lg btn-default btn-block" id="back"><i class="fa fa-backward"></i> <span id="label_createG_form_buttonback">Back </span></a>
                    </p>
                </div>
            </div>
        </div>
        </div>
    </div>
            

        <div class="modal inmodal fade" id="ModalMessage" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header"></div>
                    <div class="modal-body">
                        <div id="message"></div>
                    </div>
                   <div class="modal-footer">
                       <a href="#" class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Volver</a>
                   </div>
                </div>
            </div>
        </div>


</asp:Content>
