﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Site.Master" AutoEventWireup="true" CodeBehind="locationtext.aspx.cs" Inherits="Wifi360.locationtext" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="js/redactor.min.js"></script>
    <link href="css/redactor.css" rel="stylesheet" />
    <script>
        function getParameters() {
            var searchString = window.location.search.substring(1)
              , params = searchString.split("&")
              , hash = {}
            ;

            for (var i = 0; i < params.length; i++) {
                var val = params[i].split("=");
                hash[unescape(val[0])] = unescape(val[1]);
            }
            return hash;
        }
        var parameters = getParameters();

        $(function () {
            $('#redactor').redactor();
        });

        var returnidLocation = 0;

        $(document).ready(function () {           
            var dataString = 'action=LOCATIONTEXT&id=' + parameters.id;
            $.ajax({
                url: "handlers/SMIHandler.ashx",
                data: dataString,
                contentType: "application/json; charset=utf-8",
                dataType: "text",
                beforeSend: function () {
                    $("#myModalWait").modal('show');
                },
                complete: function () {
                    $("#myModalWait").modal('hide');
                },
                success: function (pReturn) {
                    var lReturn = JSON.parse(pReturn);
                    $("#language").val(lReturn.language);
                    $("#redactor").redactor('set', lReturn.text);

                    returnidLocation = lReturn.idlocation;

                    if (parameters.idsite != undefined)
                        $("#back").attr("href", "/location.aspx?id=" + lReturn.idlocation + "&idsite=" + parameters.idsite + "&return=" + parameters.return);
                    else
                        $("#back").attr("href","/location.aspx?id=" + parameters.id); 

                    $("#localizacionpadre").attr("href", "/location.aspx?id=" + lReturn.idlocation);

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#modal-loading").hide();
                    $("#searchResult").html(xhr.statusText)
                }
            });

            $("#back").click(function (e) {
                e.preventDefault();
                if (parameters.idsite != undefined)
                    window.location.href = "/location.aspx?id=" + returnidLocation + "&idsite=" + parameters.idsite + "&return=" + parameters.return;
                else
                    window.location.href = "/location.aspx?id=" + returnidLocation;
            });

            $("#save").click(function (e) {
                var text = $("#redactor").redactor('get');
                var parameters = getParameters();
                var data = {};
                data["action"] = 'LOCATIONTEXTSAVE';
                data["id"] = parameters.id;
                data["text"] = text;
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    type: "POST",
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $("#myModalWait").modal('show');
                    },
                    complete: function () {
                        $("#myModalWait").modal('hide');
                    },
                    success: function (msg) {
                        if (msg.code == "OK") {
                            $("#message").removeClass().addClass("alert alert-success");
                            $("#message").html(GetMessage(msg.message));
                        }
                        else {
                            $("#message").removeClass().addClass("alert alert-danger");
                            $("#message").html(GetMessage(msg.message));
                        }
                        $("#myModalMessage").modal('show');
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#loading").html("We have a problem, please try again later.");
                        alert('error: ' + xhr.statusText);
                    }
                });
            });

        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 id="headerpagelocationsetting">Settings</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="default.aspx">Dashboard</a>
                </li>
                <li>
                    <a id="breadpagefatherlocationsetting">Settings</a>
                </li>
                <li>
                    <a href="locationsettings.aspx" id="breadpagelocationsetting">Locations</a>
                </li>
                <li>
                    <a href="#" id="localizacionpadre">Location</a>
                </li>
                <li class="active">
                    <strong id="breadpagelocationsettingactive">location general text</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-9">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                    <h5 id="labelboxheaderlocationsettingtext">Location general text</h5>
                </div>
                    <div class="ibox-content">
                     <div class="form-horizontal">
                    <div class="form-group"> <label class="control-label col-sm-4" id="labellocationsettingstextformlanguage">Language</label><div class="col-sm-8"><input type="text" class="form-control" id="language" disabled="disabled" /></div></div>
                    <div class="form-group"> <label class="control-label col-sm-4" id="labellocationsettingstextformtext">Text</label><div class="col-sm-8">
                        <div class="redactor_box">
                            <textarea id="redactor" name="content" dir="ltr"></textarea>
                        </div>
                    </div></div>


                </div>
                </div>
            </div>
            </div>

            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5 id="labelboxheaderacitonlocationsettingtext">Accions over text</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <p>
                            <a href="#" class="btn btn-w-m btn-lg btn-primary btn-block" id="save"><i class="fa fa-save"></i> <span id="labelformlocationsettingsbuttonsave">Save</span></a>

                            <a href="#" role="button" class="btn btn-w-m btn-lg btn-default btn-block" id="back" data-toggle="modal"><i class="fa fa-backward"></i> <span id="labelformlocationsettingsbuttonback"> Back</span></a>

                        </p>
                    </div>
                </div>
            </div>
            </div>
            <div class="modal inmodal fade" id="myModalMessage" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                            </div>
                            <div class="modal-body">
                                <div id="message">
                                    
                                </div>
                            </div>
                          <div class="modal-footer">
                            <a href="#" class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Volver</a>
                          </div>
                        </div>
                    </div>
                </div>
        </div>
</asp:Content>
