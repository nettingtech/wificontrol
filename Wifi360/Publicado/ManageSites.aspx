﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Site.Master" AutoEventWireup="true" CodeBehind="ManageSites.aspx.cs" Inherits="Wifi360.ManageSites" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <script type="text/javascript">

            function readCookie(name) {
                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
                }
                return null;
            };
            
            $(document).ready(function () {
                //Default de variable PAGE
                var page = 0;
                var cookie = readCookie("wifi360-language");

                //Precarga de los SITES
                var dataString = 'action=SELECTALLSITES&page=' + page + '&name=' + $("#name").val();
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    beforeSend: function () {
                        $("#myModalWait").modal('show');
                    },
                    complete: function () {
                        $("#myModalWait").modal('hide');
                    },
                    success: function (pReturn) {
                        var lReturn = JSON.parse(pReturn);
                        var table = "";
                        switch (cookie) {
                            case "es": table = "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>ID</th><th class='hidden-xs'>Site</th><th>Editar</th><th>Clonar</th></tr>";
                                break;
                            default: table = "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>ID</th><th class='hidden-xs'>Site</th><th>Edit</th><th>Clone</th></tr>";
                                break;
                        }

                        $.each(lReturn.list, function (index, item) {
                                table += '<tr><td>' + item["IdHotel"] + '</td><td class="hidden-xs">' + item["Name"] + '</td>';
                                table += '<td><a href="/sitesettings.aspx?id=' + item["IdHotel"] + '"><i class="fa fa-pencil"></i></a></td>';
                                table += '<td><a href="#" rel="clone" attr="' + item["IdHotel"] + '"><i class="fa fa-copy"></i></a></td></tr>';
                        });
                                
                        if (lReturn.isAdmin) {
                            $("#new").show();
                        }

                        table += "</table>";

                        $("#searchResult").html(table);

                        var pagination = "<ul class='pagination'>";
                        var points1 = 0;
                        var points2 = 0;
                        x = parseInt(page);
                        for (var index = 0; index < lReturn.pageNumber; index++) {
                            if (lReturn.pageNumber <= 10) {
                                if (page == index)
                                    pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                else
                                    pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                            }
                            else {
                                if (page == index) {
                                    pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                                else if (index == 0) {
                                    pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                                else if (index == (lReturn.pageNumber - 1)) {
                                    pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                                else if (index < (x - 3)) {
                                    if (points1 == 0) {
                                        pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                        points1 = 1;
                                    }
                                }
                                else if (index > (x + 3)) {
                                    if (points2 == 0) {
                                        pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                        points2 = 1;
                                    }
                                }
                                else
                                    pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                            }
                        }

                        pagination += "</ul>";

                        $("#searchResult").append(pagination);

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#modal-loading").hide();
                        $("#searchResult").html(xhr.statusText)
                    }
                });
                   
                //Función de Búsqueda Filtrada
                $("#search").click(function (e) {
                    e.preventDefault();

                    var page = 0;
                    var isVolumeCombined = false;
                    var dataString2 = 'action=SELECTALLSITES&page=' + page + '&name=' + $("#management_name").val();
                    $.ajax({
                        url: "handlers/SMIHandler.ashx",
                        data: dataString2,
                        contentType: "application/json; charset=utf-8",
                        dataType: "text",
                        beforeSend: function () {
                            $("#myModalWait").modal('show');
                        },
                        complete: function () {
                            $("#myModalWait").modal('hide');
                        },
                        success: function (pReturn) {
                            var lReturn = JSON.parse(pReturn);
                            var table = "";
                            switch (cookie) {
                                case "es": table = "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>ID</th><th class='hidden-xs'>Site</th><th>Editar</th><th>Clonar</th></tr>";
                                    break;
                                default: table = "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>ID</th><th class='hidden-xs'>Site</th><th>Edit</th><th>Clone</th></tr>";
                                    break;
                            }
                            $.each(lReturn.list, function (index, item) {
                                table += '<tr><td>' + item["IdHotel"] + '</td><td class="hidden-xs">' + item["Name"] + '</td>';
                                table += '<td><a href="/sitesettings.aspx?id=' + item["IdHotel"] + '"><i class="fa fa-pencil"></i></a></td>';
                                table += '<td><a href="#" rel="clone" attr="' + item["IdHotel"] + '"><i class="fa fa-copy"></i></a></td></tr>';
                            });
                                
                            if (lReturn.isAdmin) {
                                $("#new").show();
                            }

                            table += "</table>";

                            $("#searchResult").html(table);

                            var pagination = "<ul class='pagination'>";
                            var points1 = 0;
                            var points2 = 0;
                            x = parseInt(page);
                            for (var index = 0; index < lReturn.pageNumber; index++) {
                                if (lReturn.pageNumber <= 10) {
                                    if (page == index)
                                        pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                    else
                                        pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                                else {
                                    if (page == index) {
                                        pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                    }
                                    else if (index == 0) {
                                        pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                    }
                                    else if (index == (lReturn.pageNumber - 1)) {
                                        pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                    }
                                    else if (index < (x - 3)) {
                                        if (points1 == 0) {
                                            pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                            points1 = 1;
                                        }
                                    }
                                    else if (index > (x + 3)) {
                                        if (points2 == 0) {
                                            pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                            points2 = 1;
                                        }
                                    }
                                    else
                                        pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                            }

                            pagination += "</ul>";

                            $("#searchResult").append(pagination);

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#modal-loading").hide();
                            $("#searchResult").html(xhr.statusText)
                        }
                    });
                });

                //función para hacer llamada al MODAL para la clonación
                $("#clone").click(function (e) {
                    e.preventDefault();

                    //Data Load
                    var data = {};
                    data["action"] = 'CLONESITE';
                    data["NameNewSite"] = $("#name").val();                    
                    data["idHoteltoClone"] = $("#clone").attr("idsite");

                    data["EnabledcopyBilling"] = $("#billingtypes").is(':checked');
                    data["EnabledcopyBillingCount"] = $("#billingCount").is(':checked');
                    data["EnabledcopyUsers"] = $("#users").is(':checked');
                    data["EnabledcopyLoyalties"] = $("#loyalties").is(':checked');
                    data["EnabledcopyClosedSessions"] = $("#Closedsession").is(':checked');
                    data["EnabledcopySessionsLog"] = $("#SessionsLog").is(':checked');
                    data["EnabledcopyLocationsIdentifier"] = $("#LocationsIdentifier").is(':checked');
                    data["EnabledcopyMerakiLocationsGPS"] = $("#MerakiLocationsGPS").is(':checked');

                    //Llamada AJAX para carga de datos.
                    $.ajax({
                        url: "handlers/SMIHandler.ashx",
                        type: "POST",
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "text",
                        beforeSend: function () {
                            $("#CloneSite_ModalMessage").modal('hide');
                            $("#myModalWait").modal('show');
                        },
                        complete: function () {
                            $("#myModalWait").modal('hide');
                        },
                        success: function (msg) {
                            if (msg.code == "OK") {
                                $("#message").removeClass().addClass("alert alert-success");
                                $("#message").html(GetMessage(msg.message));
                            }
                            else {
                                $("#message").removeClass().addClass("alert alert-danger");
                                $("#message").html(GetMessage(msg.message));
                            }
                            $("#myModalMessage").modal('show');
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#loading").html("We have a problem, please try again later.");
                            alert('error: ' + xhr.statusText);
                        }
                    });            
                    window.location.href = "ManageSites.aspx"; //reload la página.
                });               
            });

            $(document).on("click", 'a[rel^="page"]', function (e) {
                e.preventDefault();
                var cookie = readCookie("wifi360-language");

                var page = $(this).attr("attr");
                var dataString = 'action=SELECTALLSITES&page=' + page + '&name=' + $("#management_name").val();
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    beforeSend: function () {
                        $("#myModalWait").modal('show');
                    },
                    complete: function () {
                        $("#myModalWait").modal('hide');
                    },
                    success: function (pReturn) {
                        var lReturn = JSON.parse(pReturn);
                        var table = "";
                        switch (cookie) {
                            case "es": table = "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>ID</th><th class='hidden-xs'>Site</th><th>Editar</th><th>Clonar</th></tr>";
                                break;
                            default: table = "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>ID</th><th class='hidden-xs'>Site</th><th>Edit</th><th>Clone</th></tr>";
                                break;
                        }

                        $.each(lReturn.list, function (index, item) {                            
                            table += '<tr><td>' + item["IdHotel"] + '</td><td class="hidden-xs">' + item["Name"] + '</td>';
                            table += '<td><a href="/sitesettings.aspx?id=' + item["IdHotel"] + '"><i class="fa fa-pencil"></i></a></td>';
                            table += '<td><a href="#" rel="clone" attr="' + item["IdHotel"] + '"><i class="fa fa-copy"></i></a></td></tr>';
                        });

                        if (lReturn.isAdmin) {
                            $("#new").show();
                        }

                        table += "</table>";

                        $("#searchResult").html(table);

                        var pagination = "<ul class='pagination'>";
                        var points1 = 0;
                        var points2 = 0;
                        x = parseInt(page);
                        for (var index = 0; index < lReturn.pageNumber; index++) {
                            if (lReturn.pageNumber <= 10) {
                                if (page == index)
                                    pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                else
                                    pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                            }
                            else {
                                if (page == index) {
                                    pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                                else if (index == 0) {
                                    pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                                else if (index == (lReturn.pageNumber - 1)) {
                                    pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                                else if (index < (x - 3)) {
                                    if (points1 == 0) {
                                        pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                        points1 = 1;
                                    }
                                }
                                else if (index > (x + 3)) {
                                    if (points2 == 0) {
                                        pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                        points2 = 1;
                                    }
                                }
                                else
                                    pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                            }
                        }

                        pagination += "</ul>";

                        $("#searchResult").append(pagination);

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#modal-loading").hide();
                        $("#searchResult").html(xhr.statusText)
                    }
                });
            });
            
            $(document).on("click", 'a[rel^="clone"]', function (e) {
                e.preventDefault();
                var cookie = readCookie("wifi360-language");
                var id = $(this).attr("attr");

                $("#clone").attr("idsite", id);
                $("#CloneSite_ModalMessage").modal('show');

                //CONDICION PARA EL CLONADO
                $("#billingCount").prop('checked', false);
                $("#users").prop('checked', false);
                $("#billingtypes").prop('checked', false);
                $("#loyalties").prop('checked', false);
                $("#Closedsession").prop('checked', false);
                $("#LocationsIdentifier").prop('checked', false);
                $("#MerakiLocationsGPS").prop('checked', false);
                $("#SessionsLog").prop('checked', false);

                $("#billingCount_botton").hide();

                $("#billingtypes").on('change', function () {
                    if ($('#billingtypes').is(':checked') & $('#users').is(':checked')) {
                        $('#billingCount_botton').show();
                    } else {
                        $("#billingCount_botton").hide();
                        $('#billingCount').prop('checked', false);
                    }
                });

                $("#users").on('change', function () {
                    if ($('#billingtypes').is(':checked') & $('#users').is(':checked')) {
                        $('#billingCount_botton').show();
                    } else {
                        $("#billingCount_botton").hide();
                        $('#billingCount').prop('checked', false);
                    }
                });
            });           

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 id="headerpagemanagesites">Management Sites</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="default.aspx">Dashboard</a>
                </li>
                <li id="breadpagefathersuperadmin">
                    <a>Super Admin</a>
                </li>
                <li class="active">
                    <strong id="breadlabelmanagesites">Management Sites</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5 id="labelboxheadermanagement"> Management Sites</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <a href="NewSite.aspx" id="new" class="btn btn-primary"><i class="fa fa-plus"></i><span id="buttonnewsite"> New Site</span></a>
                        </div>
                        <div class="col-lg-12">
                        <div class="ibox border-bottom">
                        <div class="ibox-title">
                        <h5 id="headerboxfilterusersvalid">Search filters</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-down"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content" style="display: none;">
                        <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto;">
                            <div class="scroll_content" style="overflow: hidden; width: auto; ">
                                <div role="form">
                            <div class="form-group"><label for="username" id="labelform_management_description">Name</label>
                                <input type="text" id="management_name" class="form-control"/>
                            </div>

                            <div class="form-group">
                                <a href="#" id="search" class="btn btn-w-m btn-primary" ><i class="fa fa-search"></i><span id="labelformusermanagementbuttonsearch"> Search </span></a>
                            </div>

                        </div>
                        </div><div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 198.02px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                    </div>
                </div>
                        </div>
                        <div class="col-lg-12">
                            <div id="searchResult"></div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        
            <!-- Modal para la clonación de SITES -->
            <div class="modal inmodal fade" id="CloneSite_ModalMessage" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header"><h4 id="formCloneOption">Opciones de Clonado</h4></div>
                        <div class="modal-body">
                            <div style="display: grid;">                            
                            <!--NEW NAME-->
                            <div class="form-group"> <label class="control-label col-sm-5" id="labelnewsite_form_name"> New Name</label>
                                <div class="col-sm-6"><input type="text" class="form-control" id="name" /></div>
                            </div>
                            <!--CLONE BILLING TYPES BOTTON-->
                            <div class="form-group"> <label class="control-label col-sm-5" id="label_CloneBillingtypes">Clone Billing types</label>
                                <div class="col-sm-2">
                                    <div class="onoffswitch">
                                        <input type="checkbox" class="onoffswitch-checkbox" id="billingtypes"/>
                                        <label class="onoffswitch-label" for="billingtypes">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>                            
                            <!--CLONE USUERS BOTTON-->
                            <div class="form-group"> <label class="control-label col-sm-5" id="label_Clone_Users">Clone Users</label>
                                <div class="col-sm-2">
                                    <div class="onoffswitch">
                                        <input type="checkbox" class="onoffswitch-checkbox" id="users"/>
                                        <label class="onoffswitch-label" for="users">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!--CLONE BILLING BOTTON-->
                            <div class="form-group"> <label class="control-label col-sm-5" id="Label_Clone_Billings">Clone Billings</label>
                                <div class="col-sm-2">
                                    <div class="onoffswitch">
                                        <input type="checkbox" class="onoffswitch-checkbox" id="billingCount" />
                                        <label id="billingCount_botton" class="onoffswitch-label" for="billingCount">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!--CLONE LOYALTIES BOTTON-->
                            <div class="form-group"> <label class="control-label col-sm-5" id ="label_Clone_loyalties">Clone Loyalties</label>
                                <div class="col-sm-2">
                                    <div class="onoffswitch">
                                        <input type="checkbox" class="onoffswitch-checkbox" id="loyalties"/>
                                        <label class="onoffswitch-label" for="loyalties">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!--CLONE CLOSED SESSIONs BOTTON-->
                            <div class="form-group"> <label class="control-label col-sm-5" id="label_Clone_Closed_Sessions">Clone Closed Sessions</label>
                                <div class="col-sm-2">
                                    <div class="onoffswitch">
                                        <input type="checkbox" class="onoffswitch-checkbox" id="Closedsession"/>
                                        <label class="onoffswitch-label" for="Closedsession">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!--CLONE SESSIONS LOG BOTTON-->
                                <div class="form-group"> <label class="control-label col-sm-5" id="label_Clone_SessionsLog">Clone SessionsLog</label>
                                <div class="col-sm-2">
                                    <div class="onoffswitch">
                                        <input type="checkbox" class="onoffswitch-checkbox" id="SessionsLog"/>
                                        <label class="onoffswitch-label" for="SessionsLog">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!--CLONE FDSLocationsIdentifier BOTTON-->
                            <div class="form-group"> <label class="control-label col-sm-5" id="label_Clone_Locations_Identifier" >Clone Locations Identifier</label>
                                <div class="col-sm-2">
                                    <div class="onoffswitch">
                                        <input type="checkbox" class="onoffswitch-checkbox" id="LocationsIdentifier"/>
                                        <label class="onoffswitch-label" for="LocationsIdentifier">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!--CLONE MerakiLocationsGPS BOTTON-->
                            <div class="form-group"> <label class="control-label col-sm-5" id="label_Clone_Meraki_LocationsGPS">Clone Meraki Locations GPS</label>
                                <div class="col-sm-2">
                                    <div class="onoffswitch">
                                        <input type="checkbox" class="onoffswitch-checkbox" id="MerakiLocationsGPS"/>
                                        <label class="onoffswitch-label" for="MerakiLocationsGPS">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            </div>
                       </div>
                       <div class="modal-footer">
                           <a href="#" class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> <span id=""></span>Volver</a>
                           <a href="#" class="btn btn-primary" id="clone"><i class="fa fa-copy"></i> Clonar</a>
                       </div>
                    </div>
                </div>
            </div>
                
        
        
        
            <div class="modal inmodal fade" id="ModalMessage" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header"></div>
                        <div class="modal-body">
                            <div id="message"></div>
                        </div>
                       <div class="modal-footer">
                           <a href="#" class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Volver</a>
                       </div>
                    </div>
                </div>
            </div>
        </div>

</asp:Content>
