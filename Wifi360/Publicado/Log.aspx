﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Site.Master" AutoEventWireup="true" CodeBehind="Log.aspx.cs" Inherits="Wifi360.Log" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
       <link href="css/calendar.css" rel="stylesheet" />
    <script src="js/calendar.full.min.js"></script>
    <script src="js/moment.js"></script>
    <script>

        function readCookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        };

        function bindSelectAttribute(id, valueMember, displayMember, attribute, source) {
            $("#" + id).find('option').remove().end();
            var cookie = readCookie("wifi360-language");
            if (cookie != null) {
                switch (cookie) {
                    case "es": $("#" + id).append('<option value="0">Seleccionar</option>'); break;
                    default: $("#" + id).append('<option value="0">Select </option>'); break;
                }
            }
            else
                $("#" + id).append('<option value="0">Select </option>');
            $.each(source, function (index, item) {
                $("#" + id).append('<option value="' + item[valueMember] + '" rel="' + item[attribute] + '"> ' + item[displayMember] + '</option>');
            });
        };

        $(document).ready(function () {

            var cookie = readCookie("wifi360-language");
            if (cookie != null) {
                switch (cookie) {
                    case "es": var calendar = new CalendarPopup('#start', { dayOfWeekStart: 1, showMask: false, locale: 'es', showWeekIndex: true, format: 'DD/MM/YYYY HH:mm', i18n: { es: { "Chose": "Elegir", "Close": "Cerrar", "Time": "Hora" } } });
                        var calendar2 = new CalendarPopup('#end', { dayOfWeekStart: 1, showMask: false, locale: 'es', showWeekIndex: true, format: 'DD/MM/YYYY HH:mm', i18n: { es: { "Chose": "Elegir", "Close": "Cerrar", "Time": "Hora" } } }); break;
                    default: var calendar = new CalendarPopup('#start', { showMask: false, showWeekIndex: true, format: 'DD/MM/YYYY HH:mm' });
                        var calendar2 = new CalendarPopup('#end', { showMask: false, showWeekIndex: true, format: 'DD/MM/YYYY HH:mm' }); break;
                }
            }
            else {
                var calendar = new CalendarPopup('#start', { dayOfWeekStart: 1, showMask: false, showWeekIndex: true, format: 'DD/MM/YYYY HH:mm' });
                var calendar2 = new CalendarPopup('#end', { dayOfWeekStart: 1, showMask: false, showWeekIndex: true, format: 'DD/MM/YYYY HH:mm' });
            }

            $(".calendar-popup-input-wrapper").attr("style", "display:block;")

            var start = $("#start").val();
            var end = $("#end").val();
            var page = 0;

            var dataStringGroup = 'action=GROUPMEMBERS';
            $.ajax({
                url: "handlers/SMIHandler.ashx",
                data: dataStringGroup,
                contentType: "application/json; charset=utf-8",
                dataType: "text",
                success: function (pReturn) {
                    var lReturn = JSON.parse(pReturn);
                    bindSelectAttribute("members", "IdMember", "Name", "IdMemberType", lReturn.list);
                    if (lReturn.list.length == 1) {
                        $("#members").val(lReturn.list[0].IdMember).change();;

                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#modal-loading").hide();
                    $("#searchResult").html(xhr.statusText)
                }
            });

            var dataString = 'action=searchLog&s=' + start + '&e=' + end + '&idmember=0&idmembertype=0';
            $.ajax({
                url: "handlers/SMIHandler.ashx",
                data: dataString,
                contentType: "application/json; charset=utf-8",
                dataType: "text",
                beforeSend: function () {
                    $("#myModalWait").modal('show');
                },
                complete: function () {
                    $("#myModalWait").modal('hide');
                },
                success: function (pReturn) {
                    var lReturn = JSON.parse(pReturn);
                    var table = "";
                    var cookie = readCookie("wifi360-language");
                    if (cookie != null) {
                        switch (cookie) {
                            case "es": table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Fecha</th><th class='hidden-xs'>Localización</th><th>Nombre de usuario</th><th class='hidden-xs'>Contraseña</th><th>respuesta WB</th><th>Respuesta autenticación</th><th class='hidden-xs'>Mac</th><th>Página de acceso</th></tr>"; break;
                            default: table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Date</th><th class='hidden-xs'>Location</th><th>Username</th><th class='hidden-xs'>Password</th><th>WB response</th><th>Authenticate response</th><th class='hidden-xs'>Mac</th><th>Access page</th></tr>"; break;
                        }
                    }
                    else
                        table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Date</th><th class='hidden-xs'>Location</th><th>Username</th><th class='hidden-xs'>Password</th><th>WB response</th><th>Authenticate response</th><th class='hidden-xs'>Mac</th><th>Access page</th></tr>";
                    $.each(lReturn.list, function (index, item) {
                        table += '<tr><td>' + item["Date"] + '</td><td class="hidden-xs">' + item["Location"] + '</td><td>' + item["UserName"] + '</td><td class="hidden-xs">' + item["Password"] + '</td><td>' + item["NomadixResponse"] + '</td><td>' + item["RadiusResponse"] + '</td><td class="hidden-xs">' + item["CallerID"] + '</td><td>' + item["Page"] + '</td></tr>';
                    });
                    table += "</table>";

                    $("#searchResult").html(table);

                    var pagination = "<ul class='pagination'>";
                    var points1 = 0;
                    var points2 = 0;
                    x = parseInt(page);
                    for (var index = 0; index < lReturn.pageNumber; index++) {
                        if (lReturn.pageNumber <= 10) {
                            if (page == index)
                                pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                            else
                                pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                        }
                        else {
                            if (page == index) {
                                pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                            }
                            else if (index == 0) {
                                pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                            }
                            else if (index == (lReturn.pageNumber - 1)) {
                                pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                            }
                            else if (index < (x - 3)) {
                                if (points1 == 0) {
                                    pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                    points1 = 1;
                                }
                            }
                            else if (index > (x + 3)) {
                                if (points2 == 0) {
                                    pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                    points2 = 1;
                                }
                            }
                            else
                                pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                        }
                    }

                    pagination += "</ul>";

                    $("#searchResult").append(pagination);

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#modal-loading").hide();
                    $("#searchResult").html(xhr.statusText)
                }
            });

            $("#search").on("click", function () {

                var start = $("#start").val();
                var end = $("#end").val();
                var page = 0;
                var s = moment(start, "dd/MM/yyyy HH:mm");
                var e = moment(end, "dd/MM/yyyy HH:mm");
                var idmember = $("#members").val();
                var idmembertype = $('option:selected', $("#members")).attr('rel');
                if ((($("#start").val() == '') && ($("#end").val() == '')) || (s <= e) && ($("#start").val() != '') && ($("#end").val() != '')) {

                    var dataString = 'action=searchLog&s=' + start + '&e=' + end + '&idmember=' + idmember + '&idmembertype=' + idmembertype;
                    $.ajax({
                        url: "handlers/SMIHandler.ashx",
                        data: dataString,
                        contentType: "application/json; charset=utf-8",
                        dataType: "text",
                        beforeSend: function () {
                            $("#myModalWait").modal('show');
                        },
                        complete: function () {
                            $("#myModalWait").modal('hide');
                        },
                        success: function (pReturn) {
                            var lReturn = JSON.parse(pReturn);
                            var table = "";
                            var cookie = readCookie("wifi360-language");
                            if (cookie != null) {
                                switch (cookie) {
                                    case "es": table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Fecha</th><th class='hidden-xs'>Localización</th><th>Nombre de usuario</th><th class='hidden-xs'>Contraseña</th><th>respuesta WB</th><th>Respuesta autenticación</th><th class='hidden-xs'>Mac</th><th>Página de acceso</th></tr>"; break;
                                    default: table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Date</th><th class='hidden-xs'>Location</th><th>Username</th><th class='hidden-xs'>Password</th><th>WB response</th><th>Authenticate response</th><th class='hidden-xs'>Mac</th><th>Access page</th></tr>"; break;
                                }
                            }
                            else
                                table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Date</th><th class='hidden-xs'>Location</th><th>Username</th><th class='hidden-xs'>Password</th><th>WB response</th><th>Authenticate response</th><th class='hidden-xs'>Mac</th><th>Access page</th></tr>";
                            $.each(lReturn.list, function (index, item) {
                                table += '<tr><td>' + item["Date"] + '</td><td class="hidden-xs">' + item["Location"] + '</td><td>' + item["UserName"] + '</td><td class="hidden-xs">' + item["Password"] + '</td><td>' + item["NomadixResponse"] + '</td><td>' + item["RadiusResponse"] + '</td><td class="hidden-xs">' + item["CallerID"] + '</td><td>' + item["Page"] + '</td></tr>';
                            });
                            table += "</table>";

                            $("#searchResult").html(table);

                            var pagination = "<ul class='pagination'>";
                            var points1 = 0;
                            var points2 = 0;
                            x = parseInt(page);
                            for (var index = 0; index < lReturn.pageNumber; index++) {
                                if (lReturn.pageNumber <= 10) {
                                    if (page == index)
                                        pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                    else
                                        pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                                else {
                                    if (page == index) {
                                        pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                    }
                                    else if (index == 0) {
                                        pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                    }
                                    else if (index == (lReturn.pageNumber - 1)) {
                                        pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                    }
                                    else if (index < (x - 3)) {
                                        if (points1 == 0) {
                                            pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                            points1 = 1;
                                        }
                                    }
                                    else if (index > (x + 3)) {
                                        if (points2 == 0) {
                                            pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                            points2 = 1;
                                        }
                                    }
                                    else
                                        pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                            }

                            pagination += "</ul>";

                            $("#searchResult").append(pagination);

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#modal-loading").hide();
                            $("#searchResult").html(xhr.statusText)
                        }
                    });
                }
                else {

                    $("#message2").html(GetMessage("0x0022"));
                    $("#myModal").modal('show');
                }

            });

            $(document).on("click", 'a[rel^="page"]', function (e) {
                e.preventDefault();
                var page = $(this).attr("attr");

                var start = $("#start").val();
                var end = $("#end").val();
                var idmember = $("#members").val();
                var idmembertype = $('option:selected', $("#members")).attr('rel');
                var dataString = 'action=searchLog&s=' + start + '&e=' + end + '&page=' + page +'&idmember=' + idmember + '&idmembertype=' + idmembertype;
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    beforeSend: function () {
                        $("#myModalWait").modal('show');
                    },
                    complete: function () {
                        $("#myModalWait").modal('hide');
                    },
                    success: function (pReturn) {
                        var lReturn = JSON.parse(pReturn);
                        var table = "";
                        var cookie = readCookie("wifi360-language");
                        if (cookie != null) {
                            switch (cookie) {
                                case "es": table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Fecha</th><th class='hidden-xs'>Localización</th><th>Nombre de usuario</th><th class='hidden-xs'>Contraseña</th><th>respuesta WB</th><th>Respuesta autenticación</th><th class='hidden-xs'>Mac</th><th>Página de acceso</th></tr>"; break;
                                default: table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Date</th><th class='hidden-xs'>Location</th><th>Username</th><th class='hidden-xs'>Password</th><th>WB response</th><th>Authenticate response</th><th class='hidden-xs'>Mac</th><th>Access page</th></tr>"; break;
                            }
                        }
                        else
                            table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Date</th><th class='hidden-xs'>Location</th><th>Username</th><th class='hidden-xs'>Password</th><th>WB response</th><th>Authenticate response</th><th class='hidden-xs'>Mac</th><th>Access page</th></tr>";
                        $.each(lReturn.list, function (index, item) {
                            table += '<tr><td>' + item["Date"] + '</td><td class="hidden-xs">' + item["Location"] + '</td><td>' + item["UserName"] + '</td><td class="hidden-xs">' + item["Password"] + '</td><td>' + item["NomadixResponse"] + '</td><td>' + item["RadiusResponse"] + '</td><td class="hidden-xs">' + item["CallerID"] + '</td><td>' + item["Page"] + '</td></tr>';
                        });
                        table += "</table>";

                        $("#searchResult").html(table);

                        var pagination = "<ul class='pagination'>";
                        var points1 = 0;
                        var points2 = 0;
                        x = parseInt(page);
                        for (var index = 0; index < lReturn.pageNumber; index++) {
                            if (lReturn.pageNumber <= 10) {
                                if (page == index)
                                    pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                else
                                    pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                            }
                            else {
                                if (page == index) {
                                    pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                                else if (index == 0) {
                                    pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                                else if (index == (lReturn.pageNumber - 1)) {
                                    pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                                else if (index < (x - 3)) {
                                    if (points1 == 0) {
                                        pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                        points1 = 1;
                                    }
                                }
                                else if (index > (x + 3)) {
                                    if (points2 == 0) {
                                        pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                        points2 = 1;
                                    }
                                }
                                else
                                    pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                            }
                        }

                        pagination += "</ul>";

                        $("#searchResult").append(pagination);

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#modal-loading").hide();
                        $("#searchResult").html(xhr.statusText)
                    }
                });
            });
        });

        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 id="headerpagelocationsetting">Settings</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="default.aspx">Dashboard</a>
                </li>
                <li id="breadpagefatherlocationsetting">
                    <a>Settings</a>
                </li>
                <li class="active">
                    <strong>Log</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5 id="labelboxheaderlog">Log<small> list of accions on system</small></h5>
                </div>
                <div class="ibox-content">  
                    <div class="ibox border-bottom">
                    <div class="ibox-title">
                        <h5 id="headerboxfilterloyalties">Search filters</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-down"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content" style="display: none;">
                        <div role="form">
                            <div class="form-group">
                                <label for="username" id="labelformusermanagementgroupmembers">Group Members</label>
                                <select id="members" class="form-control"></select>
                            </div>
                            <div class="form-group" id="data_5">
                                <label class="font-normal" id="labelformlogdaterange"> Date range </label>
                                <div class="input-daterange input-group" id="datepicker">
                                        <span class="input-group-addon" id="labelformlogdatefrom"> from </span>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="input-sm form-control" id="start" name="start" value=""/>
                                    <span class="input-group-addon" id="labelformlogdatesince"> to </span>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="input-sm form-control" id="end" name="end" value="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <a href="#" id="search" class="btn btn-w-m btn-primary" ><i class="fa fa-search"></i> <span id="labelformlogbuttonsearch">Search</span></a>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div id="searchResult"></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        
</asp:Content>
