﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Site.Master" AutoEventWireup="true" CodeBehind="Analytics.aspx.cs" Inherits="Wifi360.Analytics" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/calendar.css" rel="stylesheet" />
    <script src="js/calendar.full.min.js"></script>
    <script src="js/moment.js"></script>

    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/pie.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>
    <script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>


    <style>
        body { margin:0; padding:0; }
        #map { position:absolute; top:70px; bottom:0; }

        #pie_top_left, #pie_top_center, #pie_top_center_2, #pie_top_right{
          width: 100%;
          height: 350px;
}
        #chartdiv4, #chartdiv5  {
          width: 100%;
          height: 450px;
}

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
            <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 id="headerpageAnalytics">Analytics</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="default.aspx">Dashboard</a>
                </li>
                <li>
                    <a id="breadheadAnalytics">Analytics</a>
                </li>
                <li class="active">
                    <strong  id="breadAnalytics">Analytics</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5 id="labelboxheaderanalytics">Analytics<small> Analytics In Interval</small></h5>
                </div>
                <div class="ibox-content">
                    <div class="ibox border-bottom">
                    <div class="ibox-title">
                        <h5 id="headerboxfilterusersvalid">Search filters</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-down"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content" style="display: none;">
                        <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto;">
                            <div class="scroll_content" style="overflow: hidden; width: auto; ">
                        <div role="form">
                            <div class="form-group">
                                <label id="labelformusermanagementdaterange"> Date Range </label>
                                <div class="input-daterange input-group" id="datepicker">
                                    <span class="input-group-addon" id="labelformusermanagementdatefrom"> From </span>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" id="start" name="start" value=""/>
                                    <span class="input-group-addon" id="labelformusermanagementdatesince"> to </span>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" id="end" name="end" value=""/>
                                </div>
                            </div>
                            <div class="form-group">
                                <a href="#" id="search" class="btn btn-w-m btn-primary" ><i class="fa fa-search"></i><span id="labelformusermanagementbuttonsearch"> Search </span></a>
                            </div>
                        </div>
                    </div>
                        </div><div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 198.02px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                    </div>
                </div>
    <div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5 id="headeronlineUsersInterval">Online device time</h5>
                    </div>
                    <div class="ibox-content">
                        <div id="chartdiv4"></div>				
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5 id="headeronlineUsersPerHourInterval">Average online device by hour</h5>
                    </div>
                    <div class="ibox-content">
                        <div id="chartdiv5"></div>		
                    </div>
            </div>
            </div>


            <div class="col-lg-4 col-sm-4" id="col1">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5 id="headerconnectioDuractionInterval">Sessions time</h5>
                    </div>
                    <div class="ibox-content">
                            <div style="text-align:center;" >
                                <div id="pie_top_left"></div>
                        </div>
                    </div>                                
                </div>
                    </div>
                        <div class="col-lg-3 col-sm-3"  id="col2">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5 id="headercol1groupinterval" style="display:none;">Sessions by Group Member</h5>
                                    <h5 id="headercol1siteinterval" style="display:none;">Sessions by Zone</h5>
                                </div>
                                <div class="ibox-content">
                                        <div  style="text-align:center;">
                                            <div id="pie_top_right"></div>
                                        </div>
                                </div>                                
                                </div>
                            </div>
            <div class="col-lg-3 col-sm-3" id="col3">
                 <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5 id="headercol3siteinterval">Sessions by Access Type</h5>
            </div>
            <div class="ibox-content">
                <div  style="text-align:center;">
                    <div id="pie_top_center"></div>
                </div>
            </div>                                
        </div>
            </div>
            <div class="col-lg-3 col-sm-3" id="col4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5 id="headercol4siteinterval">Sessions by Access Module</h5>
                </div>
                <div class="ibox-content">
                    <div  style="text-align:center;">
                        <div id="pie_top_center_2"></div>
                    </div>
                </div>                                
            </div>
                </div>


            <div class="col-lg-12 col-md-12">
                <div class="row">
                    <div class="col-sm-2">
                    <div class="widget style1 lazur-bg  text-center">
                        <div class="m-b-md">
                            <h5 id="headerdowninterval"> Download</h5>
                                <h3 class="font-bold m-xs" id="downInterval">.</h3>
                        </div>
                    </div>
                    </div>
                    <div class="col-sm-2">
                    <div class="widget style1 lazur-bg  text-center">
                    <div class="m-b-md">
                 
                        <h5 id="headerupinterval"> Upload </h5>
                               <h3 class="font-bold m-xs" id="upInterval">.</h3> 
                                        
                    </div>
                </div>
                        </div>
                    <div class="col-sm-2">
                    <div class="widget style1 lazur-bg  text-center">
                    <div class="m-b-md">
                        <h5 id="headersessionInterval">Sessions </h5>
                        <h3 class="font-bold m-xs" id="sesionInterval">.</h3>
                    </div>
                </div>
                </div>
                    <div class="col-sm-2">
                    <div class="widget style1 lazur-bg  text-center">
                        <div class="m-b-md">
                            <h5 id="headerdeviceInterval">Devices</h5>
                            <h3 class="font-bold m-xs" id="deviceInterval">.</h3>
                        </div>
                    </div>
                    </div>
                        <div class="col-sm-2">
                        <div class="widget style1 lazur-bg  text-center">
                            <div class="m-b-md">
                                <h5 id="headerUsersInterval">Users</h5>
                                <h3 class="font-bold m-xs" id="usersInterval">.</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                    <div class="widget style1 lazur-bg  text-center">
                        <div class="m-b-md">
                            <h5 id="headerRatioInterval">Sessions/Device</h5>
                             <h3 class="font-bold m-xs" id="SesionDeviceInterval">.</h3>
                        </div>
                    </div>

            </div>
                    
                    </div>
            </div>
        </div>
                </div>
    </div>
</div>

                     </div>
                </div>
            </div>
            <div class="modal inmodal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Cerrar</span></button>
                                <h4 class="modal-title">Error</h4>
                            </div>
                            <div class="modal-body">
                                <p class="alert alert-danger" id="message"></p>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>


        </div>
    
    <script>
        function readCookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;

        }



        function dashboard(e) {
            var cookieLocation = readCookie("location");
            var cookieSite = readCookie("site");
            var cookieGroup = readCookie("group");

            $("#chartdiv5").height(9 * $("#chartdiv5").width() / 16);
            $("#chartdiv4").height(9 * $("#chartdiv4").width() / 16);

            if (cookieSite != null) //estamos en un site
            {
                $("#headercol1groupinterval").hide();
                $("#headercol1siteinterval").show();
                $("#col1").removeClass().addClass("col-sm-3");
                $("#col2").removeClass().addClass("col-sm-3");
                $("#col3").removeClass().addClass("col-sm-3");
                $("#col4").removeClass().addClass("col-sm-3");
                var dataString = 'action=USERSBYBILLINGTYPEINTERVAL&start=' + $("#start").val() + '&end=' + $("#end").val();
                $.ajax({
                    url: "handlers/BigDataHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $("#myModalWait").modal('show');
                    },
                    complete: function () {
                        $("#myModalWait").modal('hide');
                    },
                    success: function (pReturn) {
                        var chart = AmCharts.makeChart("pie_top_center", {
                            "type": "pie",
                            "theme": "light",
                            "labelText": "[[percents]]%",
                            "hideLabelsPercent": 5,
                            "dataProvider": pReturn.items,
                            "valueField": "value",
                            "titleField": "label",
                            "balloon": {
                                "adjustBorderColor": true,
                                "color": "#000000",
                                "cornerRadius": 5,
                                "fillColor": "#FFFFFF"
                            },
                            "angle": 15,
                            "startEffect": "easeOutSine",
                            "startDuration": 2,
                            "labelRadius": 15,
                            "innerRadius": "10%",
                            "depth3D": 10,
                            hideCredits: true,
                            "export": {
                                "enabled": true
                            }
                        });

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#message").removeClass().addClass("alert alert-danger");
                        $("#message").html(xhr.statusText);
                        $("#myModalMessage").modal('show');

                    }
                });

                var dataString = 'action=USERSBYZONEINTERVAL&start=' + $("#start").val() + '&end=' + $("#end").val();
                $.ajax({
                    url: "handlers/BigDataHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $("#myModalWait").modal('show');
                    },
                    complete: function () {
                        $("#myModalWait").modal('hide');
                    },
                    success: function (pReturn) {
                        var chart = AmCharts.makeChart("pie_top_right", {
                            "type": "pie",
                            "theme": "light",
                            "labelText": "[[percents]]%",
                            "hideLabelsPercent": 5,
                            "dataProvider": pReturn.items,
                            "valueField": "value",
                            "titleField": "label",
                            "balloon": {
                                "adjustBorderColor": true,
                                "color": "#000000",
                                "cornerRadius": 5,
                                "fillColor": "#FFFFFF"
                            },
                            "angle": 15,
                            "startEffect": "easeOutSine",
                            "startDuration": 2,
                            "labelRadius": 15,
                            "innerRadius": "10%",
                            "depth3D": 10,
                            hideCredits: true,
                            "export": {
                                "enabled": true
                            }
                        });
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#message").removeClass().addClass("alert alert-danger");
                        $("#message").html(xhr.statusText);
                        $("#myModalMessage").modal('show');

                    }
                });

            }
            else if (cookieGroup != null) {
                $("#headercol1groupinterval").show();
                $("#headercol1siteinterval").hide();


                $("#col1").removeClass().addClass("col-sm-4");
                $("#col2").removeClass().addClass("col-sm-4");
                $("#col4").removeClass().addClass("col-sm-4");
                $("#col3").hide();

                var dataString = 'action=ONLINEUSERSBYSITEINTERVAL&start=' + $("#start").val() + '&end=' + $("#end").val()
                $.ajax({
                    url: "handlers/BigDataHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $("#myModalWait").modal('show');
                    },
                    complete: function () {
                        $("#myModalWait").modal('hide');
                    },
                    success: function (pReturn) {
                        var chart = AmCharts.makeChart("pie_top_right", {
                            "type": "pie",
                            "theme": "light",
                            "labelText": "[[percents]]%",
                            "hideLabelsPercent": 5,
                            "dataProvider": pReturn.items,
                            "valueField": "value",
                            "titleField": "label",
                            "balloon": {
                                "adjustBorderColor": true,
                                "color": "#000000",
                                "cornerRadius": 5,
                                "fillColor": "#FFFFFF"
                            },
                            "angle": 15,
                            "startEffect": "easeOutSine",
                            "startDuration": 2,
                            "labelRadius": 15,
                            "innerRadius": "10%",
                            "depth3D": 10,
                            hideCredits: true,
                            "export": {
                                "enabled": true
                            }
                        });
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#message").removeClass().addClass("alert alert-danger");
                        $("#message").html(xhr.statusText);
                        $("#myModalMessage").modal('show');

                    }
                });



            }

            if (cookieLocation != null) {
                $("#col1").removeClass().addClass("col-sm-4");
                $("#col2").hide();
                $("#col3").removeClass().addClass("col-sm-4");
                $("#col4").removeClass().addClass("col-sm-4");
            }

            var dataString = 'action=CONNECTIONBYMODULEINTERVAL&start=' + $("#start").val() + '&end=' + $("#end").val();
            $.ajax({
                url: "handlers/BigDataHandler.ashx",
                data: dataString,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $("#myModalWait").modal('show');
                },
                complete: function () {
                    $("#myModalWait").modal('hide');
                },
                success: function (pReturn) {
                    var chart = AmCharts.makeChart("pie_top_center_2", {
                        "type": "pie",
                        "theme": "light",
                        "labelText": "[[percents]]%",
                        "hideLabelsPercent": 5,
                        "dataProvider": pReturn.items,
                        "valueField": "value",
                        "titleField": "label",
                        "balloon": {
                            "adjustBorderColor": true,
                            "color": "#000000",
                            "cornerRadius": 5,
                            "fillColor": "#FFFFFF"
                        },
                        "angle": 15,
                        "startEffect": "easeOutSine",
                        "startDuration": 2,
                        "labelRadius": 15,
                        "innerRadius": "10%",
                        "depth3D": 10,
                        hideCredits: true,
                        "export": {
                            "enabled": true
                        }
                    });

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#message").removeClass().addClass("alert alert-danger");
                    $("#message").html(xhr.statusText);
                    $("#myModalMessage").modal('show');

                }
            });

            //real time
            var dataString = 'action=CLOSEDSESSIONSTIMEINTERVAL&start=' + $("#start").val() + '&end=' + $("#end").val();
            $.ajax({
                url: "handlers/BigDataHandler.ashx",
                data: dataString,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $("#myModalWait").modal('show');
                },
                complete: function () {
                    $("#myModalWait").modal('hide');
                },
                success: function (pReturn) {
                    var chart = AmCharts.makeChart("pie_top_left", {
                        "type": "pie",
                        "theme": "light",
                        "labelText": "[[percents]]%",
                        "hideLabelsPercent" : 5,
                        "dataProvider" : pReturn.items,
                        "valueField": "value",
                        "titleField": "label",
                        "balloon": {
                            "adjustBorderColor": true,
                            "color": "#000000",
                            "cornerRadius": 5,
                            "fillColor": "#FFFFFF"
                        },
                        "angle": 15,
                        "startEffect": "easeOutSine",
                        "startDuration": 2,
                        "labelRadius": 15,
                        "innerRadius": "10%",
                        "depth3D": 10,
                        hideCredits: true,
                        "export": {
                            "enabled": true
                        }
                    });

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#message").removeClass().addClass("alert alert-danger");
                    $("#message").html(xhr.statusText);
                    $("#myModalMessage").modal('show');

                }
            });

            $("#area").attr("width", $("#svg_content").width());
            var dataString = 'action=ONLINEDEVICE&start=' + $("#start").val() + '&end=' + $("#end").val();
            $.ajax({
                url: "handlers/BigDataHandler.ashx",
                data: dataString,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $("#myModalWait").modal('show');
                },
                complete: function () {
                    $("#myModalWait").modal('hide');
                },
                success: function (pReturn) {

                    var chart = AmCharts.makeChart("chartdiv4", {
                        "type": "serial",
                        "theme": "light",
                        "dataProvider": pReturn.items,
                        "colors": ["rgba(136,185,24,1)"],
                        "graphs": [{
                            "id": "g1",
                            "fillAlphas": 0.4,
                            "valueField": "value",
                            "balloonText": "<div style='margin:5px; font-size:19px;'>Users:<b>[[value]]</b></div>"
                        }],
                        "chartCursor": {
                            "categoryBalloonDateFormat": "JJ:NN, DD-MM-YYYY",
                            "cursorPosition": "mouse"
                        },
                        "categoryField": "label",
                        "categoryAxis": {
                            "minPeriod": "mm",
                            "parseDates": true
                        },
                        hideCredits: true,
                        "export": {
                            "enabled": true
                        }
                    });


                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#message").removeClass().addClass("alert alert-danger");
                    $("#message").html(xhr.statusText);
                    $("#myModalMessage").modal('show');

                }
            });

            var dataString = 'action=VALUESINTERVAL&start=' + $("#start").val() + '&end=' + $("#end").val();
            $.ajax({
                url: "handlers/BigDataHandler.ashx",
                data: dataString,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $("#myModalWait").modal('show');
                },
                complete: function () {
                    $("#myModalWait").modal('hide');
                },
                success: function (pReturn) {
                    $("#upInterval").html(pReturn.Upload_Interval + " GB");
                    $("#downInterval").html(pReturn.Download_Interval + " GB");
                    $("#sesionInterval").html(pReturn.Sesion_Interval);
                    $("#deviceInterval").html(pReturn.device_Interval);
                    $("#usersInterval").html(pReturn.Users_Interval);
                    $("#SesionDeviceInterval").html(pReturn.sesiondevice_Interval);

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#message").removeClass().addClass("alert alert-danger");
                    $("#message").html(xhr.statusText);
                    $("#myModalMessage").modal('show');

                }
            });

            //HISTOGRAMA 
            $("#histograma").attr("width", $("#svg_content").width());
            var dataString = 'action=USERSPERHOURS&start=' + $("#start").val() + '&end=' + $("#end").val();
            $.ajax({
                url: "handlers/BigDataHandler.ashx",
                data: dataString,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $("#myModalWait").modal('show');
                },
                complete: function () {
                    $("#myModalWait").modal('hide');
                },
                success: function (pReturn) {
                    
                    var chart = AmCharts.makeChart("chartdiv5", {
                        "type": "serial",
                        "theme": "light",
                        "colors": ["rgba(136,185,24,1)"],
                        "dataProvider": pReturn.items,

                        "valueAxes": [{
                            "gridColor": "#FFFFFF",
                            "gridAlpha": 0.2,
                            "dashLength": 0
                        }],
                        "gridAboveGraphs": true,
                        "startDuration": 1,
                        "graphs": [{
                            "balloonText": "[[category]]: <b>[[value]]</b>",
                            "fillAlphas": 0.8,
                            "lineAlpha": 0.2,
                            "type": "column",
                            "valueField": "value"
                        }],
                        "chartCursor": {
                            "categoryBalloonEnabled": false,
                            "cursorAlpha": 0,
                            "zoomable": false
                        },
                        "categoryField": "Hora",
                        "categoryAxis": {
                            "gridPosition": "start",
                            "gridAlpha": 0,
                            "tickPosition": "start",
                            "tickLength": 24
                        },
                        hideCredits: true,
                        "export": {
                            "enabled": true
                        }

                    });

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#message").removeClass().addClass("alert alert-danger");
                    $("#message").html(xhr.statusText);
                    $("#myModalMessage").modal('show');

                }
            });
        };

        $(document).ready(function (e) {

            var cookie = readCookie("wifi360-language");
            if (cookie != null) {
                switch (cookie) {
                    case "es": var calendar = new CalendarPopup('#start', {dayOfWeekStart: 1, showMask: false, locale: 'es', showWeekIndex: true, format: 'DD/MM/YYYY HH:mm', i18n: { es: { "Chose": "Elegir", "Close": "Cerrar", "Time": "Hora" } } });
                        var calendar2 = new CalendarPopup('#end', {dayOfWeekStart: 1, showMask: false, locale: 'es', showWeekIndex: true, format: 'DD/MM/YYYY HH:mm', i18n: { es: { "Chose": "Elegir", "Close": "Cerrar", "Time": "Hora" } } });
                        break;
                    default:
                                var calendar = new CalendarPopup('#start', { showMask: false, showWeekIndex: true, format: 'DD/MM/YYYY HH:mm' });
                                var calendar2 = new CalendarPopup('#end', { showMask: false, showWeekIndex: true, format: 'DD/MM/YYYY HH:mm' });
                }
            }
            else {
                var calendar = new CalendarPopup('#start', {dayOfWeekStart: 1, showMask: false, showWeekIndex: true, format: 'DD/MM/YYYY HH:mm' });
                var calendar2 = new CalendarPopup('#end', {dayOfWeekStart: 1, showMask: false, showWeekIndex: true, format: 'DD/MM/YYYY HH:mm' });

            }

            $(".calendar-popup-input-wrapper").attr("style", "display:block;");
            $("#start").val((moment().subtract(7, 'days').format('DD/MM/YYYY HH:mm')));
            $("#end").val((moment().format('DD/MM/YYYY HH:mm')));

            dashboard();

            $("#search").click(function (e) {
                $("#pie_top_left").html("");
                $("#pie_top_center").html("");
                $("#pie_top_center_2").html("");
                $("#pie_top_right").html("");

                $("#chartdiv4").html("");
                $("#chartdiv5").html("");

                $("#upInterval").html(".");
                $("#downInterval").html(".");
                $("#sesionInterval").html(".");
                $("#deviceInterval").html(".");
                $("#usersInterval").html(".");
                $("#SesionDeviceInterval").html(".");

                dashboard();
            });

        });

</script>
</asp:Content>
