﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Site.Master" AutoEventWireup="true" CodeBehind="location.aspx.cs" Inherits="Wifi360.location" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/plugins/switchery/switchery.css" rel="stylesheet" />
    <script src="js/plugins/switchery/switchery.js"></script>
    <script>

        function bindSelect(id, valueMember, displayMember, source) {
            $("#" + id).find('option').remove().end();
            var cookie = readCookie("wifi360-language");
            if (cookie != null) {
                switch (cookie) {
                    case "es": $("#" + id).append('<option value="0">Seleccionar</option>'); break;
                    default: $("#" + id).append('<option value="0">Select</option>'); break;
                }
            }
            else
                $("#" + id).append('<option value="0">Select</option>');
            $.each(source, function (index, item) {
                $("#" + id).append('<option value="' + item[valueMember] + '"> ' + item[displayMember] + '</option>');
            });
        };

        function readCookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        };

        function getParameters() {
            var searchString = window.location.search.substring(1)
              , params = searchString.split("&")
              , hash = {}
            ;

            for (var i = 0; i < params.length; i++) {
                var val = params[i].split("=");
                hash[unescape(val[0])] = unescape(val[1]);
            }
            return hash;
        }

        $(document).ready(function () {

            var cookie = readCookie("wifi360-language");

            var dataString3 = 'action=billingmodules';
            $.ajax({
                url: "handlers/SMIHandler.ashx",
                data: dataString3,
                contentType: "application/json; charset=utf-8",
                dataType: "text",
                beforeSend: function () {
                    $("#myModalWait").modal('show');
                },
                complete: function () {
                    $("#myModalWait").modal('hide');
                },
                success: function (pReturn) {
                    var lReturn = JSON.parse(pReturn);
                    bindSelect('defaultmodule', 'IdBillingModule', 'Name', lReturn.list);

                    var parameters = getParameters();

                    var dataString4 = 'action=Currencies';
                    $.ajax({
                        url: "handlers/SMIHandler.ashx",
                        data: dataString4,
                        contentType: "application/json; charset=utf-8",
                        dataType: "text",
                        success: function (pReturn4) {
                            var lReturn4 = JSON.parse(pReturn4);
                            bindSelect('currencies', 'Code', 'Badge', lReturn4.list);

                            var dataString = 'action=location&id=' + parameters.id;
                            $.ajax({
                                url: "handlers/SMIHandler.ashx",
                                data: dataString,
                                contentType: "application/json; charset=utf-8",
                                dataType: "text",

                                success: function (pReturn) {
                                    var lReturn = JSON.parse(pReturn);
                                    $("#token").val(lReturn.token);
                                    $("#location").val(lReturn.Description);
                                    $("#urlLanding").val(lReturn.UrlLanding);
                                    $("#defaultmodule").val(lReturn.DefaultModule);
                                    $("#currencies").val(lReturn.Currency);
                                    $("#exchange").val(lReturn.CurrencyExchange);
                                    $("exchange").disableSelection;
                                    if (lReturn.DisclaimerMandatory == true)
                                        $("#disclaimermandatory").attr('checked', true);
                                    if (lReturn.Default == true)
                                        $("#default").attr('checked', true);
                                    $("#labelcurrencysite").html(lReturn.CurrencySite)
                                    $("#latitude").val(lReturn.Latitude)
                                    $("#longitude").val(lReturn.Longitude)
                                    if (lReturn.MandatorySurvey == true)
                                        $("#mandatorysurvey").attr('checked', true);
                                    $("#delaysurvey").val(lReturn.DelaySurvey);
                                    
                                    var text = '';
                                    //$.each(lReturn.redes, function (index, item) {
                                    //    text += '<div class="form-group"> <label class="control-label col-sm-3" > ' + item["Name"] + '</label><div class="col-sm-9"><input type="checkbox" rel="checkSN" id="' + item["IdSocialNetwork"] + '" ';
                                    //    if (item["Active"])
                                    //        text += 'checked="' + item["Active"] + '"';
                                    //    text += 'class="js-switch"/></div></div>';

                                    //});

                                    //$("#socialnetwokscheks").html(text);

                                    if (lReturn.LoginModule == true)
                                        $("#loginmodule").attr('checked', true);
                                    if (lReturn.VoucherModule == true)
                                        $("#vouchermodule").attr('checked', true);
                                    if (lReturn.SocialNetworkModule == true)
                                        $("#socialnetworksmodule").attr('checked', true);
                                    if (lReturn.FreeAccessModule == true)
                                        $("#freeaccessmodule").attr('checked', true);
                                    if (lReturn.PayAccessModule == true)
                                        $("#payaccessmodule").attr('checked', true);
                                    if (lReturn.CustomModule == true)
                                        $("#custommodule").attr('checked', true);

                                    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

                                    elems.forEach(function (html) {
                                        var switchery = new Switchery(html, { color: '#1AB394' });
                                    });

                                    var table = "";
                                    var cookie = readCookie("wifi360-language");
                                    if (cookie != null) {
                                        switch (cookie) {
                                            case "es": table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Idioma</th><th class='hidden-xs'>Orden</th><th>Pregunta</th><th class='hidden-xs'>Tipo</th><th class='hidden-xs'>Valores</th><th></th></tr>"; break;
                                            default: table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>language</th><th class='hidden-xs'>Order</th><th>Question</th><th class='hidden-xs'>Type</th><th class='hidden-xs'>Values</th><th></th></tr>"; break;
                                        }
                                    }
                                    else
                                        if (lReturn.encuesta != null){
                                            table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>language</th><th class='hidden-xs'>Order</th><th>Question</th><th class='hidden-xs'>Type</th><th class='hidden-xs'>Values</th><th></th></tr>";
                                            $.each(lReturn.encuesta, function (index, item) {
                                                if (parameters.idsite != undefined)
                                                    table += '<tr><td>' + item["Language"] + '</td><td class="hidden-xs">' + item["Order"] + '</td><td>' + item["Question"] + '</td><td class="hidden-xs">' + item["Type"] + '</td><td class="hidden-xs">' + item["Values"] + '</td><td><a href="/surveyquestion.aspx?id=' + item["IdSurvey"] + '&idlocation=' + parameters.id + '&idsite=' + parameters.idsite + '&return=' + parameters.return + '"><i class="fa fa-pencil"></i></a></td></tr>';
                                                else
                                                    table += '<tr><td>' + item["Language"] + '</td><td class="hidden-xs">' + item["Order"] + '</td><td>' + item["Question"] + '</td><td class="hidden-xs">' + item["Type"] + '</td><td class="hidden-xs">' + item["Values"] + '</td><td><a href="/surveyquestion.aspx?id=' + item["IdSurvey"] + '&idlocation=' + parameters.id + '"><i class="fa fa-pencil"></i></a></td></tr>';

                                            });
                                            table += "</table>";
                                        }
                                    $("#surveys").html(table);

                                    var dataString2 = 'action=LocationsText&id=' + parameters.id;
                                    $.ajax({
                                        url: "handlers/SMIHandler.ashx",
                                        data: dataString2,
                                        contentType: "application/json; charset=utf-8",
                                        dataType: "text",

                                        success: function (pReturn) {
                                            var lReturn = JSON.parse(pReturn);
                                            var table = "";
                                            if (cookie != null) {
                                                switch (cookie) {
                                                    case "es": table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Idioma</th><th></th></tr>"; break;
                                                    default: table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Language</th><th></th></tr>"; break;
                                                }
                                            }
                                            else
                                                table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Language</th><th></th></tr>";

                                            $.each(lReturn.list, function (index, item) {
                                                if (parameters.idsite != undefined)
                                                    table += '<tr><td>' + item["language"] + '</td><td><a href="/locationtext.aspx?id=' + item["idlocationText"] + '&idsite=' + parameters.idsite + '&return='+parameters.return +'"><i class="fa fa-pencil"></i></a></td></tr>';
                                                else
                                                    table += '<tr><td>' + item["language"] + '</td><td><a href="/locationtext.aspx?id=' + item["idlocationText"] + '"><i class="fa fa-pencil"></i></a></td></tr>';
                                            });

                                            table += "</table>";

                                            $("#locationstext").html(table);
                                        },
                                        error: function (xhr, ajaxOptions, thrownError) {
                                            $("#modal-loading").hide();
                                            $("#searchResult").html(xhr.statusText)
                                        }
                                    });

                                    var dataString4 = 'action=LocationsModuleText&id=' + parameters.id;
                                    $.ajax({
                                        url: "handlers/SMIHandler.ashx",
                                        data: dataString4,
                                        contentType: "application/json; charset=utf-8",
                                        dataType: "text",

                                        success: function (pReturn) {
                                            var lReturn = JSON.parse(pReturn);
                                            var table = "";
                                            if (cookie != null) {
                                                switch (cookie) {
                                                    case "es": table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Módulo</th><th>Idioma</th><th></th></tr>"; break;
                                                    default: table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Module</th><th>Language</th><th></th></tr>"; break;
                                                }
                                            }
                                            else
                                                table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Module</th><th>Language</th><th></th></tr>";

                                            $.each(lReturn.list, function (index, item) {
                                                if (parameters.idsite != undefined)
                                                    table += '<tr><td>' + item["billingModule"] + '</td><td>' + item["language"] + '</td><td><a href="/locationmoduletext.aspx?id=' + item["idlocationText"] + '&idlocation=' + parameters.id + '&idsite=' + parameters.idsite + '&return=' + parameters.return +'"><i class="fa fa-pencil"></i></a></td></tr>';
                                                else
                                                    table += '<tr><td>' + item["billingModule"] + '</td><td>' + item["language"] + '</td><td><a href="/locationmoduletext.aspx?id=' + item["idlocationText"] + '"><i class="fa fa-pencil"></i></a></td></tr>';
                                            });

                                            table += "</table>";

                                            $("#locationsmoduletext").html(table);
                                        },
                                        error: function (xhr, ajaxOptions, thrownError) {
                                            $("#modal-loading").hide();
                                            $("#searchResult").html(xhr.statusText)
                                        }
                                    });

                                    var dataString5 = 'action=disclaimers&id=' + parameters.id;
                                    $.ajax({
                                        url: "handlers/SMIHandler.ashx",
                                        data: dataString5,
                                        contentType: "application/json; charset=utf-8",
                                        dataType: "text",

                                        success: function (pReturn) {
                                            var lReturn = JSON.parse(pReturn);
                                            var table = "";
                                            if (cookie != null) {
                                                switch (cookie) {
                                                    case "es": table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Fecha de creación</th><th>Idioma</th><th>Tipo</th><th></th></tr>"; break;
                                                    default: table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Creation Date</th><th>Language</th><th>Type</th><th></th></tr>"; break;
                                                }
                                            }
                                            else
                                                table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Creation Date</th><th>Language</th><th>Type</th><th></th></tr>";
                                            $.each(lReturn.list, function (index, item) {
                                                if (parameters.idsite != undefined)
                                                    table += '<tr><td>' + item["date"] + '</td><td>' + item["language"] + '</td><td>' + item["typeDescription"] + '</td><td><a href="/disclaimer.aspx?id=' + item["iddisclaimer"] + '&idlocation=' + parameters.id + '&idsite=' + parameters.idsite + '&return=' + parameters.return + '"><i class="fa fa-pencil"></i></a></td></tr>';
                                                else
                                                    table += '<tr><td>' + item["date"] + '</td><td>' + item["language"] + '</td><td>' + item["typeDescription"] + '</td><td><a href="/disclaimer.aspx?id=' + item["iddisclaimer"] + '"><i class="fa fa-pencil"></i></a></td></tr>';
                                            });

                                            table += "</table>";

                                            $("#disclaimers").html(table);
                                        },
                                        error: function (xhr, ajaxOptions, thrownError) {
                                            $("#modal-loading").hide();
                                            $("#searchResult").html(xhr.statusText)
                                        }
                                    });

                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    $("#modal-loading").hide();
                                    $("#searchResult").html(xhr.statusText)
                                }
                            });

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#modal-loading").hide();
                            $("#searchResult").html(xhr.statusText)
                        }
                    });

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#modal-loading").hide();
                    $("#searchResult").html(xhr.statusText)
                }
            });
            
            $("#save").click(function (e) {
                e.preventDefault();

                var parameters = getParameters();

                var url = $("#urlLanding").val();
                var defaultmodule = $("#defaultmodule").val();
                var disclaimermandatory = $("#disclaimermandatory").is(":checked");
                var _default = $("#default").is(":checked");

                var delaysurvey = 0;
                switch ($("#unidadesdelaysurvey").val()) {
                    case 'h': delaysurvey = $("#delaysurvey").val().replace(',', '.');
                        break;
                    case 'd': delaysurvey = $("#delaysurvey").val().replace(',', '.') * 24;
                        break;
                }
                var data = {};
                data["action"] = 'SAVELOCATION';
                data["idlocation"] = parameters.id;
                data["urllanding"] = url;
                data["defaultmodule"] = defaultmodule;
                data["disclaimermandatory"] = disclaimermandatory;
                data["default"] = _default;
                data["currency"] = $("#currencies").val();
                data["exchange"] = $("#exchange").val();
                data["latitude"] = $("#latitude").val();
                data["longitude"] = $("#longitude").val();
                data["freeAccessModule"] = $("#freeaccessmodule").is(':checked');
                data["payAccessModule"] = $("#payaccessmodule").is(':checked');
                data["socialNetworksModule"] = $("#socialnetworksmodule").is(':checked');
                data["customModule"] = $("#custommodule").is(':checked');
                data["voucherModule"] = $("#vouchermodule").is(':checked');
                data["loginModule"] = $("#loginmodule").is(':checked');
                data["mandatorysurvey"] = $("#mandatorysurvey").is(':checked');
                data["delaysurvey"] = delaysurvey;

                var sntext = '';
                //$('input[rel^="checkSN"]').each(function (input) {
                //    sntext += $(this).attr('id') + '=' + $(this).is(':checked') + '|';
                //});

                data["SocialNetworks"] = sntext;

                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    type: "POST",
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $("#myModalWait").modal('show');
                    },
                    complete: function () {
                        $("#myModalWait").modal('hide');
                    },
                    success: function (msg) {
                        if (msg.code == "OK") {
                            $("#message").removeClass().addClass("alert alert-success");
                            $("#message").html(GetMessage(msg.message));

                        }
                        else {
                            $("#message").removeClass().addClass("alert alert-danger");
                            $("#message").html(GetMessage(msg.message));
                        }
                        $("#myModalMessage").modal('show');
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#loading").html("We have a problem, please try again later.");
                        alert('error: ' + xhr.statusText);
                    }
                });
            });

            $("#back").click(function (e) {
                e.preventDefault();
                var parameters = getParameters();
                var url = "locationsettings.aspx";
                if (parameters.idsite != undefined) {
                    url = "locationsettings.aspx?idsite=" + parameters.idsite + "&return="+parameters.return;
                }
                window.location.href = url;                                
            });

            $("#new").click(function (e) {
                e.preventDefault();
                var parameters = getParameters();
                window.location.href = "surveyquestion.aspx?idlocation=" + parameters.id;
            });

            $("#prevunidadesdelaysurvey").val($("#unidadesdelaysurvey").val());
            $("#unidadesdelaysurvey").change(function (e) {
                switch ($("#unidadesdelaysurvey").val()) {
                    case 'h':
                        switch ($("#prevunidadesdelaysurvey").val()) {
                            case 'd': $("#delaysurvey").val(($("#delaysurvey").val().replace(",", ".") * 24).toString().replace(",", ".")); break;
                        }
                        break;
                    case 'd':
                        switch ($("#prevunidadesdelaysurvey").val()) {
                            case 'h': $("#delaysurvey").val(($("#delaysurvey").val().replace(",", ".") / 24).toString().replace(",", ".")); break;
                        }
                        break;
                }
                $("#prevunidadesdelaysurvey").val($("#unidadesdelaysurvey").val());
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 id="headerpagelocationsetting">Settings</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="default.aspx">Dashboard</a>
                </li>
                <li>
                    <a id="breadpagefatherlocationsetting">Settings</a>
                </li>
                <li>
                    <a href="locationsettings.aspx" id="breadpagelocationsetting">Locations</a>
                </li>
                <li class="active">
                    <strong id="breadpagelocationsettingactive">Location settings</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-9">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                    <h5 id="labelboxheaderlocationsetting">Location settings<small> Details of location settings</small></h5>
                </div>
                    <div class="ibox-content">
                     <div class="form-horizontal">
                         
                    <div class="form-group"> <label class="control-label col-sm-4" id="labellocationsettingsformtoken">Token</label><div class="col-sm-8"><input type="text" class="form-control" id="token" disabled="disabled" /></div></div>
                    <div class="form-group"> <label class="control-label col-sm-4" id="labellocationsettingsformname">Location</label><div class="col-sm-8"><input type="text" class="form-control" id="location" disabled="disabled" /></div></div>
                    <div class="form-group"> <label class="control-label col-sm-4" id="labellocationsettingsformmodule">Default module</label><div class="col-sm-8"><select class="form-control" id="defaultmodule"></select></div></div>
                    <div class="form-group"> <label class="control-label col-sm-4" id="labellocationsettingsformdisclaimer">Mandatory disclaimer</label><div class="col-sm-8"><input type="checkbox"  id="disclaimermandatory" class="js-switch"/></div></div>
                    <div class="form-group"> <label class="control-label col-sm-4" id="labellocationsettingsformsurvey">Mandatory survey</label><div class="col-sm-8"><input type="checkbox"  id="mandatorysurvey" class="js-switch"/></div></div>
                         <div class="form-group"> <label class="control-label col-sm-4" id="labellocationsettingsformdealysurvey">Time between surveys</label><div class="col-sm-6"><input type="number"  id="delaysurvey" class="form-control"/></div>
                             <div class="col-sm-2">
                                    <select class="form-control" id="unidadesdelaysurvey">
                                        <option value="h" selected="selected">hours</option>
                                        <option value="d">days</option>
                                    </select>
                                </div>

                         </div>
                    <div class="form-group"> <label class="control-label col-sm-4" id="labellocationsettingsformlanding">URL Landing</label><div class="col-sm-8"><input type="text" class="form-control" id="urlLanding" /></div></div>
                    <%--<div class="form-group"> <label class="control-label col-sm-4" id="labellocationsettingsformlocationid">Id per device</label><div class="col-sm-8"><input type="text" class="form-control" id="deviceid" /></div></div>--%>
                    <div class="form-group"> <label class="control-label col-sm-4" id="labellocationsettingsformdefault">Default</label><div class="col-sm-8"><input type="checkbox" id="default" class="js-switch"/></div></div>
                    <div class="form-group"> <label class="control-label col-sm-4" id="labelsettingsformpaypalcurrency">Currency code</label><div class="col-sm-8"><select id="currencies" class="form-control"></select></div></div>
                    <div class="form-group, hide"> <label class="control-label col-sm-4" id="labellocationsettingsformexchange">Exchange rate with </label><label class="control-label col-sm-1" style="text-align:left;" id="labelcurrencysite"></label><div class="col-sm-2"><input type="text" class="form-control" id="exchange" /></div></div>
                    <div class="form-group"> <label class="control-label col-sm-4" id="labellocationsettingsformlatitude">Latitude</label><div class="col-sm-4"><input type="text" class="form-control" id="latitude" /></div></div>
                    <div class="form-group"> <label class="control-label col-sm-4" id="labelsettingsformlongitude">Longitude</label><div class="col-sm-4"><input type="text" class="form-control" id="longitude" /></div></div>
                    <%--<div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title text-center">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTen" class="collapsed" aria-expanded="false"><span id="labelacordionheadersocialnetworks">Social Netwoks</span></a>
                            </h4>
                        </div>
                        <div id="collapseTen" class="panel-collapse collapse" aria-expanded="false">
                            <div class="panel-body">
                                <div class="form-horizontal" id="socialnetwokscheks">
                                    <div class="form-group"> <label class="control-label col-sm-3" id="label1">Login module</label><div class="col-sm-9"><input type="checkbox" id="check1" class="js-switch"/></div></div>
                                    <div class="form-group"> <label class="control-label col-sm-3" id="label2">Free Access module</label><div class="col-sm-9"><input type="checkbox" id="check2" class="js-switch" /></div></div>
                                    <div class="form-group"> <label class="control-label col-sm-3" id="label3">Premium Access module</label><div class="col-sm-9"><input type="checkbox" id="check3" class="js-switch"/></div></div>
                                    <div class="form-group"> <label class="control-label col-sm-3" id="label4">Social Access module</label><div class="col-sm-9"><input type="checkbox" id="check4" class="js-switch" /></div></div>
                                    <div class="form-group"> <label class="control-label col-sm-3" id="label5">Custom Access module</label><div class="col-sm-9"><input type="checkbox" id="check5" class="js-switch" /></div></div>
                                    <div class="form-group"> <label class="control-label col-sm-3" id="label5">Voucher module</label><div class="col-sm-9"><input type="checkbox" id="check6" class="js-switch" /></div></div>

                                </div>
                            </div>
                        </div>
                        </div>--%>
                         <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title text-center">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseEleven" class="collapsed" aria-expanded="false"><span id="labelacordionheaderModules">Modules</span></a>
                            </h4>
                        </div>
                        <div id="collapseEleven" class="panel-collapse collapse" aria-expanded="false">
                            <div class="panel-body">
                                <div class="form-horizontal" id="locationscheks">
                                    <div class="form-group"> <label class="control-label col-sm-3" id="label1">Login module</label><div class="col-sm-9"><input type="checkbox" id="loginmodule" class="js-switch"/></div></div>
                                    <div class="form-group"> <label class="control-label col-sm-3" id="label2">Free Access module</label><div class="col-sm-9"><input type="checkbox" id="freeaccessmodule" class="js-switch" /></div></div>
                                    <div class="form-group"> <label class="control-label col-sm-3" id="label3">Premium Access module</label><div class="col-sm-9"><input type="checkbox" id="payaccessmodule" class="js-switch"/></div></div>
                                    <div class="form-group"> <label class="control-label col-sm-3" id="label4">Social Access module</label><div class="col-sm-9"><input type="checkbox" id="socialnetworksmodule" class="js-switch" /></div></div>
                                    <div class="form-group"> <label class="control-label col-sm-3" id="label5">Custom Access module</label><div class="col-sm-9"><input type="checkbox" id="custommodule" class="js-switch" /></div></div>
                                    <div class="form-group"> <label class="control-label col-sm-3" id="label6">Voucher module</label><div class="col-sm-9"><input type="checkbox" id="vouchermodule" class="js-switch" /></div></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                </div>

                <div class="ibox-content">
                   <h5 id="labelheadergeneraltext">Location general texts</h5>
                        <div id="locationstext">
                        </div>
                </div>
                <div class="ibox-content">
                   <h5 id="labelheadergeneraltextmodule">Modules text</h5>
                        <div id="locationsmoduletext">
                        </div>
                </div>
               <div class="ibox-content">
                   <h5 id="labelheadergeneraldisclaimer">Disclaimers</h5>
                        <div id="disclaimers">
                        </div>
                </div>
                <div class="ibox-content">
                   <h5 id="labelheadergeneralsurvey">Survey</h5>
                        <a href="#" id="new" class="btn btn-primary"><i class="fa fa-plus"></i> <span id="labelformsurveybuttonnew">New</span></a>
                        <div id="surveys">
                        </div>
                </div>
            </div>
            </div>

            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5 id="labelboxheaderacitonlocationsetting">Actions over location</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <p>
                            <a href="#" class="btn btn-w-m btn-lg btn-primary btn-block" id="save"><i class="fa fa-save"></i> <span id="labelformlocationsettingsbuttonsave">Save</span> </a>

                            <a href="#" role="button" class="btn btn-w-m btn-lg btn-default btn-block" id="back" data-toggle="modal"><i class="fa fa-backward"></i> <span id="labelformlocationsettingsbuttonback">Back</span> </a>

                        </p>
                    </div>
                </div>
            </div>
                        </div>
            <div class="modal inmodal fade" id="myModalMessage" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                            </div>
                            <div class="modal-body">
                                <div id="message">
                                    
                                </div>
                            </div>
                          <div class="modal-footer">
                            <a href="#" class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Volver</a>
                          </div>
                        </div>
                    </div>
                </div>
        </div>
    <input type="text" id="prevunidadesdelaysurvey" hidden="hidden" />
</asp:Content>
