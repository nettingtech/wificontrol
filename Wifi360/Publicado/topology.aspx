﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Site.Master" AutoEventWireup="true" CodeBehind="topology.aspx.cs" Inherits="Wifi360.topology" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!-- START SIGMA IMPORTS -->
<script src="../js/sigma/sigma.core.js"></script>
<script src="../js/sigma/conrad.js"></script>
<script src="../js/sigma/utils/sigma.utils.js"></script>
<script src="../js/sigma/utils/sigma.polyfills.js"></script>
<script src="../js/sigma/sigma.settings.js"></script>
<script src="../js/sigma/classes/sigma.classes.dispatcher.js"></script>
<script src="../js/sigma/classes/sigma.classes.configurable.js"></script>
<script src="../js/sigma/classes/sigma.classes.graph.js"></script>
<script src="../js/sigma/classes/sigma.classes.camera.js"></script>
<script src="../js/sigma/classes/sigma.classes.quad.js"></script>
<script src="../js/sigma/classes/sigma.classes.edgequad.js"></script>
<script src="../js/sigma/captors/sigma.captors.mouse.js"></script>
<script src="../js/sigma/captors/sigma.captors.touch.js"></script>
<script src="../js/sigma/renderers/sigma.renderers.canvas.js"></script>
<script src="../js/sigma/renderers/sigma.renderers.webgl.js"></script>
<script src="../js/sigma/renderers/sigma.renderers.svg.js"></script>
<script src="../js/sigma/renderers/sigma.renderers.def.js"></script>
<script src="../js/sigma/renderers/webgl/sigma.webgl.nodes.def.js"></script>
<script src="../js/sigma/renderers/webgl/sigma.webgl.nodes.fast.js"></script>
<script src="../js/sigma/renderers/webgl/sigma.webgl.edges.def.js"></script>
<script src="../js/sigma/renderers/webgl/sigma.webgl.edges.fast.js"></script>
<script src="../js/sigma/renderers/webgl/sigma.webgl.edges.arrow.js"></script>
<script src="../js/sigma/renderers/canvas/sigma.canvas.labels.def.js"></script>
<script src="../js/sigma/renderers/canvas/sigma.canvas.hovers.def.js"></script>
<script src="../js/sigma/renderers/canvas/sigma.canvas.nodes.def.js"></script>
<script src="../js/sigma/renderers/canvas/sigma.canvas.edges.def.js"></script>
<script src="../js/sigma/renderers/canvas/sigma.canvas.edges.curve.js"></script>
<script src="../js/sigma/renderers/canvas/sigma.canvas.edges.arrow.js"></script>
<script src="../js/sigma/renderers/canvas/sigma.canvas.edges.curvedArrow.js"></script>
<script src="../js/sigma/renderers/canvas/sigma.canvas.edgehovers.def.js"></script>
<script src="../js/sigma/renderers/canvas/sigma.canvas.edgehovers.curve.js"></script>
<script src="../js/sigma/renderers/canvas/sigma.canvas.edgehovers.arrow.js"></script>
<script src="../js/sigma/renderers/canvas/sigma.canvas.edgehovers.curvedArrow.js"></script>
<script src="../js/sigma/renderers/canvas/sigma.canvas.extremities.def.js"></script>
<script src="../js/sigma/renderers/svg/sigma.svg.utils.js"></script>
<script src="../js/sigma/renderers/svg/sigma.svg.nodes.def.js"></script>
<script src="../js/sigma/renderers/svg/sigma.svg.edges.def.js"></script>
<script src="../js/sigma/renderers/svg/sigma.svg.edges.curve.js"></script>
<script src="../js/sigma/renderers/svg/sigma.svg.labels.def.js"></script>
<script src="../js/sigma/renderers/svg/sigma.svg.hovers.def.js"></script>
<script src="../js/sigma/middlewares/sigma.middlewares.rescale.js"></script>
<script src="../js/sigma/middlewares/sigma.middlewares.copy.js"></script>
<script src="../js/sigma/misc/sigma.misc.animation.js"></script>
<script src="../js/sigma/misc/sigma.misc.bindEvents.js"></script>
<script src="../js/sigma/misc/sigma.misc.bindDOMEvents.js"></script>
<script src="../js/sigma/misc/sigma.misc.drawHovers.js"></script>
<!-- END SIGMA IMPORTS -->
    <script src="js/sigma/sigma.plugins.dragNodes/sigma.plugins.dragNodes.js"></script>

 <style>
    #graph-container {
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
      position: absolute;
    }

    #sidebar {
      bottom: 0;
      right: 0;
      width: 200px;
      height: 150px;
      position: absolute;
      background-color: #999;
      padding: 10px;
    }
  </style>

    <script>
    $(document).ready(function (e) {
        var dataString = 'action=topology';
        $.ajax({
            url: "handlers/BigDataHandler.ashx",
            data: dataString,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $("#myModalWait").modal('show');
            },
            complete: function () {
                $("#myModalWait").modal('hide');
            },
            success: function (pReturn) {
                /**
 * This example shows how to use the dragNodes plugin.
 */
                var i,
                    s,
                    N = 25,
                    E = 20,
                    g = {
                        nodes: [],
                        edges: []
                    };
                colors = [
                  '#617db4',
                  '#668f3c',
                  '#c6583e',
                  '#b956af'
                ];
                // Generate a random graph:
                for (i = 0; i < N; i++)
                    g.nodes.push({
                        id: 'n' + i,
                        label: 'Node ' + i,
                        x: Math.random(),
                        y: Math.random(),
                        size: 20,
                        color: colors[Math.floor(Math.random() * colors.length)]
                    });

                for (i = 0; i < E; i++)
                    g.edges.push({
                        id: 'e' + i,
                        source: 'n' + (Math.random() * N | 0),
                        target: 'n' + (Math.random() * N | 0),
                        size: Math.random(),
                        color: colors[Math.floor(Math.random() * colors.length)],
                        type: 'arrow'
                    });

                var s = new sigma(
                  {
                      graph: g,
                      renderer: {
                          container: document.getElementById('graph-container'),
                          type: 'canvas'
                      },
                      settings: {
                          minArrowSize: 5,
                          doubleClickEnabled: false,
                          minEdgeSize: 0.5,
                          maxEdgeSize: 4,
                          enableEdgeHovering: true,
                          edgeHoverColor: 'edge',
                          defaultEdgeHoverColor: '#000',
                          edgeHoverSizeRatio: 1,
                          edgeHoverExtremities: true,
                      }
                  }
                );


                // Initialize the dragNodes plugin:
                var dragListener = sigma.plugins.dragNodes(s, s.renderers[0]);

                dragListener.bind('startdrag', function (event) {
                    console.log(event);
                });
                dragListener.bind('drag', function (event) {
                    console.log(event);
                });
                dragListener.bind('drop', function (event) {
                    console.log(event);
                });
                dragListener.bind('dragend', function (event) {
                    console.log(event);
                });

            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#message").removeClass().addClass("alert alert-danger");
                $("#message").html(xhr.statusText);
                $("#myModalMessage").modal('show');

            }
        });
    });
</script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="graph-container"></div>
    <script>


 
</script>
</asp:Content>
