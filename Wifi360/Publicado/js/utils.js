﻿function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;

}


function GetMessage(messageCode) {
    var message;
    var language = 'en';
    var cookie = readCookie("wifi360-language");
    if (cookie != null) {
        language = cookie;
    }
    else
        language = navigator.language.substr(0, 2);

    switch (language) {
        case 'es':
            switch (messageCode) {
                case '0x0000': message = "Operación en curso, por favor espere"; break;
                case '0x0001': message = "Error de conexión. Por favor intente de nuevo más tarde"; break;
                case '0x0002': message = "Usuario o contraseña incorrecta"; break;
                case '0x0003': message = "Usuario borrado correctamente"; break;
                case '0x0004': message = "Usuario actualizado correctamente"; break;
                case '0x0005': message = "Nombre de usuario no disponible, intente otro por favor"; break;
                case '0x0006': message = "El nombre de usuario y contraseña solo pueden contener los caracteres 0-9, A-Z y _@.:"; break;
                case '0x0007': message = "Cambios guardados correctamente"; break;
                case '0x0008': message = "Cargo borrado correctamente"; break;
                case '0x0009': message = "Tipo de acceso a internet creado correctamente"; break;
                case '0x0010': message = "Ya ha seleccionado un tipo de acceso por defecto para el fast Ticket. Por favor, desmarquelo, solo puede haber uno por defecto"; break;
                case '0x0011': message = "Todos los campos son obligatorios"; break;
                case '0x0012': message = "El valor máximo permitido para el Tiempo de Validez está excedido"; break;
                case '0x0013': message = "El valor máximo permitido para el Tiempo de Crédito está excedido"; break;
                case '0x0014': message = "El valor máximo permitido para el Ancho de banda de bajada está excedido"; break;
                case '0x0015': message = "El valor máximo permitido para el Ancho de banda de subida está excedido"; break;
                case '0x0016': message = "El valor máximo permitido para el Volumen de bajada está excedido"; break;
                case '0x0017': message = "El valor máximo permitido para el Volumen de subida está excedidod"; break;
                case '0x0018': message = "La habitación no ha sido seleccionada, por favor seleccione una"; break;
                case '0x0019': message = "No ha seleccionado tipo de acceso, por favor seleccione uno"; break;
                case '0x0020': message = "El nombre de usuario no puede tener más de 50 caractéres"; break;
                case '0x0021': message = "El nombre de usuario solo puede contener el formato XX:XX:XX:XX:XX:XX"; break;
                case '0x0022': message = "Las fechas no tienen el formato correcto"; break;
                case '0x0023': message = "El número máximo para crear usuarios es 3000"; break;
                case '0x0024': message = "Dispositivo eliminado de la lista negra"; break;
                case '0x0025': message = "Configuración salvada correctamente"; break;
                case '0x0026': message = "Localización salvada correctamente"; break;
                case '0x0027': message = "Texto salvado correctamente"; break;
                case '0x0028': message = "Prioridad salvada correctamente"; break;
                case '0x0029': message = "Filtrado salvado correctamente"; break;
                case '0x0030': message = "Filtrado borrado correctamente"; break;
                case '0x0031': message = "Pregunta guardada correctamente"; break;
                case '0x0032': message = "Pregunta borrada correctamente"; break;
                case '0x0033': message = "Las contraseñas no coinciden"; break;
                case '0x0034': message = "Contraseña cambiada correctamente"; break;
                case '0x0035': message = "La contraseña actual no coincide"; break;
                case '0x0036': message = "El valor máximo permitido para el Volumen combinado está excedido"; break;

                case '0x0040': message = "El valor mínimo para Ancho de Banda debe ser mayor que 0"; break;
                case '0x0041': message = "El valor mínimo para Volumen debe ser mayor que 0"; break;
                case '0x0042': message = "Longitud incorrecta por favor revise el formulario"; break;
                case '0x0043': message = "No es una cuenta de correo electrónica válida"; break;
                case '0x0044': message = "Formulario contiene campos con errores, por favor revíselo"; break;
                case '0x0045': message = "Longitud incorrecta en el campo Longitud nombre generación aleatoria sin prefijo"; break;
                case '0x0046': message = "Longitud incorrecta en el campo Longitud contraseña"; break;
                case '0x0047': message = "Longitud incorrecta en el campo Longitud nombre de vale generación aleatoria sin prefijo"; break;

                case '0x9999': message = "Se ha producido un error. Por favor intente de nuevo más tarde"; break;
            }
            break;
        default:
            switch (messageCode) {
                case '0x0000': message = "Operation in progress, please wait"; break;
                case '0x0001': message = "Connection error. Please try again later"; break;
                case '0x0002': message = "Wrong Username or Password"; break;
                case '0x0003': message = "User deleted successfully"; break;
                case '0x0004': message = "User updated successfully"; break;
                case '0x0005': message = "UserName not avalaible, Please select another one"; break;
                case '0x0006': message = "Username and password only can get characters 0-9, A-Z and _@.:"; break;
                case '0x0007': message = "Changes saved successfully"; break;
                case '0x0008': message = "Bill deleted successfully"; break;
                case '0x0009': message = "Access type successfully saved"; break;
                case '0x0010': message = "There is already a default fast access type selected. Please, select only one default access type"; break;
                case '0x0011': message = "All fields are mandatory"; break;
                case '0x0012': message = "The maximum value allowed for ValidTill is exceeded"; break;
                case '0x0013': message = "The maximum value allowed for Time Credit is exceeded"; break;
                case '0x0014': message = "The maximum value allowed for BandWidth Down is exceeded"; break;
                case '0x0015': message = "The maximum value allowed for BandWidth UP is exceeded"; break;
                case '0x0016': message = "The maximum value allowed for Volume Down is exceeded"; break;
                case '0x0017': message = "The maximum value allowed for Volume Up is exceeded"; break;
                case '0x0018': message = "Room field is mandatory, please select one"; break;
                case '0x0019': message = "Access Type field is mandatory, please select one"; break;
                case '0x0020': message = "UserName cannot be longer than 50 characters"; break;
                case '0x0021': message = "UserName can only contain the format XX:XX:XX:XX:XX:XX"; break;
                case '0x0022': message = "Invalid Date format, please check it"; break;
                case '0x0023': message = "The maximum number to create users is 3000"; break;
                case '0x0024': message = "Device removed from Black List"; break;
                case '0x0025': message = "Settings saved successfully"; break;
                case '0x0026': message = "Location saved successfully"; break;
                case '0x0027': message = "Text saved successfully"; break;
                case '0x0028': message = "Priority saved successfully"; break;
                case '0x0029': message = "Filter saved successfully"; break;
                case '0x0030': message = "Filter deleted successfully"; break;
                case '0x0031': message = "Question saved successfully"; break;
                case '0x0032': message = "Question deleted successfully"; break;
                case '0x0033': message = "Password do not match"; break;
                case '0x0034': message = "Password saved successfully"; break;
                case '0x0035': message = "Current password do not match"; break;
                case '0x0036': message = "The maximum value allowed for Aggregate Volume is exceeded"; break;

                case '0x0040': message = "The minimum value for BandWidth must be greater than 0"; break;
                case '0x0041': message = "The minimum value for Volumen must be greater than 0"; break;
                case '0x0042': message = "Incorrect length please check the form"; break;
                case '0x0043': message = "Incorrect mail format"; break;
                case '0x0044': message = "Form contains errors, please check it"; break;
                case '0x0045': message = "Incorrect length in the Random username length field"; break;
                case '0x0046': message = "Incorrect length in the Password length field"; break;
                case '0x0047': message = "Incorrect length in the Random Voucher username length field"; break;

                case '0x9999': message = "An error has occurred. Please try again later"; break;
            }
            break;


    }

    return message;
};
 
