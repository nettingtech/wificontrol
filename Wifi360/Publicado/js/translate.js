﻿$(document).ready(function () {

    function readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    };

    var cookie = readCookie("wifi360-language");
    if (cookie != null) {
        switch(cookie) {
        case  "es":
            //PRINCIPAL MENU
            $("#labelnewticket").html("Nuevo Ticket");
            $("#labelnewvoucher").html("Nuevo Vale");
            $("#labelmanagebillingtypes").html("Gestionar tipos de acceso");
            $("#labelbilling").html("Facturación");
            $("#labelusersmanagement").html("Gestión de usuarios");
            $("#labelvalidusers").html("Usuarios válidos");
            $("#labelonlineusers").html("Usuarios online");
            $("#labeldisabledusers").html("Usuarios deshabilitados");
            $("#labelfailedrequest").html("Errores de acceso");
            $("#labelbanneddevices").html("Dispositivos bloqueados");
            $("#labelstatistics").html("Analíticas");
            $("#labelclosedsessions").html("Sesiones cerradas");
            $("#labelinternetusage").html("Uso de internet");
            $("#labelloyalties").html("Fidelización");
            $("#labelregistrationdata").html("Datos de registro");
            $("#labelsettings").html("Configuración");
            $("#labelglobalsettings").html("Configuración general");
            $("#labellocations").html("Localizaciones");
            $("#labelPriorities").html("Prioridades / Politicas de grupo");
            $("#labelFilterProfiles").html("Perfiles de filtrado");
            $("#labelfacebookpromotions").html("Promociones de facebook");
            $("#labelsurvey").html("Encuesta");
            $("#changesite").html("Cambiar de sitio");
            $("#labeliot").html("Internet de las Cosas");
            $("#labelliiot").html("Accesos válidos");
            $("#labeliotonline").html("Dispositivos online");
            $("#labeliotdisabled").html("Accesos deshabilitados");
            $("#labelmanagevouchers").html("Gestión de vales");
            
            $("#labelAnalytics").html("Analíticas");
            $("#labelAnalytics2").html("<i class=\"fa fa-bar-chart\"></i> Analíticas");
            $("#labelonlineusers2").html("<i class=\"fa fa-users\"></i>  Usuarios online");

            //DASHBOARD
            $("#headeruserslast12hours").html("Dispositivos online en últimas 72 horas");
            $("#headeraverageuserslastweek").html("Media de dispositivos online por hora en últimas 72 horas");

            $("#headerdown24").html("Descargado 24H");
            $("#headerup24").html("Subido 24H");
            $("#headersesiones24").html("Sesiones 24H");
            $("#headerdevice24").html("Dispositivos 24H");
            $("#headerUsers24").html("Usuarios 24H");
            $("#headerRatio24H").html("Sesiones/Dispositivos 24H");
            $("#headerdown1").html("Descargado 1H");
            $("#headerup1").html("Subido 1H");
            $("#headersesiones1").html("Sesiones 1H");
            $("#headerdevice1").html("Dispositivos 1H");
            $("#headerUsers1").html("Usuarios 1H");
            $("#headerRatio1").html("Sesiones/Dispositivos 1H");

            $("#labelConnectionDuration").html("Duración de conexión de dispositivos online");
            $("#labelsitesModule").html("Dispositivos online por Módulo de Acceso");
            $("#labelsitesAccessType").html("Dispositivos online por Tipo de Acceso");
            $("#labelgroupMember").html("Dispositivos online por Miembro del Grupo");
            $("#labelsiteLocations").html("Dispositivos online por Zona");
            $("#labellasthour").html("Última hora");
            $("#labellast24hours").html("Últimas 24 horas");

            $("#headerusersmap").html("Mapa de usuarios online");

            //NEW TICKET

            $("#headerpagenewticket").html("Nuevo Ticket");
            $("#breadlabelnewticket").html("Nuevo Ticket");
            $("#labelboxheader").html("Nuevo Ticket<small> Selecciones los  parámetros</small>");
            $("#labelformroom").html("Habitación");
            $("#labelformlocation").html("Localización");
            $("#labelformbillingtype").html("Tipo de acceso");
            $("#labelboxadvancedparameters").html("Parámetros avanzados<small> Seleccione los parámetros avanzados para la creación de Tickets</small>");
            $("#labelformnumbertickets").html("Número de Tickets");
            $("#labelformusername").html("Nombre de usuario");
            $("#labelformpassword").html("Contraseña");
            $("#labelformemail").html("Correo electrónico");
            $("#labelformstart").html("Inicio de validez");
            $("#labelformend").html("Fin de validez");
            $("#buttoncreate").html("Crear nuevo");
            $("#labelformmac").html("Autenticación por MAC XX:XX:XX:XX:XX:XX");

            $("#labelprintusername").html("Nombre de usuario");
            $("#labelprintpassword").html("Contraseña");

            $("#headerprintparameters").html("Parámetros de  Impresion");
            $("#printlogo").html("Imprimir Logo");
            $("#printbandwidth").html("Imprimir Ancho de Banda");
            $("#printvolume").html("Imprimir volumen");
            $("#buttonprint").html("Imprimir");
            $("#buttonclose").html("Cerrar");

            //NEW VOUCHER

            $("#headerpagenewvoucher").html("Nuevo Vale");
            $("#breadlabelnewvoucher").html("Nuevo vale");
            $("#labelboxheadervoucher").html("Nuevo vale<small> Selecciones los  parámetros</small>");
            $("#labelformroomvoucher").html("Habitación");
            $("#labelformlocationvoucher").html("Localización");
            $("#labelformbillingtypevoucher").html("Tipo de acceso");
            $("#labelboxadvancedparametersvoucher").html("Parámetros avanzados<small> Seleccione los parámetros avanzados para la creación de vales</small>");
            $("#labelformnumberticketsvoucher").html("Número de vales");
            $("#labelformusernamevoucher").html("Nombre de usuario");
            $("#labelformemailvoucher").html("Correo electrónico");
            $("#labelformstartvoucher").html("Inicio de validez");
            $("#labelformendvoucher").html("Fin de validez");
            $("#buttoncreatevoucher").html(" Crear nuevo");
            $("#buttonexportvoucher").html(" Exportar");
            $("#labelformmacvoucher").html("Autenticación por MAC XX:XX:XX:XX:XX:XX");

            $("#labelprintusername").html("Nombre de usuario");

            $("#headerprintparameters").html("Parámetros de  Impresion");
            $("#printlogo").html("Imprimir Logo");
            $("#printbandwidth").html("Imprimir Ancho de Banda");
            $("#printvolume").html("Imprimir volumen");
            $("#buttonprint").html("Imprimir");
            $("#buttonclose").html("Cerrar");


            //MANAGEMENT ACCESS TYPES

            $("#headerpagemanagebillingtypes").html("Gestión de tipos de acceso");
            $("#breadlabelmanagebillingtypes").html("Gestión de tipos de acceso");
            $("#labelboxheaderbillingtypes").html("Tipos de acceso<small> Gestión de tipos de acceso a Internet</small>");
            $("#buttonnewbillingtype").html("Nuevo");
            $("#labelformbillingtypesdescription").html("Descripción");
            $("#labelformbillingtypesmodule").html("Módulo");
            $("#labelformbillingtypestatus").html("Estado");
            $("#labelformbillingtypeiot").html("Tipo de usuario");
            $("#status").find('option').remove().end();
            $("#status").append('<option value="1" selected="selected">Activo</option>');
            $("#status").append('<option value="0">Inactivo</option>');
            $("#status").find('option').remove().end();
            $("#status").append('<option value="true" selected="selected">Activo</option>');
            $("#status").append('<option value="false">Inactivo</option>');
            $("#iot").find('option').remove().end();
            $("#iot").append('<option value="0" selected="selected">Todos</option>');
            $("#iot").append('<option value="1">Dispositivos personales</option>');
            $("#iot").append('<option value="2">Internet de las Cosas</option>');
            
            $("#typeiotuser").find('option').remove().end();
            $("#typeiotuser").append('<option value="0">Todos los dispositivos</option>');
            $("#typeiotuser").append('<option value="1" selected="selected">Dispositivos Personales</option>');
            $("#typeiotuser").append('<option value="2">Dispositivos IoT</option>');

            $("#typeiot").find('option').remove().end();
            $("#typeiot").append('<option value="0">Todos los dispositivos</option>');
            $("#typeiot").append('<option value="1">Dispositivos Personales</option>');
            $("#typeiot").append('<option value="2" selected="selected">Dispositivos IoT</option>');

            //BILLING TYPE
            $("#headerpagebillingtype").html("Gestión de tipos de acceso");
            $("#breadlabelfatherbillingtype").html("Gestión de tipos de acceso");
            $("#breadlabelbillingtype").html("Tipo de acceso");
            $("#labelboxheaderbillingtype").html("Tipo de acceso<small> Detalles del tipo de acceso</small>");
            $("#labelformbillinglocation").html("Localización");
            $("#labelformbillingdescription").html("Descripción");
            $("#labelformbillingfolio").html("Descripción factura");
            $("#labelformbillingprice").html("Precio");
            $("#labelformbillingtimecredit").html("Tiempo de crédito por dispositivo");
            $("#labelformbillingvalidtill").html("Tiempo de validez");
            $("#labelformbillingvalidafterfirstuse").html("Tiempo de validez desde primer uso");
            $("#labelformbillingdevices").html("Máximo de dispositivos");
            $("#labelformbillingbandwidthdown").html("Ancho de banda bajada");
            $("#labelformbillingbandwidthup").html("Ancho de banda subida");
            $("#labelformbillingmodule").html("Módulo");
            $("#labelformbillingpriority").html("Prioridad / Politica de Grupo");
            $("#labelformbillingorder").html("Orden");
            $("#labelformbillingvolumedown").html("Volumen bajada por dispositivo");
            $("#labelformbillingvolumeup").html("Volumen subida por dispositivo");
            $("#labelformbillingurl").html("Url de llegada");
            $("#labelformbillingactive").html("Activo");
            $("#labelformbillingfastticket").html("Fast Ticket por defecto");
            $("#labelformbillingfilter").html("Filtrado de contenidos");
            $("#labelformbillingnextbillingtype").html("Siguiente tipo de acceso");
            $("#labelchecktimecredit").html("Valor máximo");
            $("#labelcheckminvolume").html("Valor máximo");
            $("#labelcheckmaxvolume").html("Valor máximo");
            $("#labelcheckmaxvolumecombined").html("Valor máximo");
            $("#labelformbillingvolumecombined").html("Volumen agregado por dispositivo");
            $("#labelformbillingiot").html("Internet de las Cosas");
            $("#headerbuttonsbillingtype").html("Acciones sobre el tipo de acceso");
            $("#labelbuttonsavebillingtype").html("Guardar");
            $("#labelbuttonbackbillingtype").html("Volver");
            $("#labelbuttonclosebillingtype").html("Cerrar");
            $("#unidadesvalidtill").find('option').remove().end();
            $("#unidadesvalidtill").append('<option value="h" selected="selected">horas</option>');
            $("#unidadesvalidtill").append('<option value="d">días</option>');
            $("#unidadestimecredit").find('option').remove().end();
            $("#unidadestimecredit").append('<option value="s" selected="selected">segundos</option>');
            $("#unidadestimecredit").append('<option value="h">horas</option>');
            $("#unidadestimecredit").append('<option value="d">días</option>');
            $("#unidadesvalidafterfirstuse").find('option').remove().end();
            $("#unidadesvalidafterfirstuse").append('<option value="h" selected="selected">horas</option>');
            $("#unidadesvalidafterfirstuse").append('<option value="d">días</option>');
            

            //BILLING
            $("#headerpagebilling").html("Facturación");
            $("#breadlabelbilling").html("Facturación");
            $("#labelboxheaderbilling").html("Facturación<small> Listado de ventas realizadas</small>");
            $("#labelformbillingdaterante").html("Rango de fechas");
            $("#labelformbillingdatefrom").html("Desde");
            $("#labelbillingsince").html("hasta");
            $("#labelformbillingroom").html("Habitación");
            $("#labelbuttonexportbilling").html("Exportar");
            $("#labelbuttonsearchbilling").html("Buscar");
            $("#deletebilltitle").html("Borrar factura");
            $("#messageDelete").html("¿Está seguro de borrar esta factura?");
            $("#buttonbacklabel").html("Volver");
            $("#buttondeletelabel").html("Borrar");
            $("#labelformbillinmombers").html("Miembros del grupo");

            //USERS MANAGEMENT
            $("#headerpageusersmanagement").html("Gestión de usuarios");
            $("#breadusersmanagement").html("Gestión de usuarios");
            $("#breadusersvalid").html("Usuarios válidos");
            $("#labelboxheadervalidusers").html("Usuarios válidos<small> Listado de usuarios activos en el sistema</small>");
            $("#headerboxfilterusersvalid").html("Filtros de búsqueda");
            $("#labelformusermanagementuser").html("Usuarios");
            $("#labelformusermanagementbillingtype").html("Tipo de acceso");
            $("#labelformusermanagementPriority").html("Prioridad / Politica de grupo");
            $("#labelformusermanagementfilterprofile").html("Filtrado de contenidos");
            $("#labelformusermanagementdaterange").html("Rango de fechas");
            $("#labelformusermanagementdatefrom").html("Desde");
            $("#labelformusermanagementdatesince").html("hasta");
            $("#labelformusermanagementroom").html("Habitación");
            $("#labelformusermanagementbuttonsearch").html("Buscar");

            //VOUCHERS MANAGEMENT
            $("#headerpagevouchersmanagement").html("Gestión de vales");
            $("#breadvouchersmanagement").html("Gestión de vales");
            $("#breadvouchersvalid").html("Vales");
            $("#labelboxheadervalidVouchers").html("Vales<small> Listado de vales en el sistema</small>");
            $("#headerboxfilterusersvalid").html("Filtros de búsqueda");
            $("#labelformusermanagementuser").html("Usuarios");
            $("#labelformusermanagementbillingtype").html("Tipo de acceso");
            $("#labelformusermanagementBillable").html("Usados");
            $("#labelformusermanagementEnabled").html("Estado");
            $("#labelformusermanagementdaterange").html("Rango de fechas");
            $("#labelformusermanagementdatefrom").html("Desde");
            $("#labelformusermanagementdatesince").html("hasta");
            $("#labelformusermanagementroom").html("Habitación");
            $("#labelformusermanagementbuttonsearch").html("Buscar");
            $("#billable").find('option').remove().end();
            $("#billable").append('<option value="0">Todos</option>');
            $("#billable").append('<option value="True">Usados</option>');
            $("#billable").append('<option value="False">Sin usar</option>');
            $("#enabled").find('option').remove().end();
            $("#enabled").append('<option value="0">Todos</option>');
            $("#enabled").append('<option value="True">Activos</option>');
            $("#enabled").append('<option value="False">Deshabilitados</option>');

            //USER
            $("#headerpageuser").html("Gestión de usuarios");
            $("#breadusersmanagement").html("Gestión de usuarios");
            $("#breaduser").html("Usuario");
            $("#labelboxheaderuser").html("Usuario<small> Detalles del usuario</small>");
            $("#headerboxactionsuser").html("Acciones");
            $("#labelbuttonsaveuser").html("Guardar");
            $("#labelbuttondeleteuser").html("Deshabilitar");
            $("#labelbuttonupgradeuser").html("Mejorar");
            $("#labelbuttonextenduser").html("Extender");
            $("#labelbuttonprintuser").html("Imprimir");
            $("#labelbuttonbackuser").html("Volver");

            $("#labelformuserroom").html("Habitación");
            $("#labelformuserusername").html("Nombre de usuario");
            $("#labelformuserpassword").html("Contraseña");
            $("#labelformuserbillingtype").html("Tipo de acceso");
            $("#labelformuserpriority").html("Prioridad");
            $("#labelformuserbwdown").html("Bandwidth Bajada");
            $("#labelformuserbwup").html("Bandwidth subida");
            $("#labelformuservolumedown").html("Volumen baja por dispositivo");
            $("#labelformuservolumeup").html("Volumen subida por dispositivo");
            $("#labelformuserdevice").html("Máximo de dispositivo");
            $("#labelformuserdatesince").html("Fecha de inicio");
            $("#labelformuserdatetill").html("Fecha de finalización");
            $("#labelformusercredittime").html("Crédito de tiempo");
            $("#labelformuserfilter").html("Filtrado de contenidos");
            $("#labelformusertotalused").html("Total usado");

            $("#headeruserupgrade").html("Mejorar usuario");
            $("#labelbuttonupgradeusersave").html("Guardar");
            $("#labelbuttonupgradeuserback").html("Volver");
            $("#labelupgradeuserabillingtype").html("Tipo de acceso");
            
            $("#headeruserextend").html("Extender usuario");
            $("#labelbuttonextendusersave").html("Guardar");
            $("#labelbuttonextenduserback").html("Volver");
            $("#labelextenduserabillingtype").html("Tipo de acceso");
            $("#labelbuttondetails").html(" Detalles");

            $("#div_user_conections_details").html("<h3>Detalles de las conexiones del usuario</h3> Resultados limitados a las últimas 30 sesiones");

            //ONLINE USERS
            $("#headerpageusersonline").html("Gestión de usuarios");
            $("#breadpagefatherusersonline").html("Gestión de usuarios");
            $("#breadpageusersonline").html("Usuarios online");
            $("#labelboxheaderusersonline").html("Usuarios Online<small> Lista de usuarios conectados en el sistema</small>");
            $("#buttonlabelrefreshusersonline").html("Actualizar");

            //DISABLED USERS
            $("#headerpageusersdisabled").html("Gestión de usuarios");
            $("#breadpagefatherusersdisabled").html("Gestión de usuarios");
            $("#breadpageusersdisabled").html("Usuarios Deshabilitados");
            $("#labelboxheaderusersdisabled").html("Usuarios deshabilitados<small> Lista de usuarios deshabilitados</small>");
            $("#headerboxfilterusersdisabled").html("Filtros de búsqueda");
            $("#labelformusersdisableduser").html("Usuarios");
            $("#labelforusersdisabledbillingtype").html("Tipo de acceso");
            $("#labelformusersdisableddaterange").html("Rango de fechas");
            $("#labelformusersdisableddatefrom").html("Desde");
            $("#labelformusersdisableddatesince").html("hasta");
            $("#labelformusersdisabledroom").html("Habitación");
            $("#labelformusersdisabledbuttonsearch").html("Buscar");

            //FAILED REQUEST
            $("#headerpagefailedrequest").html("Gestión de usuarios");
            $("#breadpagefatherfailedrequest").html("Gestión de usuarios");
            $("#breadpagefailedrequest").html("Errores de acceso");
            $("#labelboxheaderfailedrequest").html("Errores de acceso<small> Listado de accesos erróneos</small>");
            $("#headerboxfilterfailedrequest").html("Filtros de búsqueda");
            $("#labelforfailedrequestbillingtype").html("Rango de fechas");
            $("#labelformfailedrequestdatefrom").html("Desde");
            $("#labelformfailedrequestdatesince").html("hasta");
            $("#labelformfailedrequestbuttonsearch").html("Buscar");

            //BANNED DEVICES
            $("#headerpagebanneddevices").html("Gestión de usuarios");
            $("#breadpagefatherbanneddevices").html("Gestión de usuarios");
            $("#breadpagebanneddevices").html("Dispositivos bloqueados");
            $("#labelboxheaderbanneddevices").html("Dispositivos bloqueados<small> Listado de dispositivos bloqueados por el sistema</small>");
            $("#headerboxfilterbanneddevices").html("Filtros de búsqueda");
            $("#labelformbanneddevicesbillingtype").html("Rango de fechas");
            $("#labelformbanneddevicesdatefrom").html("Desde");
            $("#labelformbanneddevicesdatesince").html("hasta");
            $("#labelformbanneddevicesbuttonsearch").html("Buscar");

            //CLOSSED SESSIONS
            $("#headerpageusagestatistics").html("Analíticas");
            $("#breadpagefatherusagestatistics").html("Analíticas");
            $("#breadpageusagestatistics").html("Sesiones cerradas");
            $("#labelboxheaderusagestatistics").html("Sesiones cerradas");

            $("#headerboxfilterusagestatistics").html("Filtros de búsqueda");
            $("#labelformusagestatisticsbillingtype").html("Rango de fechas");
            $("#labelformusagestatisticsdatefrom").html("Desde");
            $("#labelformusagestatisticsdatesince").html("hasta");
            $("#labelformusagestatisticsusername").html("Nombre de usuario");
            $("#labelformusagestatisticsbuttonsearch").html("Buscar");

            //IOT
            $("#headerpageiot").html("Internet de las Cosas");
            $("#breaduseriot").html("Internet de las Cosas");
            $("#breaduseriot").html("Accesos válidos");
            $("#breadusersvalidiot").html("Accesos válidos");
            $("#labelboxheadervalidusersiot").html("Accesos válidos<small> Listado de accesos válidos en el sistema</small>");

            //IOT ONLINE USERS
            $("#headerpageusersonlineiot").html("Internet de las Cosas");
            $("#breadpagefatherusersonlineiot").html("Internet de las Cosas");
            $("#breaduseriot").html("Accesos válidos");
            $("#breadpageusersonlineiot").html("Dispositivos online");
            $("#labelboxheaderusersonlineoit").html("Dispositivos online<small> Listado de dispositivos conectados en el sistema</small>");

            //IOT DISABLED USERS
            $("#headerpageusersdisablediot").html("Internet de las Cosas");
            $("#breadpagefatherusersdisablediot").html("Internet de las Cosas");
            $("#breadpageusersdisablediot").html("Accesos deshabilidatos");
            $("#labelboxheaderusersdisablediot").html("Accesos deshabilitados<small> Listado de accesos deshabilitados</small>");

            //INTERNET USAGE
            $("#headerpageinternetusage").html("Analíticas");
            $("#breadpagefatherinternetusage").html("Analíticas");
            $("#breadpageinternetusage").html("Uso de internet");
            $("#labelboxheaderinternetusage").html("Uso de internet");

            $("#headerboxfilterinternetusage").html("Filtros de búsqueda");
            $("#labelforminternetusagebillingtype").html("Rango de fechas");
            $("#labelforminternetusagedatefrom").html("Desde");
            $("#labelforminternetusagedatesince").html("hasta");
            $("#labelforminternetusageusername").html("Nombre de usuario");
            $("#labelforminternetusagebuttonsearch").html("Buscar");

            //LOYALTIES
            $("#headerpageloyalties").html("Fidelización");
            $("#breadpageloyalties").html("Fidelización");
            $("#labelboxheaderloyalties").html("Datos de registro<small> Datos de los usuarios registrados</small>");

            $("#headerboxfilterloyalties").html("Filtros de búsqueda");
            $("#labelformloyaltiesbillingtype").html("Rango de fechas");
            $("#labelformloyaltiesdatefrom").html("Desde");
            $("#labelformloyaltiesdatesince").html("hasta");
            $("#labelformloyaltiesbuttonsearch").html("Buscar");
            $("#labelformloyaltiesbuttonexport").html("Exportar");

            //CONFIGURACION
            $("#headerpagesettings").html("Configuración");
            $("#breadpagefathersettings").html("Configuración");
            $("#breadpagesettings").html("Configuración general");
            $("#labelboxheadersettings").html("Configuración general");
            $("#labelboxheaderbuttonsettings").html("Acciones sobre la configuración");
            $("#labelboxheaderbuttonsettingsNewSite").html("Acciones");
            $("#labelsettingsbuttonsave").html("Guardar");
            $("#labelsettingsbuttonback").html("Volver");

            $("#labelacordionheaderbasic").html("Configuración básica");
            $("#labelacordionheaderticket").html("Configuración del Ticket / Vale");
            $("#labelacordionheaderline").html("Configuración de la línea");
            $("#labelacordionheadermodule").html("Configuración de los módulos de acceso");
            $("#labelacordionheaderfreeaccess").html("Configuración del accesso gratuito");
            $("#labelacordionheaderpaypal").html("Configuración de Paypal");
            $("#labelacordionheadersecurity").html("Configuración de seguridad");
            $("#labelacordionheaderemail").html("Configuración del correo electrónico");
            $("#labelacordionheadersurvey").html("Configuración de la encuesta");
            $("#labelacordionheadersocialnetworks").html("Redes sociales");

            $("#labelsettingsformname").html("Nombre");
            $("#labelsettingsformgtm").html("Hora GMT");
            $("#labelsettingsformlanding").html("Url de llegada");
            $("#labelsettingsformlanguage").html("Idioma por defecto");
            $("#labelsettingsformreauthenticate").html("Reautenticación CLOUD");
            $("#labelsettingsformvendor").html("Fabricante");
            $("#labelsettingsformfiltercontents").html("Filtrado de contenidos  (Mikrotik)");
            $("#labelsettingsformvolumecombined").html("Limitación de volumen agregada");

            $("#labelsettingsformprefix").html("Prefijo nombre usuario");
            $("#labelsettingsformlongusername").html("Longitud nombre generación aleatoria sin prefijo (min. 3)");
            $("#labelsettingsformvoucherprefix").html("Prefijo nombre usuario de vale");
            $("#labelsettingsformvoucherlongusername").html("Longitud nombre de vale generación aleatoria sin prefijo (min. 3)");
            $("#labelsettingsformlongpass").html("Longitud contraseña (min. 3)");
            $("#labelsettingsformprintlogo").html("Imprimir logotipo");
            $("#labelsettingsformprintspeed").html("Imprimir velocidad de conexión");
            $("#labelsettingsformprintvolume").html("Imprimir volumen");

            $("#labelsettingsformip").html("IP Principal");
            $("#labelsettingsformbandwidthdown").html("Ancho de banda de bajada");
            $("#labelsettingsformbandwidthup").html("Ancho de banda de subida");

            $("#labelsettingsformloginmodule").html("Módulo de acceso por Ticket");
            $("#labelsettingsformfreeaccessmodule").html("Módulo de acceso gratuito");
            $("#labelsettingsformpremiummodule").html("Módulo de acceso premium");
            $("#labelsettingsformsocialaccessmodule").html("Módulo de acceso social");
            $("#labelsettingsformcustommodule").html("Módulo de acceso personalizado");
            $("#labelsettingsformvouchermodule").html("Módulo de acceso por vale");
            

            $("#labelsettingsformfreeaccesstime").html("Tiempo entre accesos gratuitos");

            $("#labelsettingsformpaypalusername").html("Usuario de PayPal");
            $("#labelsettingsformpaypalcurrency").html("Código de moneda");
            $("#labelsettingsformpaypalurl").html("Url de retorno desde Paypal");

            $("#labelsettingsformmacbanned").html("Bloqueo por MAC");
            $("#labelsettingsformbannedtime").html("Tiempo de bloqueo");
            $("#labelsettingsformmaxattemps").html("Número máximo de intentos por MAC");
            $("#labelsettingsformtimeattemps").html("Intervalo de intentos por MAC");

            $("#labelsettingsformmailsender").html("Remitente");
            $("#labelsettingsformmailserver").html("Servidor SMTP");
            $("#labelsettingsformmailport").html("Puerto SMTP");
            $("#labelsettingsformmailssl").html("Cifrado SSL");
            $("#labelsettingsformmailuser").html("Usuario SMTP");
            $("#labelsettingsformmailpass").html("Contraseña SMTP");

            $("#labelacordionheaderdaypass").html("Contraseña del Sitio");
            $("#labelenabledDaylyPass").html("Activar Contraseña del sitio");
            $("#labelvalueDaylyPass").html("Contraseña del sitio");

            $("#labelsettingsformmandatorysurvey").html("Obligatoria en accesos gratuitos");

            $("#labelacordionheaderdaypass").html("Contraseña del sitio");
            

            //LOCATIONS
            $("#headerpagelocationsettings").html("Configuración");
            $("#breadpagefatherlocationsettings").html("Configuración");
            $("#breadpagelocationsettings").html("Localizaciones");
            $("#labelboxheaderlocationsettings").html("Localizaciones <small>Gestión de localizaciones</small>");

            //LOCATION
            $("#headerpagelocationsetting").html("Configuración");
            $("#breadpagefatherlocationsetting").html("Configuración");
            $("#breadpagelocationsetting").html("Localizaciones");
            $("#breadpagelocationsettingactive").html("Configuración de la localización");

            $("#labelboxheaderlocationsetting").html("Configuración de la localización<small> Detalles de configuración de una localización</small>");

            $("#labelheadergeneraltext").html("Textos generales de la localización");
            $("#labelheadergeneraltextmodule").html("Textos por módulos");
            $("#labelheadergeneraldisclaimer").html("Avisos legales");
            $("#labelheadergeneralsurvey").html("Encuesta");
            $("#labelboxheaderacitonlocationsetting").html("Acciones sobre la localización");
            $("#labelformlocationsettingsbuttonsave").html("Guardar");
            $("#labelformlocationsettingsbuttonback").html("Volver");
            $("#labelformlocationsettingsbuttondelete").html("Borrar");

            $("#labellocationsettingsformname").html("Localización");
            $("#labellocationsettingsformmodule").html("Módulo por defecto");
            $("#labellocationsettingsformdisclaimer").html("Aviso legal obligatorio");
            $("#labellocationsettingsformsurvey").html("Encuesta obligatoria");
            $("#labellocationsettingsformdealysurvey").html("Retardo de encuesta desde ultima conexión");
            
            $("#labellocationsettingsformlanding").html("Url de llegada");
            $("#labellocationsettingsformlocationid").html("Identificador por dispositivo");
            $("#labellocationsettingsformdefault").html("Establecer ubicación por defecto");
            $("#labellocationsettingsformexchange").html("Cambio respecto a");
            $("#labellocationsettingsformlatitude").html("Latitud");
            $("#labelsettingsformlongitude").html("Longitud");

            $("#unidadesdelaysurvey").find('option').remove().end();
            $("#unidadesdelaysurvey").append('<option value="h" selected="selected">horas</option>');
            $("#unidadesdelaysurvey").append('<option value="d">días</option>');

            
            

            //LOCATION GENERAL TEXT
            $("#headerpagelocationsetting").html("Configuración");
            $("#breadpagefatherlocationsetting").html("Configuración");
            $("#breadpagelocationsetting").html("Localizaciones");
            $("#localizacionpadre").html("Localizaciones");
            $("#breadpagelocationsettingactive").html("Texto General de la localización");
            $("#labelboxheaderlocationsettingtext").html("Texto general de la localización");


            $("#labelboxheaderacitonlocationsettingtext").html("Acciones sobre el texto");
            $("#labelformlocationsettingsbuttonsave").html("Guardar");
            $("#labelformlocationsettingsbuttonback").html("Volver");

            $("#labellocationsettingstextformlanguage").html("Idioma");
            $("#labellocationsettingstextformtext").html("Texto");


            //LOCATION MODULE TEXT

            $("#breadpagelocationmoduletextsettingactive").html("Texto del módulo y localización");
            $("#labelboxheaderlocationsettingtextmodule").html("Texto de módulo y localización<small> Texto a mostrar en cada localización y del módulo seleccionado.</small>");
            $("#labelboxheaderacitonlocationsettingtext").html("Acciones sobre el texto");
            $("#labelformlocationsettingsbuttonsave").html("Guardar");
            $("#labelformlocationsettingsbuttonback").html("Volver");

            $("#labellocationsettingstextformmodule").html("Módulo");

            //DISCLAIMER
            $("#breadpagelocationdisclaimersettingactive").html("Aviso Legal");
            $("#labelboxheaderlocationsettingdisclaimer").html("Aviso legal<small> Aviso legal a mostrar en cada localización.</small>");

            $("#labellocationsettingstextformcreatedate").html("Fecha de creación");
            $("#labelboxheaderacitonlocationdisclaimer").html("Acciones sobre el aviso legal");
            
            //FACEBOOK PROMOTIONS
            $("#breadpagefacebooksetting").html("Promociones de Facebook");
            $("#labelboxheaderfacebookpromotions").html("Promociones de Facebooks<small> Gestión de las configuraciones de facebook</small>");

            $("#labelformfacebookpromotionbuttonnew").html("Nueva");

            //FACEBOOK PROMOTION
            $("#breadpagefacebooksettingactive").html("Promoción de Facebook");
            $("#labelboxheaderfacebookpromotion").html("Promoción de Facebook<small> Detalles de configuración de la promoción</small>");

            $("#labelformfacebookpromotionpagename").html("Nombre de la página");
            $("#labelformfacebookpromotionpageurl").html("Url de la página");
            $("#labelformfacebookpromotionpageid").html("Identificador de la página");
            $("#labelformfacebookpromotiondatestart").html("Fecha de inicio");
            $("#labelformfacebookpromotiondateend").html("Fecha de fin");
            $("#labelformfacebookpromotionvaliddays").html("Días de validez");
            $("#labelformfacebookpromotionlocation").html("Localización");
            $("#labelformfacebookpromotionaccesstype").html("Tipo de acceso");
            $("#labelformfacebookpromotionorder").html("Orden");

            $("#labelboxheaderacitonfacebookpromotion").html("Acciones sobre la promoción");
            $("#labelformlocationsettingsbuttonsave").html("Guardar");
            $("#labelformlocationsettingsbuttonback").html("Volver");

            //PRIORITIES

            $("#breadpageprioritiessettings").html("Prioridades / Políticas de grupo");
            $("#labelboxheaderprioritiessettings").html("Listado de prioridades / Políticas de grupo<small> Gestión de prioridades</small>");

            //PRIORITY
            $("#breadpageprioritysetting").html("Prioridad / Politica de grupo");
            $("#breadpageprioritysettingactive").html("Configuración de la prioridad / política de grupo");

            $("#labelboxheaderprioritysetting").html("Configuración de la prioridad / política de grupo<small> Detalles de configuración de una prioridad / política de grupo</small>");
            $("#labelboxheaderacitonprioritysetting").html("Acciones sobre la prioridad / política de grupo");
            $("#labelprioritysettingsformname").html("Descripción");

            $("#deleteprioritytitle").html("Borrar prioridad / política de grupo");
            $("#messagepriorityDelete").html("¿Está seguro de borrar este elemento?");

            //FILTER PROFILES

            $("#breadpagefilterprofilessettings").html("Perfiles de filtrado de contenidos");
            $("#labelboxheaderfilterprofiles").html("Listado de perfiles de filtrado de contenidos<small> Gestión de perfiles</small>");

            //PRIORITY
            $("#breadpagefilterprofilessetting").html("Perfiles de filtrado de contenidos");
            $("#breadpagefilterprofilessettingactive").html("Configuración del perfil de filtrado de contenidos");

            $("#labelboxheaderfilterprofilesetting").html("Configuración del perfil de filtrado de contenidos<small> Detalles del perfil</small>");
            $("#labelboxheaderacitonfilterprofilesetting").html("Acciones sobre el perfil");
            $("#labelprioritysettingsformname").html("Descripción");

            $("#deletefilterprofiletitle").html("Borrar perfil de filtrado de contenidos");
            $("#messagepriorityDelete").html("¿Está seguro de borrar este elemento?");

            //SURVEY

            $("#breadpagesurveysetting").html("Configuración de las encuestas");
            $("#breadpagesurveyquestion").html("Configuración de la pregunta");
            $("#labelboxheadersurvey").html("Configuración de la encuesta<small> Gestión y configuración de las preguntas de la encuesta.</small>");
            $("#labelformsurveybuttonnew").html("Nueva");

            //SURVEY QUESTION
            $("#labelboxheaderacitonfacebookpromotion").html("Acciones sobre la promoción");
            $("#labelboxheaderquestion").html("Configuración de pregunta<small> Detalles de configuración de una pregunta de la encuesta</small>");

            $("#labelboxheaderactionquestion").html("Acciones sobre la pregunta");
            $("#labelformquestionsettingsbuttondelete").html("Borrar");

            $("#labelformquestionlanguage").html("Idioma");
            $("#labelformquestionquestion").html("Pregunta");
            $("#labelformquestiontype").html("Tipo de pregunta");
            $("#labelformquestionvalues").html("Valores");
            $("#labelformquestionorder").html("Orden");

            //LOG
            $("#labelboxheaderlog").html("Log<small> Listado de acciones en el sistema</small>");
            $("#labelformlogdaterange").html("Rango de fechas");
            $("#labelformlogdatefrom").html("desde");
            $("#labelformlogdatesince").html("hasta");
            $("#labelformlogbuttonsearch").html("Buscar");

            //CHANGE SITE

            $("#breadpagechangesite").html("Cambiar de sitio");
            $("#labelboxheaderchangesite").html("Cambiar el sitio<small> Cambiar el sitio a gestionar</small>");
            $("#labelformchangesites").html("Sitios");
            $("#labelformlocations").html("Localizaciones");
            $("#labelboxheaderactionchangesite").html("Acciones");
            $("#labelformchangesitebuttonchange").html("Cambiar");


            //ANALYTICS
            $("#headerpageAnalytics").html("Analíticas");
            $("#breadheadAnalytics").html("Analíticas");
            $("#breadAnalytics").html("Analíticas");
            $("#labelboxheaderanalytics").html("Analíticas <small> Analíticas en el intervalo</small>");

            $("#headeronlineUsersInterval").html("Dispositivos online");
            $("#headeronlineUsersPerHourInterval").html("Media de dispositivos online por hora");
            $("#headerconnectioDuractionInterval").html("Duración de las sesiones");

            $("#headercol1groupinterval").html("Sesiones por Miembro del Grupo");
            $("#headercol1siteinterval").html("Sesiones por zona");
            $("#headercol3siteinterval").html("Sesiones por Tipo de Acceso");
            $("#headercol4siteinterval").html("Sesiones por Módulo de Acceso");

            $("#headerdowninterval").html("Descargado");
            $("#headerupinterval").html("Subido");
            $("#headersessionInterval").html("Sesiones");
            $("#headerdeviceInterval").html("Dispositivos");
            $("#headerUsersInterval").html("Usuarios");
            $("#headerRatioInterval").html("Sesiones/Dispositivos");

            $("#labelformusermanagementseller").html("Vendedores");
            

            //MANAGEMENTS
            $("#breadlabelmanagegroup").html("Gestión de Grupos");
            $("#labelboxheadermanagement").html("Gestión de Grupos");
            $("#ButtonNewGroup").html("Nuevo Grupo");
            $("#labelboxheadermanagementusers").html("Edición del grupo");
            $("#labelnewuser_form_name").html("Nombre del grupo");
            $("#form_Family").html("Familia");
            $("#from_RealmShare").html("");
            $("#labelnewgroups_form_name").html("Seleccione grupos");
            $("#labelnewsites_form_name").html("Seleccione sites");
            $("#labelnewlocations_form_name").html("Seleccione localizaciones");
            $("#label_createG_form_buttonclone").html("Guardar grupo");
            $("#label_createG_form_buttonback").html("Volver");
            $("#table_edit").html("Editar");
            $("#table_Groups").html("Grupos");
            $("#headerpagemanagesites").html("Gestión de Sites");
            $("#breadlabelmanagesites").html("Gestión de Sites");
            $("#labelboxheadermanagement").html("Gestión de Sites");
            $("#buttonnewsite").html("Nuevo Site");
            $("#labelform_management_description").html("Nombre del Site");
            $("#formCloneOption").html("Opciones de Clonado");
            $("#labelnewsite_form_name").html("Nombre del nuevo Site");
            $("#label_CloneBillingtypes").html("Clonar Tipos de Cuenta");
            $("#label_Clone_Users").html("Clonar usuarios");
            $("#Label_Clone_Billings").html("Clonar cuentas");            
            $("#label_Clone_loyalties").html("Clonar loyalties");
            $("#label_Clone_Closed_Sessions").html("Clonar Sessiones terminadas");
            $("#label_Clone_Locations_Identifier").html("Clonar identificador de localizaciones");
            $("#label_Clone_Meraki_LocationsGPS").html("Clonar Localización GPS de Meraki");
            $("#breadpanewsite").html("Nuevo Site");
            $("#labelboxheadernewsite").html("Nuevo Site");
            $("#formnewsitedescriptionloc").html("Introduce las diferentes localizaciones que quieres crear. Ten en cuenta que se crearán por defecto las localizaciones 'All_Zones' y 'Internal'");
            $("#labelnewlocation_names").html("Nombres");
            $("#labelacordionheaderText").html("Prioridades de las localizaciones");
            $("#label_newsite_form_buttoncreate").html("Crear Grupo");
            $("#labelacordionheaderLocation").html("Añadir localizaciones");
            $("#labelPriorities").html("Prioridades");
            $("#breadlabelmanageusers").html("Gestión de usuarios");
            $("#labelboxheadermanagementusers").html("Gestión de usuarios");
            $("#ButtonNewUser").html("Nuevo usuario");
            $("#headerpagemanageusers").html("Gestión de usuarios Dashboard");
            $("#breadlabelmanageuser").html("Gestión de usuarios");
            $("#labelboxheadermanagementusersdet").html("Edición del usuario");
            $("#labelnewuser_form_name").html("Nombre del usuario Dashboard");
            $("#labelnewuser_form_fullname").html("Nombre completo");
            $("#labelnewuser_form_userrol").html("Selecciona Rol");
            $("#labelnewuser_form_usertype").html("Selecciona Tipo de usuario");
            $("#labelnewuser_form_useroptions").html("Seleccina Opcion");
            $("#buttonsaveuser").html("Guardar usuario");
            /*$("#").html("");
            $("#").html("");
            $("#").html("");
            $("#").html("");
            $("#").html("");
            $("#").html("");
            $("#").html("");
            $("#").html("");
            $("#").html("");
            $("#").html("");*/
            break;


    }
    }

});