﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Wifi360._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src='https://api.mapbox.com/mapbox.js/v3.1.1/mapbox.js'></script>
    <link href='https://api.mapbox.com/mapbox.js/v3.1.1/mapbox.css' rel='stylesheet' />

    <script src='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v1.0.0/leaflet.markercluster.js'></script>
    <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v1.0.0/MarkerCluster.css' rel='stylesheet' />
    <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v1.0.0/MarkerCluster.Default.css' rel='stylesheet' />
    <script src='https://api.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v1.0.1/Leaflet.fullscreen.min.js'></script>
    <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v1.0.1/leaflet.fullscreen.css' rel='stylesheet' />

    <link href="css/calendar.css" rel="stylesheet" />
    <script src="js/calendar.full.min.js"></script>
    <script src="js/moment.js"></script>

    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/pie.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>
    <script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>

    <style>
        body { margin:0; padding:0; }
        #map { position:absolute; top:70px; bottom:0; }

        #pie_top_left, #pie_top_center, #pie_top_right{
          width: 100%;
          height: 350px;
}
        #chartdiv4, #chartdiv5  {
          width: 100%;
          height: 450px;
}

    </style>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5 id="headeruserslast12hours">Online devices during last 72 hours </h5>
                    </div>
                    <div class="ibox-content">
                        <div id="chartdiv4"></div>				
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5 id="headeraverageuserslastweek">Average online devices by hour during last 72 hours</h5>
                    </div>
                    <div class="ibox-content">
                        <div id="chartdiv5"></div>		
                    </div>
            </div>
            </div>


                <div class="col-lg-4 col-sm-4" id="col1">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5 id="labelConnectionDuration">Online devices connection time</h5>
                        </div>
                        <div class="ibox-content">
                                <div style="text-align:center;" >
                                    <div id="pie_top_left"></div>
                            </div>
                        </div>                                
                    </div>
                        </div>
                        <div class="col-lg-4 col-sm-4" id="col2">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5 id="labelsitesAccessType" style="display:none;">Online devices by Access Type</h5>
                            <h5 id="labelsitesModule" style="display:none;">Online devices by Access Module</h5>
                        </div>
                        <div class="ibox-content">
                            <div  style="text-align:center;">
                                <div id="pie_top_center"></div>
                            </div>
                        </div>                                
                    </div>
                        </div>
                        <div class="col-lg-4 col-sm-4" id="col3">

                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5 id="labelgroupMember" style="display:none;">Online devices by Group Member</h5>
                            <h5 id="labelsiteLocations" style="display:none;">Online devices by Zone</h5>
                        </div>
                        <div class="ibox-content">
                                <div  style="text-align:center;">
                                    <div id="pie_top_right"></div>
                                </div>
                        </div>                                
                        </div>
                    </div>

              

            <div class="col-lg-12 col-md-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5 id="labellast24hours">Last 24 Hours</h5>
                    </div>
                    <div class="ibox-content text-center" id="last24content">
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="widget style1 lazur-bg  text-center">
                                    <div class="m-b-md">
                                        <h5 id="headerdown24"> Download 24H</h5>
                                        <h3 class="font-bold m-xs" id="down24">. </h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="widget style1 lazur-bg  text-center">
                                <div class="m-b-md">
                                    <h5 id="headerup24"> Upload 24H </h5>
                                    <h3 class="font-bold m-xs" id="up24"> .</h3> 
                                </div>
                            </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="widget style1 lazur-bg  text-center">
                        <div class="m-b-md">
                            <h5 id="headersesiones24">Sessions 24H </h5>
                            <h3 class="font-bold m-xs" id="sesion24">. </h3>
                        </div>
                    </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="widget style1 lazur-bg  text-center">
                            <div class="m-b-md">
                                <h5 id="headerdevice24">Devices 24H</h5>
                                <h3 class="font-bold m-xs" id="device24">.</h3>
                            </div>
                        </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="widget style1 lazur-bg  text-center">
                            <div class="m-b-md">
                                <h5 id="headerUsers24">Users 24H</h5>
                                <h3 class="font-bold m-xs" id="users24"> .</h3>
                            </div>
                        </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="widget style1 lazur-bg  text-center">
                            <div class="m-b-md">
                                <h5 id="headerRatio24H">Sessions/Devices 24H </h5>
                                <h3 class="font-bold m-xs" id="SesionDevice24">. </h3>
                            </div>
                        </div>
                            </div>
                        </div>
                </div>
            </div>
            </div>
            <div class="col-lg-12 col-md-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5 id="labellasthour">Last Hour</h5>
                    </div>
                    <div class="ibox-content text-center">
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="widget style1 blue-bg  text-center">
                            <div class="m-b-md">
                                <h5 id="headerdown1"> Download 1H</h5>
                                <h3 class="font-bold m-xs" id="down1">.</h3>

                            </div>
                        </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="widget style1 blue-bg  text-center">
                                    <div class="m-b-md">
                                        <h5 id="headerup1"> Upload 1H</h5>
                                        <h3 class="font-bold m-xs" id="up1">.</h3> 
                                        
                                    </div>
                        </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="widget style1 blue-bg  text-center">
                            <div class="m-b-md">
                                <h5 id="headersesiones1">Sessions 1H </h5>
                                <h3 class="font-bold m-xs" id="sesion1">.</h3>
                            </div>
                        </div>
                            </div>
                            <div class="col-sm-2">                 
                                <div class="widget style1 blue-bg  text-center">
                                    <div class="m-b-md">
                                        <h5 id="headerdevice1">Devices 1H</h5>
                                        <h3 class="font-bold m-xs" id="device1">.</h3>

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="widget style1 blue-bg  text-center">
                                    <div class="m-b-md">
                                        <h5 id="headerUsers1">Users 1H</h5>
                                        <h3 class="font-bold m-xs" id="users1">.</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="widget style1 blue-bg  text-center">
                            <div class="m-b-md">
                                <h5 id="headerRatio1">Sessions/Devices 1H </h5>
                                <h3 class="font-bold m-xs" id="SesionDevice1">.</h3>

                            </div>
                        </div>      
                            </div>
                        </div>
                    </div>
                </div>
        </div>

              <div class="col-lg-12 col-md-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5 id="headerusersmap">Online Users Map</h5>
                    </div>
                    <div class="ibox-content" style="height:500px;" id="map_container">
                        <div id="mapcontainer">
                            <div id="map" style="height:400px;"></div>
                        </div>
                    </div>
                </div>
            </div>

                    </div>
                   
  
</div>
    </div>
</div>
    
    <script>
        function readCookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        }

        function dashboard(e) {

            var cookieSite = readCookie("site");
            var cookieGroup = readCookie("group");
            var cookieLocation = readCookie("location");

            $("#chartdiv5").height(9 * $("#chartdiv5").width() / 16);
            $("#chartdiv4").height(9 * $("#chartdiv4").width() / 16);

            if (cookieSite != null) //estamos en un site
            {
                $("#labelsitesAccessType").show();
                $("#labelsitesModule").hide();
                $("#labelsiteLocations").show();
                $("#labelgroupMember").hide();
                var dataString = 'action=USERSBYBILLINGTYPELIVE';
                $.ajax({
                    url: "handlers/BigDataHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $("#myModalWait").modal('show');
                    },
                    complete: function () {
                        $("#myModalWait").modal('hide');
                    },
                    success: function (pReturn) {
                        var chart = AmCharts.makeChart("pie_top_center", {
                            "type": "pie",
                            "theme": "light",
                            "labelText": "[[percents]]%",
                            "dataProvider": pReturn.items,
                            "valueField": "value",
                            "titleField": "label",
                            "balloon": {
                                "adjustBorderColor": true,
                                "color": "#000000",
                                "cornerRadius": 5,
                                "fillColor": "#FFFFFF"
                            },
                            "angle": 15,
                            "startEffect": "easeOutSine",
                            "startDuration": 2,
                            "labelRadius": 15,
                            "innerRadius": "10%",
                            "depth3D": 10,
                            hideCredits: true,
                            "export": {
                                "enabled": true
                            }
                        });

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#message").removeClass().addClass("alert alert-danger");
                        $("#message").html(xhr.statusText);
                        $("#myModalMessage").modal('show');

                    }
                });

                var dataString = 'action=USERSBYZONELIVE';
                $.ajax({
                    url: "handlers/BigDataHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $("#myModalWait").modal('show');
                    },
                    complete: function () {
                        $("#myModalWait").modal('hide');
                    },
                    success: function (pReturn) {
                        var chart = AmCharts.makeChart("pie_top_right", {
                            "type": "pie",
                            "theme": "light",
                            "labelText": "[[percents]]%",
                            "dataProvider": pReturn.items,
                            "valueField": "value",
                            "titleField": "label",
                            "balloon": {
                                "adjustBorderColor": true,
                                "color": "#000000",
                                "cornerRadius": 5,
                                "fillColor": "#FFFFFF"
                            },
                            "angle": 15,
                            "startEffect": "easeOutSine",
                            "startDuration": 2,
                            "labelRadius": 15,
                            "innerRadius": "10%",
                            "depth3D": 10,
                            hideCredits: true,
                            "export": {
                                "enabled": true
                            }

                        });
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#message").removeClass().addClass("alert alert-danger");
                        $("#message").html(xhr.statusText);
                        $("#myModalMessage").modal('show');

                    }
                });

            }
            else if (cookieGroup != null) {

                $("#labelsitesAccessType").hide();
                $("#labelsitesModule").show();
                $("#labelsiteLocations").hide();
                $("#labelgroupMember").show();

                var dataString = 'action=ONLINEUSERSBYSITELIVE';
                $.ajax({
                    url: "handlers/BigDataHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $("#myModalWait").modal('show');
                    },
                    complete: function () {
                        $("#myModalWait").modal('hide');
                    },
                    success: function (pReturn) {
                        var chart = AmCharts.makeChart("pie_top_right", {
                            "type": "pie",
                            "theme": "light",
                            "labelText": "[[percents]]%",
                            "dataProvider": pReturn.items,
                            "valueField": "value",
                            "titleField": "label",
                            "balloon": {
                                "adjustBorderColor": true,
                                "color": "#000000",
                                "cornerRadius": 5,
                                "fillColor": "#FFFFFF"
                            },
                            "angle": 15,
                            "startEffect": "easeOutSine",
                            "startDuration": 2,
                            "labelRadius": 15,
                            "innerRadius": "10%",
                            "depth3D": 10,
                            hideCredits: true,
                            "export": {
                                "enabled": true
                            }
                        });
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#message").removeClass().addClass("alert alert-danger");
                        $("#message").html(xhr.statusText);
                        $("#myModalMessage").modal('show');

                    }
                });

                var dataString = 'action=CONNECTIONBYMODULELIVE';
                $.ajax({
                    url: "handlers/BigDataHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $("#myModalWait").modal('show');
                    },
                    complete: function () {
                        $("#myModalWait").modal('hide');
                    },
                    success: function (pReturn) {
                        var chart = AmCharts.makeChart("pie_top_center", {
                            "type": "pie",
                            "theme": "light",
                            "labelText": "[[percents]]%",
                            "dataProvider": pReturn.items,
                            "valueField": "value",
                            "titleField": "label",
                            "balloon": {
                                "adjustBorderColor": true,
                                "color": "#000000",
                                "cornerRadius": 5,
                                "fillColor": "#FFFFFF"
                            },
                            "angle": 15,
                            "startEffect": "easeOutSine",
                            "startDuration": 2,
                            "labelRadius": 15,
                            "innerRadius": "10%",
                            "depth3D": 10,
                            hideCredits: true,
                            "export": {
                                "enabled": true
                            }
                        });

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#message").removeClass().addClass("alert alert-danger");
                        $("#message").html(xhr.statusText);
                        $("#myModalMessage").modal('show');

                    }
                });

            }

            if (cookieLocation != null) {
                $("#col1").removeClass().addClass("col-sm-6");
                $("#col2").removeClass().addClass("col-sm-6");
                $("#col3").hide();

                $("#labelsitesAccessType").show();
                var dataString = 'action=USERSBYBILLINGTYPELIVE';
                $.ajax({
                    url: "handlers/BigDataHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $("#myModalWait").modal('show');
                    },
                    complete: function () {
                        $("#myModalWait").modal('hide');
                    },
                    success: function (pReturn) {
                        var chart = AmCharts.makeChart("pie_top_center", {
                            "type": "pie",
                            "theme": "light",
                            "labelText": "[[percents]]%",
                            "dataProvider": pReturn.items,
                            "valueField": "value",
                            "titleField": "label",
                            "balloon": {
                                "adjustBorderColor": true,
                                "color": "#000000",
                                "cornerRadius": 5,
                                "fillColor": "#FFFFFF"
                            },
                            "angle": 15,
                            "startEffect": "easeOutSine",
                            "startDuration": 2,
                            "labelRadius": 15,
                            "innerRadius": "10%",
                            "depth3D": 10,
                            hideCredits: true,
                            "export": {
                                "enabled": true
                            }
                        });

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#message").removeClass().addClass("alert alert-danger");
                        $("#message").html(xhr.statusText);
                        $("#myModalMessage").modal('show');

                    }
                });
            }


            //real time
            var dataString = 'action=CLOSEDSESSIONSTIMELIVE';
            $.ajax({
                url: "handlers/BigDataHandler.ashx",
                data: dataString,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $("#myModalWait").modal('show');
                },
                complete: function () {
                    $("#myModalWait").modal('hide');
                },
                success: function (pReturn) {
                    var chart = AmCharts.makeChart("pie_top_left", {
                        "type": "pie",
                        "theme": "light",
                        "labelText": "[[percents]]%",
                        
                        "dataProvider" : pReturn.items,
                        "valueField": "value",
                        "titleField": "label",
                        "balloon": {
                            "adjustBorderColor": true,
                            "color": "#000000",
                            "cornerRadius": 5,
                            "fillColor": "#FFFFFF"
                        },
                        "angle": 15,
                        "startEffect": "easeOutSine",
                        "startDuration": 1,
                        "labelRadius": 15,
                        "innerRadius": "10%",
                        "depth3D": 10,
                        hideCredits: true,
                        "export": {
                            "enabled": true
                        }
                    });

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#message").removeClass().addClass("alert alert-danger");
                    $("#message").html(xhr.statusText);
                    $("#myModalMessage").modal('show');

                }
            });

           
            var dataString = 'action=USERSONLINEMAP';
            $.ajax({
                url: "handlers/BigDataHandler.ashx",
                data: dataString,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $("#myModalWait").modal('show');
                },
                complete: function () {
                    $("#myModalWait").modal('hide');
                },
                success: function (pReturn) {

                    document.getElementById('mapcontainer').innerHTML = "<div id='map' style='height:400px'></div>";
                    var wid = $("#map_container").width();
                    var heig = $("#map_container").height(9 * wid / 16);
                    $("#map").width(wid);
                    $("#map").height((9 * wid / 16));

                    //$("#last24content").height($("#map").height());
                    //$("#last24bubble").height($("#map").height() - 40);

                    L.mapbox.accessToken = 'pk.eyJ1IjoiamF2aWFycm9jaGEiLCJhIjoiY2ptaXY2Ymw5MDhndTNxbmptcGo1cnAycCJ9.YMORyd3rDYpXSDmIVeuiCw';
                    var map = L.mapbox.map('map', 'mapbox.streets')
                             .setView(pReturn.groupLocation, 4);
                           //   .setView([40.4378698, -3.8196191], 4);
                            

                    L.control.fullscreen().addTo(map);

                    var markers = new L.MarkerClusterGroup();

                    for (var i = 0; i < pReturn.items.length; i++) {
                        var a = pReturn.items[i];
                        var title = a[2];
                        var marker = L.marker(new L.LatLng(a[0], a[1]), {
                            icon: L.mapbox.marker.icon({ 'marker-symbol': 'mobilephone', 'marker-color': '0044FF' }),
                            title: title
                        });
                        marker.bindPopup(title);
                        markers.addLayer(marker);
                    }

                    map.addLayer(markers);
                    
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#message").removeClass().addClass("alert alert-danger");
                    $("#message").html(xhr.statusText);
                    $("#myModalMessage").modal('show');

                }
            });

            

            $("#area").attr("width", $("#svg_content").width());
            var dataString = 'action=ONLINEDEVICE';
            $.ajax({
                url: "handlers/BigDataHandler.ashx",
                data: dataString,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $("#myModalWait").modal('show');
                },
                complete: function () {
                    $("#myModalWait").modal('hide');
                },
                success: function (pReturn) {

                    var chart = AmCharts.makeChart("chartdiv4", {
                        "type": "serial",
                        "theme": "light",
                        "dataProvider": pReturn.items,
                        "colors": ["rgba(136,185,24,1)"],
                        "graphs": [{
                            "id": "g1",
                            "fillAlphas": 0.4,
                            "valueField": "value",
                            "balloonText": "<div style='margin:5px; font-size:19px;'>Users:<b>[[value]]</b></div>"
                        }],
                        "chartCursor": {
                            "categoryBalloonDateFormat": "JJ:NN, DD-MM-YYYY",
                            "cursorPosition": "mouse"
                        },
                        "categoryField": "label",
                        "categoryAxis": {
                            "minPeriod": "mm",
                            "parseDates": true
                        },
                        hideCredits: true,
                        "export": {
                            "enabled": true
                        }
                    });


                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#message").removeClass().addClass("alert alert-danger");
                    $("#message").html(xhr.statusText);
                    $("#myModalMessage").modal('show');

                }
            });

            var dataString = 'action=VALUESLIVE';
            $.ajax({
                url: "handlers/BigDataHandler.ashx",
                data: dataString,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $("#myModalWait").modal('show');
                },
                complete: function () {
                    $("#myModalWait").modal('hide');
                },
                success: function (pReturn) {
                    $("#up1").html(pReturn.Upload_1H + " GB");
                    $("#up24").html(pReturn.Upload_24H + " GB");
                    $("#upInterval").html(pReturn.Upload_Interval + " GB");
                    $("#down1").html(pReturn.Download_1H + " GB");
                    $("#down24").html(pReturn.Download_24H + " GB");
                    $("#downInterval").html(pReturn.Download_Interval + " GB");
                    $("#sesion1").html(pReturn.Sesion_1H);
                    $("#sesion24").html(pReturn.Sesion_24H);
                    $("#sesionInterval").html(pReturn.Sesion_Interval);
                    $("#sesion1").html(pReturn.Sesion_1H);
                    $("#sesion24").html(pReturn.Sesion_24H);
                    $("#sesionInterval").html(pReturn.Sesion_Interval);
                    $("#device1").html(pReturn.device_1H);
                    $("#device24").html(pReturn.device_24H);
                    $("#deviceInterval").html(pReturn.device_Interval);
                    $("#SesionDeviceInterval").html(pReturn.sesiondevice_Interval);
                    $("#SesionDevice1").html(pReturn.sesiondevice_1H);
                    $("#SesionDevice24").html(pReturn.sesiondevice_24H);
                    $("#users24").html(pReturn.Users_24H);
                    $("#users1").html(pReturn.Users_1H);
                    
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#message").removeClass().addClass("alert alert-danger");
                    $("#message").html(xhr.statusText);
                    $("#myModalMessage").modal('show');

                }
            });

            //HISTOGRAMA 
            $("#histograma").attr("width", $("#svg_content").width());
            var dataString = 'action=USERSPERHOURS';
            $.ajax({
                url: "handlers/BigDataHandler.ashx",
                data: dataString,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $("#myModalWait").modal('show');
                },
                complete: function () {
                    $("#myModalWait").modal('hide');
                },
                success: function (pReturn) {
                    
                    var chart = AmCharts.makeChart("chartdiv5", {
                        "type": "serial",
                        "theme": "light",
                        "colors": ["rgba(136,185,24,1)"],
                        "dataProvider": pReturn.items,

                        "valueAxes": [{
                            "gridColor": "#FFFFFF",
                            "gridAlpha": 0.2,
                            "dashLength": 0
                        }],
                        "gridAboveGraphs": true,
                        "startDuration": 1,
                        "graphs": [{
                            "balloonText": "[[category]]: <b>[[value]]</b>",
                            "fillAlphas": 0.8,
                            "lineAlpha": 0.2,
                            "type": "column",
                            "valueField": "value"
                        }],
                        "chartCursor": {
                            "categoryBalloonEnabled": false,
                            "cursorAlpha": 0,
                            "zoomable": false
                        },
                        "categoryField": "Hora",
                        "categoryAxis": {
                            "gridPosition": "start",
                            "gridAlpha": 0,
                            "tickPosition": "start",
                            "tickLength": 24
                        },
                        hideCredits: true,
                        "export": {
                            "enabled": true
                        }

                    });

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#message").removeClass().addClass("alert alert-danger");
                    $("#message").html(xhr.statusText);
                    $("#myModalMessage").modal('show');

                }
            });
        };

        $(document).ready(function (e) {
            dashboard();

            setInterval(function () {
                dashboard()
            }
                , 600000);

            $("#search").click(function (e) {
                dashboard();
            });

        });

</script>
</asp:Content>
