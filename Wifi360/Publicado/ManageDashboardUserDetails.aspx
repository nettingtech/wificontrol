﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Site.Master" AutoEventWireup="true" CodeBehind="ManageDashboardUserDetails.aspx.cs" Inherits="Wifi360.ManageDashboardUserDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        
        <script src="js/plugins/chosen/chosen.jquery.js"></script>
        <script type="text/javascript">
        
            $(document).ready(function () {
                var cookie = readCookie("wifi360-language");

                //Función para obtener parámetros 
                var parameters = getParameters();
                function getParameters() {
                    var searchString = window.location.search.substring(1)
                      , params = searchString.split("&")
                      , hash = {}
                    ;

                    for (var i = 0; i < params.length; i++) {
                        var val = params[i].split("=");
                        hash[unescape(val[0])] = unescape(val[1]);
                    }
                    return hash;
                }

                //Funcion para carga de elemento SELECT
                function load_options(opt, idmember) {
                    var dataString = 'action=SELECT_ALL_GROUPS_SITE_AND_LOCATIONS&id=0';
                    $.ajax({
                        url: "handlers/SMIHandler.ashx",
                        data: dataString,
                        contentType: "application/json; charset=utf-8",
                        dataType: "text",
                        success: function (pReturn) {
                            var lReturn = JSON.parse(pReturn);
                            $("option").empty();
                            switch (opt) {
                                case 1:
                                case "1":
                                    $("#labelnewuser_form_useroptions").html("Seleccina Grupo");
                                    $.each(lReturn.Group_list, function (index, item) {
                                        $("#option").append('<option value="' + item["idGroup"] + '"> ' + item["Name"] + '</option>');
                                    });
                                    break;
                                case 2:
                                case "2":
                                    $("#labelnewuser_form_useroptions").html("Seleccina Site");
                                    $.each(lReturn.Site_list, function (index, item) {
                                        $("#option").append('<option value="' + item["IdHotel"] + '"> ' + item["Name"] + '</option>');
                                    });
                                    break;
                                case 3:
                                case "3":
                                    $("#labelnewuser_form_useroptions").html("Seleccina Localización");
                                    $.each(lReturn.Location_list, function (index, item) {
                                        $("#option").append('<option value="' + item["Idlocation"] + '">' + item["Description"] + '</option>');
                                    })
                                    break;
                            }
                            $("#option").val(idmember);
                            $("#option").trigger("chosen:updated");
                        }

                    });
                }

                //Función de Precarga de elementos Select
                function bindSelect(id, valueMember, displayMember, source) {
                    $("#" + id).find('option').remove().end();
                    var cookie = readCookie("wifi360-language");
                    if (cookie != null) {
                        switch (cookie) {
                            case "es": $("#" + id).append('<option value="0">Seleccionar</option>'); break;
                            default: $("#" + id).append('<option value="0">Select</option>'); break;
                        }
                    }
                    else
                        $("#" + id).append('<option value="0">Select</option>');
                    $.each(source, function (index, item) {
                        $("#" + id).append('<option value="' + item[valueMember] + '"> ' + item[displayMember] + '</option>');
                    });
                };                

                //Precarga del FORM                 
                var usertype = 0;
                var userrol = 0;
                if (parameters.id != undefined) {
                    var dataString_u = 'action=FDSUSERFORID&id=' + parameters.id;
                    $.ajax({
                        url: "handlers/SMIHandler.ashx",
                        data: dataString_u,
                        contentType: "application/json; charset=utf-8",
                        dataType: "text",
                        beforeSend: function () {
                            $("#myModalWait").modal('show');
                        },        
                        success: function (pReturn_u) {
                            //Load Parameters
                            var lReturn_u = JSON.parse(pReturn_u);
                            var id_fdsuser = parameters.id;
                            $("#dashboard_name").val(lReturn_u.UserName);
                            $("#pass").val(lReturn_u.Password);
                            $("#fullname").val(lReturn_u.Name);                              
                            
                            var id_rol = lReturn_u.IdRol;
                            var id_type = lReturn_u.IdType;
                            load_options(id_type, lReturn_u.idmember);

                            //Precarga de los TYPES
                            var dataString_t = 'action=SELECTALLTYPES';
                            $.ajax({
                                url: "handlers/SMIHandler.ashx",
                                data: dataString_t,
                                contentType: "application/json; charset=utf-8",
                                dataType: "text",
                                success: function (pReturn_t) {
                                    var lReturn_t = JSON.parse(pReturn_t);
                                    $.each(lReturn_t, function (index, item) {
                                        $("#types").append('<option value="' + item["Id"] + '"> ' + item["Name"] + ' (' + item["Descpription"] + ')</option>');
                                    });
                                    if (id_type != '0') {
                                        $("#types").val(id_type);
                                    }
                                    $("#types").trigger("chosen:updated");

                                    //Precarga de los SELECT ROLS
                                    var dataString_r = 'action=SELECTALLROLS';
                                    $.ajax({
                                        url: "handlers/SMIHandler.ashx",
                                        data: dataString_r,
                                        contentType: "application/json; charset=utf-8",
                                        dataType: "text",
                                        success: function (pReturn_r) {
                                            var lReturn_r = JSON.parse(pReturn_r);
                                            $.each(lReturn_r, function (index, item) {
                                                $("#rols").append('<option value="' + item["IdRol"] + '"> ' + item["Name"] + ' (' + item["Description"] + ')</option>');
                                            });
                                            if (id_rol != '0') {
                                                $("#rols").val(id_rol);
                                            }
                                            $("#rols").trigger("chosen:updated");
                                        }
                                    });
                                }
                            });
                        },
                        complete: function () {
                            $("#myModalWait").modal('hide');
                        }
                    });
                } else {
                    load_options('1');
                    //Precarga de los TYPES
                    var dataString_t = 'action=SELECTALLTYPES';
                    $.ajax({
                        url: "handlers/SMIHandler.ashx",
                        data: dataString_t,
                        contentType: "application/json; charset=utf-8",
                        dataType: "text",
                        beforeSend: function () {
                            $("#myModalWait").modal('show');
                        },
                        success: function (pReturn_t) {
                            var lReturn_t = JSON.parse(pReturn_t);
                            $.each(lReturn_t, function (index, item) {
                                $("#types").append('<option value="' + item["Id"] + '"> ' + item["Name"] + ' (' + item["Descpription"] + ')</option>');
                            });
                            $("#types").trigger("chosen:updated");

                            //Precarga de los SELECT ROLS
                            var dataString_r = 'action=SELECTALLROLS';
                            $.ajax({
                                url: "handlers/SMIHandler.ashx",
                                data: dataString_r,
                                contentType: "application/json; charset=utf-8",
                                dataType: "text",
                                success: function (pReturn_r) {
                                    var lReturn_r = JSON.parse(pReturn_r);
                                    $.each(lReturn_r, function (index, item) {
                                        $("#rols").append('<option value="' + item["IdRol"] + '"> ' + item["Name"] + ' (' + item["Description"] + ')</option>');
                                    });                                  
                                    $("#rols").trigger("chosen:updated");
                                }
                            });
                        },
                        completed: function () {
                            $("#myModalWait").modal('hide');
                        }
                    });
                }                                                          
                                               
                //Funciones para el componente CHOSEN
                $(function () {
                    $('.chosen-select').chosen();
                    $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
                });
                $('.chosen-select').chosen({ width: "100%" });

                //Función para el guardado de FDUser
                function Save_dashboard_group() {
                    var data = {};
                    //tipo de acción
                    data["action"] = 'SAVEFDSUSER';
                    data["id"] = parameters.id;
                    data["username"] = $('#dashboard_name').val();
                    data["pass"] = $('#pass').val();
                    data["fullname"] = $('#fullname').val();
                    data["types"] = $('#types').val();
                    data["rol"] = $('#rols').val();
                    //ID del grupo/site/location asosiado al Type
                    data["id_access"] = $('#option').val();

                    //Llamada AJAX al HANDLER para el SAVE.
                    $.ajax({
                        url: "handlers/SMIHandler.ashx",
                        type: "POST",
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "text",
                        beforeSend: function () {
                            $("#myModalWait").modal('show');
                        },
                        
                        success: function (msg) {
                            if (msg.code == "OK") {
                                $("#message").removeClass().addClass("alert alert-success");
                                $("#message").html(GetMessage(msg.message));

                            }
                            else {
                                $("#message").removeClass().addClass("alert alert-danger");
                                $("#message").html(GetMessage(msg.message));
                            }
                            $("#myModalMessage").modal('show');
                            var lReturn = JSON.parse(msg);
                            if (lReturn.IdUser != undefined) {
                                parameters.id = lReturn.IdUser;
                            }
                        },
                        complete: function () {
                            $("#myModalWait").modal('hide');
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#loading").html("We have a problem, please try again later.");
                            alert('error: ' + xhr.statusText);
                        }
                    });

                }

                //Evento para actualizar opciones 
                $("#types").change(function (e){
                    e.preventDefault();
                    load_options($("#types").val());
                });

                //Evento para salvar formulario FDSUSER
                $("#save_dashboard").click(function (e) {
                    e.preventDefault();
                    Save_dashboard_group();
                });
            })
       </script>             


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2 id="headerpagemanageusers">Management Dashboard User Details</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="default.aspx">Dashboard</a>
                    </li>
                    <li id="breadpagefathersuperadmin">
                        <a>Super Admin</a>
                    </li>
                    <li class="active">
                        <strong id="breadlabelmanageuser">Management User</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2"></div>
        </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-9">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5 id="labelboxheadermanagementusersdet"> Management User Details</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="form-horizontal">
                            <div class="form-group"> <label class="control-label col-sm-3" id="labelnewuser_form_name">Name DashBoard User</label>
                                <div class="col-sm-9">
                                    <input type="text"  id="dashboard_name" class="form-control" />
                                </div>
                            </div> 
                            
                            <div class="form-group"> <label class="control-label col-sm-3" id="labelformpassword">Password</label>
                                <div class="col-sm-9">
                                    <input type="text"  id="pass" class="form-control" />
                                </div>
                            </div>  
                            <div class="form-group"> <label class="control-label col-sm-3" id="labelnewuser_form_fullname" >Full Name</label>
                                <div class="col-sm-9">
                                    <input type="text"  id="fullname" class="form-control" />
                                </div>
                            </div>                         
                            <div class="form-group"><label class="control-label col-sm-3" id="labelnewuser_form_userrol">Select User Rol</label>
                                <div class="col-sm-9"><select data-placeholder="Rols..." class="chosen-select chosen-select-deselect" style="display: none;" tabindex="-1" id="rols"></select></div>         
                            </div>
                            <div class="form-group"><label class="control-label col-sm-3" id="labelnewuser_form_usertype">Select User Type</label>
                                <div class="col-sm-9"><select data-placeholder="Types..." class="chosen-select chosen-select-deselect" style="display: none;" tabindex="-1" id="types"></select></div>         
                            </div> 
                            <div class="form-group"><label class="control-label col-sm-3" id="labelnewuser_form_useroptions">Select Option</label>
                                <div class="col-sm-9"><select data-placeholder="..." class="chosen-select chosen-select-deselect" style="display: none;" tabindex="-1" id="option"></select></div></div>                                              
                        </div>
                    </div>
                </div>
            </div>

            <!-- COLUMNA DCHA -->
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5 id="labelboxheaderbuttonsettings">Actions</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <p>
                        <a href="#" class="btn btn-w-m btn-lg btn-primary btn-block" id="save_dashboard"><i class="fa fa-save"></i> <span id="buttonsaveuser"> Save User</span></a>
                        <a href="ManageDashboardUsers.aspx" role="button" class="btn btn-w-m btn-lg btn-default btn-block" id="back"><i class="fa fa-backward"></i> <span id="label_createG_form_buttonback">Back </span></a>
                    </p>
                </div>
            </div>
        </div>
        </div>
    </div>
            

        <div class="modal inmodal fade" id="ModalMessage" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header"></div>
                    <div class="modal-body">
                        <div id="message"></div>
                    </div>
                   <div class="modal-footer">
                       <a href="#" class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Volver</a>
                   </div>
                </div>
            </div>
        </div>



</asp:Content>
