﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Site.Master" AutoEventWireup="true" CodeBehind="changesite.aspx.cs" Inherits="Wifi360.changesite" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        $(document).ready(function () {


       //     $("#lisettings").attr("class", "active");
      //      $("#ulsettings").attr("class", "nav nav-second-level collapse in");
       //     $("#lichangesettings").attr("class", "active");
       //     $("#lidashboard").removeClass();
            
            var dataString = 'action=selectmembers';
            $.ajax({
                url: "handlers/SMIHandler.ashx",
                data: dataString,
                contentType: "application/json; charset=utf-8",
                dataType: "text",
                beforeSend: function () {
                    $("#modal-loading").show()
                },
                complete: function () {
                    $("#modal-loading").hide()
                },
                success: function (pReturn) {
                    var lReturn = JSON.parse(pReturn);
                    if (lReturn.list.length != 0) {
                        bindSelect('sites', 'Id', 'Name' , 'IdMembertype', lReturn.list);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#message").html(xhr.statusText);
                    $("#message").show();
                    $("#message").removeClass().addClass("alert alert-danger");
                }
            });


            $("#change").click(function (e) {
                e.preventDefault();

                if (($("#sites").is(':disabled') && $("#locations").val() == '0') || $("#sites").val() == '0') {
                    $("#message").addClass("alert alert-danger");
                    var cookie = readCookie("wifi360-language");
                    if (cookie != null) {
                        switch (cookie) {
                            case "es": $("#message").html("Es obligatorio elegir sitio"); break;
                            default: $("#message").html("Site is mandatory"); break;
                        }
                    }
                    else
                        $("#message").html("Site is mandatory");

                    $("#myModalMessage").modal("show");

                }
                else {

                    if ($("#sites").is(':disabled')) {
                        
                        var h = $("#locations").val();
                        if (h != 'site')
                            createCookie("location", h, 1);
                        else
                            createCookie("location", h, -1);
                        $("#modal-loading").show()
                        window.location.href = "Default.aspx";

                    }
                    else {
                        var h = $("#sites").val();
                        var element = $(this);
                        var rel = $('option:selected', $("#sites")).attr('rel');
                        switch(rel)
                        {
                            case "1": createCookie("location", h, -1); createCookie("site", h, -1); createCookie("group", h, 1); break;
                            case "2": createCookie("site", h, 1); createCookie("location", h, -1); break;
                            case "3": createCookie("location", h, 1); break;
                        }
                        $("#modal-loading").show()
                        window.location.href = "Default.aspx";

                    }
                }
            });

        });

        function readCookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        };

        function createCookie(name, value, days) {
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                var expires = "; expires=" + date.toGMTString();
            }
            else var expires = "";
            document.cookie = name + "=" + value + expires + "; path=/";
        }

        function bindSelect(id, valueMember, displayMember, relmember, source) {
            $("#" + id).find('option').remove().end();
            var cookie = readCookie("wifi360-language");
            if (cookie != null) {
                switch (cookie) {
                    case "es": $("#" + id).append('<option value="0">Seleccionar</option>'); break;
                    default: $("#" + id).append('<option value="0">Select</option>'); break;
                }
            }
            else
                $("#" + id).append('<option value="0">Select</option>');
            $.each(source, function (index, item) {
                $("#" + id).append('<option value="' + item[valueMember] + '" rel="'+ item[relmember] +'"> ' + item[displayMember] + '</option>');
            });
        };

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 id="headerpagelocationsetting">Settings</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="default.aspx">Dashboard</a>
                </li>
                <li id="breadpagefatherlocationsetting">
                    <a>Settings</a>
                </li>
                <li class="active">
                    <strong id="breadpagechangesite">Change site</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-9">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                    <h5 id="labelboxheaderchangesite">Change site<small> Change site to manage</small></h5>
                </div>
                    <div class="ibox-content">
                     <div class="form-horizontal">
                    <div class="form-group"> <label class="control-label col-sm-2" id="labelformchangesites">Sites</label><div class="col-sm-10"><select id="sites" class="form-control"></select></div></div>

                    <div class="form-group"> <label class="control-label col-sm-2" id="labelformlocations" style="display:none;"> Locations</label><div class="col-sm-10"><select id="locations" class="form-control" style="display:none;"></select></div></div>
                   

                </div>
                </div>
            </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5 id="labelboxheaderactionchangesite">Accions</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <p>
                            <a href="#" class="btn btn-w-m btn-lg btn-primary btn-block" id="change"><i class="fa fa-refresh"></i> <span id="labelformchangesitebuttonchange">Change</span></a>

                        </p>
                    </div>
                </div>
            </div>
            <div class="modal inmodal fade" id="myModalMessage" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                            </div>
                            <div class="modal-body">
                                <div id="message" class="text-center">
                                    
                                </div>
                            </div>
                          <div class="modal-footer">
                            <a href="#" class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Volver</a>
                          </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</asp:Content>
