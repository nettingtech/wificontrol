﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Site.Master" AutoEventWireup="true" CodeBehind="sitesettings.aspx.cs" Inherits="Wifi360.sitesettings" ValidateRequest="false"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/plugins/switchery/switchery.css" rel="stylesheet" />
    <script src="js/plugins/switchery/switchery.js"></script>

        <script type="text/javascript">

            $(document).ready(function () {

                function readCookie(name) {
                    var nameEQ = name + "=";
                    var ca = document.cookie.split(';');
                    for (var i = 0; i < ca.length; i++) {
                        var c = ca[i];
                        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
                    }
                    return null;
                };


                function bindSelect(id, valueMember, displayMember, source) {
                    $("#" + id).find('option').remove().end();
                    var cookie = readCookie("wifi360-language");
                    if (cookie != null) {
                        switch (cookie) {
                            case "es": $("#" + id).append('<option value="0">Seleccionar</option>'); break;
                            default: $("#" + id).append('<option value="0">Select</option>'); break;
                        }
                    }
                    else
                        $("#" + id).append('<option value="0">Select</option>');
                    $.each(source, function (index, item) {
                        $("#" + id).append('<option value="' + item[valueMember] + '"> ' + item[displayMember] + '</option>');
                    });
                };

                function getParameters() {
                    var searchString = window.location.search.substring(1)
                      , params = searchString.split("&")
                      , hash = {}
                    ;

                    for (var i = 0; i < params.length; i++) {
                        var val = params[i].split("=");
                        hash[unescape(val[0])] = unescape(val[1]);
                    }
                    return hash;
                }

                var parameters = getParameters(); 

                var dataString2 = 'action=languages';
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataString2,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    success: function (pReturn) {
                        var lReturn = JSON.parse(pReturn);
                        bindSelect('defaultlanguage', 'IdLanguage', 'Name', lReturn.list);

                        var dataString3 = 'action=vendors';
                        $.ajax({
                            url: "handlers/SMIHandler.ashx",
                            data: dataString3,
                            contentType: "application/json; charset=utf-8",
                            dataType: "text",
                            success: function (pReturn3) {
                                var lReturn3 = JSON.parse(pReturn3);
                                bindSelect('vendors', 'IdVendor', 'Name', lReturn3.list);

                                var dataString3 = 'action=vendors';
                                $.ajax({
                                    url: "handlers/SMIHandler.ashx",
                                    data: dataString3,
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "text",
                                    success: function (pReturn3) {
                                        var lReturn3 = JSON.parse(pReturn3);
                                        bindSelect('vendors', 'IdVendor', 'Name', lReturn3.list);

                                        var dataString4 = 'action=Currencies';
                                        $.ajax({
                                            url: "handlers/SMIHandler.ashx",
                                            data: dataString4,
                                            contentType: "application/json; charset=utf-8",
                                            dataType: "text",
                                            beforeSend: function () {
                                                $("#myModalWait").modal('show');
                                            },
                                            complete: function () {
                                                $("#myModalWait").modal('hide');
                                            },
                                            success: function (pReturn4) {
                                                var lReturn4 = JSON.parse(pReturn4);
                                                bindSelect('currencies', 'Code', 'Badge', lReturn4.list);

                                                var dataString = 'action=sitesetting&id=' + parameters.id;
                                                $.ajax({
                                                    url: "handlers/SMIHandler.ashx",
                                                    data: dataString,
                                                    contentType: "application/json; charset=utf-8",
                                                    dataType: "text",
                                                    success: function (pReturn) {
                                                        var lReturn = JSON.parse(pReturn);
                                                        if (lReturn.Name != null) {
                                                            $("#name").val(lReturn.Name);
                                                            $("#nse").val(lReturn.NSE);
                                                            $("#nseprincipalip").val(lReturn.PrincipalNSEIP);
                                                            $("#bandwidth").val(lReturn.BandWidth);
                                                            $("#bandwidthup").val(lReturn.BandwidthUp);
                                                            $("#paypaluser").val(lReturn.PayPalUser);
                                                            $("#currencies").val(lReturn.CurrencyCode);
                                                            $("#latitude").val(lReturn.latitude);
                                                            $("#longitude").val(lReturn.longitude);
                                                            $("#mailfrom").val(lReturn.MailFrom);
                                                            $("#smtp").val(lReturn.SMTP);
                                                            $("#smtpport").val(lReturn.SmtpPort);
                                                            $("#smtpuser").val(lReturn.SmtpUser);
                                                            $("#smtppassword").val(lReturn.SmtpPassword);
                                                            $("#urlhotel").val(lReturn.UrlHotel);
                                                            if (lReturn.SmtpSSL == true)
                                                                $("#smtpssl").attr('checked', true);
                                                            if (lReturn.MacBlocking == true)
                                                                $("#macblocking").attr('checked', true);
                                                            if (lReturn.macauthenticate)
                                                                $("#macauthenticate").attr('checked', true);
                                                            if (lReturn.filtercontents)
                                                                $("#filtercontents").attr('checked', true);
                                                            if (lReturn.volumecombined)
                                                                $("#volumecombined").attr('checked', true);

                                                            $("#bannedtime").val(lReturn.MacBlockingInterval);
                                                            $("#maxmacattemps").val(lReturn.MaxMacAttemps);
                                                            $("#macattemptinterval").val(lReturn.MACAttempsInterval);

                                                            if (lReturn.PrintLogo == true)
                                                                $("#logocheck").attr('checked', true);
                                                            if (lReturn.PrintVolume == true)
                                                                $("#volumecheck").attr('checked', true);
                                                            if (lReturn.PrintSpeed == true)
                                                                $("#speedcheck").attr('checked', true);

                                                            $("#urlreturnpaypal").val(lReturn.UrlReturnPaypal);
                                                            $("#freeaccessrepeattime").val(lReturn.FreeAccessRepeatTime);

                                                            if (lReturn.Survey == true)
                                                                $("#surveycheck").attr('checked', true);

                                                            if (lReturn.LoginModule == true)
                                                                $("#loginmodule").attr('checked', true);
                                                            if (lReturn.FreeAccessModule == true)
                                                                $("#freeaccessmodule").attr('checked', true);
                                                            if (lReturn.PayAccessModule == true)
                                                                $("#payaccessmodule").attr('checked', true);
                                                            if (lReturn.SocialNetworksModule == true)
                                                                $("#socialnetworkmodule").attr('checked', true);
                                                            if (lReturn.CustomAccessModule == true)
                                                                $("#customaccessmodule").attr('checked', true);
                                                            if (lReturn.VoucherAccessModule == true)
                                                                $("#voucheraccessmodule").attr('checked', true);


                                                            $("#defaultlanguage").val(lReturn.IdLanguageDefault);
                                                            $("#vendors").val(lReturn.vendor);
                                                            $("#prefix").val(lReturn.prefix);
                                                            $("#longusername").val(lReturn.longusername);
                                                            $("#longpassword").val(lReturn.longpassword);
                                                            $("#longvoucherusername").val(lReturn.longvoucherusername);
                                                            $("#voucherprefix").val(lReturn.voucherprefix);

                                                            var text = '';
                                                            //$.each(lReturn.redes, function (index, item) {
                                                            //    text += '<div class="form-group"> <label class="control-label col-sm-3" > ' + item["Name"] + '</label><div class="col-sm-9"><input type="checkbox" rel="checkSN" id="' + item["IdSocialNetwork"] + '" ';
                                                            //    if (item["Active"])
                                                            //        text += 'checked="' + item["Active"] + '"';
                                                            //    text += 'class="js-switch"/></div></div>';

                                                            //});

                                                            //$("#socialnetwokscheks").html(text);

                                                            if (lReturn.EnabledDaylyPass == true)
                                                                $("#CheckEnabledDaylyPass").attr('checked', true);

                                                            $("#valueDaylyPass").val(lReturn.valueDaylyPass);

                                                        }

                                                        if (parameters.id != undefined){
                                                            //BOTONES DE CONFIG
                                                            $("#B_Locations").show();
                                                            $("#B_Locations").attr("href", "locationsettings.aspx?idsite=" + parameters.id + "&return=sitesettings.aspx");

                                                            $("#B_Prioreties").show();
                                                            $("#B_Prioreties").attr("href", "priorities.aspx?idsite=" + parameters.id + "&return=sitesettings.aspx");

                                                            $("#B_Survey").show();
                                                            $("#B_Survey").attr("href", "surveysettings.aspx?idsite=" + parameters.id + "&return=sitesettings.aspx");

                                                            $("#B_FilterID").show();
                                                            $("#B_FilterID").attr("href", "filterprofiles.aspx?idsite=" + parameters.id + "&return=sitesettings.aspx");
                                                        } else {
                                                            //BOTONES DE CONFIG HIDE
                                                            $("#B_Locations").hide();                                                           
                                                            $("#B_Prioreties").hide();
                                                            $("#B_Survey").hide();
                                                            $("#B_FilterID").hide();
                                                        }
                                                        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

                                                        elems.forEach(function (html) {
                                                            var switchery = new Switchery(html, { color: '#1AB394' });
                                                        });
                                                    },
                                                    error: function (xhr, ajaxOptions, thrownError) {
                                                        $("#modal-loading").hide();
                                                        $("#searchResult").html(xhr.statusText)
                                                    }
                                                });
                                            },
                                            error: function (xhr, ajaxOptions, thrownError) {
                                                $("#modal-loading").hide();
                                                $("#searchResult").html(xhr.statusText)
                                            }
                                        });
                                    },
                                    error: function (xhr, ajaxOptions, thrownError) {
                                        $("#modal-loading").hide();
                                        $("#searchResult").html(xhr.statusText)
                                    }
                                });



                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                $("#modal-loading").hide();
                                $("#searchResult").html(xhr.statusText)
                            }
                        });
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#modal-loading").hide();
                        $("#searchResult").html(xhr.statusText)
                    }
                });

                $("#language").change(function (e) {
                    $("#redactor").redactor('set', '');

                    e.preventDefault();

                    if ($("#language").val() != '0') {
                        var dataString2 = 'action=disclaimer&idlanguage=' + $("#language").val() + '&idlocation=' + $("#location").val();
                        $.ajax({
                            url: "resources/handlers/BillingHandler.ashx",
                            data: dataString2,
                            contentType: "application/json; charset=utf-8",
                            dataType: "text",
                            beforeSend: function () {
                                $("#myModalWait").modal('show');
                            },
                            complete: function () {
                                $("#myModalWait").modal('hide');
                            },
                            success: function (pReturn) {
                                var lReturn = JSON.parse(pReturn);
                                $("#redactor").redactor('set', lReturn.text);
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                $("#modal-loading").hide();
                                $("#searchResult").html(xhr.statusText)
                            }
                        });
                    }
                });

                $("#save").click(function (e) {

                    var check = true;
                    e.preventDefault();                    
                    var parameters = getParameters();

                    var data = {};
                    if (parameters.id == undefined)
                        data["id"] = 0;
                    else
                        data["id"] = parameters.id;

                    data["action"] = 'SAVESTTINGS';
                    data["name"] = $("#name").val();
                    data["nse"] = $("#nse").val();
                    data["nseprincipalip"] = $("#nseprincipalip").val();
                    data["bandwidth"] = $("#bandwidth").val();
                    data["bandwidthup"] = $("#bandwidthup").val();
                    data["paypaluser"] = $("#paypaluser").val();
                    data["currencycode"] = $("#currencies").val();
                    data["mailfrom"] = $("#mailfrom").val();
                    data["smtp"] = $("#smtp").val();
                    data["smtpport"] = $("#smtpport").val();
                    data["smtpuser"] = $("#smtpuser").val();
                    data["smtppassword"] = $("#smtppassword").val();
                    data["smtpssl"] = $("#smtpssl").is(':checked');
                    data["macblocking"] = $("#macblocking").is(':checked');
                    data["macauthenticate"] = $("#macauthenticate").is(':checked');
                    data["macbannedinterval"] = $("#bannedtime").val();
                    data["maxmacattemps"] = $("#maxmacattemps").val();
                    data["macattempsinterval"] = $("#macattemptinterval").val();
                    data["urlhotel"] = $("#urlhotel").val();
                    data["urlreturnpaypal"] = $("#urlreturnpaypal").val();
                    data["freeaccessrepeattime"] = $("#freeaccessrepeattime").val();
                    data["freeaccessmodule"] = $("#freeaccessmodule").is(':checked');
                    data["payaccessmodule"] = $("#payaccessmodule").is(':checked');
                    data["socialnetworkmodule"] = $("#socialnetworkmodule").is(':checked');
                    data["customaccessmodule"] = $("#customaccessmodule").is(':checked');
                    data["voucheraccessmodule"] = $("#voucheraccessmodule").is(':checked');
                    data["loginmodule"] = $("#loginmodule").is(':checked');
                    data["idlanguagedefault"] = $("#defaultlanguage").val();
                    data["survey"] = $("#surveycheck").is(':checked');
                    data["printlogo"] = $("#logocheck").is(':checked');
                    data["printvolume"] = $("#volumecheck").is(':checked');
                    data["printspeed"] = $("#speedcheck").is(':checked');
                    data["prefix"] = $("#prefix").val();

                    if ($("#longusername").val() <= 3) {
                        check = false;
                        $("#message").html(GetMessage("0x0045"));
                        $("#myModalMessage").modal('show');
                    } else {
                        data["longusername"] = $("#longusername").val();
                    }
                    
                    if ($("#longpassword").val() <= 3) {
                        check = false;
                        $("#message").html(GetMessage("0x0046"));
                        $("#myModalMessage").modal('show');
                    } else {
                        data["longpassword"] = $("#longpassword").val();
                    }

                    if ($("#longvoucherusername").val() <= 3) {
                        check = false;
                        $("#message").html(GetMessage("0x0047"));
                        $("#myModalMessage").modal('show');
                    }else{
                        data["longvoucherusername"] = $("#longvoucherusername").val();
                    }
                    
                    data["enableddaylypass"] = $("#CheckEnabledDaylyPass").is(':checked');
                    data["valuedaylypass"] = $("#valueDaylyPass").val();
                    data["voucherprefix"] = $("#voucherprefix").val();
                    
                    data["vendor"] = $("#vendors").val();
                    data["filtercontents"] = $("#filtercontents").is(':checked');                  
                    data["latitude"] = $("#latitude").val();
                    data["longitude"] = $("#longitude").val();
                    data["volumecombined"] = $("#volumecombined").is(':checked');

                    var sntext = '';
                    //$('input[rel^="checkSN"]').each(function (input) {
                    //    sntext += $(this).attr('id') + '=' + $(this).is(':checked') + '|';
                    //});

                    data["SocialNetworks"] = sntext;
                    
                    if (check) {
                        $.ajax({
                            url: "handlers/SMIHandler.ashx",
                            type: "POST",
                            data: JSON.stringify(data),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            beforeSend: function () {
                                $("#myModalWait").modal('show');
                            },
                            complete: function () {
                                $("#myModalWait").modal('hide');
                            },
                            success: function (msg) {
                                if (msg.code == "OK") {
                                    $("#message").removeClass().addClass("alert alert-success");
                                    $("#message").html(GetMessage("0x0025"));

                                }
                                else {
                                    $("#message").removeClass().addClass("alert alert-danger");
                                    $("#message").html(msg.message);
                                }
                                $("#myModalMessage").modal('show');
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                $("#loading").html("We have a problem, please try again later.");
                                alert('error: ' + xhr.statusText);
                            }
                        });
                    }//fin chekeo
                });

                $("#back").click(function (e) {
                    e.preventDefault();
                    var url = "default.aspx";
                    if (parameters.id != undefined) {
                        url = "managesites.aspx";
                    }
                    window.location.href = url;
                });

            });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 id ="headerpagesettings">Settings</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="default.aspx">Dashboard</a>
                </li>
                <li>
                    <a id="breadpagefathersettings">Settings</a>
                </li>
                <li class="active">
                    <strong id="breadpagesettings">Global Settings</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-9">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5 id="labelboxheadersettings">Global Settings</h5>
                    </div>
                    <div class="ibox-content">
                        <div id="bottons">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <a href="locationsettings.aspx" class="btn btn-info" id="B_Locations"><i class="fa fa-database"></i> Locations</a>
                                    <a href="priorities.aspx" class="btn btn-info" id="B_Prioreties"><i class="fa fa-building"></i> Priority</a>
                                    <a href="surveysettings.aspx" class="btn btn-info" id="B_Survey"><i class="fa fa-university"></i> Survey</a>
                                    <a href="filterprofiles.aspx" class="btn btn-info" id="B_FilterID"><i class="fa fa-bullseye"></i> Filters Contents</a>
                                </div>                        
                            </div>
                        </div>
                        <div class="panel-group" id="accordion">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h5 class="panel-title text-center">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" class=""><span id="labelacordionheaderbasic">Basic Settings</span></a>
                                            </h5>
                                        </div>
                                        
                                        <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true">
                                            <div class="panel-body">
                                                <div class="form-horizontal">
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformname">Name</label><div class="col-sm-9"><input type="text" class="form-control" id="name" /></div></div>
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformlanguage">Default language</label><div class="col-sm-9"><select class="form-control" id="defaultlanguage" ></select></div></div>
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformpaypalcurrency">Currency code</label><div class="col-sm-9"><select id="currencies" class="form-control"></select></div></div>
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labellocationsettingsformlatitude">Latitude</label><div class="col-sm-4"><input type="text" class="form-control" id="latitude" /></div></div>
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformlongitude">Longitude</label><div class="col-sm-4"><input type="text" class="form-control" id="longitude" /></div></div>
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformlanding">Landing page</label><div class="col-sm-9"><input type="text" class="form-control" id="urlhotel" /></div></div>
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformvendor">Vendor</label><div class="col-sm-9"><select class="form-control" id="vendors" ></select></div></div>
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformreauthenticate">CLOUD Reauthentication</label><div class="col-sm-9"><input type="checkbox" id="macauthenticate" class="js-switch" /></div></div>
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformfiltercontents">Content filtering (Mikrotik)</label><div class="col-sm-9"><input type="checkbox" id="filtercontents" class="js-switch" /></div></div>
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformvolumecombined">Aggregate volume limitation</label><div class="col-sm-9"><input type="checkbox" id="volumecombined" class="js-switch" /></div></div>
                                                
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title text-center">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed" aria-expanded="false"><span id="labelacordionheaderticket">Ticket / Voucher Settings</span></a>
                                            </h4>
                                        </div>
                                        <div id="collapseTwo" class="panel-collapse collapse" aria-expanded="false">
                                            <div class="panel-body">
                                                <div class="form-horizontal">
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformprefix">Username Prefix</label><div class="col-sm-9"><input type="text" class="form-control" id="prefix" /></div></div>
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformlongusername">Random username length without prefix (min. 3)</label><div class="col-sm-9"><input type="number" class="form-control" id="longusername" /></div></div>
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformlongpass">Password length (min. 3)</label><div class="col-sm-9"><input type="number" class="form-control" id="longpassword" /></div></div>
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformvoucherprefix">Voucher Username Prefix</label><div class="col-sm-9"><input type="text" class="form-control" id="voucherprefix" /></div></div>
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformvoucherlongusername">Random Voucher username length without prefix (min. 3)</label><div class="col-sm-9"><input type="number" class="form-control" id="longvoucherusername" /></div></div>

                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformprintlogo">Print logo</label><div class="col-sm-9"><input type="checkbox" id="logocheck" class="js-switch" /></div></div>
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformprintspeed">Print conection bandwidth</label><div class="col-sm-9"><input type="checkbox" id="speedcheck" class="js-switch"/></div></div>
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformprintvolume">Print volume</label><div class="col-sm-9"><input type="checkbox" id="volumecheck" class="js-switch" /></div></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title text-center">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="collapsed" aria-expanded="false"><span id="labelacordionheaderline">Line Settings</span></a>
                                            </h4>
                                        </div>
                                        <div id="collapseThree" class="panel-collapse collapse" aria-expanded="false">
                                            <div class="panel-body">
                                                <div class="form-horizontal">
                                                    <div class="form-group"> <label class="control-label col-sm-3">NSE (Nomadix)</label><div class="col-sm-9"><input type="text" id="nse" class="form-control" /></div></div>
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformip">Main IP</label><div class="col-sm-9"><input type="text" id="nseprincipalip" class="form-control" /></div></div>
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformbandwidthdown">Bandwidth Down</label><div class="col-sm-9"><div class="input-group m-b"><input type="text" id="bandwidth" class="form-control" /><span class="input-group-addon">Kbps</span></div></div></div>
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformbandwidthup">Bandwidth Up</label><div class="col-sm-9"><div class="input-group m-b"><input type="text" id="bandwidthup" class="form-control" /><span class="input-group-addon">Kbps</span></div></div></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title text-center">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" class="collapsed" aria-expanded="false"><span id="labelacordionheadermodule">Access Module Settings</span></a>
                                            </h4>
                                        </div>
                                        <div id="collapseFour" class="panel-collapse collapse" aria-expanded="false">
                                            <div class="panel-body">
                                                <div class="form-horizontal">
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformloginmodule">Login module</label><div class="col-sm-9"><input type="checkbox" id="loginmodule" class="js-switch"/></div></div>
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformfreeaccessmodule">Free Access module</label><div class="col-sm-9"><input type="checkbox" id="freeaccessmodule" class="js-switch" /></div></div>
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformpremiummodule">Premium Access module</label><div class="col-sm-9"><input type="checkbox" id="payaccessmodule" class="js-switch"/></div></div>
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformsocialaccessmodule">Social Access module</label><div class="col-sm-9"><input type="checkbox" id="socialnetworkmodule" class="js-switch" /></div></div>
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformcustommodule">Custom Access module</label><div class="col-sm-9"><input type="checkbox" id="customaccessmodule" class="js-switch" /></div></div>
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformvouchermodule">Voucher Access module</label><div class="col-sm-9"><input type="checkbox" id="voucheraccessmodule" class="js-switch" /></div></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title text-center">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive" class="collapsed" aria-expanded="false"><span id="labelacordionheaderfreeaccess">Free Access Settings</span></a>
                                            </h4>
                                        </div>
                                        <div id="collapseFive" class="panel-collapse collapse" aria-expanded="false">
                                            <div class="panel-body">
                                                <div class="form-horizontal">
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformfreeaccesstime">Time between free Access</label><div class="col-sm-9"><div class="input-group m-b"><input type="number" id="freeaccessrepeattime" class="form-control" /><span class="input-group-addon">h</span></div></div></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title text-center">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix" class="collapsed" aria-expanded="false"><span id="labelacordionheaderpaypal">PayPal Settings</span></a>
                                            </h4>
                                        </div>
                                        <div id="collapseSix" class="panel-collapse collapse" aria-expanded="false">
                                            <div class="panel-body">
                                                <div class="form-horizontal">
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformpaypalusername">Paypal user</label><div class="col-sm-9"><input type="text" id="paypaluser" class="form-control" /></div></div>
                                                    
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformpaypalurl">UrlLanding from Paypal</label><div class="col-sm-9"><input type="text" id="urlreturnpaypal" class="form-control" /></div></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title text-center">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" class="collapsed" aria-expanded="false"><span id="labelacordionheadersecurity">Security Settings</span></a>
                                            </h4>
                                        </div>
                                        <div id="collapseSeven" class="panel-collapse collapse" aria-expanded="false">
                                            <div class="panel-body">
                                                <div class="form-horizontal">
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformmacbanned">MAC Banned</label><div class="col-sm-9"><input type="checkbox" id="macblocking" class="js-switch"/></div></div>
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformbannedtime">Banned time</label><div class="col-sm-9"><div class="input-group m-b"><input type="number" id="bannedtime" class="form-control" /><span class="input-group-addon">s</span></div></div></div>
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformmaxattemps">Max number of MAC attemps</label><div class="col-sm-9"><input type="number" id="maxmacattemps" class="form-control" /></div></div>
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformpaylabelsettingsformtimeattempspalurl">Range time MAC Attemps</label><div class="col-sm-9"><div class="input-group m-b"><input type="number" id="macattemptinterval" class="form-control" /><span class="input-group-addon">s</span></div></div></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title text-center">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseEight" class="collapsed" aria-expanded="false"><span id="labelacordionheaderemail">Email Settings</span></a>
                                            </h4>
                                        </div>
                                        <div id="collapseEight" class="panel-collapse collapse" aria-expanded="false">
                                            <div class="panel-body">
                                                <div class="form-horizontal">
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformmailsender">Sender</label><div class="col-sm-9"><input type="text" id="mailfrom" class="form-control" /></div></div>
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformmailserver">SMTP Server</label><div class="col-sm-9"><input type="text" id="smtp" class="form-control" /></div></div>
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformmailport">SMTP Port</label><div class="col-sm-9"><input type="number" id="smtpport" class="form-control" /></div></div>
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformmailssl">SSL Encrypt</label><div class="col-sm-9"><input type="checkbox" id="smtpssl" class="js-switch"/></div></div>
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformmailuser">SMTP Users</label><div class="col-sm-9"><input type="text" id="smtpuser" class="form-control" /></div></div>
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformmailpass">SMTP Password</label><div class="col-sm-9"><input type="text" id="smtppassword" class="form-control" /></div></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title text-center">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseNine" class="collapsed" aria-expanded="false"><span id="labelacordionheadersurvey">Survey Settings</span></a>
                                            </h4>
                                        </div>
                                        <div id="collapseNine" class="panel-collapse collapse" aria-expanded="false">
                                            <div class="panel-body">
                                                <div class="form-horizontal">
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformmandatorysurvey">Mandatory in free access</label><div class="col-sm-9"><input type="checkbox" id="surveycheck" class="js-switch"/></div></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                               
                                     <%--<div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title text-center">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTen" class="collapsed" aria-expanded="false"><span id="labelacordionheadersocialnetworks">Social Netwoks</span></a>
                                            </h4>
                                        </div>
                                        <div id="collapseTen" class="panel-collapse collapse" aria-expanded="false">
                                            <div class="panel-body">
                                                <div class="form-horizontal" id="socialnetwokscheks">
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="label1">Login module</label><div class="col-sm-9"><input type="checkbox" id="Checkbox1" class="js-switch"/></div></div>
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="label2">Free Access module</label><div class="col-sm-9"><input type="checkbox" id="Checkbox2" class="js-switch" /></div></div>
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="label3">Premium Access module</label><div class="col-sm-9"><input type="checkbox" id="Checkbox3" class="js-switch"/></div></div>
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="label4">Social Access module</label><div class="col-sm-9"><input type="checkbox" id="Checkbox4" class="js-switch" /></div></div>
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="label5">Custom Access module</label><div class="col-sm-9"><input type="checkbox" id="Checkbox5" class="js-switch" /></div></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>--%>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title text-center">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseEleven" class="collapsed" aria-expanded="false"><span id="labelacordionheaderdaypass">Site Pass</span></a>
                                            </h4>
                                        </div>
                                        <div id="collapseEleven" class="panel-collapse collapse" aria-expanded="false">
                                            <div class="panel-body">
                                                <div class="form-horizontal" id="daypass">
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelenabledDaylyPass">Enable Site Pass</label><div class="col-sm-9"><input type="checkbox" id="CheckEnabledDaylyPass" class="js-switch"/></div></div>
                                                    <div class="form-group"> <label class="control-label col-sm-3" id="labelvalueDaylyPass">Site Pass</label><div class="col-sm-9"><input type="text" id="valueDaylyPass" class="form-control" /></div></div>
                                                    

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                             </div>                    
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5 id="labelboxheaderbuttonsettings">Actions over settings</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <p>
                            <a href="#" class="btn btn-w-m btn-lg btn-primary btn-block" id="save"><i class="fa fa-save"></i> <span id="labelsettingsbuttonsave"> Save</span></a>
                            <a href="#" role="button" class="btn btn-w-m btn-lg btn-default btn-block" id="back" data-toggle="modal"><i class="fa fa-backward"></i> <span id="labelsettingsbuttonback">Back </span></a>

                        </p>
                    </div>
                </div>
            </div>
            <div class="modal inmodal fade" id="myModalMessage" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                            </div>
                            <div class="modal-body">
                                <div id="message">
                                    
                                </div>
                            </div>
                          <div class="modal-footer">
                            <a href="#" class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Volver</a>
                          </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</asp:Content>
