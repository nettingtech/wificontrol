﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Site.Master" AutoEventWireup="true" CodeBehind="internetusage.aspx.cs" Inherits="Wifi360.internetusage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/calendar.css" rel="stylesheet" />
    <script src="js/calendar.full.min.js"></script>
    <script src="js/moment.js"></script>

    <script>
        function readCookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        };
    </script>
        <script type="text/javascript">

            $(document).ready(function () {

                function bindSelectAttribute(id, valueMember, displayMember, attribute, source) {
                    $("#" + id).find('option').remove().end();
                    var cookie = readCookie("wifi360-language");
                    if (cookie != null) {
                        switch (cookie) {
                            case "es": $("#" + id).append('<option value="0">Seleccionar</option>'); break;
                            default: $("#" + id).append('<option value="0">Select </option>'); break;
                        }
                    }
                    else
                        $("#" + id).append('<option value="0">Select </option>');
                    $.each(source, function (index, item) {
                        $("#" + id).append('<option value="' + item[valueMember] + '" rel="' + item[attribute] + '"> ' + item[displayMember] + '</option>');
                    });
                };

                var cookie = readCookie("wifi360-language");
                if (cookie != null) {
                    switch (cookie) {
                        case "es": var calendar = new CalendarPopup('#start', { dayOfWeekStart: 1, showMask: false, locale: 'es', showWeekIndex: true, format: 'DD/MM/YYYY HH:mm', i18n: { es: { "Chose": "Elegir", "Close": "Cerrar", "Time": "Hora" } } });
                            var calendar2 = new CalendarPopup('#end', { dayOfWeekStart: 1, showMask: false, locale: 'es', showWeekIndex: true, format: 'DD/MM/YYYY HH:mm', i18n: { es: { "Chose": "Elegir", "Close": "Cerrar", "Time": "Hora" } } }); break;
                        default: var calendar = new CalendarPopup('#start', { showMask: false, showWeekIndex: true, format: 'DD/MM/YYYY HH:mm' });
                            var calendar2 = new CalendarPopup('#end', { showMask: false, showWeekIndex: true, format: 'DD/MM/YYYY HH:mm' }); break;
                    }
                }
                else {
                    var calendar = new CalendarPopup('#start', { dayOfWeekStart: 1, showMask: false, showWeekIndex: true, format: 'DD/MM/YYYY HH:mm' });
                    var calendar2 = new CalendarPopup('#end', { dayOfWeekStart: 1, showMask: false, showWeekIndex: true, format: 'DD/MM/YYYY HH:mm' });
                }

                $(".calendar-popup-input-wrapper").attr("style", "display:block;")

                var start = $("#start").val();
                var end = $("#end").val();
                var page = 0;
                var username = $("#user").val();

                var dataStringGroup = 'action=GROUPMEMBERS';
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataStringGroup,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    success: function (pReturn) {
                        var lReturn = JSON.parse(pReturn);
                        bindSelectAttribute("members", "IdMember", "Name", "IdMemberType", lReturn.list);
                        if (lReturn.list.length == 1) {
                            $("#members").val(lReturn.list[0].IdMember).change();;
                        }

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#modal-loading").hide();
                        $("#searchResult").html(xhr.statusText)
                    }
                });

                var dataString = 'action=searchUsersPeriod&s=' + start + '&e=' + end + '&page=' + page + '&username=' + username + '&idmember=0&idmembertype=0';
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    beforeSend: function () {
                        $("#myModalWait").modal('show');
                    },
                    complete: function () {
                        $("#myModalWait").modal('hide');
                    },
                    success: function (pReturn) {
                        var lReturn = JSON.parse(pReturn);
                        var table = "";
                        var cookie = readCookie("wifi360-language");
                        if (cookie != null) {
                            switch (cookie) {
                                case "es": table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th class='hidden-xs'>Habitación</th><th>Nombre de usuario</th><th class='hidden-xs'>Contraseña</th><th>Tipo de Acceso</th><th class='hidden-xs'>Fecha de inicio</th><th class='hidden-xs'>Fecha de finalización</th><th class='hidden-xs'>Crédito de tiempo</th><th></th></tr>"; break;
                                default: table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th class='hidden-xs'>Room</th><th>Username</th><th class='hidden-xs'>Password</th><th>Access Type</th><th class='hidden-xs'>Valid Since</th><th class='hidden-xs'>Valid Till</th><th class='hidden-xs'>Time Credit</th><th></th></tr>"; break;
                            }
                        }
                        else
                            table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th class='hidden-xs'>Room</th><th>Username</th><th class='hidden-xs'>Password</th><th>Access Type</th><th class='hidden-xs'>Valid Since</th><th class='hidden-xs'>Valid Till</th><th class='hidden-xs'>Time Credit</th><th></th></tr>";

                        $.each(lReturn.list, function (index, item) {
                            table += '<tr><td class="hidden-xs">' + item["Room"] + '</td><td>' + item["UserName"] + '</td><td class="hidden-xs">' + item["Password"] + '</td><td>' + item["AccessType"] + '</td><td class="hidden-xs">' + item["ValidSince"] + '</td><td class="hidden-xs">' + item["ValidTill"] + '</td><td class="hidden-xs">' + item["TimeCredit"] + '</td><td><a href="/UserDetails.aspx?id=' + item["IdUser"] + '"><i class="fa fa-search-plus"></i></td></tr>';
                        });
                        table += "</table>";


                        $("#searchResult").html(table);

                        var pagination = "<ul class='pagination'>";
                        var points1 = 0;
                        var points2 = 0;
                        x = parseInt(page);
                        for (var index = 0; index < lReturn.pageNumber; index++) {
                            if (lReturn.pageNumber <= 10) {
                                if (page == index)
                                    pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                else
                                    pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                            }
                            else {
                                if (page == index) {
                                    pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                                else if (index == 0) {
                                    pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                                else if (index == (lReturn.pageNumber - 1)) {
                                    pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                                else if (index < (x - 3)) {
                                    if (points1 == 0) {
                                        pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                        points1 = 1;
                                    }
                                }
                                else if (index > (x + 3)) {
                                    if (points2 == 0) {
                                        pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                        points2 = 1;
                                    }
                                }
                                else
                                    pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                            }
                        }

                        pagination += "</ul>";

                        $("#searchResult").append(pagination);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#modal-loading").hide();
                        $("#searchResult").html(xhr.statusText)
                    }
                });

                $(document).on("click", 'a[rel^="page"]', function (e) {
                    e.preventDefault();
                    var page = $(this).attr("attr");

                    var start = $("#start").val();
                    var end = $("#end").val();
                    var username = $("#user").val();
                    var idmember = $("#members").val();
                    var idmembertype = $('option:selected', $("#members")).attr('rel');

                    var dataString = 'action=searchUsersPeriod&s=' + start + '&e=' + end + '&page=' + page + '&username=' + username + '&idmember=' + idmember + '&idmembertype=' + idmembertype;
                    $.ajax({
                        url: "handlers/SMIHandler.ashx",
                        data: dataString,
                        contentType: "application/json; charset=utf-8",
                        dataType: "text",
                        beforeSend: function () {
                            $("#myModalWait").modal('show');
                        },
                        complete: function () {
                            $("#myModalWait").modal('hide');
                        },
                        success: function (pReturn) {
                            var lReturn = JSON.parse(pReturn);
                            var table = "";
                            var cookie = readCookie("wifi360-language");
                            if (cookie != null) {
                                switch (cookie) {
                                    case "es": table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th class='hidden-xs'>Habitación</th><th>Nombre de usuario</th><th class='hidden-xs'>Contraseña</th><th>Tipo de Acceso</th><th class='hidden-xs'>Fecha de inicio</th><th class='hidden-xs'>Fecha de finalización</th><th class='hidden-xs'>Crédito de tiempo</th><th></th></tr>"; break;
                                    default: table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th class='hidden-xs'>Room</th><th>Username</th><th class='hidden-xs'>Password</th><th>Access Type</th><th class='hidden-xs'>Valid Since</th><th class='hidden-xs'>Valid Till</th><th class='hidden-xs'>Time Credit</th><th></th></tr>"; break;
                                }
                            }
                            else
                                table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th class='hidden-xs'>Room</th><th>Username</th><th class='hidden-xs'>Password</th><th>Access Type</th><th class='hidden-xs'>Valid Since</th><th class='hidden-xs'>Valid Till</th><th class='hidden-xs'>Time Credit</th><th></th></tr>";

                            $.each(lReturn.list, function (index, item) {
                                table += '<tr><td class="hidden-xs">' + item["Room"] + '</td><td>' + item["UserName"] + '</td><td class="hidden-xs">' + item["Password"] + '</td><td>' + item["AccessType"] + '</td><td class="hidden-xs">' + item["ValidSince"] + '</td><td class="hidden-xs">' + item["ValidTill"] + '</td><td class="hidden-xs">' + item["TimeCredit"] + '</td><td><a href="/UserDetails.aspx?id=' + item["IdUser"] + '"><i class="fa fa-search-plus"></i> </td></tr>';
                            });
                            table += "</table>";


                            $("#searchResult").html(table);

                            var pagination = "<ul class='pagination'>";
                            var points1 = 0;
                            var points2 = 0;
                            x = parseInt(page);
                            for (var index = 0; index < lReturn.pageNumber; index++) {
                                if (lReturn.pageNumber <= 10) {
                                    if (page == index)
                                        pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                    else
                                        pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                                else {
                                    if (page == index) {
                                        pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                    }
                                    else if (index == 0) {
                                        pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                    }
                                    else if (index == (lReturn.pageNumber - 1)) {
                                        pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                    }
                                    else if (index < (x - 3)) {
                                        if (points1 == 0) {
                                            pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                            points1 = 1;
                                        }
                                    }
                                    else if (index > (x + 3)) {
                                        if (points2 == 0) {
                                            pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                            points2 = 1;
                                        }
                                    }
                                    else
                                        pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                            }

                            pagination += "</ul>";

                            $("#searchResult").append(pagination);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#modal-loading").hide();
                            $("#searchResult").html(xhr.statusText)
                        }
                    });
                });

                $("#search").on("click", function () {

                    var start = $("#start").val();
                    var end = $("#end").val();
                    var s = moment(start, "DD/MM/yyyy HH:mm");
                    var e = moment(end, "DD/MM/yyyy HH:mm");
                    var page = 0;
                    var username = $("#user").val();
                    var idmember = $("#members").val();
                    var idmembertype = $('option:selected', $("#members")).attr('rel');

                    if ((($("#start").val() == '') && ($("#end").val() == '')) || (s <= e) && ($("#start").val() != '') && ($("#end").val() != '')) {
                        var dataString = 'action=searchUsersPeriod&s=' + start + '&e=' + end + '&page=' + page + '&username=' + username + '&idmember=' + idmember + '&idmembertype=' + idmembertype;
                        $.ajax({
                            url: "handlers/SMIHandler.ashx",
                            data: dataString,
                            contentType: "application/json; charset=utf-8",
                            dataType: "text",
                            beforeSend: function () {
                                $("#myModalWait").modal('show');
                            },
                            complete: function () {
                                $("#myModalWait").modal('hide');
                            },
                            success: function (pReturn) {
                                var lReturn = JSON.parse(pReturn);
                                var table = "";
                                var cookie = readCookie("wifi360-language");
                                if (cookie != null) {
                                    switch (cookie) {
                                        case "es": table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th class='hidden-xs'>Habitación</th><th>Nombre de usuario</th><th class='hidden-xs'>Contraseña</th><th>Tipo de Acceso</th><th class='hidden-xs'>Fecha de inicio</th><th class='hidden-xs'>Fecha de finalización</th><th class='hidden-xs'>Crédito de tiempo</th><th></th></tr>"; break;
                                        default: table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th class='hidden-xs'>Room</th><th>Username</th><th class='hidden-xs'>Password</th><th>Access Type</th><th class='hidden-xs'>Valid Since</th><th class='hidden-xs'>Valid Till</th><th class='hidden-xs'>Time Credit</th><th></th></tr>"; break;
                                    }
                                }
                                else
                                    table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th class='hidden-xs'>Room</th><th>Username</th><th class='hidden-xs'>Password</th><th>Access Type</th><th class='hidden-xs'>Valid Since</th><th class='hidden-xs'>Valid Till</th><th class='hidden-xs'>Time Credit</th><th></th></tr>";

                                $.each(lReturn.list, function (index, item) {
                                    table += '<tr><td class="hidden-xs">' + item["Room"] + '</td><td>' + item["UserName"] + '</td><td class="hidden-xs">' + item["Password"] + '</td><td>' + item["AccessType"] + '</td><td class="hidden-xs">' + item["ValidSince"] + '</td><td class="hidden-xs">' + item["ValidTill"] + '</td><td class="hidden-xs">' + item["TimeCredit"] + '</td><td><a href="/UserDetails.aspx?id=' + item["IdUser"] + '"><i class="fa fa-search-plus"></i> </td></tr>';
                                });
                                table += "</table>";


                                $("#searchResult").html(table);

                                var pagination = "<ul class='pagination'>";
                                var points1 = 0;
                                var points2 = 0;
                                x = parseInt(page);
                                for (var index = 0; index < lReturn.pageNumber; index++) {
                                    if (lReturn.pageNumber <= 10) {
                                        if (page == index)
                                            pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                        else
                                            pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                    }
                                    else {
                                        if (page == index) {
                                            pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                        }
                                        else if (index == 0) {
                                            pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                        }
                                        else if (index == (lReturn.pageNumber - 1)) {
                                            pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                        }
                                        else if (index < (x - 3)) {
                                            if (points1 == 0) {
                                                pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                                points1 = 1;
                                            }
                                        }
                                        else if (index > (x + 3)) {
                                            if (points2 == 0) {
                                                pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                                points2 = 1;
                                            }
                                        }
                                        else
                                            pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                    }
                                }

                                pagination += "</ul>";

                                $("#searchResult").append(pagination);
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                $("#modal-loading").hide();
                                $("#searchResult").html(xhr.statusText)
                            }
                        });
                    }
                    else {
                        $("#message").html(GetMessage("0x0022"));
                        $("#myModal").modal('show');
                    }

                });
            });



    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 id="headerpageinternetusage">Analytics</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="default.aspx">Dashboard</a>
                </li>
                <li>
                    <a id="breadpagefatherinternetusage">Analytics</a>
                </li>
                <li class="active">
                    <strong id="breadpageinternetusage">Internet usage</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5 id="labelboxheaderinternetusage">Internet usage</h5>
                </div>
                 <div class="ibox-content">
                    <div class="ibox border-bottom">
                    <div class="ibox-title">
                        <h5 id="headerboxfilterusagestatistics">Search filters</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-down"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content" style="display: none;">
                    <div role="form">
                        <div class="form-group">
                            <label for="username" id="labelformusermanagementgroupmembers">Group Members</label>
                            <select id="members" class="form-control"></select>
                        </div>
                            <div class="form-group" id="data_5">
                                <label class="font-normal" id="labelforminternetusagebillingtype">Date range</label>
                                <div class="input-daterange input-group" id="datepicker">
                                    <span class="input-group-addon" id="labelforminternetusagedatefrom"> from </span>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="input-sm form-control" id="start" name="start" value=""/>
                                    <span class="input-group-addon" id="labelforminternetusagedatesince"> to </span>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="input-sm form-control" id="end" name="end" value="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="font-normal control-label" id="labelforminternetusageusername">Username </label>
                                <input type="text" id="user" class="form-control" />
                            </div>
                            <div class="form-group">
                                <a href="#" id="search" class="btn btn-w-m btn-primary" ><i class="fa fa-search"></i> <span id="labelforminternetusagebuttonsearch"> Search</span> </a>
                            </div>
                    </div>
                    </div>
                    </div>
                    
                    <div id="searchResult"></div>

                        </div>
                    </div>
                </div>
            </div>
        
                <div class="modal inmodal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Cerrar</span></button>
                                <h4 class="modal-title">Error</h4>
                            </div>
                            <div class="modal-body">
                                <p class="alert alert-danger" id="message"></p>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
</asp:Content>
