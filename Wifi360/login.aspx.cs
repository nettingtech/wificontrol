﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Wifi360
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string[] request = Page.Request.Url.Host.Split('.');
            if (File.Exists(Path.Combine(Server.MapPath(string.Format("~/img/portales/login_{0}.css", request[0])))))
            {
                var link = new HtmlLink();

                link.Href = string.Format("~/img/portales/login_{0}.css", request[0]);
                link.Attributes.Add("rel", "stylesheet");
                link.Attributes.Add("type", "text/css");
                Page.Header.Controls.Add(link);
            }
        }
    }
}