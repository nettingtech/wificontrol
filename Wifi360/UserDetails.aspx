﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Site.Master" AutoEventWireup="true" CodeBehind="UserDetails.aspx.cs" Inherits="Wifi360.UserDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
            <script type="text/javascript">
                $(document).ready(function () {
                    var parameters = getParameters();

                    function drawChartActivity(data) {
                        var options = {
                            hAxis: null
                        };

                        var chart = new google.visualization.AreaChart(document.getElementById('chart_stadistics'));

                        chart.draw(data, options);
                    }

                    var dataString = 'action=userdetails&id=' + parameters.id;
                    $.ajax({
                        url: "handlers/SMIHandler.ashx",
                        data: dataString,
                        contentType: "application/json; charset=utf-8",
                        dataType: "text",
                        beforeSend: function () {
                            $("#myModalWait").modal('show');
                        },
                        complete: function () {
                            $("#myModalWait").modal('hide');
                        },
                        success: function (pReturn) {
                            var lReturn = JSON.parse(pReturn);
                            $("#UserName").html(lReturn.UserName);
                            $("#Password").html(lReturn.Password);
                            $("#Room").html(lReturn.Room);
                            $("#AccessType").html(lReturn.AccessType);
                            $("#ValidTill").html(lReturn.ValidTill);
                            $("#ValidSince").html(lReturn.ValidSince);
                            $("#TimeCredit").html(lReturn.TimeCredit);
                            $("#Priority").html(lReturn.Priority);
                            $("#BwUp").html(lReturn.BWUp);
                            $("#BwDown").html(lReturn.BWDown);
                            $("#VolumeUp").html(lReturn.VolumeUp);
                            $("#VolumeDown").html(lReturn.VolumeDown);
                            $("#MaxDevices").html(lReturn.MaxDevices);
                            $("#Totals").html("Total Devices: <b> " + lReturn.TotalMacs + " </b>; Total time: <b> " + lReturn.TotalTime + "  s.</b>; Total Bytes Down: <b>" + lReturn.TotalBytesIn + "</b>; Total Bytes Up: <b>" + lReturn.TotalBytesOut + "</b>");

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#modal-loading").hide();
                            $("#searchResult").html(xhr.statusText)
                        }
                    });

                    var dataString = 'action=closedsessions&iduser=' + parameters.id;
                    $.ajax({
                        url: "handlers/SMIHandler.ashx",
                        data: dataString,
                        contentType: "application/json; charset=utf-8",
                        dataType: "text",
                        beforeSend: function () {
                            $("#myModalWait").modal('show');
                        },
                        complete: function () {
                            $("#myModalWait").modal('hide');
                        },
                        success: function (pReturn) {
                            var lReturn = JSON.parse(pReturn);
                            $("#resultSearch").html("")
                            var table = ""
                            table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Start</th><th>Finish</th><th class='hidden-xs'>Bytes In</th><th class='hidden-xs'>Bytes Out</th><th>MAC</th></tr>";
                            $.each(lReturn.list, function (index, item) {
                                table += '<tr><td>' + item["Start"] + '</td><td>' + item["Finish"] + '</td><td class="hidden-xs">' + item["BytesIn"] + '</td><td class="hidden-xs">' + item["BytesOut"] + '</td><td>' + item["MAC"] + '</td></tr>';
                            });
                            table += "</table>";
                            $("#resultSearch").html(table)

                            $("#modal-loading").hide();

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#modal-loading").hide();
                            $("#searchResult").html(xhr.statusText)
                        }
                    });


                    $("#details").on("click", function (e) {
                        $('#myModalDetails').modal('show');
                    });

                    $("#back").on("click", function (e) {
                        var url = 'internetusage.aspx';

                        window.location.href = url;
                    });

                });


                function getParameters() {
                    var searchString = window.location.search.substring(1)
                      , params = searchString.split("&")
                      , hash = {}
                    ;

                    for (var i = 0; i < params.length; i++) {
                        var val = params[i].split("=");
                        hash[unescape(val[0])] = unescape(val[1]);
                    }
                    return hash;
                }

                function bindSelect(id, valueMember, displayMember, source) {
                    $("#" + id).find('option').remove().end();
                    $("#" + id).append('<option value="0">Seleccionar</option>');
                    $.each(source, function (index, item) {
                        $("#" + id).append('<option value="' + item[valueMember] + '"> ' + item[displayMember] + '</option>');
                    });
                };


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>User Usage Details</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="default.aspx">Dashboard</a>
                </li>
                <li>
                    <a>Usage & Statistics</a>
                </li>
                <li>
                    <a href="internetusage.aspx">Internet Usage</a>
                </li>
                <li class="active">
                    <strong>User Usage Details</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-9">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                    <h5>User Usage Details<small> Details of User</small></h5>
                </div>
                    <div class="ibox-content">
                        <div class="form-horizontal">
                    <div class="form-group"> <label class="control-label col-sm-2">Room</label><div class="col-sm-10"><label class="form-control" id="Room" >Room</label></div></div>
                    <div class="form-group"> <label class="control-label col-sm-2">UserName</label><div class="col-sm-10"><label class="form-control" id="UserName" /></div></div>
                    <div class="form-group"> <label class="control-label col-sm-2">Password</label><div class="col-sm-10"><label class="form-control" id="Password" /></div></div>
                    <div class="form-group"> <label class="control-label col-sm-2">AccessType</label><div class="col-sm-10"><label class="form-control" id="AccessType">AccessType</label></div></div>
                    <div class="form-group"> <label class="control-label col-sm-2">Priority</label><div class="col-sm-10"><label class="form-control" id="Priority"></label></div></div>
                    <div class="form-group"> <label class="control-label col-sm-2">BandWidth Down</label><div class="col-sm-10"><label class="form-control" id="BwDown"></label></div></div>
                    <div class="form-group"> <label class="control-label col-sm-2">Bandwidth UP</label><div class="col-sm-10"><label class="form-control" id="BwUp"></label></div></div>
                    <div class="form-group"> <label class="control-label col-sm-2">Volume Down</label><div class="col-sm-10"><label class="form-control" id="VolumeDown"></label></div></div>
                    <div class="form-group"> <label class="control-label col-sm-2">Volume Up</label><div class="col-sm-10"><label class="form-control" id="VolumeUp"></label></div></div>
                    <div class="form-group"> <label class="control-label col-sm-2">MaxDevices</label><div class="col-sm-10"><label class="form-control" id="MaxDevices"></label></div></div>
                    <div class="form-group"> <label class="control-label col-sm-2">Valid Since</label><div class="col-sm-10"><label class="form-control" id="ValidSince">ValidSince</label></div></div>
                    <div class="form-group"> <label class="control-label col-sm-2">Valid Till</label><div class="col-sm-10"><label class="form-control" id="ValidTill">ValidTill</label></div></div>
                    <div class="form-group"> <label class="control-label col-sm-2">Time Credit</label><div class="col-sm-10"><label class="form-control" id="TimeCredit">TimeCredit</label></div></div>
                    
                    <div class="form-group"> <label class="control-label col-sm-2">Total Used</label><div class="col-sm-10"><label class="form-control" id="Totals">Totals</label></div></div>
                </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Actions over User</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <p>
                            <a href="#" class="btn btn-w-m btn-lg btn-primary btn-block" id="details"><i class="fa fa-search-plus"></i> Details</a>
                            <a href="#" role="button" class="btn btn-w-m btn-lg btn-default btn-block" id="back" data-toggle="modal"><i class="fa fa-backward"></i> Back</a>

                        </p>
                    </div>
                </div>
            </div>

            <div class="modal inmodal fade" id="myModalMessage" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                            </div>
                            <div class="modal-body">
                                <div id="message">
                                    
                                </div>
                            </div>
                          <div class="modal-footer">
                            <a href="#" class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Back</a>
                          </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>

        <div id="myModalDetails" class="modal inhide fade" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                            <h3 id="H2">User Conections Details</h3>
                          </div>
                          <div class="modal-body">
                              <div id="resultSearch" style="max-height:400px;overflow-y:auto;"></div>
                          </div>
                          <div class="modal-footer">
                            <a href="#" class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Back</a>
                          </div>
                        </div>
                        </div>
        </div>

</asp:Content>
