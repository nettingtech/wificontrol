﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Site.Master" AutoEventWireup="true" CodeBehind="BigData.aspx.cs" Inherits="Wifi360.BigData" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        /*.axis--x path {
          display: none;
        }*/

        .line {
          fill: none;
          stroke: steelblue;
          stroke-width: 1.5px;
        }



</style>
    <script src="js/d3.v4.min.js"></script>
    <script src="js/d3pie.js"></script> 
    
    <script src='https://api.mapbox.com/mapbox.js/v3.1.1/mapbox.js'></script>
    <link href='https://api.mapbox.com/mapbox.js/v3.1.1/mapbox.css' rel='stylesheet' />

    <script src='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v1.0.0/leaflet.markercluster.js'></script>
    <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v1.0.0/MarkerCluster.css' rel='stylesheet' />
    <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v1.0.0/MarkerCluster.Default.css' rel='stylesheet' />
<script>
    var dataString = 'action=SNMP_MIKROTIK';
    $.ajax({
        url: "handlers/BigDataHandler.ashx",
        data: dataString,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $("#myModalWait").modal('show');
        },
        complete: function () {
            $("#myModalWait").modal('hide');
        },
        success: function (msg) {
            if (msg.code == "OK") {

                var svg = d3.select("svg"),
                margin = { top: 20, right: 80, bottom: 30, left: 50 },
                width = svg.attr("width") - margin.left - margin.right,
                height = svg.attr("height") - margin.top - margin.bottom,
                g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                var parseTime = d3.timeParse("%Y%m%d%H%M%S");

                var x = d3.scaleTime().range([0, width]),
                    y = d3.scaleLinear().range([height, 0]),
                    z = d3.scaleOrdinal(d3.schemeCategory10);

                var line = d3.line()
                    .curve(d3.curveBasis)
                    .x(function (d) { return x(d.date); })
                    .y(function (d) { return y(d.Value); });

                
               // d3.selection.data(data);
                d3.tsv(msg.message, type, function (error, data) {
                //d3.tsv("data.tsv", type, function (error, data) {
                    if (error) throw error;

                    var cities = data.columns.slice(1).map(function (id) {
                        return {
                            id: id,
                            values: data.map(function (d) {
                                return { date: d.date, Value: d[id] };
                            })
                        };
                    });

                    x.domain(d3.extent(data, function (d) { return d.date; }));

                    y.domain([
                      d3.min(cities, function (c) { return d3.min(c.values, function (d) { return d.Value; }); }),
                      d3.max(cities, function (c) { return d3.max(c.values, function (d) { return d.Value; }); })
                    ]);

                    z.domain(cities.map(function (c) { return c.id; }));

                    g.append("g")
                        .attr("class", "axis axis--x")
                        .attr("transform", "translate(0," + height + ")")
                        .call(d3.axisBottom(x));

                    g.append("g")
                        .attr("class", "axis axis--y")
                        .call(d3.axisLeft(y))
                      .append("text")
                        .attr("transform", "rotate(-90)")
                        .attr("y", 6)
                        .attr("dy", "0.71em")
                        .attr("fill", "#000")
                        .text("Value, Number");

                    var city = g.selectAll(".city")
                      .data(cities)
                      .enter().append("g")
                        .attr("class", "city");

                    city.append("path")
                        .attr("class", "line")
                        .attr("d", function (d) { return line(d.values); })
                        .style("stroke", function (d) { return z(d.id); });

                    city.append("text")
                        .datum(function (d) { return { id: d.id, value: d.values[d.values.length - 1] }; })
                        .attr("transform", function (d) { return "translate(" + x(d.value.date) + "," + y(d.value.Value) + ")"; })
                        .attr("x", 3)
                        .attr("dy", "0.35em")
                        .style("font", "10px sans-serif")
                        .text(function (d) { return d.id; });
                });

                function type(d, _, columns) {
                    d.date = parseTime(d.date);
                    for (var i = 1, n = columns.length, c; i < n; ++i) d[c = columns[i]] = +d[c];
                    return d;
                }
            }
            else {
                $("#message").removeClass().addClass("alert alert-danger");
                $("#message").html(msg.message);

                

            }
            $("#myModalMessage").modal('show');

        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#message").removeClass().addClass("alert alert-danger");
            $("#message").html(xhr.statusText);
            $("#myModalMessage").modal('show');

        }
    });

</script>

<script>
    $(document).ready(function (e) {
        var dataString = 'action=CLOSEDSESSIONS';
        $.ajax({
            url: "handlers/BigDataHandler.ashx",
            data: dataString,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $("#myModalWait").modal('show');
            },
            complete: function () {
                $("#myModalWait").modal('hide');
            },
            success: function (pReturn) {
                var pie = new d3pie("pie", {
                    data: {
                        content: pReturn.items
                    }
                });

            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#message").removeClass().addClass("alert alert-danger");
                $("#message").html(xhr.statusText);
                $("#myModalMessage").modal('show');

            }
        });

        var dataString = 'action=ONLINEDEVICE';
        $.ajax({
            url: "handlers/BigDataHandler.ashx",
            data: dataString,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $("#myModalWait").modal('show');
            },
            complete: function () {
                $("#myModalWait").modal('hide');
            },
            success: function (msg) {
                if (msg.code == "OK") {

                    var svg = d3.select("svg"),
                    margin = { top: 20, right: 80, bottom: 30, left: 50 },
                    width = svg.attr("width") - margin.left - margin.right,
                    height = svg.attr("height") - margin.top - margin.bottom,
                    g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                    var parseTime = d3.timeParse("%Y%m%d%H%M%S");

                    var x = d3.scaleTime().range([0, width]),
                        y = d3.scaleLinear().range([height, 0]),
                        z = d3.scaleOrdinal(d3.schemeCategory10);

                    var line = d3.line()
                        .curve(d3.curveBasis)
                        .x(function (d) { return x(d.date); })
                        .y(function (d) { return y(d.Value); });


                    // d3.selection.data(data);
                    d3.tsv(msg.message, type, function (error, data) {
                        //d3.tsv("data.tsv", type, function (error, data) {
                        if (error) throw error;

                        var cities = data.columns.slice(1).map(function (id) {
                            return {
                                id: id,
                                values: data.map(function (d) {
                                    return { date: d.date, Value: d[id] };
                                })
                            };
                        });

                        x.domain(d3.extent(data, function (d) { return d.date; }));

                        y.domain([
                          d3.min(cities, function (c) { return d3.min(c.values, function (d) { return d.Value; }); }),
                          d3.max(cities, function (c) { return d3.max(c.values, function (d) { return d.Value; }); })
                        ]);

                        z.domain(cities.map(function (c) { return c.id; }));

                        g.append("g")
                            .attr("class", "axis axis--x")
                            .attr("transform", "translate(0," + height + ")")
                            .call(d3.axisBottom(x));

                        g.append("g")
                            .attr("class", "axis axis--y")
                            .call(d3.axisLeft(y))
                          .append("text")
                            .attr("transform", "rotate(-90)")
                            .attr("y", 6)
                            .attr("dy", "0.71em")
                            .attr("fill", "#000")
                            .text("Value, Number");

                        var city = g.selectAll(".city")
                          .data(cities)
                          .enter().append("g")
                            .attr("class", "city");

                        city.append("path")
                            .attr("class", "line")
                            .attr("d", function (d) { return line(d.values); })
                            .style("stroke", function (d) { return z(d.id); });

                        city.append("text")
                            .datum(function (d) { return { id: d.id, value: d.values[d.values.length - 1] }; })
                            .attr("transform", function (d) { return "translate(" + x(d.value.date) + "," + y(d.value.Value) + ")"; })
                            .attr("x", 3)
                            .attr("dy", "0.35em")
                            .style("font", "10px sans-serif")
                            .text(function (d) { return d.id; });
                    });

                    function type(d, _, columns) {
                        d.date = parseTime(d.date);
                        for (var i = 1, n = columns.length, c; i < n; ++i) d[c = columns[i]] = +d[c];
                        return d;
                    }
                }
                else {
                    $("#message").removeClass().addClass("alert alert-danger");
                    $("#message").html(msg.message);
                }

            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#message").removeClass().addClass("alert alert-danger");
                $("#message").html(xhr.statusText);
                $("#myModalMessage").modal('show');

            }
        });
    });
</script>

    <script>
        $(document).ready(function (e) {
            var dataString = 'action=UsersLocation';
        $.ajax({
            url: "handlers/BigDataHandler.ashx",
            data: dataString,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $("#myModalWait").modal('show');
            },
            complete: function () {
                $("#myModalWait").modal('hide');
            },
            success: function (pReturn) {
                L.mapbox.accessToken = 'pk.eyJ1IjoiamF2aWFycm9jaGEiLCJhIjoiY2ptaXY2Ymw5MDhndTNxbmptcGo1cnAycCJ9.YMORyd3rDYpXSDmIVeuiCw';
                var map = L.mapbox.map('map', 'mapbox.streets')
                    .setView([40.4378698, -3.8196191], 6);
               
                var markers = new L.MarkerClusterGroup();

                for (var i = 0; i < pReturn.items.length; i++) {
                    var a = pReturn.items[i];
                    var title = a[2];
                    var marker = L.marker(new L.LatLng(a[0], a[1]), {
                        icon: L.mapbox.marker.icon({ 'marker-symbol': 'mobilephone', 'marker-color': '0044FF' }),
                        title: title
                    });
                    marker.bindPopup(title);
                    markers.addLayer(marker);
                }

                map.addLayer(markers);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#message").removeClass().addClass("alert alert-danger");
                $("#message").html(xhr.statusText);
                $("#myModalMessage").modal('show');

            }
        });
        });

    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div class="row">
        <div class="col-sm-6">
            <svg width="960" height="500"></svg>
        </div>
        <div class="col-sm-6">
            <div id="pie"></div>
        </div>
    <div class="col-sm-12">
    <hr />
            <div id="map" style="width:800px; height:600px; margin-bottom:50px;"></div>
        </div>
    </div>
</asp:Content>
