﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Site.Master" AutoEventWireup="true" CodeBehind="User.aspx.cs" Inherits="Wifi360.User" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        
    <link href="css/calendar.css" rel="stylesheet" />
    <script src="js/calendar.full.min.js"></script>
    <script src="js/moment.js"></script>

    <script type="text/javascript">

            function ilegalCharacter(string) {
                if (/^[a-zA-Z0-9-@_.:]*$/.test(string) == false) {
                    return false;
                }
            };

            function readCookie(name) {
                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
                }
                return null;
            };

            $(document).ready(function () {
                var parameters = getParameters();

                $("#returnPage").attr('href', parameters.return);
               
                var cookie = readCookie("wifi360-language");
                var rol = readCookie("FDSUserRol");
                if (rol == "3")
                {
                    $("#UserName").attr("disabled", "disabled");
                    $("#Password").attr("disabled", "disabled");
                    $("#ValidSince").attr("disabled", "disabled");
                    $("#ValidTill").attr("disabled", "disabled");
                    $("#Priority").attr("disabled", "disabled");
                    
                    $("#extended").hide();
                    $("#upgrade").hide();
                }
                else if (rol == "5") {
                    $("#UserName").attr("disabled", "disabled");
                    $("#Password").attr("disabled", "disabled");
                    $("#ValidSince").attr("disabled", "disabled");
                    $("#ValidTill").attr("disabled", "disabled");
                    $("#Priority").attr("disabled", "disabled");

                    $("#extended").hide();
                    $("#upgrade").hide();
                    $("#save").hide();
                    $("#print").hide();
                    $("#delete").hide();

                }

                if (parameters.return == "DisabledUsers.aspx") {
                    $("#delete").hide();
                    $("#save").hide();
                    $("#upgrade").hide();
                    $("#UserName").attr("disabled", "disabled");
                    $("#Password").attr("disabled", "disabled");

                    if (cookie != null) {
                        switch (cookie) {
                            case "es": $("#returnPage").html("Usuarios deshabilitados"); break;
                            default: $("#returnPage").html("Disabled users"); break;
                        }
                    }
                    else
                        $("#returnPage").html("Disabled users");

                    $("#lidisabledusers").attr("class", "active");
                }
                else if (parameters.return == "iot.aspx") {
                    if (cookie != null) {
                        switch (cookie) {
                            case "es": $("#returnPage").html("Accesos válidos"); break;
                            default: $("#returnPage").html("Valid Accesses"); break;
                        }
                    }
                    else
                        $("#returnPage").html("Disabled users");

                }
                else if (parameters.return == "DisabledUsersiot.aspx") {
                    $("#delete").hide();
                    $("#save").hide();
                    $("#upgrade").hide();
                    $("#UserName").attr("disabled", "disabled");
                    $("#Password").attr("disabled", "disabled");

                    if (cookie != null) {
                        switch (cookie) {
                            case "es": $("#returnPage").html("Usuarios deshabilitados"); break;
                            default: $("#returnPage").html("Disabled users"); break;
                        }
                    }
                    else
                        $("#returnPage").html("Disabled users");

                }
                else {
                    $("#limanagement").attr("class", "active");
                    if (cookie != null) {
                        switch (cookie) {
                            case "es": $("#returnPage").html("Usuarios válidos"); break;
                            default: $("#returnPage").html("Valid users"); break;
                        }
                    }
                    else
                        $("#returnPage").html("Valid users");

                }

                var calendar;
                var calendar2;
                var cookie = readCookie("wifi360-language");
                if (cookie != null) {
                    switch (cookie) {
                        case "es": calendar = new CalendarPopup('#ValidSince', { dayOfWeekStart: 1, showMask: false, locale: 'es', showWeekIndex: true, format: 'DD/MM/YYYY HH:mm', i18n: { es: { "Chose": "Elegir", "Close": "Cerrar", "Time": "Hora" } } });
                            calendar2 = new CalendarPopup('#ValidTill', { dayOfWeekStart: 1, showMask: false, locale: 'es', showWeekIndex: true, format: 'DD/MM/YYYY HH:mm', i18n: { es: { "Chose": "Elegir", "Close": "Cerrar", "Time": "Hora" } } }); break;
                        default: calendar = new CalendarPopup('#ValidSince', { showMask: false, showWeekIndex: true, format: 'DD/MM/YYYY HH:mm' });
                            calendar2 = new CalendarPopup('#ValidTill', { showMask: false, showWeekIndex: true, format: 'DD/MM/YYYY HH:mm' }); break;
                    }
                }
                else {
                    calendar = new CalendarPopup('#ValidSince', { dayOfWeekStart: 1, showMask: false, showWeekIndex: true, format: 'DD/MM/YYYY HH:mm' });
                    calendar2 = new CalendarPopup('#ValidTill', { dayOfWeekStart: 1, showMask: false, showWeekIndex: true, format: 'DD/MM/YYYY HH:mm' });
                }

                var dataString3 = 'action=prioritiesUserFilter&iduser=' + parameters.id;
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataString3,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    success: function (pReturn) {
                        var lReturn = JSON.parse(pReturn);
                        bindSelect('Priority', 'IdPriority', 'Description', lReturn.list);

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#modal-loading").hide();
                        $("#searchResult").html(xhr.statusText)
                    }
                });

                var dataString2 = 'action=filterprofilesUserFilter&iduser=' + parameters.id;
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataString2,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    success: function (pReturn2) {
                        var lReturn2 = JSON.parse(pReturn2);
                        bindSelectNone('filterprofile', 'IdFilter', 'Description', lReturn2.list);

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#modal-loading").hide();
                        $("#searchResult").html(xhr.statusText)
                    }
                });

                var dataString = 'action=billingTypesUserFilter&iduser=' + parameters.id;
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    beforeSend: function () {
                        $("#myModalWait").modal('show');
                    },
                    complete: function () {
                        $("#myModalWait").modal('hide');
                    },
                    success: function (pReturn) {
                        var lReturn = JSON.parse(pReturn);
                        bindSelect('accesstype', 'IdBillingType', 'Description', lReturn.list);
                        bindSelect('accesstype2', 'IdBillingType', 'Description', lReturn.list);
                        $("#modal-loading").hide();

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#modal-loading").hide();
                        $("#searchResult").html(xhr.statusText)
                    }
                });

                var dataString = 'action=user&id=' + parameters.id;
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    beforeSend: function () {
                        $("#myModalWait").modal('show');
                    },
                    complete: function () {
                        $("#myModalWait").modal('hide');
                    },
                    success: function (pReturn) {
                        var lReturn = JSON.parse(pReturn);
                        $("#UserName").val(lReturn.UserName);
                        $("#Password").val(lReturn.Password);
                        $("#Room").html(lReturn.Room);
                        $("#AccessType").html(lReturn.AccessType);
                        calendar2.value.set(lReturn.ValidTill);
                        $("#ValidTill").val(lReturn.ValidTill);
                        calendar.value.set(lReturn.ValidSince);
                        $("#ValidSince").val(lReturn.ValidSince);
                        $("#TimeCredit").html(lReturn.TimeCreditFormatted);
                        $("#Priority").val(lReturn.IdPriority);
                        $("#BwUp").html(lReturn.BWUp);
                        $("#BwDown").html(lReturn.BWDown);
                        $("#VolumeUp").html(lReturn.VolumeUp + ' MB');
                        $("#VolumeDown").html(lReturn.VolumeDown + ' MB');
                        $("#VolumeCombined").html(lReturn.VolumeDown + ' MB');
                        $("#MaxDevices").html(lReturn.MaxDevices);
                        $("#filterprofile").val(lReturn.filterid);
                        if (cookie != null) {
                            switch (cookie) {
                                case "es": $("#Totals").html("Dispositivos totales: <b>" + lReturn.macs.length + "</b>; Tiempo total: <b>" + lReturn.TotalTime + " s.</b>; Total Bytes Bajados: <b>" + lReturn.TotalBytesIn + "</b>; Total Bytes Subidos: <b>" + lReturn.TotalBytesOut + "</b>"); break;
                                default: $("#Totals").html("Total devices: <b>" + lReturn.macs.length + "</b>; Total time: <b>" + lReturn.TotalTime + " s.</b>; Total Download Bytes : <b>" + lReturn.TotalBytesIn + "</b>; Total Upload Bytes: <b>" + lReturn.TotalBytesOut + "</b>"); break;
                            }
                        }
                        else
                            $("#Totals").html("Total devices: <b>" + lReturn.macs.length + "</b>; Total time: <b>" + lReturn.TotalTime + " s.</b>; Total Download Bytes : <b>" + lReturn.TotalBytesIn + "</b>; Total Upload Bytes: <b>" + lReturn.TotalBytesOut + "</b>");

                        $("#timecreditformatted").val(lReturn.TimeCreditFormatted);
                        $("#comment").html(lReturn.Comment);

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#modal-loading").hide();
                        $("#searchResult").html(xhr.statusText)
                    }
                });

                var dataString = 'action=sitesetting&iduser=' + parameters.id;
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    beforeSend: function () {
                        $("#myModalWait").modal('show');
                    },
                    complete: function () {
                        $("#myModalWait").modal('hide');
                    },
                    success: function (pReturn) {
                        var lReturn = JSON.parse(pReturn);
                        $("#isVolumeCombined").val(lReturn.volumecombined);
                        if (lReturn.PrintLogo == true)
                            $("#logocheck").attr('checked', true);
                        if (lReturn.PrintVolume == true)
                            $("#volumecheck").attr('checked', true);
                        if (lReturn.PrintSpeed == true)
                            $("#speedcheck").attr('checked', true);
                        if (lReturn.filtercontents == true) {
                            $("#filtergroup").show();
                        }
                        if(lReturn.vendor != 1)
                            $("#filtergroup").hide();

                        $("#idhotel").val(lReturn.IdHotel);

                        if (lReturn.volumecombined) {
                            $("#groupvolumeup").hide();
                            $("#groupvolumedown").hide();
                            $("#groupvolumecombined").show();
                        }
                        else {
                            $("#groupvolumeup").show();
                            $("#groupvolumedown").show();
                            $("#groupvolumecombined").hide();
                        }

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#modal-loading").hide();
                        $("#searchResult").html(xhr.statusText)
                    }
                });

            $("#delete").on("click", function (e) {
                e.preventDefault();

                var parameters = getParameters();

                var dataString = 'action=userdelete&id=' + parameters.id;
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $("#myModalWait").modal('show');
                    },
                    complete: function () {
                        $("#myModalWait").modal('hide');
                    },
                    success: function (msg) {
                        if (msg.code == "OK") {
                            $("#message").removeClass().addClass("alert alert-success");
                            $("#message").html(GetMessage(msg.message));
                        }
                        else {
                            $("#message").removeClass().addClass("alert alert-danger");
                            $("#message").html(GetMessage(msg.message));

                        }
                        $("#myModalMessage").modal('show');

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#message").removeClass().addClass("alert alert-danger");
                        $("#message").html(xhr.statusText);
                        $("#myModalMessage").modal('show');

                    }
                });
            });

            $("#save").on("click", function (e) {
                e.preventDefault();

                var parameters = getParameters();
                var user = $("#UserName").val();
                var password = $("#Password").val();
                var comment = $("#comment").val();
                var priority = $("#Priority").val();
                var filterid = $("#filterprofile").val();
                var validsince = $("#ValidSince").val();
                var validtill = $("#ValidTill").val();
                if ((ilegalCharacter(user) != false) && (ilegalCharacter(password) != false)) {

                    var data = {};
                    data["action"] = 'USERSAVE';
                    data["id"] = parameters.id;
                    data["user"] = user;
                    data["password"] = password;
                    data["comment"] = comment;
                    data["filterid"] = filterid;
                    data["priority"] = priority;
                    data["validsince"] = validsince;
                    data["validtill"] = validtill;


                    $.ajax({
                        url: "handlers/SMIHandler.ashx",
                        type: "POST",
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {
                            $("#myModalWait").modal('show');
                        },
                        complete: function () {
                            $("#myModalWait").modal('hide');
                        },
                        success: function (msg) {
                            if (msg.code == "OK") {
                                $("#message").removeClass().addClass("alert alert-success");
                                $("#message").html(GetMessage(msg.message));

                            }
                            else if (msg.code == "NO") {
                                $("#message").removeClass().addClass("alert alert-warning");
                                $("#message").html(GetMessage(msg.message));
                            }
                            else {
                                $("#message").removeClass().addClass("alert alert-danger");
                                $("#message").html(GetMessage(msg.message));
                            }
                            $("#myModalMessage").modal('show');
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#message").removeClass().addClass("alert alert-danger");
                            $("#message").html(xhr.statusText);
                            $("#myModalMessage").modal('show');

                        }
                    });
                }
                else {
                    $("#message").addClass("alert alert-danger");
                    $("#message").html(GetMessage("0x0006"));
                    $("#myModalMessage").modal('show');

                }
            });

            $("#saveExtended").on("click", function (e) {
                e.preventDefault();

                var parameters = getParameters();
                var accessType = $("#accesstype").val();
                var cookie = readCookie("wifi360-language");

                var dataString = 'action=usersaveextended&id=' + parameters.id + '&accesstype=' + accessType;
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $("#myModalWait").modal('show');
                    },
                    complete: function () {
                        $("#myModalWait").modal('hide');
                    },
                    success: function (msg) {
                        if (msg.code == "OK") {
                            $("#message").removeClass().addClass("alert alert-success");
                            $("#message").html(GetMessage(msg.message));

                            var dataString = 'action=user&id=' + parameters.id;
                            $.ajax({
                                url: "handlers/SMIHandler.ashx",
                                data: dataString,
                                contentType: "application/json; charset=utf-8",
                                dataType: "text",
                                success: function (pReturn) {
                                    var lReturn = JSON.parse(pReturn);
                                    $("#UserName").val(lReturn.UserName);
                                    $("#Password").val(lReturn.Password);
                                    $("#Room").html(lReturn.Room);
                                    $("#AccessType").html(lReturn.AccessType);
                                    calendar2.value.set(lReturn.ValidTill);
                                    $("#ValidTill").val(lReturn.ValidTill);
                                    calendar.value.set(lReturn.ValidSince);
                                    $("#ValidSince").val(lReturn.ValidSince);
                                    $("#TimeCredit").html(lReturn.TimeCreditFormatted);
                                    $("#Priority").val(lReturn.IdPriority);
                                    $("#filterprofile").val(lReturn.filterid);
                                    $("#BwUp").html(lReturn.BWUp);
                                    $("#BwDown").html(lReturn.BWDown);
                                    $("#VolumeUp").html(lReturn.VolumeUp + ' MB');
                                    $("#VolumeDown").html(lReturn.VolumeDown + ' MB');
                                    $("#VolumeCombined").html(lReturn.VolumeDown + ' MB');
                                    $("#timecreditformatted").html(lReturn.TimeCreditFormatted);

                                    $("#MaxDevices").html(lReturn.MaxDevices);
                                    if (cookie != null) {
                                        switch (cookie) {
                                            case "es": $("#Totals").html("Dispositivos totales: <b>" + lReturn.macs.length + "</b>; Tiempo total: <b>" + lReturn.TotalTime + " s.</b>; Total Bytes Bajados: <b>" + lReturn.TotalBytesIn + "</b>; Total Bytes Subidos: <b>" + lReturn.TotalBytesOut + "</b>"); break;
                                            default: $("#Totals").html("Total devices: <b>" + lReturn.macs.length + "</b>; Total time: <b>" + lReturn.TotalTime + " s.</b>; Total Download Bytes : <b>" + lReturn.TotalBytesIn + "</b>; Total Upload Bytes: <b>" + lReturn.TotalBytesOut + "</b>"); break;
                                        }
                                    }
                                    else
                                        $("#Totals").html("Total devices: <b>" + lReturn.macs.length + "</b>; Total time: <b>" + lReturn.TotalTime + " s.</b>; Total Download Bytes : <b>" + lReturn.TotalBytesIn + "</b>; Total Upload Bytes: <b>" + lReturn.TotalBytesOut + "</b>");



                                    if (lReturn.macs.length == 0) {
                                        $("#chart_stadistics").html("No internet activity for this user");
                                    }
                                    else if (lReturn.macs.length == 1) {
                                        $("#selectmacs").hide();
                                        var array = new Array();
                                        array.push(['Date', 'BytesIn', 'BytesOut']);
                                        $.each(lReturn.list, function (index, item) {
                                            array.push([item['Date'], item["BytesIn"], item["BytesOut"]]);
                                        });

                                        var data = google.visualization.arrayToDataTable(array);

                                        google.load("visualization", "1", { packages: ["corechart"] });
                                        google.setOnLoadCallback(drawChartActivity(data));
                                    }
                                    else {
                                        $("#selectmacs").show();
                                        $("#selectmacs").append('<option value="0">Seleccione</option>');
                                        $.each(lReturn.macs, function (index, item) {
                                            $("#selectmacs").append('<option value="' + item + '"> ' + item + '</option>');
                                        });
                                    }

                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    $("#modal-loading").hide();
                                    $("#searchResult").html(xhr.statusText)
                                }
                            });

                            $("#modalExtended").modal('hide');
                            $("#myModalMessage").modal('show');

                        }
                        else if (msg.code == "NO") {
                            $("#message").removeClass().addClass("alert alert-danger");
                            $("#message").html(GetMessage(msg.message));

                            $("#modalExtended").modal('hide');
                            $("#myModalMessage").modal('show');
                        }
                        else {
                            $("#message").removeClass().addClass("alert alert-danger");
                            $("#message").html(GetMessage(msg.message));
                            $("#extended").modal('hide');
                            $("#myModalMessage").modal('show');

                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#message").removeClass().addClass("alert alert-danger");
                        $("#message").html(xhr.statusText);
                        $("#modalExtended").modal('hide');
                        $("#myModalMessage").modal('show');

                    }
                });
            });

            $("#saveUpgrade").on("click", function (e) {
                e.preventDefault();

                var parameters = getParameters();
                var accessType = $("#accesstype2").val();

                var cookie = readCookie("wifi360-language");

                var dataString = 'action=usersaveupgrade&id=' + parameters.id + '&accesstype=' + accessType;
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $("#myModalWait").modal('show');
                    },
                    complete: function () {
                        $("#myModalWait").modal('hide');
                    },
                    success: function (msg) {
                        if (msg.code == "OK") {
                            $("#message").removeClass().addClass("alert alert-success");
                            $("#message").html(GetMessage(msg.message));
                            $("#message").show();

                            var dataString = 'action=user&id=' + parameters.id;
                            $.ajax({
                                url: "handlers/SMIHandler.ashx",
                                data: dataString,
                                contentType: "application/json; charset=utf-8",
                                dataType: "text",
                                success: function (pReturn) {
                                    var lReturn = JSON.parse(pReturn);
                                    $("#UserName").val(lReturn.UserName);
                                    $("#Password").val(lReturn.Password);
                                    $("#Room").html(lReturn.Room);
                                    $("#AccessType").html(lReturn.AccessType);
                                    calendar2.value.set(lReturn.ValidTill);
                                    $("#ValidTill").val(lReturn.ValidTill);
                                    calendar.value.set(lReturn.ValidSince);
                                    $("#ValidSince").val(lReturn.ValidSince);
                                    $("#TimeCredit").html(lReturn.TimeCreditFormatted);
                                    $("#Priority").val(lReturn.IdPriority);
                                    $("#filterprofile").val(lReturn.filterid);
                                    $("#BwUp").html(lReturn.BWUp);
                                    $("#BwDown").html(lReturn.BWDown);
                                    $("#VolumeUp").html(lReturn.VolumeUp + ' MB');
                                    $("#VolumeDown").html(lReturn.VolumeDown + ' MB');
                                    $("#VolumeCombined").html(lReturn.VolumeDown + ' MB');
                                    $("#timecreditformatted").html(lReturn.TimeCreditFormatted);

                                    $("#MaxDevices").html(lReturn.MaxDevices);
                                    if (cookie != null) {
                                        switch (cookie) {
                                            case "es": $("#Totals").html("Dispositivos totales: <b>" + lReturn.macs.length + "</b>; Tiempo total: <b>" + lReturn.TotalTime + " s.</b>; Total Bytes Bajados: <b>" + lReturn.TotalBytesIn + "</b>; Total Bytes Subidos: <b>" + lReturn.TotalBytesOut + "</b>"); break;
                                            default: $("#Totals").html("Total devices: <b>" + lReturn.macs.length + "</b>; Total time: <b>" + lReturn.TotalTime + " s.</b>; Total Download Bytes : <b>" + lReturn.TotalBytesIn + "</b>; Total Upload Bytes: <b>" + lReturn.TotalBytesOut + "</b>"); break;
                                        }
                                    }
                                    else
                                        $("#Totals").html("Total devices: <b>" + lReturn.macs.length + "</b>; Total time: <b>" + lReturn.TotalTime + " s.</b>; Total Download Bytes : <b>" + lReturn.TotalBytesIn + "</b>; Total Upload Bytes: <b>" + lReturn.TotalBytesOut + "</b>");

                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    $("#modal-loading").hide();
                                    $("#searchResult").html(xhr.statusText)
                                }
                            });

                            $("#modalUpgrade").modal('hide');
                            $("#myModalMessage").modal('show');

                        }
                        else {
                            $("#message").removeClass().addClass("alert alert-danger");
                            $("#message").html(GetMessage(msg.message));
                            $("#extended").modal('hide');
                            $("#myModalMessage").modal('show');

                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#message").removeClass().addClass("alert alert-danger");
                        $("#message").html(xhr.statusText);
                        $("#modalExtended").modal('hide');
                        $("#myModalMessage").modal('show');

                    }
                });
            });

            $("#back").on("click", function (e) {

                var parameters = getParameters();
                if (parameters.return != undefined)
                    window.location.href = parameters.return;

            });

            $("#print").on("click", function (e) {

                e.preventDefault();

                $("#myModalWait").modal('show');

                var fast = '';
                $(".body").append('<br />');
                fast = '<div class="fastticket2">';
                if ($("#logocheck").is(':checked'))
                    fast += '<img src="/resources/images/' + $("#idhotel").val() + '/logo-bw.png" alt="fast" />';
                else
                    fast += '<span>' + $("#AccessType").html() + '</span>';
                fast += '<hr/>';
                fast += '<b>USERNAME: ' + $("#UserName").val() + '</b><br />';
                fast += '<b>PASSWORD: ' + $("#Password").val() + '</b><br />';
                fast += '<b>MAX DEVICES: ' + $("#MaxDevices").html() + '</b><br />';
                fast += '<b>SINCE: ' + $("#ValidSince").val() + '</b><br />';
                fast += '<b>TILL: ' + $("#ValidTill").val() + '</b><br />';
                fast += '<b>CREDIT: ' + $("#timecreditformatted").val() + '</b><br />'
                if ($("#speedcheck").is(':checked')) {
                    fast += '<b>SPEED UP: ' + $("#BwUp").html() + ' kbps</b><br />';
                    fast += '<b>SPEED DW: ' + $("#BwDown").html() + ' kbps</b><br />';
                }
                if ($("#volumecheck").is(':checked')) {
                    if ($("#isVolumeCombined") == "false") {
                        fast += '<b>VOLUME UP: ' + $("#VolumeUp").html() + ' </b><br />';
                        fast += '<b>VOLUME DW: ' + $("#VolumeDown").html() + '</b><br />';
                    }
                    else
                        fast += '<b>VOLUME AGG: ' + $("#VolumeDown").html() + '</b><br />';
                }

                fast += '<hr/>';
                fast += '</div>';

                $("#fastticketprint").html(fast);
                //var d = new Date();

                var fastticketprinthtml = $("#fastticketprint").html();

                var printWindow = window.open('', '', 'height=500,width=500');
                printWindow.document.write('<html><head><title>Fast Ticket</title>');
                printWindow.document.write('<link rel="stylesheet" href="css/default.css" type="text/css" />');
                printWindow.document.write('<style> html, body {font-family: sans-serif; padding: 0px; margin:0px;}</style>');
                printWindow.document.write('</head><body>');
                printWindow.document.write('<div style="page-break-after:always;">');
                printWindow.document.write(fast);
                //printWindow.document.write('printed: ' + d.toLocaleString());
                printWindow.document.write('</div>');
                printWindow.document.write('</body></html>');
                printWindow.document.close();

                $("#myModalWait").modal('hide');

                setTimeout(function () { printWindow.print(); }, 1000);

            });

            $("#details").on("click", function (e) {

                e.preventDefault();
                var parameters = getParameters();

                var dataString = 'action=closedsessions&iduser=' + parameters.id;
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    beforeSend: function () {
                        $("#myModalWait").modal('show');
                    },
                    complete: function () {
                        $("#myModalWait").modal('hide');
                    },
                    success: function (pReturn) {
                        var lReturn = JSON.parse(pReturn);
                        $("#resultSearch").html("")
                        var table = ""
                        table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Start</th><th>Finish</th><th class='hidden-xs'>Bytes Upload</th><th class='hidden-xs'>Bytes Download</th><th>MAC</th></tr>";
                        $.each(lReturn.list, function (index, item) {
                            table += '<tr><td>' + item["Start"] + '</td><td>' + item["Finish"] + '</td><td class="hidden-xs">' + item["BytesIn"] + '</td><td class="hidden-xs">' + item["BytesOut"] + '</td><td>' + item["MAC"] + '</td></tr>';
                        });
                        table += "</table>";
                        $("#resultSearch").html(table)

                        $("#modal-loading").hide();

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#modal-loading").hide();
                        $("#searchResult").html(xhr.statusText)
                    }
                });

                $('#myModalDetails').modal('show');

            });

            function getParameters() {
                var searchString = window.location.search.substring(1)
                  , params = searchString.split("&")
                  , hash = {}
                ;

                for (var i = 0; i < params.length; i++) {
                    var val = params[i].split("=");
                    hash[unescape(val[0])] = unescape(val[1]);
                }
                return hash;
            }

            function bindSelect(id, valueMember, displayMember, source) {
                $("#" + id).find('option').remove().end();
                var cookie = readCookie("wifi360-language");
                if (cookie != null) {
                    switch (cookie) {
                        case "es": $("#" + id).append('<option value="0">Seleccionar</option>'); break;
                        default: $("#" + id).append('<option value="0">Select</option>'); break;
                    }
                }
                else
                     $("#" + id).append('<option value="0">Select</option>');
                $.each(source, function (index, item) {
                    $("#" + id).append('<option value="' + item[valueMember] + '"> ' + item[displayMember] + '</option>');
                });
            };

            function bindSelectNone(id, valueMember, displayMember, source) {
                $("#" + id).find('option').remove().end();
                var cookie = readCookie("wifi360-language");
                if (cookie != null) {
                    switch (cookie) {
                        case "es": $("#" + id).append('<option value="0">Ninguno</option>'); break;
                        default: $("#" + id).append('<option value="0">None</option>'); break;
                    }
                }
                else
                    $("#" + id).append('<option value="0">None</option>');

                $.each(source, function (index, item) {
                    $("#" + id).append('<option value="' + item[valueMember] + '"> ' + item[displayMember] + '</option>');
                });
            };

    });


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 id="headerpageuser">Users management</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="default.aspx">Dashboard</a>
                </li>
                <li>
                    <a id="breadusersmanagement">Users management</a>
                </li>
                <li>
                    <a href="#" id="returnPage">ReturnPage</a>
                </li>
                <li class="active">
                    <strong id="breaduser">User</strong>
                </li>
            </ol>
           
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-9">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                    <h5 id="labelboxheaderuser">User<small> Details of user</small></h5>
                </div>
                    <div class="ibox-content">
                     <div class="form-horizontal">
                    <div class="form-group"> <label class="control-label col-sm-4" id="labelformuserroom">Room</label><div class="col-sm-8"><label class="form-control" id="Room" >Room</label></div></div>
                    <div class="form-group"> <label class="control-label col-sm-4" id="labelformuserusername">Username</label><div class="col-sm-8"><input type="text" class="form-control" id="UserName" /></div></div>
                    <div class="form-group"> <label class="control-label col-sm-4" id="labelformuserpassword">Password</label><div class="col-sm-8"><input type="text" class="form-control" id="Password" /></div></div>
                    <div class="form-group"> <label class="control-label col-sm-4" id="labelformuserbillingtype">Access type</label><div class="col-sm-8"><label class="form-control" id="AccessType">AccessType</label></div></div>
                    <div class="form-group"> <label class="control-label col-sm-4" id="labelformuserpriority">Priority / Group Policy</label><div class="col-sm-8"><select class="form-control" id="Priority"></select></div></div>
                    <div class="form-group" style="display:none;" id="filtergroup"> <label class="control-label col-sm-4" id="labelformuserfilter">Content filter</label><div class="col-sm-8"><select class="form-control" id="filterprofile"></select></div></div>
                    <div class="form-group"> <label class="control-label col-sm-4" id="labelformuserbwdown">BandWidth Down</label><div class="col-sm-8"><label class="form-control" id="BwDown"></label></div></div>
                    <div class="form-group"> <label class="control-label col-sm-4" id="labelformuserbwup">Bandwidth UP</label><div class="col-sm-8"><label class="form-control" id="BwUp"></label></div></div>
                    <div class="form-group" id="groupvolumedown"> <label class="control-label col-sm-4" id="labelformuservolumedown">Volume Down per device</label><div class="col-sm-8"><label class="form-control" id="VolumeDown"></label></div></div>
                    <div class="form-group" id="groupvolumeup"> <label class="control-label col-sm-4" id="labelformuservolumeup">Volume Up per device</label><div class="col-sm-8"><label class="form-control" id="VolumeUp"></label></div></div>
                    <div class="form-group" id="groupvolumecombined"> <label class="control-label col-sm-4" id="labelformuservolumecombined">Volume Aggregate per device</label><div class="col-sm-8"><label class="form-control" id="VolumeCombined"></label></div></div>
                    <div class="form-group"> <label class="control-label col-sm-4" id="labelformuserdevice">Max devices</label><div class="col-sm-8"><label class="form-control" id="MaxDevices"></label></div></div>
                    <div class="form-group"> <label class="control-label col-sm-4" id="labelformuserdatesince">Valid since</label><div class="col-sm-8"><input type="text" class="form-control" id="ValidSince"/></div></div>
                    <div class="form-group"> <label class="control-label col-sm-4" id="labelformuserdatetill">Valid till</label><div class="col-sm-8"><input type="text" class="form-control" id="ValidTill"/></div></div>
                    <div class="form-group"> <label class="control-label col-sm-4" id="labelformusercredittime">Time Credit</label><div class="col-sm-8"><label class="form-control" id="TimeCredit">TimeCredit</label></div></div>

                    
                    <div class="form-group"> <label class="control-label col-sm-4" id="labelformusercomment">Comment</label><div class="col-sm-8"><textarea class="form-control" id="comment" rows="4"></textarea></div></div>

                            </div>

                <input type="checkbox" id="speedcheck" hidden="hidden" />
                <input type="checkbox" id="logocheck" hidden="hidden" />
                <input type="checkbox" id="volumecheck" hidden="hidden" />
                <input type="text" id="idhotel" hidden="hidden"  style="display:none;"/>
                <div id="#fastticketprint" style="display:none;"></div>
                </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5 id="headerboxactionsuser">Actions</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <p>
                            <a href="#" class="btn btn-w-m btn-lg btn-primary btn-block" id="save"><i class="fa fa-save"></i> <span id="labelbuttonsaveuser"> Save</span></a>
                            <a href="#" class="btn btn-w-m btn-lg btn-danger btn-block" id="delete"><i class="fa fa-trash"></i> <span id="labelbuttondeleteuser"> Disable</span></a>
                            <a href="#modalUpgrade" role="button" class="btn btn-lg  btn-w-m btn-success btn-block" id="upgrade" data-toggle="modal"><i class="fa fa-upload"> </i> <span id="labelbuttonupgradeuser">Upgrade</span></a>
                            <a href="#modalExtended" role="button" class="btn btn-lg  btn-w-m btn-info btn-block" id="extended" data-toggle="modal"><i class="fa fa-arrows-h"></i> <span id="labelbuttonextenduser"> Extend</span></a>
                            <a href="#" role="button" class="btn btn-w-m btn-lg btn-warning btn-block" id="print" data-toggle="modal"><i class="fa fa-print"></i> <span id="labelbuttonprintuser">Print</span></a>
                            <a href="#" role="button" class="btn btn-w-m btn-lg btn-default btn-block" id="back" data-toggle="modal"><i class="fa fa-backward"></i> <span id="labelbuttonbackuser"> Back</span></a>
                            <hr />
                            <a href="#" role="button" class="btn btn-w-m btn-lg btn-invert btn-block" id="details" data-toggle="modal"><i class="fa fa-search-plus"></i> <span id="labelbuttondetails"> Details</span></a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="modal inmodal fade" id="myModalMessage" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                            </div>
                            <div class="modal-body">
                                <div id="message">
                                    
                                </div>
                            </div>
                          <div class="modal-footer">
                            <a href="#" class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Volver</a>
                          </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>

            <div id="modalUpgrade" class="modal inhide fade" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-md">
                        <div class="modal-content">
                            <div class="modal-header">
                            <h3 id="headeruserupgrade">Upgrade User</h3>
                          </div>
                          <div class="modal-body">
                              <div class="form-horizontal">
                                <div class="form-group"> <label class="control-label col-sm-3" id="labelupgradeuserabillingtype">Access Type</label>
                                <div class="col-sm-9">
                                  <select id="accesstype2" class="form-control"></select>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <a href="#" class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> <span id="labelbuttonupgradeuserback"> Back</span> </a>
                            <a href="#" class="btn btn-primary" id="saveUpgrade"><i class="fa fa-save"></i>  <span id="labelbuttonupgradeusersave"> Save</span> </a>
                          </div>
                        </div>
                    </div>
            </div>

        <div id="modalExtended" class="modal inhide fade" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-md">
                        <div class="modal-content">
                            <div class="modal-header">
                            <h3 id="headeruserextend">Extend User</h3>
                          </div>
                          <div class="modal-body">
                            <div class="form-horizontal">
                                <div class="form-group"> <label class="control-label col-sm-3" id="labelextenduserabillingtype">Access Type</label>
                                <div class="col-sm-9">
                                  <select id="accesstype" class="form-control"></select>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <a href="#" class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> <span id="labelbuttonextenduserback" >Back</span></a>
                            <a href="#" class="btn btn-primary" id="saveExtended"><i class="fa fa-save"></i>  <span id="labelbuttonextendusersave" >Save</span></a>
                          </div>
                        </div>
                        </div>
        </div>

        <div id="myModalDetails" class="modal inhide fade" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                            <div id="div_user_conections_details"><h3 id="H2">User Conections Details</h3> Results limited to last 30 sessions</div>
                          </div>
                          <div class="modal-body">
                              <div id="resultSearch" style="max-height:400px;overflow-y:auto;"></div>
                          </div>
                          <div class="modal-footer">
                            <a href="#" class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Back</a>
                           <!-- <a href="#" class="btn btn-primary" id="A1"><i class="fa fa-save"></i> Save</a>-->
                          </div>
                        </div>
                        </div>
        </div>

    <input type="text" id="timecreditformatted" hidden="hidden" />
    <input type="text" id="isVolumeCombined" hidden="hidden" />
   
</asp:Content>
