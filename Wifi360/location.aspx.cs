﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Wifi360
{
    public partial class location : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                HttpCookie cookieRol = Request.Cookies["FDSUserRol"];
                switch (cookieRol.Value)
                {
                    case "2": //ADMIN
                    case "4": //SUPER ADMIN
                        //ACCESS SUCCESFULL
                        break;
                    default:
                        //ACCESS REFUSED
                        HttpContext.Current.Response.Redirect("default.aspx", false);
                        break;
                }

            }
            catch
            {
                HttpContext.Current.Response.Redirect("login.aspx", false);
            }
        }
    }
}