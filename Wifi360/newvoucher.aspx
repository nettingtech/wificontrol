﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Site.Master" AutoEventWireup="true" CodeBehind="newvoucher.aspx.cs" Inherits="Wifi360.newvoucher" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/calendar.css" rel="stylesheet" />
    <script src="js/calendar.full.min.js"></script>
    <script src="js/moment.js"></script>
    <script type="text/javascript">

        var min_pass = 0;
        var longvoucherusername = 0;

        function ilegalCharacter(string) {
            if ($("#macauthenticate").is(":checked"))
            {
                if (string != "") {
                    if (/^([0-9A-F]{2}[:]){5}([0-9A-F]{2})$/.test(string) == false) {
                        return false;
                    }
                }
            }
            else {
                if (/^[a-zA-Z0-9_\.-@_-]*$/.test(string) == false) {
                //if (/^[a-zA-Z0-9_\.-]*$/.test(string) == false) {
                    return false;
                }
            }
        };

        function readCookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        };

        $(document).ready(function () {
            $("#check").hide();

            var cookie = readCookie("wifi360-language");
            if (cookie != null) {
                switch (cookie) {
                    case "es": var calendar = new CalendarPopup('#start', { dayOfWeekStart: 1, showMask: false, locale: 'es', showWeekIndex: true, format: 'DD/MM/YYYY HH:mm', i18n: { es: { "Chose": "Elegir", "Close": "Cerrar", "Time": "Hora" } } });
                        var calendar2 = new CalendarPopup('#end', { dayOfWeekStart: 1, showMask: false, locale: 'es', showWeekIndex: true, format: 'DD/MM/YYYY HH:mm', i18n: { es: { "Chose": "Elegir", "Close": "Cerrar", "Time": "Hora" } } }); break;
                    default: var calendar = new CalendarPopup('#start', { showMask: false, showWeekIndex: true, format: 'DD/MM/YYYY HH:mm' });
                        var calendar2 = new CalendarPopup('#end', { showMask: false, showWeekIndex: true, format: 'DD/MM/YYYY HH:mm' }); break;
                }
            }
            else {
                var calendar = new CalendarPopup('#start', { dayOfWeekStart: 1, showMask: false, showWeekIndex: true, format: 'DD/MM/YYYY HH:mm' });
                var calendar2 = new CalendarPopup('#end', { dayOfWeekStart: 1, showMask: false, showWeekIndex: true, format: 'DD/MM/YYYY HH:mm' });
            }

            $(".calendar-popup-input-wrapper").attr("style", "display:inherit;")

            var rol = readCookie("FDSUserRol");
            if (rol == "3")
            {
                $("#groupemail").hide();
                $("#groupvalidsince").hide();
                $("#groupvalidtill").hide();
            }

            var dataString4 = 'action=sitesetting';
            $.ajax({
                url: "handlers/SMIHandler.ashx",
                data: dataString4,
                contentType: "application/json; charset=utf-8",
                dataType: "text",
                success: function (pReturn) {
                    var lReturn = JSON.parse(pReturn);

                    longvoucherusername = lReturn.longvoucherusername
                    min_pass = lReturn.longpassword;

                    if (lReturn.EnabledFasTicket == true)
                        $("#enabledfastticket").attr('checked', true);
                    if (lReturn.PrintLogo == true)
                        $("#logocheck").attr('checked', true);
                    if (lReturn.PrintVolume == true)
                        $("#volumecheck").attr('checked', true);
                    if (lReturn.PrintSpeed == true)
                        $("#speedcheck").attr('checked', true);


                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#modal-loading").hide();
                    $("#searchResult").html(xhr.statusText)
                }
            });

            var dataString2 = 'action=RoomsNoFilter';
            $.ajax({
                url: "handlers/SMIHandler.ashx",
                data: dataString2,
                contentType: "application/json; charset=utf-8",
                dataType: "text",
                success: function (pReturn) {
                    var lReturn = JSON.parse(pReturn);
                    var cookie_l = readCookie("wifi360-language");
                    if (cookie_l != null) {
                        switch (cookie_l) {
                            case "es": bindSelect("Seleccione habitación", "rooms", "IdRoom", "Name", lReturn.list); break;
                            default: bindSelect("Selec Room", "rooms", "IdRoom", "Name", lReturn.list); break;
                        }
                    }
                    else
                        bindSelect("Selec Room", "rooms", "IdRoom", "Name", lReturn.list);
                    if (lReturn.Default != 0) {
                        $("#rooms").val(lReturn.Default);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#message").removeClass().addClass("alert alert-error");
                    $("#message").html(xhr.statusText);
                    $("#message").show();
                    $("#modal-loading").hide();
                }
            });

            var cookielocation = readCookie("location");

            var dataString = 'action=LocationsNoAllZones';
            $.ajax({
                url: "handlers/SMIHandler.ashx",
                data: dataString,
                contentType: "application/json; charset=utf-8",
                dataType: "text",
                complete: function () {
                    $("#myModalWait").modal('hide');
                },
                success: function (pReturn) {
                    var lReturn = JSON.parse(pReturn);
                    var cookie = readCookie("wifi360-language");
                    if (cookie != null) {
                        if (cookie == "es") {
                            bindSelect("seleccionar localización", "location", "Idlocation", "Description", lReturn.list);
                        }
                        else
                            bindSelect("select location", "location", "Idlocation", "Description", lReturn.list);
                    }
                    else
                        bindSelect("select location", "location", "Idlocation", "Description", lReturn.list);

                        if (lReturn.Default != 0) {
                            $("#location").val(lReturn.Default);

                        var dataString = 'action=billingTypesLocation&l=' + lReturn.Default;
                        $.ajax({
                            url: "handlers/SMIHandler.ashx",
                            data: dataString,
                            contentType: "application/json; charset=utf-8",
                            dataType: "text",
                            success: function (pReturn) {
                                var lReturn2 = JSON.parse(pReturn);
                                var cookie = readCookie("wifi360-language");
                                if (cookie != null) {
                                    if (cookie == "es") {
                                        bindSelect("seleccionar tipo de acceso", "billingTypes", "IdBillingType", "Description", lReturn2.list);
                                    }
                                    else
                                        bindSelect("select access type", "billingTypes", "IdBillingType", "Description", lReturn2.list);
                                }
                                else
                                    bindSelect("select access type", "billingTypes", "IdBillingType", "Description", lReturn2.list);
                                if (lReturn2.Default != 0) {
                                    $("#billingTypes").val(lReturn2.Default);
                                }
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                $("#message").removeClass().addClass("alert alert-error");
                                $("#message").html(xhr.statusText);
                                $("#message").show();
                                $("#modal-loading").hide();
                            }
                        });
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#message").removeClass().addClass("alert alert-error");
                    $("#message").html(xhr.statusText);
                    $("#message").show();
                    $("#modal-loading").hide();
                }
            });

            $("#location").change(function (e) {

                e.preventDefault();

                var location = $("#location").val();

                if (location == "0") {
                    $('#billingTypes').children().remove().end();
                }
                else {

                    var dataString = 'action=billingTypesLocation&l=' + location;
                    $.ajax({
                        url: "handlers/SMIHandler.ashx",
                        data: dataString,
                        contentType: "application/json; charset=utf-8",
                        dataType: "text",
                        beforeSend: function () {
                            $("#myModalWait").modal('show');
                        },
                        complete: function () {
                            $("#myModalWait").modal('hide');
                        },
                        success: function (pReturn) {
                            var lReturn = JSON.parse(pReturn);
                            var cookie = readCookie("wifi360-language");
                            if (cookie != null) {
                                if (cookie == "es") {
                                    bindSelect("seleccionar tipo de acceso", "billingTypes", "IdBillingType", "Description", lReturn.list);
                                }
                                else
                                    bindSelect("select access type", "billingTypes", "IdBillingType", "Description", lReturn.list);
                            }
                            else
                                bindSelect("Select access type", "billingTypes", "IdBillingType", "Description", lReturn.list);

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#message").removeClass().addClass("alert alert-error");
                            $("#message").html(xhr.statusText);
                            $("#message").show();
                            $("#modal-loading").hide();
                        }
                    });
                }

            });

            function BuyTicket() {

                var room = $("#rooms").val();
                var billingType = $("#billingTypes").val();
                var email = $("#email").val();
                var username = $("#username").val();
                var numero = $("#numero").val();
                var start = $("#start").val();
                var end = $("#end").val();
                var comment = $("#comment").val();
                var cookie = readCookie("wifi360-language");

                if (room == 0) {
                    $("#message").html(GetMessage("0x0018"));
                    $("#myModal").modal('show');
                } else if (username.length < longvoucherusername) {
                    $("#message").html(GetMessage("0x0042").toString() + " (min: " + longvoucherusername + " )");
                    $("#myModal").modal('show');
                }else if ( $("#email").val().indexOf('@', 0) == -1 || $("#email").val().indexOf('.', 0) == -1) {
                    $("#message").html(GetMessage("0x0043"));
                    $("#myModal").modal('show');
                }else if ((billingType == 0) || (billingType == null)) {
                    $("#message").html(GetMessage("0x0019"));
                    $("#myModal").modal('show');
                }
                else if (username.length > 50) {
                    $("#message").html(GetMessage("0x0020"));
                    $("#myModal").modal('show');
                }
                else if ((ilegalCharacter(username) == false)) {
                    if ($("#macauthenticate").is(":checked")) {
                        $("#message").html(GetMessage("0x0021"));
                    }
                    else {
                        $("#message").html(GetMessage("0x0006"));
                    }
                    $("#myModal").modal('show');
                }
                else {

                    var data = {};
                    data["action"] = 'CREATEVOUCHER';
                    data["idbillingtype"] = billingType;
                    data["username"] = username;
                    data["idroom"] = room;
                    data["mail"] = email;
                    data["numero"] = numero;
                    data["start"] = start;
                    data["end"] = end;
                    data["comment"] = comment;


                    $.ajax({
                        url: "handlers/SMIHandler.ashx",
                        type: "POST",
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "text",
                        beforeSend: function () {
                            $("#myModalWait").modal('show');
                        },
                        complete: function () {
                            $("#myModalWait").modal('hide');
                        },
                        success: function (msg) {
                            var lReturn = JSON.parse(msg);
                            if (lReturn.list.length > 0) {
                                if (lReturn.list.length == 1) { //TICKET NORMAL
                                    if (lReturn.list[0].IdUser == "0") {
                                        $("#message").html(GetMessage("0x0005"));
                                        $("#myModal").modal('show');
                                    }
                                    else {
                                        $("#resultusername").html(lReturn.list[0].UserName);
                                        $("#resultpassword").html(lReturn.list[0].Password);

                                        $("#ticket").show();
                                        $("#printpanel").show();

                                        var fast = '';
                                        var logo = '';
                                        var logo2 = '';
                                        var speed = '';
                                        var volume = '';
                                        $(".body").append('<br />');

                                        logo = '<img src="/resources/images/' + lReturn.list[0].IdUser + '/logo-bw.png" alt="fast" />';
                                        logo2 += '<span>' + lReturn.list[0].AccessType + '</span>';
                                        fast += '<hr/>';
                                        fast += '<b>USERNAME: ' + lReturn.list[0].UserName + '</b><br />';
                                        fast += '<b>MAX DEVICES: ' + lReturn.list[0].MaxDevices + '</b><br />';
                                        fast += '<b>SINCE: ' + lReturn.list[0].ValidSince + '</b><br />';
                                        fast += '<b>TILL: ' + lReturn.list[0].ValidTill + '</b><br />';
                                        fast += '<b>CREDIT: ' + lReturn.list[0].TimeCredit + '</b><br />'

                                        speed += '<b>SPEED UP: ' + lReturn.list[0].BWUp + ' kbps</b><br />';
                                        speed += '<b>SPEED DW: ' + lReturn.list[0].BWDown + ' kbps</b><br />';

                                        volume += '<b>VOLUME UP: ' + lReturn.list[0].VolumeUp + ' MB</b><br />';
                                        volume += '<b>VOLUME DW: ' + lReturn.list[0].VolumeDown + ' MB</b><br />';


                                        fast += '<hr/>';


                                        $("#fastticket").html(fast);
                                        $("#fastticketlogo").html(logo);
                                        $("#fastticketlogo2").html(logo2);
                                        $("#fastticketspeed").html(speed);
                                        $("#fastticketvolume").html(volume);
                                    }
                                }
                                else if (lReturn.list.length == "-1") {
                                    $("#message").html(GetMessage("0x9999"));
                                    $("#myModal").modal('show');
                                }
                                else {
                                    var text = '';
                                    var fast = '';
                                    $(".body").append('<br />');
                                    $.each(lReturn.list, function (index, item) {
                                        text += '<div class="fastticket"><img src="/resources/images/' + lReturn.pageNumber + '/logo-bw.png" alt="logo" />';
                                        text += '<p><b>ACCESS TYPE:</b> ' + item["AccessType"] + '<br /><b>USERNANE:</b> ' + item["UserName"] + '<br />';
                                        text += '<b>VALID SINCE: </b>' + item["ValidSince"] + '<br />';
                                        text += '<b> VALID TILL </b>:' + item["ValidTill"] + '<br /><b>TIME CREDIT</b>:' + item["TimeCredit"] + '.<br /></div>';
                                    });

                                    var mywindow = window.open('', 'new div', 'height=800,width=700');
                                    mywindow.document.write('<html><head><title>Fast Ticket</title>');
                                    mywindow.document.write('<link rel="stylesheet" href="css/default.css" type="text/css" />');
                                    mywindow.document.write('<link href="https://fonts.googleapis.com/css?family=Oxygen+Mono" rel="stylesheet" type="text/css">');
                                    mywindow.document.write('<style> body {font-family: "Oxygen Mono", sans-serif;}</style>');
                                    mywindow.document.write('</head><body><div style="width: 900px">');
                                    mywindow.document.write(text);
                                    mywindow.document.write('</div></body></html>');
                                    mywindow.document.write('<script>(' + (function () {
                                        function checkReadyState() {
                                            if (document.readyState === 'complete') {
                                                window.focus();
                                                window.print();
                                                window.close();
                                            } else {
                                                setTimeout(checkReadyState, 200); // May need to change interval
                                            }
                                        }

                                        checkReadyState();
                                    }) + ')();</sc' + 'ript>');
                                }
                            }
                            else {
                                $("#message").html(GetMessage("0x9999"));
                                $("#myModal").modal('show');
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#message").removeClass().addClass("alert alert-danger");
                            $("#message").html(GetMessage("0x9999"));
                            $("#myModalMessage").modal('show');

                        }
                    });
                }

            };

            function Export() {

                var room = $("#rooms").val();
                var billingType = $("#billingTypes").val();
                var email = $("#email").val();
                var username = $("#username").val();
                var numero = $("#numero").val();
                var start = $("#start").val();
                var end = $("#end").val();
                var comment = $("#comment").val();
                var cookie = readCookie("wifi360-language");

                if (room == 0) {
                    $("#message").html(GetMessage("0x0018"));
                    $("#myModal").modal('show');
                }
                else if ((billingType == 0) || (billingType == null)) {
                    $("#message").html(GetMessage("0x0019"));
                    $("#myModal").modal('show');
                }
                else if (username.length > 50) {
                    $("#message").html(GetMessage("0x0020"));
                    $("#myModal").modal('show');
                }
                else if (ilegalCharacter(username) == false) {
                    if ($("#macauthenticate").is(":checked")) {
                        $("#message").html(GetMessage("0x0021"));
                    }
                    else {
                        $("#message").html(GetMessage("0x0006"));
                    }

                    $("#myModal").modal('show');
                }
                else {

                    var data = {};
                    data["action"] = 'EXPORTVOUCHER';
                    data["idbillingtype"] = billingType;
                    data["username"] = username;
                    data["idroom"] = room;
                    data["mail"] = email;
                    data["numero"] = numero;
                    data["start"] = start;
                    data["end"] = end;
                    data["comment"] = comment;


                    $.ajax({
                        url: "handlers/SMIHandler.ashx",
                        type: "POST",
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "text",
                        beforeSend: function () {
                            $("#myModalWait").modal('show');
                        },
                        complete: function () {
                            $("#myModalWait").modal('hide');
                        },
                        success: function (pReturn) {
                            var lReturn = JSON.parse(pReturn);
                            if (lReturn.code == "OK") {
                                var url = lReturn.message;
                                window.location.href = url;
                            }
                            else {
                                $("#message").html(pReturn.message);
                                $("#myModal").modal("show");
                            }

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#message").removeClass().addClass("alert alert-danger");
                            $("#message").html(xhr.statusText);
                            $("#myModalMessage").modal('show');

                        }
                    });
                }

            };

            $("#create").click(function (e) {

                    e.preventDefault();
                    var numero = $("#numero").val();
                    var start = $("#start").val();
                    var end = $("#end").val();
                    var s = moment(start, "DD/MM/YYYY HH:mm");
                    var e = moment(end, "DD/MM/YYYY HH:mm");
                    var cookie = readCookie("wifi360-language");


                    if (numero <= 3000) {
                        if (
                            ($("#start").val() != '' && $("#end").val() != '' && s.toDate() < e.toDate())
                            || (($("#start").val() != '') && ($("#end").val() == ''))
                            || (($("#start").val() == '') && ($("#end").val() != ''))
                            || (($("#start").val() == '') && ($("#end").val() == ''))) {

                            BuyTicket();
                        }
                        else {
                            $("#message").html(GetMessage("0x0022"));
                            $("#myModal").modal('show');
                        }
                    }
                    else {
                        $("#message").html(GetMessage("0x0023"));
                        $("#myModal").modal('show');
                    }
            });         

            $("#export").click(function (e) {
                if (usua_valido && mail_valido) {
                    e.preventDefault();
                    var numero = $("#numero").val();
                    var start = $("#start").val();
                    var end = $("#end").val();
                    var s = moment(start, "DD/MM/YYYY HH:mm");
                    var e = moment(end, "DD/MM/YYYY HH:mm");
                    var cookie = readCookie("wifi360-language");
                    if (numero <= 3000) {
                        if (
                            ($("#start").val() != '' && $("#end").val() != '' && s.toDate() < e.toDate())
                            || (($("#start").val() != '') && ($("#end").val() == ''))
                            || (($("#start").val() == '') && ($("#end").val() != ''))
                            || (($("#start").val() == '') && ($("#end").val() == ''))) {

                            Export();
                        }
                        else {
                            $("#message").html(GetMessage("0x0022"));
                            $("#myModal").modal('show');
                        }
                    }
                    else {
                        $("#message").html(GetMessage("0x0023"));
                        $("#myModal").modal('show');
                    }
                } else {
                    $("#message").html(GetMessage("0x0042"));
                    $("#myModal").modal('show');
                }
            });

            $("#print").click(function (e) {
                e.preventDefault();

                var fast = ''
                fast = '<div class="fastticket2">';
                if ($("#logocheck").is(':checked'))
                    fast += $("#fastticketlogo").html();
                else
                    fast += $("#fastticketlogo2").html();

                fast += $("#fastticket").html();

                if ($("#speedcheck").is(':checked')) {
                    fast += $("#fastticketspeed").html();
                }
                if ($("#volumecheck").is(':checked')) {
                    fast += $("#fastticketvolume").html();
                }
                fast += '</div>';

                var fastticketprinthtml = fast;
                var printWindow = window.open('', '', 'height=500,width=800');
                printWindow.document.write('<html><head><title>Fast Ticket</title>');
                printWindow.document.write('<link rel="stylesheet" href="/css/default.css" type="text/css" />');
                printWindow.document.write('<style> body {font-family: sans-serif; padding: 0px; margin:0px;}</style>');
                printWindow.document.write('</head><body>');
                printWindow.document.write('<div style="page-break-after:always">');
                printWindow.document.write(fastticketprinthtml);
                printWindow.document.write('</div>');
                printWindow.document.write('</body></html>');
                printWindow.document.close();
                setTimeout(function () {
                    printWindow.print();
                    //your code to be executed after 1 second
                }, 2000);
                
            });

            $("#numero").change(function (e) {
                var numero = $("#numero").val();
                if (numero > 1) {
                    $("#username").attr("disabled", "disabled");
                    $("#email").attr("disabled", "disabled");
                }
                else {
                    $("#username").removeAttr("disabled");
                    $("#email").removeAttr("disabled");
                }
                    
            });
        });

        function bindSelect(firstItem, id, valueMember, displayMember, source) {
            $("#" + id).find('option').remove().end();
            $("#" + id).append('<option value="0">' + firstItem + '</option>');
            $.each(source, function (index, item) {
                $("#" + id).append('<option value="' + item[valueMember] + '"> ' + item[displayMember] + '</option>');
            });
        };

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 id="headerpagenewvoucher">New Voucher</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="default.aspx">Dashboard</a>
                </li>
                <li>
                    <a>Tickets</a>
                </li>
                <li class="active" id="breadlabelnewvoucher">
                    <strong>New Voucher</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-7 col-md-7">
                <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5 id="labelboxheadervoucher">New Voucher<small> Select parameters</small></h5>
                </div>
                <div class="ibox-content">
                        <div role="form">
                            <div class="form-group"><label id="labelformroomvoucher">Room </label> <select id="rooms" class="form-control"></select></div>
                            <div class="form-group"><label id="labelformlocationvoucher">Location</label> <select id="location" class="form-control"></select></div>
                            <div class="form-group"><label id="labelformbillingtypevoucher">Access type</label>  <select id="billingTypes" class="form-control"></select></div>
                        </div>
                    </div>
                </div>

                <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5 id="labelboxadvancedparametersvoucher">Advanced parameters<small> Select advanced parameters to create new tickets</small></h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                        </div>
                </div>
                <div class="ibox-content" style="display:none">
                        <div role="form">
                            <div class="form-group"><label id="labelformnumberticketsvoucher">Number of vouchers</label> <input type="number" id="numero" min="1" max="5000" class="form-control" value="1" min="1" max="999"/></div>
                            <div class="form-group"><label id="labelformusernamevoucher">Username</label> <input type="text" id="username" class="form-control"/></div>
                            <div class="form-group" id="groupemail"><label id="labelformemailvoucher">Email</label> <input type="email" id="email" class="form-control"/></div>
                            <div class="form-group" id="groupvalidsince"><label id="labelformstartvoucher">Valid since</label> <input type="text" id="start" class="form-control"/></div>
                            <div class="form-group" id="groupvalidtill"><label id="labelformendvoucher">Valid till</label> <input type="text" id="end" class="form-control"/></div>
                            <div class="form-group"><label id="labelCommentvoucher">Comment</label> <textarea id="comment" class="form-control" rows="3"></textarea></div>
                            
                        </div>
                    </div>
                </div>

                <div class="ibox float-e-margins">
                
                <div class="ibox-content">
                    <div class="text-center">
                        <a class="btn btn-block  btn-primary" href="#" id="create"><i class="fa fa-ticket"></i><span id="buttoncreatevoucher"> Create new</span> </a>
                        <a class="btn btn-block btn-warning" href="#" id="check"><i class="fa fa-check-square"></i><span id="buttoncheck">Check form</span> </a>
                        <a class="btn btn-block  btn-primary" href="#" id="export"><i class="fa fa-file-excel-o"></i><span id="buttonexportvoucher"> Export</span> </a>
                    </div>
               </div>
            </div>
            </div>
            <div class="col-lg-5 col-md-5">
                <div class="widget navy-bg p-lg text-center" id="ticket" style="display:none;">
                    <div class="m-b-md">
                        <i class="fa fa-ticket fa-4x"></i>
                        <hr />
                        <h3 class="m-xs" id="labelprintusername">Username</h3>
                        <h1 id="resultusername">USERNAME</h1>
                    </div>
                </div>

                
                <div class="ibox float-e-margins" id="printpanel" style="display:none;">
                <div class="ibox-title">
                    <h5 id="headerprintparameters">Print parameters</h5>
                </div>
                <div class="ibox-content">
                        <div role="form">
                            <div class="checkbox i-checks"><label> <input type="checkbox" id="logocheck" /><i></i> <span id="printlogo"> Print Logo </span></label></div>
                            <div class="checkbox i-checks"><label> <input type="checkbox" id="speedcheck" /><i></i> <span id="printbandwidth">Print bandwidth</span></label></div>
                            <div class="checkbox i-checks"><label> <input type="checkbox" id="volumecheck" /><i></i> <span id="printvolume">Print volume </span></label></div>
                            <div class="text-center">
                                <a class="btn btn-block btn-primary" href="#" id="print"><i class="fa fa-print"></i> <span id="buttonprint">Print</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal inmodal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only" id="buttonclose">Close</span></button>
                                <h4 class="modal-title">Error</h4>
                            </div>
                            <div class="modal-body">
                                <p class="alert alert-danger" id="message"></p>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

            <div id="fastticket" style="display:none"></div>
            <div id="fastticketlogo" style="display:none"></div>
            <div id="fastticketlogo2" style="display:none"></div>
            <div id="fastticketspeed" style="display:none"></div>
            <div id="fastticketvolume" style="display:none"></div>
    </div>
</div>
</asp:Content>
