﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Site.Master" AutoEventWireup="true" CodeBehind="NewSite.aspx.cs" Inherits="Wifi360.NewSite" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <!--CSS LIB-->
    <link href="../css/plugins/switchery/switchery.css" rel="stylesheet" />
    <link href="../css/plugins/iCheck/custom.css" rel="stylesheet"/>
    <link href="../css/plugins/steps/jquery.steps.css" rel="stylesheet" />
    <link href="../css/animate.css" rel="stylesheet"/>
    <link href="../css/plugins/tagsinput/bootstrap-tagsinput.css" rel="stylesheet" />

    <!--JS LIB-->
    <script src="../js/jquery-3.1.1.min.js"></script>
    <script src="../js/plugins/switchery/switchery.js"></script>
    <script src="../js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="../js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="../js/plugins/staps/jquery.steps.min.js"></script>
    <script src="../js/plugins/validate/jquery.validate.min.js"></script>
    <script src="../js/plugins/tagsinput/bootstrap-tagsinput.js"></script>

    <!--Funcion para el formulario WIZARD -->
    <script>
        $(document).ready(function () {

            $('.tagsinput').tagsinput({
                tagClass: 'label label-primary'
            });
           
        });
    </script>
    
    <!--Funciones  -->
    <script type="text/javascript">
        $(document).ready(function () {

            //Lectura de la cookie para definir idioma del usuario
            function readCookie(name) {
                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
                }
                return null;
            };

            //FUnción de Precarga de elementos Select
            function bindSelect(id, valueMember, displayMember, source) {
                    $("#" + id).find('option').remove().end();
                    var cookie = readCookie("wifi360-language");
                    if (cookie != null) {
                        switch (cookie) {
                            case "es": $("#" + id).append('<option value="0">Seleccionar</option>'); break;
                            default: $("#" + id).append('<option value="0">Select</option>'); break;
                        }
                    }
                    else
                        $("#" + id).append('<option value="0">Select</option>');
                    $.each(source, function (index, item) {
                        $("#" + id).append('<option value="' + item[valueMember] + '"> ' + item[displayMember] + '</option>');
                    });
            };

            

            //PRECARGA DE LENGUAJES
            var data_languages = 'action=languages';
            $.ajax({
                url: "handlers/SMIHandler.ashx",
                data: data_languages,
                contentType: "application/json; charset=utf-8",
                dataType: "text",
                success: function (pReturn) {

                    //CARGA DE LOS Obj tipo checkbox
                    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
                    elems.forEach(function (html) {
                        var switchery = new Switchery(html, { color: '#1AB394' });
                    });
                    //FIN carga checkbox

                    var lReturn = JSON.parse(pReturn);
                    bindSelect('defaultlanguage', 'IdLanguage', 'Name', lReturn.list);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#searchResult").html(xhr.statusText)
                }
            });

           //PRECARGA DE VENDEDORES
           var data_vendor = 'action=vendors';
           $.ajax({
               url: "handlers/SMIHandler.ashx",
               data: data_vendor,
               contentType: "application/json; charset=utf-8",
               dataType: "text",
               success: function (pReturn_vendor) {

                   var lReturn_vendor = JSON.parse(pReturn_vendor);
                   bindSelect('vendors', 'IdVendor', 'Name', lReturn_vendor.list);
               },
               error: function (xhr, ajaxOptions, thrownError) {
                   $("#searchResult").html(xhr.statusText)
               }
           });

           //PRECARGA DE CURRENCIES
           var data_currencies = 'action=Currencies';
           $.ajax({
               url: "handlers/SMIHandler.ashx",
               data: data_currencies,
               contentType: "application/json; charset=utf-8",
               dataType: "text",
               //beforeSend: function () {
               //    $("#myModalWait").modal('show');  //Barra de progreso Bootsrap
               //},
               //complete: function () {
               //    $("#myModalWait").modal('hide');
               //},
               success: function (pReturn_currencies) {
                   var lReturn_currencies = JSON.parse(pReturn_currencies);
                   bindSelect('currencies', 'Code', 'Badge', lReturn_currencies.list);
               },
               error: function (xhr, ajaxOptions, thrownError) {
                   $("#searchResult").html(xhr.statusText)
               }
           });

           //FUNCION DE CREACION DE SITE
           function create_site() {

               if ($("#longusername").val() <= 3) {
                   check = false;
                   $("#message").html(GetMessage("0x0045"));
                   $("#myModalMessage").modal('show');
               } else {
                   data["longusername"] = $("#longusername").val();
               }

               if ($("#longpassword").val() <= 3) {
                   check = false;
                   $("#message").html(GetMessage("0x0046"));
                   $("#myModalMessage").modal('show');
               } else {
                   data["longpassword"] = $("#longpassword").val();
               }

               if ($("#longvoucherusername").val() <= 3) {
                   check = false;
                   $("#message").html(GetMessage("0x0047"));
                   $("#myModalMessage").modal('show');
               } else {


                   var data = {};
                   //tipo de acción
                   data["action"] = 'CREATESITE';

                   //PARTI I
                   //Step1
                   data["name"] = $("#name").val();
                   data["idlanguagedefault"] = $("#defaultlanguage").val();
                   data["currencycode"] = $("#currencies").val();
                   data["latitude"] = $("#latitude").val();
                   data["longitude"] = $("#longitude").val();
                   data["urlhotel"] = $("#urlhotel").val();
                   data["vendor"] = $("#vendors").val();
                   data["macauthenticate"] = $("#macauthenticate").is(':checked');
                   data["filtercontents"] = $("#filtercontents").is(':checked');
                   data["volumecombined"] = $("#volumecombined").is(':checked');

                   //Step2

                   data["prefix"] = $("#prefix").val();
                   data["longusername"] = $("#longusername").val();
                   data["longpassword"] = $("#longpassword").val();
                   data["voucherprefix"] = $("#voucherprefix").val();
                   data["longvoucherusername"] = $("#longvoucherusername").val();
                   data["printlogo"] = $("#logocheck").is(':checked');
                   data["printspeed"] = $("#speedcheck").is(':checked');
                   data["printvolume"] = $("#volumecheck").is(':checked');

                   //Step3
                   data["nse"] = $("#nse").val();
                   data["nseprincipalip"] = $("#nseprincipalip").val();
                   data["bandwidth"] = $("#bandwidth").val();
                   data["bandwidthup"] = $("#bandwidthup").val();

                   //Step4
                   data["loginmodule"] = $("#loginmodule").is(':checked');
                   data["freeaccessmodule"] = $("#freeaccessmodule").is(':checked');
                   data["payaccessmodule"] = $("#payaccessmodule").is(':checked');
                   data["socialnetworkmodule"] = $("#socialnetworkmodule").is(':checked');
                   data["customaccessmodule"] = $("#customaccessmodule").is(':checked');
                   data["voucheraccessmodule"] = $("#voucheraccessmodule").is(':checked');

                   //Step5
                   data["freeaccessrepeattime"] = $("#freeaccessrepeattime").val();

                   //Step6
                   data["paypaluser"] = $("#paypaluser").val();
                   data["urlreturnpaypal"] = $("#urlreturnpaypal").val();

                   //Step7
                   data["macblocking"] = $("#macblocking").is(':checked');
                   data["macbannedinterval"] = $("#bannedtime").val();
                   data["maxmacattemps"] = $("#maxmacattemps").val();
                   data["macattempsinterval"] = $("#macattemptinterval").val();

                   //Step8
                   data["mailfrom"] = $("#mailfrom").val();
                   data["smtp"] = $("#smtp").val();
                   data["smtpport"] = $("#smtpport").val();
                   data["smtpssl"] = $("#smtpssl").is(':checked');
                   data["smtpuser"] = $("#smtpuser").val();
                   data["smtppassword"] = $("#smtppassword").val();

                   //Step9
                   data["survey"] = $("#surveycheck").is(':checked');

                   //Step10 (Para esta parte habría que atacar tabla SocialNetworks y recorrer las llamadas a los inputs)
                   data["SocialNetworks"] = '1=0|2=0|3=0|4=0|5=0'; //Valores por defecto deshabilitados

                                            //'1=' + $("#Facebook").is(':checked') + "|" +
                                            //'2=' + $("#Instagram").is(':checked') + "|" +
                                            //'3=' + $("#Twitter").is(':checked') + "|" +
                                            //'4=' + $("#GooglePlus").is(':checked') + "|" +
                                            //'5=' + $("#Linkedin").is(':checked');

                   //Step11
                   data["enableddaylypass"] = $("#CheckEnabledDaylyPass").is(':checked');
                   data["valuedaylypass"] = $("#valueDaylyPass").val();

                   //PART II
                   //LOCATION NAMES
                   data["LocationNames"] = $("#LocationNames").val();
                   //PRIORITIES
                   data["Priorities"] = $("#Priorities").val();

                   //Llamada AJAX para carga de datos.
                   $.ajax({
                       url: "handlers/SMIHandler.ashx",
                       type: "POST",
                       data: JSON.stringify(data),
                       contentType: "application/json; charset=utf-8",
                       dataType: "text",
                       beforeSend: function () {
                           $("#myModalWait").modal('show');
                       },
                       complete: function () {
                           $("#myModalWait").modal('hide');
                       },
                       success: function (msg) {
                           if (msg.code == "OK") {
                               $("#message").removeClass().addClass("alert alert-success");
                               $("#message").html(GetMessage(msg.message));

                           }
                           else {
                               $("#message").removeClass().addClass("alert alert-danger");
                               $("#message").html(GetMessage(msg.message));
                           }
                           $("#myModalMessage").modal('show');
                       },
                       error: function (xhr, ajaxOptions, thrownError) {
                           $("#loading").html("We have a problem, please try again later.");
                           alert('error: ' + xhr.statusText);
                       }
                   });
               }
           }

           $("#create").click(function (e) {
               e.preventDefault();
               create_site(); //Creación del SITE
           });

           $("#back").click(function (e) {
               e.preventDefault();
               var url = "managesites.aspx";
               window.location.href = url;
           });
        }); 
    </script>
                           
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     
    <!--Breadcrump -->
    <div class="row wrapper border-bottom white-bg page-heading" >
        <div class="col-lg-10">
            <h2 id="headerpagelocationsetting">Super Admin</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="default.aspx">Dashboard</a>
                </li>
                <li id="breadpagefathersuperadmin">
                    <a>Super Admin</a>
                </li>
                <li class="active">
                    <strong id="breadpanewsite">New Site</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>

    <!-- FORM NUEVO SITE -->
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-9">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5 id="labelboxheadernewsite">New Site</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="panel-group" id="accordion">
                            <!--panel_1.1-->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h5 class="panel-title text-center">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" class=""><span id="labelacordionheaderbasic">Basic Settings</span></a>
                                    </h5>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true">
                                    <div class="panel-body">
                                        <div class="form-horizontal">
                                            <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformname">Name</label><div class="col-sm-9"><input type="text" class="form-control" id="name" /></div></div>
                                            <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformlanguage">Default language</label><div class="col-sm-9"><select class="form-control" id="defaultlanguage" ></select></div></div>
                                            <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformpaypalcurrency">Currency code</label><div class="col-sm-9"><select id="currencies" class="form-control"></select></div></div>
                                            <div class="form-group"> <label class="control-label col-sm-3" id="labellocationsettingsformlatitude">Latitude</label><div class="col-sm-4"><input type="text" value="0" class="form-control" id="latitude" /></div></div>
                                            <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformlongitude">Longitude</label><div class="col-sm-4"><input type="text" value="0" class="form-control" id="longitude" /></div></div>
                                            <div class="form-group"> <label class="control-label col-sm-3" id="labellocationsettingsformlanding">Landing page</label><div class="col-sm-9"><input type="text"  class="form-control" id="urlhotel" /></div></div>
                                            <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformvendor">Vendor</label><div class="col-sm-9"><select class="form-control" id="vendors" ></select></div></div>
                                            <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformreauthenticate">CLOUD Reauthentication</label>
                                                <div class="col-sm-2">
                                                    <div class="onoffswitch">
                                                         <input type="checkbox" class="onoffswitch-checkbox" id="macauthenticate"/>
                                                         <label class="onoffswitch-label" for="macauthenticate">
                                                             <span class="onoffswitch-inner"></span>
                                                             <span class="onoffswitch-switch"></span>
                                                         </label>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformfiltercontents">Content filtering (Mikrotik)</label>
                                                <div class="col-sm-2">
                                                    <div class="onoffswitch">
                                                         <input type="checkbox" class="onoffswitch-checkbox" id="filtercontents"/>
                                                         <label class="onoffswitch-label" for="filtercontents">
                                                             <span class="onoffswitch-inner"></span>
                                                             <span class="onoffswitch-switch"></span>
                                                         </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformvolumecombined">Aggregate volume limitation</label>
                                                <div class="col-sm-2">
                                                    <div class="onoffswitch">
                                                         <input type="checkbox" class="onoffswitch-checkbox" id="volumecombined"/>
                                                         <label class="onoffswitch-label" for="volumecombined">
                                                             <span class="onoffswitch-inner"></span>
                                                             <span class="onoffswitch-switch"></span>
                                                         </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--panel_1.2-->
                            <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title text-center">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed" aria-expanded="false"><span id="labelacordionheaderticket">Ticket / Voucher Settings</span></a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" aria-expanded="false">
                                <div class="panel-body">
                                    <div class="form-horizontal">
                                        <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformprefix">Username Prefix</label><div class="col-sm-9"><input type="text" class="form-control" id="prefix" /></div></div>
                                        <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformlongusername">Random username length without prefix (min. 3)</label><div class="col-sm-9"><input type="number" value="3" class="form-control" id="longusername" /></div></div>
                                        <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformlongpass">Password length (min. 3)</label><div class="col-sm-9"><input type="number" value="3" class="form-control" id="longpassword" /></div></div>
                                        <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformvoucherprefix">Voucher Username Prefix</label><div class="col-sm-9"><input type="text" class="form-control" id="voucherprefix" /></div></div>
                                        <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformvoucherlongusername">Random Voucher username length without prefix (min. 3)</label><div class="col-sm-9"><input type="number" value="3" class="form-control" id="longvoucherusername" /></div></div>
                                        <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformprintlogo">Print logo</label><div class="col-sm-9"><input type="checkbox" id="logocheck" class="js-switch" /></div></div>
                                        <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformprintspeed">Print conection bandwidth</label><div class="col-sm-9"><input type="checkbox" id="speedcheck" class="js-switch"/></div></div>
                                        <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformprintvolume">Print volume</label><div class="col-sm-9"><input type="checkbox" id="volumecheck" class="js-switch" /></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <!--panel_1.3-->
                            <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title text-center">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="collapsed" aria-expanded="false"><span id="labelacordionheaderline">Line Settings</span></a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" aria-expanded="false">
                                <div class="panel-body">
                                    <div class="form-horizontal">
                                        <div class="form-group"> <label class="control-label col-sm-3" id="labelnewsite_form_NSE">NSE (Nomadix)</label><div class="col-sm-9"><input type="text" id="nse" class="form-control" /></div></div>
                                        <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformip">Main IP</label><div class="col-sm-9"><input type="text" id="nseprincipalip" class="form-control" /></div></div>
                                        <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformbandwidthdown">Bandwidth Down</label><div class="col-sm-9"><div class="input-group m-b"><input type="text" value="0" id="bandwidth" class="form-control" /><span class="input-group-addon">Kbps</span></div></div></div>
                                        <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformbandwidthup">Bandwidth UP</label><div class="col-sm-9"><div class="input-group m-b"><input type="text" value="0" id="bandwidthup" class="form-control" /><span class="input-group-addon">Kbps</span></div></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <!--panel_1.4-->
                            <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title text-center">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" class="collapsed" aria-expanded="false"><span id="labelacordionheadermodule">Access Module Settings</span></a>
                                </h4>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse" aria-expanded="false">
                                <div class="panel-body">
                                    <div class="form-horizontal">
                                        <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformloginmodule">Login module</label><div class="col-sm-9"><input type="checkbox" id="loginmodule" class="js-switch"/></div></div>
                                        <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformfreeaccessmodule">Free Access module</label><div class="col-sm-9"><input type="checkbox" id="freeaccessmodule" class="js-switch" /></div></div>
                                        <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformpremiummodule">Premium Access module</label><div class="col-sm-9"><input type="checkbox" id="payaccessmodule" class="js-switch"/></div></div>
                                        <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformsocialaccessmodule">Social Access module</label><div class="col-sm-9"><input type="checkbox" id="socialnetworkmodule" class="js-switch" /></div></div>
                                        <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformcustommodule">Custom Access module</label><div class="col-sm-9"><input type="checkbox" id="customaccessmodule" class="js-switch" /></div></div>
                                        <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformvouchermodule">Voucher Access module</label><div class="col-sm-9"><input type="checkbox" id="voucheraccessmodule" class="js-switch" /></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <!--panel_1.5-->
                            <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title text-center">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive" class="collapsed" aria-expanded="false"><span id="labelacordionheaderfreeaccess">Free Access Settings</span></a>
                                </h4>
                            </div>
                            <div id="collapseFive" class="panel-collapse collapse" aria-expanded="false">
                                <div class="panel-body">
                                    <div class="form-horizontal">
                                        <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformfreeaccesstime">Time between free Access</label><div class="col-sm-9"><div class="input-group m-b"><input value="0" type="number" id="freeaccessrepeattime" class="form-control" /><span class="input-group-addon">h</span></div></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <!--panel_1.6-->
                            <div class="panel panel-default">
                           <div class="panel-heading">
                               <h4 class="panel-title text-center">
                                   <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix" class="collapsed" aria-expanded="false"><span id="labelacordionheaderpaypal">PayPal Settings</span></a>
                               </h4>
                           </div>
                           <div id="collapseSix" class="panel-collapse collapse" aria-expanded="false">
                               <div class="panel-body">
                                   <div class="form-horizontal">
                                       <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformpaypalusername">Paypal user</label><div class="col-sm-9"><input type="text" id="paypaluser" class="form-control" /></div></div>
                                       <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformpaypalurl">UrlLanding from Paypal</label><div class="col-sm-9"><input type="text" id="urlreturnpaypal" class="form-control" /></div></div>
                                   </div>
                               </div>
                           </div>
                       </div>
                            <!--panel_1.7-->
                            <div class="panel panel-default">
                           <div class="panel-heading">
                               <h4 class="panel-title text-center">
                                   <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" class="collapsed" aria-expanded="false"><span id="labelacordionheadersecurity">Security Settings</span></a>
                               </h4>
                           </div>
                           <div id="collapseSeven" class="panel-collapse collapse" aria-expanded="false">
                               <div class="panel-body">
                                   <div class="form-horizontal">
                                       <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformmacbanned">MAC Banned</label><div class="col-sm-9"><input type="checkbox" id="macblocking" class="js-switch"/></div></div>
                                       <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformbannedtime">Banned time</label><div class="col-sm-9"><div class="input-group m-b"><input type="number" value="0" id="bannedtime" class="form-control" /><span class="input-group-addon">s</span></div></div></div>
                                       <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformmaxattemps">Max number of MAC attemps</label><div class="col-sm-9"><input type="number" id="maxmacattemps" value="0" class="form-control" /></div></div>
                                       <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformtimeattemps">Range time MAC Attemps</label><div class="col-sm-9"><div class="input-group m-b"><input type="number" value="0" id="macattemptinterval" class="form-control" /><span class="input-group-addon">s</span></div></div></div>
                                   </div>
                               </div>
                           </div>
                       </div>
                            <!--panel_1.8-->
                            <div class="panel panel-default">
                           <div class="panel-heading">
                               <h4 class="panel-title text-center">
                                   <a data-toggle="collapse" data-parent="#accordion" href="#collapseEight" class="collapsed" aria-expanded="false"><span id="labelacordionheaderemail">Email Settings</span></a>
                               </h4>
                           </div>
                           <div id="collapseEight" class="panel-collapse collapse" aria-expanded="false">
                               <div class="panel-body">
                                   <div class="form-horizontal">
                                       <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformmailsender">Sender</label><div class="col-sm-9"><input type="text" id="mailfrom" class="form-control" /></div></div>
                                       <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformmailserver">SMTP Server</label><div class="col-sm-9"><input type="text" id="smtp" class="form-control" /></div></div>
                                       <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformmailport">SMTP Port</label><div class="col-sm-9"><input type="number" id="smtpport" class="form-control" /></div></div>
                                       <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformmailssl">SSL Encrypt</label><div class="col-sm-9"><input type="checkbox" id="smtpssl" class="js-switch"/></div></div>
                                       <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformmailuser">SMTP Users</label><div class="col-sm-9"><input type="text" id="smtpuser" class="form-control" /></div></div>
                                       <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformmailpass">SMTP Password</label><div class="col-sm-9"><input type="text" id="smtppassword" class="form-control" /></div></div>
                                   </div>
                               </div>
                           </div>
                       </div>
                            <!--panel_1.9-->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title text-center">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseNine" class="collapsed" aria-expanded="false"><span id="labelacordionheadersurvey">Survey Settings</span></a>
                                    </h4>
                                </div>
                                <div id="collapseNine" class="panel-collapse collapse" aria-expanded="false">
                                    <div class="panel-body">
                                        <div class="form-horizontal">
                                            <div class="form-group"> <label class="control-label col-sm-3" id="labelsettingsformmandatorysurvey">Mandatory in free access</label><div class="col-sm-9"><input type="checkbox" id="surveycheck" class="js-switch"/></div></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--panel_1.10-->
                            

                            <!--panel_1.11-->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title text-center">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseEleven" class="collapsed" aria-expanded="false"><span id="labelacordionheaderdaypass">Site Pass</span></a>
                                    </h4>
                                </div>
                                <div id="collapseEleven" class="panel-collapse collapse" aria-expanded="false">
                                    <div class="panel-body">
                                        <div class="form-horizontal" id="daypass2">
                                            <div class="form-group"> <label class="control-label col-sm-3" id="labelenabledDaylyPass">Enable Site Pass</label><div class="col-sm-9"><input type="checkbox" id="CheckEnabledDaylyPass" class="js-switch"/></div></div>
                                            <div class="form-group"> <label class="control-label col-sm-3" id="labelvalueDaylyPass">Site Pass</label><div class="col-sm-9"><input type="text" id="valueDaylyPass" class="form-control" /></div></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-group" id="accordion2">
                            <!--panel_2.1-->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h5 class="panel-title text-center">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwodotOne" aria-expanded="true" class=""><span id="labelacordionheaderLocation">Add Locations</span></a>
                                    </h5>
                                </div>
                                <div id="collapseTwodotOne" class="panel-collapse collapse in" aria-expanded="true">
                                    <div class="panel-body">
                                        <h5 id="formnewsitedescriptionloc">Enter the names of the different locations you want to create. The default locations "All_Zones" and "Internal" will be created</h5>
                                        <div class="form-horizontal">
                                            <div class="form-group"> 
                                                <label class="control-label col-sm-3" id="labelnewlocation_names">Names </label>
                                                <div class="col-sm-9"><input class="tagsinput form-control" type="text" value="" style="display: none;" id="LocationNames"/></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--panel_2.2-->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h5 class="panel-title text-center">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwodotTwo" aria-expanded="true" class=""><span id="labelacordionheaderText">Locations Priorities</span></a>
                                    </h5>
                                </div>
                                <div id="collapseTwodotTwo" class="panel-collapse collapse in" aria-expanded="true">
                                    <div class="panel-body">
                                        <div class="form-horizontal">
                                            <div class="form-group"> 
                                                <label class="control-label col-sm-3" id="labelPriorities">Priorities</label>
                                                <div class="col-sm-9"><input class="tagsinput form-control" type="text" value="" style="display: none;" id="Priorities"/></div>                                               
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>                          
                        </div>
                    </div>
                </div>
            </div>

        <!-- COLUMNA DCHA -->
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5 id="labelboxheaderbuttonsettingsNewSite">Actions</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <p>
                        <a href="#" class="btn btn-w-m btn-lg btn-primary btn-block" id="create"><i class="fa fa-save"></i> <span id="label_newsite_form_buttoncreate"> Create</span></a>
                        <a href="#" role="button" class="btn btn-w-m btn-lg btn-default btn-block" id="back" data-toggle="modal"><i class="fa fa-backward"></i> <span id="buttonbacklabel">Back </span></a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    </div>
</asp:Content>
