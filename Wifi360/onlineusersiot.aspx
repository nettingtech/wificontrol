﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Site.Master" AutoEventWireup="true" CodeBehind="onlineusersiot.aspx.cs" Inherits="Wifi360.onlineusersiot" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <script type="text/javascript">

            function readCookie(name) {
                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
                }
                return null;
            };

            function bindSelect(id, valueMember, displayMember, source) {
                $("#" + id).find('option').remove().end();
                var cookie = readCookie("wifi360-language");
                if (cookie != null) {
                    switch (cookie) {
                        case "es": $("#" + id).append('<option value="0" selected="selected">Todos</option>'); break;
                        default: $("#" + id).append('<option value="0" selected="selected">All</option>'); break;
                    }
                }
                else
                    $("#" + id).append('<option value="0" selected="selected">All </option>');
                $.each(source, function (index, item) {
                    $("#" + id).append('<option value="' + item[valueMember] + '"> ' + item[displayMember] + '</option>');
                });
            };

            function bindSelectAttribute(id, valueMember, displayMember, attribute, source) {
                $("#" + id).find('option').remove().end();
                var cookie = readCookie("wifi360-language");
                if (cookie != null) {
                    switch (cookie) {
                        case "es": $("#" + id).append('<option value="0">Seleccionar</option>'); break;
                        default: $("#" + id).append('<option value="0">Select </option>'); break;
                    }
                }
                else
                    $("#" + id).append('<option value="0">Select </option>');
                $.each(source, function (index, item) {
                    $("#" + id).append('<option value="' + item[valueMember] + '" rel="' + item[attribute] + '"> ' + item[displayMember] + '</option>');
                });
            };

            function bindSelectNone(id, valueMember, displayMember, source) {
                $("#" + id).find('option').remove().end();
                var cookie = readCookie("wifi360-language");
                if (cookie != null) {
                    switch (cookie) {
                        case "es": $("#" + id).append('<option value="0">Ninguno</option>'); break;
                        default: $("#" + id).append('<option value="0">None</option>'); break;
                    }
                }
                else
                    $("#" + id).append('<option value="0">None</option>');

                $.each(source, function (index, item) {
                    $("#" + id).append('<option value="' + item[valueMember] + '"> ' + item[displayMember] + '</option>');
                });
            };

            $(document).on("click", 'a[rel^="page"]', function (e) {
                e.preventDefault();
                var page = $(this).attr("attr");

                var cookie = readCookie("wifi360-language");
                var idmember = $("#members").val();
                var idmembertype = $('option:selected', $("#members")).attr('rel');
                var idfdsuser = $("#sellers").val();

                var dataString = 'action=Usersonline&page=' + page + "&iot=" + $("#typeiot").val() + "&username=" + $("#username").val() + "&idbillingtype=" + $("#billingtypes").val() + "&idpriority=" + $("#Priority").val() + "&idfilter=" + $("#filterprofile").val() + '&idmember=' + idmember + '&idmembertype=' + idmembertype + '&idfdsuser=' + idfdsuser;
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    beforeSend: function () {
                        $("#myModalWait").modal('show');
                    },
                    complete: function () {
                        $("#myModalWait").modal('hide');
                    },
                    success: function (pReturn) {
                        var showfilterprofile = $("#filtercontent").val();
                        var lReturn = JSON.parse(pReturn);
                        var cookieRol = readCookie("FDSUserRol");
                        var table = "";
                        if (cookie != null) {
                            switch (cookie) {
                                case "es": table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><th>Inicio</th><th>Nombre de usuario</th><th class='hidden-xs'>Habitación</th><th class='hidden-xs'>Tipo de acceso</th><th class='hidden-xs'>Total Descargado</th><th class='hidden-xs'>Total Subido</th><th>MAC</th><th class='hidden-xs'>Comentario</th><th class='hidden-xs'>Prioridad / Politica de grupo</th>";
                                    if (showfilterprofile != "false") table += "<th class='hidden-xs'>Filtrado de contenidos</th>";
                                    if (cookieRol != "3") table += "<th class='hidden-xs'>Vendedor</th>";
                                    break;
                                default: table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><th>Start</th><th>UserName</th><th class='hidden-xs'>Room</th><th class='hidden-xs'>Access type</th><th class='hidden-xs'>Download total</th><th class='hidden-xs'>Upload total</th><th>MAC</th><th class='hidden-xs'>Comment</th><th class='hidden-xs'>Priority / Group Policy</th>";
                                    if (showfilterprofile != "false") table += "<th class='hidden-xs'>Content Filter</th>";
                                    if (cookieRol != "3") table += "<th class='hidden-xs'>Seller</th>";
                                    break;
                            }
                        }
                        else {
                            table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><th>Start</th><th>UserName</th><th class='hidden-xs'>Room</th><th class='hidden-xs'>Access type</th><th class='hidden-xs'>Download total</th><th class='hidden-xs'>Upload total</th><th>MAC</th><th class='hidden-xs'>Comment</th><th class='hidden-xs'>Priority / Group Policy</th>";
                            if (showfilterprofile != "false") table += "<th class='hidden-xs'>Content Filter</th>";
                            if (cookieRol != "3") table += "<th class='hidden-xs'>Seller</th>";

                        }
                        $.each(lReturn.list, function (index, item) {
                            table += '<tr><td>' + item["Initiated"] + '</td><td><a href="/User.aspx?id=' + item["IdUser"] + '&return=UsersOnline.aspx">' + item["UserName"] + '</a></td><td class="hidden-xs">' + item["Room"] + '</td><td class="hidden-xs">' + item["AccessType"] + '</td><td class="hidden-xs">' + item["TotalDown"] + '</td><td class="hidden-xs">' + item["TotalUp"] + '</td><td>' + item["MAC"] + '</td><td class="hidden-xs">' + item["Comment"] + '</td><td class="hidden-xs">' + item["Priority"] + '</td>';
                            if (showfilterprofile != "false") table += '<td class="hidden-xs">' + item["Filter"] + '</td>';
                            if (cookieRol != "3") table += "<td class='hidden-xs'>" + item["fdsuser"] + "</td>";
                            table += '</tr>';
                        });
                        table += "</table>";
                        $("#searchResult").html(table);

                        var pagination = "<ul class='pagination'>";
                        var points1 = 0;
                        var points2 = 0;
                        x = parseInt(page);
                        for (var index = 0; index < lReturn.pageNumber; index++) {
                            if (lReturn.pageNumber <= 10) {
                                if (page == index)
                                    pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                else
                                    pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                            }
                            else {
                                if (page == index) {
                                    pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                                else if (index == 0) {
                                    pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                                else if (index == (lReturn.pageNumber - 1)) {
                                    pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                                else if (index < (x - 3)) {
                                    if (points1 == 0) {
                                        pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                        points1 = 1;
                                    }
                                }
                                else if (index > (x + 3)) {
                                    if (points2 == 0) {
                                        pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                        points2 = 1;
                                    }
                                }
                                else
                                    pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                            }
                        }

                        pagination += "</ul>";

                        $("#searchResult").append(pagination);

                        $("#myModalWait").modal('hide');

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#loading").html("Su ha producido un error durante el envio. Por favor intentelo más tarde.");
                        alert('error: ' + xhr.statusText);
                        $('#ButtonEnviar').show();
                    }
                });
            });

            $(document).ready(function () {

                var option;
                var cookie = readCookie("wifi360-language");
                if (cookie != null) {
                    switch (cookie) {
                        case "es": option = '<option value="0">No disponible</option>'; break;
                        default: option = '<option value="0">Not avalaible</option>'; break;
                    }
                }
                else {
                    option = '<option value="0">Not avalaible</option>';
                }

                var cookieRol = readCookie("FDSUserRol");
                if (cookieRol == "3")
                    $("#sellers").attr("disabled", "disabled");

                var page = 0;
                var username = "";
                var priority = "0";
                var billing = "0";
                var contentfilter = "0";

                $("#billingtypes").append(option);
                $("#Priority").append(option);
                $("#filterprofile").append(option);

                var dataStringGroup = 'action=GROUPMEMBERS';
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataStringGroup,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    success: function (pReturn) {
                        var lReturn = JSON.parse(pReturn);
                        bindSelectAttribute("members", "IdMember", "Name", "IdMemberType", lReturn.list);
                        if (lReturn.list.length == 1) {
                            $("#members").val(lReturn.list[0].IdMember).change();;
                            ChangeMember();
                        }
                        $("#sellers").empty();
                        var cookie = readCookie("wifi360-language");

                        var option;
                        if (cookie != null) {
                            switch (cookie) {
                                case "es": option = '<option value="0">No disponible para grupos</option>'; break;
                                default: option = '<option value="0">Not avalaible for groups </option>'; break;
                            }
                        }
                        else
                            option = '<option value="0">Not avalaible for groups </option>';

                        $("#sellers").append(option);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#modal-loading").hide();
                        $("#searchResult").html(xhr.statusText)
                    }
                });

                var dataString = 'action=sitesetting';
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    beforeSend: function () {
                        $("#myModalWait").modal('show');
                    },
                    complete: function () {
                        $("#myModalWait").modal('hide');
                    },
                    success: function (pReturn) {
                        var lReturn = JSON.parse(pReturn);
                        if (lReturn.filtercontents == true) {
                            $("#filtergroup").show();
                            $("#filtercontent").val("true");
                        }
                        else {
                            $("#filterprofile").attr("disabled", "disabled");
                            $("#filtercontent").val("false");
                        }

                        if (lReturn.vendor != 1) {
                            $("#filtercontent").val("false");
                            $("#filtergroup").attr("disabled", "disabled");
                            lReturn.filtercontents = false;
                        }
                        var page = 0;

                        var cookie = readCookie("wifi360-language");
                        var dataString2 = 'action=Usersonline&page=' + page + "&iot=" + $("#typeiot").val() + "&username=" + username + "&idbillingtype=" + billing + "&idpriority=" + priority + "&idfilter=" + contentfilter + '&idmember=0&idmembertype=0';
                        $.ajax({
                            url: "handlers/SMIHandler.ashx",
                            data: dataString2,
                            contentType: "application/json; charset=utf-8",
                            dataType: "text",
                            beforeSend: function () {
                                $("#myModalWait").modal('show');
                            },
                            complete: function () {
                                $("#myModalWait").modal('hide');
                            },
                            success: function (pReturn2) {
                                var lReturn2 = JSON.parse(pReturn2);
                                var cookieRol = readCookie("FDSUserRol");
                                var table = "";
                                if (cookie != null) {
                                    switch (cookie) {
                                        case "es": table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><th>Inicio</th><th>Nombre de usuario</th><th class='hidden-xs'>Habitación</th><th class='hidden-xs'>Tipo de acceso</th><th class='hidden-xs'>Total Descargado</th><th class='hidden-xs'>Total Subido</th><th>MAC</th><th class='hidden-xs'>Comentario</th><th class='hidden-xs'>Prioridad / Politica de grupo</th>";
                                            if (lReturn.filtercontents) table += "<th class='hidden-xs'>Filtrado de contenidos</th>";
                                            if (cookieRol != "3") table += "<th class='hidden-xs'>Vendedor</th>";
                                            break;
                                        default: table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><th>Start</th><th>UserName</th><th class='hidden-xs'>Room</th><th class='hidden-xs'>Access type</th><th class='hidden-xs'>Download total</th><th class='hidden-xs'>Upload total</th><th>MAC</th><th class='hidden-xs'>Comment</th><th class='hidden-xs'>Priority / Group Policy</th>";
                                            if (lReturn.filtercontents) table += "<th class='hidden-xs'>Content Filter</th>";
                                            if (cookieRol != "3") table += "<th class='hidden-xs'>Seller</th>";
                                            break;
                                    }
                                }
                                else {
                                    table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><th>Start</th><th>UserName</th><th class='hidden-xs'>Room</th><th class='hidden-xs'>Access type</th><th class='hidden-xs'>Download total</th><th class='hidden-xs'>Upload total</th><th>MAC</th><th class='hidden-xs'>Comment</th><th class='hidden-xs'>Priority / Group Policy</th>";
                                    if (lReturn.filtercontents) table += "<th class='hidden-xs'>Content Filter</th>";
                                    if (cookieRol != "3") table += "<th class='hidden-xs'>Seller</th>";

                                }
                                $.each(lReturn2.list, function (index, item) {
                                    table += '<tr><td>' + item["Initiated"] + '</td><td><a href="/User.aspx?id=' + item["IdUser"] + '&return=UsersOnline.aspx">' + item["UserName"] + '</a></td><td class="hidden-xs">' + item["Room"] + '</td><td class="hidden-xs">' + item["AccessType"] + '</td><td class="hidden-xs">' + item["TotalDown"] + '</td><td class="hidden-xs">' + item["TotalUp"] + '</td><td>' + item["MAC"] + '</td><td class="hidden-xs">' + item["Comment"] + '</td><td class="hidden-xs">' + item["Priority"] + '</td>';
                                    if (lReturn.filtercontents) table += '<td class="hidden-xs">' + item["Filter"] + '</td>';
                                    if (cookieRol != "3") table += "<td class='hidden-xs'>" + item["fdsuser"] + "</td>";
                                    table += '</tr>';
                                });
                                table += "</table>";


                                $("#searchResult").html(table);

                                var pagination = "<ul class='pagination'>";
                                var points1 = 0;
                                var points2 = 0;
                                x = parseInt(page);
                                for (var index = 0; index < lReturn2.pageNumber; index++) {
                                    if (lReturn2.pageNumber <= 10) {
                                        if (page == index)
                                            pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                        else
                                            pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                    }
                                    else {
                                        if (page == index) {
                                            pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                        }
                                        else if (index == 0) {
                                            pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                        }
                                        else if (index == (lReturn2.pageNumber - 1)) {
                                            pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                        }
                                        else if (index < (x - 3)) {
                                            if (points1 == 0) {
                                                pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                                points1 = 1;
                                            }
                                        }
                                        else if (index > (x + 3)) {
                                            if (points2 == 0) {
                                                pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                                points2 = 1;
                                            }
                                        }
                                        else
                                            pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                    }
                                }

                                pagination += "</ul>";

                                $("#searchResult").append(pagination);

                                $("#myModalWait").modal('hide');

                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                $("#loading").html("Su ha producido un error durante el envio. Por favor intentelo más tarde.");
                                alert('error: ' + xhr.statusText);
                                $('#ButtonEnviar').show();
                            }
                        });

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#modal-loading").hide();
                        $("#searchResult").html(xhr.statusText)
                    }
                });

                $("#members").change(function (e) {
                    ChangeMember();
                });

                function ChangeMember() {
                    var idmember = $("#members").val();
                    var idmembertype = $('option:selected', $("#members")).attr('rel');
                    if (idmember != "0") {
                        if (idmembertype != "1") {

                            var dataString = 'action=billingTypesfilter&idmember=' + idmember + '&idmembertype=' + idmembertype;
                            $.ajax({
                                url: "handlers/SMIHandler.ashx",
                                data: dataString,
                                contentType: "application/json; charset=utf-8",
                                dataType: "text",
                                success: function (pReturn) {
                                    var lReturn = JSON.parse(pReturn);
                                    bindSelect("billingtypes", "IdBillingType", "Description", lReturn.list);

                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    $("#modal-loading").hide();
                                    $("#searchResult").html(xhr.statusText)
                                }
                            });

                            var dataString3 = 'action=Rooms&idmember=' + idmember + '&idmembertype=' + idmembertype;
                            $.ajax({
                                url: "handlers/SMIHandler.ashx",
                                data: dataString3,
                                contentType: "application/json; charset=utf-8",
                                dataType: "text",
                                success: function (pReturn) {
                                    var lReturn = JSON.parse(pReturn);
                                    bindSelect("rooms", "IdRoom", "Name", lReturn.list);
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    $("#message").removeClass().addClass("alert alert-error");
                                    $("#message").html(xhr.statusText);
                                    $("#message").show();
                                    $("#modal-loading").hide();
                                }
                            });

                            var dataString3 = 'action=PRIORITIESFILTER&idmember=' + idmember + '&idmembertype=' + idmembertype;
                            $.ajax({
                                url: "handlers/SMIHandler.ashx",
                                data: dataString3,
                                contentType: "application/json; charset=utf-8",
                                dataType: "text",
                                beforeSend: function () {
                                    $("#myModalWait").modal('show');
                                },
                                complete: function () {
                                    $("#myModalWait").modal('hide');
                                },
                                success: function (pReturn) {
                                    var lReturn = JSON.parse(pReturn);
                                    bindSelect('Priority', 'IdPriority', 'Description', lReturn.list);

                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    $("#modal-loading").hide();
                                    $("#searchResult").html(xhr.statusText)
                                }
                            });

                            var dataString2 = 'action=filterprofilesFilter&idmember=' + idmember + '&idmembertype=' + idmembertype;
                            $.ajax({
                                url: "handlers/SMIHandler.ashx",
                                data: dataString2,
                                contentType: "application/json; charset=utf-8",
                                dataType: "text",
                                beforeSend: function () {
                                    $("#myModalWait").modal('show');
                                },
                                complete: function () {
                                    $("#myModalWait").modal('hide');
                                },
                                success: function (pReturn2) {
                                    var lReturn2 = JSON.parse(pReturn2);
                                    bindSelect('filterprofile', 'IdFilter', 'Description', lReturn2.list);

                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    $("#modal-loading").hide();
                                    $("#searchResult").html(xhr.statusText)
                                }
                            });

                            var cookieLocation = readCookie("location");
                            if (cookieLocation != null)
                                idmember = "";
                            var dataStringGroup = 'action=FDSUSERS&idmember=' + idmember + '&idmembertype=' + idmembertype;
                            $.ajax({
                                url: "handlers/SMIHandler.ashx",
                                data: dataStringGroup,
                                contentType: "application/json; charset=utf-8",
                                dataType: "text",
                                success: function (pReturn) {
                                    var lReturn = JSON.parse(pReturn);
                                    bindSelect("sellers", "idFDSUser", "Name", lReturn.list);

                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    $("#modal-loading").hide();
                                    $("#searchResult").html(xhr.statusText)
                                }
                            });
                        }
                        else {
                            $("#billingtypes").empty();
                            $("#rooms").empty();
                            $("#Priority").empty();
                            $("#filterprofile").empty();
                            $("#sellers").empty();
                            var cookie = readCookie("wifi360-language");

                            var option;
                            if (cookie != null) {
                                switch (cookie) {
                                    case "es": option = '<option value="0">No disponible para grupos</option>'; break;
                                    default: option = '<option value="0">Not avalaible for groups </option>'; break;
                                }
                            }
                            else
                                option = '<option value="0">Not avalaible for groups </option>';

                            $("#billingtypes").append(option);
                            $("#rooms").append(option);
                            $("#Priority").append(option);
                            $("#filterprofile").append(option);
                            $("#sellers").append(option);
                        }
                    }
                    else {
                        $("#billingtypes").empty();
                        $("#rooms").empty();
                        $("#Priority").empty();
                        $("#filterprofile").empty();
                        $("#sellers").empty();

                        var cookie = readCookie("wifi360-language");

                        var option;
                        if (cookie != null) {
                            switch (cookie) {
                                case "es": option = '<option value="0">No disponible</option>'; break;
                                default: option = '<option value="0">Not avalaible</option>'; break;
                            }
                        }
                        else
                            option = '<option value="0">Not avalaible</option>';

                        $("#billingtypes").append(option);
                        $("#rooms").append(option);
                        $("#Priority").append(option);
                        $("#filterprofile").append(option);
                        $("#sellers").append(option);
                    }
                };

                $('#refresh').click(function (e) {
                    e.preventDefault();
                    var page = 0;
                    var idmember = $("#members").val();
                    var idfdsuser = $("#sellers").val();
                    var idmembertype = $('option:selected', $("#members")).attr('rel');
                    var cookie = readCookie("wifi360-language");
                    var dataString = 'action=Usersonline&page=' + page + "&iot=" + $("#typeiot").val() + "&username=" + $("#username").val() + "&idbillingtype=" + $("#billingtypes").val() + "&idpriority=" + $("#Priority").val() + "&idfilter=" + $("#filterprofile").val() + '&idmember=' + idmember + '&idmembertype=' + idmembertype + '&idfdsuser=' + idfdsuser;
                    $.ajax({
                        url: "handlers/SMIHandler.ashx",
                        data: dataString,
                        contentType: "application/json; charset=utf-8",
                        dataType: "text",
                        beforeSend: function () {
                            $("#myModalWait").modal('show');
                        },
                        complete: function () {
                            $("#myModalWait").modal('hide');
                        },
                        success: function (pReturn) {
                            var showfilterprofile = $("#filtercontent").val();
                            var lReturn = JSON.parse(pReturn);
                            var cookieRol = readCookie("FDSUserRol");
                            var table = "";
                            if (cookie != null) {
                                switch (cookie) {
                                    case "es": table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><th>Inicio</th><th>Nombre de usuario</th><th class='hidden-xs'>Habitación</th><th class='hidden-xs'>Tipo de acceso</th><th class='hidden-xs'>Total Descargado</th><th class='hidden-xs'>Total Subido</th><th>MAC</th><th class='hidden-xs'>Comentario</th><th class='hidden-xs'>Prioridad / Politica de grupo</th>";
                                        if (showfilterprofile != "false") table += "<th class='hidden-xs'>Filtrado de contenidos</th>";
                                        if (cookieRol != "3") table += "<th class='hidden-xs'>Vendedor</th>";
                                        break;
                                    default: table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><th>Start</th><th>UserName</th><th class='hidden-xs'>Room</th><th class='hidden-xs'>Access type</th><th class='hidden-xs'>Download total</th><th class='hidden-xs'>Upload total</th><th>MAC</th><th class='hidden-xs'>Comment</th><th class='hidden-xs'>Priority / Group Policy</th>";
                                        if (showfilterprofile != "false") table += "<th class='hidden-xs'>Content Filter</th>";
                                        if (cookieRol != "3") table += "<th class='hidden-xs'>Seller</th>";
                                        break;
                                }
                            }
                            else {
                                table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><th>Start</th><th>UserName</th><th class='hidden-xs'>Room</th><th class='hidden-xs'>Access type</th><th class='hidden-xs'>Download total</th><th class='hidden-xs'>Upload total</th><th>MAC</th><th class='hidden-xs'>Comment</th><th class='hidden-xs'>Priority / Group Policy</th>";
                                if (showfilterprofile != "false") table += "<th class='hidden-xs'>Content Filter</th>";
                                if (cookieRol != "3") table += "<th class='hidden-xs'>Seller</th>";

                            }
                            $.each(lReturn.list, function (index, item) {
                                table += '<tr><td>' + item["Initiated"] + '</td><td><a href="/User.aspx?id=' + item["IdUser"] + '&return=UsersOnline.aspx">' + item["UserName"] + '</a></td><td class="hidden-xs">' + item["Room"] + '</td><td class="hidden-xs">' + item["AccessType"] + '</td><td class="hidden-xs">' + item["TotalDown"] + '</td><td class="hidden-xs">' + item["TotalUp"] + '</td><td>' + item["MAC"] + '</td><td class="hidden-xs">' + item["Comment"] + '</td><td class="hidden-xs">' + item["Priority"] + '</td>';
                                if (showfilterprofile != "false") table += '<td class="hidden-xs">' + item["Filter"] + '</td>';
                                if (cookieRol != "3") table += "<td class='hidden-xs'>" + item["fdsuser"] + "</td>";
                                table += '</tr>';
                            });
                            table += "</table>";

                            $("#searchResult").html(table);

                            var pagination = "<ul class='pagination'>";
                            var points1 = 0;
                            var points2 = 0;
                            x = parseInt(page);
                            for (var index = 0; index < lReturn.pageNumber; index++) {
                                if (lReturn.pageNumber <= 10) {
                                    if (page == index)
                                        pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                    else
                                        pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                                else {
                                    if (page == index) {
                                        pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                    }
                                    else if (index == 0) {
                                        pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                    }
                                    else if (index == (lReturn.pageNumber - 1)) {
                                        pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                    }
                                    else if (index < (x - 3)) {
                                        if (points1 == 0) {
                                            pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                            points1 = 1;
                                        }
                                    }
                                    else if (index > (x + 3)) {
                                        if (points2 == 0) {
                                            pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                            points2 = 1;
                                        }
                                    }
                                    else
                                        pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                            }

                            pagination += "</ul>";

                            $("#searchResult").append(pagination);

                            $("#myModalWait").modal('hide');

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#loading").html("Su ha producido un error durante el envio. Por favor intentelo más tarde.");
                            alert('error: ' + xhr.statusText);
                            $('#ButtonEnviar').show();
                        }
                    });
                });

                $('#search').click(function (e) {
                    e.preventDefault();
                    var page = 0;
                    var idmember = $("#members").val();
                    var idfdsuser = $("#sellers").val();
                    var idmembertype = $('option:selected', $("#members")).attr('rel');
                    var cookie = readCookie("wifi360-language");
                    var dataString = 'action=Usersonline&page=' + page + "&iot=" + $("#typeiot").val() + "&username=" + $("#username").val() + "&idbillingtype=" + $("#billingtypes").val() + "&idpriority=" + $("#Priority").val() + "&idfilter=" + $("#filterprofile").val() + '&idmember=' + idmember + '&idmembertype=' + idmembertype + '&idfdsuser=' + idfdsuser;
                    $.ajax({
                        url: "handlers/SMIHandler.ashx",
                        data: dataString,
                        contentType: "application/json; charset=utf-8",
                        dataType: "text",
                        beforeSend: function () {
                            $("#myModalWait").modal('show');
                        },
                        complete: function () {
                            $("#myModalWait").modal('hide');
                        },
                        success: function (pReturn) {
                            var showfilterprofile = $("#filtercontent").val();
                            var lReturn = JSON.parse(pReturn);
                            var cookieRol = readCookie("FDSUserRol");
                            var table = "";
                            if (cookie != null) {
                                switch (cookie) {
                                    case "es": table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><th>Inicio</th><th>Nombre de usuario</th><th class='hidden-xs'>Habitación</th><th class='hidden-xs'>Tipo de acceso</th><th class='hidden-xs'>Total Descargado</th><th class='hidden-xs'>Total Subido</th><th>MAC</th><th class='hidden-xs'>Comentario</th><th class='hidden-xs'>Prioridad / Politica de grupo</th>";
                                        if (showfilterprofile != "false") table += "<th class='hidden-xs'>Filtrado de contenidos</th>";
                                        if (cookieRol != "3") table += "<th class='hidden-xs'>Vendedor</th>";
                                        break;
                                    default: table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><th>Start</th><th>UserName</th><th class='hidden-xs'>Room</th><th class='hidden-xs'>Access type</th><th class='hidden-xs'>Download total</th><th class='hidden-xs'>Upload total</th><th>MAC</th><th class='hidden-xs'>Comment</th><th class='hidden-xs'>Priority / Group Policy</th>";
                                        if (showfilterprofile != "false") table += "<th class='hidden-xs'>Content Filter</th>";
                                        if (cookieRol != "3") table += "<th class='hidden-xs'>Seller</th>";
                                        break;
                                }
                            }
                            else {
                                table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><th>Start</th><th>UserName</th><th class='hidden-xs'>Room</th><th class='hidden-xs'>Access type</th><th class='hidden-xs'>Download total</th><th class='hidden-xs'>Upload total</th><th>MAC</th><th class='hidden-xs'>Comment</th><th class='hidden-xs'>Priority / Group Policy</th>";
                                if (showfilterprofile != "false") table += "<th class='hidden-xs'>Content Filter</th>";
                                if (cookieRol != "3") table += "<th class='hidden-xs'>Seller</th>";

                            }
                            $.each(lReturn.list, function (index, item) {
                                table += '<tr><td>' + item["Initiated"] + '</td><td><a href="/User.aspx?id=' + item["IdUser"] + '&return=UsersOnline.aspx">' + item["UserName"] + '</a></td><td class="hidden-xs">' + item["Room"] + '</td><td class="hidden-xs">' + item["AccessType"] + '</td><td class="hidden-xs">' + item["TotalDown"] + '</td><td class="hidden-xs">' + item["TotalUp"] + '</td><td>' + item["MAC"] + '</td><td class="hidden-xs">' + item["Comment"] + '</td><td class="hidden-xs">' + item["Priority"] + '</td>';
                                if (showfilterprofile != "false") table += '<td class="hidden-xs">' + item["Filter"] + '</td>';
                                if (cookieRol != "3") table += "<td class='hidden-xs'>" + item["fdsuser"] + "</td>";
                                table += '</tr>';
                            });
                            table += "</table>";

                            $("#searchResult").html(table);

                            var pagination = "<ul class='pagination'>";
                            var points1 = 0;
                            var points2 = 0;
                            x = parseInt(page);
                            for (var index = 0; index < lReturn.pageNumber; index++) {
                                if (lReturn.pageNumber <= 10) {
                                    if (page == index)
                                        pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                    else
                                        pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                                else {
                                    if (page == index) {
                                        pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                    }
                                    else if (index == 0) {
                                        pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                    }
                                    else if (index == (lReturn.pageNumber - 1)) {
                                        pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                    }
                                    else if (index < (x - 3)) {
                                        if (points1 == 0) {
                                            pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                            points1 = 1;
                                        }
                                    }
                                    else if (index > (x + 3)) {
                                        if (points2 == 0) {
                                            pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                            points2 = 1;
                                        }
                                    }
                                    else
                                        pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                            }

                            pagination += "</ul>";

                            $("#searchResult").append(pagination);

                            $("#myModalWait").modal('hide');

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#loading").html("Su ha producido un error durante el envio. Por favor intentelo más tarde.");
                            alert('error: ' + xhr.statusText);
                            $('#ButtonEnviar').show();
                        }
                    });
                });
            });

        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
            <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
                        <h2 id="headerpageusersonlineiot">Internet of Things</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="default.aspx">Dashboard</a>
                </li>
                <li>
                    <a id="breadpagefatherusersonlineiot">Internet of Things</a>
                </li>
                <li class="active">
                    <strong id="breadpageusersonlineiot">Online Devices</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5 id="labelboxheaderusersonlineoit">Online Devices <small> List of online devices</small></h5>
                </div>
                <div class="ibox-content">
                    <div role="form" class="form-inline">
                       <a href="#" id="refresh" class="btn btn-w-m btn-primary btn-block" ><i class="fa fa-refresh"></i> <span id="buttonlabelrefreshusersonline">Refresh</span> </a>
                    </div>
                                    <div class="ibox-content">
                    <div class="ibox border-bottom">
                    <div class="ibox-title">
                        <h5 id="headerboxfilterusersvalid">Search filters</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-down"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content" style="display: none;">
                        <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto;">
                            <div class="scroll_content" style="overflow: hidden; width: auto; ">
                        <div role="form">
                            <div class="row">
                            <div class="col-sm-9">
                                    <div class="form-group">
                                        <label for="username" id="labelformusermanagementuser">Users</label>
                                        <input type="text" id="username" class="form-control"/>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <label for="username" id="labelformusermanagementseller">Sellers</label>
                                    <select id="sellers" class="form-control">
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label for="username" id="labelformusermanagementmembers">Group Members</label>

                                        <select id="members" class="form-control">
                                        </select>
                                    </div>

                                    <div class="col-sm-3">
                                        <label for="username" id="labelformusermanagementbillingtype">Access type</label>

                                        <select id="billingtypes" class="form-control">
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="username" id="labelformusermanagementPriority">Priority / Group Policy</label>
                                        <select id="Priority" class="form-control"></select>
                                    </div>
                                    <div class="col-sm-3" id="filtergroup">
                                        <label for="username" id="labelformusermanagementfilterprofile">Content Filter</label>
                                        <select id="filterprofile" class="form-control"></select>
                                    </div>
                                </div>
                                
                            </div>

                            <div class="form-group"><label for="username" id="labelformbillingtypeiot">User type</label>
                                <select id="typeiot" class="form-control">
                                    <option value="0">All Devices</option>
                                    <option value="1">Personal Devices</option>
                                    <option value="2" selected="selected">IoT Devices</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <a href="#" id="search" class="btn btn-w-m btn-primary" ><i class="fa fa-search"></i><span id="labelformusermanagementbuttonsearch"> Search </span></a>
                            </div>
                        </div>
                        </div><div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 198.02px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                    </div>
                </div>
                </div>
                    <div id="searchResult"></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input id="filtercontent" type="text" value="true" hidden="hidden" />
</asp:Content>
