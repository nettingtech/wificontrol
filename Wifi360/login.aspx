﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="Wifi360.login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

   <title>Netting WiFi Dashboard</title>

    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet" />

    <link href="css/animate.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" />

    <script src="js/jquery-2.1.1.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <script src="js/utils.js"></script>
    <link href="css/login.css" rel="stylesheet" />
    <script type="text/javascript">
        $(document).ready(function () {
            if (window.location.host == "hispasat.netting.tech") {
                $("#loginimage").attr("src", "/img/portales/logo_hispasat.png");
            }
            else if (window.location.host == "fibratel.netting.tech")
                $("#loginimage").attr("src", "/img/portales/logo_fibratel.png");
            else if (window.location.host == "on.netting.tech")
                $("#loginimage").attr("src", "/img/portales/logo_on.png");
            else if (window.location.host == "nexmachina.netting.tech")
                $("#loginimage").attr("src", "/img/portales/logo_nexmachina.png");

            $(document).keypress(function (e) {
                if (e.which == 13) {
                    if ($("#login").is(":visible"))
                        Login();
                    else if ($("#site").is(":visible"))
                        SelectSite();
                }
            });

            function createCookie(name, value, days) {
                if (days) {
                    var date = new Date();
                    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                    var expires = "; expires=" + date.toGMTString();
                }
                else var expires = "";
                document.cookie = name + "=" + value + expires + "; path=/";
            }

            function bindSelect(id, valueMember, displayMember, source) {
                $("#" + id).find('option').remove().end();
                $("#" + id).append('<option value="0">Select</option>');
                $.each(source, function (index, item) {
                    $("#" + id).append('<option value="' + item[valueMember] + '"> ' + item[displayMember] + '</option>');
                });
            };

            function Login() {
                var username = $("#username").val();
                var password = $("#password").val();

                var dataString = 'action=login&u=' + username + '&p=' + password;
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (msg.code == 'OK') {
                            $("#username").val("");
                            $("#password").val("");

                            switch (msg.idtype) {
                                case "1":
                                    createCookie("group", msg.idgroup, 1);
                                    createCookie("parent", msg.idgroup, 1);
                                    window.location.href = msg.message;
                                    break;
                                case "2":
                                    createCookie("site", msg.idsite, 1);
                                    window.location.href = msg.message;
                                    break;
                                case "3": //LOCATION
                                    createCookie("location", msg.idlocation, 1);
                                    createCookie("location-noconfig", " ", 1);
                                    window.location.href = msg.message;
                                    break;
                            }
                        }
                        else {
                            $("#message").show();
                            $("#message").html(GetMessage(msg.message));
                            $("#message").removeClass().addClass("alert alert-danger");

                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        $("#message").html(xhr.statusText);
                        $("#message").show();
                        $("#message").removeClass().addClass("alert alert-error");
                    }
                });
            };

            function SelectSite() {
                if ($("#sites").val() == 0) {
                    $("#message").show();
                    $("#message").html("Debe seleccionar un site");
                    $("#message").removeClass().addClass("alert alert-danger");
                }
                else {
                    var h = $("#sites").val();
                    createCookie("site", h, 1);
                    window.location.href = "Default.aspx"
                }
            };

            $("#login").click(function (e) {

                e.preventDefault();
                Login();

            });

            $("#site").click(function (e) {

                e.preventDefault();
                SelectSite();

            });


        });
</script>
</head>
<body class="full-black-bg">
    <form id="form1" runat="server">
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                <h1 class="logo-name"><img src="img/Netting-Logo.png" alt="Netting Tech" class="img-responsive" id="loginimage" /></h1>

            </div>
            <h3>Welcome to Netting WiFi Dashboard</h3>
            <div class="m-t" role="form">
                <div class="form-group" id="user">
                    <input type="text" id="username" class="form-control" placeholder="Username" required=""/>
                </div>
                <div class="form-group" id="pass">
                    <input type="password" id="password" class="form-control" placeholder="Password" required=""/>
                </div>
                <input type="button" id="login" class="btn btn-primary block full-width m-b" value="Login" />
                
                <div class="form-group" id="selectSites" style="display:none">
                    <label for="sites">Select site</label>
                    <select id="sites" class="form-control"></select>
                    
                </div>
                <input type="button" id="site" class="btn btn-primary block full-width m-b" value="Continue" style="display:none;" />
                <div id="message" style="display:none"></div>
            </div>
            <p class="m-t copyright"> <small>Netting Tech &copy; 2019</small> </p>
        </div>
    </div>
    </form>
</body>
</html>
