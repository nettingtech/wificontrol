﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Site.Master" AutoEventWireup="true" CodeBehind="Billing.aspx.cs" Inherits="Wifi360.Billing" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/calendar.css" rel="stylesheet" />
    <script src="js/calendar.full.min.js"></script>
    <script src="js/moment.js"></script>

       <script type="text/javascript">
           $(document).ready(function () {

               var currencySymbol;
               var option;

               var cookie = readCookie("wifi360-language");
               var cookieRol = readCookie("FDSUserRol");
               if (cookie != null) {
                   switch (cookie) {
                       case "es": var calendar = new CalendarPopup('#start', {dayOfWeekStart: 1, showMask: false, locale: 'es', showWeekIndex: true, format: 'DD/MM/YYYY HH:mm', i18n: { es: { "Chose": "Elegir", "Close": "Cerrar", "Time": "Hora" } } });
                           var calendar2 = new CalendarPopup('#end', {dayOfWeekStart: 1, showMask: false, locale: 'es', showWeekIndex: true, format: 'DD/MM/YYYY HH:mm', i18n: { es: { "Chose": "Elegir", "Close": "Cerrar", "Time": "Hora" } } });
                           option = '<option value="0">No disponible</option>'; break;
                       default: var calendar = new CalendarPopup('#start', { showMask: false, showWeekIndex: true, format: 'DD/MM/YYYY HH:mm' });
                           var calendar2 = new CalendarPopup('#end', { showMask: false, showWeekIndex: true, format: 'DD/MM/YYYY HH:mm' });
                           option = '<option value="0">Not avalaible</option>'; break;
                   }
               }
               else {
                   var calendar = new CalendarPopup('#start', {dayOfWeekStart: 1, showMask: false, showWeekIndex: true, format: 'DD/MM/YYYY HH:mm' });
                   var calendar2 = new CalendarPopup('#end', {dayOfWeekStart: 1, showMask: false, showWeekIndex: true, format: 'DD/MM/YYYY HH:mm' });
                   option = '<option value="0">Not avalaible</option>';
               }

               if (cookieRol == "3")
                   $("#sellers").attr("disabled", "disabled");

               $("#sellers").append(option);

               $(".calendar-popup-input-wrapper").attr("style", "display:block;")
               $("#rooms").append(option);

               $("#lidashboard").removeClass();
               $("#libilling").removeClass().addClass("active");
               $("#lidashboard").removeClass();
               var dataString2 = 'action=sitesetting';
               $.ajax({
                   url: "handlers/SMIHandler.ashx",
                   data: dataString2,
                   contentType: "application/json; charset=utf-8",
                   dataType: "text",
                   success: function (pReturn) {
                       var lReturn = JSON.parse(pReturn);
                       currencySymbol = lReturn.CurrencySymbol;
                   },
                   error: function (xhr, ajaxOptions, thrownError) {
                       $("#modal-loading").hide();
                       $("#searchResult").html(xhr.statusText)
                   }
               });


               var dataStringGroup = 'action=GROUPMEMBERS';
               $.ajax({
                   url: "handlers/SMIHandler.ashx",
                   data: dataStringGroup,
                   contentType: "application/json; charset=utf-8",
                   dataType: "text",
                   success: function (pReturn) {
                       var lReturn = JSON.parse(pReturn);
                       bindSelectAttribute("members", "IdMember", "Name", "IdMemberType", lReturn.list);
                       if (lReturn.list.length == 1) {
                           $("#members").val(lReturn.list[0].IdMember).change();;
                           ChangeMember();
                       }

                   },
                   error: function (xhr, ajaxOptions, thrownError) {
                       $("#modal-loading").hide();
                       $("#searchResult").html(xhr.statusText)
                   }
               });

               var start = $("#start").val();
               var end = $("#end").val();
               var room = "0";
               var page = 0;
               var billing = "0";

               var dataString = 'action=searchBilling&r=' + room + '&s=' + start + '&e=' + end + '&b=' + billing + '&idmember=0&idmembertype=0';
               $.ajax({
                   url: "handlers/SMIHandler.ashx",
                   data: dataString,
                   contentType: "application/json; charset=utf-8",
                   dataType: "text",
                   beforeSend: function () {
                       $("#myModalWait").modal('show');
                   },
                   complete: function () {
                       $("#myModalWait").modal('hide');
                   },

                   success: function (pReturn) {
                       var lReturn = JSON.parse(pReturn);
                       
                       //Table Body
                       var user_visible = false;
                       var table_body = "";
                       var cookie = readCookie("wifi360-language");

                       if (($("#sellers option[value='0']").text() != 'Not avalaible') && $("#sellers option[value='0']").text() != 'No disponible') {
                           $.each(lReturn.sellers, function (index, item) {
                               if ($("#sellers option[value='" + item["idFDSUser"] + "']").text() == "")
                                   $("#sellers").append("<option value=" + item["idFDSUser"] + ">" + item["Name"] + "</option>");

                           });
                       }


                       $.each(lReturn.list, function (index, item) {
                           table_body += '<tr id="row_' + item["idbilling"] + '" ><td>' + item["Date"] + '</td><td>' + item["Room"] + '</td><td>' + item["Product"] + '</td><td><span id="amount_' + item["idbilling"] + '">' + item["Amount"] + '</span></td><td>' + item["Flag"] + '</td>';
                           if (item["iduser"] == 0) {
                               table_body += '<td> -- </td>';
                               table_body += '<td><a href="#" rel=' + item["idbilling"] + ' func="delete"><i class="fa fa-trash"></i></a></tr>';
                           }
                           else {
                               if (cookieRol == "3") {
                                   if (item["propierty"]) {
                                       if (item["fdsusername"] != null)
                                           table_body += '<td>' + item["fdsusername"] + '</td>';
                                       else
                                           table_body += '<td></td>';

                                       table_body += '<td><a href="User.aspx?id=' + item["iduser"] + '&return=billing.aspx">' + item["username"] + '</a></td>';
                                       table_body += '<td></td></tr>';
                                       user_visible = true;
                                   }
                                   else {
                                       if (item["fdsusername"] != null)
                                           table_body += '<td>' + item["fdsusername"] + '</td>';
                                       else
                                           table_body += '<td></td>';

                                       table_body += '<td>' + item["username"] + '</td>';
                                       table_body += '<td></td></tr>';
                                       user_visible = true;
                                   }
                               }
                               else if (cookieRol == "5") {
                                   if (item["fdsusername"] != null)
                                       table_body += '<td>' + item["fdsusername"] + '</td>';
                                   else
                                       table_body += '<td></td>';

                                   table_body += '<td><a href="User.aspx?id=' + item["iduser"] + '&return=billing.aspx">' + item["username"] + '</a></td>';
                                   table_body += '<td></td></tr>';
                                   user_visible = true;
                               }
                               else {
                                   if (item["fdsusername"] != null)
                                       table_body += '<td>' + item["fdsusername"] + '</td>';
                                   else
                                       table_body += '<td></td>';

                                   table_body += '<td><a href="User.aspx?id=' + item["iduser"] + '&return=billing.aspx">' + item["username"] + '</a></td>';
                                   table_body += '<td><a href="#" rel=' + item["idbilling"] + ' func="delete"><i class="fa fa-trash"></i></a></td></tr>';
                                   user_visible = true;
                               }
                           }
                               
                           
                       });
                       table_body += "</table>";
                       //EnD table Body

                       //Table header
                       var table_header = "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'>";                      
                       if (cookie != null) {
                           switch (cookie) {
                               case "es":
                                   table_header += "<th>Fecha</th><th>Habitación</th><th>Producto</th><th>Importe</th><th>Plataforma</th><th>Vendedor</th>";
                                   if (user_visible)
                                       table_header += "<th>Usuario</th>";
                                   break;
                               default:
                                   table_header += "<th>Date</th><th>Room</th><th>Product</th><th>Amount</th><th>Platform</th><th>Seller</th>";
                                   if (user_visible)
                                       table_header += "<th>User</th>";
                                   break;
                           }
                       }
                       else{
                           table_header += "<th>Date</th><th>Room</th><th>Product</th><th>Amount</th><th>Platform</th><th>Seller</th>";
                           if (user_visible)
                               table_header += "<th>User</th>";                           
                       }
                       table_header += "<th>#</th>";
                       //end Table Header

                       var table = table_header + table_body;

                       $("#total").html(lReturn.total);

                       $("#searchResult").html(table);

                       var pagination = "<ul class='pagination'>";
                       var points1 = 0;
                       var points2 = 0;
                       x = parseInt(page);
                       for (var index = 0; index < lReturn.pageNumber; index++) {
                           if (lReturn.pageNumber <= 10) {
                               if (page == index)
                                   pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                               else
                                   pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                           }
                           else {
                               if (page == index) {
                                   pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                               }
                               else if (index == 0) {
                                   pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                               }
                               else if (index == (lReturn.pageNumber - 1)) {
                                   pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                               }
                               else if (index < (x - 3)) {
                                   if (points1 == 0) {
                                       pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                       points1 = 1;
                                   }
                               }
                               else if (index > (x + 3)) {
                                   if (points2 == 0) {
                                       pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                       points2 = 1;
                                   }
                               }
                               else
                                   pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                           }
                       }

                       pagination += "</ul>";

                       $("#searchResult").append(pagination);

                   },
                   error: function (xhr, ajaxOptions, thrownError) {
                       $("#modal-loading").hide();
                       $("#searchResult").html(xhr.statusText)
                   }
               });


               $("#members").change(function (e) {
                   ChangeMember();
               });

               function ChangeMember() {
                   var idmember = $("#members").val();
                   var idmembertype = $('option:selected', $("#members")).attr('rel');
                   if (idmember != "0") {
                       var start = $("#start").val();
                       var end = $("#end").val();

                       if (idmembertype != "1") {
                           var cookie = readCookie("wifi360-language");
                           var dataString3 = 'action=Rooms&idmember=' + idmember + '&idmembertype=' + idmembertype;
                           $.ajax({
                               url: "handlers/SMIHandler.ashx",
                               data: dataString3,
                               contentType: "application/json; charset=utf-8",
                               dataType: "text",
                               success: function (pReturn) {
                                   var lReturn = JSON.parse(pReturn);
                                   bindSelect("rooms", "IdRoom", "Name", lReturn.list);
                               },
                               error: function (xhr, ajaxOptions, thrownError) {
                                   $("#message").removeClass().addClass("alert alert-error");
                                   $("#message").html(xhr.statusText);
                                   $("#message").show();
                                   $("#modal-loading").hide();
                               }
                           });

                           var cookieLocation = readCookie("location");
                           if (cookieLocation != null)
                               idmember = "";

                           var dataStringGroup = 'action=FDSUSERS&idmember=' + idmember + '&idmembertype=' + idmembertype + '&start=' + start + '&end=' + end;
                           $.ajax({
                               url: "handlers/SMIHandler.ashx",
                               data: dataStringGroup,
                               contentType: "application/json; charset=utf-8",
                               dataType: "text",
                               success: function (pReturn) {
                                   var lReturn = JSON.parse(pReturn);
                                   bindSelect("sellers", "idFDSUser", "Name", lReturn.list);

                               },
                               error: function (xhr, ajaxOptions, thrownError) {
                                   $("#modal-loading").hide();
                                   $("#searchResult").html(xhr.statusText)
                               }
                           });

                           if (cookie != null) {
                               switch (cookie) {
                                   case "es":$("#labelformusermanagementseller").html("Vendedor"); break;
                                   default: $("#labelformusermanagementseller").html("Seller"); break;
                               }
                           }
                           else {
                               $("#labelformusermanagementseller").html("Seller");
                           }
                       }
                       else {
                           $("#billingtypes").empty();
                           $("#rooms").empty();
                           $("#sellers").empty();
                           var cookie = readCookie("wifi360-language");

                           var dataStringGroup = 'action=FDSUSERS&idmember=' + idmember + '&idmembertype=' + idmembertype + '&start=' + start + '&end=' + end;
                           $.ajax({
                               url: "handlers/SMIHandler.ashx",
                               data: dataStringGroup,
                               contentType: "application/json; charset=utf-8",
                               dataType: "text",
                               success: function (pReturn) {
                                   var lReturn = JSON.parse(pReturn);
                                   bindSelect("sellers", "idFDSUser", "Name", lReturn.list);

                               },
                               error: function (xhr, ajaxOptions, thrownError) {
                                   $("#modal-loading").hide();
                                   $("#searchResult").html(xhr.statusText)
                               }
                           });

                           var option;
                           if (cookie != null) {
                               switch (cookie) {
                                   case "es": option = '<option value="0">No disponible</option>';
                                       $("#labelformusermanagementseller").html("Vendedores con facturación"); break;
                                   default:
                                       $("#labelformusermanagementseller").html("Sellers with billing");
                                       option = '<option value="0">Not avalaible</option>'; break;
                               }
                           }
                           else {
                               option = '<option value="0">Not avalaible</option>';
                               $("#labelformusermanagementseller").html("Sellers with billing");
                           }

                           $("#rooms").append(option);
                          // $("#sellers").append(option);
                       }
                   }
                   else {
                       $("#billingtypes").empty();
                       $("#rooms").empty();
                       $("#Priority").empty();
                       $("#filterprofile").empty();
                       $("#sellers").empty();

                       var cookie = readCookie("wifi360-language");

                       var option;
                       if (cookie != null) {
                           switch (cookie) {
                               case "es": option = '<option value="0">No disponible</option>';
                                   $("#labelformusermanagementseller").html("Vendedor"); break;
                               default: option = '<option value="0">Not avalaible</option>';
                                   $("#labelformusermanagementseller").html("Seller"); break;
                           }
                       }
                       else {
                           option = '<option value="0">Not avalaible</option>';
                           $("#labelformusermanagementseller").html("Seller");
                       }

                       $("#billingtypes").append(option);
                       $("#rooms").append(option);
                       $("#Priority").append(option);
                       $("#filterprofile").append(option);
                       $("#sellers").append(option);

                   }
               };

               $("#search").click(function () {

                   var start = $("#start").val();
                   var end = $("#end").val();
                   var room = $("#rooms").val();
                   var billing = $("#billingtypes").val();
                   var page = 0;
                   var s = moment(start, "DD/MM/YYYY HH:mm");
                   var e = moment(end, "DD/MM/YYYY HH:mm");
                   var idmember = $("#members").val();
                   var idmembertype = $('option:selected', $("#members")).attr('rel');
                   var idfdsuser = $("#sellers").val();
                   
                   if ((($("#start").val() == '') && ($("#end").val() == '')) || (s <= e) && ($("#start").val() != '') && ($("#end").val() != '')) {
                       var dataString = 'action=searchBilling&r=' + room + '&s=' + start + '&e=' + end + '&b=' + billing + '&idmember=' + idmember + '&idmembertype=' + idmembertype + '&idfdsuser='+idfdsuser;
                       $.ajax({
                           url: "handlers/SMIHandler.ashx",
                           data: dataString,
                           contentType: "application/json; charset=utf-8",
                           dataType: "text",
                           beforeSend: function () {
                               $("#myModalWait").modal('show');
                           },
                           complete: function () {
                               $("#myModalWait").modal('hide');
                           },
                           success: function (pReturn) {
                               var lReturn = JSON.parse(pReturn);

                               //Table Body
                               var user_visible = false;
                               var table_body = "";
                               var cookie = readCookie("wifi360-language");

                               if (($("#sellers option[value='0']").text() != 'Not avalaible') && $("#sellers option[value='0']").text() != 'No disponible') {
                                   $.each(lReturn.sellers, function (index, item) {
                                       if ($("#sellers option[value='" + item["idFDSUser"] + "']").text() == "")
                                           $("#sellers").append("<option value=" + item["idFDSUser"] + ">" + item["Name"] + "</option>");

                                   });
                               }

                               $.each(lReturn.list, function (index, item) {
                                   table_body += '<tr id="row_' + item["idbilling"] + '" ><td>' + item["Date"] + '</td><td>' + item["Room"] + '</td><td>' + item["Product"] + '</td><td><span id="amount_' + item["idbilling"] + '">' + item["Amount"] + '</span></td><td>' + item["Flag"] + '</td>';
                                   if (item["iduser"] == 0) {
                                       table_body += '<td> -- </td>';
                                       table_body += '<td><a href="#" rel=' + item["idbilling"] + ' func="delete"><i class="fa fa-trash"></i></a></tr>';
                                   }
                                   else {
                                       if (cookieRol == "3") {
                                           if (item["propierty"]) {
                                               if (item["fdsusername"] != null)
                                                   table_body += '<td>' + item["fdsusername"] + '</td>';
                                               else
                                                   table_body += '<td></td>';

                                               table_body += '<td><a href="User.aspx?id=' + item["iduser"] + '&return=billing.aspx">' + item["username"] + '</a></td>';
                                               table_body += '<td></td></tr>';
                                               user_visible = true;
                                           }
                                           else {
                                               if (item["fdsusername"] != null)
                                                   table_body += '<td>' + item["fdsusername"] + '</td>';
                                               else
                                                   table_body += '<td></td>';

                                               table_body += '<td>' + item["username"] + '</td>';
                                               table_body += '<td></td></tr>';
                                               user_visible = true;
                                           }
                                       }
                                       else if (cookieRol == "5") {
                                           if (item["fdsusername"] != null)
                                               table_body += '<td>' + item["fdsusername"] + '</td>';
                                           else
                                               table_body += '<td></td>';

                                           table_body += '<td><a href="User.aspx?id=' + item["iduser"] + '&return=billing.aspx">' + item["username"] + '</a></td>';
                                           table_body += '<td></td></tr>';
                                           user_visible = true;
                                       }
                                       else {
                                           if (item["fdsusername"] != null)
                                               table_body += '<td>' + item["fdsusername"] + '</td>';
                                           else
                                               table_body += '<td></td>';

                                           table_body += '<td><a href="User.aspx?id=' + item["iduser"] + '&return=billing.aspx">' + item["username"] + '</a></td>';
                                           table_body += '<td><a href="#" rel=' + item["idbilling"] + ' func="delete"><i class="fa fa-trash"></i></a></td></tr>';
                                           user_visible = true;
                                       }
                                   }


                               });
                               table_body += "</table>";
                               //EnD table Body

                               //Table header
                               var table_header = "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'>";
                               if (cookie != null) {
                                   switch (cookie) {
                                       case "es":
                                           table_header += "<th>Fecha</th><th>Habitación</th><th>Producto</th><th>Importe</th><th>Plataforma</th><th>Vendedor</th>";
                                           if (user_visible)
                                               table_header += "<th>Usuario</th>";
                                           break;
                                       default:
                                           table_header += "<th>Date</th><th>Room</th><th>Product</th><th>Amount</th><th>Platform</th><th>Seller</th>";
                                           if (user_visible)
                                               table_header += "<th>User</th>";
                                           break;
                                   }
                               }
                               else {
                                   table_header += "<th>Date</th><th>Room</th><th>Product</th><th>Amount</th><th>Platform</th><th>Seller</th>";
                                   if (user_visible)
                                       table_header += "<th>User</th>";
                               }
                               table_header += "<th>#</th>";
                               //end Table Header

                               var table = table_header + table_body;

                               $("#total").html(lReturn.total);

                               $("#searchResult").html(table);

                               var pagination = "<ul class='pagination'>";
                               var points1 = 0;
                               var points2 = 0;
                               x = parseInt(page);
                               for (var index = 0; index < lReturn.pageNumber; index++) {
                                   if (lReturn.pageNumber <= 10) {
                                       if (page == index)
                                           pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                       else
                                           pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                   }
                                   else {
                                       if (page == index) {
                                           pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                       }
                                       else if (index == 0) {
                                           pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                       }
                                       else if (index == (lReturn.pageNumber - 1)) {
                                           pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                       }
                                       else if (index < (x - 3)) {
                                           if (points1 == 0) {
                                               pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                               points1 = 1;
                                           }
                                       }
                                       else if (index > (x + 3)) {
                                           if (points2 == 0) {
                                               pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                               points2 = 1;
                                           }
                                       }
                                       else
                                           pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                   }
                               }

                               pagination += "</ul>";

                               $("#searchResult").append(pagination);

                           },
                           error: function (xhr, ajaxOptions, thrownError) {
                               $("#modal-loading").hide();
                               $("#searchResult").html(xhr.statusText)
                           }
                       });
                   }
                   else {
                       $("#message").html(GetMessage("0x0022"));
                       $("#myModal").modal('show');
                   }
               });

               $(document).on("click", 'a[rel^="page"]', function (e) {
                   e.preventDefault();
                   var page = $(this).attr("attr");

                   var start = $("#start").val();
                   var end = $("#end").val();
                   var room = $("#rooms").val();
                   var billing = $("#billingtypes").val();
                   var idmember = $("#members").val();
                   var idfdsuser = $("#sellers").val();
                   var idmembertype = $('option:selected', $("#members")).attr('rel');

                   var dataString = 'action=searchBilling&r=' + room + '&s=' + start + '&e=' + end + '&page=' + page + '&b=' + billing + '&idmember=' + idmember + '&idmembertype=' + idmembertype + '&idfdsuser=' + idfdsuser;
                   $.ajax({
                       url: "handlers/SMIHandler.ashx",
                       data: dataString,
                       contentType: "application/json; charset=utf-8",
                       dataType: "text",
                       beforeSend: function () {
                           $("#myModalWait").modal('show');
                       },
                       complete: function () {
                           $("#myModalWait").modal('hide');
                       },
                       success: function (pReturn) {
                           var lReturn = JSON.parse(pReturn);
                           //Table Body
                           var user_visible = false;
                           var table_body = "";
                           var cookie = readCookie("wifi360-language");

                           if (($("#sellers option[value='0']").text() != 'Not avalaible') && $("#sellers option[value='0']").text() != 'No disponible') {
                               $.each(lReturn.sellers, function (index, item) {
                                   if ($("#sellers option[value='" + item["idFDSUser"] + "']").text() == "")
                                       $("#sellers").append("<option value=" + item["idFDSUser"] + ">" + item["Name"] + "</option>");

                               });
                           }


                           $.each(lReturn.list, function (index, item) {
                               table_body += '<tr id="row_' + item["idbilling"] + '" ><td>' + item["Date"] + '</td><td>' + item["Room"] + '</td><td>' + item["Product"] + '</td><td><span id="amount_' + item["idbilling"] + '">' + item["Amount"] + '</span></td><td>' + item["Flag"] + '</td>';
                               if (item["iduser"] == 0) {
                                   table_body += '<td> -- </td>';
                                   table_body += '<td> -- </td>';
                                   table_body += '<td><a href="#" rel=' + item["idbilling"] + ' func="delete"><i class="fa fa-trash"></i></a></tr>';
                                   user_visible = true;
                               }
                               else {
                                   if (cookieRol == "3") {
                                       if (item["propierty"]) {
                                           if (item["fdsusername"] != null)
                                               table_body += '<td>' + item["fdsusername"] + '</td>';
                                           else
                                               table_body += '<td></td>';

                                           table_body += '<td><a href="User.aspx?id=' + item["iduser"] + '&return=billing.aspx">' + item["username"] + '</a></td>';
                                           table_body += '<td></td></tr>';
                                           user_visible = true;
                                       }
                                       else {
                                           if (item["fdsusername"] != null)
                                               table_body += '<td>' + item["fdsusername"] + '</td>';
                                           else
                                               table_body += '<td></td>';

                                           table_body += '<td>' + item["username"] + '</td>';
                                           table_body += '<td></td></tr>';
                                           user_visible = true;
                                       }
                                   }
                                   else if (cookieRol == "5") {
                                       if (item["fdsusername"] != null)
                                           table_body += '<td>' + item["fdsusername"] + '</td>';
                                       else
                                           table_body += '<td></td>';

                                       table_body += '<td><a href="User.aspx?id=' + item["iduser"] + '&return=billing.aspx">' + item["username"] + '</a></td>';
                                       table_body += '<td></td></tr>';
                                       user_visible = true;
                                   }
                                   else {
                                       if (item["fdsusername"] != null)
                                           table_body += '<td>' + item["fdsusername"] + '</td>';
                                       else
                                           table_body += '<td></td>';

                                       table_body += '<td><a href="User.aspx?id=' + item["iduser"] + '&return=billing.aspx">' + item["username"] + '</a></td>';
                                       table_body += '<td><a href="#" rel=' + item["idbilling"] + ' func="delete"><i class="fa fa-trash"></i></a></td></tr>';
                                       user_visible = true;
                                   }
                               }


                           });
                           table_body += "</table>";
                           //EnD table Body

                           //Table header
                           var table_header = "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'>";
                           if (cookie != null) {
                               switch (cookie) {
                                   case "es":
                                       table_header += "<th>Fecha</th><th>Habitación</th><th>Producto</th><th>Importe</th><th>Plataforma</th><th>Vendedor</th>";
                                       if (user_visible)
                                           table_header += "<th>Usuario</th>";
                                       break;
                                   default:
                                       table_header += "<th>Date</th><th>Room</th><th>Product</th><th>Amount</th><th>Platform</th><th>Seller</th>";
                                       if (user_visible)
                                           table_header += "<th>User</th>";
                                       break;
                               }
                           }
                           else {
                               table_header += "<th>Date</th><th>Room</th><th>Product</th><th>Amount</th><th>Platform</th><th>Seller</th>";
                               if (user_visible)
                                   table_header += "<th>User</th>";
                           }
                           table_header += "<th>#</th>";
                           //end Table Header

                           var table = table_header + table_body;

                           $("#total").html(lReturn.total);

                           $("#searchResult").html(table);

                           var pagination = "<ul class='pagination'>";
                           var points1 = 0;
                           var points2 = 0;
                           x = parseInt(page);
                           for (var index = 0; index < lReturn.pageNumber; index++) {
                               if (lReturn.pageNumber <= 10) {
                                   if (page == index)
                                       pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                   else
                                       pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                               }
                               else {
                                   if (page == index) {
                                       pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                   }
                                   else if (index == 0) {
                                       pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                   }
                                   else if (index == (lReturn.pageNumber - 1)) {
                                       pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                   }
                                   else if (index < (x - 3)) {
                                       if (points1 == 0) {
                                           pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                           points1 = 1;
                                       }
                                   }
                                   else if (index > (x + 3)) {
                                       if (points2 == 0) {
                                           pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                           points2 = 1;
                                       }
                                   }
                                   else
                                       pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                               }
                           }

                           pagination += "</ul>";

                           $("#searchResult").append(pagination);

                       },
                       error: function (xhr, ajaxOptions, thrownError) {
                           $("#modal-loading").hide();
                           $("#searchResult").html(xhr.statusText)
                       }
                   });
               });

               $(document).on("click", 'a[rel^="hand"]', function (e) {
                   e.preventDefault();

                   var id = $(this).attr("attr");
                   $("#idbill").html(id);
                   $('#myModalcharge').modal('show');
               });

               $(document).on("click", 'a[func^="delete"]', function (e) {
                   e.preventDefault();

                   var id = $(this).attr("rel");
                   $("#deletebutton").attr("rel", $(this).attr("rel"));
                   $('#myModaDelete').modal('show');
                   
               });


               $('#handbill').click(function (e) {
                   e.preventDefault();


                   var id = $("#idbill").html();

                   var dataString = 'action=billingHandProcess&id=' + id;
                   $.ajax({
                       url: "handlers/SMIHandler.ashx",
                       data: dataString,
                       contentType: "application/json; charset=utf-8",
                       dataType: "json",
                       beforeSend: function () {
                           $("#modal-loading").show()
                       },
                       complete: function () {
                           $("#modal-loading").hide()
                       },
                       success: function (pReturn) {
                           if (pReturn.code == "OK") {
                               $("#" + id).removeClass().html("<i class=\"icon-hand-right\"></i>");
                               $('#myModalcharge').modal('hide');
                           }

                       },
                       error: function (xhr, ajaxOptions, thrownError) {
                           $("#modal-loading").hide();
                           $("#searchResult").html(xhr.statusText)
                       }
                   });
               });

               $("#export").click(function () {

                   var start = $("#start").val();
                   var end = $("#end").val();
                   var room = $("#rooms").val();
                   var billing = $("#billingtypes").val();
                   var page = 0;
                   var s = moment(start, "DD/MM/yyyy HH:mm");
                   var e = moment(end, "DD/MM/yyyy HH:mm");
                   var idmember = $("#members").val();
                   var idfdsuser = $("#sellers").val();
                   var idmembertype = $('option:selected', $("#members")).attr('rel');
                   if ((($("#start").val() == '') && ($("#end").val() == '')) || (s <= e) && ($("#start").val() != '') && ($("#end").val() != '')) {

                       var dataString = 'action=export&s=' + start + '&e=' + end + '&r=' + room + '&b=' + billing + '&idmember=' + idmember + '&idmembertype=' + idmembertype + '&idfdsuser=' + idfdsuser;
                       $.ajax({
                           url: "handlers/SMIHandler.ashx",
                           data: dataString,
                           contentType: "application/json; charset=utf-8",
                           dataType: "json",
                           beforeSend: function () {
                               $("#myModalWait").modal('show');
                           },
                           complete: function () {
                               $("#myModalWait").modal('hide');
                           },

                           success: function (pReturn) {
                               if (pReturn.code == "OK") {
                                   var url = pReturn.message;
                                   window.location.href = url;
                               }
                           },
                           error: function (xhr, ajaxOptions, thrownError) {
                               $("#myModalWait").hide();
                           }
                       });

                   }
                   else {
                        $("#message").html(GetMessage("0x0022"));
                       $("#myModal").modal('show');
                   }


               });

               $('#deletebutton').click(function (e) {
                   e.preventDefault();

                   var id = $("#deletebutton").attr("rel");

                   var dataString = 'action=deletebill&id=' + id;
                   $.ajax({
                       url: "handlers/SMIHandler.ashx",
                       data: dataString,
                       contentType: "application/json; charset=utf-8",
                       dataType: "json",
                       beforeSend: function () {
                           $("#myModalWait").modal('show');
                       },
                       complete: function () {
                           $("#myModalWait").modal('hide');
                       },

                       success: function (pReturn) {
                           if (pReturn.code == "OK") {
                               var amount = parseFloat($("#amount_" + id).html().replace("€", "").replace(",", "."));
                               var total = parseFloat($("#total").html().replace("€", "").replace(",", "."));
                               $("#row_" + id).remove();
                               $("#total").html((total - amount).toFixed(2).replace(".", ","));
                               $("#total").append(" €");
                               $('#myModaDelete').modal('hide');

                               $("#message").removeClass().addClass("alert alert-success");
                               $("#message").html(GetMessage(pReturn.message));
                               $("#myModal").modal('show');
                           }
                           else
                           {
                               $("#message").removeClass().addClass("alert alert-danger");
                               $("#message").html(GetMessage(pReturn.message));
                               $("#myModal").modal('show');
                           }
                       },
                       error: function (xhr, ajaxOptions, thrownError) {
                           $("#modal-loading").hide();
                           $("#searchResult").html(xhr.statusText)
                       }
                   });
               });

           });

           function readCookie(name) {
               var nameEQ = name + "=";
               var ca = document.cookie.split(';');
               for (var i = 0; i < ca.length; i++) {
                   var c = ca[i];
                   while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                   if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
               }
               return null;
           };

           function bindSelectAttribute(id, valueMember, displayMember, attribute, source) {
               $("#" + id).find('option').remove().end();
               var cookie = readCookie("wifi360-language");
               if (cookie != null) {
                   switch (cookie) {
                       case "es": $("#" + id).append('<option value="0">Seleccionar</option>'); break;
                       default: $("#" + id).append('<option value="0">Select </option>'); break;
                   }
               }
               else
                   $("#" + id).append('<option value="0">Select </option>');
               $.each(source, function (index, item) {
                   $("#" + id).append('<option value="' + item[valueMember] + '" rel="' + item[attribute] + '"> ' + item[displayMember] + '</option>');
               });
           };

           function bindSelect(id, valueMember, displayMember, source) {
               $("#" + id).find('option').remove().end();
               var cookie = readCookie("wifi360-language");
               if (cookie != null) {
                   switch (cookie) {
                       case "es": $("#" + id).append('<option value="0">Seleccionar</option>'); break;
                       default: $("#" + id).append('<option value="0">Select</option>'); break;
                   }
               }
               else
                   $("#" + id).append('<option value="0">Select</option>');

               $.each(source, function (index, item) {
                   $("#" + id).append('<option value="' + item[valueMember] + '"> ' + item[displayMember] + '</option>');
               });
           };

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 id="headerpagebilling">Billing</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="default.aspx">Dashboard</a>
                </li>
                <li class="active">
                    <strong id="breadlabelbilling">Billing</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5 id="labelboxheaderbilling">Billing<small> List of billings</small></h5>
                </div>
                <div class="ibox-content">
                    <div class="ibox border-bottom">
                    <div class="ibox-title">
                        <h5 id="headerboxfilterusersvalid">Search filters</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-down"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content" style="display: none;">
                        <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto;">
                            <div class="scroll_content" style="overflow: hidden; width: auto; ">
                                <div role="form">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label class="font-normal control-label" id="labelformbillinmombers"> Group Members</label> <select id="members" class="form-control"></select>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="font-normal control-label" id="labelformbillingroom"> Room</label> <select id="rooms" class="form-control"></select>
                                        </div>
                                                                                <div class="col-sm-4">
                                            <label class="font-normal control-label" id="labelformusermanagementseller"> Seller</label> <select id="sellers" class="form-control"></select>
                                        </div>

                                    </div>
                                </div>
                                    <div class="form-group" id="data_5">
                                        <label class="font-normal" id="labelformbillingdaterante"> Date range </label>
                                        <div class="input-daterange input-group" id="datepicker">
                                            <span class="input-group-addon" id="labelformbillingdatefrom"> From </span>
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" id="start" name="start" value=""/>
                                            <span class="input-group-addon" id="labelbillingsince"> to </span>
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" id="end" name="end" value="" />
                                        </div>
                                    </div>

                                <div class="form-group">
                                    <a href="#" id="search" class="btn btn-w-m btn-primary" ><i class="fa fa-search"></i><span id="labelbuttonsearchbilling"> Search</span> </a>
                                    <a href="#" id="export" class="btn btn-w-m btn-default" ><i class="fa fa-file-excel-o"></i> <span id="labelbuttonexportbilling"> Export</span> </a>
                                </div>
                            </div>                  
                            </div>
                        <div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 198.02px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                    </div>
                </div>
                    <div class="row">
                        <div class="col-lg-12">
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-9"></div>
                        <div class="col-lg-3">
                            <div class="widget style1 navy-bg">
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa fa-money fa-3x"></i>
                        </div>
                        <div class="col-xs-8 text-right">
                            
                            <h2 class="font-bold" id="total"></h2>
                        </div>
                    </div>
                </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="searchResult"></div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <div class="modal inmodal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Cerrar</span></button>
                                <h4 class="modal-title">Error</h4>
                            </div>
                            <div class="modal-body">
                                <p class="alert alert-danger" id="message"></p>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>


        <div class="modal inmodal fade" id="myModaDelete" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h3 id="deletebilltitle">Delete Bill</h3>
                            </div>
                            <div class="modal-body">
                                <div id="messageDelete">
                                    Are you sure to delete this bill?
                                </div>
                            </div>
                          <div class="modal-footer">
                            <a href="#" class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i><span id="buttonbacklabel">Back</span> </a>
                            <a href="#" class="btn btn-danger"  aria-hidden="true" id="deletebutton"><i class="fa fa-trash"></i><span id="buttondeletelabel"> Delete</span> </a>
                          </div>
                        </div>
                    </div>
                </div>
        </div>
</asp:Content>
