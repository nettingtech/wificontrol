﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Site.Master" AutoEventWireup="true" CodeBehind="fastticket.aspx.cs" Inherits="Wifi360.fastticket" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
                <script type="text/javascript">

                    $(document).ready(function () {
                        var dataString = 'action=SITESETTING';
                        $.ajax({
                            url: "handlers/SMIHandler.ashx",
                            data: dataString,
                            contentType: "application/json; charset=utf-8",
                            dataType: "text",
                            beforeSend: function () {
                                $("#myModalWait").modal('show');
                            },
                            complete: function () {
                                $("#myModalWait").modal('hide');
                            },
                            success: function (pReturn) {
                                var lReturn = JSON.parse(pReturn);

                                if (lReturn.EnabledFasTicket == true)
                                    $("#enabledfastticket").attr('checked', true);
                                if (lReturn.PrintLogo == true)
                                    $("#logocheck").attr('checked', true);
                                if (lReturn.PrintVolume == true)
                                    $("#volumecheck").attr('checked', true);
                                if (lReturn.PrintSpeed == true)
                                    $("#speedcheck").attr('checked', true);
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                $("#modal-loading").hide();
                                $("#searchResult").html(xhr.statusText)
                            }
                        });

                        $("#print").click(function (e) {
                            e.preventDefault();

                            var dataString = 'action=newfastticket';
                            $.ajax({
                                url: "handlers/SMIHandler.ashx",
                                data: dataString,
                                contentType: "application/json; charset=utf-8",
                                dataType: "text",
                                beforeSend: function () {
                                    $("#myModalWait").modal('show');
                                },
                                complete: function () {
                                    $("#myModalWait").modal('hide');
                                },
                                success: function (pReturn) {
                                    var lReturn = JSON.parse(pReturn);
                                    if (lReturn.IdUser == "0") {
                                        $("#result").addClass("alert alert-error");
                                        $("#result").html("Error: There is no default access type selected for Fast Ticket.");
                                        $("#result").show();
                                        $("#modal-loading").hide();
                                    }
                                    else {
                                        var fast = '';
                                        $(".body").append('<br />');
                                        fast = '<div class="fastticket2">';
                                        if ($("#logocheck").is(':checked'))
                                            fast += '<img src="/resources/images/' + lReturn.IdUser + '/logo-bw.png" alt="fast" />';
                                        else
                                            fast += '<span>' + lReturn.AccessType + '</span>';
                                        fast += '<hr/>';
                                        fast += '<b>USERNAME: ' + lReturn.UserName + '</b><br />';
                                        fast += '<b>PASSWORD: ' + lReturn.Password + '</b><br />';
                                        fast += '<b>MAX DEVICES: ' + lReturn.MaxDevices + '</b><br />';
                                        fast += '<b>SINCE: ' + lReturn.ValidSince + '</b><br />';
                                        fast += '<b>TILL: ' + lReturn.ValidTill + '</b><br />';
                                        fast += '<b>CREDIT: ' + lReturn.TimeCredit + '</b><br />'
                                        if ($("#speedcheck").is(':checked')) {
                                            fast += '<b>SPEED UP: ' + lReturn.BWUp + ' kbps</b><br />';
                                            fast += '<b>SPEED DW: ' + lReturn.BWDown + ' kbps</b><br />';
                                        }
                                        if ($("#volumecheck").is(':checked')) {
                                            fast += '<b>VOLUME UP: ' + lReturn.VolumeUp + ' MB</b><br />';
                                            fast += '<b>VOLUME DW: ' + lReturn.VolumeDown + ' MB</b><br />';
                                        }

                                        fast += '<hr/>';
                                        fast += '</div>';

                                        $("#fastticketprint").html(fast);
                                        //var d = new Date();

                                        var fastticketprinthtml = $("#fastticketprint").html();

                                        var printWindow = window.open('', '', 'height=500,width=500');
                                        printWindow.document.write('<html><head><title>Fast Ticket</title>');
                                        printWindow.document.write('<link rel="stylesheet" href="resources/css/default.css" type="text/css" />');
                                        printWindow.document.write('<style> html, body {font-family: sans-serif; padding: 0px; margin:0px;}</style>');
                                        printWindow.document.write('</head><body>');
                                        printWindow.document.write('<div style="page-break-after:always;">');
                                        printWindow.document.write(fast);
                                        //printWindow.document.write('printed: ' + d.toLocaleString());
                                        printWindow.document.write('</div>');
                                        printWindow.document.write('</body></html>');
                                        printWindow.document.close();
                                        setTimeout(function () { printWindow.print(); }, 1000);

                                    }

                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    $("#message").removeClass().addClass("alert alert-error");
                                    $("#message").html(xhr.statusText);
                                    $("#myModalMessage").modal('show');
                                }
                            });
                        });
                    });
   </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Users Management</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="default.aspx">Dashboard</a>
                </li>
                <li>
                    <a>Tickets</a>
                </li>
                <li class="active">
                    <strong>Fast Ticket</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                    <h5>Fast Ticket<small> Create new fast Ticket</small></h5>
                </div>
                    <div class="ibox-content">
                     <div class="form-horizontal">
                <div class="form-group"> <label class="control-label col-sm-4">Print Logo</label><div class="col-sm-8"><input type="checkbox" id="logocheck" /></div></div>
                <div class="form-group"> <label class="control-label col-sm-4">Print Speed</label><div class="col-sm-8"><input type="checkbox" id="speedcheck" /></div></div>
                <div class="form-group"> <label class="control-label col-sm-4">Print Volume</label><div class="col-sm-8"><input type="checkbox" id="volumecheck" /></div></div>

                <a href="#" class="btn btn-w-m btn-lg btn-primary btn-block" id="print"><i class="fa fa-print"></i> Print</a>

                <input type="text" id="idhotel" hidden="hidden"  style="display:none;"/>
                <div id="#fastticketprint" style="display:none;"></div>
                </div>
                        </div>
                </div>
            </div>

            <div class="modal inmodal fade" id="myModalMessage" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                            </div>
                            <div class="modal-body">
                                <div id="message">
                                    
                                </div>
                            </div>
                          <div class="modal-footer">
                            <a href="#" class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Back</a>
                          </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</asp:Content>
