﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Site.Master" AutoEventWireup="true" CodeBehind="filterprofile.aspx.cs" Inherits="Wifi360.filterprofile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>

        function readCookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        };

        function getParameters() {
            var searchString = window.location.search.substring(1)
              , params = searchString.split("&")
              , hash = {}
            ;

            for (var i = 0; i < params.length; i++) {
                var val = params[i].split("=");
                hash[unescape(val[0])] = unescape(val[1]);
            }
            return hash;
        }

        $(document).ready(function () {

            var cookie = readCookie("wifi360-language");

            var parameters = getParameters();

            if (parameters.id != "undefined") {
                var dataString = 'action=filterprofile&id=' + parameters.id;
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",

                    success: function (pReturn) {
                        var lReturn = JSON.parse(pReturn);
                        $("#description").val(lReturn.Description);

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#modal-loading").hide();
                        $("#searchResult").html(xhr.statusText)
                    }
                });
            }

            $("#save").click(function (e) {
                e.preventDefault();

                var parameters = getParameters();
                var data = {};
                data["action"] = 'SAVEFILTERPROFILE';
                data["id"] = parameters.id;

                if (!$.isNumeric(parameters.id))
                    data["id"] = "undefined";
                data["description"] = $("#description").val();

                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    type: "POST",
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $("#myModalWait").modal('show');
                    },
                    complete: function () {
                        $("#myModalWait").modal('hide');
                    },
                    success: function (msg) {
                        if (msg.code == "OK") {
                            $("#message").removeClass().addClass("alert alert-success");
                            $("#message").html(GetMessage(msg.message));

                        }
                        else {
                            $("#message").removeClass().addClass("alert alert-danger");
                            $("#message").html(GetMessage(msg.message));
                        }
                        $("#myModalMessage").modal('show');
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#loading").html("We have a problem, please try again later.");
                        alert('error: ' + xhr.statusText);
                    }
                });
            });

            $("#back").click(function (e) {
                e.preventDefault();
                var parameters = getParameters();
                if (parameters.returnurl == undefined)
                    url = "filterprofiles.aspx";
                else
                {
                    var split = window.location.search.substring(1).split('=');
                    if (split.length > 2) {
                        if (split[0] == "returnurl") {
                            url = parameters.returnurl + "=" + split[2];
                        }
                    }
                    else                 
                        url = parameters.returnurl;
                }

                if (parameters.idsite != undefined)
                    url = "filterprofiles.aspx?&idsite=" + parameters.idsite + "&return=" + parameters.return;

                window.location.href = url;
            });

            $("#delete").click(function (e) {
                e.preventDefault();
                $("#myModaDelete").modal("show");
            });

            $("#deletebutton").click(function (e) {
                e.preventDefault();
                e.preventDefault();

                var parameters = getParameters();
                var id = parameters.id;

                var dataString = 'action=deletefilterprofile&id=' + id;
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $("#modal-loading").show()
                    },
                    complete: function () {
                        $("#modal-loading").hide()
                    },
                    success: function (pReturn) {
                        if (pReturn.code == "OK") {
                            $('#myModaDelete').modal('hide');
                            window.location.href="filterprofiles.aspx";
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#modal-loading").hide();
                        $("#searchResult").html(xhr.statusText)
                    }
                });
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
         <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 id="headerpagelocationsetting">Settings</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="default.aspx">Dashboard</a>
                </li>
                <li>
                    <a id="breadpagefatherlocationsetting">Settings</a>
                </li>
                <li>
                    <a href="filterprofiles.aspx" id="breadpagefilterprofilessetting">Filter profiles</a>
                </li>
                <li class="active">
                    <strong id="breadpagefilterprofilessettingactive">Filter profile</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-9">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                    <h5 id="labelboxheaderfilterprofilesetting">Filter profile settings<small> Details of Filter profile settings</small></h5>
                </div>
                    <div class="ibox-content">
                        <div class="form-horizontal">
                            <div class="form-group"> <label class="control-label col-sm-4" id="labelprioritysettingsformname">Description</label><div class="col-sm-8"><input type="text" class="form-control" id="description"/></div></div>
                        </div>
                    </div>
            </div>
            </div>

            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5 id="labelboxheaderacitonfilterprofilesetting">Actions over Filter profile</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <p>
                            <a href="#" class="btn btn-w-m btn-lg btn-primary btn-block" id="save"><i class="fa fa-save"></i> <span id="labelformlocationsettingsbuttonsave">Save</span> </a>
                            <a href="#" class="btn btn-w-m btn-lg btn-danger btn-block" id="delete"><i class="fa fa-trash"></i> <span id="labelformlocationsettingsbuttondelete">Delete</span> </a>

                            <a href="#" role="button" class="btn btn-w-m btn-lg btn-default btn-block" id="back" data-toggle="modal"><i class="fa fa-backward"></i> <span id="labelformlocationsettingsbuttonback">Back</span> </a>

                        </p>
                    </div>
                </div>
            </div>
                        </div>
            <div class="modal inmodal fade" id="myModalMessage" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                            </div>
                            <div class="modal-body">
                                <div id="message">
                                    
                                </div>
                            </div>
                          <div class="modal-footer">
                            <a href="#" class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Volver</a>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="modal inmodal fade" id="myModaDelete" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h3 id="deletefilterprofiletitle">Delete Filter profile</h3>
                            </div>
                            <div class="modal-body">
                                <div id="messagepriorityDelete">
                                    Are you sure to delete this item?
                                </div>
                            </div>
                          <div class="modal-footer">
                            <a href="#" class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i><span id="buttonbacklabel">Back</span> </a>
                            <a href="#" class="btn btn-danger"  aria-hidden="true" id="deletebutton"><i class="fa fa-trash"></i><span id="buttondeletelabel"> Delete</span> </a>
                          </div>
                        </div>
                    </div>
                </div>
        </div>
</asp:Content>
