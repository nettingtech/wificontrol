﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Site.Master" AutoEventWireup="true" CodeBehind="locationsettings.aspx.cs" Inherits="Wifi360.locationsettings" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
            <script type="text/javascript">
                $(document).ready(function () {

                    var page = 0;

                    function readCookie(name) {
                        var nameEQ = name + "=";
                        var ca = document.cookie.split(';');
                        for (var i = 0; i < ca.length; i++) {
                            var c = ca[i];
                            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
                        }
                        return null;
                    };

                    function getParameters() {
                        var searchString = window.location.search.substring(1)
                          , params = searchString.split("&")
                          , hash = {}
                        ;

                        for (var i = 0; i < params.length; i++) {
                            var val = params[i].split("=");
                            hash[unescape(val[0])] = unescape(val[1]);
                        }
                        return hash;
                    }

                    var parameters = getParameters();

                    var dataString = 'action=locations&page=' + page + '&idsite=' + parameters.idsite;
                    $.ajax({
                        url: "handlers/SMIHandler.ashx",
                        data: dataString,
                        contentType: "application/json; charset=utf-8",
                        dataType: "text",
                        beforeSend: function () {
                            $("#myModalWait").modal('show');
                        },
                        complete: function () {
                            $("#myModalWait").modal('hide');
                        },
                        success: function (pReturn) {
                            var lReturn = JSON.parse(pReturn);
                            var table = "";
                            var cookie = readCookie("wifi360-language");
                            if (cookie != null) {
                                switch (cookie) {
                                    case "es": table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Localización</th><th class='hidden-xs'>Descripción</th><th>Por Defecto</th><th></th></tr>"; break;
                                    default: table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Location</th><th class='hidden-xs'>Description</th><th>Default</th><th></th></tr>"; break;
                                }
                            }
                            else
                                table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Location</th><th class='hidden-xs'>Description</th><th>Default</th><th></th></tr>";
                            $.each(lReturn.list, function (index, item) {
                                table += '<tr><td>' + item["Value"] + '</td><td class="hidden-xs">' + item["Description"] + '</td><td><input type="checkbox" disabled="disabled"';
                                if (item["Default"] == true)
                                    table += 'checked="checked" ';
                                if(parameters.idsite != undefined)
                                    table += '/></td><td><a href="/location.aspx?id=' + item["Idlocation"] + '&idsite=' + parameters.idsite + '&return=' + parameters.return +'"><i class="fa fa-pencil"></i> </a></td></tr>';
                                else
                                    table += '/></td><td><a href="/location.aspx?id=' + item["Idlocation"] + '"><i class="fa fa-pencil"></i> </a></td></tr>';
                            });

                            table += "</table>";

                            $("#searchResult").html(table);

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#modal-loading").hide();
                            $("#searchResult").html(xhr.statusText)
                        }
                    });

                    //Return Control
                    if (parameters.idsite != undefined) {
                        $("#div_locations").removeClass().addClass("col-lg-9");
                        $("#div_buttons").show();
                    }
                    else {
                        $("#div_buttons").hide();
                    }

                    $("#back").click(function (e) {
                        e.preventDefault();

                        var parameters = getParameters();
                        if (parameters.idsite != undefined)
                        {
                            var url = parameters.return + "?id=" + parameters.idsite;
                            window.location.href = url;
                        }
                    });
                });

                </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 id="headerpagelocationsettings">Settings</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="default.aspx">Dashboard</a>
                </li>
                <li id="breadpagefatherlocationsettings">
                    Settings
                </li>
                <li class="active">
                    <strong id="breadpagelocationsettings">Locations</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div id="div_locations" class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5 id="labelboxheaderlocationsettings">Locations<small> Manage locations</small></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <a href="location.aspx" id="new" class="btn btn-primary" style="display:none;"><i class="fa fa-plus"></i> Nueva</a>
                            </div>
                        
                        <div class="col-lg-12">
                            <div id="searchResult"></div>
                        </div></div>
                    </div>
                </div>
            </div>
            <div id="div_buttons" class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5 id="labelboxheaderbuttonsettings">Actions over locations</h5>
                        <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        <i class="fa fa-wrench"></i>
                                    </a>
                                </div>
                    </div>
                    <div class="ibox-content">
                        <p>
                                <a href="#" role="button" class="btn btn-w-m btn-lg btn-default btn-block" id="back"><i class="fa fa-backward"></i> <span id="labelsettingsbuttonback">Back </span></a>
                            </p>                           
                    </div>
                </div>
            </div>
     </div>
                    
    
    <div class="modal inmodal fade" id="myModalMessage" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                </div>
                <div class="modal-body">
                    <div id="message"></div>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Volver</a>
                </div>
             </div>
         </div>
     </div>

</asp:Content>
