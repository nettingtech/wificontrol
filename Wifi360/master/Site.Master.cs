﻿using Wifi360.Data.Controllers;
using Wifi360.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;
using Newtonsoft.Json;
using System.Xml;
using Wifi360.response;
using Wifi360.Data.Class;

namespace Wifi360.master
{
    public partial class Site : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {

                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    labelUser.Text = string.Format("{0}", fdsUser.Name);

                    Locations2 location = null;
                    Hotels hotel = null;
                    FDSGroups group = null;
                    string lat = string.Empty;
                    string lng = string.Empty;
                    SitesGMT siteGMT = null;
                    //El usuario es de tipo Location
                    if (HttpContext.Current.Request.Cookies["location"] != null)
                    {
                        location = BillingController.GetLocation(Int32.Parse(HttpContext.Current.Request.Cookies["location"].Value));
                        hotel = BillingController.GetHotel(location.IdHotel);
                        imagenLogo.ImageUrl = string.Format("/resources/images/{0}/{1}-logo.png", location.IdHotel, location.IdLocation);
                        labelHotel.Text = location.Description;
                        lat = location.Latitude;
                        lng = location.Longitude;
                        siteGMT = BillingController.SiteGMTSitebyLocation(location.IdLocation);
                    }
                    //El usuario es de tipo Site
                    else if (HttpContext.Current.Request.Cookies["site"] != null)
                    {
                        hotel = BillingController.GetHotel(Int32.Parse(HttpContext.Current.Request.Cookies["site"].Value));
                        imagenLogo.ImageUrl = string.Format("/resources/images/{0}/logo.png", hotel.IdHotel);
                        labelHotel.Text = hotel.Name;
                        lat = hotel.Latitude;
                        lng = hotel.Longitude;
                        siteGMT = BillingController.SiteGMTSitebySite(hotel.IdHotel);
                    }//El usuario es de tipo group
                    else
                    {
                        group = BillingController.GetGroup(Int32.Parse(HttpContext.Current.Request.Cookies["group"].Value));
                        imagenLogo.ImageUrl = string.Format("/resources/images/groups/{0}/logo.png", group.IdGroup);
                        labelHotel.Text = group.Name;
                        lat = group.Latitude;
                        lng = group.Longitude;
                        siteGMT = BillingController.SiteGMTSitebyGroup(group.IdGroup);
                    }

                    if (siteGMT.Id.Equals(0) || DateTime.Now.ToUniversalTime().Subtract(siteGMT.lastupdate).TotalHours >= 6)
                    {
                        HttpClient client = new HttpClient();
                        string _address = "http://api.timezonedb.com/v2.1/get-time-zone?key=YSFN7EK1ONKS&format=xml&by=position&lat=" + lat + "&lng=" + lng;
                        var response = client.GetAsync(_address).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            // Modify as per your requirement    
                            string responseString = response.Content.ReadAsStringAsync().Result;
                            // Your code goes here      
                            XmlDocument doc = new XmlDocument();
                            doc.LoadXml(responseString);

                            XmlNodeList xmlnode = doc.GetElementsByTagName("gmtOffset");

                            XmlNodeList xmlnodeDST = doc.GetElementsByTagName("dst");

                            siteGMT.gmtoffset = Int32.Parse(xmlnode[0].ChildNodes.Item(0).InnerText);
                            siteGMT.dst = Int32.Parse(xmlnodeDST[0].ChildNodes.Item(0).InnerText);
                            siteGMT.IdGroup = group != null ? group.IdGroup : 0;
                            siteGMT.IdSite = hotel != null ? hotel.IdHotel : 0;
                            siteGMT.IdLocation = location != null ? location.IdLocation : 0;
                            siteGMT.lastupdate = DateTime.Now.ToUniversalTime();

                            BillingController.SaveSiteGMT(siteGMT);
                        }
                    }

                    selectMembers();

                    
                }
                else 
                    Response.Redirect("login.aspx");
            }
        }

        List<FDSMemberGroupInfo> membersInfo = null;
        private void selectMembers()
        {
            ListMemberResult response = new ListMemberResult();
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    response.list = new List<MemberResult>();

                    if (HttpContext.Current.Request.Cookies["group"] != null)
                    {
                        FDSGroups groupParent = BillingController.GetGroup(int.Parse(HttpContext.Current.Request.Cookies["parent"].Value));
                        FDSGroups group = BillingController.GetGroup(int.Parse(HttpContext.Current.Request.Cookies["group"].Value));
                        List<FDSGroupLevel> levels = new List<FDSGroupLevel>();
                        FDSGroupLevel rootLevel = new FDSGroupLevel();
                        rootLevel.idGroup = groupParent.IdGroup;
                        rootLevel.level = 0;
                        levels.Add(rootLevel);

                        List<FDSGroupsMembers> members = new List<FDSGroupsMembers>();
                        membersInfo = new List<FDSMemberGroupInfo>();
                        FDSGroupsMembers root = new FDSGroupsMembers();
                        root.IdGroup = groupParent.IdGroup;
                        FDSMemberGroupInfo rootInfo = new FDSMemberGroupInfo();
                        Wifi360.Data.Common.Util.Map(root, rootInfo);
                        rootInfo.Name = groupParent.Name;
                        rootInfo.IdMember = groupParent.IdGroup;
                        rootInfo.IdMemberType = 1;
                        members.Add(root);
                     //   membersInfo.Add(rootInfo);
                        List<FDSGroupsMembers> membersRoot = BillingController.SelectFDSGroupsMembers(groupParent.IdGroup);
                        int level = 1;
                        int idlevel = groupParent.IdGroup;
                        for (int i = 0; i < membersRoot.Count; i++)
                        {
                            FDSGroupsMembers child = new FDSGroupsMembers();
                            child.IdGroup = membersRoot[i].IdGroup;
                            child.IdMember = membersRoot[i].IdMember;
                            child.IdMemberType = membersRoot[i].IdMemberType;
                            child.Id = membersRoot[i].Id;
                            FDSMemberGroupInfo childInfo = new FDSMemberGroupInfo();
                            Wifi360.Data.Common.Util.Map(child, childInfo);
                            members.Add(child);

                            switch (membersRoot[i].IdMemberType)
                            {
                                //location:
                                case 1:
                                    childInfo.Name = BillingController.GetGroup(child.IdMember).Name;
                                    List<FDSGroupsMembers> listchildgroup = BillingController.SelectFDSGroupsMembers(membersRoot[i].IdMember);
                                    FDSGroupLevel auxLevel = new FDSGroupLevel();
                                    FDSGroupLevel tempLevel = (from t in levels where t.idGroup.Equals(child.IdGroup) select t).FirstOrDefault();
                                    if (tempLevel != null)
                                        auxLevel.level = tempLevel.level + 1;
                                    else
                                    {
                                        auxLevel.level = level;
                                    }
                                    auxLevel.idGroup = child.IdMember;
                                    levels.Add(auxLevel);
                                    foreach (FDSGroupsMembers aux in listchildgroup)
                                    {
                                        membersRoot.Add(aux);
                                    }

                                    break;
                                case 2:
                                    childInfo.Name = BillingController.GetHotel(child.IdMember).Name; break;
                                case 3:
                                    childInfo.Name = BillingController.GetLocation(child.IdMember).Description; break;
                            }

                            membersInfo.Add(childInfo);
                        }

                        literalmenuChange.Text = CreateMenuParent(rootInfo);

                        //for (int i = 0; i < membersInfo.Count(); i++)
                        //{
                        //    FDSMemberGroupInfo m = membersInfo[i];
                        //    MemberResult rs = new MemberResult();
                        //    rs.Id = m.IdGroup;
                        //    rs.IdMembertype = "1";
                        //    rs.Name = groupParent.Name;
                        //    response.list.Add(rs);
                        //    membersInfo.Remove(m);
                        //    List<FDSMemberGroupInfo> mgs = (from t in membersInfo where t.IdGroup.Equals(m.IdGroup) && !t.IdMember.Equals(0) select t).ToList();

                        //    for (int j = 0; j < mgs.Count(); j++)
                        //    {
                        //        MemberResult mresult = new MemberResult();

                        //        FDSMemberGroupInfo aux = mgs[j];
                        //        mresult.Id = aux.IdMember;
                        //        mresult.IdMembertype = aux.IdMemberType.ToString();
                        //        mresult.Name = aux.IdMember.ToString();
                        //        mresult.IdParent = aux.IdGroup;

                        //        FDSGroupLevel levelInfo = (from t in levels where t.idGroup.Equals(aux.IdGroup) select t).FirstOrDefault();
                        //        switch (aux.IdMemberType)
                        //        {
                        //            case 1: mresult.Name = string.Format("{0}", aux.Name); break;
                        //            case 2: mresult.Name = string.Format("{0}", aux.Name); break;
                        //            case 3: mresult.Name = string.Format("{0}", aux.Name); break;
                        //        }

                        //        response.list.Add(mresult);
                        //        membersInfo.Remove(aux);
                        //        FDSMemberGroupInfo auxDelete = (from t in membersInfo where t.Id.Equals(aux.Id) select t).FirstOrDefault();
                        //        membersInfo.Remove(auxDelete);
                        //        if (aux.IdMemberType.Equals(1))
                        //        {
                        //            int k = 1;
                        //            List<FDSGroupsMembers> childs = BillingController.SelectFDSGroupsMembers(aux.IdMember);
                        //            foreach (FDSGroupsMembers childAux in childs)
                        //            {
                        //                FDSMemberGroupInfo childAuxInfo = new FDSMemberGroupInfo();
                        //                Wifi360.Data.Common.Util.Map(childAux, childAuxInfo);
                        //                switch (childAux.IdMemberType)
                        //                {
                        //                    case 1:
                        //                        childAuxInfo.Name = BillingController.GetGroup(childAux.IdMember).Name;
                        //                        break;
                        //                    case 2:
                        //                        childAuxInfo.Name = BillingController.GetHotel(childAux.IdMember).Name; break;
                        //                    case 3:
                        //                        childAuxInfo.Name = BillingController.GetLocation(childAux.IdMember).Description; break;
                        //                }
                        //                mgs.Insert(j + k, childAuxInfo);
                        //                k++;
                        //            }
                        //        }

                        //    }
                        //}

                    }
                    else if (HttpContext.Current.Request.Cookies["site"] != null)
                    {
                        Hotels hotel = BillingController.GetHotel(Int32.Parse(HttpContext.Current.Request.Cookies["site"].Value.ToString()));
                        List<FDSLocationsIdentifier> locations = BillingController.GetFDSLocationsIdentifiers(hotel.IdHotel);
                        if (locations.Count > 0)
                        {
                            MemberResult r = new MemberResult();
                            r.IdMembertype = "2";
                            r.Name = string.Format("{0}", hotel.Name);
                            r.Id = hotel.IdHotel;
                            response.list.Add(r);
                            foreach (FDSLocationsIdentifier identifier in locations.OrderBy(a => a.Identifier))
                            {
                                Locations2 l = BillingController.GetLocation(identifier.IdLocation);
                                MemberResult rl = new MemberResult();
                                rl.IdMembertype = "3";
                                rl.Name = string.Format("{0}", l.Description);
                                rl.Id = identifier.IdLocation;

                                response.list.Add(rl);
                            }
                        }

                        literalmenuChange.Text = "<a href=\"#\"  rel=\"site\" idmember=\"" + response.list[0].Id + "\" type=\"" + response.list[0].IdMembertype + "\" ><i class=\"fa fa-sitemap\"></i> <span class=\"nav-label\" >" + response.list[0].Name + "</span> <span class=\"caret\"></span></a>";
                        literalmenuChange.Text += "<ul class=\"dropdown-menu\">";
                        for (int i = 1; i < response.list.Count; i++)
                        {
                            literalmenuChange.Text += "<li><a rel=\"site\" idmember=\"" + response.list[i].Id + "\" type=\"" + response.list[i].IdMembertype + "\" href=\"#\">" + response.list[i].Name;
                            if (response.list[i].IdMembertype.Equals("1"))
                            {
                                literalmenuChange.Text += " <span class=\"caret\"></a>";
                                literalmenuChange.Text += "<ul class=\"dropdown-menu\">";
                                int idparent = response.list[i].Id;
                                for (int j = i + 1; j < response.list.Count; j++)
                                {
                                    if (idparent.Equals(response.list[j].IdParent))
                                    {
                                        literalmenuChange.Text += "<li><a rel=\"site\" idmember=\"" + response.list[j].Id + "\" type=\"" + response.list[j].IdMembertype + "\" href=\"#\">" + response.list[j].Name;
                                        if (response.list[j].IdMembertype.Equals("1"))
                                        {
                                            literalmenuChange.Text += " <span class=\"caret\"></a>";
                                            literalmenuChange.Text += "<ul class=\"dropdown-menu\">";
                                            int idparent2 = response.list[j].Id;
                                            for (int k = j + 1; k < response.list.Count; k++)
                                            {
                                                if (idparent2.Equals(response.list[k].IdParent))
                                                {
                                                    literalmenuChange.Text += "<li><a rel=\"site\" idmember=\"" + response.list[k].Id + "\" type=\"" + response.list[k].IdMembertype + "\" href=\"#\">" + response.list[k].Name + "</a>";
                                                    if (k + 1 == response.list.Count)
                                                        j = k;
                                                }
                                                else
                                                {
                                                    j = k - 1;
                                                    break;
                                                }

                                            }
                                            literalmenuChange.Text += "</ul>";
                                        }
                                        else
                                            literalmenuChange.Text += " </a>";


                                        literalmenuChange.Text += "</li>";
                                        if (j + 1 == response.list.Count)
                                            i = j;
                                    }
                                    else
                                    {
                                        i = j - 1;
                                        break;
                                    }
                                }
                                literalmenuChange.Text += "</ul>";
                            }
                            else
                            {
                                literalmenuChange.Text += "</a>";
                            }
                            literalmenuChange.Text += "</li>";
                        }
                        literalmenuChange.Text += "</ul>";
                    }

                }

                

            }
            catch (Exception ex)
            {
            }
        }


        private string CreateMenuParent(FDSMemberGroupInfo root)
        {
            string menu_text = "<a href=\"#\"  rel=\"site\" idmember=\"" + root.IdMember + "\" type=\"" + root.IdMemberType + "\" ><i class=\"fa fa-sitemap\"></i> <span class=\"nav-label\" >" + root.Name + "</span> <span class=\"caret\"></span></a>";
            menu_text += "<ul class=\"dropdown-menu\">";
            List<FDSMemberGroupInfo> childs = (from t in membersInfo where t.IdGroup.Equals(root.IdMember) select t).ToList();
            foreach(FDSMemberGroupInfo child in childs.OrderBy(a => a.Name).OrderBy(a => a.IdMemberType))
                menu_text += CreateChilds(child);
            menu_text += "</ul>";

            return menu_text;
        }

        private string CreateChilds(FDSMemberGroupInfo info)
        {
            string menu_text = "<li><a rel=\"site\" idmember=\"" + info.IdMember + "\" type=\"" + info.IdMemberType + "\" href=\"#\">" + info.Name;
            if (info.IdMemberType.Equals(1))
            {
                List<FDSMemberGroupInfo> childs = (from t in membersInfo where t.IdGroup.Equals(info.IdMember) select t).ToList();

                menu_text += "<span class=\"caret\"></a>";
                menu_text += "<ul class=\"dropdown-menu\">";

                foreach (FDSMemberGroupInfo child in childs.OrderBy(a => a.Name).OrderBy(a => a.IdMemberType))
                {
                    if (child.IdMemberType.Equals(1))
                    {
                        menu_text += CreateChilds(child);
                    }
                    else
                        menu_text += "<li><a rel=\"site\" idmember=\"" + child.IdMember + "\" type=\"" + child.IdMemberType + "\" href=\"#\">" + child.Name + "</a></li>";
                }
                menu_text += "</ul>";
            }
            menu_text += "</li>";

            menu_text += "</a></li>";
            return menu_text;

        }
    }
}