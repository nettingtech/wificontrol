﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Site.Master" AutoEventWireup="true" CodeBehind="ManageDashboardUsers.aspx.cs" Inherits="Wifi360.ManageDashboardUsers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

        <script type="text/javascript">

            function readCookie(name) {
                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
                }
                return null;
            };

            //Funciones de preCarga
            $(document).ready(function () {

                //Default de variable PAGE
                var page = 0;
                var name = "";
                var cookie = readCookie("wifi360-language");

                if( $("#management_name").val() != null){
                    name = $("#management_name").val();
                }                

                var dataString = 'action=FDSALLUSERS&page=' + page + '&username=' + name;
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    beforeSend: function () {
                        $("#myModalWait").modal('show');
                    },
                    complete: function () {
                        $("#myModalWait").modal('hide');
                    },

                    success: function (pReturn) {
                        var lReturn = JSON.parse(pReturn);
                        var table = "";
                        switch (cookie) {
                            case "es": table = "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>ID</th><th class='hidden-xs'>Nombre de usuario</th><th>Rol</th><th>Tipo</th><th>Editar</th></tr>";
                                break;
                            default: table = "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>ID</th><th class='hidden-xs'>Username</th><th>Rol</th><th>Type</th><th>Edit</th></tr>";
                                break;
                        }

                        $.each(lReturn.list, function (index, item) {
                            table += '<tr><td>' + item["idFDSUser"] + '</td><td class="hidden-xs">' + item["UserName"] + '</td><td class="hidden-xs">' + item["rol"] + '</td><td class="hidden-xs">' + item["type"] + '</td>';
                            table += '<td><a href="/ManageDashboardUserDetails.aspx?id=' + item["idFDSUser"] + '"><i class="fa fa-pencil"></i></a></td>';
                            table += '</tr>';
                        });
                        table += "</table>";
                        $("#searchResult").html(table);

                        //Código para la paginación
                        var pagination = "<ul class='pagination'>";
                        var points1 = 0;
                        var points2 = 0;
                        x = parseInt(page);
                        for (var index = 0; index < lReturn.PageNumber; index++) {
                            if (lReturn.PageNumber <= 10) {
                                if (page == index)
                                    pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                else
                                    pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                            }
                            else {
                                if (page == index) {
                                    pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                                else if (index == 0) {
                                    pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                                else if (index == (lReturn.PageNumber - 1)) {
                                    pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                                else if (index < (x - 3)) {
                                    if (points1 == 0) {
                                        pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                        points1 = 1;
                                    }
                                }
                                else if (index > (x + 3)) {
                                    if (points2 == 0) {
                                        pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                        points2 = 1;
                                    }
                                }
                                else
                                    pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                            }
                        }
                        pagination += "</ul>";
                        $("#searchResult").append(pagination);
                        //Fin Código para la paginación
                    },
                    completed: function () {
                        $("#myModalWait").modal('hide');
                    }
                });
                

                function save_dashboard_user() {
                    //NO RULES!!!
                    var data = {};
                    //tipo de acción
                    data["action"] = 'SAVE_DASHBOARD_USER';
                    data["Name"] = $("#dashboard_name").val();
  
                    //Llamada AJAX para carga de datos.
                    $.ajax({
                        url: "handlers/SMIHandler.ashx",
                        type: "POST",
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "text",
                        success: function (msg) {
                            if (msg.code == "OK") {
                                $("#message").removeClass().addClass("alert alert-success");
                                $("#message").html(GetMessage(msg.message));

                            }
                            else {
                                $("#message").removeClass().addClass("alert alert-danger");
                                $("#message").html(GetMessage(msg.message));
                            }
                            $("#myModalMe>ssage").modal('show');
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#loading").html("We have a problem, please try again later.");
                            alert('error: ' + xhr.statusText);
                        }
                    });
                }

                $("#save_dashboard").click(function (e) {
                    e.preventDefault();
                    save_dashboard_user();
                });

                //Función de búsqueda
                $("#search").click(function (e) {
                    e.preventDefault();

                    var page = 0;
                    var name = "";
                    var cookie = readCookie("wifi360-language");

                    if ($("#management_name").val() != null) {
                        name = $("#management_name").val();
                    }
                    var dataString_01 = 'action=FDSALLUSERS&page=' + page + '&username=' + name;
                    $.ajax({
                        url: "handlers/SMIHandler.ashx",
                        data: dataString_01,
                        contentType: "application/json; charset=utf-8",
                        dataType: "text",
                        beforeSend: function () {
                            $("#myModalWait").modal('show');
                        },
                        complete: function () {
                            $("#myModalWait").modal('hide');
                        },
                        success: function (pReturn_01) {
                            var lReturn_01 = JSON.parse(pReturn_01);
                            var table = "";
                            switch (cookie) {
                                case "es": table = "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>ID</th><th class='hidden-xs'>Nombre de usuario</th><th>Rol</th><th>Tipo</th><th>Editar</th></tr>";
                                    break;
                                default: table = "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>ID</th><th class='hidden-xs'>Username</th><th>Rol</th><th>Type</th><th>Edit</th></tr>";
                                    break;
                            }

                            $.each(lReturn_01.list, function (index, item) {
                                table += '<tr><td>' + item["idFDSUser"] + '</td><td class="hidden-xs">' + item["UserName"] + '</td><td class="hidden-xs">' + item["rol"] + '</td><td class="hidden-xs">' + item["type"] + '</td>';
                                table += '<td><a href="/ManageDashboardUserDetails.aspx?id=' + item["idFDSUser"] + '"><i class="fa fa-pencil"></i></a></td>';
                                table += '</tr>';
                            });

                            if (lReturn_01.isAdmin) {
                                $("#new").show();
                            }

                            table += "</table>";
                            $("#searchResult").html(table);

                            //Código para la paginación
                            var pagination = "<ul class='pagination'>";
                            var points1 = 0;
                            var points2 = 0;
                            x = parseInt(page);
                            for (var index = 0; index < lReturn_01.PageNumber; index++) {
                                if (lReturn_01.PageNumber <= 10) {
                                    if (page == index)
                                        pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                    else
                                        pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                                else {
                                    if (page == index) {
                                        pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                    }
                                    else if (index == 0) {
                                        pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                    }
                                    else if (index == (lReturn_01.PageNumber - 1)) {
                                        pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                    }
                                    else if (index < (x - 3)) {
                                        if (points1 == 0) {
                                            pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                            points1 = 1;
                                        }
                                    }
                                    else if (index > (x + 3)) {
                                        if (points2 == 0) {
                                            pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                            points2 = 1;
                                        }
                                    }
                                    else
                                        pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                            }
                            pagination += "</ul>";
                            $("#searchResult").append(pagination);
                            //Fin Código para la paginación
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#modal-loading").hide();
                            $("#searchResult").html(xhr.statusText)
                        }

                    });
                });
            });

            //Evento para Función para el paginado
            $(document).on("click", 'a[rel^="page"]', function (e) {
                e.preventDefault();
                var cookie = readCookie("wifi360-language");
                var page = $(this).attr("attr");
                var name = "";

                if ($("#management_name").val() != null) {
                    name = $("#management_name").val();
                }

                var dataString = 'action=FDSALLUSERS&page=' + page + '&username=' + name;
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    beforeSend: function () {
                        $("#myModalWait").modal('show');
                    },
                    complete: function () {
                        $("#myModalWait").modal('hide');
                    },
                    success: function (pReturn) {
                        var lReturn = JSON.parse(pReturn);
                        var table = "";
                        switch (cookie) {
                            case "es": table = "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>ID</th><th class='hidden-xs'>Nombre de usuario</th><th>Rol</th><th>Tipo</th><th>Editar</th></tr>";
                                break;
                            default: table = "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>ID</th><th class='hidden-xs'>Username</th><th>Rol</th><th>Type</th><th>Edit</th></tr>";
                                break;
                        }

                        $.each(lReturn.list, function (index, item) {
                            table += '<tr><td>' + item["idFDSUser"] + '</td><td class="hidden-xs">' + item["UserName"] + '</td><td class="hidden-xs">' + item["rol"] + '</td><td class="hidden-xs">' + item["type"] + '</td>';
                            table += '<td><a href="/ManageDashboardUserDetails.aspx?id=' + item["idFDSUser"] + '"><i class="fa fa-pencil"></i></a></td>';
                            table += '</tr>';
                        });

                        if (lReturn.isAdmin) {
                            $("#new").show();
                        }

                        table += "</table>";

                        //Código para la paginación
                        $("#searchResult").html(table);
                        var pagination = "<ul class='pagination'>";
                        var points1 = 0;
                        var points2 = 0;
                        x = parseInt(page);
                        for (var index = 0; index < lReturn.PageNumber; index++) {
                            if (lReturn.PageNumber <= 10) {
                                if (page == index)
                                    pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                else
                                    pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                            }
                            else {
                                if (page == index) {
                                    pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                                else if (index == 0) {
                                    pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                                else if (index == (lReturn.PageNumber - 1)) {
                                    pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                                else if (index < (x - 3)) {
                                    if (points1 == 0) {
                                        pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                        points1 = 1;
                                    }
                                }
                                else if (index > (x + 3)) {
                                    if (points2 == 0) {
                                        pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                        points2 = 1;
                                    }
                                }
                                else
                                    pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                            }
                        }
                        pagination += "</ul>";
                        $("#searchResult").append(pagination);
                        //Fin Código para la paginación
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#modal-loading").hide();
                        $("#searchResult").html(xhr.statusText)
                    }
                });
            });
           
                        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       
    
      <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 id="headerpagemanageusers">Management Dashboard Users</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="default.aspx">Dashboard</a>
                </li>
                <li id="breadpagefathersuperadmin">
                    <a>Super Admin</a>
                </li>
                <li class="active">
                    <strong id="breadlabelmanageusers">Management Users</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5 id="labelboxheadermanagementusers"> Management Users</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <a href="ManageDashboardUserDetails.aspx" id="NewUser" class="btn btn-primary"><i class="fa fa-plus"></i><span id="ButtonNewUser"> New User</span></a>
                        </div>
                        <div class="col-lg-12">
                        <div class="ibox border-bottom">
                        <div class="ibox-title">
                        <h5 id="headerboxfilterusersvalid">Search filters</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-down"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content" style="display: none;">
                        <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto;">
                            <div class="scroll_content" style="overflow: hidden; width: auto; ">
                                <div role="form">
                            <div class="form-group"><label for="username" id="labelform_management_description">Name</label>
                                <input type="text" id="management_name" class="form-control"/>
                            </div>

                            <div class="form-group">
                                <a href="#" id="search" class="btn btn-w-m btn-primary" ><i class="fa fa-search"></i><span id="labelformusermanagementbuttonsearch"> Search </span></a>
                            </div>

                        </div>
                        </div><div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 198.02px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                    </div>
                </div>
                        </div>
                        <div class="col-lg-12">
                            <div id="searchResult"></div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        
            <!-- Modal para la clonación de SITES -->
            <div class="modal inmodal fade" id="CloneSite_ModalMessage" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header"><h4>Opciones de Clonado</h4></div>
                        <div class="modal-body">
                            <!--<div id="CloneSite_message"></div>-->
                            <div class="form-group"> <label class="control-label col-sm-3" id="labelnewsite_form_name">Name's New Site</label>
                                <div class="col-sm-9"><input type="text" class="form-control" id="name" /></div>
                           </div>
                            <div class="form-group"><br/></div>
                            <div class="form-group"> <label class="control-label col-sm-3" id="labelnewsite_form_billingtypes">I want clone Billing types</label>
                                <div class="col-sm-2"><div class="onoffswitch">
                                        <input type="checkbox" class="onoffswitch-checkbox" id="billingtypes"/>
                                        <label class="onoffswitch-label" for="billingtypes">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                </div></div>
                            </div>
                        </div>
                       <div class="modal-footer">
                           <a href="#" class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Volver</a>
                           <a href="#" class="btn btn-primary" id="clone"><i class="fa fa-copy"></i> Clonar</a>
                       </div>
                    </div>
                </div>
            </div>
                
        
            <div class="modal inmodal fade" id="ModalMessage" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header"></div>
                        <div class="modal-body">
                            <div id="message"></div>
                        </div>
                       <div class="modal-footer">
                           <a href="#" class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Volver</a>
                       </div>
                    </div>
                </div>
            </div>
        </div>

</asp:Content>
