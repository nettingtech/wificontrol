﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Threading.Tasks;
using System.Net;

using Splunk.Client;
using System.Data;

namespace Wifi360
{
    public partial class BigData : System.Web.UI.Page
    {

        protected static string cpu_temp = string.Empty;
        protected static string uptime = string.Empty;
        protected static string console = string.Empty;
        protected static string total = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            //try
            //{
            //    ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            //    ServicePointManager.ServerCertificateValidationCallback += (sender1, certificate, chain, sslPolicyErrors) =>
            //    {
            //        return true;
            //    };

            //    SplunkConnect().Wait();

            //    ;

            //}
            //catch(Exception ex)
            //{
            //    throw ex;
            //    ;
            //}
        }

        private static async Task SplunkConnect ()
        {
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                var service = new Service(Scheme.Https, "splunk.future-broadband.com", 8089);

                await service.LogOnAsync("jarrocha@ilos.es", "wBo2pOZ4-HMC_7-Sh-KSr");

                // query que te da la temperatura del mikrotik
                var query = "search index=fbd source=\"snmp://snmp_Mikrotik\" |  head 1 | eval cpu_temp=cpu_temp/10 | table cpu_temp";
                var args = new JobArgs
                {

                    EarliestTime = "-1w",
                    LatestTime = "now"
                };

                // visualización de los datos
                using (SearchResultStream resultStream = await service.SearchOneShotAsync(query, args: args))
                {
                    foreach (SearchResult result in resultStream)
                    {
                        cpu_temp = "CPU Temp:" + result.GetValue("cpu_temp");
                    }
                };

                var query2 = "search index=fbd source=\"snmp://snmp_Mikrotik\" | head 1 | eval uptime = uptime / 100 / 60 / 60 / 24 | eval mDias = floor(uptime)  | eval uptime = ((uptime - mDias) * 24) | eval mHoras = floor(uptime) | eval uptime = ((uptime - mHoras) * 60) | eval mMinutos = floor(uptime) | eval uptime = ((uptime - mMinutos) * 60) | eval mSegundos = floor(uptime) | eval uptime = printf(\"%dd %dh %dm %ds\", mDias, mHoras, mMinutos, mSegundos) | table  uptime";
                var args2 = new JobArgs
                {

                    EarliestTime = "-1m@",
                    LatestTime = "now"
                };

                // visualización de los datos
                using (SearchResultStream resultStream = await service.SearchOneShotAsync(query2, args: args2))
                {
                    foreach (SearchResult result in resultStream)
                    {
                        uptime = "Uptime:" + result.GetValue("uptime");
                    }
                };

                var query3 = "search index=fbd source=\"snmp://snmp_controladora_Ruckus\" | timechart span = 1m max(total_clients)";
                var args3 = new JobArgs
                {

                    EarliestTime = "-30m@",
                    LatestTime = "now"
                };

                // visualización de los datos
                using (SearchResultStream resultStream = await service.SearchOneShotAsync(query3, args: args3))
                {
                    foreach (SearchResult result in resultStream)
                    {
                        total += "Total clientes:" + result.GetValue("uptime") + " ms";
                    }
                };

                var query4 = "search index = \"fbd\" sourcetype = \"response_time\" | head 1 | eval time_total = time_total * 1000 | table time_total";
                var args4 = new JobArgs
                {

                    EarliestTime = "-30m@",
                    LatestTime = "now"
                };

                // visualización de los datos
                using (SearchResultStream resultStream = await service.SearchOneShotAsync(query4, args: args4))
                {
                    foreach (SearchResult result in resultStream)
                    {
                        console += "Response time:" + result.GetValue("time_total");
                    }
                };

                //var query5 = "search index=fbd source=SessionsLog  earliest=\"08/02/2018:00:00:00\" latest=\"08/20/2018:00:00:00\" IdHotel=67 ";
                //query5 +="| chart  values(Count) as Usuarios by _time";

                var query5 = "search index = fbd source = SessionsLog earliest =\"08/02/2018:00:00:00\" latest=\"08/20/2018:00:00:00\"   IdHotel=67 | chart  values(Count) as Usuarios by _time";

                var args5 = new JobArgs { };

                // visualización de los datos
                using (
                    SearchResultStream resultStream = await service.SearchOneShotAsync(query5, args: args5))
                {
                    foreach (SearchResult result in resultStream)
                    {
                        // console += "ActiveSessions: " + result.GetValue("Usuarios");
                        ;
                    }
                };

                var query6 =
                      "search index=fbd source=\"snmp://snmp_Mikrotik\" "
                      + "| eval cpu_temp = cpu_temp / 10 "
                      + "| eval equipo_temp = equipo_temp / 10 "
                      + "| timechart span=1m max(cpu_temp) as \"CPU_temp\",max(equipo_temp) as \"Mikrotik_temp\",max(cpu_usage) as \"CPU_usage\"";

                var args6 = new JobArgs
                {

                    EarliestTime = "-60m@m",
                    LatestTime = "now"
                };
                DataTable table = new DataTable();
                table.Columns.Add("Time");
                table.Columns.Add("CPU_temp");
                table.Columns.Add("Mikrotik_temp");
                table.Columns.Add("CPU_usage");
                // visualización de los datos
                using (SearchResultStream resultStream = await service.SearchOneShotAsync(query6, args: args6))
                {
                    foreach (SearchResult result in resultStream)
                    {
                        DataRow row = table.NewRow();
            //            console += result + Environment.NewLine;

                        row["Time"] = result.GetValue("_time");
                        row["CPU_temp"] = result.GetValue("CPU_temp");
                        row["Mikrotik_temp"] = result.GetValue("Mikrotik_temp");
                        row["CPU_usage"] = result.GetValue("CPU_usage");

                        table.Rows.Add(row);
                    }
                }

                var query7 = "| dbxquery query = ";
                query7 += "\"select * from \"radius_produccion\".\"dbo\".\"ActiveSessions\" WHERE IdHotel = 67\"";
                query7 += "connection=\"fb_connection\"";
                query7 += "stats count";
               

                var args7 = new JobArgs {};

                // visualización de los datos
                using (SearchResultStream resultStream = await service.SearchOneShotAsync(query7, args: args7))
                {
                    foreach (SearchResult result in resultStream)
                    {
                        console += "ActiveSessions: " + result.GetValue("Usuarios");
                    }
                };

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
    }
}