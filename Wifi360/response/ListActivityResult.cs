﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class ListActivityResult
    {
        public List<ActivityResult> list;
        public int pageNumber;
    }
}