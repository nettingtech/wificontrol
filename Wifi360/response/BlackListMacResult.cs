﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class BlackListMacResult
    {
        public string IdBackListMac { get; set; }
        public string Mac { get; set; }
        public string BanningStart { get; set; }
        public string BanningEnd { get; set; }
        public string IdHotel { get; set; }
        public string lastUser { get; set; }
        public string idUser { get; set; }
    }
}