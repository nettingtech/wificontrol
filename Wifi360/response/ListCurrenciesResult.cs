﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class ListCurrenciesResult
    {
        public List<CurrencyResult> list;
        public int pageNumber;
    }
}