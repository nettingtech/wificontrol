﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class BillingTypeResult
    {
        public int IdBillingType { get; set; }
        public string Description { get; set; }
    }
}