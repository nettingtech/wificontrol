﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class LanguageResult
    {
        public int IdLanguage { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}