﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class VendorsResult
    {
        public int IdVendor { get; set; }
        public string Name { get; set; }
    }
}