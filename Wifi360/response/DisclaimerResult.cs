﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class DisclaimerResult
    {
        public int iddisclaimer { get; set; }
        public int idhotel { get; set; }
        public int idlocation { get; set; }
        public int idlanguage { get; set; }
        public string language { get; set; }
        public string text { get; set; }
        public string date { get; set; }
        public int type { get; set; }
        public string typeDescription { get; set; }
    }
}