﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class LoyaltyResult
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Date { get; set; }
        public string Survey { get; set; }
    }
}