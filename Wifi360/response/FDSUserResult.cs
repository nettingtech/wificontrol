﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class FDSUserResult
    {
        public int idFDSUser{ get; set; }
        public int IdRol { get; set; }
        public int IdType { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string rol { get; set; }
        public string type { get; set; }

        public int idmember  { get; set; }
        public string member { get; set; }

    }
}