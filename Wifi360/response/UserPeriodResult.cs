﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class UserPeriodResult
    {
        public int IdUser { get; set; }
        public string Room { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string AccessType { get; set; }
        public string ValidSince { get; set; }
        public string ValidTill { get; set; }
        public string TimeCredit { get; set; }
    }
}