﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class ActivityResult
    {
        public string User { get; set; }
        public string Start { get; set; }
        public string Finish { get; set; }
        public string BytesIn { get; set; }
        public string BytesOut { get; set; }
        public string MAC { get; set; }
    }
}