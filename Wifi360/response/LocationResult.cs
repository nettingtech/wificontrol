﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class LocationResult
    {
        public int Idlocation { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public string UrlLanding { get; set; }
        public string deviceid { get; set; }
        public int DefaultModule { get; set; }
        public bool DisclaimerMandatory { get; set; }
        public bool Default { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public double CurrencyExchange { get; set; }
        public int IdCurrency { get; set; }
        public string Currency { get; set; }
        public string CurrencySite { get; set; }
        public string Currencysymbol { get; set; }
        public int GMT { get; set; }
        public List<SocialNetworkResult> redes { get; set; }
        public bool LoginModule { get; set; }
        public bool VoucherModule { get; set; }
        public bool CustomModule { get; set; }
        public bool PayAccessModule { get; set; }
        public bool FreeAccessModule { get; set; }
        public bool SocialNetworkModule { get; set; }
        public List<SurveyResult> encuesta { get; set; }
        public bool MandatorySurvey { get; set; }
        public int DelaySurvey { get; set; }
        public string token { get; set; }

    }
}