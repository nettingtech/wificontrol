﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class ListDisclaimerResult
    {
        public List<DisclaimerResult> list;
        public int pageNumber;
    }
}