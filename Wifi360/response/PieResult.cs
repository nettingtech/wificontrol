﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class PieResult
    {
        public int id { get; set; }
        public int type { get; set; }
        public string label { get; set; }
        public string value { get; set; }
    }
}