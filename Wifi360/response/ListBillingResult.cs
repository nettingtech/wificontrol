﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class ListBillingResult
    {
        public List<BillingResult> list { get; set; }

        public List<FDSUserResult> sellers { get; set; }
        public int pageNumber { get; set; }
        public string total { get; set; }
    }
}