﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class LocationTextResult
    {
        public int idlocationText { get; set; }
        public int idlocation { get; set; }
        public int idlanguage { get; set; }
        public string language { get; set; }
        public string text { get; set; }
    }
}