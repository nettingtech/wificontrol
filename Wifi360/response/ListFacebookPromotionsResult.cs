﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class ListFacebookPromotionsResult
    {
        public List<FacebookPromotionResult> list;
        public int pageNumber;
    }
}