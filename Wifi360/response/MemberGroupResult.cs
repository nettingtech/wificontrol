﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class MemberGroupResult
    {
        public int IdMember { get; set; }
        public int IdMemberType { get; set; }
        public string Name { get; set; }
    }
}