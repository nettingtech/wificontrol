﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class ListUserManagementResult
    {
        public int pageNumber { get; set; }
        public List<UserManagementResult> list { get; set; }
    }
}