﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class ListBillingTypeResult
    {
        public List<BillingTypeResult> list { get; set; }
        public int Default = 0;
    }
}