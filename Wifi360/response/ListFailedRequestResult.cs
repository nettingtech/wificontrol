﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class ListFailedRequestResult
    {
        public List<FailedRequestResult> list { get; set; }
        public int pageNumber { get; set; }
    }
}