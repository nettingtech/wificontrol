﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class FailedRequestResult
    {
        public string Date { get; set; }
        public string UserName { get; set; }
        public string Message { get; set; }
        public string MAC { get; set; }
    }
}