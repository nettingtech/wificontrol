﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class SocialNetworkResult
    {
        public int IdSocialNetwork { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
    }
}