﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class ListRoomResult
    {
        public List<RoomResult> list { get; set; }
        public int Default = 0;
    }
}