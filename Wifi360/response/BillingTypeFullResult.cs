﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class BillingTypeFullResult
    {
        public int IdBillingType { get; set; }
        public int IdLocation { get; set; }
        public string Description { get; set; }
        public string PaypalDescription { get; set; }
        public string Price { get; set; }
        public string TimeCredit { get; set; }
        public string ValidTill { get; set; }
        public string ValidAfterFirstUse { get; set; }
        public string MaxDevices { get; set; }
        public string BWDown { get; set; }
        public string BWUp { get; set; }
        public int Priority { get; set; }
        public string PriorityDescription { get; set; }
        public string LocationDescription { get; set; }
        public string Order { get; set; }
        public string VolumeUp { get; set; }
        public string VolumeDown { get; set; }
        public string VolumeCombined { get; set; }
        public bool FreeAccess{ get; set; }
        public bool STB{ get; set; }
        public bool Enabled { get; set; }
        public int IdBillingModule { get; set; }
        public string BillingModule { get; set; }
        public bool DefaultFastTicket { get; set; }
        public int filterid {get; set;}
        public string urllanding { get; set; }
        public bool iot { get; set; }
        public string currency { get; set; }
        public int nextBillingType { get; set; }
    }
}