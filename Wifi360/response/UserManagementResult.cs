﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class UserManagementResult
    {
        public string Room { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string AccessType { get; set; }
        public string ValidSince { get; set; }
        public string ValidTill { get; set; }
        public string TimeCredit { get; set; }
        public string IdUser { get; set; }
        public string TotalTime { get; set; }
        public string TotalBytesIn { get; set; }
        public string TotalBytesOut { get; set; }
        public string TotalMacs { get; set; }
        public string Priority { get; set; }
        public int IdPriority { get; set; }
        public string BWUp { get; set; }
        public string BWDown { get; set; }
        public string MaxDevices { get; set; }
        public string VolumeUp { get; set; }
        public string VolumeDown { get; set; }
        public string Comment { get; set; }

        public List<UserActivityResult> list { get; set; }
        public List<string> macs { get; set; }

        public string TimeCreditFormatted { get; set; }
        public int filterid { get; set; }
        public string filterdescription { get; set; }

        public int idfdsuser { get; set; }
        public string fdsuser { get; set; }
        public bool Enabled { get; set; }
        public bool Billable { get; set; }
        public string BillingCharge { get; set; }

        public string validAfterFirstUse { get; set; }

    }
}