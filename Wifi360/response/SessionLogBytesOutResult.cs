﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class SessionLogBytesOutResult
    {
        public string Date { get; set; }
        public long Count { get; set; }
    }
}