﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class DashboardResult
    {
        public int bandwidth { get; set; }
        public int bandwidthUp { get; set; }
        public List<InternetAccessResult> access { get; set; }
        public List<SessionLogResult> log { get; set; }
        public List<SessionLogResult> usuariosHoras { get; set; }
        public List<SessionLogBytesInResult> bytesIn { get; set; }
        public List<SessionLogBytesOutResult> bytesOut { get; set; }
        public bool fastticket { get; set; }

        public string connections24hours { get; set; }
        public string connections1hour { get; set; }
        public string down24hours { get; set; }
        public string down1hour { get; set; }
        public string up24hours { get; set; }
        public string up1hour { get; set; }
        public string media24hours { get; set; }
        public string media1hour { get; set; }
        public string speed24hours { get; set; }
        public string speed1hour { get; set; }
        public string device24hours { get; set; }
        public string device1hour { get; set; }
        public string averageuserusage { get; set; }
    }
}