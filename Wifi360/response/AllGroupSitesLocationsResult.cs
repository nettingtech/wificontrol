﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class AllGroupSitesLocationsResult
    {
        public List <GroupResult> Group_list { get; set; }
        public List <HotelResult> Site_list { get; set; }
        public List <LocationResult> Location_list { get; set; }

        public List<GroupResult> Group_list_selected { get; set; }
        public List<HotelResult> Site_list_selected { get; set; }
        public List<LocationResult> Location_list_selected { get; set; }

        public string Name { get; set; }
        public string Currencie { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public Boolean Family { get; set; }
        public Boolean Realmshare { get; set; }

    }
}