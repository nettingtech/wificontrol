﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class MemberResult
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string IdMembertype { get; set; }
        public int IdParent { get; set; }

    }
}