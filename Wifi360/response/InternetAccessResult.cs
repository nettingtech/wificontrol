﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class InternetAccessResult
    {
        public string name { get; set; }
        public int value { get; set; }
    }
}