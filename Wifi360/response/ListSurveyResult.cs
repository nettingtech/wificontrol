﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class ListSurveyResult
    {
        public List<SurveyResult> list;
        public int pageNumber;
    }
}