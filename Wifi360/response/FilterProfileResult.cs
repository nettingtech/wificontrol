﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class FilterProfileResult
    {
        public int IdFilter { get; set; }
        public string Description { get; set; }
    }
}