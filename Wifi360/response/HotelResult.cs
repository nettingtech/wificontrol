﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class HotelResult
    {
        public int IdHotel { get; set; }
        public string Name { get; set; }
        public string NSE { get; set; }
        public string PrincipalNSEIP { get; set; }
        public string PayPalUser { get; set; }
        public string SMTP { get; set; }
        public string SmtpUser { get; set; }
        public string SmtpPassword { get; set; }
        public string SmtpPort { get; set; }
        public bool SmtpSSL { get; set; }
        public string BandWidth { get; set; }
        public string MailFrom { get; set; }
        public string CurrencyCode { get; set; }
        public string CurrencySymbol { get; set; }
        public string GMT { get; set; }
        public string BandwidthUp { get; set; }
        public string UrlHotel { get; set; }
        public bool MacBlocking { get; set; }
        public string MacBlockingInterval { get; set; }
        public string MaxMacAttemps { get; set; }
        public string MACAttempsInterval { get; set; }
        public string UrlReturnPaypal { get; set; }
        public bool EnabledFasTicket { get; set; }
        public bool PrintLogo { get; set; }
        public bool PrintSpeed { get; set; }
        public bool PrintVolume { get; set; }
        public string FreeAccessRepeatTime { get; set; }
        public bool Survey { get; set; }
        public bool LoginModule { get; set; }
        public bool FreeAccessModule { get; set; }
        public bool PayAccessModule { get; set; }
        public bool SocialNetworksModule { get; set; }
        public bool CustomAccessModule { get; set; }
        public bool VoucherAccessModule { get; set; }
        public int IdLanguageDefault { get; set; }
        public bool macauthenticate {get; set;}
        public int vendor { get; set; }
        public bool filtercontents { get; set; }
        public string longitude { get; set; }
        public string latitude { get; set; }
        public string prefix { get; set; }
        public int longusername { get; set; }
        public int longpassword { get; set; }
        public List<SocialNetworkResult> redes { get; set; }

        public bool EnabledDaylyPass { get; set; }
        public string valueDaylyPass { get; set; }

        public int longvoucherusername { get; set; }
        public string voucherprefix { get; set; }

        public bool volumecombined { get; set; }

    }
}