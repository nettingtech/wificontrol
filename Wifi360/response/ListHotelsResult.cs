﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class ListHotelsResult
    {
        public List<HotelResult> list { get; set; }
        public string Name { get; set; }
        public int pageNumber { get; set; }
    }
}