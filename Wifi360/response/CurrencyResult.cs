﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class CurrencyResult
    {
        private int _id;
        private string _code;
        private int _number;
        private string _symbol;
        private string _badge;

        public int Id
        {
            get {return _id ; }
            set { _id = value; }
        }

        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }

        public int Number
        {
            get { return _number; }
            set { _number = value; }
        }

        public string Symbol
        {
            get { return _symbol; }
            set { _symbol = value; }
        }

        public string Badge
        {
            get { return _badge; }
            set { _badge = value; }
        }

    }
}