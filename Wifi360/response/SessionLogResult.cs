﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class SessionLogResult
    {
        public string Date { get; set; }
        public int Count { get; set; }
    }
}