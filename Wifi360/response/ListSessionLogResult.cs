﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class ListSessionLogResult
    {
        public List<SessionLogResult> list { get; set; }
        public int pageNumber { get; set; }
    }
}