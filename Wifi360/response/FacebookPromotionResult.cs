﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class FacebookPromotionResult
    {
        public int IdFacebookPromotion { get; set; }
        public int IdHotel { get; set; }
        public int IdLocation { get; set; }
        public string Location { get; set; }
        public int IdBillingType { get; set; }
        public string BillingType { get; set; }
        public string Page { get; set; }
        public string PageID { get; set; }
        public string CreationDate { get; set; }
        public string EndDate { get; set; }
        public bool Visible { get; set; }
        public string PageName { get; set; }
        public int ValidDays { get; set; }
        public int Order { get; set; }
    }
}