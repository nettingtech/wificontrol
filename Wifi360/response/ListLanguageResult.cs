﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class ListLanguageResult
    {
        public List<LanguageResult> list;
        public int pageNumber;
    }
}