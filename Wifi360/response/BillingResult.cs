﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class BillingResult
    {
        public int idbilling{ get; set; }
        public string Date { get; set; }
        public string Room { get; set; }
        public string Product { get; set; }
        public string Amount { get; set; }
        public string Flag { get; set; }
        public int iduser { get; set; }
        public string username { get; set; }
        public int idfdsuser { get; set; }
        public string fdsusername { get; set; }
        public bool propierty { get; set; }

    }
}