﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class ListBillingModuleResult
    {
        public List<BillingModuleResult> list { get; set; }
        public int pageNumber { get; set; }
    }
}