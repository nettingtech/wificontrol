﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class ListBillingTypeFullResult
    {
        public List<BillingTypeFullResult> list { get; set; }
        public int pageNumber { set; get; }
        public bool isAdmin { get; set; }
    }
}