﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class SessionLogBytesInResult
    {
        public string Date { get; set; }
        public long Count { get; set; }
    }
}