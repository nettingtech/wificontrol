﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class ListOnlineUserResult
    {
        public List<OnlineUserResult> list;
        public int pageNumber;
    }
}