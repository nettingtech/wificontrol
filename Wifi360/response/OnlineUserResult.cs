﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class OnlineUserResult
    {
        public string Initiated { get; set; }
        public string UserName { get; set; }
        public string Room { get; set; }
        public string AccessType { get; set; }
        public string TotalDown { get; set; }
        public string TotalUp { get; set; }
        public string MAC { get; set; }
        public string Comment { get; set; }
        public int IdUser { get; set; }
        public string Filter { get; set; }
        public string Priority { get; set; }
        public int IdFDSUser { get; set; }
        public string fdsuser { get; set; }

        public bool property { get; set; }
    }
}