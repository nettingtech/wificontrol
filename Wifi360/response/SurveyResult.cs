﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class SurveyResult
    {
        public int IdSurvey { get; set; }
        public int IdHotel { get; set; }
        public int IdLanguage { get; set; }
        public int IdLocation { get; set; }
        public string Language { get; set; }
        public string Question { get; set; }
        public string Type { get; set; }
        public string Values { get; set; }
        public int Order { get; set; }
    }
}