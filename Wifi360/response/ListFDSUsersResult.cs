﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class ListFDSUsersResult
    {
        public List<FDSUserResult> list;

        public int PageNumber { get; set; }
    }
}