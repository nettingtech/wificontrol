﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class UserActivityResult
    {
        public string Date { get; set; }
        public long BytesIn { get; set; }
        public long BytesOut { get; set; }
    }
}