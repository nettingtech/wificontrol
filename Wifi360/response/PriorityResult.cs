﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class PriorityResult
    {
        public int IdPriority { get; set; }
        public string Description { get; set; }
    }
}