﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wifi360.response
{
    public class RoomResult
    {
        public int IdRoom { get; set; }
        public string Name { get; set; }
    }
}