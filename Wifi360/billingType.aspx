﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Site.Master" AutoEventWireup="true" CodeBehind="billingType.aspx.cs" Inherits="Wifi360.billingType" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <script src="../js/bignumber.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {

                $("#lidashboard").removeClass();

                var dataString2 = 'action=sitesetting';
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataString2,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    success: function (pReturn) {
                        var lReturn = JSON.parse(pReturn);
                        
                        if (lReturn.filtercontents)
                            $("#filtercontentsgroup").show();
                        else
                            $("#filtercontentsgroup").hide();

                        if (lReturn.vendor != 1)
                            $("#filtercontentsgroup").hide();

                        $("#isVolumeCombined").val(lReturn.volumecombined);

                        if (lReturn.volumecombined) {
                            $("#groupvolumecombined").show();
                            $("#groupvolumedown").hide();
                            $("#groupvolumeup").hide();
                        }
                        else {
                            $("#groupvolumecombined").hide();
                            $("#groupvolumedown").show();
                            $("#groupvolumeup").show();

                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#modal-loading").hide();
                        $("#searchResult").html(xhr.statusText)
                    }
                });


                var dataString2 = 'action=locations';
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataString2,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    success: function (pReturn) {
                        var lReturn = JSON.parse(pReturn);
                        bindSelect('location', 'Idlocation', 'Description', lReturn.list);

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#modal-loading").hide();
                        $("#searchResult").html(xhr.statusText)
                    }
                });

                var dataString3 = 'action=priorities';
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataString3,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    success: function (pReturn) {
                        var lReturn = JSON.parse(pReturn);
                        bindSelect('priority', 'IdPriority', 'Description', lReturn.list);

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#modal-loading").hide();
                        $("#searchResult").html(xhr.statusText)
                    }
                });

                var dataString3 = 'action=filterprofiles';
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataString3,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    success: function (pReturn) {
                        var lReturn = JSON.parse(pReturn);
                        bindSelect('filterprofile', 'IdFilter', 'Description', lReturn.list);

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#modal-loading").hide();
                        $("#searchResult").html(xhr.statusText)
                    }
                });

                var dataString3 = 'action=billingtypes';
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataString3,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    success: function (pReturn) {
                        var lReturn = JSON.parse(pReturn);
                        bindSelectNone('nextbillingtype', 'IdBillingType', 'Description', lReturn.list);

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#modal-loading").hide();
                        $("#searchResult").html(xhr.statusText)
                    }
                });

                var dataString3 = 'action=billingmodules';
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataString3,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    success: function (pReturn) {
                        var lReturn = JSON.parse(pReturn);
                        bindSelect('billingmodule', 'IdBillingModule', 'Name', lReturn.list);

                        var parameters = getParameters();
                        if (parameters.id != undefined) {
                            var dataString = 'action=billingType&id=' + parameters.id;
                            $.ajax({
                                url: "handlers/SMIHandler.ashx",
                                data: dataString,
                                contentType: "application/json; charset=utf-8",
                                dataType: "text",
                                beforeSend: function () {
                                    $("#myModalWait").modal('show');
                                },
                                complete: function () {
                                    $("#myModalWait").modal('hide');
                                },
                                success: function (pReturn) {
                                    var lReturn = JSON.parse(pReturn);
                                    $("#description").val(lReturn.Description);
                                    $("#paypaldescription").val(lReturn.PaypalDescription);
                                    $("#price").val(lReturn.Price.replace(',', '.'));
                                    $("#timecredit").val(lReturn.TimeCredit);
                                    $("#validtill").val(lReturn.ValidTill);
                                    $("#validafterfirstuse").val(lReturn.ValidAfterFirstUse);
                                    $("#maxdevices").val(lReturn.MaxDevices);
                                    $("#bwdown").val(lReturn.BWDown);
                                    $("#bwup").val(lReturn.BWUp);
                                    $("#location").val(lReturn.IdLocation);
                                    $("#priority").val(lReturn.Priority);
                                    $("#order").val(lReturn.Order);
                                    $("#volumeup").val(lReturn.VolumeUp);
                                    $("#volumedown").val(lReturn.VolumeDown);
                                    $("#enabled").attr('checked', lReturn.Enabled);
                                    $("#billingmodule").val(lReturn.IdBillingModule);
                                    $("#enabledfastticket").attr('checked', lReturn.DefaultFastTicket);
                                    $("#filterprofile").val(lReturn.filterid);
                                    $("#urllanding").val(lReturn.urllanding);
                                    $("#iot").attr('checked', lReturn.iot);
                                    $("#nextbillingtype").val(lReturn.nextBillingType);
                                    $("#currency").html(lReturn.currency);
                                    $("#volumecombined").val(lReturn.VolumeCombined);

                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    $("#modal-loading").hide();
                                    $("#searchResult").html(xhr.statusText)
                                }
                            });
                        }

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#modal-loading").hide();
                        $("#searchResult").html(xhr.statusText)
                    }
                });

                function savebilling() {

                    var parameters = getParameters();

                    var data = {};
                    data["action"] = 'SAVEBILLINGTYPE';

                    data["id"] = parameters.id;
                    
                    if (!$.isNumeric(parameters.id))
                        data["id"] = "undefined";

                    var timecredit = 0;
                    switch ($("#unidadestimecredit").val()) {
                        case 's': timecredit = $("#timecredit").val().replace(',', '.');
                            break;
                        case 'h': timecredit = $("#timecredit").val().replace(',', '.') * 3600;
                            break;
                        case 'd': timecredit = $("#timecredit").val().replace(',', '.') * 86400;
                            break;
                    }

                    var validtill = 0;
                    switch ($("#unidadesvalidtill").val()) {
                        case 'h': validtill = $("#validtill").val().replace(',', '.').split('.')[0];
                            break;
                        case 'd': validtill = $("#validtill").val().replace(',', '.').split('.')[0] * 24;
                            break;
                    }
                    var validafterfirstuse = 0;
                    switch ($("#unidadesvalidafterfirstuse").val()) {
                        case 'h': validafterfirstuse = $("#validafterfirstuse").val().replace(',', '.').split('.')[0];
                            break;
                        case 'd': validafterfirstuse = $("#validafterfirstuse").val().replace(',', '.').split('.')[0] * 24;
                            break;
                    }

                    var bwup = 0;
                    switch ($("#unidadesbwup").val()) {
                        case 'kbps': bwup = BigNumber($("#bwup").val().replace(',', '.'));
                            break;
                        case 'mbps': bwup = BigNumber($("#bwup").val().replace(',', '.')).multipliedBy(BigNumber("1000"));
                            break;
                    }

                    var bwdown = 0;
                    switch ($("#unidadesbwdown").val()) {
                        case 'kbps': bwdown = BigNumber($("#bwdown").val().replace(',', '.'));
                            break;
                        case 'mbps': bwdown = BigNumber($("#bwdown").val().replace(',', '.')).multipliedBy(BigNumber("1000"));
                            break;
                    }

                    var volumeup = 0;
                    var volumeuppre = $("#volumeup").val().replace(',', '.').split('.')[0];
                    switch ($("#unidadesvolumeup").val()) {
                        case 'bytes': volumeup = BigNumber(volumeuppre);
                            break;
                        case 'KB': volumeup = BigNumber(volumeuppre).multipliedBy(BigNumber("1024"));
                            break;
                        case 'MB': volumeup = BigNumber(volumeuppre).multipliedBy(BigNumber("1048576"));
                            break;
                        case 'GB': volumeup = BigNumber(volumeuppre).multipliedBy(BigNumber("1073741824"));
                            break;
                    }

                    var volumedown = 0;
                    var volumedowndown = $("#volumedown").val().replace(',', '.').split('.')[0];
                    switch ($("#unidadesvolumedown").val()) {
                        case 'bytes': volumedown = BigNumber(volumedowndown);
                            break;
                        case 'KB': volumedown = BigNumber(volumedowndown).multipliedBy(BigNumber("1024"));
                            break;
                        case 'MB': volumedown = BigNumber(volumedowndown).multipliedBy(BigNumber("1048576"));
                            break;
                        case 'GB': volumedown = BigNumber(volumedowndown).multipliedBy(BigNumber("1073741824"));
                            break;
                    }

                    var volumecombined = 0;
                    var volumecombinedformat = $("#volumecombined").val().replace(',', '.').split('.')[0];
                    switch ($("#unidadesvolumecombined").val()) {
                        case 'bytes': volumecombined = BigNumber(volumecombinedformat);
                            break;
                        case 'KB': volumecombined = BigNumber(volumecombinedformat).multipliedBy(BigNumber("1024"));
                            break;
                        case 'MB': volumecombined = BigNumber(volumecombinedformat).multipliedBy(BigNumber("1048576"));
                            break;
                        case 'GB': volumecombined = BigNumber(volumecombinedformat).multipliedBy(BigNumber("1073741824"));
                            break;
                    }

                    data["description"] = $("#description").val();
                    data["paypaldescription"]  = $("#paypaldescription").val();
                    data["price"] = $("#price").val().replace('.', ',');
                    data["timecredit"] = timecredit;
                    data["validtill"] = validtill;
                    data["validafterfirstuse"] = validafterfirstuse;
                    data["maxdevices"]  = $("#maxdevices").val();
                    data["bwdown"]  = bwdown.toFixed(5);
                    data["bwup"] = bwup.toFixed(5);
                    data["priority"]  = $("#priority").val();
                    data["location"]  = $("#location").val();
                    data["order"]  = $("#order").val();
                    data["volumeup"] = volumeup.toFixed(5);
                    data["volumedown"] = volumedown.toFixed(5);
                    data["freeaccess"]  = "False";
                    data["visible"]  = "False";
                    data["enabled"]  = $("#enabled").is(":checked") ? "True" : "False";
                    data["billingmodule"]  = $("#billingmodule").val();
                    data["fastticket"]  = $("#enabledfastticket").is(":checked") ? "True" : "False";
                    data["urllanding"] = $("#urllanding").val();
                    data["iot"] = $("#iot").is(":checked") ? "True" : "False";
                    data["nextBillingType"] = $("#nextbillingtype").val();
                    data["filterid"] = $("#filterprofile").val();
                    data["volumecombined"] = volumecombined.toFixed(5);

                    $.ajax({
                        url: "handlers/SMIHandler.ashx",
                        type: "POST",
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {
                            $("#myModalWait").modal('show');
                        },
                        complete: function () {
                            $("#myModalWait").modal('hide');
                        },
                        success: function (msg) {
                            if (msg.code == 'OK') {
                                $("#message").removeClass().addClass("alert alert-success");
                                $("#message").html(GetMessage(msg.message));
                             }
                            else {
                                $("#message").removeClass().addClass("alert alert-danger");
                                $("#message").html(GetMessage(msg.message));
                            }

                            $("#myModalMessage").modal('show');
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#message").removeClass().addClass("alert alert-danger");
                            $("#message").html(xhr.statusText);
                            $("#myModalMessage").modal('show');

                        }
                    });
                };

                $("#save").on("click", function (e) {
                    e.preventDefault();
                    $("#message").removeClass().addClass("alert alert-danger");
                    var cookie = readCookie("wifi360-language");
                    var MaxBigInt = BigNumber("9223372036854775807");
                    if (($("#bwdown").val() == null) || ($("#bwdown").val() <=0)
                        || ($("#bwup").val() == null) || ($("#bwup").val() <= 0)) {
                        $("#message").html(GetMessage('0x0040'));
                        $("#myModalMessage").modal('show');
                    }


                    
                    var bwdown = BigNumber($("#bwdown").val().replace(',','.'));
                    var bwup = BigNumber($("#bwup").val().replace(',', '.'));
                    if ($("#isVolumeCombined").val() == "true") {
                        $("#volumedown").val("0");
                        $("#volumeup").val("0");
                    }
                    else {
                        $("#volumecombined").val("0");
                        if (($("#volumedown").val() == null) || ($("#volumedown").val() <= 0)
                            || ($("#volumeup").val() == null) || ($("#volumeup").val() <= 0)) {
                            $("#message").html(GetMessage('0x0041'));
                            $("#myModalMessage").modal('show');
                        }
                    }
                    var volumedownpre = $("#volumedown").val().replace(',', '.').split('.')[0];
                    var volumedown = BigNumber(volumedownpre);
                    var volumeuppre = $("#volumeup").val().replace(',', '.').split('.')[0];
                    var volumeup = BigNumber(volumeuppre);
                    var volumecombinedpre = $("#volumecombined").val().replace(',', '.').split('.')[0];
                    var volumecombined = BigNumber(volumecombinedpre);


                    if (($("#price").val() == '') || $("#location").val() == "0" || $("#description").val() == '' || $("#paypaldescription").val() == ''
                        || $("#timecredit").val() == '' || $("#validtill").val() == '' || $("#maxdevices").val() == '' || $("#bwdown").val() == ''
                         || $("#bwup").val() == '' || $("#order").val() == '' || $("#volumeup").val() == '' || $("#volumedown").val() == '' || $("#location").val() == "0"
                         || $("#billingmodule").val() == "0" || $("#priority").val() == "0") {

                        $("#message").html(GetMessage('0x0011'));
                        $("#myModalMessage").modal('show');
                    }
                    else if ((($("#unidadesvalidtill").val() == 'h') && ($("#validtill").val() > (2147483647)))
                        || (($("#unidadesvalidtill").val() == 'd') && ($("#validtill").val() > (2147483647 / 24)))) { //
                        $("#message").html(GetMessage('0x0012'));
                        $("#myModalMessage").modal('show');
                    }
                    else if ((($("#unidadesvalidafterfirstuse").val() == 'h') && ($("#validafterfirstuse").val() > (2147483647)))
                        || (($("#unidadesvalidafterfirstuse").val() == 'd') && ($("#validafterfirstuse").val() > (2147483647 / 24)))) { //
                        $("#message").html(GetMessage('0x0012'));
                        $("#myModalMessage").modal('show');
                    }
                    else if ((($("#unidadestimecredit").val() == 's') && ($("#timecredit").val() * $("#maxdevices").val() > 2147483647))
                    || (($("#unidadestimecredit").val() == 'h') && ($("#timecredit").val() * $("#maxdevices").val() > (2147483647 / 3600)))
                    || (($("#unidadestimecredit").val() == 'd') && ($("#timecredit").val() * $("#maxdevices").val() > (2147483647 / 86400)))) { //
                        $("#message").html(GetMessage('0x0013'));
                        $("#myModalMessage").modal('show');
                    }
                        //AHORA COMPROBAMOS EL BANDWIDTH DOWN
                    else if ((($("#unidadesbwdown").val() == 'kbps') && (bwdown > MaxBigInt))
                    || (($("#unidadesbwdown").val() == 'mbps') && (bwdown > (MaxBigInt.dividedBy(BigNumber("1000")).toFixed(5)))))
                    { //
                        $("#message").html(GetMessage('0x0014'));
                        $("#myModalMessage").modal('show');
                    } //AHORA COMPROBAMOS EL BANDWIDTH UP
                    else if ((($("#unidadesbwup").val() == 'kbps') && (bwup > MaxBigInt))
                    || (($("#unidadesbwup").val() == 'mbps') && (bwup > (MaxBigInt.dividedBy(BigNumber("1000")).toFixed(5))))) { //
                        $("#message").html(GetMessage('0x0015'));
                        $("#myModalMessage").modal('show');
                    } //AHORA COMPROBAMOS EL VOLUME DOWN
                    else if ((($("#unidadesvolumedown").val() == 'bytes') && (volumedown.multipliedBy(BigNumber($("#maxdevices").val())) > MaxBigInt))
                    || (($("#unidadesvolumedown").val() == 'KB') && (volumedown.multipliedBy(BigNumber($("#maxdevices").val())) > (MaxBigInt.dividedBy(BigNumber("1024")).toFixed(5))))
                    || (($("#unidadesvolumedown").val() == 'MB') && (volumedown.multipliedBy(BigNumber($("#maxdevices").val())) > (MaxBigInt.dividedBy(BigNumber("1048576")).toFixed(5))))
                    || (($("#unidadesvolumedown").val() == 'GB') && (volumedown.multipliedBy(BigNumber($("#maxdevices").val())) > (MaxBigInt.dividedBy(BigNumber("1073741824")).toFixed(5)))))
                    { //
                        $("#message").html(GetMessage('0x0016'));
                        $("#myModalMessage").modal('show');
                    }
                        //AHORA COMPROBAMOS EL VOLUME UP
                    else if ((($("#unidadesvolumeup").val() == 'bytes') && (volumeup.multipliedBy(BigNumber($("#maxdevices").val())) > MaxBigInt))
                    || (($("#unidadesvolumeup").val() == 'KB') && (volumeup.multipliedBy(BigNumber($("#maxdevices").val())) > (MaxBigInt.dividedBy(BigNumber("1024")).toFixed(5))))
                    || (($("#unidadesvolumeup").val() == 'MB') && (volumeup.multipliedBy(BigNumber($("#maxdevices").val())) > (MaxBigInt.dividedBy(BigNumber("1048576")).toFixed(5))))
                    || (($("#unidadesvolumeup").val() == 'GB') && (volumeup.multipliedBy(BigNumber($("#maxdevices").val())) > (MaxBigInt.dividedBy(BigNumber("1073741824")).toFixed(5))))) { //
                        $("#message").html(GetMessage('0x0017'));

                        $("#myModalMessage").modal('show');
                    }
                        //AHORA COMPROBAMOS EL VOLUME COMBINED
                    else if ((($("#unidadesvolumecombined").val() == 'bytes') && (volumecombined.multipliedBy(BigNumber($("#maxdevices").val())) > MaxBigInt))
                    || (($("#unidadesvolumecombined").val() == 'KB') && (volumecombined.multipliedBy(BigNumber($("#maxdevices").val())) > (MaxBigInt.dividedBy(BigNumber("1024")).toFixed(5))))
                    || (($("#unidadesvolumecombined").val() == 'MB') && (volumecombined.multipliedBy(BigNumber($("#maxdevices").val())) > (MaxBigInt.dividedBy(BigNumber("1048576")).toFixed(5))))
                    || (($("#unidadesvolumecombined").val() == 'GB') && (volumecombined.multipliedBy(BigNumber($("#maxdevices").val())) > (MaxBigInt.dividedBy(BigNumber("1073741824")).toFixed(5))))) { //
                        $("#message").html(GetMessage('0x0036'));

                        $("#myModalMessage").modal('show');
                    }
                    else
                        savebilling();
                });

                $("#back").on("click", function (e) {
                    e.preventDefault();
                    window.location = "ManageBillingTypes.aspx";
                });

                $("#checktimecredit").change(function (e) {
                    e.preventDefault();
                    if ($("#checktimecredit").is(":checked")) {
                        var devices = $("#maxdevices").val();
                        switch ($("#unidadestimecredit").val()) {
                            case 's': $("#timecredit").val((2147483647 / devices).toString().replace(",", ".")); break;
                            case 'h': $("#timecredit").val(((2147483647 / 3600) / devices).toFixed(2).replace(",", ".")); break;
                            case 'd': $("#timecredit").val(((2147483647 / 86400) / devices).toFixed(2).replace(",", ".")); break;
                        }
                    }
                });

                $("#price").change(function (e) {
                    e.preventDefault();
                    $("#price").val($("#price").val().replace(",", "."));
                });

                $("#prevunidadtimecredit").val($("#unidadestimecredit").val());
                $("#unidadestimecredit").change(function (e) {
                    switch ($("#unidadestimecredit").val()) {
                        case 's':
                            switch ($("#prevunidadtimecredit").val())
                            {
                                case 'h': $("#timecredit").val(($("#timecredit").val().replace(",", ".") * 3600).toString().replace(",", ".")); break;
                                case 'd': $("#timecredit").val(($("#timecredit").val().replace(",", ".") * 86400).toString().replace(",", ".")); break;
                            }
                            break;
                        case 'h':
                            switch ($("#prevunidadtimecredit").val()) {
                                case 's': $("#timecredit").val(($("#timecredit").val().replace(",", ".") / 3600).toString().replace(",", ".")); break;
                                case 'd': $("#timecredit").val(($("#timecredit").val().replace(",", ".") * 24).toString().replace(",", ".")); break;
                            }
                            break;
                        case 'd':
                            switch ($("#prevunidadtimecredit").val()) {
                                case 's': $("#timecredit").val(($("#timecredit").val().replace(",", ".") / 86400).toString().replace(",", ".")); break;
                                case 'h': $("#timecredit").val(($("#timecredit").val().replace(",", ".") / 24).toString().replace(",", ".")); break;
                            }
                            break;
                    }
                    $("#prevunidadtimecredit").val($("#unidadestimecredit").val());
                });

                $("#prevunidadvalidtill").val($("#unidadesvalidtill").val());
                $("#unidadesvalidtill").change(function (e) {
                    switch ($("#unidadesvalidtill").val()) {
                        case 'h':
                            switch ($("#prevunidadvalidtill").val()) {
                                case 'd': $("#validtill").val(($("#validtill").val().replace(",", ".") * 24).toString().replace(",", ".")); break;
                            }
                            break;
                        case 'd':
                            switch ($("#prevunidadvalidtill").val()) {
                                case 'h': $("#validtill").val(($("#validtill").val().replace(",", ".") / 24).toString().replace(",", ".")); break;
                            }
                            break;
                    }
                    $("#prevunidadvalidtill").val($("#unidadesvalidtill").val());
                });

                $("#prevunidadvalidafterfirstuse").val($("#unidadesvalidafterfirstuse").val());
                $("#unidadesvalidafterfirstuse").change(function (e) {
                    switch ($("#unidadesvalidafterfirstuse").val()) {
                        case 'h':
                            switch ($("#prevunidadvalidafterfirstuse").val()) {
                                case 'd': $("#validafterfirstuse").val(($("#validafterfirstuse").val().replace(",", ".") * 24).toString().replace(",", ".")); break;
                            }
                            break;
                        case 'd':
                            switch ($("#prevunidadvalidafterfirstuse").val()) {
                                case 'h': $("#validafterfirstuse").val(($("#validafterfirstuse").val().replace(",", ".") / 24).toString().replace(",", ".")); break;
                            }
                            break;
                    }
                    $("#prevunidadvalidafterfirstuse").val($("#unidadesvalidafterfirstuse").val());
                });

                $("#prevunidadbwdw").val($("#unidadesbwdown").val());
                $("#unidadesbwdown").change(function (e) {
                    switch ($("#unidadesbwdown").val()) {
                        case 'kbps':
                            switch ($("#prevunidadbwdw").val()) {
                                case 'mbps': $("#bwdown").val(($("#bwdown").val().replace(",", ".") * 1000).toString().replace(",", ".")); break;
                            }
                            break;
                        case 'mbps':
                            switch ($("#prevunidadbwdw").val()) {
                                case 'kbps': $("#bwdown").val(($("#bwdown").val().replace(",", ".") / 1000).toString().replace(",", ".")); break;
                            }
                            break;
                    }
                    $("#prevunidadbwdw").val($("#unidadesbwdown").val());
                });

                $("#prevunidadbwup").val($("#unidadesbwup").val());
                $("#unidadesbwup").change(function (e) {
                    switch ($("#unidadesbwup").val()) {
                        case 'kbps':
                            switch ($("#prevunidadbwup").val()) {
                                case 'mbps': $("#bwup").val(($("#bwup").val().replace(",", ".") * 1000).toString().replace(",", ".")); break;
                            }
                            break;
                        case 'mbps':
                            switch ($("#prevunidadbwup").val()) {
                                case 'kbps': $("#bwup").val(($("#bwup").val().replace(",", ".") / 1000).toString().replace(",", ".")); break;
                            }
                            break;
                    }
                    $("#prevunidadbwup").val($("#unidadesbwup").val());
                });

                $("#prevunidadvolumedw").val($("#unidadesvolumedown").val());
                $("#unidadesvolumedown").change(function (e) {
                    var maxBigInt = BigNumber("9223372036854775807");
                    var devices = BigNumber($("#maxdevices").val());
                    var kb = BigNumber("1024");
                    var mb = BigNumber("1048576");
                    var gb = BigNumber("1073741824");
                    var value = BigNumber($("#volumedown").val().replace(",", "."));
                    switch ($("#unidadesvolumedown").val()) {
                        case 'bytes':
                            switch ($("#prevunidadvolumedw").val()) {
                                case 'KB': if ((maxBigInt.dividedBy(devices)) > (value.multipliedBy(kb).dividedBy(devices)))
                                    $("#volumedown").val(value.multipliedBy(kb).toFixed(5).replace(",", "."));
                                else
                                    $("#volumedown").val(maxBigInt);

                                    break;
                                case 'MB':
                                    if ((maxBigInt.dividedBy(devices)) > (value.multipliedBy(mb).dividedBy(devices)))
                                        $("#volumedown").val(value.multipliedBy(mb).toFixed(5).replace(",", "."));
                                    else
                                        $("#volumedown").val(maxBigInt);
                                    break;
                                case 'GB':
                                    if ((maxBigInt.dividedBy(devices)) > (value.multipliedBy(gb).dividedBy(devices)))
                                        $("#volumedown").val(value.multipliedBy(gb).toFixed(5).replace(",", "."));
                                    else
                                        $("#volumedown").val(maxBigInt);
                                     break;
                            }
                            break;
                        case 'KB':
                            switch ($("#prevunidadvolumedw").val()) {
                                case 'bytes': $("#volumedown").val(value.dividedBy(kb).toFixed(5).replace(",", ".")); break;
                                case 'MB': $("#volumedown").val(value.multipliedBy(kb).toFixed(5).replace(",", ".")); break;
                                case 'GB': $("#volumedown").val(value.multipliedBy(mb).toFixed(5).replace(",", ".")); break;
                            }
                            break;
                        case 'MB':
                            switch ($("#prevunidadvolumedw").val()) {
                                case 'bytes': $("#volumedown").val(value.dividedBy(mb).toFixed(5).replace(",", ".")); break;
                                case 'KB': $("#volumedown").val(value.dividedBy(kb).toFixed(5).replace(",", ".")); break;
                                case 'GB': $("#volumedown").val(value.multipliedBy(kb).toFixed(5).replace(",", ".")); break;
                            }
                            break;
                        case 'GB':
                            switch ($("#prevunidadvolumedw").val()) {
                                case 'bytes': $("#volumedown").val(value.dividedBy(gb).toFixed(5).replace(",", ".")); break;
                                case 'KB': $("#volumedown").val(value.dividedBy(mb).toFixed(5).replace(",", ".")); break;
                                case 'MB': $("#volumedown").val(value.dividedBy(kb).toFixed(5).replace(",", ".")); break;
                            }
                            break;
                    }
                    $("#prevunidadvolumedw").val($("#unidadesvolumedown").val());
                });

                $("#checkminvolume").change(function (e) {
                    e.preventDefault();
                    if ($("#checkminvolume").is(":checked")) {
                        var maxBigInt = BigNumber("9223372036854775807");
                        var devices = BigNumber($("#maxdevices").val());
                        switch ($("#unidadesvolumedown").val()) {
                            case 'bytes': $("#volumedown").val(maxBigInt.dividedBy(devices).toFixed(5).replace(",", ".")); break;
                            case 'KB': var kb = BigNumber("1024"); $("#volumedown").val((maxBigInt.dividedBy(kb)).dividedBy(devices).toFixed(5).replace(",", ".")); break;
                            case 'MB': var mb = BigNumber("1048576"); $("#volumedown").val((maxBigInt.dividedBy(mb)).dividedBy(devices).toFixed(5).replace(",", ".")); break;
                            case 'GB': var gb = BigNumber("1073741824"); $("#volumedown").val((maxBigInt.dividedBy(gb)).dividedBy(devices).toFixed(5).replace(",", ".")); break;
                        }
                    }

                });

                $("#checkmaxvolume").change(function (e) {
                    e.preventDefault();
                    if ($("#checkmaxvolume").is(":checked")) {
                        var maxBigInt = BigNumber("9223372036854775807");
                        var devices = BigNumber($("#maxdevices").val());
                        switch ($("#unidadesvolumeup").val()) {
                            case 'bytes': $("#volumeup").val(maxBigInt.dividedBy(devices).toFixed(5).replace(",", ".")); break;
                            case 'KB': var kb = BigNumber("1024"); $("#volumeup").val((maxBigInt.dividedBy(kb)).dividedBy(devices).toFixed(5).replace(",", ".")); break;
                            case 'MB': var mb = BigNumber("1048576"); $("#volumeup").val((maxBigInt.dividedBy(mb)).dividedBy(devices).toFixed(5).replace(",", ".")); break;
                            case 'GB': var gb = BigNumber("1073741824"); $("#volumeup").val((maxBigInt.dividedBy(gb)).dividedBy(devices).toFixed(5).replace(",", ".")); break;
                        }
                    }

                });
                
                $("#prevunidadvolumeup").val($("#unidadesvolumeup").val());
                $("#unidadesvolumeup").change(function (e) {
                    var maxBigInt = BigNumber("9223372036854775807");
                    var devices = BigNumber($("#maxdevices").val());
                    var kb = BigNumber("1024");
                    var mb = BigNumber("1048576");
                    var gb = BigNumber("1073741824");
                    var value = BigNumber($("#volumeup").val().replace(",", "."));
                    switch ($("#unidadesvolumeup").val()) {
                        case 'bytes':
                            switch ($("#prevunidadvolumeup").val()) {
                                case 'KB': if ((maxBigInt.dividedBy(devices)) > (value.multipliedBy(kb).dividedBy(devices)))
                                    $("#volumeup").val(value.multipliedBy(kb).toFixed(5).replace(",", "."));
                                else
                                    $("#volumeup").val(maxBigInt);

                                    break;
                                case 'MB':
                                    if ((maxBigInt.dividedBy(devices)) > (value.multipliedBy(mb).dividedBy(devices)))
                                        $("#volumeup").val(value.multipliedBy(mb).toFixed(5).replace(",", "."));
                                    else
                                        $("#volumeup").val(maxBigInt);
                                    break;
                                case 'GB':
                                    if ((maxBigInt.dividedBy(devices)) > (value.multipliedBy(gb).dividedBy(devices)))
                                        $("#volumeup").val(value.multipliedBy(gb).toFixed(5).replace(",", "."));
                                    else
                                        $("#volumeup").val(maxBigInt);
                                    break;
                            }
                            break;
                        case 'KB':
                            switch ($("#prevunidadvolumeup").val()) {
                                case 'bytes': $("#volumeup").val(value.dividedBy(kb).toFixed(5).replace(",", ".")); break;
                                case 'MB': $("#volumeup").val(value.multipliedBy(kb).toFixed(5).replace(",", ".")); break;
                                case 'GB': $("#volumeup").val(value.multipliedBy(mb).toFixed(5).replace(",", ".")); break;
                            }
                            break;
                        case 'MB':
                            switch ($("#prevunidadvolumeup").val()) {
                                case 'bytes': $("#volumeup").val(value.dividedBy(mb).toFixed(5).replace(",", ".")); break;
                                case 'KB': $("#volumeup").val(value.dividedBy(kb).toFixed(5).replace(",", ".")); break;
                                case 'GB': $("#volumeup").val(value.multipliedBy(kb).toFixed(5).replace(",", ".")); break;
                            }
                            break;
                        case 'GB':
                            switch ($("#prevunidadvolumeup").val()) {
                                case 'bytes': $("#volumeup").val(value.dividedBy(gb).toFixed(5).replace(",", ".")); break;
                                case 'KB': $("#volumeup").val(value.dividedBy(mb).toFixed(5).replace(",", ".")); break;
                                case 'MB': $("#volumeup").val(value.dividedBy(kb).toFixed(5).replace(",", ".")); break;
                            }
                            break;
                    }
                    $("#prevunidadvolumeup").val($("#unidadesvolumeup").val());
                });

                $("#checkmaxvolumecombined").change(function (e) {
                    e.preventDefault();
                    if ($("#checkmaxvolumecombined").is(":checked")) {
                        var maxBigInt = BigNumber("9223372036854775807");
                        var devices = BigNumber($("#maxdevices").val());
                        switch ($("#unidadesvolumecombined").val()) {
                            case 'bytes': $("#volumecombined").val(maxBigInt.dividedBy(devices).toFixed(5).replace(",", ".")); break;
                            case 'KB': var kb = BigNumber("1024"); $("#volumecombined").val((maxBigInt.dividedBy(kb)).dividedBy(devices).toFixed(5).replace(",", ".")); break;
                            case 'MB': var mb = BigNumber("1048576"); $("#volumecombined").val((maxBigInt.dividedBy(mb)).dividedBy(devices).toFixed(5).replace(",", ".")); break;
                            case 'GB': var gb = BigNumber("1073741824"); $("#volumecombined").val((maxBigInt.dividedBy(gb)).dividedBy(devices).toFixed(5).replace(",", ".")); break;
                        }
                    }
                });

                $("#prevunidadvolumecombined").val($("#unidadesvolumecombined").val());
                $("#unidadesvolumecombined").change(function (e) {
                    var maxBigInt = BigNumber("9223372036854775807");
                    var devices = BigNumber($("#maxdevices").val());
                    var kb = BigNumber("1024");
                    var mb = BigNumber("1048576");
                    var gb = BigNumber("1073741824");
                    var value = BigNumber($("#volumecombined").val().replace(",", "."));
                    switch ($("#unidadesvolumecombined").val()) {
                        case 'bytes':
                            switch ($("#prevunidadvolumecombined").val()) {
                                case 'KB':
                                    if ((maxBigInt.dividedBy(devices)) < (value.multipliedBy(kb).dividedBy(devices)))
                                        $("#volumecombined").val(value.multipliedBy(kb).toFixed(5).replace(",", "."));
                                    else
                                        $("#volumecombined").val(maxBigInt);
                                    break;
                                case 'MB':
                                        if ((maxBigInt.dividedBy(devices)) < (value.multipliedBy(mb).dividedBy(devices)))
                                            $("#volumecombined").val(value.multipliedBy(mb).toFixed(5).replace(",", "."));
                                        else
                                            $("#volumecombined").val(maxBigInt);
                                        break;
                                case 'GB':
                                        if ((maxBigInt.dividedBy(devices)) < (value.multipliedBy(gb).dividedBy(devices)))
                                            $("#volumecombined").val(value.multipliedBy(gb).toFixed(5).replace(",", "."));
                                        else
                                            $("#volumecombined").val(maxBigInt);
                                        break;
                            }
                            break;
                        case 'KB':
                            switch ($("#prevunidadvolumecombined").val()) {
                                case 'bytes': $("#volumecombined").val(value.dividedBy(kb).toFixed(5).replace(",", ".")); break;
                                case 'MB': $("#volumecombined").val(value.multipliedBy(kb).toFixed(5).replace(",", ".")); break;
                                case 'GB': $("#volumecombined").val(value.multipliedBy(mb).toFixed(5).replace(",", ".")); break;
                            }
                            break;
                        case 'MB':
                            switch ($("#prevunidadvolumecombined").val()) {
                                case 'bytes': $("#volumecombined").val(value.dividedBy(mb).toFixed(5).replace(",", ".")); break;
                                case 'KB': $("#volumecombined").val(value.dividedBy(kb).toFixed(5).replace(",", ".")); break;
                                case 'GB': $("#volumecombined").val(value.multipliedBy(kb).toFixed(5).replace(",", ".")); break;
                            }
                            break;
                        case 'GB':
                            switch ($("#prevunidadvolumecombined").val()) {
                                case 'bytes': $("#volumecombined").val(value.dividedBy(gb).toFixed(5).replace(",", ".")); break;
                                case 'KB': $("#volumecombined").val(value.dividedBy(mb).toFixed(5).replace(",", ".")); break;
                                case 'MB': $("#volumecombined").val(value.dividedBy(kb).toFixed(5).replace(",", ".")); break;
                            }
                            break;
                    }
                    $("#prevunidadvolumecombined").val($("#unidadesvolumecombined").val());
                });

                $("#newpriority").click(function (e) {
                    e.preventDefault();

                    var returnurl = window.location.pathname + window.location.search;
                    window.location.href = "priority.aspx?returnurl=" + returnurl;
                });

                $("#newprofile").click(function (e) {
                    e.preventDefault();

                    var returnurl = window.location.pathname + window.location.search;
                    window.location.href = "filterprofile.aspx?returnurl=" + returnurl;
                });

                $("#location").change(function (e) {
                     
                    var dataString2 = 'action=location&id=' +$("#location").val();
                    $.ajax({
                        url: "handlers/SMIHandler.ashx",
                        data: dataString2,
                        contentType: "application/json; charset=utf-8",
                        dataType: "text",
                        beforeSend: function () {
                            $("#myModalWait").modal('show');
                        },
                        complete: function () {
                            $("#myModalWait").modal('hide');
                        },
                        success: function (pReturn) {
                            var lReturn = JSON.parse(pReturn);
                            
                            $("#currency").html(lReturn.Currencysymbol);

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#modal-loading").hide();
                            $("#searchResult").html(xhr.statusText)
                        }
                    });
                });

            });

            

            function readCookie(name) {
                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
                }
                return null;
            };

            function getParameters() {
                var searchString = window.location.search.substring(1)
                  , params = searchString.split("&")
                  , hash = {}
                ;

                for (var i = 0; i < params.length; i++) {
                    var val = params[i].split("=");
                    hash[unescape(val[0])] = unescape(val[1]);
                }
                return hash;
            }

            function bindSelect(id, valueMember, displayMember, source) {
                $("#" + id).find('option').remove().end();
                var cookie = readCookie("wifi360-language");
                if (cookie != null) {
                    switch (cookie) {
                        case "es": $("#" + id).append('<option value="0">Seleccionar</option>'); break;
                        default: $("#" + id).append('<option value="0">Select</option>'); break;
                    }
                }
                else
                    $("#" + id).append('<option value="0">Select</option>');

                $.each(source, function (index, item) {
                    $("#" + id).append('<option value="' + item[valueMember] + '"> ' + item[displayMember] + '</option>');
                });
            };

            function bindSelectNone(id, valueMember, displayMember, source) {
                $("#" + id).find('option').remove().end();
                var cookie = readCookie("wifi360-language");
                if (cookie != null) {
                    switch (cookie) {
                        case "es": $("#" + id).append('<option value="0">Ninguno</option>'); break;
                        default: $("#" + id).append('<option value="0">None</option>'); break;
                    }
                }
                else
                    $("#" + id).append('<option value="0">None</option>');

                $.each(source, function (index, item) {
                    $("#" + id).append('<option value="' + item[valueMember] + '"> ' + item[displayMember] + '</option>');
                });
            };

           
            

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 id="headerpagebillingtype">Access Type Management</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="default.aspx">Dashboard</a>
                </li>
                <li>
                    <a>Tickets</a>
                </li>
                <li>
                    <a href="ManageBillingTypes.aspx" id="breadlabelfatherbillingtype">Access types</a>
                </li>
                <li class="active">
                    <strong id="breadlabelbillingtype">Access type</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-9">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                    <h5 id="labelboxheaderbillingtype">Access type<small> Details of access type</small></h5>
                </div>
                    <div class="ibox-content">
                        <div class="form-horizontal">
                            <div class="form-group"> <label class="control-label col-sm-4" id="labelformbillinglocation">Location</label><div class="col-sm-8"><select id="location" class="form-control"></select></div></div>
                            <div class="form-group"> <label class="control-label col-sm-4" id="labelformbillingdescription">Description</label><div class="col-sm-8"><input type="text" class="form-control" id="description" /></div></div>
                            <div class="form-group"> <label class="control-label col-sm-4" id="labelformbillingfolio">Invoice description</label><div class="col-sm-8"><input type="text" class="form-control" id="paypaldescription" /></div></div>
                            <div class="form-group"> <label class="control-label col-sm-4" id="labelformbillingprice">Price</label><div class="col-sm-8"><div class="input-group m-b"><input type="text"class="form-control"  id="price" value="0" /><span class="input-group-addon" id="currency"></span></div></div></div>
                            <!-- TIMECREDIT -->
                            <div class="form-group"> <label class="control-label col-sm-4" id="labelformbillingdevices">Max devices</label><div class="col-sm-8"><input type="number" id="maxdevices" class="form-control"  value="0" /></div></div>
                            <div class="form-group"> 
                                <label class="control-label col-sm-4" id="labelformbillingtimecredit">Time credit per device</label>
                                <div class="col-sm-6">
                                        <input type="text" class="form-control"  id="timecredit"  value="0"/>
                                </div>
                                <div class="col-sm-2">
                                        <select class="form-control" id="unidadestimecredit">
                                            <option value="s" selected="selected">seconds</option>
                                            <option value="h">hours</option>
                                            <option value="d">days</option>
                                        </select>
                                </div>

                                <div class="col-sm-8 col-sm-offset-4">
                                    <div class="checkbox i-checks">
                                        <label> 
                                            <input type="checkbox" id="checktimecredit" />
                                            <i></i>
                                            <span id="labelchecktimecredit"> Max value </span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!-- VALID TILL -->
                            <div class="form-group"> 
                                <label class="control-label col-sm-4" id="labelformbillingvalidtill">Valid time</label>
                                <div class="col-sm-6">
                                        <input type="text" id="validtill" class="form-control" value="0"/>
                                </div>
                                <div class="col-sm-2">
                                    <select class="form-control" id="unidadesvalidtill">
                                        <option value="h" selected="selected">hours</option>
                                        <option value="d">days</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group"> 
                                <label class="control-label col-sm-4" id="labelformbillingvalidafterfirstuse">Valid time after first use</label>
                                <div class="col-sm-6">
                                        <input type="text" id="validafterfirstuse" class="form-control"  value="0" />
                                </div>
                                <div class="col-sm-2">
                                    <select class="form-control" id="unidadesvalidafterfirstuse">
                                        <option value="h" selected="selected">hours</option>
                                        <option value="d">days</option>
                                    </select>
                                </div>
                            </div>
                            
                            <!-- BANDWIDTH DOWN -->
                            <div class="form-group"> 
                                <label class="control-label col-sm-4" id="labelformbillingbandwidthdown">Bandwidth Down</label>
                                <div class="col-sm-6">
                                    <input type="text" id="bwdown" class="form-control" value="0" />
                                </div>
                                <div class="col-sm-2">
                                    <select class="form-control" id="unidadesbwdown">
                                        <option value="kbps" selected="selected">kbps</option>
                                        <option value="mbps">Mbps</option>
                                    </select>
                                </div>
                            </div>
                            <!-- BANDWIDTH UP -->
                            <div class="form-group"> 
                                <label class="control-label col-sm-4" id="labelformbillingbandwidthup">Bandwidth UP</label>
                                <div class="col-sm-6">
                                    <input type="text" id="bwup" class="form-control" value="0" />
                                </div>
                                <div class="col-sm-2">
                                    <select class="form-control" id="unidadesbwup">
                                        <option value="kbps" selected="selected">kbps</option>
                                        <option value="mbps">Mbps</option>
                                    </select>
                                </div>
                            </div>
                            <!-- VOLUME DOWN -->
                            <div class="form-group" id="groupvolumedown"> 
                                <label class="control-label col-sm-4" id="labelformbillingvolumedown">Volume Down per device</label>
                                <div class="col-sm-6">
                                        <input type="text" id="volumedown" class="form-control"  value="0"/>
                                    </div>
                                    <div class="col-sm-2">
                                        <select class="form-control" id="unidadesvolumedown">
                                            <option value="bytes" selected="selected">bytes</option>
                                            <option value="KB">KB</option>
                                            <option value="MB">MB</option>
                                            <option value="GB">GB</option>

                                        </select>
                                    </div>
                                    <div class="col-sm-8 col-sm-offset-4">
                                    <div class="checkbox i-checks">
                                        <label>
                                            <input type="checkbox" id="checkminvolume" />
                                            <i></i>
                                            <span id="labelcheckminvolume"> Max value </span> 
                                        </label>
                                    </div>
                                </div>
                            </div>
                            
                            <!-- VOLUME UP -->
                            <div class="form-group" id="groupvolumeup"> 
                            <label class="control-label col-sm-4" id="labelformbillingvolumeup">Volume Up per device</label>
                            <div class="col-sm-6">
                                    <input type="text" id="volumeup" class="form-control" value="0"  />
                                </div>
                                <div class="col-sm-2">
                                    <select class="form-control" id="unidadesvolumeup">
                                            <option value="bytes" selected="selected">bytes</option>
                                            <option value="KB">KB</option>
                                            <option value="MB">MB</option>
                                            <option value="GB">GB</option>
                                        </select>
                                </div>
                                <div class="col-sm-8 col-sm-offset-4">
                                <div class="checkbox i-checks">
                                    <label> 
                                        <input type="checkbox" id="checkmaxvolume" />
                                        <i></i>
                                        <span id="labelcheckmaxvolume"> Max value </span>
                                    </label>
                                </div>
                            </div>
                        </div>
                            <!-- VOLUME COMBINED -->
                            <div class="form-group" id="groupvolumecombined"> 
                            <label class="control-label col-sm-4" id="labelformbillingvolumecombined">Aggregate volume per device</label>
                            <div class="col-sm-6">
                                    <input type="text" id="volumecombined" class="form-control" value="0"  />
                                </div>
                                <div class="col-sm-2">
                                    <select class="form-control" id="unidadesvolumecombined">
                                            <option value="bytes" selected="selected">bytes</option>
                                            <option value="KB">KB</option>
                                            <option value="MB">MB</option>
                                            <option value="GB">GB</option>
                                        </select>
                                </div>
                                <div class="col-sm-8 col-sm-offset-4">
                                <div class="checkbox i-checks">
                                    <label> 
                                        <input type="checkbox" id="checkmaxvolumecombined" />
                                        <i></i>
                                        <span id="labelcheckmaxvolumecombined"> Max value </span>
                                    </label>
                                </div>
                            </div>
                        </div>

                            <div class="form-group"> <label class="control-label col-sm-4" id="labelformbillingmodule">Module</label><div class="col-sm-8"><select id="billingmodule" class="form-control" ></select></div></div>
                            <div class="form-group"> <label class="control-label col-sm-4" id="labelformbillingpriority">Priority / Group Policy</label><div class="col-sm-7"><select id="priority" class="form-control" ></select></div><div class="col-sm-1"><a href="#" id="newpriority" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></a></div></div>
                            <div class="form-group" id="filtercontentsgroup" style="display:none;"> <label class="control-label col-sm-4" id="labelformbillingfilter">Content filter</label><div class="col-sm-7"><select id="filterprofile" class="form-control" ></select></div><div class="col-sm-1"><a href="#" id="newprofile" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></a></div></div>
                            <div class="form-group"> <label class="control-label col-sm-4" id="labelformbillingorder">Order</label><div class="col-sm-8"><input type="number" id="order" class="form-control"  value="0"  /></div></div>
                            <div class="form-group"> <label class="control-label col-sm-4" id="labelformbillingurl">Landing Url</label><div class="col-sm-8"><input type="text" id="urllanding" class="form-control"  /></div></div>
                            <div class="form-group"> <label class="control-label col-sm-4" id="labelformbillingnextbillingtype">Next Access Type</label><div class="col-sm-8"><select id="nextbillingtype" class="form-control" ></select></div></div>
                            
                            <div class="form-group"> <label class="control-label col-sm-4" id="labelformbillingactive">Active</label><div class="col-sm-8"><input type="checkbox" id="enabled" checked="checked" /></div></div>
                            <div class="form-group"> <label class="control-label col-sm-4" id="labelformbillingfastticket">Fast Ticket default</label><div class="col-sm-8"><input type="checkbox" id="enabledfastticket" /></div></div>
                            <div class="form-group"> <label class="control-label col-sm-4" id="labelformbillingiot">Internet of Things</label><div class="col-sm-8"><input type="checkbox" id="iot" /></div></div>

                        </div>
                </div>
            </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5 id="headerbuttonsbillingtype">Actions</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <p>
                            <a href="#" class="btn btn-w-m btn-lg btn-primary btn-block" id="save"><i class="fa fa-save"></i> <span id="labelbuttonsavebillingtype"> Save</span></a>
                            <a href="#" role="button" class="btn btn-w-m btn-lg btn-default btn-block" id="back" data-toggle="modal"><i class="fa fa-backward"></i> <span id="labelbuttonbackbillingtype"> Back</span></a>

                        </p>
                    </div>
                </div>
            </div>

            <div class="modal inmodal fade" id="myModalMessage" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                            </div>
                            <div class="modal-body">
                                <div id="message">
                                    
                                </div>
                            </div>
                          <div class="modal-footer">
                            <a href="#" class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> <span id="labelbuttonclosebillingtype"> Close</span></a>
                          </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <input type="text" id="prevunidadtimecredit" hidden="hidden" />
    <input type="text" id="prevunidadvalidtill" hidden="hidden" />
    <input type="text" id="prevunidadvalidafterfirstuse" hidden="hidden" />
    <input type="text" id="prevunidadbwdw" hidden="hidden" />
    <input type="text" id="prevunidadbwup" hidden="hidden" />
    <input type="text" id="prevunidadvolumedw" hidden="hidden" />
    <input type="text" id="prevunidadvolumeup" hidden="hidden" />
    <input type="text" id="prevunidadvolumecombined" hidden="hidden" />
    <input type="text" id="isVolumeCombined" hidden="hidden" />

</asp:Content>
