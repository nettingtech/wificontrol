﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Site.Master" AutoEventWireup="true" CodeBehind="profile.aspx.cs" Inherits="Wifi360.profile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        $(document).ready(function () {
                var parameters = getParameters();

                var cookie = readCookie("wifi360-language");

                var dataString = 'action=fdsuser';
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    beforeSend: function () {
                        $("#myModalWait").modal('show');
                    },
                    complete: function () {
                        $("#myModalWait").modal('hide');
                    },
                    success: function (pReturn) {
                        var lReturn = JSON.parse(pReturn);
                        $("#UserName").html(lReturn.UserName);
                        $("#Name").html(lReturn.Name);
                        $("#Type").html(lReturn.type);
                        $("#Rol").html(lReturn.rol);

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#modal-loading").hide();
                        $("#searchResult").html(xhr.statusText)
                    }
                });


            $("#save").on("click", function (e) {
                e.preventDefault();

                var parameters = getParameters();
                var oldpassword = $("#oldpassword").val();
                var password = $("#password").val();
                var confirmpassword = $("#confirmpassword").val();
                if (password == confirmpassword) {
                    if ((ilegalCharacter(password) != false)) {

                        var data = {};
                        data["action"] = 'FDSUSERSAVE';
                        data["oldpassword"] = oldpassword;
                        data["newpassword"] = password;


                        $.ajax({
                            url: "handlers/SMIHandler.ashx",
                            type: "POST",
                            data: JSON.stringify(data),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            beforeSend: function () {
                                $("#myModalWait").modal('show');
                            },
                            complete: function () {
                                $("#myModalWait").modal('hide');
                            },
                            success: function (msg) {
                                if (msg.code == "OK") {
                                    $("#message").removeClass().addClass("alert alert-success");
                                    $("#message").html(GetMessage(msg.message));

                                }
                                else if (msg.code == "NO") {
                                    $("#message").removeClass().addClass("alert alert-warning");
                                    $("#message").html(GetMessage(msg.message));
                                }
                                else {
                                    $("#message").removeClass().addClass("alert alert-danger");
                                    $("#message").html(GetMessage(msg.message));
                                }
                                $("#myModalMessage").modal('show');
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                $("#message").removeClass().addClass("alert alert-danger");
                                $("#message").html(xhr.statusText);
                                $("#myModalMessage").modal('show');

                            }
                        });
                    }

                    else {
                        $("#message").addClass("alert alert-danger");
                        $("#message").html(GetMessage("0x0006"));
                        $("#myModalMessage").modal('show');

                    }
                }
                else {
                    $("#message").addClass("alert alert-danger");
                    $("#message").html(GetMessage("0x0033"));
                    $("#myModalMessage").modal('show');

                }
            });

            function getParameters() {
                var searchString = window.location.search.substring(1)
                  , params = searchString.split("&")
                  , hash = {}
                ;

                for (var i = 0; i < params.length; i++) {
                    var val = params[i].split("=");
                    hash[unescape(val[0])] = unescape(val[1]);
                }
                return hash;
            }

            function ilegalCharacter(string) {
                if (/^[a-zA-Z0-9_\.-@_#-]*$/.test(string) == false) {
                    return false;
                }
            };

            
    });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 id="headerpageprofile">Profile</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="default.aspx">Dashboard</a>
                </li>
                <li class="active">
                    <strong id="breadprofile">Profile</strong>
                </li>
            </ol>
           
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-9">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                    <h5 id="labelboxheaderuser">Profile<small> Details of profile</small></h5>
                </div>
                    <div class="ibox-content">
                     <div class="form-horizontal">
                        <div class="form-group"> <label class="control-label col-sm-4" id="labelformprofilename">Name</label><div class="col-sm-8"><label class="form-control" id="Name" ></label></div></div>
                        <div class="form-group"> <label class="control-label col-sm-4" id="labelformprofileusername">Username</label><div class="col-sm-8"><label class="form-control" id="UserName" > </label></div></div>
                        <div class="form-group"> <label class="control-label col-sm-4" id="labelformprofilerol">Rol</label><div class="col-sm-8"><label class="form-control" id="Rol"></label></div></div>
                         <div class="form-group"> <label class="control-label col-sm-4" id="labelformprofiletype">Type</label><div class="col-sm-8"><label class="form-control" id="Type"></label></div></div>
                    </div>
                   </div>
                
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                    <h5 id="labelboxheaderpassword">Password<small> Change Password</small></h5>
                </div>
                    <div class="ibox-content">
                     <div class="form-horizontal">
                         <div class="form-group"> <label class="control-label col-sm-4" id="labelformprofileoldpassword">Old Password</label><div class="col-sm-8"><input type="password" class="form-control" id="oldpassword" /></div></div>
                        <div class="form-group"> <label class="control-label col-sm-4" id="labelformprofilenewpassword">New password</label><div class="col-sm-8"><input type="password" class="form-control" id="password" /></div></div>
                        <div class="form-group"> <label class="control-label col-sm-4" id="labelformprofilenewpasswordconfirm">Confirm new password</label><div class="col-sm-8"><input type="password" class="form-control" id="confirmpassword" /></div></div>
                    </div>
                   </div>
                
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5 id="headerboxactionsuser">Actions</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <p>
                            <a href="#" class="btn btn-w-m btn-lg btn-primary btn-block" id="save"><i class="fa fa-save"></i> <span id="labelbuttonsaveuser"> Save</span></a>
                            <a href="default.aspx" role="button" class="btn btn-w-m btn-lg btn-default btn-block" id="back"><i class="fa fa-backward"></i> <span id="labelbuttonbackuser"> Back</span></a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="modal inmodal fade" id="myModalMessage" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                            </div>
                            <div class="modal-body">
                                <div id="message">
                                    
                                </div>
                            </div>
                          <div class="modal-footer">
                            <a href="#" class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Volver</a>
                          </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</asp:Content>
