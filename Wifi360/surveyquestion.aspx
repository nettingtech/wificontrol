﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Site.Master" AutoEventWireup="true" CodeBehind="surveyquestion.aspx.cs" Inherits="Wifi360.surveyquestion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <script type="text/javascript">
            $(document).ready(function (e) {

                function readCookie(name) {
                    var nameEQ = name + "=";
                    var ca = document.cookie.split(';');
                    for (var i = 0; i < ca.length; i++) {
                        var c = ca[i];
                        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
                    }
                    return null;
                };

                function getParameters() {
                    var searchString = window.location.search.substring(1)
                      , params = searchString.split("&")
                      , hash = {}
                    ;

                    for (var i = 0; i < params.length; i++) {
                        var val = params[i].split("=");
                        hash[unescape(val[0])] = unescape(val[1]);
                    }
                    return hash;
                }

                var parameters = getParameters();

                if (parameters.idlocation == undefined)
                    $("#breadpagesurveysetting").attr("href","surveysettings.aspx");
                else
                    $("#breadpagesurveysetting").attr("href", "location.aspx?id=" + parameters.idlocation);

                var dataString2 = 'action=languages';
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataString2,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    beforeSend: function () {
                        $("#myModalWait").modal('show');
                    },
                    complete: function () {
                        $("#myModalWait").modal('hide');
                    },
                    success: function (pReturn) {
                        var lReturn = JSON.parse(pReturn);
                        bindSelect('language', 'IdLanguage', 'Name', lReturn.list);

                        var parameters = getParameters();

                        if (parameters.id != undefined) {

                            var dataString = 'action=surveyquestion&id=' + parameters.id;
                            $.ajax({
                                url: "handlers/SMIHandler.ashx",
                                data: dataString,
                                contentType: "application/json; charset=utf-8",
                                dataType: "text",
                                success: function (pReturn) {
                                    var lReturn = JSON.parse(pReturn);
                                    $("#language").val(lReturn.IdLanguage);
                                    $("#type").val(lReturn.Type);
                                    $("#question").val(lReturn.Question);
                                    $("#values").val(lReturn.Values);
                                    $("#order").val(lReturn.Order);
                                    if (lReturn.Type == "select")
                                        $("#values").removeAttr("disabled");

                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    $("#modal-loading").hide();
                                    $("#searchResult").html(xhr.statusText)
                                }
                            });
                        }
                        else {
                            $("#delete").hide();
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#modal-loading").hide();
                        $("#searchResult").html(xhr.statusText)
                    }
                });

               

                $("#type").change(function (e) {

                    var type = $("#type").val();

                    if (type == "select")
                        $("#values").removeAttr("disabled");
                    else
                        $("#values").attr("disabled", "disabled");

                });


                function bindSelect(id, valueMember, displayMember, source) {
                    $("#" + id).find('option').remove().end();
                    var cookie = readCookie("wifi360-language");
                    if (cookie != null) {
                        switch (cookie) {
                            case "es": $("#" + id).append('<option value="0">Seleccionar</option>'); break;
                            default: $("#" + id).append('<option value="0">Select</option>'); break;
                        }
                    }
                    else
                        $("#" + id).append('<option value="0">Select</option>');
                    $.each(source, function (index, item) {
                        $("#" + id).append('<option value="' + item[valueMember] + '"> ' + item[displayMember] + '</option>');
                    });
                };

                $("#save").click(function (e) {
                    e.preventDefault();

                    var parameters = getParameters();
                    var dataString = 'action=savesurveyquestion&id=' + parameters.id + '&language=' + $("#language").val() + '&question=' + $("#question").val() + '&type=' + $("#type").val() + '&order=' + $("#order").val() + '&values=' + $("#values").val();
                    if (parameters.idlocation != undefined)
                        dataString += "&idlocation=" + parameters.idlocation;
                    $.ajax({
                        url: "handlers/SMIHandler.ashx",
                        data: dataString,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {
                            $("#myModalWait").modal('show');
                        },
                        complete: function () {
                            $("#myModalWait").modal('hide');
                        },
                        success: function (msg) {
                            if (msg.code == 'OK') {
                                $("#message").removeClass().addClass("alert alert-success");
                                $("#message").html(GetMessage(msg.message));
                            }
                            else {
                                $("#message").removeClass().addClass("alert alert-danger");
                                $("#message").html(GetMessage(msg.message));
                            }

                            $("#myModalMessage").modal('show');
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#message").removeClass().addClass("alert alert-error");
                            $("#message").html(xhr.statusText);
                            $("#message").show();
                            $("#modal-loading").hide();

                        }
                    });
                });

                $("#delete").click(function (e) {
                    e.preventDefault();
                    e.preventDefault();

                    var parameters = getParameters();
                    var dataString = 'action=deletesurveyquestion&id=' + parameters.id;
                    $.ajax({
                        url: "handlers/SMIHandler.ashx",
                        data: dataString,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {
                            $("#myModalWait").modal('show');
                        },
                        complete: function () {
                            $("#myModalWait").modal('hide');
                        },
                        success: function (msg) {
                            if (msg.code == 'OK') {
                                $("#message").removeClass().addClass("alert alert-success");
                                $("#message").html(GetMessage(msg.message));
                            }
                            else {
                                $("#message").removeClass().addClass("alert alert-danger");
                                $("#message").html(GetMessage(msg.message));
                            }

                            $("#myModalMessage").modal('show');
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#message").removeClass().addClass("alert alert-error");
                            $("#message").html(xhr.statusText);
                            $("#message").show();
                            $("#modal-loading").hide();

                        }
                    });
                });

                $("#back").click(function (e) {
                    e.preventDefault();
                    parameters = getParameters();

                    if (parameters.idlocation == undefined)
                        window.location = "surveysettings.aspx";
                    else                        
                        window.location = "location.aspx?id=" + parameters.idlocation;

                    if (parameters.idsite != undefined){
                        window.location = "surveysettings.aspx?idsite=" + parameters.idsite + "&return=" + parameters.return;
                        if (parameters.idlocation != undefined)
                            window.location = "surveysettings.aspx?idsite=" + parameters.idsite + "&return=" + parameters.return + "&idlocation=" + parameters.idlocation;
                    }
                });
            });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
         <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 id="headerpagelocationsetting">Settings</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="default.aspx">Dashboard</a>
                </li>
                <li id="breadpagefatherlocationsetting">
                    <a>Settings</a>
                </li>
                <li>
                    <a href="#" id="breadpagesurveysetting">Surveys</a>
                </li>
                <li class="active">
                    <strong id="breadpagesurveyquestion">Question settings</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-9">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                    <h5 id="labelboxheaderquestion">Question settings<small> Details of question settings</small></h5>
                </div>
                    <div class="ibox-content">
                     <div class="form-horizontal">
                    <div class="form-group"> <label class="control-label col-sm-4" id="labelformquestionlanguage">Language</label><div class="col-sm-8"><select id="language" class="form-control" ></select></div></div>
                    <div class="form-group"> <label class="control-label col-sm-4" id="labelformquestionquestion">Question</label><div class="col-sm-8"><input type="text" class="form-control" id="question"/></div></div>
                         <div class="form-group"> <label class="control-label col-sm-4" id="labelformquestiontype">Type</label><div class="col-sm-8"><select id="type" class="form-control" ><option value="0">Select</option><option value="text">Text</option><option value="select">Select</option></select></div></div>
                    <div class="form-group"> <label class="control-label col-sm-4" id="labelformquestionvalues">Values</label><div class="col-sm-8"><input type="text" class="form-control" id="values" disabled="disabled" /></div></div>
                         <div class="form-group"> <label class="control-label col-sm-4" id="labelformquestionorder">Order</label><div class="col-sm-8"><input type="number" id="order" class="form-control" /></div></div>


                </div>
                </div>
            </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5 id="labelboxheaderactionquestion">Accions over the question</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <p>
                            <a href="#" class="btn btn-w-m btn-lg btn-primary btn-block" id="save"><i class="fa fa-save"></i> <span id="labelformlocationsettingsbuttonsave">Save</span></a>
                            <a href="#" class="btn btn-w-m btn-lg btn-danger btn-block" id="delete"><i class="fa fa-trash"></i> <span id="labelformquestionsettingsbuttondelete">Delete</span></a>
                            <a href="#" role="button" class="btn btn-w-m btn-lg btn-default btn-block" id="back" data-toggle="modal"><i class="fa fa-backward"></i> <span id="labelformlocationsettingsbuttonback">Back</span></a>

                        </p>
                    </div>
                </div>
            </div>
            <div class="modal inmodal fade" id="myModalMessage" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                            </div>
                            <div class="modal-body">
                                <div id="message">
                                    
                                </div>
                            </div>
                          <div class="modal-footer">
                            <a href="#" class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Volver</a>
                          </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</asp:Content>
