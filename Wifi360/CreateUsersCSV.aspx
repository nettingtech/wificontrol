﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Site.Master" AutoEventWireup="true" CodeBehind="CreateUsersCSV.aspx.cs" Inherits="Wifi360.CreateUsersCSV" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <link href="css/calendar.css" rel="stylesheet" />
    <script src="js/calendar.full.min.js"></script>
    <script src="js/moment.js"></script>
    <script type="text/javascript">

        function readCookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        };

        $(document).ready(function () {
            var cookie = readCookie("wifi360-language");
            if (cookie != null) {
                switch (cookie) {
                    case "es": var calendar = new CalendarPopup('#start', {dayOfWeekStart: 1, showMask: false, locale: 'es', showWeekIndex: true, format: 'DD/MM/YYYY HH:mm', i18n: { es: { "Chose": "Elegir", "Close": "Cerrar", "Time": "Hora" } } });
                        var calendar2 = new CalendarPopup('#end', {dayOfWeekStart: 1, showMask: false, locale: 'es', showWeekIndex: true, format: 'DD/MM/YYYY HH:mm', i18n: { es: { "Chose": "Elegir", "Close": "Cerrar", "Time": "Hora" } } }); break;
                    default: var calendar = new CalendarPopup('#start', { showMask: false, showWeekIndex: true, format: 'DD/MM/YYYY HH:mm' });
                        var calendar2 = new CalendarPopup('#end', { showMask: false, showWeekIndex: true, format: 'DD/MM/YYYY HH:mm' }); break;
                }
            }
            else {
                var calendar = new CalendarPopup('#start', { dayOfWeekStart: 1, showMask: false, showWeekIndex: true, format: 'DD/MM/YYYY HH:mm' });
                var calendar2 = new CalendarPopup('#end', {dayOfWeekStart: 1, showMask: false, showWeekIndex: true, format: 'DD/MM/YYYY HH:mm' });
            }

            $(".calendar-popup-input-wrapper").attr("style", "display:inherit;")

            var dataString4 = 'action=sitesetting';
            $.ajax({
                url: "handlers/SMIHandler.ashx",
                data: dataString4,
                contentType: "application/json; charset=utf-8",
                dataType: "text",
                beforeSend: function () {
                    $("#myModalWait").modal('show');
                },
                success: function (pReturn) {
                    var lReturn = JSON.parse(pReturn);

                    if (lReturn.EnabledFasTicket == true)
                        $("#enabledfastticket").attr('checked', true);
                    if (lReturn.PrintLogo == true)
                        $("#logocheck").attr('checked', true);
                    if (lReturn.PrintVolume == true)
                        $("#volumecheck").attr('checked', true);
                    if (lReturn.PrintSpeed == true)
                        $("#speedcheck").attr('checked', true);


                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#modal-loading").hide();
                    $("#searchResult").html(xhr.statusText)
                }
            });

            var dataString2 = 'action=RoomsNoFilter';
            $.ajax({
                url: "handlers/SMIHandler.ashx",
                data: dataString2,
                contentType: "application/json; charset=utf-8",
                dataType: "text",
                success: function (pReturn) {
                    var lReturn = JSON.parse(pReturn);
                    var cookie_l = readCookie("wifi360-language");
                    if (cookie_l != null) {
                        switch (cookie_l) {
                            case "es": bindSelect("Seleccione habitación", "rooms", "IdRoom", "Name", lReturn.list); break;
                            default: bindSelect("Selec Room", "rooms", "IdRoom", "Name", lReturn.list); break;
                        }
                    }
                    else
                        bindSelect("Selec Room", "rooms", "IdRoom", "Name", lReturn.list);

                    
                    if (lReturn.Default != 0) {
                        $("#rooms").val(lReturn.Default);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#message").removeClass().addClass("alert alert-error");
                    $("#message").html(xhr.statusText);
                    $("#message").show();
                    $("#modal-loading").hide();
                }
            });

            var cookielocation = readCookie("location");

            var dataString = 'action=LocationsNoAllZones';
            $.ajax({
                url: "handlers/SMIHandler.ashx",
                data: dataString,
                contentType: "application/json; charset=utf-8",
                dataType: "text",
                complete: function () {
                    $("#myModalWait").modal('hide');
                },
                success: function (pReturn) {
                    var lReturn = JSON.parse(pReturn);
                    var cookie = readCookie("wifi360-language");
                    if (cookie != null) {
                        if (cookie == "es") {
                            bindSelect("seleccionar localización", "location", "Idlocation", "Description", lReturn.list);
                        }
                        else
                            bindSelect("select location", "location", "Idlocation", "Description", lReturn.list);
                    }
                    else
                        bindSelect("select location", "location", "Idlocation", "Description", lReturn.list);

                        if (lReturn.Default != 0) {
                            $("#location").val(lReturn.Default);

                        var dataString = 'action=billingTypesLocation&l=' + lReturn.Default;
                        $.ajax({
                            url: "handlers/SMIHandler.ashx",
                            data: dataString,
                            contentType: "application/json; charset=utf-8",
                            dataType: "text",
                            success: function (pReturn) {
                                var lReturn2 = JSON.parse(pReturn);
                                var cookie = readCookie("wifi360-language");
                                if (cookie != null) {
                                    if (cookie == "es") {
                                        bindSelect("seleccionar tipo de acceso", "billingTypes", "IdBillingType", "Description", lReturn2.list);
                                    }
                                    else
                                        bindSelect("select access type", "billingTypes", "IdBillingType", "Description", lReturn2.list);
                                }
                                else
                                    bindSelect("select access type", "billingTypes", "IdBillingType", "Description", lReturn2.list);
                                if (lReturn2.Default != 0) {
                                    $("#billingTypes").val(lReturn2.Default);
                                }
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                $("#message").removeClass().addClass("alert alert-error");
                                $("#message").html(xhr.statusText);
                                $("#message").show();
                                $("#modal-loading").hide();
                            }
                        });
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#message").removeClass().addClass("alert alert-error");
                    $("#message").html(xhr.statusText);
                    $("#message").show();
                    $("#modal-loading").hide();
                }
            });

            $("#location").change(function (e) {

                e.preventDefault();

                var location = $("#location").val();

                if (location == "0") {
                    $('#billingTypes').children().remove().end();
                }
                else {

                    var dataString = 'action=billingTypesLocation&l=' + location;
                    $.ajax({
                        url: "handlers/SMIHandler.ashx",
                        data: dataString,
                        contentType: "application/json; charset=utf-8",
                        dataType: "text",
                        beforeSend: function () {
                            $("#myModalWait").modal('show');
                        },
                        complete: function () {
                            $("#myModalWait").modal('hide');
                        },
                        success: function (pReturn) {
                            var lReturn = JSON.parse(pReturn);
                            var cookie = readCookie("wifi360-language");
                            if (cookie != null) {
                                if (cookie == "es") {
                                    bindSelect("seleccionar tipo de acceso", "billingTypes", "IdBillingType", "Description", lReturn.list);
                                }
                                else
                                    bindSelect("select access type", "billingTypes", "IdBillingType", "Description", lReturn.list);
                            }
                            else
                                bindSelect("Select access type", "billingTypes", "IdBillingType", "Description", lReturn.list);

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#message").removeClass().addClass("alert alert-error");
                            $("#message").html(xhr.statusText);
                            $("#message").show();
                            $("#modal-loading").hide();
                        }
                    });
                }

            });

            function BuyTicket() {

                var room = $("#rooms").val();
                var billingType = $("#billingTypes").val();
                var start = $("#start").val();
                var end = $("#end").val();
                var comment = $("#comment").val();

                if (room == 0) {
                    $("#message").html("La habitación no ha sido seleccionada");
                    $("#myModal").modal('show');

                }
                else if ((billingType == 0) || (billingType == null)) {
                    $("#message").html("No ha seleccionado tipo de acceso");
                    $("#myModal").modal('show');
                }
                else {

                    var fileUpload = $("#fileUpload").get(0);
                    var files = fileUpload.files;

                    var data = new FormData();
                    data.append('action','CREATETICKETMASIVE');
                    data.append('idbillingtype',billingType);
                    data.append('idroom',room);
                    data.append('start',start);
                    data.append('end',end);
                    data.append('comment', comment);
                    for (var i = 0; i < files.length; i++) {
                        data.append(files[i].name, files[i]);
                    }

                    $.ajax({
                        url: "handlers/UploadCSVHandler.ashx",
                        type: "POST",
                        data: data,
                        contentType: false,
                        processData: false,
                        //dataType: "json",
                        beforeSend: function () {
                            $("#myModalWait").modal('show');
                        },
                        complete: function () {
                            $("#myModalWait").modal('hide');
                        },
                        success: function (msg) {
                            if (msg == "NO") {
                                $("#message").removeClass().addClass("alert alert-success").html("Usuarios creados correctamente");
                                $("#myModal").modal('show');
                            }
                            else {
                                $("#message").removeClass().addClass("alert alert-danger").html(msg);
                                $("#myModal").modal('show');
                            }

                                
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#message").removeClass().addClass("alert alert-danger");
                            $("#message").html(xhr.statusText);
                            $("#myModal").modal('show');

                        }
                    });
                }

            };

            $("#create").click(function (e) {
                e.preventDefault();
                var start = $("#start").val();
                var end = $("#end").val();
                var s = moment(start, "DD/MM/YYYY HH:mm");
                var e = moment(end, "DD/MM/YYYY HH:mm");
                if (
                    ($("#start").val() != '' && $("#end").val() != '' && s.toDate() < e.toDate())
                    || (($("#start").val() != '') && ($("#end").val() == ''))
                    || (($("#start").val() == '') && ($("#end").val() != ''))
                    || (($("#start").val() == '') && ($("#end").val() == ''))) {
                        BuyTicket();
                }
                else {
                    $("#message").html("Las fechas no tienen el formato correcto");
                    $("#myModal").modal('show');
                }
            });
        });

        function bindSelect(firstItem, id, valueMember, displayMember, source) {
            $("#" + id).find('option').remove().end();
            $("#" + id).append('<option value="0">' + firstItem + '</option>');
            $.each(source, function (index, item) {
                $("#" + id).append('<option value="' + item[valueMember] + '"> ' + item[displayMember] + '</option>');
            });
        };

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 id="headerpagenewticket">New Ticket</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="default.aspx">Dashboard</a>
                </li>
                <li>
                    <a>Tickets</a>
                </li>
                <li class="active" id="breadlabelnewticket">
                    <strong>New Ticket</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-7 col-md-7">
                <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5 id="labelboxheader">New 
                        <small> Select parameters</small></h5>
                </div>
                <div class="ibox-content">
                        <div role="form">
                            <div class="form-group"><label id="labelformroom">Room </label> <select id="rooms" class="form-control"></select></div>
                            <div class="form-group"><label id="labelformlocation">Location</label> <select id="location" class="form-control"></select></div>
                            <div class="form-group"><label id="labelformbillingtype">Access type</label>  <select id="billingTypes" class="form-control"></select></div>
                            <div class="form-group"><label id="labelformfile">File</label>  <input type="file" id="fileUpload" class="form-control" accept=".csv" /><br /><small>Formato: tres columnas sin encabezado, 1ª nombre de usuario, 2ª contraseña, 3ª comentario (optativo)</small></div>
                        </div>
                    </div>
                </div>

                <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5 id="labelboxadvancedparameters">Advanced parameters<small> Select advanced parameters to create new tickets</small></h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                        </div>
                </div>
                <div class="ibox-content" style="display:none">
                        <div role="form">
                            <div class="form-group"><label id="labelformstart">Valid since</label> <input type="text" id="start" class="form-control"/></div>
                            <div class="form-group"><label id="labelformend">Valid till</label> <input type="text" id="end" class="form-control"/></div>
                            <div class="form-group"><label id="labelComment">Comment</label> <textarea id="comment" class="form-control" rows="3"></textarea></div>
                        </div>
                    </div>
                </div>

                <div class="ibox float-e-margins">
                
                <div class="ibox-content">
                    <div class="text-center">
                        <a class="btn btn-block  btn-primary" href="#" id="create"><i class="fa fa-ticket"></i><span id="buttoncreate">Create new</span> </a>
                    </div>
               </div>
            </div>
            </div>
            <div class="col-lg-5 col-md-5">
                <div class="widget navy-bg p-lg text-center" id="ticket" style="display:none;">
                    <div class="m-b-md">
                        <i class="fa fa-ticket fa-4x"></i>
                        <hr />
                        <h3 class="m-xs" id="labelprintusername">Username</h3>
                        <h1 id="resultusername">USERNAME</h1>
                        <h3 class="font-bold no-margins" id="labelprintpassword">
                            Password
                        </h3>
                        <h1 id="resultpassword">PASSWORD</h1>
                    </div>
                </div>

                
                <div class="ibox float-e-margins" id="printpanel" style="display:none;">
                <div class="ibox-title">
                    <h5 id="headerprintparameters">Print parameters</h5>
                </div>
                <div class="ibox-content">
                        <div role="form">
                            <div class="checkbox i-checks"><label> <input type="checkbox" id="logocheck" /><i></i> <span id="printlogo"> Print Logo </span></label></div>
                            <div class="checkbox i-checks"><label> <input type="checkbox" id="speedcheck" /><i></i> <span id="printbandwidth">Print bandwidth</span></label></div>
                            <div class="checkbox i-checks"><label> <input type="checkbox" id="volumecheck" /><i></i> <span id="printvolume">Print volume </span></label></div>
                            <div class="text-center">
                                <a class="btn btn-block btn-primary" href="#" id="print"><i class="fa fa-print"></i> <span id="buttonprint">Print</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal inmodal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only" id="buttonclose">Close</span></button>
                                <h4 class="modal-title">Error</h4>
                            </div>
                            <div class="modal-body">
                                <p class="alert alert-danger" id="message"></p>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

           

            <div id="fastticket" style="display:none"></div>
            <div id="fastticketlogo" style="display:none"></div>
            <div id="fastticketlogo2" style="display:none"></div>
            <div id="fastticketspeed" style="display:none"></div>
            <div id="fastticketvolume" style="display:none"></div>
    </div>
</div>
</asp:Content>
