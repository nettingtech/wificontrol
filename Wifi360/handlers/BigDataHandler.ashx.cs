﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Text;

using Splunk.Client;
using System.Data;
using Wifi360.Data.Model;
using Wifi360.Data.Controllers;
using System.IO;
using Wifi360.response;
using System.Web.Script.Serialization;
using System.Configuration;

namespace Wifi360.handlers
{
    /// <summary>
    /// Descripción breve de BigDataHandler
    /// </summary>
    public class BigDataHandler : IHttpHandler
    {
        #region metodos privados
        private static SitesGMT GetSiteGMt(out Locations2 location, out Hotels hotel, out FDSGroups group, HttpContext context)
        {
            SitesGMT siteGMT = null;
            location = null;
            hotel = null;
            group = null;
            try
            {
                if (context.Request.Cookies["location"] != null)
                {
                    location = BillingController.GetLocation(Int32.Parse(context.Request.Cookies["location"].Value));
                    hotel = BillingController.GetHotel(location.IdHotel);
                    siteGMT = BillingController.SiteGMTSitebyLocation(location.IdLocation);
                    if (siteGMT.Id.Equals(0))
                        siteGMT = BillingController.SiteGMTSitebySite(location.IdHotel);
                }
                else if (context.Request.Cookies["site"] != null)
                {
                    hotel = BillingController.GetHotel(Int32.Parse(context.Request.Cookies["site"].Value));
                    siteGMT = BillingController.SiteGMTSitebySite(hotel.IdHotel);
                }
                else
                {
                    group = BillingController.GetGroup(Int32.Parse(context.Request.Cookies["group"].Value));
                    siteGMT = BillingController.SiteGMTSitebyGroup(group.IdGroup);
                }


                return siteGMT;
            }
            catch (Exception ex)
            {
                return null;
            }


        }

        private static string ListadoHoteles(FDSGroups group)
        {
            if (group != null)
            {
                List<Hotels> hoteles = new List<Hotels>();
                List<FDSGroupsMembers> members = BillingController.SelectFDSGroupsMembers(group.IdGroup);
                for (int i = 0; i < members.Count; i++)
                {
                    switch (members[i].IdMemberType)
                    {
                        //tipo grupo
                        case 1:
                            List<FDSGroupsMembers> auxs = BillingController.SelectFDSGroupsMembers(members[i].IdMember);
                            foreach (FDSGroupsMembers aux in auxs)
                                members.Add(aux);
                            break;
                        //tipo site
                        case 2:
                            hoteles.Add(BillingController.GetHotel(members[i].IdMember));
                            break;
                        //tipo location
                        case 3: break;

                    }
                }

                string listadosHoteles = string.Empty;
                foreach (Hotels h in hoteles)
                    listadosHoteles += string.Format("{0},", h.IdHotel);
                listadosHoteles = listadosHoteles.Substring(0, listadosHoteles.Length - 1);
                return listadosHoteles;
            }
            else
                return string.Empty;
        }

        #endregion

        public void ProcessRequest(HttpContext context)
        {
            if (string.IsNullOrEmpty(context.Request.Params["action"])) ProcessEmtpy(context);
            switch (context.Request.Params["action"].ToUpper())
            {
                case "SNMP_MIKROTIK": snmp_Mikrotik(context); break;
                case "CLOSEDSESSIONSTIMELIVE": closedSessionsTimeLive(context); break;
                case "CLOSEDSESSIONSTIMEINTERVAL": closedSessionsTimeInterval(context); break;
                case "CONNECTIONBYMODULELIVE": connectionByModuleLive(context); break;
                case "CONNECTIONBYMODULEINTERVAL": connectionByModuleInterval(context); break;
                case "USERSLOCATION": UsersLocation(context); break;
                case "USERSONLINEMAP": UsersOnlineMap(context); break;
                case "TOPOLOGY": topology(context); break;
                case "ONLINEDEVICE": onlineDevice(context); break;
                case "ONLINEUSERSBYSITEINTERVAL": OnlineUsersBySiteInterval(context); break;
                case "ONLINEUSERSBYSITELIVE": OnlineUsersBySiteLive(context); break;
                case "VALUESLIVE": ValuesLive(context); break;
                case "VALUESINTERVAL": ValuesInterval(context); break;
                case "USERSPERHOURS": UsersPerHours(context); break;
                case "USERSBYZONELIVE": UsersByZoneLive(context); break;
                case "USERSBYZONEINTERVAL": UsersByZoneInterval(context); break;
                case "USERSBYBILLINGTYPELIVE": UsersByBillingTypeLive(context); break;
                case "USERSBYBILLINGTYPEINTERVAL": UsersByBillingTypeInterval(context); break;

            }
        }

        private async void snmp_Mikrotik(HttpContext context)
        {
            string json = string.Empty;
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback += (sender1, certificate, chain, sslPolicyErrors) =>
                {
                    return true;
                };

                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;


                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                var service = new Service(Scheme.Https, ConfigurationManager.AppSettings["SplunkUrl"].ToString(), Int32.Parse(ConfigurationManager.AppSettings["SplunkPort"].ToString()));

                await service.LogOnAsync(ConfigurationManager.AppSettings["SplunkUser"].ToString(), ConfigurationManager.AppSettings["SplunkPass"].ToString());

                var query6 =
                      "search index=fbd source=\"snmp://snmp_Mikrotik\" "
                      + "| eval cpu_temp = cpu_temp / 10 "
                      + "| eval equipo_temp = equipo_temp / 10 "
                      + "| timechart span=1m max(cpu_temp) as \"CPU_temp\",max(equipo_temp) as \"Mikrotik_temp\",max(cpu_usage) as \"CPU_usage\"";

                var args6 = new JobArgs
                {

                    EarliestTime = "-60m@m",
                    LatestTime = "now"
                };

                string file = context.Server.MapPath("~/resources/docs/output.tsv");
                using (StreamWriter outputfile = new StreamWriter(file))
                {
                    string headfile = "date\tCPU Temp\tMikrotik Temp\tCPU Usage";
                    outputfile.WriteLine(headfile);

                    // visualización de los datos
                    using (SearchResultStream resultStream = await service.SearchOneShotAsync(query6, args: args6))
                    {
                        foreach (SearchResult result in resultStream)
                        {
                            DateTime time = DateTime.Parse(result.GetValue("_time"));
                            string line = string.Format("{0}\t{1}\t{2}\t{3}", time.ToString("yyyyMMddHHmmss"),
                                result.GetValue("CPU_temp"), result.GetValue("Mikrotik_temp"), result.GetValue("CPU_usage"));
                            outputfile.WriteLine(line);
                        }
                    }
                }

                json = "{\"code\":\"OK\",\"message\":\""+ "../resources/docs/output.tsv\"}";
            }
            catch (Exception ex)
            {
                json = "{\"code\":\"ERROR\",\"message\":\"Se ha producido un error en el envio. Inténtelo más tarde.\"}";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();

        }

        private async void closedSessionsTimeLive(HttpContext context)
        {
            string json = string.Empty;
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback += (sender1, certificate, chain, sslPolicyErrors) =>
                {
                    return true;
                };

                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                var service = new Service(Scheme.Https, ConfigurationManager.AppSettings["SplunkUrl"].ToString(), Int32.Parse(ConfigurationManager.AppSettings["SplunkPort"].ToString()));

                await service.LogOnAsync(ConfigurationManager.AppSettings["SplunkUser"].ToString(), ConfigurationManager.AppSettings["SplunkPass"].ToString());

                Locations2 location = null;
                Hotels hotel = null;
                FDSGroups group = null;
                SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group, context);
                string consulta = string.Empty;
                var query6 = "";
                if (group != null)
                {
                    string listadoHoteles = ListadoHoteles(group);

                    string[] ids = listadoHoteles.Split(',');
                    foreach (string id in ids)
                    {
                        consulta += string.Format("IdHotel={0} OR ", id);
                    }
                    consulta = consulta.Substring(0, consulta.Length - 4);
                    query6 = "| savedsearch sd_conn_duration_idhotel_realtime_string IdHotel_String=\"" + consulta + "\"";
                }
                else if (location != null)
                {
                    FDSLocationsIdentifier identifier = BillingController.GetFDSLocationIdentifier(location.IdLocation);
                    consulta = identifier.Identifier;
                    query6 = "| savedsearch sd_conn_duration_location_realtime location='" + consulta + "'";
                    
                }
                else
                {
                    consulta = string.Format("IdHotel = {0}", hotel.IdHotel);
                    query6 = "| savedsearch sd_conn_duration_idhotel_realtime_string IdHotel_String=\"" + consulta + "\"";
                }


                var args6 = new JobArgs
                {

                };

                json = "{\"message\": \"ok\", \"items\": [";
                // visualización de los datos
                using (SearchResultStream resultStream = await service.SearchOneShotAsync(query6, args: args6))
                {

                    foreach (SearchResult result in resultStream)
                    {
                        json += "{\"label\": \"" + result.GetValue("myrange") + "\", \"value\" : " + result.GetValue("Usuarios") + "},";
                    }
                }
                json = json.Substring(0, json.Length - 1);
                json += "]}";
            }
            catch (Exception ex)
            {
                ;
            }

            // json = "{\"code\":\"OK\",\"message\":\"\"}";
            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();

        }

        private async void closedSessionsTimeInterval(HttpContext context)
        {
            string json = string.Empty;
            ListPieResult response = new ListPieResult();
            try
            {
                string start = (context.Request["start"] == null ? string.Empty : context.Request["start"].ToString());
                string end = (context.Request["end"] == null ? string.Empty : context.Request["end"].ToString());

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback += (sender1, certificate, chain, sslPolicyErrors) =>
                {
                    return true;
                };

                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;


                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                var service = new Service(Scheme.Https, ConfigurationManager.AppSettings["SplunkUrl"].ToString(), Int32.Parse(ConfigurationManager.AppSettings["SplunkPort"].ToString()));

                await service.LogOnAsync(ConfigurationManager.AppSettings["SplunkUser"].ToString(), ConfigurationManager.AppSettings["SplunkPass"].ToString());

                Locations2 location = null;
                Hotels hotel = null;
                FDSGroups group = null;
                SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group, context);
                string consulta = string.Empty;
                if (group != null)
                {
                    string listadoHoteles = ListadoHoteles(group);

                    string[] ids = listadoHoteles.Split(',');
                    foreach (string id in ids)
                    {
                        consulta += string.Format("IdHotel = {0} OR ", id);
                    }
                    consulta = consulta.Substring(0, consulta.Length - 4);
                }
                else
                    consulta = string.Format("IdHotel = {0}", hotel.IdHotel);

                string timefilter = "earliest="+ DateTime.Now.AddDays(-7).ToUniversalTime().ToString("MM/dd/yyyy:HH:mm:ss") + " latest=" + DateTime.Now.ToUniversalTime().ToString("MM/dd/yyyy:HH:mm:ss");
                if (!string.IsNullOrEmpty(start) && !string.IsNullOrEmpty(end))
                    timefilter = "earliest=" + DateTime.Parse(start).AddSeconds(-siteGMT.gmtoffset).ToString("MM/dd/yyyy:HH:mm:ss") + " latest=" + DateTime.Parse(end).AddSeconds(-siteGMT.gmtoffset).ToString("MM/dd/yyyy:HH:mm:ss");

                var query6 = "| savedsearch sd_conn_duration_idhotel_interval_string IdHotel_String=\"" + consulta + "\" " + timefilter;

                var args6 = new JobArgs {};

                json = "{\"message\": \"ok\", \"items\": [";
                // visualización de los datos
                using (SearchResultStream resultStream = await service.SearchOneShotAsync(query6, args: args6))
                {
                    response.list = new List<PieResult>();
                    foreach (SearchResult result in resultStream)
                    {
                        json += "{\"label\": \"" + result.GetValue("myrange") + "\", \"value\" : " + result.GetValue("Usuarios") + "},";
                    }
                }
                json = json.Substring(0, json.Length - 1);
                json += "]}";
            }
            catch (Exception ex)
            {
            }

           // json = "{\"code\":\"OK\",\"message\":\"\"}";
            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();

        }

        private async void connectionByModuleLive(HttpContext context)
        {
            string json = string.Empty;
            ListPieResult response = new ListPieResult();
            try
            {

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback += (sender1, certificate, chain, sslPolicyErrors) =>
                {
                    return true;
                };

                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                var service = new Service(Scheme.Https, ConfigurationManager.AppSettings["SplunkUrl"].ToString(), Int32.Parse(ConfigurationManager.AppSettings["SplunkPort"].ToString()));

                await service.LogOnAsync(ConfigurationManager.AppSettings["SplunkUser"].ToString(), ConfigurationManager.AppSettings["SplunkPass"].ToString());

                Locations2 location = null;
                Hotels hotel = null;
                FDSGroups group = null;
                SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group, context);
                string consulta = string.Empty;
                if (group != null)
                {
                    string listadoHoteles = ListadoHoteles(group);

                    string[] ids = listadoHoteles.Split(',');
                    foreach (string id in ids)
                    {
                        consulta += string.Format("ActiveSessions.IdHotel={0} OR ", id);
                    }
                    consulta = consulta.Substring(0, consulta.Length - 4);
                }
                else
                    consulta = string.Format("ActiveSessions.IdHotel={0}", hotel.IdHotel);
                

                var query6 = "|savedsearch sd_access_module_idhotel_realtime_string IdHotel_String=\"" + consulta +"\"";
                var args6 = new JobArgs{};

                json = "{\"message\": \"ok\", \"items\": [";
                // visualización de los datos
                using (SearchResultStream resultStream = await service.SearchOneShotAsync(query6, args: args6))
                {
                    response.list = new List<PieResult>();
                    foreach (SearchResult result in resultStream)
                    {
                        PieResult obj = new PieResult();

                        json += "{\"label\": \"" + result.GetValue("BillingModule") + "\", \"value\" : " + result.GetValue("Cuenta") + "},";
                    }
                }
                json = json.Substring(0, json.Length - 1);
                json += "]}";
            }
            catch (Exception ex)
            {
            }

            // json = "{\"code\":\"OK\",\"message\":\"\"}";
            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();

        }

        private async void connectionByModuleInterval(HttpContext context)
        {
            string json = string.Empty;
            ListPieResult response = new ListPieResult();
            try
            {
                string start = (context.Request["start"] == null ? string.Empty : context.Request["start"].ToString());
                string end = (context.Request["end"] == null ? string.Empty : context.Request["end"].ToString());

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback += (sender1, certificate, chain, sslPolicyErrors) =>
                {
                    return true;
                };

                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;


                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                var service = new Service(Scheme.Https, ConfigurationManager.AppSettings["SplunkUrl"].ToString(), Int32.Parse(ConfigurationManager.AppSettings["SplunkPort"].ToString()));

                await service.LogOnAsync(ConfigurationManager.AppSettings["SplunkUser"].ToString(), ConfigurationManager.AppSettings["SplunkPass"].ToString());

                Locations2 location = null;
                Hotels hotel = null;
                FDSGroups group = null;
                SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group, context);
                string consulta = string.Empty;
                if (group != null)
                {
                    string listadoHoteles = ListadoHoteles(group);

                    string[] ids = listadoHoteles.Split(',');
                    foreach (string id in ids)
                    {
                        consulta += string.Format("IdHotel = {0} OR ", id);
                    }
                    consulta = consulta.Substring(0, consulta.Length - 4);
                }
                else
                    consulta = string.Format("IdHotel = {0}", hotel.IdHotel);

                string timefilter ="earliest=" + DateTime.Now.AddDays(-7).ToUniversalTime().ToString("MM/dd/yyyy:HH:mm:ss") + " latest=" + DateTime.Now.ToUniversalTime().ToString("MM/dd/yyyy:HH:mm:ss");
                if (!string.IsNullOrEmpty(start) && !string.IsNullOrEmpty(end))
                    timefilter = "earliest=" + DateTime.Parse(start).AddSeconds(-siteGMT.gmtoffset).ToString("MM/dd/yyyy:HH:mm:ss") + " latest=" + DateTime.Parse(end).AddSeconds(-siteGMT.gmtoffset).ToString("MM/dd/yyyy:HH:mm:ss");


                var query6 = "| savedsearch sd_access_modules_interval_string IdHotel_String=\"" + consulta + "\" " + timefilter;

                var args6 = new JobArgs
                {

                };

                json = "{\"message\": \"ok\", \"items\": [";
                // visualización de los datos
                using (SearchResultStream resultStream = await service.SearchOneShotAsync(query6, args: args6))
                {
                    response.list = new List<PieResult>();
                    foreach (SearchResult result in resultStream)
                    {

                        json += "{\"label\": \"" + result.GetValue("BillingModule") + "\", \"value\" : " + result.GetValue("count(UserName)") + "},";
                    }
                }
                json = json.Substring(0, json.Length - 1);
                json += "]}";
            }
            catch (Exception ex)
            {
            }

            // json = "{\"code\":\"OK\",\"message\":\"\"}";
            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();

        }

        private async void OnlineUsersBySiteInterval(HttpContext context)
        {
            string json = string.Empty;
            ListPieResult response = new ListPieResult();
            try
            {
                string start = (context.Request["start"] == null ? string.Empty : context.Request["start"].ToString());
                string end = (context.Request["end"] == null ? string.Empty : context.Request["end"].ToString());

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback += (sender1, certificate, chain, sslPolicyErrors) =>
                {
                    return true;
                };

                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;


                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                var service = new Service(Scheme.Https, ConfigurationManager.AppSettings["SplunkUrl"].ToString(), Int32.Parse(ConfigurationManager.AppSettings["SplunkPort"].ToString()));

                await service.LogOnAsync(ConfigurationManager.AppSettings["SplunkUser"].ToString(), ConfigurationManager.AppSettings["SplunkPass"].ToString());

                Locations2 location = null;
                Hotels hotel = null;
                FDSGroups group = null;
                SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group, context);
                string consulta = string.Empty;
                if (group != null)
                {
                    string listadoHoteles = ListadoHoteles(group);

                    string[] ids = listadoHoteles.Split(',');
                    foreach (string id in ids)
                    {
                        consulta += string.Format("IdHotel = {0} OR ", id);
                    }
                    consulta = consulta.Substring(0, consulta.Length - 4);
                }
                else
                    consulta = string.Format("IdHotel = {0}", hotel.IdHotel);

                response.list = new List<PieResult>();
                List<FDSGroupsMembers> members = BillingController.SelectFDSGroupsMembers(group.IdGroup);
                foreach (FDSGroupsMembers member in members)
                {
                    PieResult pie = new PieResult();
                    switch (member.IdMemberType)
                    {
                        case 1: pie.id = member.IdMember; pie.value = "0"; pie.label = BillingController.GetGroup(member.IdMember).Name; pie.type = 1; break;
                        case 2: pie.id = member.IdMember; pie.value = "0"; pie.label = BillingController.GetHotel(member.IdMember).Name; pie.type = 2; break;

                    }
                    response.list.Add(pie);
                }

                string timefilter = "earliest=" + DateTime.Now.AddDays(-7).ToUniversalTime().ToString("MM/dd/yyyy:HH:mm:ss") + " latest=" + DateTime.Now.ToUniversalTime().ToString("MM/dd/yyyy:HH:mm:ss");
                if (!string.IsNullOrEmpty(start) && !string.IsNullOrEmpty(end))
                    timefilter = "earliest=" + DateTime.Parse(start).AddSeconds(-siteGMT.gmtoffset).ToString("MM/dd/yyyy:HH:mm:ss") + " latest=" + DateTime.Parse(end).AddSeconds(-siteGMT.gmtoffset).ToString("MM/dd/yyyy:HH:mm:ss");


                var query6 = "| savedsearch sd_onlineusers_bysite_interval_string IdHotel_String=\"" + consulta + "\" " + timefilter;

                

                var args6 = new JobArgs { };

                json = "{\"message\": \"ok\", \"items\": [";
                // visualización de los datos
                using (SearchResultStream resultStream = await service.SearchOneShotAsync(query6, args: args6))
                {
                    foreach (SearchResult result in resultStream)
                    {
                        var idhotel = result.GetValue("IdHotel");
                        PieResult obj = (from t in response.list where t.id.Equals(int.Parse((string)idhotel)) && t.type.Equals(2) select t).FirstOrDefault();
                        if (obj != null)
                        {
                            var users = result.GetValue("count(UserName)");
                            string count = (string)users;
                            obj.value = (Int32.Parse(obj.value) + Int32.Parse(users)).ToString();
                        }
                        else
                        {
                            PieResult obj2 = null;
                            int iditem = Int32.Parse(idhotel);
                            do
                            {
                                FDSGroupsMembers member = BillingController.SelectFDSGroupsMemberByIdMember(iditem);
                                obj2 = (from t in response.list where t.id.Equals(member.IdGroup) && t.type.Equals(1) select t).FirstOrDefault();
                                iditem = member.IdGroup;
                            } while (obj2 == null);

                            var users = result.GetValue("count(UserName)");
                            string count = (string)users;
                            obj2.value = (Int32.Parse(obj2.value) + Int32.Parse(users)).ToString();
                        }

                    }
                }
                foreach (PieResult p in response.list)
                    if (!p.value.Equals("0"))
                        json += "{\"label\": \"" + p.label + "\", \"value\" : " + p.value + "},";
                json = json.Substring(0, json.Length - 1);
                json += "]}";
            }
            catch (Exception ex)
            {
            }

            // json = "{\"code\":\"OK\",\"message\":\"\"}";
            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();

        }

        private async void OnlineUsersBySiteLive(HttpContext context)
        {
            string json = string.Empty;
            ListPieResult response = new ListPieResult();
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback += (sender1, certificate, chain, sslPolicyErrors) =>
                {
                    return true;
                };

                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;


                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                var service = new Service(Scheme.Https, ConfigurationManager.AppSettings["SplunkUrl"].ToString(), Int32.Parse(ConfigurationManager.AppSettings["SplunkPort"].ToString()));

                await service.LogOnAsync(ConfigurationManager.AppSettings["SplunkUser"].ToString(), ConfigurationManager.AppSettings["SplunkPass"].ToString());

                Locations2 location = null;
                Hotels hotel = null;
                FDSGroups group = null;
                SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group, context);
                string consulta = string.Empty;
                if (group != null)
                {
                    string listadoHoteles = ListadoHoteles(group);

                    string[] ids = listadoHoteles.Split(',');
                    foreach (string id in ids)
                    {
                        consulta += string.Format("ActiveSessions.IdHotel={0} OR ", id);
                    }
                    consulta = consulta.Substring(0, consulta.Length - 4);
                }
                else
                    consulta = string.Format("ActiveSessions.IdHotel={0}", hotel.IdHotel);

                response.list = new List<PieResult>();
                List<FDSGroupsMembers> members = BillingController.SelectFDSGroupsMembers(group.IdGroup);
                foreach(FDSGroupsMembers member in members)
                {
                    PieResult pie = new PieResult();
                    switch(member.IdMemberType)
                    {
                        case 1: pie.id = member.IdMember; pie.value = "0"; pie.label = BillingController.GetGroup(member.IdMember).Name; pie.type = 1; break;
                        case 2: pie.id = member.IdMember; pie.value = "0"; pie.label = BillingController.GetHotel(member.IdMember).Name; pie.type = 2; break;

                    }
                    response.list.Add(pie);
                }

                var query6 = "|savedsearch sd_onlineusers_byzone_realtime_string IdHotel_String=\"" + consulta + "\"";

                var args6 = new JobArgs {};

                json = "{\"message\": \"ok\", \"items\": [";
                // visualización de los datos
                using (SearchResultStream resultStream = await service.SearchOneShotAsync(query6, args: args6))
                {
                    foreach (SearchResult result in resultStream)
                    {
                        var idhotel = result.GetValue("IdHotel");
                        string value = (string)idhotel; 

                        //buscamos que el item en los elementos del pie, si el item no pertenece a ningún grupo, se añade directamente.
                        PieResult obj = (from t in response.list where t.id.Equals(int.Parse(value)) && t.type.Equals(2) select t).FirstOrDefault();
                        if (obj != null)
                        {
                            var users = result.GetValue("Users");
                            string count = (string)users;
                            obj.value = (Int32.Parse(obj.value) + Int32.Parse(users)).ToString();
                        }
                        //sino es un hotel, es un grupo, buscamos el grupo al que pertenece.
                        else
                        {

                            //List<PieResult> auxList = (from t in response.list where t.type.Equals(1) select t).ToList();
                            //foreach (PieResult aux in auxList)
                            //{
                            //    List<FDSGroupsMembers> auxMembers = BillingController.SelectFDSGroupsMembers(aux.id);
                            //    foreach (FDSGroupsMembers auxMember in auxMembers)
                            //    {
                            //        if (auxMember.IdMember.Equals(int.Parse(value)))
                            //        {
                            //            PieResult objAux = (from t in response.list where t.id.Equals(aux.id) select t).FirstOrDefault();
                            //            if (objAux != null)
                            //            {
                            //                var users = result.GetValue("Users");
                            //                string count = (string)users;
                            //                objAux.value = (Int32.Parse(objAux.value) + Int32.Parse(users)).ToString();
                            //            }

                            //            break;
                            //        }
                            //    }

                            //}
                            PieResult obj2 = null;
                            int iditem = Int32.Parse(value);
                            do
                            {
                                FDSGroupsMembers member = BillingController.SelectFDSGroupsMemberByIdMember(iditem);
                                obj2 = (from t in response.list where t.id.Equals(member.IdGroup) && t.type.Equals(1) select t).FirstOrDefault();
                                iditem = member.IdGroup;
                            } while (obj2 == null);

                            var users = result.GetValue("Users");
                            string count = (string)users;
                            obj2.value = (Int32.Parse(obj2.value) + Int32.Parse(users)).ToString();
                        }

                    }
                }
                foreach(PieResult p in response.list)
                    if (!p.value.Equals("0"))
                        json += "{\"label\": \"" + p.label + "\", \"value\" : " + p.value + "},";
                json = json.Substring(0, json.Length - 1);
                json += "]}";
            }
            catch (Exception ex)
            {
            }

            // json = "{\"code\":\"OK\",\"message\":\"\"}";
            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();

        }

        private async void onlineDevice(HttpContext context)
        {
            string json = string.Empty;
            ListPieResult response = new ListPieResult();
            try
            {

                string start = (context.Request["start"] == null ? string.Empty : context.Request["start"].ToString());
                string end = (context.Request["end"] == null ? string.Empty : context.Request["end"].ToString());

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback += (sender1, certificate, chain, sslPolicyErrors) =>
                {
                    return true;
                };

                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                var service = new Service(Scheme.Https, ConfigurationManager.AppSettings["SplunkUrl"].ToString(), Int32.Parse(ConfigurationManager.AppSettings["SplunkPort"].ToString()));

                Locations2 location = null;
                Hotels hotel = null;
                FDSGroups group = null;
                SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group, context);

                await service.LogOnAsync(ConfigurationManager.AppSettings["SplunkUser"].ToString(), ConfigurationManager.AppSettings["SplunkPass"].ToString());

                string consulta = string.Empty;
                var query6 = "";

                if (string.IsNullOrEmpty(start) && string.IsNullOrEmpty(end))
                {
                    start = DateTime.Now.ToUniversalTime().AddDays(-3).ToString();
                    end = DateTime.Now.ToUniversalTime().ToString();
                }
                string timefilter = "earliest=" + DateTime.Now.AddDays(-7).ToUniversalTime().AddSeconds(-siteGMT.gmtoffset).ToString("MM/dd/yyyy:HH:mm:ss") + " latest=" + DateTime.Now.ToUniversalTime().AddSeconds(-siteGMT.gmtoffset).ToString("MM/dd/yyyy:HH:mm:ss");
                if (!string.IsNullOrEmpty(start) && !string.IsNullOrEmpty(end))
                {
                    timefilter = "earliest=" + DateTime.Parse(start).ToString("MM/dd/yyyy:HH:mm:ss") + " latest=" + DateTime.Parse(end).ToString("MM/dd/yyyy:HH:mm:ss") + "";
                }

                 if (group != null)
                {
                    string listadoHoteles = ListadoHoteles(group);

                    string[] ids = listadoHoteles.Split(',');
                    foreach (string id in ids)
                    {
                        consulta += string.Format("IdHotel = {0} OR ", id);
                    }
                    consulta = consulta.Substring(0, consulta.Length - 4);
                    query6 = "| savedsearch sd_onlineusers_idhotel_interval_string IdHotel_String=\"" + consulta + "\" " + timefilter;
                }
                else if (location != null)
                {
                    FDSLocationsIdentifier locIdentifier = BillingController.GetFDSLocationIdentifier(location.IdLocation);
                    consulta = "Identifier=" + locIdentifier.Identifier;
                    query6 = "| savedsearch sd_onlineusers_location_interval LocationIdentifier_String=\"" + consulta + "\" " + timefilter;
                }
                else
                {
                    consulta = string.Format("IdHotel = {0}", hotel.IdHotel);
                    query6 = "| savedsearch sd_onlineusers_idhotel_interval_string IdHotel_String=\"" + consulta + "\" " + timefilter;
                }

               

                

                var args6 = new JobArgs{};

                json = "{\"message\": \"ok\", \"items\": [";
                // visualización de los datos
                using (SearchResultStream resultStream = await service.SearchOneShotAsync(query6, args: args6))
                {
                    foreach (SearchResult result in resultStream)
                    {
                        DateTime time = DateTime.Parse(result.GetValue("_time"));
                        time = time.AddSeconds(siteGMT.gmtoffset);
                        json += "{\"label\": \"" + time.ToString("yyyy/MM/dd HH:mm:ss") + "\", \"value\" : " + result.GetValue("Usuarios Totales") + "},";
                    }
                }
                json = json.Substring(0, json.Length - 1);
                json += "]}";
            }
            catch (Exception ex)
            {
                ;
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();

        }

        private async void UsersPerHours(HttpContext context)
        {
            string json = string.Empty;
            ListPieResult response = new ListPieResult();
            try
            {

                string start = (context.Request["start"] == null ? string.Empty : context.Request["start"].ToString());
                string end = (context.Request["end"] == null ? string.Empty : context.Request["end"].ToString());

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback += (sender1, certificate, chain, sslPolicyErrors) =>
                {
                    return true;
                };

                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                var service = new Service(Scheme.Https, ConfigurationManager.AppSettings["SplunkUrl"].ToString(), Int32.Parse(ConfigurationManager.AppSettings["SplunkPort"].ToString()));

                Locations2 location = null;
                Hotels hotel = null;
                FDSGroups group = null;
                SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group, context);

                await service.LogOnAsync(ConfigurationManager.AppSettings["SplunkUser"].ToString(), ConfigurationManager.AppSettings["SplunkPass"].ToString());

                string consulta = string.Empty;
                if (string.IsNullOrEmpty(start) && string.IsNullOrEmpty(end))
                {
                    start = DateTime.Now.ToUniversalTime().AddDays(-3).ToString();
                    end = DateTime.Now.ToUniversalTime().ToString();
                }
                string timefilter = "earliest_string=" + DateTime.Now.AddDays(-7).ToUniversalTime().ToString("MM/dd/yyyy:HH:mm:ss") + " earliest_string=" + DateTime.Now.ToUniversalTime().ToString("MM/dd/yyyy:HH:mm:ss");
                if (!string.IsNullOrEmpty(start) && !string.IsNullOrEmpty(end))
                {
                    timefilter = "earliest_string=" + DateTime.Parse(start).ToString("MM/dd/yyyy:HH:mm:ss") + " latest_string=" + DateTime.Parse(end).ToString("MM/dd/yyyy:HH:mm:ss") + "";

                }
                var query6 = "";
                if (group != null)
                {
                    string listadoHoteles = ListadoHoteles(group);

                    string[] ids = listadoHoteles.Split(',');
                    foreach (string id in ids)
                    {
                        consulta += string.Format("IdHotel={0} OR ", id);
                    }
                    consulta = consulta.Substring(0, consulta.Length - 4);
                    query6 = "| savedsearch sd_onlineusers_histogram_idhotel_interval_string Idhotel_String=\"" + consulta + "\" " + timefilter;
                }
                else if (location != null)
                {
                    FDSLocationsIdentifier locIdentifier = BillingController.GetFDSLocationIdentifier(location.IdLocation);
                    consulta = "Identifier=" + locIdentifier.Identifier;
                    query6 = "| savedsearch sd_onlineusers_histogram_location_interval LocationIdentifier_String=\"" + consulta + "\" " + timefilter;
                }
                else
                {
                    consulta = string.Format("IdHotel={0}", hotel.IdHotel);
                    query6 = "| savedsearch sd_onlineusers_histogram_idhotel_interval_string Idhotel_String=\"" + consulta + "\" " + timefilter;
                }

                var args6 = new JobArgs { };

                int offset = siteGMT.gmtoffset / 3600;

                List<HistogramaElement> listHistograma = new List<HistogramaElement>();
                json = "{\"message\": \"ok\", \"items\": [";
                // visualización de los datos
                using (SearchResultStream resultStream = await service.SearchOneShotAsync(query6, args: args6))
                {
                    foreach (SearchResult result in resultStream)
                    {
                        string value = result.GetValue("avg(Count)");
                        int hora = int.Parse(result.GetValue("HoraNum"));
                        int horaSiteGMT = (hora + offset) + 24;
                        HistogramaElement element = new HistogramaElement();
                        
                        element.hora = horaSiteGMT % 24;
                        element.value = value;
                        listHistograma.Add(element);
                    }
                }

                foreach(HistogramaElement obj in listHistograma.OrderBy(a => a.hora))
                    json += "{\"Hora\": \"" + obj.hora + "\", \"value\" : " + obj.value + "},";
                json = json.Substring(0, json.Length - 1);
                json += "]}";

            }
            catch (Exception ex)
            {
                ;
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();

        }

        private async void sessionlog(HttpContext context)
        {
            string json = string.Empty;
            ListPieResult response = new ListPieResult();
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback += (sender1, certificate, chain, sslPolicyErrors) =>
                {
                    return true;
                };

                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                var service = new Service(Scheme.Https, ConfigurationManager.AppSettings["SplunkUrl"].ToString(), Int32.Parse(ConfigurationManager.AppSettings["SplunkPort"].ToString()));

                await service.LogOnAsync(ConfigurationManager.AppSettings["SplunkUser"].ToString(), ConfigurationManager.AppSettings["SplunkPass"].ToString());

                var query6 =
                    "search index = fbd source = ClosedSessions earliest = \"09/10/2018:00:00:00\" latest = \"09/20/2018:00:00:00\"   IdHotel = 67";
                query6 += "| eval start = strptime('SessionStarted', \"%Y-%m-%d %T\")";
                query6 += "| eval end = strptime('SessionTerminated', \"%Y-%m-%d %T\")";
                query6 += "| eval duration = (end - start) / 60";
                query6 += "| eval myrange =case(duration <= 15,\" < 15 Min\",duration > 15 AND duration<= 60,\"Entre 15 y 60 Min\",duration > 60,\"> 60 Min\")";
                query6 += "| chart count(myrange) as \"Usuarios conectados\" by myrange";

                var args6 = new JobArgs
                {

                };

                json = "{\"message\": \"ok\", \"items\": [";
                // visualización de los datos
                using (SearchResultStream resultStream = await service.SearchOneShotAsync(query6, args: args6))
                {
                    response.list = new List<PieResult>();
                    foreach (SearchResult result in resultStream)
                    {
                        PieResult obj = new PieResult();
                        obj.label = result.GetValue("myrange");
                        obj.value = result.GetValue("Usuarios conectados");

                        response.list.Add(obj);

                        json += "{\"label\": \"" + result.GetValue("myrange") + "\", \"value\" : " + result.GetValue("Usuarios conectados") + "},";
                    }
                }
                json = json.Substring(0, json.Length - 1);
                json += "]}";
            }
            catch (Exception ex)
            {
            }

            // json = "{\"code\":\"OK\",\"message\":\"\"}";
            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();

        }

        private async void UsersLocation(HttpContext context)
        {
            string json = string.Empty;
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback += (sender1, certificate, chain, sslPolicyErrors) =>
                {
                    return true;
                };

                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                var service = new Service(Scheme.Https, ConfigurationManager.AppSettings["SplunkUrl"].ToString(), Int32.Parse(ConfigurationManager.AppSettings["SplunkPort"].ToString()));

                await service.LogOnAsync(ConfigurationManager.AppSettings["SplunkUser"].ToString(), ConfigurationManager.AppSettings["SplunkPass"].ToString());


                var query7 = "| dbxquery query=\"select* from \"radius_produccion\".\"dbo\".\"ActiveSessions\" WHERE IdHotel = 174\" connection=\"fb_connection\"";
                query7 += "| search LocationIdentifier = DREAMFIT_ZARAGOZA OR LocationIdentifier = DREAMFIT_VENTAS OR LocationIdentifier = DREAMFIT_ALICANTE OR LocationIdentifier = DREAMFIT_CASTELLON OR LocationIdentifier = DREAMFIT_SEGOVIA";
                query7 += "| lookup fb_coordenadas.csv LocationIdentifier OUTPUT latitude, longitude, municipio, provincia";


                var args7 = new JobArgs { };
                json = "{\"message\": \"ok\", \"items\": [";
                // visualización de los datos
                using (SearchResultStream resultStream = await service.SearchOneShotAsync(query7, args: args7))
                {
                    foreach (SearchResult result in resultStream)
                    {
                        
                        json += "[ " + result.GetValue("latitude") + "," + result.GetValue("longitude") + ", \"" + result.GetValue("CallerID") + "\"],";
                    }
                };
                json = json.Substring(0, json.Length - 1);
                json += "]}";

                
            }
            catch (Exception ex)
            {
            }

            // json = "{\"code\":\"OK\",\"message\":\"\"}";
            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();

        }

        private async void UsersOnlineMap(HttpContext context)
        {
            string json = string.Empty;
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback += (sender1, certificate, chain, sslPolicyErrors) =>
                {
                    return true;
                };

                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                var service = new Service(Scheme.Https, ConfigurationManager.AppSettings["SplunkUrl"].ToString(), Int32.Parse(ConfigurationManager.AppSettings["SplunkPort"].ToString()));

                await service.LogOnAsync(ConfigurationManager.AppSettings["SplunkUser"].ToString(), ConfigurationManager.AppSettings["SplunkPass"].ToString());


                Locations2 location = null;
                Hotels hotel = null;
                FDSGroups group = null;
                SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group, context);
                string consulta = string.Empty;
                string locationCoordenates = string.Empty;
                List<FDSLocationsIdentifier> listLocations = null;
                if (group != null)
                {
                    string listadoHoteles = ListadoHoteles(group);

                    string[] ids = listadoHoteles.Split(',');
                    foreach (string id in ids)
                    {
                        consulta += string.Format("ActiveSessions.IdHotel = {0} OR ", id);
                    }
                    consulta = consulta.Substring(0, consulta.Length - 4);
                    locationCoordenates = group.Latitude + ", " + group.Longitude;
                }
                else
                {
                    consulta = string.Format("ActiveSessions.IdHotel = {0}", hotel.IdHotel);
                    locationCoordenates = hotel.Latitude + ", " + hotel.Longitude;
                    listLocations = BillingController.GetFDSLocationsIdentifiers(hotel.IdHotel);
                    
                }


                var query7 = "";

                if (listLocations == null || listLocations.Count == 0)
                {
                    query7 = "| dbxquery query=\"select CallerId, Latitude, Longitude from \"radius_produccion\".\"dbo\".\"ActiveSessions\" ";
                    query7 += "inner join \"radius_produccion\".\"dbo\".\"Hotels\" on ";
                    query7 += "Hotels.IdHotel = ActiveSessions.IdHotel  WHERE " + consulta + "\" connection=\"fb_connection\" ";
                }
                else
                {
                    if (!hotel.IdVendor.Equals(2) && !hotel.IdVendor.Equals(4))
                    {
                        query7 = "| dbxquery query=\"select ActiveSessions.CallerId, Locations.Latitude, Locations.Longitude from \"radius_produccion\".\"dbo\".\"ActiveSessions\" ";
                        query7 += "inner join \"radius_produccion\".\"dbo\".\"Hotels\" on ";
                        query7 += "Hotels.IdHotel = ActiveSessions.IdHotel  ";
                        query7 += "inner join \"radius_produccion\".\"dbo\".\"FDSLocationsIdentifier\" on ";
                        query7 += "FDSLocationsIdentifier.Identifier = ActiveSessions.LocationIdentifier  ";
                        query7 += "inner join \"radius_produccion\".\"dbo\".\"Locations\" on ";
                        query7 += "Locations.IdLocation = FDSLocationsIdentifier.IdLocation  ";
                        query7 += "WHERE " + consulta + "\" connection=\"fb_connection\" ";

                    }
                    else
                    {
                        if (hotel.IdVendor.Equals(2))
                        {
                            if (location != null)
                            {
                                query7 = "| dbxquery query=\"select ActiveSessions.CallerId, MerakiLocationsGPS.Latitude, MerakiLocationsGPS.Longitude from \"radius_produccion\".\"dbo\".\"ActiveSessions\" ";
                                query7 += "inner join \"radius_produccion\".\"dbo\".\"Hotels\" on ";
                                query7 += "Hotels.IdHotel = ActiveSessions.IdHotel  ";
                                query7 += "inner join \"radius_produccion\".\"dbo\".\"MerakiLocationsGPS\" on ";
                                query7 += "MerakiLocationsGPS.NAS_MAC  = ActiveSessions.NAS_MAC    ";
                                query7 += "inner join \"radius_produccion\".\"dbo\".\"Locations\" on ";
                                query7 += "Locations.IdLocation = MerakiLocationsGPS.IdLocation  ";
                                query7 += "WHERE Locations.IdLocation=" + location.IdLocation + "\" connection=\"fb_connection\" ";
                            }
                            else
                            {
                                query7 = "| dbxquery query=\"select ActiveSessions.CallerId, MerakiLocationsGPS.Latitude, MerakiLocationsGPS.Longitude from \"radius_produccion\".\"dbo\".\"ActiveSessions\" ";
                                query7 += "inner join \"radius_produccion\".\"dbo\".\"Hotels\" on ";
                                query7 += "Hotels.IdHotel = ActiveSessions.IdHotel  ";
                                query7 += "inner join \"radius_produccion\".\"dbo\".\"MerakiLocationsGPS\" on ";
                                query7 += "MerakiLocationsGPS.NAS_MAC  = ActiveSessions.NAS_MAC    ";
                                query7 += "inner join \"radius_produccion\".\"dbo\".\"Locations\" on ";
                                query7 += "Locations.IdLocation = MerakiLocationsGPS.IdLocation  ";
                                query7 += "WHERE " + consulta + "\" connection=\"fb_connection\" ";
                            }
                        }
                        else if (hotel.IdVendor.Equals(4))
                        {
                            if (location != null)
                            {
                                query7 = "| dbxquery query=\"select ActiveSessions.CallerId, CambiumLocationsGPS.Latitude, CambiumLocationsGPS.Longitude from \"radius_produccion\".\"dbo\".\"ActiveSessions\" ";
                                query7 += "inner join \"radius_produccion\".\"dbo\".\"Hotels\" on ";
                                query7 += "Hotels.IdHotel = ActiveSessions.IdHotel  ";
                                query7 += "inner join \"radius_produccion\".\"dbo\".\"CambiumLocationsGPS\" on ";
                                query7 += "CambiumLocationsGPS.NAS_MAC  = ActiveSessions.NAS_MAC    ";
                                query7 += "inner join \"radius_produccion\".\"dbo\".\"Locations\" on ";
                                query7 += "Locations.IdLocation = CambiumLocationsGPS.IdLocation  ";
                                query7 += "WHERE Locations.IdLocation=" + location.IdLocation + "\" connection=\"fb_connection\" ";
                            }
                            else
                            {
                                query7 = "| dbxquery query=\"select ActiveSessions.CallerId, CambiumLocationsGPS.Latitude, CambiumLocationsGPS.Longitude from \"radius_produccion\".\"dbo\".\"ActiveSessions\" ";
                                query7 += "inner join \"radius_produccion\".\"dbo\".\"Hotels\" on ";
                                query7 += "Hotels.IdHotel = ActiveSessions.IdHotel  ";
                                query7 += "inner join \"radius_produccion\".\"dbo\".\"CambiumLocationsGPS\" on ";
                                query7 += "CambiumLocationsGPS.NAS_MAC  = ActiveSessions.NAS_MAC    ";
                                query7 += "inner join \"radius_produccion\".\"dbo\".\"Locations\" on ";
                                query7 += "Locations.IdLocation = CambiumLocationsGPS.IdLocation  ";
                                query7 += "WHERE " + consulta + "\" connection=\"fb_connection\" ";
                            }
                        }
                    }

                }

                var args7 = new JobArgs { };
                json = "{\"message\": \"ok\", \"groupLocation\":["+ locationCoordenates + "], \"items\": [";
                //json = "{\"message\": \"ok\", \"items\": [";
                // visualización de los datos
                using (SearchResultStream resultStream = await service.SearchOneShotAsync(query7, args: args7))
                {
                    foreach (SearchResult result in resultStream)
                    {

                        json += "[ " + result.GetValue("Latitude") + "," + result.GetValue("Longitude") + ", \"" + result.GetValue("CallerId") + "\"],";
                    }
                };
                json = json.Substring(0, json.Length - 1);
                json += "]}";


            }
            catch (Exception ex)
            {
                ;
            }

            // json = "{\"code\":\"OK\",\"message\":\"\"}";
            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();

        }

        private async void topology(HttpContext context)
        {
            string json = string.Empty;
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback += (sender1, certificate, chain, sslPolicyErrors) =>
                {
                    return true;
                };

                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                json = "{\"message\": \"ok\", \"items\": [";

                var service = new Service(Scheme.Https, "splunk.future-broadband.com", 8089);

                await service.LogOnAsync("jarrocha@ilos.es", "xs564s05");

                var query7 = " |  inputlookup fb_topologia_princesa.csv | table sourceHost sourceRole targetHost targetRole linkType";
              // var query7 = "search index=fbd source=\"snmp://snmp_controladora_ruckus\" | head 1";


                var args7 = new JobArgs { };
                
                // visualización de los datos
                using (SearchResultStream resultStream = await service.SearchOneShotAsync(query7, args: args7))
                {
                    foreach (SearchResult result in resultStream)
                    {

                        ;
                    }
                };

                json = json.Substring(0, json.Length - 1);
                json += "]}";


            }
            catch (Exception ex)
            {
            }

            // json = "{\"code\":\"OK\",\"message\":\"\"}";
            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();

        }

        private async void ValuesLive(HttpContext context)
        {
            string json = string.Empty;
            try
            {

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback += (sender1, certificate, chain, sslPolicyErrors) =>{return true;};

                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                json = "{\"message\": \"ok\",";

                var service = new Service(Scheme.Https, ConfigurationManager.AppSettings["SplunkUrl"].ToString(), Int32.Parse(ConfigurationManager.AppSettings["SplunkPort"].ToString()));

                await service.LogOnAsync(ConfigurationManager.AppSettings["SplunkUser"].ToString(), ConfigurationManager.AppSettings["SplunkPass"].ToString());

                Locations2 location = null;
                Hotels hotel = null;
                FDSGroups group = null;
                SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group, context);
                string consulta = string.Empty;
                if (group != null)
                {
                    string listadoHoteles = ListadoHoteles(group);

                    string[] ids = listadoHoteles.Split(',');
                    foreach (string id in ids)
                    {
                        consulta += string.Format("IdHotel={0} OR ", id);
                    }
                    consulta = consulta.Substring(0, consulta.Length - 4);
                }
                else if (location != null)
                {
                    FDSLocationsIdentifier identifier = BillingController.GetFDSLocationIdentifier(location.IdLocation);
                    consulta = string.Format("LocationIdentifier={0}", identifier.Identifier);
                }
                else
                    consulta = string.Format("IdHotel={0}", hotel.IdHotel);

                var args7 = new JobArgs { };

                #region 24H

                var download24hquery = "|savedsearch sd_download_24h_idhotel_string IdHotel_String=\"" + consulta + "\"";

                // visualización de los datos
                using (SearchResultStream resultStream = await service.SearchOneShotAsync(download24hquery, args: args7))
                {
                    foreach (SearchResult result in resultStream)
                    {
                        string value = result.GetValue("DownloadTotal_GB");
                        json += "\"Download_24H\": \"" + Math.Round(Double.Parse(value.Replace('.', ',')), 2) + "\",";
                    }
                };

                var upload24hquery = "|savedsearch sd_upload_24h_idhotel_string IdHotel_String=\"" + consulta + "\"";

                using (SearchResultStream resultStream = await service.SearchOneShotAsync(upload24hquery, args: args7))
                {
                    foreach (SearchResult result in resultStream)
                    {
                        string value = result.GetValue("UploadTotal_GB");
                        json += "\"Upload_24H\": \"" + Math.Round(Double.Parse(value.Replace('.', ',')), 2) + "\",";
                    }
                };

                var sesion24hquery = "|savedsearch sd_sessions_24h_idhotel_string IdHotel_String=\"" + consulta + "\"";

                using (SearchResultStream resultStream = await service.SearchOneShotAsync(sesion24hquery, args: args7))
                {
                    foreach (SearchResult result in resultStream)
                    {
                        string value = result.GetValue("dc(AcctSessionID)");
                        json += "\"Sesion_24H\": \"" + value + "\",";
                    }
                };

                var users24hquery = "|savedsearch sd_usuarios_24h_idhotel_string IdHotel_String=\"" + consulta + "\"";

                using (SearchResultStream resultStream = await service.SearchOneShotAsync(users24hquery, args: args7))
                {
                    foreach (SearchResult result in resultStream)
                    {
                        string value = result.GetValue("dc(UserName)");
                        json += "\"Users_24H\": \"" + value + "\",";
                    }
                };

                var device24hquery = "|savedsearch sd_devices_24h_idhotel_string IdHotel_String=\"" + consulta + "\"";

                using (SearchResultStream resultStream = await service.SearchOneShotAsync(device24hquery, args: args7))
                {
                    foreach (SearchResult result in resultStream)
                    {
                        string value = result.GetValue("dc(CallerID)");
                        json += "\"device_24H\": \"" + value + "\",";
                    }
                };

                var sesiondevice24hquery = "|savedsearch sd_sessions_device_24h_idhotel_string IdHotel_String=\"" + consulta + "\"";

                using (SearchResultStream resultStream = await service.SearchOneShotAsync(sesiondevice24hquery, args: args7))
                {
                    foreach (SearchResult result in resultStream)
                    {
                        string value = result.GetValue("RATIO");
                        json += "\"sesiondevice_24H\": \"" + Math.Round(Double.Parse(value.Replace('.', ',')), 2) + "\",";
                    }
                };

                #endregion

                #region 1H

                var download1hquery = "|savedsearch sd_download_1h_idhotel_string IdHotel_String=\"" + consulta + "\"";

                // visualización de los datos
                using (SearchResultStream resultStream = await service.SearchOneShotAsync(download1hquery, args: args7))
                {
                    foreach (SearchResult result in resultStream)
                    {
                        string value = result.GetValue("DownloadTotal_GB");
                        json += "\"Download_1H\": \"" + Math.Round(Double.Parse(value.Replace('.', ',')), 2) + "\",";
                    }
                };

                var upload1hquery = "|savedsearch sd_upload_1h_idhotel_string IdHotel_String=\"" + consulta + "\"";

                using (SearchResultStream resultStream = await service.SearchOneShotAsync(upload1hquery, args: args7))
                {
                    foreach (SearchResult result in resultStream)
                    {
                        string value = result.GetValue("UploadTotal_GB");
                        json += "\"Upload_1H\": \"" + Math.Round(Double.Parse(value.Replace('.', ',')), 2) + "\",";
                    }
                };

                var sesion1hquery = "| savedsearch sd_sessions_1h_idhotel_string IdHotel_String=\"" + consulta + "\"";

                using (SearchResultStream resultStream = await service.SearchOneShotAsync(sesion1hquery, args: args7))
                {
                    foreach (SearchResult result in resultStream)
                    {
                        string value = result.GetValue("dc(AcctSessionID)");
                        json += "\"Sesion_1H\": \"" + value + "\",";
                    }
                };

                var users1hquery = "| savedsearch sd_usuarios_1h_idhotel_string IdHotel_String=\"" + consulta + "\"";

                using (SearchResultStream resultStream = await service.SearchOneShotAsync(users1hquery, args: args7))
                {
                    foreach (SearchResult result in resultStream)
                    {
                        string value = result.GetValue("dc(UserName)");
                        json += "\"Users_1H\": \"" + value + "\",";
                    }
                };

                var device1hquery = "| savedsearch sd_devices_1h_idhotel_string IdHotel_String=\"" + consulta + "\"";

                using (SearchResultStream resultStream = await service.SearchOneShotAsync(device1hquery, args: args7))
                {
                    foreach (SearchResult result in resultStream)
                    {
                        string value = result.GetValue("dc(CallerID)");
                        json += "\"device_1H\": \"" + value + "\",";
                    }
                };

                var sesiondevice1hquery = "| savedsearch sd_sessions_device_1h_idhotel_string IdHotel_String=\"" + consulta + "\"";

                using (SearchResultStream resultStream = await service.SearchOneShotAsync(sesiondevice1hquery, args: args7))
                {
                    foreach (SearchResult result in resultStream)
                    {
                        string value = result.GetValue("RATIO");
                        json += "\"sesiondevice_1H\": \"" + Math.Round(Double.Parse(value.Replace('.', ',')), 2) + "\",";
                    }
                };

                #endregion

                json = json.Substring(0, json.Length - 1);
                json += "}";


            }
            catch (Exception ex)
            {
            }

            // json = "{\"code\":\"OK\",\"message\":\"\"}";
            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();

        }

        private async void ValuesInterval(HttpContext context)
        {
            string json = string.Empty;
            try
            {

                string start = (context.Request["start"] == null ? string.Empty : context.Request["start"].ToString());
                string end = (context.Request["end"] == null ? string.Empty : context.Request["end"].ToString());

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback += (sender1, certificate, chain, sslPolicyErrors) => { return true; };

                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                json = "{\"message\": \"ok\",";

                var service = new Service(Scheme.Https, ConfigurationManager.AppSettings["SplunkUrl"].ToString(), Int32.Parse(ConfigurationManager.AppSettings["SplunkPort"].ToString()));

                await service.LogOnAsync(ConfigurationManager.AppSettings["SplunkUser"].ToString(), ConfigurationManager.AppSettings["SplunkPass"].ToString());

                Locations2 location = null;
                Hotels hotel = null;
                FDSGroups group = null;
                SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group, context);
                string consulta = string.Empty;
                if (group != null)
                {
                    string listadoHoteles = ListadoHoteles(group);

                    string[] ids = listadoHoteles.Split(',');
                    foreach (string id in ids)
                    {
                        consulta += string.Format("IdHotel={0} OR ", id);
                    }
                    consulta = consulta.Substring(0, consulta.Length - 4);
                }
                else if (location != null)
                {
                    FDSLocationsIdentifier identifier = BillingController.GetFDSLocationIdentifier(location.IdLocation);
                    consulta = string.Format("LocationIdentifier={0}", identifier.Identifier);
                }
                else
                    consulta = string.Format("IdHotel={0}", hotel.IdHotel);

                string timefilter = "earliest=" + DateTime.Now.AddDays(-7).ToUniversalTime().ToString("MM/dd/yyyy:HH:mm:ss") + " latest=" + DateTime.Now.ToUniversalTime().ToString("MM/dd/yyyy:HH:mm:ss");
                if (!string.IsNullOrEmpty(start) && !string.IsNullOrEmpty(end))
                    timefilter = "earliest=" + DateTime.Parse(start).AddSeconds(-siteGMT.gmtoffset).ToString("MM/dd/yyyy:HH:mm:ss") + " latest=" + DateTime.Parse(end).AddSeconds(-siteGMT.gmtoffset).ToString("MM/dd/yyyy:HH:mm:ss");


                var args7 = new JobArgs { };

                #region Interval
                var downloadintevalquery = "| savedsearch sd_download_interval_idhotel_string IdHotel_String=\"" + consulta + "\" " + timefilter;

                // visualización de los datos
                using (SearchResultStream resultStream = await service.SearchOneShotAsync(downloadintevalquery, args: args7))
                {
                    foreach (SearchResult result in resultStream)
                    {
                        string value = result.GetValue("DownloadTotal_GB");
                        json += "\"Download_Interval\": \"" + Math.Round(Double.Parse(value.Replace('.', ',')), 2) + "\",";
                    }
                };

                var uploadintevalquery = "| savedsearch sd_upload_interval_idhotel_string IdHotel_String=\"" + consulta + "\" " + timefilter;

                using (SearchResultStream resultStream = await service.SearchOneShotAsync(uploadintevalquery, args: args7))
                {
                    foreach (SearchResult result in resultStream)
                    {
                        string value = result.GetValue("UploadTotal_GB");
                        json += "\"Upload_Interval\": \"" + Math.Round(Double.Parse(value.Replace('.', ',')), 2) + "\",";
                    }
                };

                var sesionintevalquery = "| savedsearch sd_sessions_interval_idhotel_string IdHotel_String=\"" + consulta + "\" " + timefilter;

                using (SearchResultStream resultStream = await service.SearchOneShotAsync(sesionintevalquery, args: args7))
                {
                    foreach (SearchResult result in resultStream)
                    {
                        string value = result.GetValue("dc(AcctSessionID)");
                        json += "\"Sesion_Interval\": \"" + value + "\",";
                    }
                };

                var usersintevalquery = "| savedsearch sd_usuarios_interval_idhotel_string IdHotel_String=\"" + consulta + "\" " + timefilter;

                using (SearchResultStream resultStream = await service.SearchOneShotAsync(usersintevalquery, args: args7))
                {
                    foreach (SearchResult result in resultStream)
                    {
                        string value = result.GetValue("dc(UserName)");
                        json += "\"Users_Interval\": \"" + value + "\",";
                    }
                };

                var deviceintevalquery = "| savedsearch sd_devices_interval_idhotel_string IdHotel_String=\"" + consulta + "\" " + timefilter;

                using (SearchResultStream resultStream = await service.SearchOneShotAsync(deviceintevalquery, args: args7))
                {
                    foreach (SearchResult result in resultStream)
                    {
                        string value = result.GetValue("dc(CallerID)");
                        json += "\"device_Interval\": \"" + value + "\",";
                    }
                };

                var sesiondeviceintevalquery = "| savedsearch sd_sessions_device_interval_idhotel_string IdHotel_String=\"" + consulta + "\" " + timefilter;

                using (SearchResultStream resultStream = await service.SearchOneShotAsync(sesiondeviceintevalquery, args: args7))
                {
                    foreach (SearchResult result in resultStream)
                    {
                        string value = result.GetValue("RATIO");
                        json += "\"sesiondevice_Interval\": \"" + Math.Round(Double.Parse(value.Replace('.', ',')), 2) + "\",";
                    }
                };

                #endregion

                json = json.Substring(0, json.Length - 1);
                json += "}";


            }
            catch (Exception ex)
            {
            }

            // json = "{\"code\":\"OK\",\"message\":\"\"}";
            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();

        }

        private async void UsersByZoneLive(HttpContext context)
        {
            string json = string.Empty;
            ListPieResult response = new ListPieResult();
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback += (sender1, certificate, chain, sslPolicyErrors) =>
                {
                    return true;
                };

                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                var service = new Service(Scheme.Https, ConfigurationManager.AppSettings["SplunkUrl"].ToString(), Int32.Parse(ConfigurationManager.AppSettings["SplunkPort"].ToString()));

                await service.LogOnAsync(ConfigurationManager.AppSettings["SplunkUser"].ToString(), ConfigurationManager.AppSettings["SplunkPass"].ToString());

                Locations2 location = null;
                Hotels hotel = null;
                FDSGroups group = null;
                SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group, context);
                string consulta = string.Empty;
                if (group != null)
                {
                    string listadoHoteles = ListadoHoteles(group);

                    string[] ids = listadoHoteles.Split(',');
                    foreach (string id in ids)
                    {
                        consulta += string.Format("IdHotel={0} OR ", id);
                    }
                    consulta = consulta.Substring(0, consulta.Length - 4);
                }
                else
                    consulta = string.Format("IdHotel={0}", hotel.IdHotel);

                response.list = new List<PieResult>();

                var query6 = "|savedsearch sd_onlineusers_byzone_realtime " + consulta;

                var args6 = new JobArgs { };

                json = "{\"message\": \"ok\", \"items\": [";
                // visualización de los datos
                using (SearchResultStream resultStream = await service.SearchOneShotAsync(query6, args: args6))
                {
                    foreach (SearchResult result in resultStream)
                    {
                        json += "{\"label\": \"" + result.GetValue("Zona") + "\", \"value\" : " + result.GetValue("Usuarios") + "},";
                    }
                }

                json = json.Substring(0, json.Length - 1);
                json += "]}";
            }
            catch (Exception ex)
            {
            }

            // json = "{\"code\":\"OK\",\"message\":\"\"}";
            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();

        }

        private async void UsersByZoneInterval(HttpContext context)
        {
            string json = string.Empty;
            ListPieResult response = new ListPieResult();
            try
            {
                string start = (context.Request["start"] == null ? string.Empty : context.Request["start"].ToString());
                string end = (context.Request["end"] == null ? string.Empty : context.Request["end"].ToString());

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback += (sender1, certificate, chain, sslPolicyErrors) =>
                {
                    return true;
                };

                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                var service = new Service(Scheme.Https, ConfigurationManager.AppSettings["SplunkUrl"].ToString(), Int32.Parse(ConfigurationManager.AppSettings["SplunkPort"].ToString()));

                await service.LogOnAsync(ConfigurationManager.AppSettings["SplunkUser"].ToString(), ConfigurationManager.AppSettings["SplunkPass"].ToString());

                Locations2 location = null;
                Hotels hotel = null;
                FDSGroups group = null;
                SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group, context);
                string consulta = string.Empty;
                if (group != null)
                {
                    string listadoHoteles = ListadoHoteles(group);

                    string[] ids = listadoHoteles.Split(',');
                    foreach (string id in ids)
                    {
                        consulta += string.Format("IdHotel = {0} OR ", id);
                    }
                    consulta = consulta.Substring(0, consulta.Length - 4);
                }
                else
                    consulta = string.Format("IdHotel = {0}", hotel.IdHotel);

                response.list = new List<PieResult>();

                string timefilter = "earliest=-7d latest=now";
                if (!string.IsNullOrEmpty(start) && !string.IsNullOrEmpty(end))
                    timefilter = "earliest=" + DateTime.Parse(start).AddSeconds(-siteGMT.gmtoffset).ToString("MM/dd/yyyy:HH:mm:ss") + " latest=" + DateTime.Parse(end).AddSeconds(-siteGMT.gmtoffset).ToString("MM/dd/yyyy:HH:mm:ss");

                var query6 = "| savedsearch sd_onlineusers_byzone_interval_string IdHotel_String=\"" + consulta + "\" " + timefilter;
                var args6 = new JobArgs { };

                json = "{\"message\": \"ok\", \"items\": [";
                // visualización de los datos
                using (SearchResultStream resultStream = await service.SearchOneShotAsync(query6, args: args6))
                {
                    foreach (SearchResult result in resultStream)
                    {
                        json += "{\"label\": \"" + result.GetValue("LocationIdentifier") + "\", \"value\" : " + result.GetValue("count(UserName)") + "},";
                    }
                }

                json = json.Substring(0, json.Length - 1);
                json += "]}";
            }
            catch (Exception ex)
            {
            }

            // json = "{\"code\":\"OK\",\"message\":\"\"}";
            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();

        }

        private async void UsersByBillingTypeLive(HttpContext context)
        {
            string json = string.Empty;
            ListPieResult response = new ListPieResult();
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback += (sender1, certificate, chain, sslPolicyErrors) =>
                {
                    return true;
                };

                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                var service = new Service(Scheme.Https, ConfigurationManager.AppSettings["SplunkUrl"].ToString(), Int32.Parse(ConfigurationManager.AppSettings["SplunkPort"].ToString()));

                await service.LogOnAsync(ConfigurationManager.AppSettings["SplunkUser"].ToString(), ConfigurationManager.AppSettings["SplunkPass"].ToString());

                Locations2 location = null;
                Hotels hotel = null;
                FDSGroups group = null;
                SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group, context);
                string consulta = string.Empty;
                var query6 = "";
                if (group != null)
                {
                    string listadoHoteles = ListadoHoteles(group);

                    string[] ids = listadoHoteles.Split(',');
                    foreach (string id in ids)
                    {
                        consulta += string.Format("ActiveSessions.IdHotel = {0} OR ", id);
                    }
                    consulta = consulta.Substring(0, consulta.Length - 4);
                    query6 = "|savedsearch sd_access_type_idhotel_realtime " + consulta;
                }
                else if (location != null)
                {
                    FDSLocationsIdentifier identifier = BillingController.GetFDSLocationIdentifier(location.IdLocation);
                    consulta = "LocationIdentifier='" + identifier.Identifier + "'";
                    query6 = "|savedsearch sd_access_type_location_realtime " + consulta;
                }
                else
                {
                    consulta = string.Format("IdHotel={0}", hotel.IdHotel);
                    query6 = "|savedsearch sd_access_type_idhotel_realtime " + consulta;
                }

               

                var args6 = new JobArgs { };

                json = "{\"message\": \"ok\", \"items\": [";
                // visualización de los datos
                using (SearchResultStream resultStream = await service.SearchOneShotAsync(query6, args: args6))
                {
                    foreach (SearchResult result in resultStream)
                    {
                        json += "{\"label\": \"" + result.GetValue("Description") + "\", \"value\" : " + result.GetValue("Cuenta") + "},";
                    }
                }

                json = json.Substring(0, json.Length - 1);
                json += "]}";
            }
            catch (Exception ex)
            {
            }

            // json = "{\"code\":\"OK\",\"message\":\"\"}";
            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();

        }

        private async void UsersByBillingTypeInterval(HttpContext context)
        {
            string json = string.Empty;
            try
            {
                string start = (context.Request["start"] == null ? string.Empty : context.Request["start"].ToString());
                string end = (context.Request["end"] == null ? string.Empty : context.Request["end"].ToString());

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback += (sender1, certificate, chain, sslPolicyErrors) =>
                {
                    return true;
                };

                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                var service = new Service(Scheme.Https, ConfigurationManager.AppSettings["SplunkUrl"].ToString(), Int32.Parse(ConfigurationManager.AppSettings["SplunkPort"].ToString()));

                await service.LogOnAsync(ConfigurationManager.AppSettings["SplunkUser"].ToString(), ConfigurationManager.AppSettings["SplunkPass"].ToString());

                Locations2 location = null;
                Hotels hotel = null;
                FDSGroups group = null;
                SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group, context);
                string consulta = string.Empty;

                string timefilter = "earliest=-7d latest=now";
                if (!string.IsNullOrEmpty(start) && !string.IsNullOrEmpty(end))
                    timefilter = "earliest=" + DateTime.Parse(start).AddSeconds(-siteGMT.gmtoffset).ToString("MM/dd/yyyy:HH:mm:ss") + " latest=" + DateTime.Parse(end).AddSeconds(-siteGMT.gmtoffset).ToString("MM/dd/yyyy:HH:mm:ss");

                var query6 = "";
                if (group != null)
                {
                    string listadoHoteles = ListadoHoteles(group);

                    string[] ids = listadoHoteles.Split(',');
                    foreach (string id in ids)
                    {
                        consulta += string.Format("IdHotel = {0} OR ", id);
                    }
                    consulta = consulta.Substring(0, consulta.Length - 4);
                    query6 = "| savedsearch sd_access_type_idhotel_interval_string IdHotel_String=\"" + consulta + "\" " + timefilter;
                }
                else if (location != null)
                {
                    FDSLocationsIdentifier identifier = BillingController.GetFDSLocationIdentifier(location.IdLocation);
                    consulta = "LocationIdentifier=" + identifier.Identifier;
                    query6 = "| savedsearch sd_access_type_location_interval LocationIdentifier_String=\"" + consulta + "\" " + timefilter;
                }
                else
                {
                    consulta = string.Format("IdHotel = {0}", hotel.IdHotel);
                    query6 = "| savedsearch sd_access_type_idhotel_interval_string IdHotel_String=\"" + consulta + "\" " + timefilter;
                }

                var args6 = new JobArgs { };

                json = "{\"message\": \"ok\", \"items\": [";
                // visualización de los datos
                using (SearchResultStream resultStream = await service.SearchOneShotAsync(query6, args: args6))
                {
                    foreach (SearchResult result in resultStream)
                    {
                        json += "{\"label\": \"" + result.GetValue("Description") + "\", \"value\" : " + result.GetValue("count(UserName)") + "},";
                    }
                }

                json = json.Substring(0, json.Length - 1);
                json += "]}";
            }
            catch (Exception ex)
            {
            }

            // json = "{\"code\":\"OK\",\"message\":\"\"}";
            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();

        }

        public void ProcessEmtpy(HttpContext context)
        {
            // Set the content type and encoding for JSON
            context.Response.ContentType = "application/json";
            context.Response.ContentEncoding = Encoding.UTF8;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }

    public class HistogramaElement
    {
        public int hora { get; set; }

        public string value { get; set; }
    }
}