﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Wifi360.Data.Controllers;
using Wifi360.Data.Model;

namespace Wifi360.handlers
{
    /// <summary>
    /// Descripción breve de UploadCSVHandler
    /// </summary>
    public class UploadCSVHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string error = string.Empty;
            if (context.Request.Files.Count > 0)
            {
                HttpFileCollection files = context.Request.Files;

                FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));

                //PARAMETRO OBLIGATORIOS

                string idroom = (context.Request.Params["idroom"] == null ? string.Empty : context.Request.Params["idroom"].ToString());
                string idbillingtype = (context.Request.Params["idbillingType"] == null ? string.Empty : context.Request.Params["idbillingType"].ToString());
                string start = (context.Request.Params["start"] == null ? string.Empty : context.Request.Params["start"].ToString());
                string end = (context.Request.Params["end"] == null ? string.Empty : context.Request.Params["end"].ToString());
                string comment = (context.Request.Params["comment"] == null ? string.Empty : context.Request.Params["comment"].ToString());

                Rooms room = BillingController.GetRoom(Int32.Parse(idroom));
                BillingTypes billingType = BillingController.ObtainBillingType(Int32.Parse(idbillingtype));
                Locations2 location = BillingController.GetLocation(billingType.IdLocation);
                Hotels hotel = BillingController.GetHotel(location.IdHotel);
                

                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFile file = files[i];
                    if (Path.GetExtension(file.FileName) == ".csv")
                    {
                        StreamReader csvreader = new StreamReader(file.InputStream);
                        while (!csvreader.EndOfStream)
                        {
                            var line = csvreader.ReadLine();
                            var values = line.Split(';');

                            Users existuser = BillingController.GetRadiusUser(values[0].ToUpper(), hotel.IdHotel);
                            if (existuser.IdUser.Equals(0) || !existuser.Enabled)
                            {
                                Users user = new Users();

                                user.IdHotel = hotel.IdHotel;
                                user.IdRoom = room.IdRoom;
                                user.IdBillingType = billingType.IdBillingType;
                                user.BWDown = billingType.BWDown;
                                user.BWUp = billingType.BWUp;
                                user.IdLocation = location.IdLocation;

                                user.ValidSince = string.IsNullOrEmpty(start) ? DateTime.Now : DateTime.Parse(start);
                                user.ValidTill = string.IsNullOrEmpty(end) ? user.ValidSince.AddHours(billingType.ValidTill) : DateTime.Parse(end).AddHours(23).AddMinutes(59);

                                //CALCULAMOS EL TIMECREDIT PARA RECORTARLO EN TAL CASO
                                int credit = billingType.TimeCredit / billingType.MaxDevices; //Calculamos el tiempo por dispositivo
                                if (user.ValidTill < user.ValidSince.AddSeconds(credit))
                                {
                                    TimeSpan diff = user.ValidTill - user.ValidSince;
                                    user.TimeCredit = billingType.MaxDevices * (int)diff.TotalSeconds;
                                }
                                else
                                    user.TimeCredit = billingType.TimeCredit;

                                user.MaxDevices = billingType.MaxDevices;
                                user.Priority = billingType.IdPriority;
                                user.VolumeDown = billingType.VolumeDown;
                                user.VolumeUp = billingType.VolumeUp;
                                user.Enabled = true;
                                user.Name = values[0].ToUpper();
                                user.Password = values[1].ToUpper();
                                user.CHKO = true;
                                user.BuyingFrom = 0;
                                user.BuyingCallerID = "NOT APPLICABLE";
                                user.Filter_Id = billingType.Filter_Id;
                                user.Comment = comment;
                                if (values.Length == 3)
                                    user.Comment = values[2];

                                BillingController.SaveUser(user);
                            }
                            else
                            {
                                error += string.Format("El usuario {0} ya existía, no se ha creado", values[0]);
                                error += Environment.NewLine;
                            }
                        }
                    }
                }

            }
            string json;
            if (string.IsNullOrEmpty(error))
                json = "NO";
            else
                json = error;


            context.Response.ContentType = "text/plain";
            context.Response.Write(json);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}