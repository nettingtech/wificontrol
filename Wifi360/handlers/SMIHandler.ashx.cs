﻿using ClosedXML.Excel;
using Wifi360.Data.Class;
using Wifi360.Data.Controllers;
using Wifi360.Data.Model;
using Wifi360.common;
using Wifi360.response;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Data.Objects.SqlClient;
using System.Net.Http;
using System.Xml;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using System.Xml.Linq;

namespace Wifi360.handlers
{
    /// <summary>
    /// Descripción breve de SMIHandler
    /// </summary>
    public class SMIHandler : IHttpHandler
    {
        private int _pageSize = -1;
        private int PageSize
        {
            get
            {
                if (_pageSize == -1)
                {
                    _pageSize = 12;
                    if (ConfigurationManager.AppSettings["PageSize"] != null)
                    {
                        if (Int32.TryParse(ConfigurationManager.AppSettings["PageSize"].ToString(), out _pageSize))
                            return _pageSize;
                    }
                }
                return _pageSize;
            }
        }

        private int _pageIndex = 0;
        private int PageIndex
        {
            set { _pageIndex = value; }
            get { return _pageIndex; }
        }

        public void ProcessRequest(HttpContext context)
        {
            if (string.IsNullOrEmpty(context.Request.Params["action"])) ProcessEmtpy(context);
            var jss = new JavaScriptSerializer();
            string json = new StreamReader(context.Request.InputStream).ReadToEnd();
            Dictionary<string, string> sData = jss.Deserialize<Dictionary<string, string>>(json);
            if (string.IsNullOrEmpty(json))
            {
                switch (context.Request.Params["action"].ToUpper())
                {
                    case "ACTIVITY": activity(context); break;
                    case "BILLINGMODULES": billingModules(context); break;
                    case "BILLINGTYPE": billingType(context); break;
                    case "BILLINGTYPES": billinTypes(context); break;
                    case "BILLINGTYPESUSERFILTER": billingTypesUserFilter(context); break;
                    case "BILLINGTYPESFILTER": billinTypesFilter(context); break;
                    case "BILLINGTYPESIOT": billinTypesIoT(context); break;
                    case "BILLINGTYPESREGULAR": billinTypesRegular(context); break;
                    case "BILLINGTYPESFULL": billinTypesFull(context); break;
                    case "BILLINGTYPESMODULE": billinTypesModule(context); break;
                    case "BILLINGTYPESLOCATION": billingTypesLocation(context); break;
                    case "BLACKLISTMACS": blacklistmacs(context); break;
                    case "CHECKCURRENCIES": CheckCurrencies(); break;
                    case "CLOSEDSESSIONS": closedsession(context); break;
                    case "CURRENCIES": currencies(context); break;
                    case "DASHBOARD": dashBoard(context); break;
                    case "DELETEBILL": deletebill(context); break;
                    case "DELETEBLACKLISTMACS": deleteblacklistmac(context); break;
                    case "DELETEFILTERPROFILE": deletefilterprofile(context); break;
                    case "DELETEPRIORITY": deletepriority(context); break;
                    case "DELETESURVEYQUESTION": deletesurveyQuestion(context); break;
                    case "DISABLEDUSERS": DisabledUsersManagement(context); break;
                    case "DISABLEDUSERSIOT": DisabledUsersManagementIOT(context); break;
                    case "DISCLAIMER": Disclaimer(context); break;
                    case "DISCLAIMERS": Disclaimers(context); break;
                    case "EXPORT": exportToExcel(context); break;
                    case "EXPORTACTIVITY": exportactivity(context); break;
                    case "EXPORTLOYALTY": exportToExcelLoyalty(context); break;
                    case "FDSUSERFORID": GetFDSUserForId(context); break;
                    case "FDSUSERS": GetFDSUsers(context); break;
                    case "FDSALLUSERS": GetAllFDSUsers(context); break;
                    case "FILTERPROFILES": filterprofiles(context); break;
                    case "FILTERPROFILESFILTER": filterprofilesFilter(context); break;
                    case "FILTERPROFILESUSERFILTER": filterprofilesUserFilter(context); break;
                    case "FILTERPROFILE": filterprofile(context); break;
                    case "GRAPHIC": graphic(context); break;
                    case "GROUPMEMBERS": GroupMembers(context); break;
                    case "LANGUAGES": languages(context); break;
                    case "LOGIN": login(context); break;
                    case "LOGOUT": logout(context); break;
                    case "LOCATION": Location(context); break;
                    case "LOCATIONMODULETEXT": LocationModuleText(context); break;
                    case "LOCATIONS": Locations(context); break;
                    case "LOCATIONSMODULETEXT": LocationsModuleText(context); break;
                    case "LOCATIONSNOALLZONES": LocationsNoAllZones(context); break;
                    case "LOCATIONSTEXT": LocationsText(context); break;
                    case "LOCATIONTEXT": LocationText(context); break;
                    case "PRIORITIES": Priorities(context); break;
                    case "PRIORITIESFILTER": PrioritiesFilter(context); break;
                    case "PRIORITIESUSERFILTER": prioritiesUserFilter(context); break;
                    case "PRIORITY": Priority(context); break;
                    case "ROOMS": Rooms(context); break;
                    case "ROOMSNOFILTER": RoomsNoFilter(context); break;
                    case "SAVEBILLINGTYPE": saveBillingType(context); break;
                    case "SAVESURVEYQUESTION": savesurveyQuestion(context); break;
                    case "SEARCHBILLING": searchBilling(context); break;
                    case "SEARCHFAILEDREQUEST": searchFailedRequest(context); break;
                    case "SEARCHLOG": searchLog(context); break;
                    case "SEARCHLOYALTY": searchLoyalty(context); break;
                    case "SEARCHUSERSPERIOD": searchUsersPeriod(context); break;
                    case "SELECTLOCATIONSBROWSER": selectLocationsBrowse(context); break;
                    case "SELECTMEMBERS": selectMembers(context); break;
                    case "SELECTSITES": selectSites(context); break;
                    case "SELECTSURVEY": selectSurvey(context); break;
                    case "SELECTALLGROUPS": selectallgroups(context); break;
                    case "SELECTALLSITES": selectAllSites(context); break;
                    case "SELECTALLTYPES": selectalltypes(context); break;
                    case "SELECTALLROLS": selectallrols(context); break;
                    case "SELECT_ALL_GROUPS_SITE_AND_LOCATIONS": select_all_groups_site_and_locations(context); break;
                    case "SITESETTING": sitesetting(context); break;
                    case "SITESETTINGSAVE": sitesettingsave(context); break;
                    case "SURVEYQUESTION": surveyQuestion(context); break;
                    case "USER": user(context); break;
                    case "USERDELETE": userdelete(context); break;
                    case "USERDETAILS": userdetails(context); break;
                    case "USERMANAGEMENT": userManagement(context); break;
                    case "USERMANAGEMENTIOT": userManagementIoT(context); break;
                    case "USERSONLINE": usersonline(context); break;
                    case "USERSONLINEIOT": usersonlineIoT(context); break;
                    case "USERSAVE": usersave(context); break;
                    case "USERSAVEEXTENDED": usersaveexteded(context); break;
                    case "USERSAVEUPGRADE": usersaveupgrade(context); break;
                    case "VENDORS": Vendors(context); break;
                    case "VOUCHERSMANAGEMENT": VouchersManagement(context); break;
                    case "USERSONLINELOCATION": UsersOnlineLocation(context);break;

                    default: ProcessEmtpy(context); break;
                }
            }
            else
            {
                switch (sData["action"].ToUpper())
                {
                    case "CLONESITE": CloneSite(context, sData); break;
                    case "CREATESITE": CreateSite(context, sData); break;                                       
                    case "CREATETICKET": CreateTicket(context, sData); break;
                    case "CREATEVOUCHER": CreateVoucher(context, sData); break;
                    case "EXPORTVOUCHER": ExportVoucher(context, sData); break;
                    case "LOCATIONTEXTSAVE": LocationTextsave(context, sData); break;
                    case "LOCATIONMODULETEXTSAVE": LocationModuleTextsave(context, sData); break;
                    case "SAVELOCATION": savelocation(context, sData); break;
                    case "DISCLAIMERSAVE": DisclaimerSave(context, sData); break;
                    case "SAVESTTINGS": savesettings(context, sData); break;
                    case "SAVEBILLINGTYPE": saveBillingType(context, sData); break;
                    case "SAVEFILTERPROFILE": saveFilterProfile(context, sData); break; 
                    case "SAVEPRIORITY": savePriority(context, sData); break;
                    case "SAVE_DASHBOARD_GROUPS": save_dashboard_groups(context, sData); break;
                    case "SAVEFDSUSER": SaveFDSUser(context, sData); break;
                    case "USERSAVE": usersave(context, sData); break;
                    case "FDSUSERSAVE": fdsusersave(context, sData); break;
                }
            }
        }

        #region GROUPS

        /// Selección de todos los GRUPOS del DASHBOARD
        private ListGroupResult selectallgroups(HttpContext context)
        {
            ListGroupResult response = new ListGroupResult();
            int PageNumber = 0;
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    string page = (context.Request["page"] == null ? string.Empty : context.Request["page"].ToString());
                    string name = (context.Request["name"] == null ? string.Empty : context.Request["name"].ToString());

                    int p = 0;
                    if (Int32.TryParse(page, out p))
                        PageIndex = p;

                    List<FDSGroups> Grupos = BillingController.GetGroups(name, PageIndex, PageSize, ref PageNumber);
                    response.list = new List<GroupResult>();
                    response.pageNumber = PageNumber;

                    foreach (FDSGroups Grupo_item in Grupos)
                    {
                        GroupResult result = new GroupResult();
                        result.Name = Grupo_item.Name;
                        result.idGroup = Grupo_item.IdGroup;

                        response.list.Add(result);
                    }


                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        /// Selección de todos los GRUPOS, SITE Y LOCATIONS de un GRUPO DASHBOARD
        private AllGroupSitesLocationsResult select_all_groups_site_and_locations(HttpContext context)
        {
            AllGroupSitesLocationsResult response = new AllGroupSitesLocationsResult();
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    string idGroup = (context.Request["id"] == null ? string.Empty : context.Request["id"].ToString());


                    List<FDSGroups> G_List = BillingController.GetGroups();


                    List<Hotels> S_List = BillingController.GetHotels();


                    List<Locations2> L_List = BillingController.GetLocations();



                    response.Group_list = new List<GroupResult>();
                    response.Site_list = new List<HotelResult>();
                    response.Location_list = new List<LocationResult>();

                    response.Group_list_selected = new List<GroupResult>();
                    response.Site_list_selected = new List<HotelResult>();
                    response.Location_list_selected = new List<LocationResult>();

                    //ALL GROUPS

                    foreach (FDSGroups temp_List in G_List.OrderBy(a => a.Name))
                    {
                        GroupResult Grupo = new GroupResult();
                        Grupo.idGroup = temp_List.IdGroup;
                        Grupo.Name = temp_List.Name;
                        response.Group_list.Add(Grupo);
                    }


                    foreach (Hotels temp_list in S_List.OrderBy(a => a.Name))
                    {
                        HotelResult Site = new HotelResult();
                        Site.IdHotel = temp_list.IdHotel;
                        Site.Name = temp_list.Name;
                        response.Site_list.Add(Site);
                    }


                    foreach (Locations2 temp_list in L_List.OrderBy(a => a.IdHotel))
                    {
                        LocationResult Location = new LocationResult();
                        Hotels h = (from te in S_List where te.IdHotel.Equals(temp_list.IdHotel) select te).FirstOrDefault();
                        Location.Idlocation = temp_list.IdLocation;
                        Location.Description = string.Format("{0} - {1}", h.Name, temp_list.Description);

                        response.Location_list.Add(Location);

                    }

                    int id = 0;
                    if ((Int32.TryParse(idGroup, out id)) && (idGroup != "0"))
                    {
                        FDSGroups group = BillingController.GetGroup(id);
                        response.Name = group.Name;
                        response.Latitude = group.Latitude;
                        response.Longitude = group.Longitude;
                        response.Currencie = group.Currency;
                        response.Realmshare = group.RealmShare;
                        response.Family = group.Family;

                        List<FDSGroups> G_List_selected = BillingController.GetGroupsPorGrupo(id);
                        List<Hotels> S_List_selected = BillingController.GetHotelsPorGrupo(id);
                        List<Locations2> L_List_selected = BillingController.GetLocationsPorGrupo(id);

                        //SELECTED 

                        foreach (FDSGroups temp_List in G_List_selected.OrderBy(a => a.Name))
                        {
                            GroupResult Grupo = new GroupResult();
                            Grupo.idGroup = temp_List.IdGroup;
                            Grupo.Name = temp_List.Name;
                            response.Group_list_selected.Add(Grupo);
                        }


                        foreach (Hotels temp_list in S_List_selected.OrderBy(a => a.Name))
                        {
                            HotelResult Site = new HotelResult();
                            Site.IdHotel = temp_list.IdHotel;
                            Site.Name = temp_list.Name;
                            response.Site_list_selected.Add(Site);
                        }


                        foreach (Locations2 temp_list in L_List_selected.OrderBy(a => a.IdHotel))
                        {
                            LocationResult Location = new LocationResult();
                            Hotels h = (from te in S_List where te.IdHotel.Equals(temp_list.IdHotel) select te).FirstOrDefault();
                            Location.Idlocation = temp_list.IdLocation;
                            Location.Description = string.Format("{0} - {1}", h.Name, temp_list.Description);

                            response.Location_list_selected.Add(Location);
                        }
                    }



                }
            }
            catch (Exception ex)
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;

        }

        ///Creación y salvado de grupo en fdsgroups y actualizacion de tabla members (Grupos, Sites y Locations)
        private FDSGroups save_dashboard_groups(HttpContext context, Dictionary<string, string> sData)
        {
            FDSGroups response = new FDSGroups();
            string json = string.Empty;
            int idGroup = 0;
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {

                    string groups = sData["GROUPS"];
                    string sites = sData["SITES"];
                    string locations = sData["LOCATIONS"];
                    string new_name = sData["Name"];
                    string new_currencie = sData["Currencie"];
                    string new_latitude = sData["latitude"];
                    string new_longitude = sData["longitude"];
                    bool new_realshare = Boolean.Parse(sData["realmshare"]);
                    bool new_family = Boolean.Parse(sData["family"]);

                    FDSGroups new_group = new FDSGroups();

                    try
                    {
                        if (Int32.TryParse(sData["id"], out idGroup))
                            new_group = BillingController.GetGroup(idGroup);
                    }
                    catch
                    {
                        ;
                    }

                    new_group.Name = new_name;
                    new_group.Currency = new_currencie;
                    new_group.Latitude = new_latitude;
                    new_group.Longitude = new_longitude;
                    new_group.Family = new_family;
                    new_group.RealmShare = new_realshare;
                    BillingController.SaveFDSGroups(new_group);

                    response.Name = new_group.Name;
                    response.IdGroup = new_group.IdGroup;

                    //ACTUALIZACION DE GRUPOS
                    string[] ids_groups = groups.Split(',');

                    List<string> ids_groups_to_add = new List<string>(ids_groups);
                    List<FDSGroupsMembers> groups_exits = BillingController.SelectFDSGroupsMembers(new_group.IdGroup);

                    foreach (FDSGroupsMembers member in groups_exits)
                    {
                        if (member.IdMemberType.Equals(1))
                        {
                            var results = Array.FindAll(ids_groups, s => s.Equals(member.IdMember.ToString()));
                            if (results.Length == 0)
                            {
                                BillingController.DeleteGroupMember(member.Id);
                            }
                            else
                            {
                                ids_groups_to_add.Remove(member.IdMember.ToString());
                            }
                        }
                    }
                    //ADD MIEMBROS TIPO GRUPO EN TABLA FDSGroupsMembers DEL GRUPO PRINCIPAL
                    if (ids_groups_to_add.Count > 0)
                    {
                        foreach (string id in ids_groups_to_add)
                        {
                            if (!id.Equals("null"))
                            {
                                FDSGroupsMembers new_member = new FDSGroupsMembers();
                                new_member.IdGroup = new_group.IdGroup;
                                new_member.IdMemberType = 1;
                                new_member.IsParent = false;
                                new_member.IdMember = int.Parse(id);
                                BillingController.SaveFDSGroupsMember(new_member);

                            }
                        }
                    }

                    //ACTUALIZACION DE SITES
                    string[] ids_sites = sites.Split(',');
                    List<string> ids_sites_to_add = new List<string>(ids_sites);
                    List<FDSGroupsMembers> sites_exits = BillingController.SelectFDSGroupsMembers(new_group.IdGroup);
                    foreach (FDSGroupsMembers member in sites_exits)
                    {
                        if (member.IdMemberType.Equals(2))
                        {
                            var results = Array.FindAll(ids_sites, s => s.Equals(member.IdMember.ToString()));
                            if (results.Length == 0)
                            {
                                BillingController.DeleteGroupMember(member.Id);
                            }
                            else
                            {
                                ids_sites_to_add.Remove(member.IdMember.ToString());
                            }
                        }
                    }
                    //ADD MIEMBROS TIPO SITE TABLA FDSGroupsMembers DEL GRUPO PRINCIPAL
                    if (ids_sites_to_add.Count > 0)
                    {
                        foreach (string id in ids_sites)
                        {
                            if (!id.Equals("null"))
                            {
                                FDSGroupsMembers new_member = new FDSGroupsMembers();
                                new_member.IdGroup = new_group.IdGroup;
                                new_member.IdMemberType = 2;
                                new_member.IsParent = false;
                                new_member.IdMember = int.Parse(id);
                                BillingController.SaveFDSGroupsMember(new_member);
                            }
                        }
                    }


                    //ACTUALIZACION DE LOCATIONS
                    string[] ids_locations = locations.Split(',');
                    List<string> ids_locations_to_add = new List<string>(ids_locations);
                    List<FDSGroupsMembers> Locations_exits = BillingController.SelectFDSGroupsMembers(new_group.IdGroup);
                    foreach (FDSGroupsMembers member in Locations_exits)
                    {
                        if (member.IdMemberType.Equals(3))
                        {
                            var results = Array.FindAll(ids_locations, s => s.Equals(member.IdMember.ToString()));
                            if (results.Length == 0)
                            {
                                BillingController.DeleteGroupMember(member.Id);
                            }
                            else
                            {
                                ids_locations_to_add.Remove(member.IdMember.ToString());
                            }
                        }
                    }
                    //ADD MIEMBROS TIPO LOCATION TABLA FDSGroupsMembers DEL GRUPO PRINCIPA
                    if (ids_locations_to_add.Count > 0)
                    {
                        foreach (string id in ids_locations_to_add)
                        {
                            if (!id.Equals("null"))
                            {
                                FDSGroupsMembers new_member = new FDSGroupsMembers();
                                new_member.IdGroup = new_group.IdGroup;
                                new_member.IdMemberType = 3;
                                new_member.IsParent = false;
                                new_member.IdMember = int.Parse(id);
                                BillingController.SaveFDSGroupsMember(new_member);
                            }
                        }
                    }

                    json = "{\"code\":\"OK\",\"message\":\"0x0039\"}";
                    
                }
                else
                {
                    json = "{\"code\":\"ERROR\",\"message\":\"0x9999\"}";

                }
            }
            catch (Exception ex)
            {
                json = "{\"code\":\"ERROR\",\"message\":\" " + ex.Message + ".\"}";

            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
            //context.Response.ContentType = "application/json";
            //context.Response.Write(json);
            //context.Response.End();
        }

        #endregion

        #region SITES
        ///creacion de site
        private void CreateSite(HttpContext context, Dictionary<string, string> sData)
        {
            string json = string.Empty;
            try
            {
                //OBTENEMOS EL TIPO DE USUARIOS
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));

                    //ADD NEW HOTEL
                    Hotels newHotel = new Hotels();

                    newHotel.Name = sData["name"];
                    newHotel.IdLanguage = Int32.Parse(sData["idlanguagedefault"]);
                    newHotel.CurrencyCode = sData["currencycode"];
                    newHotel.Latitude = sData["latitude"];
                    newHotel.Longitude = sData["longitude"];
                    newHotel.UrlHotel = sData["urlhotel"];
                    newHotel.IdVendor = Int32.Parse(sData["vendor"]);
                    newHotel.MacAuthenticate = Boolean.Parse(sData["macauthenticate"]);
                    newHotel.FilterContents = Boolean.Parse(sData["filtercontents"]);
                    newHotel.PrintLogo = Boolean.Parse(sData["printlogo"]);
                    newHotel.PrintSpeed = Boolean.Parse(sData["printspeed"]);
                    newHotel.PrintVolume = Boolean.Parse(sData["printvolume"]);
                    newHotel.NSE = sData["nse"];
                    newHotel.PrincipalNSEIP = sData["nseprincipalip"];
                    newHotel.Bandwidth = Int64.Parse(sData["bandwidth"]);
                    newHotel.BandwidthUp = Int64.Parse(sData["bandwidthup"]);
                    newHotel.LoginModule = Boolean.Parse(sData["loginmodule"]);
                    newHotel.FreeAccessModule = Boolean.Parse(sData["freeaccessmodule"]);
                    newHotel.PayAccessModule = Boolean.Parse(sData["payaccessmodule"]);
                    newHotel.SocialNetworksModule = Boolean.Parse(sData["socialnetworkmodule"]);
                    newHotel.CustomAccessModule = Boolean.Parse(sData["customaccessmodule"]);
                    newHotel.VoucherModule = Boolean.Parse(sData["voucheraccessmodule"]);
                    newHotel.FreeAccessRepeatTime = Int32.Parse(sData["freeaccessrepeattime"]);
                    newHotel.PayPalUser = sData["paypaluser"];
                    newHotel.UrlReturnPaypal = sData["urlreturnpaypal"];
                    newHotel.MACBlocking = Boolean.Parse(sData["macblocking"]);
                    newHotel.MACBannedInterval = Int32.Parse(sData["macbannedinterval"]);
                    newHotel.MaxMACAttemps = Int32.Parse(sData["maxmacattemps"]);
                    newHotel.MACAttempsInterval = Int32.Parse(sData["macattempsinterval"]);
                    newHotel.MailFrom = sData["mailfrom"];
                    newHotel.SMTP = sData["smtp"];
                    newHotel.SmtpPort = sData["smtpport"];
                    newHotel.SmtpSSL = Boolean.Parse(sData["smtpssl"]);
                    newHotel.SmtpUser = sData["smtpuser"];
                    newHotel.SmtpPassword = sData["smtppassword"];
                    newHotel.Survey = Boolean.Parse(sData["survey"]);
                    newHotel.CurrencyExchange = 1;

                    BillingController.SaveHotel(newHotel);

                    //ADD Parametros socialnetwork Tabla SITESOCIALNETWORKS
                    string SN_new_site = sData["SocialNetworks"];
                    string[] valuesSN = SN_new_site.Split('|');

                    foreach (string valSN in valuesSN)
                    {
                        string[] val = valSN.Split('=');
                        SitesSocialNetworks new_siteSN = new SitesSocialNetworks();
                        new_siteSN.IdSite = newHotel.IdHotel;
                        new_siteSN.IdSocialNetwork = int.Parse(val[0]);
                        new_siteSN.Active = bool.Parse(val[1]);

                        BillingController.SaveSiteSocialNetwork(new_siteSN);
                    }


                    //BORRAR

                    //BORRAR

                    //Add Locations for New Site

                    string AllLocationNames = sData["LocationNames"].ToString();

                    AllLocationNames = $"{AllLocationNames},Internal,All_Zones"; //Añadimos los Locations por defecto

                    string[] LocationValues = AllLocationNames.Split(',');

                    List<Languages> languages = BillingController.GetLanguages();
                    List<BillingModules> modules = BillingController.ObtainBillingModules();

                    foreach (string New_Values in LocationValues)
                    {

                        Locations2 newLocation = new Locations2();
                        newLocation.IdHotel = newHotel.IdHotel;

                        newLocation.Value = New_Values;
                        newLocation.Description = New_Values;
                        newLocation.UrlLanding = newHotel.UrlHotel;
                        newLocation.DefaultModule = 1;
                        newLocation.Disclaimer = true;
                        newLocation.Default = false;
                        newLocation.Currency = newHotel.CurrencyCode;
                        newLocation.ExchangeCurrency = newHotel.CurrencyExchange;
                        newLocation.Latitude = newHotel.Latitude;
                        newLocation.Longitude = newHotel.Longitude;
                        newLocation.GMT = newHotel.GMT;
                        newLocation.LoginModule = newHotel.LoginModule;
                        newLocation.VoucherModule = newHotel.VoucherModule;
                        newLocation.FreeAccessModule = newHotel.FreeAccessModule;
                        newLocation.CustomAccessModule = newHotel.CustomAccessModule;
                        newLocation.PayAccessModule = newHotel.PayAccessModule;
                        newLocation.SocialNetworksModule = newHotel.SocialNetworksModule;
                        newLocation.MandatorySurvey = false;

                        BillingController.SaveLocation(newLocation);

                        //ADD Parametros socialnetwork Tabla SITESOCIALNETWORKS
                        //sData["SocialNetworks"];

                        List<SocialNetworks> list_social_net = BillingController.GetSocialNetwoks();
                        foreach (SocialNetworks LocalSN in list_social_net)
                        {
                            LocationsSocialNetworks newLocationSN = new LocationsSocialNetworks();
                            newLocationSN.IdLocation = newLocation.IdLocation;
                            newLocationSN.IdSocialNetwork = LocalSN.IdSocialNetwork;
                            newLocationSN.Active = false;

                            BillingController.SaveLocationSocialNetwork(newLocationSN);
                        }

                        //Create Location Text
                        foreach (Languages lang in languages)
                        {
                            LocationsTexts locationText = new LocationsTexts();
                            locationText.IdLanguage = lang.IdLanguage;
                            locationText.IdLocation = newLocation.IdLocation;
                            locationText.Text = string.Empty;

                            BillingController.SaveLocationText(locationText);

                            Wifi360.Data.Model.Disclaimers disclaimer1 = new Data.Model.Disclaimers();
                            disclaimer1.IdType = 1;
                            disclaimer1.IdLocation = newLocation.IdLocation;
                            disclaimer1.IdLanguage = lang.IdLanguage;
                            disclaimer1.IdHotel = newLocation.IdHotel;
                            disclaimer1.Text = string.Empty;
                            disclaimer1.Date = DateTime.Now.ToUniversalTime();
                            BillingController.SaveDisclaimer(disclaimer1);

                            Wifi360.Data.Model.Disclaimers disclaimer2 = new Data.Model.Disclaimers();
                            disclaimer2.IdType = 2;
                            disclaimer2.IdLocation = newLocation.IdLocation;
                            disclaimer2.IdLanguage = lang.IdLanguage;
                            disclaimer2.IdHotel = newLocation.IdHotel;
                            disclaimer2.Text = string.Empty;
                            disclaimer2.Date = DateTime.Now.ToUniversalTime();
                            BillingController.SaveDisclaimer(disclaimer2);
                        }

                        foreach (BillingModules mod in modules)
                        {
                            foreach (Languages lang in languages)
                            {
                                LocationModuleText newModuleText = new LocationModuleText();
                                newModuleText.IdLocation = newLocation.IdLocation;
                                newModuleText.IdBillingModule = mod.IdBillingModule;
                                newModuleText.IdLanguage = lang.IdLanguage;
                                newModuleText.Text = string.Empty;

                                BillingController.SaveLocationModuleText(newModuleText);
                            }
                        }

                    }

                    //Add Priorities for Site
                    string AllPriorities = sData["Priorities"].ToString();
                    string[] PrioritiesValues = AllPriorities.Split(',');

                    foreach (string new_priorities_value in PrioritiesValues)
                    {
                        Wifi360.Data.Model.Priorities new_priorities = new Wifi360.Data.Model.Priorities();
                        new_priorities.IdHotel = newHotel.IdHotel;
                        new_priorities.Description = new_priorities_value;
                        BillingController.SavePriority(new_priorities);
                    }

                    //ADD A NEW ROOM ON LOCATION
                    Wifi360.Data.Model.Rooms newRoom = new Data.Model.Rooms();
                    newRoom.IdHotel = newHotel.IdHotel;
                    newRoom.Name = "DEFAULT";
                    newRoom.CheckIn = false;
                    newRoom.CheckInDate = DateTime.Now.ToUniversalTime();
                    newRoom.CheckOutDate = DateTime.Now.ToUniversalTime();
                    newRoom.Blocked = false;
                    newRoom.STB_Mac = string.Empty;
                    newRoom.STB_IP = string.Empty;
                    newRoom.Default = false;
                    BillingController.SaveRooms(newRoom);

                    //ADD a NEW FLTERING PROFILE
                    Wifi360.Data.Model.FilteringProfiles newFilter = new Wifi360.Data.Model.FilteringProfiles();
                    newFilter.IdSite = newHotel.IdHotel;
                    newFilter.Description = "no_filter";
                    BillingController.SaveFilterProfile(newFilter);

                    //Parametros de configuración Tabla PARAMETROSCONF.
                    string longusername = sData["longusername"];
                    Wifi360.Data.Model.ParametrosConfiguracion newParametrosLONGUSERNAME = new ParametrosConfiguracion();
                    newParametrosLONGUSERNAME.IdSite = newHotel.IdHotel;
                    newParametrosLONGUSERNAME.Key = "LONGUSERNAME";
                    newParametrosLONGUSERNAME.value = longusername;
                    newParametrosLONGUSERNAME.Type = "N";
                    newParametrosLONGUSERNAME.Description = "Prefijo de los nombres de usuarios generados automaticamente";
                    BillingController.SaveParametroConfiguracion(newParametrosLONGUSERNAME);

                    string longpassword = sData["longpassword"];
                    Wifi360.Data.Model.ParametrosConfiguracion newParametrosLONGPASSWORD = new Wifi360.Data.Model.ParametrosConfiguracion();
                    newParametrosLONGPASSWORD.IdSite = newHotel.IdHotel;
                    newParametrosLONGPASSWORD.Key = "LONGPASSWORD";
                    newParametrosLONGPASSWORD.value = longusername;
                    newParametrosLONGPASSWORD.Type = "N";
                    newParametrosLONGPASSWORD.Description = "Longitud de la contraseña generado automaticamente";
                    BillingController.SaveParametroConfiguracion(newParametrosLONGPASSWORD);

                    string voucherprefix = sData["voucherprefix"];
                    Wifi360.Data.Model.ParametrosConfiguracion newParametrosVOUCHERPREFIX = new Wifi360.Data.Model.ParametrosConfiguracion();
                    newParametrosVOUCHERPREFIX.IdSite = newHotel.IdHotel;
                    newParametrosVOUCHERPREFIX.Key = "VOUCHERPREFIX";
                    newParametrosVOUCHERPREFIX.value = voucherprefix;
                    newParametrosVOUCHERPREFIX.Type = "S";
                    newParametrosVOUCHERPREFIX.Description = "Prefijo de los voucher generados automaticamente.";
                    BillingController.SaveParametroConfiguracion(newParametrosVOUCHERPREFIX);

                    string longvoucher = sData["longvoucherusername"];
                    Wifi360.Data.Model.ParametrosConfiguracion newParametrosLONGVOUCHERUSER = new Wifi360.Data.Model.ParametrosConfiguracion();
                    newParametrosLONGVOUCHERUSER.IdSite = newHotel.IdHotel;
                    newParametrosLONGVOUCHERUSER.Key = "LONGVOUCHERUSER";
                    newParametrosLONGVOUCHERUSER.value = longvoucher;
                    newParametrosLONGVOUCHERUSER.Type = "N";
                    newParametrosLONGVOUCHERUSER.Description = "Longitud del usuario voucher generado automaticamente.";
                    BillingController.SaveParametroConfiguracion(newParametrosLONGVOUCHERUSER);

                    string volume = sData["volumecombined"];
                    Wifi360.Data.Model.ParametrosConfiguracion newParametrosVOLUMECOMBINED = new Wifi360.Data.Model.ParametrosConfiguracion();
                    newParametrosVOLUMECOMBINED.IdSite = newHotel.IdHotel;
                    newParametrosVOLUMECOMBINED.Key = "VOLUMECOMBINED";
                    newParametrosVOLUMECOMBINED.value = volume;
                    newParametrosVOLUMECOMBINED.Type = "B";
                    newParametrosVOLUMECOMBINED.Description = "Prefijo de los voucher generados automaticamente.";
                    BillingController.SaveParametroConfiguracion(newParametrosVOLUMECOMBINED);

                    string valuedayly = sData["valuedaylypass"];
                    Wifi360.Data.Model.ParametrosConfiguracion newParametrosSITEPASSVALUE = new Wifi360.Data.Model.ParametrosConfiguracion();
                    newParametrosSITEPASSVALUE.IdSite = newHotel.IdHotel;
                    newParametrosSITEPASSVALUE.Key = "SITEPASSVALUE";
                    newParametrosSITEPASSVALUE.value = valuedayly;
                    newParametrosSITEPASSVALUE.Type = "S";
                    newParametrosSITEPASSVALUE.Description = "Valor de la clave diaria.";
                    BillingController.SaveParametroConfiguracion(newParametrosSITEPASSVALUE);


                    string enabledayly = sData["enableddaylypass"];
                    Wifi360.Data.Model.ParametrosConfiguracion newParametrosSITEPASSENABLE = new Wifi360.Data.Model.ParametrosConfiguracion();
                    newParametrosSITEPASSENABLE.IdSite = newHotel.IdHotel;
                    newParametrosSITEPASSENABLE.Key = "SITEPASSENABLE";
                    newParametrosSITEPASSENABLE.value = enabledayly;
                    newParametrosSITEPASSENABLE.Type = "B";
                    newParametrosSITEPASSENABLE.Description = "Activa o desactiva la variable de clave diaria";
                    BillingController.SaveParametroConfiguracion(newParametrosSITEPASSENABLE);

                }
                json = "{\"code\":\"OK\",\"message\":\"0x0037\"}";
            }
            catch (Exception ex)
            {
                CaptivePortalLogInternal internalLog = new CaptivePortalLogInternal();
                internalLog.NSEId = "";
                internalLog.CallerID = "";
                internalLog.Date = DateTime.Now;
                internalLog.UserName = "";
                internalLog.Password = "";
                internalLog.Page = "Create Site";
                internalLog.IdLocation = 0;
                internalLog.NomadixResponse = "ERROR NO CONTEMPLADO - " + ex.Message;
                internalLog.RadiusResponse = "OK";

                BillingController.SaveCaptivePortalLogInternal(internalLog);

                json = "{\"code\":\"ERROR\",\"message\":\"0x9999\"}";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();
        }

        ///clonación de site
        private void CloneSite(HttpContext context, Dictionary<string, string> sData)
        {
            string json = string.Empty;
            try
            {
                //OBTENEMOS EL TIPO DE USUARIOS
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));

                    bool cloneBillingTypes = bool.Parse(sData["EnabledcopyBilling"]);
                    bool cloneBilling = bool.Parse(sData["EnabledcopyBillingCount"]);
                    bool cloneUsers = bool.Parse(sData["EnabledcopyUsers"]);
                    bool cloneLoyalties = bool.Parse(sData["EnabledcopyLoyalties"]);
                    bool cloneClosedSessions = bool.Parse(sData["EnabledcopyClosedSessions"]);
                    bool LocationsIdentifier = bool.Parse(sData["EnabledcopyLocationsIdentifier"]);
                    bool MerakiLocationsGPS = bool.Parse(sData["EnabledcopyMerakiLocationsGPS"]);
                    bool sessionsLog = bool.Parse(sData["EnabledcopySessionsLog"]);


                    //PRIMERA PARTE DE LA CLONACION                   

                    //Search ID HOTEL to Clone
                    int id_HoteltoClone = int.Parse(sData["idHoteltoClone"]);

                    //Get Hotel to Clone
                    Hotels newHotel = new Hotels();
                    newHotel = BillingController.GetHotel(id_HoteltoClone);
                    newHotel.IdHotel = 0;  //nos aseguramos que haga Insert
                    newHotel.Name = sData["NameNewSite"];

                    //ADD NEW HOTEL CLONED
                    BillingController.SaveHotel(newHotel);
                    var id_NewHotel = newHotel.IdHotel;

                    //Add Priorities for Site  [Creación de listas para reasignar los IDs ya que sólo nos interesa la descripición]
                    //Implica IDHOTEL                    
                    List<Wifi360.Data.Model.Priorities> PrioritiesToClone = BillingController.GetPriorities(id_HoteltoClone);
                    List<Wifi360.Data.Model.Priorities> ListClonedPriorities = new List<Data.Model.Priorities>();
                    foreach (Wifi360.Data.Model.Priorities cloned_priorities in PrioritiesToClone)
                    {
                        Wifi360.Data.Model.Priorities new_priorities_value = new Data.Model.Priorities();
                        new_priorities_value.Description = cloned_priorities.Description;
                        new_priorities_value.IdPriority = 0;
                        new_priorities_value.IdHotel = id_NewHotel;

                        BillingController.SavePriority(new_priorities_value);
                        ListClonedPriorities.Add(new_priorities_value);
                    }

                    //Clone Filters Profile [Creación de listas para la reasignación de IDs sólo nos interesa Descripción]
                    //Implica IDHOTEL                    
                    List<Wifi360.Data.Model.FilteringProfiles> FilterProfileToCLone = BillingController.GetFilteringProfiles(id_HoteltoClone);
                    List<Wifi360.Data.Model.FilteringProfiles> ListClonedFilter = new List<FilteringProfiles>();
                    foreach (Wifi360.Data.Model.FilteringProfiles clone_filterProfile in FilterProfileToCLone)
                    {
                        FilteringProfiles new_filterProfile = new FilteringProfiles();
                        new_filterProfile.IdFilter = 0;
                        new_filterProfile.IdSite = id_NewHotel;
                        new_filterProfile.Description = clone_filterProfile.Description;

                        BillingController.SaveFilterProfile(new_filterProfile);
                        ListClonedFilter.Add(new_filterProfile);
                    }

                    //Clone Rooms for Cloned SITE
                    //Implica IDHOTEL                    
                    List<Rooms> RoomsToClone = BillingController.ObtainRooms(id_HoteltoClone);
                    List<Rooms> RoomsCloned = new List<Data.Model.Rooms>();    
                    foreach (Rooms clone_rooms in RoomsToClone)
                    {
                        Rooms new_rooms = new Data.Model.Rooms();
                        util.Map(clone_rooms, new_rooms);

                        new_rooms.IdRoom = 0;
                        new_rooms.IdHotel = id_NewHotel;

                        BillingController.SaveRooms(new_rooms);
                        RoomsCloned.Add(new_rooms);
                    }

                    //Clone Configuration Table PARAMETROSCONF.
                    //Implica IDHOTEL
                    List<Wifi360.Data.Model.ParametrosConfiguracion> ParametrosToCLone = BillingController.GetParametrosConfiguracion(id_HoteltoClone);
                    foreach (Wifi360.Data.Model.ParametrosConfiguracion clone_parametros in ParametrosToCLone)
                    {
                        ParametrosConfiguracion new_parametros = new ParametrosConfiguracion();
                        util.Map(clone_parametros, new_parametros);

                        new_parametros.IdParameter = 0;
                        new_parametros.IdSite = id_NewHotel;

                        BillingController.SaveParametroConfiguracion(new_parametros);
                    }

                    

                    //Search Locations 
                    List<Locations2> LocationstoClone = BillingController.GetLocations(id_HoteltoClone);
                    List<Locations2> LocationsCloned = new List<Locations2>();
                    foreach (Locations2 Clone_Location in LocationstoClone)
                    {
                        Locations2 newLocation = new Locations2();
                        util.Map(Clone_Location, newLocation);
                        int id_LocationtoClone = newLocation.IdLocation;
                        newLocation.IdLocation = 0;
                        newLocation.IdHotel = id_NewHotel;

                        BillingController.SaveLocation(newLocation);
                        LocationsCloned.Add(newLocation);
                        int id_LocationCloned = newLocation.IdLocation;

                        //Clone Parameters socialnetwork Tabla SITESOCIALNETWORKS
                        List<LocationsSocialNetworks> list_locations_sn_to_clone = BillingController.GetLocationSocialNetwoks(id_LocationtoClone);
                        foreach (LocationsSocialNetworks Clone_locations_sn in list_locations_sn_to_clone)
                        {
                            LocationsSocialNetworks New_locations_sn = new LocationsSocialNetworks();
                            util.Map(Clone_locations_sn, New_locations_sn);

                            New_locations_sn.IdLocationSocialNetwork = 0;
                            New_locations_sn.IdLocation = id_LocationCloned;

                            BillingController.SaveLocationSocialNetwork(New_locations_sn);
                        }

                        //Clone Location Text
                        List<Disclaimers> DisclamertoClone = BillingController.GetDisclaimers(id_LocationtoClone);
                        foreach (Disclaimers Clone_disclamers in DisclamertoClone)
                        {
                            Disclaimers new_disclamers = new Disclaimers();
                            util.Map(Clone_disclamers, new_disclamers);

                            new_disclamers.IdDisclaimer = 0;
                            new_disclamers.IdHotel = id_NewHotel;
                            new_disclamers.IdLocation = id_LocationCloned;

                            BillingController.SaveDisclaimer(new_disclamers);
                        }

                        //Clone LocationsText                       
                        List<LocationsTexts> LocationsTextToClone = BillingController.GetLocationsText(id_LocationtoClone);
                        foreach (LocationsTexts Clone_locationsText in LocationsTextToClone)
                        {
                            LocationsTexts new_locationsText = new LocationsTexts();
                            util.Map(Clone_locationsText, new_locationsText);

                            new_locationsText.IdLocationText = 0;
                            new_locationsText.IdLocation = id_LocationCloned;

                            BillingController.SaveLocationText(new_locationsText);
                        }

                        //Clone LocationsModules
                        List<Wifi360.Data.Model.LocationModuleText> LocationsModuleTextToClone = BillingController.GetLocationModuleTexts(id_LocationtoClone);
                        foreach (Wifi360.Data.Model.LocationModuleText Clone_LocationModule in LocationsModuleTextToClone)
                        {
                            Data.Model.LocationModuleText New_LocationModule = new Data.Model.LocationModuleText();
                            util.Map(Clone_LocationModule, New_LocationModule);

                            New_LocationModule.IdLocationModuleText = 0;
                            New_LocationModule.IdLocation = id_LocationCloned;

                            BillingController.SaveLocationModuleText(New_LocationModule);
                        }

                        //Clone BillingTypes                       
                        List<BillingTypes2> BillingTypesToClone = BillingController.ObtainAllBillingTypesLocation(id_LocationtoClone);
                        List<BillingTypes2> BillingTypesCloned = new List<BillingTypes2>();
                        if (cloneBillingTypes)
                        {

                            foreach (BillingTypes2 clone_BillingTypes in BillingTypesToClone)
                            {

                                BillingTypes2 new_billingTypes = new BillingTypes2();
                                util.Map(clone_BillingTypes, new_billingTypes);
                                
                                //SEARCH ID DE LOS DESCRIPCION EN PRIORIDADES
                                if ((PrioritiesToClone.Count != 0) && (new_billingTypes.IdPriority != 0))
                                {
                                    Wifi360.Data.Model.Priorities OldPriority = (from t in PrioritiesToClone where t.IdPriority.Equals(new_billingTypes.IdPriority) select t).FirstOrDefault();
                                    if (OldPriority == null)
                                    {
                                        new_billingTypes.IdPriority = 0;
                                    }
                                    else
                                    {
                                        Wifi360.Data.Model.Priorities NewPriority = (from t in ListClonedPriorities where t.Description.Equals(OldPriority.Description) select t).FirstOrDefault();
                                        new_billingTypes.IdPriority = NewPriority.IdPriority;
                                    }                                    
                                    
                                }else
                                {
                                    new_billingTypes.IdPriority = 0;
                                }                                    

                                //SEARCH ID DE LOS DESCRIPCIONS EN FILTERING PROFILE
                                if ((FilterProfileToCLone.Count != 0) && !new_billingTypes.Filter_Id.Equals(0))
                                {
                                    Wifi360.Data.Model.FilteringProfiles OldFilter = (from t in FilterProfileToCLone where t.IdFilter.Equals(new_billingTypes.Filter_Id) select t).FirstOrDefault();
                                    if (OldFilter == null)
                                    {
                                        new_billingTypes.Filter_Id = 0;
                                    }else
                                    {
                                        Wifi360.Data.Model.FilteringProfiles NewFilter = (from t in ListClonedFilter where t.Description.Equals(OldFilter.Description) select t).FirstOrDefault();
                                        new_billingTypes.Filter_Id = NewFilter.IdFilter;
                                    }                                    
                                }
                                else
                                {
                                    new_billingTypes.Filter_Id = 0;
                                }
                                new_billingTypes.IdHotel = newHotel.IdHotel;
                                new_billingTypes.IdLocation = id_LocationCloned;
                                //new_billingTypes.NextBillingType = null;
                                new_billingTypes.IdBillingType = 0;
                                
                                BillingController.SaveBillingType(new_billingTypes);
                                BillingTypesCloned.Add(new_billingTypes);
                                
                            }
                            //NextBillingTypes Cloned (replace old id for new id)
                            List<BillingTypes2> BillingTypesToCloneAndNextBillingType = BillingController.ObtainAllBillingTypesLocation(id_LocationCloned);
                            foreach (BillingTypes2 clone_BillingTypesAndNextBillingType in BillingTypesToCloneAndNextBillingType)
                            {
                                if (clone_BillingTypesAndNextBillingType.NextBillingType != null) {
                                    BillingTypes2 new_NextBillingType = (from t in BillingTypesToClone where t.IdBillingType.Equals(clone_BillingTypesAndNextBillingType.NextBillingType) select t).FirstOrDefault();
                                    BillingTypes2 new_NextBillingType2 = (from t in BillingTypesToCloneAndNextBillingType where t.Description.Equals(new_NextBillingType.Description) select t).FirstOrDefault();
                                    clone_BillingTypesAndNextBillingType.NextBillingType = new_NextBillingType2.IdBillingType;
                                    BillingController.SaveBillingType(clone_BillingTypesAndNextBillingType);
                                }
                            }

                            CaptivePortalLogInternal internalLogB = new CaptivePortalLogInternal();
                            internalLogB.NSEId = "";
                            internalLogB.CallerID = "";
                            internalLogB.Date = DateTime.Now;
                            internalLogB.UserName = "";
                            internalLogB.Password = "";
                            internalLogB.Page = "Clone Site";
                            internalLogB.IdLocation = 0;
                            internalLogB.NomadixResponse = "BILLINGTYPES CLONADOS EN SITE " + id_NewHotel + " LOCATION " + id_LocationCloned + " CORRECTAMENTE - ";
                            internalLogB.RadiusResponse = "OK";

                            BillingController.SaveCaptivePortalLogInternal(internalLogB);
                        }
                    }

                    List<FDSUsersAccess> usersAccessToClone = BillingController.GetFDSUserAccessesBySite(id_HoteltoClone);
                    List<FDSUsersAccess> usersAccessCloned = new List<FDSUsersAccess>();
                    List<FDSUsers> FDSUsersToClone = new List<FDSUsers>();
                    List<FDSUsers> FDSUsersCloned = new List<FDSUsers>();
                    foreach(FDSUsersAccess userAccessToClone in usersAccessToClone)
                    {
                        FDSUsersAccess userAccessCloned = new FDSUsersAccess();
                        util.Map(userAccessToClone, userAccessCloned);
                        userAccessCloned.Id = 0;
                        userAccessCloned.IdSite = id_NewHotel;
                        if (!userAccessCloned.IdLocation.Equals(0))
                        {
                            Locations2 locationToClone = (from t in LocationstoClone where t.IdLocation.Equals(userAccessToClone.IdLocation) select t).FirstOrDefault();
                            Locations2 locationCloned = (from t in LocationsCloned where t.Value.Equals(locationToClone.Value) select t).FirstOrDefault();
                            userAccessCloned.IdLocation = locationCloned.IdLocation;
                        }

                        FDSUsers userToClone = BillingController.GetFDSUser(userAccessToClone.IdUser);
                        FDSUsers userCloned = new FDSUsers();
                        util.Map(userToClone, userCloned);
                        userCloned.Username = string.Format("{0}_{1}", userCloned.Username, id_NewHotel);
                        userCloned.IdUser = 0;

                        BillingController.SaveFDSUser(userCloned);
                        userAccessCloned.IdUser = userCloned.IdUser;
                        BillingController.SaveFDSUserAccess(userAccessCloned);

                        FDSUsersToClone.Add(userToClone);
                        FDSUsersCloned.Add(userCloned);
                        usersAccessCloned.Add(userAccessCloned);

                    }


                    CaptivePortalLogInternal internalLog_fdsusers = new CaptivePortalLogInternal();
                    internalLog_fdsusers.NSEId = "";
                    internalLog_fdsusers.CallerID = "";
                    internalLog_fdsusers.Date = DateTime.Now;
                    internalLog_fdsusers.UserName = "";
                    internalLog_fdsusers.Password = "";
                    internalLog_fdsusers.Page = "Clone Site";
                    internalLog_fdsusers.IdLocation = 0;
                    internalLog_fdsusers.NomadixResponse = "CLONADO FDSUSERS EN SITE " + id_NewHotel + " CORRECTAMENTE - ";
                    internalLog_fdsusers.RadiusResponse = "OK";

                    BillingController.SaveCaptivePortalLogInternal(internalLog_fdsusers);


                    CaptivePortalLogInternal internalLog_1a = new CaptivePortalLogInternal();
                    internalLog_1a.NSEId = "";
                    internalLog_1a.CallerID = "";
                    internalLog_1a.Date = DateTime.Now;
                    internalLog_1a.UserName = "";
                    internalLog_1a.Password = "";
                    internalLog_1a.Page = "Clone Site";
                    internalLog_1a.IdLocation = 0;
                    internalLog_1a.NomadixResponse = "CLONADO TRAMO1 EN SITE " + id_NewHotel + " CORRECTAMENTE - ";
                    internalLog_1a.RadiusResponse = "OK";

                    BillingController.SaveCaptivePortalLogInternal(internalLog_1a);


                    //SEGUNDA PARTE DE LA CLONACION

                    List<Users> UsersToCloneFull = new List<Users>();
                    List<Users> UsersClonedFull = new List<Users>();

                    foreach (Locations2 Locations in LocationstoClone)
                    {
                        int id_LocationtoClone = Locations.IdLocation;                        
                        int id_LocationCloned = (from t in LocationsCloned where t.Description.Equals(Locations.Description) select t.IdLocation).FirstOrDefault();

                        //Clone USUARIOS
                        //List<Users> pre_UsersToClone = BillingController.GetAllUsersLocation(id_LocationtoClone);
                        List<Users> UsersToClone = BillingController.GetAllUsersLocation(id_LocationtoClone);
                        List<Users> UsersCloned = new List<Users>();
                        if (cloneUsers)
                        {
                            List<BillingTypes2> BillingTypesToClone = BillingController.ObtainBillingTypes2All(id_HoteltoClone);
                            List<BillingTypes2> BillingTypesCloned = BillingController.ObtainBillingTypes2All(id_NewHotel);

                            foreach (Users clone_Users in UsersToClone)
                            {
                                Users new_user = new Users();
                                util.Map(clone_Users, new_user);

                                new_user.IdUser = 0;
                                new_user.IdLocation = id_LocationCloned;
                                new_user.IdHotel = id_NewHotel;

                                FDSUsers FDSUserToClone = (from t in FDSUsersToClone where t.IdUser.Equals(clone_Users.IdFDSUser) select t).FirstOrDefault();
                                if (FDSUserToClone != null)
                                {
                                    FDSUsers FDSUserCloned = (from t in FDSUsersCloned where t.Username.Equals(string.Format("{0}_{1}", FDSUserToClone.Username, id_NewHotel)) select t).FirstOrDefault();
                                    if (FDSUserCloned != null)
                                        new_user.IdFDSUser = FDSUserCloned.IdUser;
                                }

                                //SEARCH ID BILLINGTYPE
                                if ((BillingTypesToClone.Count != 0) && (new_user.IdBillingType != 0))
                                {
                                    Wifi360.Data.Model.BillingTypes2 OldBillingTypes = (from t in BillingTypesToClone where t.IdBillingType.Equals(new_user.IdBillingType) select t).FirstOrDefault();
                                    if (OldBillingTypes == null)
                                    {
                                        Wifi360.Data.Model.BillingTypes2 TempBillingTypes = (from h in BillingTypesCloned select h).FirstOrDefault();
                                        new_user.IdBillingType = TempBillingTypes.IdBillingType;
                                    }
                                    else
                                    { 
                                        Wifi360.Data.Model.BillingTypes2 NewBillingTypes = (from t in BillingTypesCloned where t.Description.Equals(OldBillingTypes.Description) select t).FirstOrDefault();
                                        new_user.IdBillingType = NewBillingTypes.IdBillingType;
                                    }
                                }
                                //SEARCH ID ROOM
                                if ((RoomsToClone.Count != 0)  && (new_user.IdRoom != 0))
                                {
                                    Wifi360.Data.Model.Rooms OldRooms = (from t in RoomsToClone where t.IdRoom.Equals(new_user.IdRoom) select t).FirstOrDefault();
                                    if (OldRooms == null)
                                    {
                                        Wifi360.Data.Model.Rooms defaulROOM = (from t in RoomsCloned where t.Name.ToUpper().Equals("DEFAULT") select t).FirstOrDefault();
                                        new_user.IdRoom = defaulROOM.IdRoom;
                                    }
                                    else
                                    { 
                                        Wifi360.Data.Model.Rooms NewRooms = (from t in RoomsCloned where t.Name.Equals(OldRooms.Name) select t).FirstOrDefault();
                                        new_user.IdRoom = NewRooms.IdRoom;
                                    }
                                }
                                //SEARCH ID PRIORITIES
                                if ((PrioritiesToClone.Count != 0) && (new_user.Priority != 0))
                                {
                                    Wifi360.Data.Model.Priorities OldPriority = (from t in PrioritiesToClone where t.IdPriority.Equals(new_user.Priority) select t).FirstOrDefault();
                                    if (OldPriority == null)
                                    {
                                        new_user.Priority = 0;
                                    }
                                    else
                                    {
                                        Wifi360.Data.Model.Priorities NewPriority = (from t in ListClonedPriorities where t.Description.Equals(OldPriority.Description) select t).FirstOrDefault();
                                        new_user.Priority = NewPriority.IdPriority;
                                    }
                                }
                                //SEARCH ID FILTERS
                                if ((FilterProfileToCLone.Count != 0) && (new_user.Filter_Id != 0))
                                {
                                    Wifi360.Data.Model.FilteringProfiles OldFilter = (from t in FilterProfileToCLone where t.IdFilter.Equals(new_user.Filter_Id) select t).FirstOrDefault();
                                    if (OldFilter == null)
                                    {
                                        new_user.Filter_Id = 0;
                                    }
                                    else
                                    {
                                        Wifi360.Data.Model.FilteringProfiles NewFilter = (from t in ListClonedFilter where t.Description.Equals(OldFilter.Description) select t).FirstOrDefault();
                                        new_user.Filter_Id = NewFilter.IdFilter;
                                    }                                    
                                }

                                BillingController.SaveUser(new_user);
                                UsersCloned.Add(new_user);
                            }

                            //añadimos los usuarios a USERSFULL LIST
                            UsersToCloneFull = UsersToCloneFull.Concat(UsersToClone).ToList();
                            UsersClonedFull = UsersClonedFull.Concat(UsersCloned).ToList();

                            //LOGs
                            CaptivePortalLogInternal internalLogUsers = new CaptivePortalLogInternal();
                            internalLogUsers.NSEId = "";
                            internalLogUsers.CallerID = "";
                            internalLogUsers.Date = DateTime.Now;
                            internalLogUsers.UserName = "";
                            internalLogUsers.Password = "";
                            internalLogUsers.Page = "Clone Site";
                            internalLogUsers.IdLocation = 0;
                            internalLogUsers.NomadixResponse = "USUARIOS CLONADOS EN SITE " + id_NewHotel + " LOCATION " + id_LocationCloned + " CORRECTAMENTE - ";
                            internalLogUsers.RadiusResponse = "OK";

                            BillingController.SaveCaptivePortalLogInternal(internalLogUsers);
                        }

                        //Clone Loyalties
                        if (cloneLoyalties)
                        {
                            BillingController.CloneLoyaltiesByLocation(id_HoteltoClone, id_NewHotel , id_LocationtoClone, id_LocationCloned);

                            CaptivePortalLogInternal internalLogLocyalites = new CaptivePortalLogInternal();
                            internalLogLocyalites.NSEId = "";
                            internalLogLocyalites.CallerID = "";
                            internalLogLocyalites.Date = DateTime.Now;
                            internalLogLocyalites.UserName = "";
                            internalLogLocyalites.Password = "";
                            internalLogLocyalites.Page = "Clone Site";
                            internalLogLocyalites.IdLocation = 0;
                            internalLogLocyalites.NomadixResponse = "LOYALTIES CLONADOS EN SITE "+ id_NewHotel +" LOCATION "+ id_LocationCloned + " CORRECTAMENTE - ";
                            internalLogLocyalites.RadiusResponse = "OK";

                            BillingController.SaveCaptivePortalLogInternal(internalLogLocyalites);
                        }

                        //Clone Location Identify
                        if (LocationsIdentifier)
                        {
                            List<FDSLocationsIdentifier> LocIdenToCLone = BillingController.GetFDSLocationsIdentifiers(id_LocationtoClone, id_HoteltoClone);
                            foreach (FDSLocationsIdentifier cloned_LocIdeds in LocIdenToCLone)
                            {
                                FDSLocationsIdentifier new_LocIdeds = new FDSLocationsIdentifier();
                                util.Map(cloned_LocIdeds, new_LocIdeds);

                                new_LocIdeds.Id = 0;
                                new_LocIdeds.IdLocation = id_LocationCloned;
                                new_LocIdeds.IdSite = id_NewHotel;

                                BillingController.SaveLocationIdentifiers(new_LocIdeds); //Solo hace insert no Update

                                CaptivePortalLogInternal internalLogUsers = new CaptivePortalLogInternal();
                                internalLogUsers.NSEId = "";
                                internalLogUsers.CallerID = "";
                                internalLogUsers.Date = DateTime.Now;
                                internalLogUsers.UserName = "";
                                internalLogUsers.Password = "";
                                internalLogUsers.Page = "Clone Site";
                                internalLogUsers.IdLocation = 0;
                                internalLogUsers.NomadixResponse = "LocationsIdentifier CLONADOS EN SITE " + id_NewHotel + " LOCATION " + id_LocationCloned + " CORRECTAMENTE - ";
                                internalLogUsers.RadiusResponse = "OK";

                                BillingController.SaveCaptivePortalLogInternal(internalLogUsers);
                            }
                        }

                        //Clone Meraki Locations GPS
                        if (MerakiLocationsGPS)
                        {
                            List<MerakiLocationsGPS> MerakiToCLone = BillingController.GetAllMerakiLocationsGPSSite(id_LocationtoClone, id_HoteltoClone);
                            foreach (MerakiLocationsGPS clone_Meraki in MerakiToCLone)
                            {
                                MerakiLocationsGPS new_Meraki = new Data.Model.MerakiLocationsGPS();
                                util.Map(clone_Meraki, new_Meraki);

                                new_Meraki.Id = 0;
                                new_Meraki.IdLocation = id_LocationCloned;
                                new_Meraki.IdSite = id_NewHotel;

                                BillingController.SaveMerakiLocationsGPS(new_Meraki);

                                CaptivePortalLogInternal internalLogUsers = new CaptivePortalLogInternal();
                                internalLogUsers.NSEId = "";
                                internalLogUsers.CallerID = "";
                                internalLogUsers.Date = DateTime.Now;
                                internalLogUsers.UserName = "";
                                internalLogUsers.Password = "";
                                internalLogUsers.Page = "Clone Site";
                                internalLogUsers.IdLocation = 0;
                                internalLogUsers.NomadixResponse = "MerakiLocationsGPS CLONADOS EN SITE " + id_NewHotel + " LOCATION " + id_LocationCloned + " CORRECTAMENTE - ";
                                internalLogUsers.RadiusResponse = "OK";

                                BillingController.SaveCaptivePortalLogInternal(internalLogUsers);
                            }
                        }
                    }

                    //Clone BILLINGS ONLY if USER, BILLINGTYPES is CLONED
                    if ((cloneBilling) & (cloneBillingTypes) & (cloneUsers))
                    {
                        List<Billings> BillingsToClone = BillingController.ObtainBillings(id_HoteltoClone);

                        List<BillingTypes2> BillingTypesToClone = BillingController.ObtainBillingTypes2All(id_HoteltoClone);
                        List<BillingTypes2> BillingTypesCloned = BillingController.ObtainBillingTypes2All(id_NewHotel);

                        foreach (Billings Clone_billings in BillingsToClone)
                        {
                            Billings New_billings = new Billings();
                            util.Map(Clone_billings, New_billings);

                            Locations2 locationToClone = (from t in LocationstoClone where t.IdLocation.Equals(Clone_billings.IdLocation) select t).FirstOrDefault();
                            if (locationToClone != null)
                            {
                                Locations2 locationCloned = (from t in LocationsCloned where t.Value.Equals(locationToClone.Value) select t).FirstOrDefault();
                                if (LocationsCloned != null && Clone_billings.IdLocation != 0)
                                    New_billings.IdLocation = locationCloned.IdLocation;
                                else
                                    New_billings.IdLocation = 0;
                            }
                            else
                                New_billings.IdLocation = 0;

                            New_billings.IdBilling = 0;
                            New_billings.IdHotel = id_NewHotel;

                            FDSUsers FDSUserToClone = (from t in FDSUsersToClone where t.IdUser.Equals(Clone_billings.IdFDSUser) select t).FirstOrDefault();
                            if (FDSUserToClone != null)
                            {
                                FDSUsers FDSUserCloned = (from t in FDSUsersCloned where t.Username.Equals(string.Format("{0}_{1}", FDSUserToClone.Username, id_NewHotel)) select t).FirstOrDefault();
                                if (FDSUserCloned != null)
                                    New_billings.IdFDSUser = FDSUserCloned.IdUser;
                            }


                            //FIND ID FOR ROOMS
                            if ((RoomsToClone.Count != 0) && (New_billings.IdRoom != 0))
                            {
                                Wifi360.Data.Model.Rooms OldRooms = (from t in RoomsToClone where t.IdRoom.Equals(New_billings.IdRoom) select t).FirstOrDefault();
                                if (OldRooms == null)
                                {
                                    Wifi360.Data.Model.Rooms defaulROOM = (from t in RoomsCloned where t.Name.ToUpper().Equals("DEFAULT") select t).FirstOrDefault();
                                    New_billings.IdRoom = defaulROOM.IdRoom;
                                }
                                else
                                {
                                    Wifi360.Data.Model.Rooms NewRooms = (from t in RoomsCloned where t.Name.Equals(OldRooms.Name) select t).FirstOrDefault();
                                    New_billings.IdRoom = NewRooms.IdRoom;
                                }

                            }
                            //FIND ID FOR BILLINGTYPES
                            if ((BillingTypesToClone.Count != 0) && (New_billings.IdBillingType != 0))
                            {
                                Wifi360.Data.Model.BillingTypes2 OldBillingTypes = (from t in BillingTypesToClone where t.IdBillingType.Equals(New_billings.IdBillingType) select t).FirstOrDefault();
                                if (OldBillingTypes == null)
                                {
                                    New_billings.IdBillingType = 0;
                                }
                                else
                                {
                                    Wifi360.Data.Model.BillingTypes2 NewBillingTypes = (from t in BillingTypesCloned where t.Description.Equals(OldBillingTypes.Description) select t).FirstOrDefault();
                                    New_billings.IdBillingType = NewBillingTypes.IdBillingType;
                                }

                            }
                            //FIND ID FOR USERS
                            if (UsersToCloneFull.Count != 0)
                            {
                                Wifi360.Data.Model.Users OldUser = (from t in UsersToCloneFull where t.IdUser.Equals(New_billings.IdUser) select t).FirstOrDefault();
                                if (OldUser == null)
                                {
                                    New_billings.IdUser = 0;
                                }
                                else
                                {
                                    Wifi360.Data.Model.Users NewUser = (from t in UsersClonedFull where t.Name.Equals(OldUser.Name) select t).FirstOrDefault();
                                    New_billings.IdUser = NewUser.IdUser;
                                }

                            }

                            BillingController.SaveBilling(New_billings);

                        }
                        CaptivePortalLogInternal internalLogBillings = new CaptivePortalLogInternal();
                        internalLogBillings.NSEId = "";
                        internalLogBillings.CallerID = "";
                        internalLogBillings.Date = DateTime.Now;
                        internalLogBillings.UserName = "";
                        internalLogBillings.Password = "";
                        internalLogBillings.Page = "Clone Site";
                        internalLogBillings.IdLocation = 0;
                        internalLogBillings.NomadixResponse = "BILLINGS CLONADOS EN SITE " + id_NewHotel + "  CORRECTAMENTE - ";
                        internalLogBillings.RadiusResponse = "OK";

                        BillingController.SaveCaptivePortalLogInternal(internalLogBillings);
                    }

                    CaptivePortalLogInternal internalLogt2 = new CaptivePortalLogInternal();
                    internalLogt2.NSEId = "";
                    internalLogt2.CallerID = "";
                    internalLogt2.Date = DateTime.Now;
                    internalLogt2.UserName = "";
                    internalLogt2.Password = "";
                    internalLogt2.Page = "Clone Site";
                    internalLogt2.IdLocation = 0;
                    internalLogt2.NomadixResponse = "CLONADO TRAMO2 EN SITE " + id_NewHotel + " CORRECTAMENTE - ";
                    internalLogt2.RadiusResponse = "OK";

                    BillingController.SaveCaptivePortalLogInternal(internalLogt2);

                    //Clone Closed Sessions no se Clona Identificación de Localización(Sólo copiamos su valor)
                    //Clonado por SP (Procedimiento de almacenamiento)
                    //Implica IDHOTEL
                    if (cloneClosedSessions)
                    {
                        BillingController.CloneClosedSession(id_HoteltoClone, id_NewHotel);

                        CaptivePortalLogInternal internalLogUsers = new CaptivePortalLogInternal();
                        internalLogUsers.NSEId = "";
                        internalLogUsers.CallerID = "";
                        internalLogUsers.Date = DateTime.Now;
                        internalLogUsers.UserName = "";
                        internalLogUsers.Password = "";
                        internalLogUsers.Page = "Clone Site";
                        internalLogUsers.IdLocation = 0;
                        internalLogUsers.NomadixResponse = "CLOSED SESSIONS CLONADOS EN SITE " + id_NewHotel + " CORRECTAMENTE - ";
                        internalLogUsers.RadiusResponse = "OK";

                        BillingController.SaveCaptivePortalLogInternal(internalLogUsers);
                    }

                    ////Clone SessionsLog
                    ////Clonado por SP (Procedimiento de almacenamiento)
                    if (sessionsLog)
                    {
                        BillingController.CloneSessionsLog(id_HoteltoClone, id_NewHotel);

                        CaptivePortalLogInternal internalLogUsers = new CaptivePortalLogInternal();
                        internalLogUsers.NSEId = "";
                        internalLogUsers.CallerID = "";
                        internalLogUsers.Date = DateTime.Now;
                        internalLogUsers.UserName = "";
                        internalLogUsers.Password = "";
                        internalLogUsers.Page = "Clone Site";
                        internalLogUsers.IdLocation = 0;
                        internalLogUsers.NomadixResponse = "SESSION LOGs CLONADOS EN SITE " + id_NewHotel + " CORRECTAMENTE - ";
                        internalLogUsers.RadiusResponse = "OK";

                        BillingController.SaveCaptivePortalLogInternal(internalLogUsers);
                    }

                    ////Clone Loyalties
                    ////Clonado por SP (Procedimiento de almacenamiento)
                    if (cloneLoyalties)
                    {
                        BillingController.CloneLoyaltiesLocationIsNull(id_HoteltoClone, id_NewHotel);

                        CaptivePortalLogInternal internalLogUsers = new CaptivePortalLogInternal();
                        internalLogUsers.NSEId = "";
                        internalLogUsers.CallerID = "";
                        internalLogUsers.Date = DateTime.Now;
                        internalLogUsers.UserName = "";
                        internalLogUsers.Password = "";
                        internalLogUsers.Page = "Clone Site";
                        internalLogUsers.IdLocation = 0;
                        internalLogUsers.NomadixResponse = "LOYALTIES NULL CLONADOS EN SITE " + id_NewHotel + " CORRECTAMENTE - ";
                        internalLogUsers.RadiusResponse = "OK";

                        BillingController.SaveCaptivePortalLogInternal(internalLogUsers);
                    }


                    json = "{\"code\":\"OK\",\"message\":\"0x0038\"}";
                    CaptivePortalLogInternal internalLogfin = new CaptivePortalLogInternal();
                    internalLogfin.NSEId = "";
                    internalLogfin.CallerID = "";
                    internalLogfin.Date = DateTime.Now;
                    internalLogfin.UserName = "";
                    internalLogfin.Password = "";
                    internalLogfin.Page = "Clone Site";
                    internalLogfin.IdLocation = 0;
                    internalLogfin.NomadixResponse = "GREAT JOB - NEW SITE " + id_NewHotel + " CLONADO DEL SITE: "+ id_HoteltoClone;
                    internalLogfin.RadiusResponse = "OK";

                    BillingController.SaveCaptivePortalLogInternal(internalLogfin);

                }
            }
            catch (Exception ex)
            {
                CaptivePortalLogInternal internalLog = new CaptivePortalLogInternal();
                internalLog.NSEId = "";
                internalLog.CallerID = "";
                internalLog.Date = DateTime.Now;
                internalLog.UserName = "";
                internalLog.Password = "";
                internalLog.Page = "Clone Site";
                internalLog.IdLocation = 0;
                internalLog.NomadixResponse = "ERROR NO CONTEMPLADO - " + ex.Message + " | " + ex.TargetSite;
                internalLog.RadiusResponse = "OK";

                BillingController.SaveCaptivePortalLogInternal(internalLog);

                json = "{\"code\":\"ERROR\",\"message\":\"0x9999\"}";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();
        }

        //LISTADO DE TODOS LOS SITIOS
        private ListHotelsResult selectAllSites(HttpContext context)
        {
            ListHotelsResult response = new ListHotelsResult();
            int PageNumber = 0;
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    string page = (context.Request["page"] == null ? string.Empty : context.Request["page"].ToString());
                    string name = (context.Request["name"] == null ? string.Empty : context.Request["name"].ToString());

                    int p = 0;
                    if (Int32.TryParse(page, out p))
                        PageIndex = p;

                    List<Hotels> sites = BillingController.GetHotels(name, PageIndex, PageSize, ref PageNumber);

                    response.list = new List<HotelResult>();
                    response.pageNumber = PageNumber;
                    foreach (Hotels site in sites)
                    {
                        HotelResult result = new HotelResult();
                        result.Name = site.Name;
                        result.IdHotel = site.IdHotel;

                        response.list.Add(result);
                    }
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }


        #endregion

        #region FDSUsers

        private FDSUsers SaveFDSUser(HttpContext context, Dictionary<string, string> sData)
        {
            FDSUsers response = new FDSUsers();
            string json = string.Empty;
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    //Insert new FDSUser                   
                    FDSUsers new_FDSUser = new FDSUsers();
                    new_FDSUser.IdUser = 0;

                    int idUser = 0;
                    try
                    {
                        if (Int32.TryParse(sData["id"], out idUser))
                            new_FDSUser = BillingController.GetFDSUser(idUser);   
                    }
                    catch
                    {
                    }

                    new_FDSUser.Username = sData["username"];
                    new_FDSUser.Password = sData["pass"];
                    new_FDSUser.Name = sData["fullname"];
                    new_FDSUser.IdType = int.Parse(sData["types"]);
                    new_FDSUser.IdRol = int.Parse(sData["rol"]);
                    
                    BillingController.SaveFDSUser(new_FDSUser);
                    response.Name = new_FDSUser.Name;
                    response.IdUser = new_FDSUser.IdUser;

                    //Insert new FDSUserAccess
                    FDSUsersAccess new_FDSUserAccess = new FDSUsersAccess();
                    
                    if (idUser != 0)
                    {
                        new_FDSUserAccess = BillingController.GetFDSUserAccess(idUser);
                        if (new_FDSUserAccess.Id.Equals(0))
                            new_FDSUserAccess.IdUser = idUser;
                    }else
                    {
                        new_FDSUserAccess.Id = 0;
                        new_FDSUserAccess.IdUser = new_FDSUser.IdUser;
                    }

                    //ID del grupo/site/location segun IDTYPE
                    var id_Access = int.Parse(sData["id_access"]);

                    switch (new_FDSUser.IdType)
                    {
                        case 1:
                            //GROUPS
                            new_FDSUserAccess.IdGroup = id_Access;
                            new_FDSUserAccess.IdSite = 0;
                            new_FDSUserAccess.IdLocation = 0;
                            break;
                        case 2:
                            //SITES
                            new_FDSUserAccess.IdGroup = 0;
                            new_FDSUserAccess.IdSite = id_Access;
                            new_FDSUserAccess.IdLocation = 0;
                            break;
                        case 3:
                            //LOCATION (debemos guardar también el SITE)
                            Wifi360.Data.Model.Locations2 mi_loc = new Locations2();
                            mi_loc = BillingController.GetLocation(id_Access);
                            new_FDSUserAccess.IdGroup = 0;
                            new_FDSUserAccess.IdSite = mi_loc.IdHotel;
                            new_FDSUserAccess.IdLocation = id_Access;
                            break;
                    }
                    
                    BillingController.SaveFDSUserAccess(new_FDSUserAccess);

                    json = "{\"code\":\"OK\",\"message\":\"0x0039\"}";
                }
                else
                {
                    json = "{\"code\":\"ERROR\",\"message\":\"0x9999\"}";
                }
            }
            catch (Exception ex)
            {
                json = "{\"code\":\"ERROR\",\"message\":\" " + ex.Message + ".\"}";
            }

            //context.Response.ContentType = "application/json";
            //context.Response.Write(json);
            //context.Response.End();

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private List<FDSUsersType> selectalltypes(HttpContext context)
        {
            List<FDSUsersType> response = new List < FDSUsersType > ();
            try
            {
                response = BillingController.GetFDSUserTypes();
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private List<Rols> selectallrols(HttpContext context)
        {
            List<Rols> response = new List<Rols>();
            try
            {
                response = BillingController.GetFDSRols();
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private ListFDSUsersResult GetAllFDSUsers(HttpContext context)
        {
            ListFDSUsersResult response = new ListFDSUsersResult();
            int PageNumber = 0;
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    //Código para el paginado
                    string page = (context.Request["page"] == null ? string.Empty : context.Request["page"].ToString());                    
                    int p = 0;
                    if (Int32.TryParse(page, out p))
                        PageIndex = p;

                    //Filtrado
                    string name = (context.Request["username"] == null ? string.Empty : context.Request["username"].ToString());

                    List<FDSUsers> list = BillingController.GetFDSUsers(name, PageIndex, PageSize, ref PageNumber);
                    response.PageNumber = PageNumber;

                    response.list = new List<FDSUserResult>();
                    List<Rols> listRols = BillingController.GetFDSRols();
                    List<FDSUsersType> listTypes = BillingController.GetFDSUserTypes();
                    foreach (FDSUsers b in list.OrderBy(a => a.Username))
                    {
                        FDSUserResult elemento = new FDSUserResult();
                        
                        elemento.idFDSUser = b.IdUser;
                        elemento.IdType = b.IdType;
                        elemento.IdRol = b.IdRol;
                        elemento.Name = b.Name;
                        elemento.Password = b.Password;
                        elemento.UserName = b.Username;

                        FDSUsersType resultado_type = listTypes.FirstOrDefault(s => s.Id.Equals(b.IdType));
                        elemento.type = resultado_type.Name;

                        Rols resultado_rol = listRols.FirstOrDefault(s => s.IdRol.Equals(b.IdRol));
                        elemento.rol = resultado_rol.Name;

                        response.list.Add(elemento);
                    }
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;


        }
        #endregion

        private ListActivityResult activity(HttpContext context)
        {
            ListActivityResult response = new ListActivityResult();
            string json = string.Empty;
            try
            {
                int PageNumber = 0;

                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    Hotels hotel = null;
                    Locations2 location = null;
                    FDSGroups group = null;
                    SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group);

                    string idroom = (context.Request["r"] == null ? string.Empty : context.Request["r"].ToString());
                    string start = (context.Request["s"] == null ? string.Empty : context.Request["s"].ToString());
                    string end = (context.Request["e"] == null ? string.Empty : context.Request["e"].ToString());
                    string user = (context.Request["u"] == null ? string.Empty : context.Request["u"].ToString());
                    string idmember = (context.Request["idmember"] == null ? string.Empty : context.Request["idmember"].ToString());
                    string idmembertype = (context.Request["idmembertype"] == null ? string.Empty : context.Request["idmembertype"].ToString());
                    string page = (context.Request["page"] == null ? string.Empty : context.Request["page"].ToString());


                    if ((start.Equals("undefined")) || string.IsNullOrEmpty(start))
                    {
                        start = DateTime.Now.AddSeconds(-siteGMT.gmtoffset).AddHours(-24).ToString("dd/MM/yyyy HH:mm:ss");
                        end = DateTime.Now.AddSeconds(-siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");

                    }
                    else
                    {
                        start = DateTime.Parse(start).AddSeconds(-siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                        end = DateTime.Parse(end).AddSeconds(-siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                    }

                    int p = 0;
                    if (Int32.TryParse(page, out p))
                        PageIndex = p;

                    List<ClosedSessions2> list = null;
                    string listadoHoteles = ListadoHoteles(group);
                    if (int.Parse(idmember) != 0)
                    {
                        int type = int.Parse(idmembertype);
                        switch (type)
                        {
                            case 1:
                                FDSGroups temp = BillingController.GetGroup(int.Parse(idmember));
                                listadoHoteles = ListadoHoteles(temp); break;
                            case 2: hotel = BillingController.GetHotel(int.Parse(idmember)); break;
                            case 3:
                                location = BillingController.GetLocation(int.Parse(idmember));
                                hotel = BillingController.GetHotel(location.IdHotel); break;

                        }
                    }

                    if (location!= null)
                    {
                        FDSLocationsIdentifier identifier = BillingController.GetFDSLocationIdentifier(Int32.Parse(HttpContext.Current.Request.Cookies["location"].Value));
                        list = BillingController.GetClosedSession(identifier.Identifier, hotel.IdHotel, user, DateTime.Parse(start), DateTime.Parse(end), PageIndex, PageSize, ref PageNumber);
                    }
                    else if (hotel != null)
                    { 
                        list = BillingController.GetClosedSession(hotel.IdHotel, user, DateTime.Parse(start), DateTime.Parse(end), PageIndex, PageSize, ref PageNumber);
                    }
                    else
                        list = BillingController.GetClosedSessionListadoHoteles(listadoHoteles, user, DateTime.Parse(start), DateTime.Parse(end), PageIndex, PageSize, ref PageNumber);

                    response.list = new List<ActivityResult>();
                    response.pageNumber = PageNumber;

                    foreach (ClosedSessions2 b in list)
                    {
                        ActivityResult r = new ActivityResult();
                        r.User = b.UserName;
                        r.Start = b.SessionStarted.Value.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                        r.Finish = b.SessionTerminated.Value.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                        r.BytesIn = b.BytesIn.ToString();
                        r.BytesOut = b.BytesOut.ToString();
                        r.MAC = b.CallerID;

                        response.list.Add(r);
                    }
                }
            }
            catch (Exception ex)
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;


        }

        private ListBillingModuleResult billingModules(HttpContext context)
        {
            ListBillingModuleResult response = new ListBillingModuleResult();
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    List<BillingModules> list = BillingController.ObtainBillingModules();

                    response.list = new List<BillingModuleResult>();


                    foreach (BillingModules b in list.OrderBy(a => a.IdBillingModule))
                    {
                        BillingModuleResult br = new BillingModuleResult();
                        br.IdBillingModule = b.IdBillingModule;
                        br.Name = b.Name;

                        response.list.Add(br);
                    }
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;

        }

        private BillingTypeFullResult billingType(HttpContext context)
        {
            BillingTypeFullResult response = new BillingTypeFullResult();
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));

                    string id = (context.Request["id"] == null ? string.Empty : context.Request["id"].ToString());
                    BillingTypes2 b = BillingController.ObtainBillingType2(Int32.Parse(id));
                    Locations2 l = BillingController.ObtainLocation(b.IdLocation);
                    Wifi360.Data.Model.Priorities p = BillingController.ObtainPriority(b.IdLocation);
                    BillingModules bm = BillingController.ObtainBillingModule(b.IdBillingModule);

                    response.IdBillingType = b.IdBillingType;
                    response.Description = b.Description;
                    response.BWDown = b.BWDown.ToString();
                    response.BWUp = b.BWUp.ToString();
                    response.Enabled = b.Enabled;
                    response.FreeAccess = b.FreeAccess;
                    response.MaxDevices = b.MaxDevices.ToString();
                    response.Order = b.Order.ToString();
                    response.Price = b.Price.ToString();
                    response.Priority = b.IdPriority;
                    response.STB = b.Visible;
                    Currencies2 currency = BillingController.Currency(l.Currency);
                    response.currency = currency.Symbol;

                    if ((b.TimeCredit != 0) && (b.MaxDevices != 0))
                        response.TimeCredit = (b.TimeCredit / b.MaxDevices).ToString();
                    else
                        response.TimeCredit = b.TimeCredit.ToString();
                    if ((b.VolumeDown != 0) && (b.MaxDevices != 0))
                        response.VolumeDown = (b.VolumeDown / b.MaxDevices).ToString();
                    else
                        response.VolumeDown = b.VolumeDown.ToString();
                    if ((b.VolumeUp != 0) && (b.MaxDevices != 0))
                        response.VolumeUp = (b.VolumeUp / b.MaxDevices).ToString();
                    else
                        response.VolumeUp = b.VolumeUp.ToString();

                    response.VolumeCombined = response.VolumeDown;
                    response.ValidTill = b.ValidTill.ToString();
                    response.ValidAfterFirstUse = b.ValidAfterFirstUse.ToString();
                    response.IdLocation = b.IdLocation;
                    response.LocationDescription = l.Description;
                    response.PriorityDescription = p.Description;
                    response.PaypalDescription = b.PayPalDescription;
                    response.IdBillingModule = b.IdBillingModule;
                    response.BillingModule = bm.Name;
                    response.DefaultFastTicket = b.DefaultFastTicket;
                    response.urllanding = b.UrlLanding;
                    response.filterid = b.Filter_Id;
                    response.iot = b.IoT;
                    if (b.NextBillingType.HasValue)
                        response.nextBillingType = b.NextBillingType.Value;
                    else
                        response.nextBillingType = 0;

                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }
        
        private ListBillingTypeResult billinTypes(HttpContext context)
        {
            ListBillingTypeResult response = new ListBillingTypeResult();
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    Hotels hotel = null;
                    Locations2 location = null;
                    FDSGroups group = null;
                    SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group);
                    
                    List<BillingTypes> list = BillingController.ObtainBillingTypes(hotel.IdHotel);
                    response.list = new List<BillingTypeResult>();

                    foreach (BillingTypes u in list.OrderBy(a => a.Order))
                    {
                        if (HttpContext.Current.Request.Cookies["location"] != null)
                        {
                            if (location.IdLocation.Equals(u.IdLocation))
                            {
                                BillingTypeResult r = new BillingTypeResult();
                                r.IdBillingType = u.IdBillingType;
                                r.Description = u.Description;

                                response.list.Add(r);
                            }
                        }
                        else
                        {
                            BillingTypeResult r = new BillingTypeResult();
                            r.IdBillingType = u.IdBillingType;
                            r.Description = u.Description;

                            response.list.Add(r);
                        }
                    }
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private ListBillingTypeResult billingTypesUserFilter(HttpContext context)
        {
            ListBillingTypeResult response = new ListBillingTypeResult();
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    string iduser = (context.Request["iduser"] == null ? string.Empty : context.Request["iduser"].ToString());

                    Hotels hotel = null;
                    Locations2 location = null;
                    FDSGroups group = null;
                    SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group);
                    Users user = BillingController.GetRadiusUser(int.Parse(iduser));

                    List<BillingTypes> list = BillingController.ObtainBillingTypes(user.IdHotel);
                    response.list = new List<BillingTypeResult>();

                    foreach (BillingTypes u in list.OrderBy(a => a.Order))
                    {
                        if (location != null)
                        {
                            if (location.IdLocation.Equals(u.IdLocation))
                            {
                                BillingTypeResult r = new BillingTypeResult();
                                r.IdBillingType = u.IdBillingType;
                                r.Description = u.Description;

                                response.list.Add(r);
                            }
                        }
                        else
                        {
                            BillingTypeResult r = new BillingTypeResult();
                            r.IdBillingType = u.IdBillingType;
                            r.Description = u.Description;

                            response.list.Add(r);
                        }
                    }
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private ListBillingTypeResult billinTypesRegular(HttpContext context)
        {
            ListBillingTypeResult response = new ListBillingTypeResult();
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    string idmember = (context.Request["idmember"] == null ? string.Empty : context.Request["idmember"].ToString());
                    string idmembertype = (context.Request["idmembertype"] == null ? string.Empty : context.Request["idmembertype"].ToString());

                    List<BillingTypes> list = new List<BillingTypes>();
                    switch(idmembertype)
                    {
                        case "2": list = BillingController.ObtainBillingTypes(int.Parse(idmember)); break;
                        case "3":
                            Locations2 loca = BillingController.GetLocation(int.Parse(idmember));
                            Hotels h = BillingController.GetHotel(loca.IdHotel);
                            list = BillingController.ObtainBillingTypes(h.IdHotel); break;
                    }

                    response.list = new List<BillingTypeResult>();

                    foreach (BillingTypes u in list.OrderBy(a => a.Order))
                    {
                        if (!u.IoT)
                        {
                            if (HttpContext.Current.Request.Cookies["location"] != null)
                            {
                                int idlocation = int.Parse(HttpContext.Current.Request.Cookies["location"].Value.ToString());
                                if (idlocation.Equals(u.IdLocation))
                                {
                                    BillingTypeResult r = new BillingTypeResult();
                                    r.IdBillingType = u.IdBillingType;
                                    r.Description = u.Description;

                                    response.list.Add(r);
                                }
                            }
                            else
                            {
                                BillingTypeResult r = new BillingTypeResult();
                                r.IdBillingType = u.IdBillingType;
                                r.Description = u.Description;

                                response.list.Add(r);
                            }
                        }
                    }
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private ListBillingTypeResult billinTypesFilter(HttpContext context)
        {
            ListBillingTypeResult response = new ListBillingTypeResult();
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    string idmember = (context.Request["idmember"] == null ? string.Empty : context.Request["idmember"].ToString());
                    string idmembertype = (context.Request["idmembertype"] == null ? string.Empty : context.Request["idmembertype"].ToString());

                    List<BillingTypes> list = new List<BillingTypes>();
                    switch (idmembertype)
                    {
                        case "2": list = BillingController.ObtainBillingTypes(int.Parse(idmember)); break;
                        case "3":
                            Locations2 loca = BillingController.GetLocation(int.Parse(idmember));
                            Hotels h = BillingController.GetHotel(loca.IdHotel);
                            list = BillingController.ObtainBillingTypes(h.IdHotel); break;
                    }

                    response.list = new List<BillingTypeResult>();


                    foreach (BillingTypes u in list.OrderBy(a => a.Order))
                    {
                        if (HttpContext.Current.Request.Cookies["location"] != null)
                        {
                            int idlocation = int.Parse(HttpContext.Current.Request.Cookies["location"].Value.ToString());
                            if (idlocation.Equals(u.IdLocation))
                            {
                                BillingTypeResult r = new BillingTypeResult();
                                r.IdBillingType = u.IdBillingType;
                                r.Description = u.Description;

                                response.list.Add(r);
                            }
                        }
                        else
                        {
                            BillingTypeResult r = new BillingTypeResult();
                            r.IdBillingType = u.IdBillingType;
                            r.Description = u.Description;

                            response.list.Add(r);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private ListBillingTypeResult billinTypesIoT(HttpContext context)
        {
            ListBillingTypeResult response = new ListBillingTypeResult();
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    string idmember = (context.Request["idmember"] == null ? string.Empty : context.Request["idmember"].ToString());
                    string idmembertype = (context.Request["idmembertype"] == null ? string.Empty : context.Request["idmembertype"].ToString());

                    List<BillingTypes> list = new List<BillingTypes>();
                    switch (idmembertype)
                    {
                        case "2": list = BillingController.ObtainBillingTypes(int.Parse(idmember)); break;
                        case "3":
                            Locations2 loca = BillingController.GetLocation(int.Parse(idmember));
                            Hotels h = BillingController.GetHotel(loca.IdHotel);
                            list = BillingController.ObtainBillingTypes(h.IdHotel); break;
                    }

                    response.list = new List<BillingTypeResult>();


                    foreach (BillingTypes u in list.OrderBy(a => a.Order))
                    {
                        if (u.IoT)
                        {
                            if (HttpContext.Current.Request.Cookies["location"] != null)
                            {
                                int idlocation = int.Parse(HttpContext.Current.Request.Cookies["location"].ToString());
                                if (idlocation.Equals(u.IdLocation))
                                {
                                    BillingTypeResult r = new BillingTypeResult();
                                    r.IdBillingType = u.IdBillingType;
                                    r.Description = u.Description;

                                    response.list.Add(r);
                                }
                            }
                            else
                            {
                                BillingTypeResult r = new BillingTypeResult();
                                r.IdBillingType = u.IdBillingType;
                                r.Description = u.Description;

                                response.list.Add(r);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private ListBillingTypeFullResult billinTypesFull(HttpContext context)
        {
            ListBillingTypeFullResult response = new ListBillingTypeFullResult();
            try
            {
                //OBTENEMOS EL TIPO DE USUARIOS
                int PageNumber = 0;

                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    Hotels hotel = null;
                    Locations2 location = null;
                    List<BillingTypes> list = null;

                    if (HttpContext.Current.Request.Cookies["location"] != null) {
                        location = BillingController.GetLocation(Int32.Parse(HttpContext.Current.Request.Cookies["location"].Value));
                        hotel = BillingController.GetHotel(location.IdHotel);
                    }
                    else 
                        hotel = BillingController.GetHotel(Int32.Parse(HttpContext.Current.Request.Cookies["site"].Value));
                    
                    if (fdsUser.IdRol.Equals(2) || fdsUser.IdRol.Equals(4))
                        response.isAdmin = true;
                    else
                        response.isAdmin = false;

                    string page = (context.Request["page"] == null ? string.Empty : context.Request["page"].ToString());
                    string filter = (context.Request["filter"] == null ? string.Empty : context.Request["filter"].ToString());
                    string status = (context.Request["status"] == null ? string.Empty : context.Request["status"].ToString());
                    string module = (context.Request["module"] == null ? string.Empty : context.Request["module"].ToString());
                    string iot = (context.Request["iot"] == null ? string.Empty : context.Request["iot"].ToString());

                    int pa = 0;
                    if (Int32.TryParse(page, out pa))
                        PageIndex = pa;


                    if (location != null)
                        list = BillingController.ObtainBillingTypesLocation(location.IdLocation, filter, Int32.Parse(module), Boolean.Parse(status), Int32.Parse(iot), PageIndex, PageSize, ref PageNumber);
                    else 
                        //list = BillingController.ObtainBillingTypes(hotel.IdHotel, PageIndex, PageSize, ref PageNumber);
                        list = BillingController.ObtainBillingTypes(hotel.IdHotel, filter, Int32.Parse(module), Boolean.Parse(status), Int32.Parse(iot), PageIndex, PageSize, ref PageNumber);

                    response.list = new List<BillingTypeFullResult>();
                    response.pageNumber = PageNumber;

                    foreach (BillingTypes u in list.OrderBy(a => a.Order))
                    {
                        if (location == null)
                        {
                            BillingTypeFullResult r = new BillingTypeFullResult();
                            Locations2 l = BillingController.ObtainLocation(u.IdLocation);
                            Wifi360.Data.Model.Priorities p = BillingController.ObtainPriority(u.IdPriority);
                            BillingModules bm = BillingController.ObtainBillingModule(u.IdBillingModule);
                            r.IdBillingType = u.IdBillingType;
                            r.Description = u.Description;
                            r.BWDown = u.BWDown.ToString();
                            r.BWUp = u.BWUp.ToString();
                            r.Enabled = u.Enabled;
                            r.FreeAccess = u.FreeAccess;
                            r.MaxDevices = u.MaxDevices.ToString();
                            r.Order = u.Order.ToString();
                            r.Price = u.Price.ToString();
                            r.Priority = u.IdPriority;
                            r.STB = u.Visible;
                            r.ValidTill = u.ValidTill.ToString();
                            r.VolumeDown = u.VolumeDown.ToString();
                            r.VolumeUp = u.VolumeUp.ToString();
                            r.LocationDescription = l.Description;
                            r.PriorityDescription = p.Description; r.TimeCredit = u.TimeCredit.ToString();
                            r.PaypalDescription = u.PayPalDescription;
                            r.IdBillingModule = u.IdBillingModule;
                            r.BillingModule = bm.Name;
                            r.DefaultFastTicket = u.DefaultFastTicket;
                            r.iot = u.IoT;
                            Currencies2 currency = BillingController.Currency(l.Currency);
                            r.currency = currency.Symbol;

                            response.list.Add(r);
                        }
                        else
                        {
                            if (u.IdLocation.Equals(location.IdLocation)) 
                            { 
                                BillingTypeFullResult r = new BillingTypeFullResult();
                                Wifi360.Data.Model.Priorities p = BillingController.ObtainPriority(u.IdPriority);
                                BillingModules bm = BillingController.ObtainBillingModule(u.IdBillingModule);
                                r.IdBillingType = u.IdBillingType;
                                r.Description = u.Description;
                                r.BWDown = u.BWDown.ToString();
                                r.BWUp = u.BWUp.ToString();
                                r.Enabled = u.Enabled;
                                r.FreeAccess = u.FreeAccess;
                                r.MaxDevices = u.MaxDevices.ToString();
                                r.Order = u.Order.ToString();
                                r.Price = u.Price.ToString();
                                r.Priority = u.IdPriority;
                                r.STB = u.Visible;
                                r.ValidTill = u.ValidTill.ToString();
                                r.VolumeDown = u.VolumeDown.ToString();
                                r.VolumeUp = u.VolumeUp.ToString();
                                r.LocationDescription = location.Description;
                                r.PriorityDescription = p.Description; r.TimeCredit = u.TimeCredit.ToString();
                                r.PaypalDescription = u.PayPalDescription;
                                r.IdBillingModule = u.IdBillingModule;
                                r.BillingModule = bm.Name;
                                r.DefaultFastTicket = u.DefaultFastTicket;
                                r.iot = u.IoT;
                                Currencies2 currency = BillingController.Currency(location.Currency);
                                r.currency = currency.Symbol;

                                response.list.Add(r);
                            }
                        }
                    }
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private ListBillingTypeResult billinTypesModule(HttpContext context)
        {
            ListBillingTypeResult response = new ListBillingTypeResult();
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));

                    string id = (context.Request["id"] == null ? string.Empty : context.Request["id"].ToString());


                    List<BillingTypes> list = BillingController.ObtainBillingTypesModules(Int32.Parse(HttpContext.Current.Request.Cookies["site"].Value), Int32.Parse(id));
                    response.list = new List<BillingTypeResult>();

                    foreach (BillingTypes u in list.OrderBy(a => a.Order)) 
                    {

                        BillingTypeResult r = new BillingTypeResult();
                        r.IdBillingType = u.IdBillingType;
                        r.Description = u.Description;

                        response.list.Add(r);
                    }
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private ListBillingTypeResult billingTypesLocation(HttpContext context)
        {
            ListBillingTypeResult response = new ListBillingTypeResult();
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));

                    string idlocation = (context.Request["l"] == null ? string.Empty : context.Request["l"].ToString());
                    Locations2 l = BillingController.GetLocation(Int32.Parse(idlocation));

                    List<BillingTypes> list = BillingController.SelectBillingTypesAllAndAllZones(l.IdHotel, l.IdLocation);
                    response.list = new List<BillingTypeResult>();

                    foreach (BillingTypes u in list.OrderBy(a => a.Order))
                    {

                        BillingTypeResult r = new BillingTypeResult();
                        r.IdBillingType = u.IdBillingType;
                        r.Description = u.Description;
                        if (u.DefaultFastTicket)
                            response.Default = u.IdBillingType;

                        response.list.Add(r);
                    }
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private ListBlackListMacResult blacklistmacs(HttpContext context)
        {
            ListBlackListMacResult response = new ListBlackListMacResult();
            try
            {
                int PageNumber = 0;

                //OBTENEMOS EL TIPO DE USUARIOS
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    Hotels hotel = null;
                    Locations2 location = null;
                    FDSGroups group = null;

                    SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group);

                    string start = (context.Request["s"] == null ? string.Empty : context.Request["s"].ToString());
                    string end = (context.Request["e"] == null ? string.Empty : context.Request["e"].ToString());
                    string idmember = (context.Request["idmember"] == null ? string.Empty : context.Request["idmember"].ToString());
                    string idmembertype = (context.Request["idmembertype"] == null ? string.Empty : context.Request["idmembertype"].ToString());
                    string page = (context.Request["page"] == null ? string.Empty : context.Request["page"].ToString());

                    if ((start.Equals("undefined")) || string.IsNullOrEmpty(start))
                    {
                        start = DateTime.Now.ToUniversalTime().AddHours(-24).ToString("dd/MM/yyyy HH:mm:ss");
                        end = DateTime.Now.ToUniversalTime().ToString("dd/MM/yyyy HH:mm:ss");

                    }
                    else
                    {
                        start = DateTime.Parse(start).AddHours(-siteGMT.dst).AddSeconds(-siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                        end = DateTime.Parse(end).AddHours(-siteGMT.dst).AddSeconds(-siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                    }

                    int p = 0;
                    if (Int32.TryParse(page, out p))
                        PageIndex = p;

                    List<BlackListMACs> list = null;
                    string ListHoteles = ListadoHoteles(group);
                    if (int.Parse(idmember) != 0)
                    {
                        int type = int.Parse(idmembertype);
                        switch (type)
                        {
                            case 1:
                                FDSGroups temp = BillingController.GetGroup(int.Parse(idmember));
                                ListHoteles = ListadoHoteles(temp); break;
                            case 2: hotel = BillingController.GetHotel(int.Parse(idmember)); break;
                            case 3:
                                location = BillingController.GetLocation(int.Parse(idmember));
                                hotel = BillingController.GetHotel(location.IdHotel); break;

                        }
                    }
                    if (hotel != null)
                        list = BillingController.GetBlacListMac(hotel.IdHotel, DateTime.Parse(start), DateTime.Parse(end), PageIndex, PageSize, ref PageNumber);
                    else
                        list = BillingController.GetBlacListMac(ListHoteles, DateTime.Parse(start), DateTime.Parse(end), PageIndex, PageSize, ref PageNumber);

                    response.list = new List<BlackListMacResult>();
                    response.pageNumber = PageNumber;

                    foreach (BlackListMACs l in list)
                    {
                        string macformated = string.Empty;
                        if (!l.MAC.Contains('-'))
                        {
                            for (int i = 1; i <= l.MAC.Length; i++) //COGEMOS LA MAC POR PARAMETROS Y LA FORMATEAMOS PARA COMPARAR
                            {
                                macformated += l.MAC[i - 1];
                                if (i % 2 == 0)
                                    macformated += "-";
                            }
                            macformated = macformated.Substring(0, macformated.Length - 1);

                        }
                        else
                            macformated = l.MAC;

                        Users u = BillingController.GetRadiusUsermac(macformated, l.IdHotel);

                        BlackListMacResult r = new BlackListMacResult();
                        r.IdBackListMac = l.IdMACBlackList.ToString();
                        r.Mac = macformated;
                        r.BanningStart = l.BanningStart.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss"); ;
                        r.BanningEnd = l.BanningEnd.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss"); ;
                        r.IdHotel = l.IdHotel.ToString();
                        if (!u.IdUser.Equals(0))
                        {
                            r.lastUser = u.Name;
                            r.idUser = u.IdUser.ToString();
                        }
                        else
                        {
                            r.lastUser = "--";
                            r.idUser = "0";
                        }

                        if (HttpContext.Current.Request.Cookies["location"] != null)
                        {
                            if (location.IdLocation.Equals(l.IdLocation))
                                response.list.Add(r);
                        }
                        else
                            response.list.Add(r);
                    }
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private ListActivityResult closedsession(HttpContext context)
        {
            ListActivityResult response = new ListActivityResult();
            string json = string.Empty;
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    Locations2 location = null;
                    Hotels hotel = null;
                    FDSGroups group = null;
                    SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group);

                    string iduser = (context.Request["iduser"] == null ? string.Empty : context.Request["iduser"].ToString());
                    Users user = BillingController.GetRadiusUser(Int32.Parse(iduser));
                    List<UserAll> users = BillingController.GetRadiusUsers(user.IdHotel, user.Name, user.ValidSince);
                    DateTime validTill = user.ValidTill;
                    foreach (UserAll other in users)
                        if (other.ValidTill > user.ValidTill)
                            validTill = other.ValidTill;

                    List<ClosedSessions2> list = BillingController.GetClosedSession(user.IdHotel, user.Name, user.ValidSince, validTill);
                    if (list.Count > 30)
                        list = list.OrderBy(a => a.SessionTerminated).Skip(list.Count - 30).Take(30).ToList();

                    response.list = new List<ActivityResult>();
                    response.pageNumber = 0;

                    foreach (ClosedSessions2 b in list.OrderBy(a => a.SessionTerminated))
                    {
                        ActivityResult r = new ActivityResult();
                        r.User = b.UserName;
                        r.Start = b.SessionStarted.Value.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                        r.Finish = b.SessionTerminated.Value.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                        r.BytesIn = b.BytesIn.ToString();
                        r.BytesOut = b.BytesOut.ToString();
                        r.MAC = b.CallerID;

                        response.list.Add(r);
                    }
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;


        }

        private DashboardResult dashBoard(HttpContext context)
        {
            DashboardResult response = new DashboardResult();
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));

                    Hotels hotel = null;
                    Locations2 location = null;
                    SitesGMT siteGMT = GetSiteGMt(out location, out hotel);

                    if (location != null)
                    {
                        response.bandwidth = BillingController.BWUsedLocation(location.IdLocation);
                        response.bandwidthUp = BillingController.BWUsedDownLocation(location.IdLocation);
                    }
                    else
                    {
                        response.bandwidth = BillingController.BWUsed(hotel.IdHotel);
                        response.bandwidthUp = BillingController.BWUsedDown(hotel.IdHotel);
                    }

                    #region ClosedSessions y Accounting

                    List<ClosedSessions2> closedSessions = null;

                    if (location != null)
                    {
                        closedSessions = BillingController.GetClosedSessionLocation(location.IdLocation, DateTime.Now.AddDays(-1));
                    }
                    else
                        closedSessions = BillingController.GetClosedSession(hotel.IdHotel, DateTime.Now.AddDays(-1));

                    List<ClosedSessions2> closedLastHour = (from t in closedSessions where t.SessionTerminated > DateTime.Now.AddHours(-1) select t).ToList<ClosedSessions2>();

                    //double bytesIn = Double.Parse(closedSessions.Sum(a => a.BytesOut).Value.ToString()) * 8;
                    //var diff = closedSessions.Where(t => SqlFunctions.DateDiff("seconds", t.SessionTerminated, t.SessionStarted) > 0).FirstOrDefault();

                    //UNA VEZ OBTENIDAS LAS SESIONES, VAMOS A CALCULAR LOS DATOS NECESARIOS

                    //CONEXIONES TOTALES EN LAS ULTIMAS 24 H0RAS

                    response.connections24hours = closedSessions.Count().ToString();

                    //CONEXIONES TOTALES EN LA ULTIMA HORA

                    response.connections1hour = closedLastHour.Count().ToString();

                    //DATOS TOTALES SUBIDOS EN LAS ULTIMAS 24 HORAS

                    response.up24hours = string.Format("{0:N} GB", (Double.Parse(closedSessions.Sum(a => a.BytesOut).Value.ToString())/ (1000000000)));

                    //DATOS TOTALES SUBIDOS EN LA ULTIMA HORA

                    double value = Double.Parse(closedLastHour.Sum(a => a.BytesOut).Value.ToString());
                    response.up1hour = string.Format("{0:N} GB", (Double.Parse(closedLastHour.Sum(a => a.BytesOut).Value.ToString()) / (1000000000)));

                    //DATOS TOTALES DESCARGADOS EN LAS ULTIMAS 24 HORAS

                    long? bytesIn24 = closedSessions.Sum(a => a.BytesIn);

                    response.down24hours = string.Format("{0:N} GB", (Double.Parse(closedSessions.Sum(a => a.BytesIn).Value.ToString()) / (1000000000)));
                    

                    //DATOS TOTALES DESCARGADOS EN LA ULTIMA HORA

                    response.down1hour = string.Format("{0:N} GB", (Double.Parse(closedLastHour.Sum(a => a.BytesIn).Value.ToString()) / (1000000000)));
                    long? bytesIn1 = closedLastHour.Sum(a => a.BytesIn);

                    //NUMERO de dispositivos
                    int DevicesLastHour = closedLastHour.GroupBy(x => x.CallerID).Select(g => g.First()).Count();
                    int Devices24Hour = closedSessions.GroupBy(x => x.CallerID).Select(g => g.First()).Count();

                    response.device1hour = DevicesLastHour.ToString();
                    response.device24hours = Devices24Hour.ToString();

                    double seconds = 0;

                    foreach (ClosedSessions2 c in closedSessions)
                    {
                        TimeSpan t = c.SessionTerminated.Value - c.SessionStarted.Value;
                        seconds += t.TotalSeconds;
                    }

                    double mediaseconds = seconds / closedSessions.Count();
                    if (closedSessions.Count().Equals(0))
                        mediaseconds = 0;

                    double secondslasthour = 0;
                    foreach (ClosedSessions2 c in closedLastHour)
                    {
                        TimeSpan t = c.SessionTerminated.Value - c.SessionStarted.Value;
                        secondslasthour += t.TotalSeconds;
                    }

                    double mediasecondslasthour = 0;
                    if (closedLastHour.Count() != 0)
                        mediasecondslasthour = secondslasthour / closedLastHour.Count();

                    DateTime media24 = DateTime.MinValue;
                    DateTime media1 = DateTime.MinValue;

                    if (seconds.Equals(0))
                        seconds = 1;

                    if (secondslasthour.Equals(0))
                        secondslasthour = 1;

                    response.media1hour = media1.AddSeconds(mediasecondslasthour).ToString("HH:mm:ss");
                    response.media24hours = media24.AddSeconds(mediaseconds).ToString("HH:mm:ss");

                    response.speed1hour = string.Format("{0:N} Kbps", ((bytesIn1.Value / secondslasthour) * 8 / 1000));
                    response.speed24hours = string.Format("{0:N} Kbps", ((bytesIn24.Value / seconds) * 8 / 1000));

                    #endregion

                    //OBTIENE LOS USUARIOS EN LAS ULTIMAS 12 HORAS

                    #region usuarios ultimas 12 horas

                    List<SessionsLog> logs = null;

                    if (location != null)
                    {
                        logs = BillingController.GetLogPeriodLocation(location.IdLocation, DateTime.Now.AddHours(-12), DateTime.Now.AddDays(1));
                    }
                    else
                    {
                        logs = BillingController.GetLogPeriod(hotel.IdHotel, DateTime.Now.AddHours(-12), DateTime.Now.AddDays(1));
                    }

                    response.log = new List<SessionLogResult>();

                    int salto = logs.Count() / 30;

                    for (int i = 0; i < logs.Count; i++)
                    {
                        SessionLogResult obj = new SessionLogResult();
                        obj.Date = logs[i].Date.AddSeconds(siteGMT.gmtoffset).ToString("HH:mm:ss");
                        obj.Count = logs[i].Count;

                        response.log.Add(obj);

                        i += salto;
                    }

                    SessionLogResult objLast = new SessionLogResult();
                    objLast.Date = logs[logs.Count() - 1].Date.AddSeconds(siteGMT.gmtoffset).ToString("HH:mm:ss");
                    objLast.Count = logs[logs.Count() - 1].Count;

                    response.log.Add(objLast);

                    List<SessionsLog> logsLastHour = (from t in logs where t.Date > DateTime.Now.AddHours(-1) select t).ToList<SessionsLog>();

                    response.bytesIn = new List<SessionLogBytesInResult>();
                    response.bytesOut = new List<SessionLogBytesOutResult>();

                    int salto2 = logsLastHour.Count() / 60;

                    for (int i = 0; i < logsLastHour.Count; i++)
                    {
                        SessionLogBytesInResult bin = new SessionLogBytesInResult();
                        bin.Date = logsLastHour[i].Date.AddSeconds(siteGMT.gmtoffset).ToString("HH:mm:ss");
                        bin.Count = logsLastHour[i].BytesIn1hour / (1024 * 1024);

                        response.bytesIn.Add(bin);

                        SessionLogBytesOutResult bout = new SessionLogBytesOutResult();
                        bout.Date = logsLastHour[i].Date.AddSeconds(siteGMT.gmtoffset).ToString("HH:mm:ss");
                        bout.Count = logsLastHour[i].BytesOut1hour / (1024 * 1024);

                        response.bytesOut.Add(bout);

                        i += salto2;
                    }

                    #endregion

                    #region usuarios por hora ultima semana

                    List<SessionsLog> logsLastWeek = null;

                    if (location != null)
                    {
                        logsLastWeek = BillingController.GetLogPeriodLocation(location.IdLocation, DateTime.Now.AddDays(-7), DateTime.Now);
                    }
                    else
                        logsLastWeek = BillingController.GetLogPeriod(hotel.IdHotel, DateTime.Now.AddDays(-7), DateTime.Now);

                    int a0 = ((from t in logsLastWeek where t.Date.Hour.Equals(0) select t).ToList()).Sum(a => a.Count);
                    if (a0 > 0) a0 = (a0 / ((from t in logsLastWeek where t.Date.Hour.Equals(0) select t).ToList()).Count);
                    int a1 = ((from t in logsLastWeek where t.Date.Hour.Equals(1) select t).ToList()).Sum(a => a.Count);
                    if (a1 > 0) a1 = (a1 / ((from t in logsLastWeek where t.Date.Hour.Equals(1) select t).ToList()).Count);
                    int a2 = ((from t in logsLastWeek where t.Date.Hour.Equals(2) select t).ToList()).Sum(a => a.Count);
                    if (a2 > 0) a2 = (a2 / ((from t in logsLastWeek where t.Date.Hour.Equals(2) select t).ToList()).Count);
                    int a3 = ((from t in logsLastWeek where t.Date.Hour.Equals(3) select t).ToList()).Sum(a => a.Count);
                    if (a3 > 0) a3 = (a3 / ((from t in logsLastWeek where t.Date.Hour.Equals(3) select t).ToList()).Count);
                    int a4 = ((from t in logsLastWeek where t.Date.Hour.Equals(4) select t).ToList()).Sum(a => a.Count);
                    if (a4 > 0) a4 = (a4 / ((from t in logsLastWeek where t.Date.Hour.Equals(4) select t).ToList()).Count);
                    int a5 = ((from t in logsLastWeek where t.Date.Hour.Equals(5) select t).ToList()).Sum(a => a.Count);
                    if (a5 > 0) a5 = (a5 / ((from t in logsLastWeek where t.Date.Hour.Equals(5) select t).ToList()).Count);
                    int a6 = ((from t in logsLastWeek where t.Date.Hour.Equals(6) select t).ToList()).Sum(a => a.Count);
                    if (a6 > 0) a6 = (a6 / ((from t in logsLastWeek where t.Date.Hour.Equals(6) select t).ToList()).Count);
                    int a7 = ((from t in logsLastWeek where t.Date.Hour.Equals(7) select t).ToList()).Sum(a => a.Count);
                    if (a7 > 0) a7 = (a7 / ((from t in logsLastWeek where t.Date.Hour.Equals(7) select t).ToList()).Count);
                    int a8 = ((from t in logsLastWeek where t.Date.Hour.Equals(8) select t).ToList()).Sum(a => a.Count);
                    if (a8 > 0) a8 = (a8 / ((from t in logsLastWeek where t.Date.Hour.Equals(8) select t).ToList()).Count);
                    int a9 = ((from t in logsLastWeek where t.Date.Hour.Equals(9) select t).ToList()).Sum(a => a.Count);
                    if (a9 > 0) a9 = (a9 / ((from t in logsLastWeek where t.Date.Hour.Equals(9) select t).ToList()).Count);
                    int a10 = ((from t in logsLastWeek where t.Date.Hour.Equals(10) select t).ToList()).Sum(a => a.Count);
                    if (a10 > 0) a10 = (a10 / ((from t in logsLastWeek where t.Date.Hour.Equals(10) select t).ToList()).Count);
                    int a11 = ((from t in logsLastWeek where t.Date.Hour.Equals(11) select t).ToList()).Sum(a => a.Count);
                    if (a11 > 0) a11 = (a11 / ((from t in logsLastWeek where t.Date.Hour.Equals(11) select t).ToList()).Count);
                    int a12 = ((from t in logsLastWeek where t.Date.Hour.Equals(12) select t).ToList()).Sum(a => a.Count);
                    if (a12 > 0) a12 = (a12 / ((from t in logsLastWeek where t.Date.Hour.Equals(12) select t).ToList()).Count);
                    int a13 = ((from t in logsLastWeek where t.Date.Hour.Equals(13) select t).ToList()).Sum(a => a.Count);
                    if (a13 > 0) a13 = (a13 / ((from t in logsLastWeek where t.Date.Hour.Equals(13) select t).ToList()).Count);
                    int a14 = ((from t in logsLastWeek where t.Date.Hour.Equals(14) select t).ToList()).Sum(a => a.Count);
                    if (a14 > 0) a14 = (a14 / ((from t in logsLastWeek where t.Date.Hour.Equals(14) select t).ToList()).Count);
                    int a15 = ((from t in logsLastWeek where t.Date.Hour.Equals(15) select t).ToList()).Sum(a => a.Count);
                    if (a15 > 0) a15 = (a15 / ((from t in logsLastWeek where t.Date.Hour.Equals(15) select t).ToList()).Count);
                    int a16 = ((from t in logsLastWeek where t.Date.Hour.Equals(16) select t).ToList()).Sum(a => a.Count);
                    if (a16 > 0) a16 = (a16 / ((from t in logsLastWeek where t.Date.Hour.Equals(16) select t).ToList()).Count);
                    int a17 = ((from t in logsLastWeek where t.Date.Hour.Equals(17) select t).ToList()).Sum(a => a.Count);
                    if (a17 > 0) a17 = (a17 / ((from t in logsLastWeek where t.Date.Hour.Equals(17) select t).ToList()).Count);
                    int a18 = ((from t in logsLastWeek where t.Date.Hour.Equals(18) select t).ToList()).Sum(a => a.Count);
                    if (a18 > 0) a18 = (a18 / ((from t in logsLastWeek where t.Date.Hour.Equals(18) select t).ToList()).Count);
                    int a19 = ((from t in logsLastWeek where t.Date.Hour.Equals(19) select t).ToList()).Sum(a => a.Count);
                    if (a19 > 0) a19 = (a19 / ((from t in logsLastWeek where t.Date.Hour.Equals(19) select t).ToList()).Count);
                    int a20 = ((from t in logsLastWeek where t.Date.Hour.Equals(20) select t).ToList()).Sum(a => a.Count);
                    if (a20 > 0) a20 = (a20 / ((from t in logsLastWeek where t.Date.Hour.Equals(20) select t).ToList()).Count);
                    int a21 = ((from t in logsLastWeek where t.Date.Hour.Equals(21) select t).ToList()).Sum(a => a.Count);
                    if (a21 > 0) a21 = (a21 / ((from t in logsLastWeek where t.Date.Hour.Equals(21) select t).ToList()).Count);
                    int a22 = ((from t in logsLastWeek where t.Date.Hour.Equals(22)select t).ToList()).Sum(a => a.Count);
                    if (a22 > 0) a22 = (a22 / ((from t in logsLastWeek where t.Date.Hour.Equals(22) select t).ToList()).Count);
                    int a23 = ((from t in logsLastWeek where t.Date.Hour.Equals(23) select t).ToList()).Sum(a => a.Count);
                    if (a23 > 0) a23 = (a23 / ((from t in logsLastWeek where t.Date.Hour.Equals(23) select t).ToList()).Count);

                    response.usuariosHoras = new List<SessionLogResult>();
                    for (int i = 0; i < 24; i++)
                    {
                        SessionLogResult n = new SessionLogResult();
                        switch (i)
                        {
                           
                            case 0: n.Count = a6; n.Date = "6:00-7:00"; break;
                            case 1: n.Count = a7; n.Date = "7:00-8:00"; break;
                            case 2: n.Count = a8; n.Date = "8:00-9:00"; break;
                            case 3: n.Count = a9; n.Date = "9:00-10:00"; break;
                            case 4: n.Count = a10; n.Date = "10:00-11:00"; break;
                            case 5: n.Count = a11; n.Date = "11:00-12:00"; break;
                            case 6: n.Count = a12; n.Date = "12:00-13:00"; break;
                            case 7: n.Count = a13; n.Date = "13:00-14:00"; break;
                            case 8: n.Count = a14; n.Date = "14:00-15:00"; break;
                            case 9: n.Count = a15; n.Date = "15:00-16:00"; break;
                            case 10: n.Count = a16; n.Date = "16:00-17:00"; break;
                            case 11: n.Count = a17; n.Date = "17:00-18:00"; break;
                            case 12: n.Count = a18; n.Date = "18:00-19:00"; break;
                            case 13: n.Count = a19; n.Date = "19:00-20:00"; break;
                            case 14: n.Count = a20; n.Date = "20:00-21:00"; break;
                            case 15: n.Count = a21; n.Date = "21:00-22:00"; break;
                            case 16: n.Count = a22; n.Date = "22:00-23:00"; break;
                            case 17: n.Count = a23; n.Date = "23:00-0:00"; break;
                            case 18: n.Count = a0; n.Date = "0:00-1:00"; break;
                            case 19: n.Count = a1; n.Date = "1:00-2:00"; break;
                            case 20: n.Count = a2; n.Date = "2:00-3:00"; break;
                            case 21: n.Count = a3; n.Date = "3:00-4:00"; break;
                            case 22: n.Count = a4; n.Date = "4:00-5:00"; break;
                            case 23: n.Count = a5; n.Date = "5:00-6:00"; break;
                        }

                        response.usuariosHoras.Add(n);
                    }

                    #endregion

                    response.fastticket = BillingController.GetHotel(hotel.IdHotel).EnabledFastTicket;
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;

        }

        private void deletebill(HttpContext context)
        {
            string json = string.Empty;
            try
            {

                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));

                    string id = (context.Request["id"] == null ? string.Empty : context.Request["id"].ToString());

                    if (BillingController.DeleteBill(Int32.Parse(id)))
                        json = "{\"code\":\"OK\",\"message\":\"0x0008\"}";
                    else
                        json = "{\"code\":\"ERROR\",\"message\":\"0x9999\"}";
                }
            }
            catch
            {
                json = "{\"code\":\"ERROR\",\"message\":\"0x9999\"}";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();


        }

        private void deletepriority(HttpContext context)
        {
            string json = string.Empty;
            try
            {

                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));

                    string id = (context.Request["id"] == null ? string.Empty : context.Request["id"].ToString());

                    if (BillingController.DeletePriority(Int32.Parse(id)))
                        json = "{\"code\":\"OK\",\"message\":\" \"}";
                    else
                        json = "{\"code\":\"ERROR\",\"message\":\"Se ha producido un error en el envio. Inténtelo más tarde.\"}";
                }
            }
            catch
            {
                json = "{\"code\":\"ERROR\",\"message\":\"Se ha producido un error en el envio. Inténtelo más tarde.\"}";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();


        }

        private void deletefilterprofile(HttpContext context)
        {
            string json = string.Empty;
            try
            {

                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));

                    string id = (context.Request["id"] == null ? string.Empty : context.Request["id"].ToString());

                    if (BillingController.DeleteFilterProfile(Int32.Parse(id)))
                        json = "{\"code\":\"OK\",\"message\":\"0x0030\"}";
                    else
                        json = "{\"code\":\"ERROR\",\"message\":\"0x9999\"}";
                }
            }
            catch
            {
                json = "{\"code\":\"ERROR\",\"message\":\"0x9999\"}";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();


        }

        private void deleteblacklistmac(HttpContext context)
        {
            string json = string.Empty;
            try
            {

                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));

                    string idMACBlacklist = (context.Request["id"] == null ? string.Empty : context.Request["id"].ToString());

                    if (BillingController.DelteBlackListMac(Int32.Parse(idMACBlacklist)))
                        json = "{\"code\":\"OK\",\"message\":\" \"}";
                    else
                        json = "{\"code\":\"ERROR\",\"message\":\"Se ha producido un error en el envio. Inténtelo más tarde.\"}";
                }
            }
            catch
            {
                json = "{\"code\":\"ERROR\",\"message\":\"Se ha producido un error en el envio. Inténtelo más tarde.\"}";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();


        }

        private void deletesurveyQuestion(HttpContext context)
        {
            string json = string.Empty;
            try
            {
                //OBTENEMOS EL TIPO DE USUARIOS
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));

                    string id = string.Empty;
                    id = (context.Request["id"] == null ? string.Empty : context.Request["id"].ToString());

                    Surveys2 survey = BillingController.GetSurvey(Int32.Parse(id));

                    BillingController.DeleteSurveyQuestion(survey);

                    json = "{\"code\":\"OK\",\"message\":\"0x0032\"}";
                }
            }
            catch
            {
                json = "{\"code\":\"ERROR\",\"message\":\"0x9999\"}";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();
        }

        private ListUserManagementResult DisabledUsersManagement(HttpContext context)
        {
            ListUserManagementResult response = new ListUserManagementResult();
            try
            {
                int PageNumber = 0;

                //OBTENEMOS EL TIPO DE USUARIOS
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    Locations2 location = null;
                    Hotels hotel = null;
                    FDSGroups group = null;
                    SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group);

                    string page = (context.Request["page"] == null ? string.Empty : context.Request["page"].ToString());
                    string username = (context.Request["username"] == null ? string.Empty : context.Request["username"].ToString());
                    string start = (context.Request["start"] == null ? string.Empty : context.Request["start"].ToString());
                    string end = (context.Request["end"] == null ? string.Empty : context.Request["end"].ToString());
                    string billing = (context.Request["billing"] == null ? string.Empty : context.Request["billing"].ToString());
                    string room = (context.Request["room"] == null ? string.Empty : context.Request["room"].ToString());
                    string idmember = (context.Request["idmember"] == null ? string.Empty : context.Request["idmember"].ToString());
                    string idmembertype = (context.Request["idmembertype"] == null ? string.Empty : context.Request["idmembertype"].ToString());
                    string idfdsuser = (context.Request["idfdsuser"] == null ? string.Empty : context.Request["idfdsuser"].ToString());
                    string iot = (context.Request["iot"] == null ? string.Empty : context.Request["iot"].ToString());

                    int p = 0;
                    if (Int32.TryParse(page, out p))
                        PageIndex = p;

                    DateTime startfilter = (string.IsNullOrEmpty(start)) ? DateTime.Now.ToUniversalTime().AddDays(-1) : DateTime.Parse(start).AddHours(-siteGMT.dst).AddSeconds(-siteGMT.gmtoffset);
                    DateTime endfilter = (string.IsNullOrEmpty(end)) ? DateTime.Now.ToUniversalTime() : DateTime.Parse(end).AddHours(-siteGMT.dst).AddSeconds(-siteGMT.gmtoffset);
                    int roomFilter = (string.IsNullOrEmpty(room)) ? 0 : Int32.Parse(room);
                    int billingFilter = (string.IsNullOrEmpty(billing)) ? 0 : Int32.Parse(billing);
                    int idfdsuserfilter = (string.IsNullOrEmpty(idfdsuser)) ? 0 : Int32.Parse(idfdsuser);
                    int iotfilter = (string.IsNullOrEmpty(iot)) ? 0 : Int32.Parse(iot);

                    if (fdsUser.IdRol.Equals(3))
                        idfdsuserfilter = fdsUser.IdUser;

                    List<Users> list = null;
                    string ListHoteles = ListadoHoteles(group);
                    if (int.Parse(idmember) != 0)
                    {
                        int type = int.Parse(idmembertype);
                        switch (type)
                        {
                            case 1:
                                FDSGroups temp = BillingController.GetGroup(int.Parse(idmember));
                                ListHoteles = ListadoHoteles(temp); break;
                            case 2: hotel = BillingController.GetHotel(int.Parse(idmember)); break;
                            case 3:
                                location = BillingController.GetLocation(int.Parse(idmember));
                                hotel = BillingController.GetHotel(location.IdHotel); break;

                        }
                    }
                    FDSUsersAccess fdsuserAccess = BillingController.GetFDSUserAccess(fdsUser.IdUser);
                    List<FDSUsers> listFDSUser = null;
                    switch (fdsUser.IdType)
                    {
                        case 2: listFDSUser = BillingController.GetFDSUsersSite(fdsuserAccess.IdSite); break;
                        case 3: listFDSUser = BillingController.GetFDSUsersLocation(fdsuserAccess.IdLocation); break;
                        case 1: listFDSUser = BillingController.GetFDSUsers(); break;
                    }

                    if (location != null)
                    {
                        list = BillingController.GetDisabledUsersLocations(location.IdLocation, listFDSUser, username, startfilter, endfilter, billingFilter, roomFilter, idfdsuserfilter, iotfilter, PageIndex, PageSize, ref PageNumber);

                    }
                    else if (hotel != null)
                    {
                        list = BillingController.GetDisabledUsers(hotel.IdHotel, listFDSUser, username, startfilter, endfilter, billingFilter, roomFilter, idfdsuserfilter, iotfilter,  PageIndex, PageSize, ref PageNumber);
                    }
                    else
                    {
                        list = BillingController.GetDisabledUsers(ListHoteles, username, startfilter, endfilter, billingFilter, roomFilter, iotfilter,  PageIndex, PageSize, ref PageNumber);
                    }

                    response.list = new List<UserManagementResult>();
                    response.pageNumber = PageNumber;

                    string[] languages = context.Request.UserLanguages;
                    string lang = languages[0].Substring(0, 2).ToUpper();
                    List<FDSUsers> fdsusers = BillingController.GetFDSUsers();
                    foreach (Users u in list.OrderBy(a => a.ValidSince))
                    {
                        if (HttpContext.Current.Request.Cookies["location"] != null)
                        {
                            if (location.IdLocation.Equals(u.IdLocation))
                            {
                                UserManagementResult r = new UserManagementResult();
                                r.UserName = u.Name;
                                r.Password = u.Password;

                                if (u.TimeCredit <= 0)
                                    r.Room = lang.Equals("EN") ? "NO TIMECREDIT" : "CREDITO DE TIEMPO AGOTADO";
                                else if (u.VolumeUp <= 0)
                                    r.Room = lang.Equals("EN") ? "NO VOLUME CREDIT" : "VOLUMEN DE TRÁFICO AGOTADO";
                                else if (u.VolumeDown <= 0)
                                    r.Room = lang.Equals("EN") ? "NO VOLUME CREDIT" : "VOLUMEN DE TRÁFICO AGOTADO";
                                else if (u.ValidTill <= DateTime.Now)
                                    r.Room = lang.Equals("EN") ? "TICKET EXPIRED" : "TICKET CADUCADO";
                                else
                                    r.Room = lang.Equals("EN") ? "MANUAL DISABLED": "DESHABILITACIÓN MANUAL";

                                r.ValidSince = u.ValidSince.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                                r.ValidTill = u.ValidTill.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                                r.TimeCredit = u.TimeCredit.ToString();
                                r.IdUser = u.IdUser.ToString();
                                r.VolumeDown = (u.VolumeDown / (1024 * 1024)).ToString() + " MB";
                                r.VolumeUp = (u.VolumeUp / (1024 * 1024)).ToString() + " MB";
                                r.Comment = u.Comment;

                                if (u.Comment.Contains("timeout after first connection"))
                                {
                                    r.Room = lang.Equals("EN") ? "VALID TIME EXPIRED" : "TIEMPO DE VALIDEZ EXCEDIDO";
                                    string[] values = u.Comment.Split(' ');
                                    if (values.Length > 0)
                                    {
                                        try
                                        {
                                            string validtill = string.Format("{0} {1}", values[values.Length - 2], values[values.Length - 1]);
                                            r.ValidTill = DateTime.Parse(validtill).AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                                        }
                                        catch
                                        {

                                        }
                                    }

                                }

                                r.idfdsuser = u.IdFDSUser;
                                FDSUsers fds = (from t in fdsusers where t.IdUser.Equals(u.IdFDSUser) select t).FirstOrDefault();
                                if (fds != null)
                                    r.fdsuser = fds.Name;
                                else
                                    r.fdsuser = string.Empty;

                                response.list.Add(r);

                            }
                        }
                        else
                        {
                            UserManagementResult r = new UserManagementResult();
                            r.UserName = u.Name;
                            r.Password = u.Password;
                            if (u.TimeCredit <= 0)
                                r.Room = lang.Equals("EN") ? "NO TIMECREDIT" : "CREDITO DE TIEMPO AGOTADO";
                            else if (u.VolumeUp <= 0)
                                r.Room = lang.Equals("EN") ? "NO VOLUME CREDIT" : "VOLUMEN DE TRÁFICO AGOTADO";
                            else if (u.VolumeDown <= 0)
                                r.Room = lang.Equals("EN") ? "NO VOLUME CREDIT" : "VOLUMEN DE TRÁFICO AGOTADO";
                            else if (u.ValidTill <= DateTime.Now)
                                r.Room = lang.Equals("EN") ? "EXPIRED" : "CADUCADO";
                            else
                                r.Room = lang.Equals("EN") ? "MANUAL DISABLED" : "DESHABILITACIÓN MANUAL";

                            r.ValidSince = u.ValidSince.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                            r.ValidTill = u.ValidTill.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                            r.TimeCredit = u.TimeCredit.ToString();
                            r.IdUser = u.IdUser.ToString();
                            r.VolumeDown = (u.VolumeDown / (1024 * 1024)).ToString() + " MB";
                            r.VolumeUp = (u.VolumeUp / (1024 * 1024)).ToString() + " MB";
                            r.Comment = u.Comment;
                            r.idfdsuser = u.IdFDSUser;
                            FDSUsers fds = (from t in fdsusers where t.IdUser.Equals(u.IdFDSUser) select t).FirstOrDefault();
                            if (fds != null)
                                r.fdsuser = fds.Name;
                            else
                                r.fdsuser = string.Empty;

                            if (u.Comment.Contains("timeout after first connection"))
                            {
                                r.Room = lang.Equals("EN") ? "VALID TIME EXPIRED" : "TIEMPO DE VALIDEZ EXCEDIDO";
                                string[] values = u.Comment.Split(' ');
                                if (values.Length > 0)
                                {
                                    try
                                    {
                                        string validtill = string.Format("{0} {1}", values[values.Length - 2], values[values.Length - 1]);
                                        r.ValidTill = DateTime.Parse(validtill).AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                                    }
                                    catch
                                    {

                                    }
                                }

                            }

                            response.list.Add(r);
                        }
                    }
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private ListUserManagementResult DisabledUsersManagementIOT(HttpContext context)
        {
            ListUserManagementResult response = new ListUserManagementResult();
            try
            {
                int PageNumber = 0;

                //OBTENEMOS EL TIPO DE USUARIOS
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    Locations2 location = null;
                    Hotels hotel = null;
                    FDSGroups group = null;

                    SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group);

                    string page = (context.Request["page"] == null ? string.Empty : context.Request["page"].ToString());
                    string username = (context.Request["username"] == null ? string.Empty : context.Request["username"].ToString());
                    string start = (context.Request["start"] == null ? string.Empty : context.Request["start"].ToString());
                    string end = (context.Request["end"] == null ? string.Empty : context.Request["end"].ToString());
                    string billing = (context.Request["billing"] == null ? string.Empty : context.Request["billing"].ToString());
                    string room = (context.Request["room"] == null ? string.Empty : context.Request["room"].ToString());
                    string idmember = (context.Request["idmember"] == null ? string.Empty : context.Request["idmember"].ToString());
                    string idmembertype = (context.Request["idmembertype"] == null ? string.Empty : context.Request["idmembertype"].ToString());

                    int p = 0;
                    if (Int32.TryParse(page, out p))
                        PageIndex = p;

                    DateTime startfilter = (string.IsNullOrEmpty(start)) ? DateTime.MaxValue : DateTime.Parse(start).AddHours(-siteGMT.dst).AddSeconds(-siteGMT.gmtoffset);
                    DateTime endfilter = (string.IsNullOrEmpty(end)) ? DateTime.MaxValue : DateTime.Parse(end).AddHours(-siteGMT.dst).AddSeconds(-siteGMT.gmtoffset);
                    int roomFilter = (string.IsNullOrEmpty(room)) ? 0 : Int32.Parse(room);
                    int billingFilter = (string.IsNullOrEmpty(billing)) ? 0 : Int32.Parse(billing);


                    List<Users> list = null;
                    string ListHoteles = ListadoHoteles(group);
                    if (int.Parse(idmember) != 0)
                    {
                        int type = int.Parse(idmembertype);
                        switch (type)
                        {
                            case 1:
                                FDSGroups temp = BillingController.GetGroup(int.Parse(idmember));
                                ListHoteles = ListadoHoteles(temp); break;
                            case 2: hotel = BillingController.GetHotel(int.Parse(idmember)); break;
                            case 3:
                                location = BillingController.GetLocation(int.Parse(idmember));
                                hotel = BillingController.GetHotel(location.IdHotel); break;

                        }
                    }

                    FDSUsersAccess fdsuserAccess = BillingController.GetFDSUserAccess(fdsUser.IdUser);
                    List<FDSUsers> listFDSUser = null;
                    switch (fdsUser.IdType)
                    {
                        case 2: listFDSUser = BillingController.GetFDSUsersSite(fdsuserAccess.IdSite); break;
                        case 3: listFDSUser = BillingController.GetFDSUsersLocation(fdsuserAccess.IdLocation); break;
                        case 1: listFDSUser = BillingController.GetFDSUsers(); break;
                    }

                    if (location != null)
                        list = BillingController.GetDisabledUsersLocationsIoT(location.IdLocation, username, startfilter, endfilter, billingFilter, roomFilter, PageIndex, PageSize, ref PageNumber);
                    else if (hotel != null)
                    {
                        list = BillingController.GetDisabledUsersIoT(hotel.IdHotel, username, startfilter, endfilter, billingFilter, roomFilter, PageIndex, PageSize, ref PageNumber);
                    }
                    else 
                        list = BillingController.GetDisabledUsersIoT(ListHoteles, username, startfilter, endfilter, billingFilter, roomFilter, PageIndex, PageSize, ref PageNumber);

                    response.list = new List<UserManagementResult>();
                    response.pageNumber = PageNumber;

                    string[] languages = context.Request.UserLanguages;
                    string lang = languages[0].Substring(0, 2).ToUpper();
                    List<FDSUsers> fdsusers = BillingController.GetFDSUsers();
                    foreach (Users u in list.OrderBy(a => a.ValidSince))
                    {
                        if (HttpContext.Current.Request.Cookies["location"] != null)
                        {
                            if (location.IdLocation.Equals(u.IdLocation))
                            {
                                UserManagementResult r = new UserManagementResult();
                                r.UserName = u.Name;
                                r.Password = u.Password;

                                if (u.TimeCredit <= 0)
                                    r.Room = lang.Equals("EN") ? "NO TIMECREDIT" : "CREDITO DE TIEMPO AGOTADO";
                                else if (u.VolumeUp <= 0)
                                    r.Room = lang.Equals("EN") ? "NO VOLUME CREDIT" : "VOLUMEN DE TRÁFICO AGOTADO";
                                else if (u.VolumeDown <= 0)
                                    r.Room = lang.Equals("EN") ? "NO VOLUME CREDIT" : "VOLUMEN DE TRÁFICO AGOTADO";
                                else if (u.ValidTill <= DateTime.Now)
                                    r.Room = lang.Equals("EN") ? "EXPIRED" : "CADUCADO";
                                else
                                    r.Room = lang.Equals("EN") ? "MANUAL DISABLED" : "DESHABILITACIÓN MANUAL";

                                r.ValidSince = u.ValidSince.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                                r.ValidTill = u.ValidTill.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                                r.TimeCredit = u.TimeCredit.ToString();
                                r.IdUser = u.IdUser.ToString();
                                r.VolumeDown = (u.VolumeDown / (1024 * 1024)).ToString() + " MB";
                                r.VolumeUp = (u.VolumeUp / (1024 * 1024)).ToString() + " MB";

                                if (u.Comment.Contains("timeout after first connection"))
                                {
                                    r.Room = lang.Equals("EN") ? "VALID TIME EXPIRED" : "TIEMPO DE VALIDEZ EXCEDIDO";
                                    string[] values = u.Comment.Split(' ');
                                    if (values.Length > 0)
                                    {
                                        try
                                        {
                                            string validtill = string.Format("{0} {1}", values[values.Length - 2], values[values.Length - 1]);
                                            r.ValidTill = DateTime.Parse(validtill).AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                                        }
                                        catch
                                        {

                                        }
                                    }

                                }

                                r.idfdsuser = u.IdFDSUser;
                                FDSUsers fds = (from t in fdsusers where t.IdUser.Equals(u.IdFDSUser) select t).FirstOrDefault();
                                if (fds != null)
                                    r.fdsuser = fds.Name;
                                else
                                    r.fdsuser = string.Empty;

                                response.list.Add(r);

                            }
                        }
                        else
                        {
                            UserManagementResult r = new UserManagementResult();
                            r.UserName = u.Name;
                            r.Password = u.Password;
                            if (u.TimeCredit <= 0)
                                r.Room = lang.Equals("EN") ? "NO TIMECREDIT" : "CREDITO DE TIEMPO AGOTADO";
                            else if (u.VolumeUp <= 0)
                                r.Room = lang.Equals("EN") ? "NO VOLUME CREDIT" : "VOLUMEN DE TRÁFICO AGOTADO";
                            else if (u.VolumeDown <= 0)
                                r.Room = lang.Equals("EN") ? "NO VOLUME CREDIT" : "VOLUMEN DE TRÁFICO AGOTADO";
                            else if (u.ValidTill <= DateTime.Now)
                                r.Room = lang.Equals("EN") ? "EXPIRED" : "CADUCADO";
                            else
                                r.Room = lang.Equals("EN") ? "MANUAL DISABLED" : "DESHABILITACIÓN MANUAL";

                            r.ValidSince = u.ValidSince.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                            r.ValidTill = u.ValidTill.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                            r.TimeCredit = u.TimeCredit.ToString();
                            r.IdUser = u.IdUser.ToString();
                            r.VolumeDown = (u.VolumeDown / (1024 * 1024)).ToString() + " MB";
                            r.VolumeUp = (u.VolumeUp / (1024 * 1024)).ToString() + " MB";

                            r.idfdsuser = u.IdFDSUser;
                            FDSUsers fds = (from t in fdsusers where t.IdUser.Equals(u.IdFDSUser) select t).FirstOrDefault();
                            if (fds != null)
                                r.fdsuser = fds.Name;
                            else
                                r.fdsuser = string.Empty;

                            if (u.Comment.Contains("timeout after first connection"))
                            {
                                r.Room = lang.Equals("EN") ? "VALID TIME EXPIRED" : "TIEMPO DE VALIDEZ EXCEDIDO";
                                string[] values = u.Comment.Split(' ');
                                if (values.Length > 0)
                                {
                                    try
                                    {
                                        string validtill = string.Format("{0} {1}", values[values.Length - 2], values[values.Length - 1]);
                                        r.ValidTill = DateTime.Parse(validtill).AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                                    }
                                    catch
                                    {

                                    }
                                }

                            }

                            response.list.Add(r);
                        }
                    }
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private DisclaimerResult Disclaimer(HttpContext context)
        {
            DisclaimerResult response = new DisclaimerResult();
            try
            {
                //OBTENEMOS EL TIPO DE USUARIOS
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    string id = (context.Request["id"] == null ? string.Empty : context.Request["id"].ToString());

                    Disclaimers d = BillingController.GetDisclaimer(Int32.Parse(id));

                    response.idhotel = d.IdHotel;
                    response.idlanguage = d.IdLanguage;
                    response.iddisclaimer = d.IdDisclaimer;
                    response.text = d.Text;
                    response.idlocation = d.IdLocation;
                    response.date = d.Date.ToShortDateString();
                    response.type = d.IdType;
                    response.typeDescription = d.IdType.Equals(1) ? "P" : "U";
                    response.language = BillingController.GetLanguage(d.IdLanguage).Name;
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private ListDisclaimerResult Disclaimers(HttpContext context)
        {
            ListDisclaimerResult response = new ListDisclaimerResult();
            try
            {
                string id = (context.Request["id"] == null ? string.Empty : context.Request["id"].ToString());

                List<Disclaimers> list = BillingController.GetDisclaimers(Int32.Parse(id));

                response.list = new List<DisclaimerResult>();

                foreach (Disclaimers b in list.OrderByDescending(a => a.Date))
                {
                    DisclaimerResult br = new DisclaimerResult();
                    br.idlocation = b.IdLocation;
                    br.iddisclaimer = b.IdDisclaimer;
                    br.idlanguage = b.IdLanguage;
                    br.text = b.Text;
                    br.type = b.IdType;
                    br.typeDescription =  b.IdType.Equals(1) ? "P" : "U";
                    br.language = BillingController.GetLanguage(b.IdLanguage).Name;
                    br.date = b.Date.ToShortDateString();

                    response.list.Add(br);
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private void DisclaimerSave(HttpContext context, Dictionary<string, string> sData)
        {
            string json = string.Empty;
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));

                    string id = sData["id"];
                    string text = sData["text"];
                    string type = sData["type"];

                    Disclaimers obj = BillingController.GetDisclaimer(Int32.Parse(id));

                    obj.Text = text;
                    obj.Date = DateTime.Now;
                    obj.IdType = Int32.Parse(type);

                    BillingController.SaveDisclaimer(obj);

                    json = "{\"code\":\"OK\",\"message\":\"Disclaimer save successfully\"}";
                }
                else
                {
                    json = "{\"code\":\"ERROR\",\"message\":\"Error, please try again later.\"}";
                }
            }
            catch (Exception ex)
            {
                json = "{\"code\":\"ERROR\",\"message\":\" " + ex.Message + ".\"}";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();
        }

        private void exportToExcel(HttpContext context)
        {
            string json = string.Empty;
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    string idroom = (context.Request["r"] == null ? string.Empty : context.Request["r"].ToString());
                    string start = (context.Request["s"] == null ? string.Empty : context.Request["s"].ToString());
                    string end = (context.Request["e"] == null ? string.Empty : context.Request["e"].ToString());
                    string idmember = (context.Request["idmember"] == null ? string.Empty : context.Request["idmember"].ToString());
                    string idmembertype = (context.Request["idmembertype"] == null ? string.Empty : context.Request["idmembertype"].ToString());
                    string idfdsuser = (context.Request["idfdsuser"] == null ? string.Empty : context.Request["idfdsuser"].ToString());

                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    Locations2 location;
                    Hotels hotel;
                    FDSGroups group;
                    SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group);
                    Currencies2 currency = null;

                    if (group !=null)
                        currency = BillingController.Currency(group.Currency);
                    else if (hotel != null)
                        currency = BillingController.Currency(hotel.CurrencyCode);
                    else
                        currency = BillingController.Currency(location.Currency);

                    if (string.IsNullOrEmpty(start))
                    {
                        start = DateTime.Now.ToUniversalTime().AddDays(-1).ToString();
                        end = DateTime.Now.ToUniversalTime().ToString();

                    }
                    else
                    {
                        start = DateTime.Parse(start).AddSeconds(-siteGMT.gmtoffset).ToString();
                        end = DateTime.Parse(end).AddSeconds(-siteGMT.gmtoffset).ToString();
                    }
                    string ListHoteles = ListadoHoteles(group);
                    if (int.Parse(idmember) != 0)
                    {
                        int type = int.Parse(idmembertype);
                        switch (type)
                        {
                            case 1:
                                FDSGroups temp = BillingController.GetGroup(int.Parse(idmember));
                                ListHoteles = ListadoHoteles(temp); break;
                            case 2: hotel = BillingController.GetHotel(int.Parse(idmember)); break;
                            case 3:
                                location = BillingController.GetLocation(int.Parse(idmember));
                                hotel = BillingController.GetHotel(location.IdHotel); break;

                        }
                    }
                    List<BillingInfo> listCompleta = null;
                    
                    string billing = string.Empty;
                    int idfdsuserfilter = 0;
                    if (!string.IsNullOrEmpty(idfdsuser))
                        idfdsuserfilter = int.Parse(idfdsuser);
                    if (fdsUser.IdRol.Equals(3))
                        idfdsuserfilter = fdsUser.IdUser;

                    FDSUsersAccess fdsuserAccess = BillingController.GetFDSUserAccess(fdsUser.IdUser);
                    List<FDSUsers> listFDSUser = null;
                    switch (fdsUser.IdType)
                    {
                        case 2: listFDSUser = BillingController.GetFDSUsersSite(fdsuserAccess.IdSite); break;
                        case 3: listFDSUser = BillingController.GetFDSUsersLocation(fdsuserAccess.IdLocation); break;
                        case 1: listFDSUser = BillingController.GetFDSUsers(); break;
                    }

                    if (location != null)
                    {
                        listCompleta = BillingController.produccionLocation(location.IdLocation, int.Parse(idroom), billing, idfdsuserfilter, start, end);
                    }
                    else if (hotel != null)
                    {
                        listCompleta = BillingController.produccion(hotel.IdHotel, int.Parse(idroom), start, end, idfdsuserfilter);
                    }
                    else
                    {
                        listCompleta = BillingController.produccion(ListHoteles, DateTime.Parse(start), DateTime.Parse(end), int.Parse(idroom), idfdsuserfilter);
                    }

                    double total = 0;
                    List<FDSUsers> fdsUsers = BillingController.GetFDSUsers();


                    DataTable table = new DataTable();
                    table.Columns.Add("IdBilling");    
                    table.Columns.Add("IdSite");
                    table.Columns.Add("BillingDate");
                    table.Columns.Add("BuyFrom");
                    table.Columns.Add("Room");
                    table.Columns.Add("Product");
                    table.Columns.Add("Amount");
                    table.Columns.Add("User");
                    table.Columns.Add("IdFdsUser");
                    table.Columns.Add("FDSUser");
                    

                    List<Locations2> listLocations = new List<Data.Model.Locations2>();
                    if (ListHoteles.Length > 0)
                    {
                        foreach (string id in ListHoteles.Split(','))
                            listLocations = listLocations.Concat(BillingController.GetLocations(int.Parse(id))).ToList();
                    }
                    else
                        listLocations = BillingController.GetLocations(hotel.IdHotel);

                    string currency_base = ConfigurationManager.AppSettings["CurrencyBase"].ToString();

                    foreach (BillingInfo b in listCompleta.OrderBy(a => a.BillingCharge))
                    {
                        DataRow row = table.NewRow();
                        Locations2 location_site = (from t in listLocations where t.IdLocation.Equals(b.IdLocation) select t).FirstOrDefault();
                        if (location_site != null)
                            row["IdSite"] = location_site.IdHotel;
                        row["IdBilling"] = b.IdBilling.ToString();
                        row["BillingDate"] = b.BillingCharge.AddSeconds(siteGMT.gmtoffset).ToString();
                        row["Room"] = b.Room;
                        row["Product"] = b.BillingType;

                        double cambio = 1;
                        if (!b.IdLocation.Equals(0))
                        {
                            if (location != null)
                            {
                                FDSUsers u = (from t in listFDSUser where t.IdUser.Equals(b.IdFDSUser) select t).FirstOrDefault();
                                if (u != null)
                                {
                                    Locations2 temp_location = BillingController.GetLocation(b.IdLocation);
                                    Currencies2 moneda = BillingController.Currency(temp_location.Currency);

                                    if (!temp_location.Currency.Equals(currency.Code))
                                    {
                                        if (!currency_base.Equals(currency.Code))
                                        {
                                            Currencies2 moneda_nueva_base = BillingController.Currency(currency.Code);
                                            cambio = ((1 / moneda_nueva_base.Exchange.Value) * moneda.Exchange.Value);
                                        }
                                        else
                                            cambio = moneda.Exchange.Value;
                                    }
                                }
                            }
                            else if (hotel != null)
                            {
                                FDSUsers u = (from t in listFDSUser where t.IdUser.Equals(b.IdFDSUser) select t).FirstOrDefault();
                                if (u != null)
                                {
                                    Locations2 temp_location = BillingController.GetLocation(b.IdLocation);
                                    Currencies2 moneda = BillingController.Currency(temp_location.Currency);

                                    if (!temp_location.Currency.Equals(currency.Code))
                                    {
                                        if (!currency_base.Equals(currency.Code))
                                        {
                                            Currencies2 moneda_nueva_base = BillingController.Currency(currency.Code);
                                            cambio = ((1 / moneda_nueva_base.Exchange.Value) * moneda.Exchange.Value);
                                        }
                                        else
                                            cambio = moneda.Exchange.Value;
                                    }
                                }
                            }
                            else
                            {
                                Locations2 loc = BillingController.GetLocation(b.IdLocation);
                                Currencies2 moneda = BillingController.Currency(loc.Currency);

                                if (!loc.Currency.Equals(currency.Code))
                                {
                                    if (!currency_base.Equals(currency.Code))
                                    {
                                        Currencies2 moneda_nueva_base = BillingController.Currency(currency.Code);
                                        cambio = ((1 / moneda_nueva_base.Exchange.Value) * moneda.Exchange.Value);
                                    }
                                    else
                                        cambio = moneda.Exchange.Value;
                                }
                            }
                        }
                        row["Amount"] = b.Amount * cambio;
                        
                        if (b.Flag.Equals(3))
                            row["BuyFrom"] = "Wifi 360";
                        else if (b.Flag.Equals(5))
                            row["BuyFrom"] = "Wifi 360 Manual";
                        else if (b.Flag.Equals(6))
                            row["BuyFrom"] = "Paypal";
                        else if (b.Flag.Equals(8))
                            row["BuyFrom"] = "Visa/MasterCard";

                        row["User"] = (b.IdUser.Equals(0) ? string.Empty : b.Name);
                        row["IdFdsUser"] = b.IdFDSUser;

                        FDSUsers fdsuserbill = (from t in fdsUsers where t.IdUser.Equals(b.IdFDSUser) select t).FirstOrDefault();
                        if (fdsuserbill != null)
                            row["FDSUser"] = fdsuserbill.Name;

                        total += b.Amount;
                        table.Rows.Add(row);
                    }

                    string folder_name = string.Empty;
                    if ((location != null) || (hotel != null))
                        folder_name = hotel.IdHotel.ToString();
                    else
                        folder_name = string.Format("group_{0}", group.IdGroup);

                    if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Format("../resources/docs/{0}", folder_name))))
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Format("../resources/docs/{0}", folder_name)));

                    string filename = HttpContext.Current.Server.MapPath(string.Format("../resources/docs/{0}/billing.xlsx", folder_name));
                    string url = string.Format("/resources/docs/{0}/billing.xlsx", folder_name);
                    var workbook = new XLWorkbook();
                    var worksheet = workbook.Worksheets.Add("Hoja1");

                    worksheet.Cell("A1").Value = "Identificador";
                    worksheet.Cell("B1").Value = "IdSite";
                    worksheet.Cell("C1").Value = "Fecha de compra";
                    worksheet.Cell("D1").Value = "Punto de compra";
                    worksheet.Cell("E1").Value = "Habitación";
                    worksheet.Cell("F1").Value = "Descripción";
                    worksheet.Cell("G1").Value = "Importe";
                    worksheet.Cell("H1").Value = "Usuario";
                    
                    //si no tiene rol de tendero no se añaden los propietarios
                    if (!fdsUser.IdRol.Equals(3))
                    {
                        worksheet.Cell("H1").Value = "Vendedor";
                    }

                    int prow = 2;
                    foreach (DataRow row in table.Rows)
                    {
                        worksheet.Cell(string.Format("A{0}", prow)).Value = row["IdBilling"].ToString();
                        worksheet.Cell(string.Format("B{0}", prow)).Value = row["IdSite"].ToString();

                        worksheet.Cell(string.Format("C{0}", prow)).Value = row["BillingDate"].ToString();
                        worksheet.Cell(string.Format("C{0}", prow)).DataType = XLCellValues.DateTime;
                        worksheet.Cell(string.Format("D{0}", prow)).Value = row["BuyFrom"].ToString();
                        worksheet.Cell(string.Format("E{0}", prow)).Value = row["Room"].ToString();
                        worksheet.Cell(string.Format("F{0}", prow)).Value = row["Product"].ToString();
                        worksheet.Cell(string.Format("G{0}", prow)).Value = row["Amount"].ToString();
                        worksheet.Cell(string.Format("G{0}", prow)).DataType = XLCellValues.Number;
                        worksheet.Cell(string.Format("H{0}", prow)).Value = row["User"].ToString();
                        if (!fdsUser.IdRol.Equals(3))
                        {
                            worksheet.Cell(string.Format("I{0}", prow)).Value = row["FDSUser"].ToString();
                        }
                        prow = prow + 1;
                    }

                    workbook.SaveAs(filename);

                    json = "{\"code\":\"OK\",\"message\":\"" + url + "\"}";
                }
            }
            catch (Exception ex)
            {
                json = "{\"code\":\"ERROR\",\"message\":\"We have a problem, try again later.\"}";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();
        }

        private static SitesGMT GetSiteGMt(out Locations2 location, out Hotels hotel)
        {
            SitesGMT siteGMT = null;
            location = null;
            hotel = null;
            try
            {
                if (HttpContext.Current.Request.Cookies["location"] != null)
                {
                    location = BillingController.GetLocation(Int32.Parse(HttpContext.Current.Request.Cookies["location"].Value));
                    hotel = BillingController.GetHotel(location.IdHotel);
                    siteGMT = BillingController.SiteGMTSitebyLocation(location.IdLocation);
                    if (siteGMT.Id.Equals(0))
                        siteGMT = BillingController.SiteGMTSitebySite(location.IdHotel);
                }
                else
                {
                    hotel = BillingController.GetHotel(Int32.Parse(HttpContext.Current.Request.Cookies["site"].Value));
                    siteGMT = BillingController.SiteGMTSitebySite(hotel.IdHotel);
                }

                return siteGMT;
            }
            catch(Exception ex)
            {
                return null;
            }

            
        }

        private static void GetCurrenciesOnlineExchange(ref List<Exchanges> listadodeconversiones, string Currencie_base)
        {
            try
            {
                Currencie_base = "EUR"; //LA API ACTUAL NO PERMITE CAMBIO DE BASE
                string listadodesumbols = string.Empty;
                bool firstime = true;
                foreach (Exchanges new_exchange in listadodeconversiones)
                {
                    if (firstime)
                    {
                        firstime = false;
                        listadodesumbols = new_exchange.Currency;
                    }
                    else
                        listadodesumbols += "," + new_exchange.Currency;
                }
                HttpClient client = new HttpClient();               
                string ACCESS_KEY = "d86f200d152c740d71f034b2bb411cb3";
                string _address = "http://data.fixer.io/api/latest?access_key=" + ACCESS_KEY + "&base="+ Currencie_base +"& symbols=" + listadodesumbols;

                var response = client.GetAsync(_address).Result;
                if (response.IsSuccessStatusCode)
                {                    
                    string responseString = response.Content.ReadAsStringAsync().Result;
                    XmlDocument doc = JsonConvert.DeserializeXmlNode(responseString, "root");                 
                    
                    foreach(Exchanges item in listadodeconversiones)
                    {
                        string val = doc.ChildNodes[0].ChildNodes[4].SelectSingleNode(item.Currency).InnerText;
                        double val_exchange = double.Parse(val.Replace(".", ","));
                        item.Exchange = val_exchange;
                        item.ReverseExchange = (1 / val_exchange);
                        item.Base = "EUR";                       
                    }
                }
            }
            catch (Exception ex)
            {
                ;
            }
        }

        private static SitesGMT GetSiteGMt(out Locations2 location, out Hotels hotel, out FDSGroups group)
        {
            SitesGMT siteGMT = null;
            location = null;
            hotel = null;
            group = null;
            try
            {
                if (HttpContext.Current.Request.Cookies["location"] != null)
                {
                    location = BillingController.GetLocation(Int32.Parse(HttpContext.Current.Request.Cookies["location"].Value));
                    hotel = BillingController.GetHotel(location.IdHotel);
                    siteGMT = BillingController.SiteGMTSitebyLocation(location.IdLocation);
                    if (siteGMT.Id.Equals(0))
                        siteGMT = BillingController.SiteGMTSitebySite(location.IdHotel);
                }
                else if (HttpContext.Current.Request.Cookies["site"] != null)
                {
                    hotel = BillingController.GetHotel(Int32.Parse(HttpContext.Current.Request.Cookies["site"].Value));
                    siteGMT = BillingController.SiteGMTSitebySite(hotel.IdHotel);
                }
                else
                {
                    group = BillingController.GetGroup(Int32.Parse(HttpContext.Current.Request.Cookies["group"].Value));
                    siteGMT = BillingController.SiteGMTSitebyGroup(group.IdGroup);
                }


                return siteGMT;
            }
            catch (Exception ex)
            {
                return null;
            }


        }

        private void exportactivity(HttpContext context)
        {
            string json = string.Empty;
            try
            {

                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    Hotels hotel = null;
                    Locations2 location = null;
                    FDSGroups group = null;
                    SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group);

                    string idroom = (context.Request["r"] == null ? string.Empty : context.Request["r"].ToString());
                    string start = (context.Request["s"] == null ? string.Empty : context.Request["s"].ToString());
                    string end = (context.Request["e"] == null ? string.Empty : context.Request["e"].ToString());
                    string user = (context.Request["u"] == null ? string.Empty : context.Request["u"].ToString());
                    string idmember = (context.Request["idmember"] == null ? string.Empty : context.Request["idmember"].ToString());
                    string idmembertype = (context.Request["idmembertype"] == null ? string.Empty : context.Request["idmembertype"].ToString());
                    string page = (context.Request["page"] == null ? string.Empty : context.Request["page"].ToString());


                    if ((start.Equals("undefined")) || string.IsNullOrEmpty(start))
                    {
                        start = DateTime.Now.AddSeconds(-siteGMT.gmtoffset).AddHours(-24).ToString("dd/MM/yyyy HH:mm:ss");
                        end = DateTime.Now.AddSeconds(-siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");

                    }
                    else
                    {
                        start = DateTime.Parse(start).AddSeconds(-siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                        end = DateTime.Parse(end).AddSeconds(-siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                    }

                    int p = 0;
                    if (Int32.TryParse(page, out p))
                        PageIndex = p;

                    List<ClosedSessions2> list = null;
                    string listadoHoteles = ListadoHoteles(group);
                    if (int.Parse(idmember) != 0)
                    {
                        int type = int.Parse(idmembertype);
                        switch (type)
                        {
                            case 1:
                                FDSGroups temp = BillingController.GetGroup(int.Parse(idmember));
                                listadoHoteles = ListadoHoteles(temp); break;
                            case 2: hotel = BillingController.GetHotel(int.Parse(idmember)); break;
                            case 3:
                                location = BillingController.GetLocation(int.Parse(idmember));
                                hotel = BillingController.GetHotel(location.IdHotel); break;

                        }
                    }

                    if (location != null)
                    {
                        FDSLocationsIdentifier identifier = BillingController.GetFDSLocationIdentifier(Int32.Parse(HttpContext.Current.Request.Cookies["location"].Value));
                        list = BillingController.GetClosedSession(identifier.Identifier, hotel.IdHotel, user, DateTime.Parse(start), DateTime.Parse(end));
                    }
                    else if (hotel != null)
                    {
                        list = BillingController.GetClosedSession(hotel.IdHotel, user, DateTime.Parse(start), DateTime.Parse(end));
                    }
                    else
                        list = BillingController.GetClosedSessionListadoHoteles(listadoHoteles, user, DateTime.Parse(start), DateTime.Parse(end));

                    string folder_name = string.Empty;
                    if (hotel != null)
                        folder_name = hotel.IdHotel.ToString();
                    else
                        folder_name = string.Format("group_{0}", group.IdGroup);

                    if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Format("../resources/docs/{0}", folder_name))))
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Format("../resources/docs/{0}", folder_name)));

                    string filename = HttpContext.Current.Server.MapPath(string.Format("../resources/docs/{0}/closedsessions_{1}_{2}.xlsx", folder_name, DateTime.Parse(start).ToString("ddMMyy"), DateTime.Parse(end).ToString("ddMMyy")));
                    string url = string.Format("../resources/docs/{0}/closedsessions_{1}_{2}.xlsx", folder_name, DateTime.Parse(start).ToString("ddMMyy"), DateTime.Parse(end).ToString("ddMMyy"));
                    var workbook = new XLWorkbook();
                    var worksheet = workbook.Worksheets.Add("Hoja1");

                    worksheet.Cell("A1").Value = "Username";
                    worksheet.Cell("B1").Value = "Session Started";
                    worksheet.Cell("C1").Value = "Session Terminated";
                    worksheet.Cell("D1").Value = "Session Time";
                    worksheet.Cell("E1").Value = "Bytes Upload";
                    worksheet.Cell("F1").Value = "Bytes Download";
                    worksheet.Cell("G1").Value = "Total Bytes";
                    worksheet.Cell("H1").Value = "MAC";
                    worksheet.Cell("I1").Value = "Location";
                    if (hotel != null) { 
                        if (hotel.IdVendor.Equals(2))
                        {
                            worksheet.Cell("J1").Value = "NAS_MAC";
                            worksheet.Cell("K1").Value = "Description";
                        }
                    }
                    int prow = 2;

                    foreach (ClosedSessions2 b in list)
                    {
                        worksheet.Cell(string.Format("A{0}", prow)).Value = b.UserName;
                        worksheet.Cell(string.Format("A{0}", prow)).DataType = XLCellValues.Text;
                        worksheet.Cell(string.Format("B{0}", prow)).Value = b.SessionStarted.Value.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                        worksheet.Cell(string.Format("B{0}", prow)).DataType = XLCellValues.DateTime;
                        worksheet.Cell(string.Format("C{0}", prow)).Value = b.SessionTerminated.Value.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                        worksheet.Cell(string.Format("C{0}", prow)).DataType = XLCellValues.DateTime;
                        TimeSpan diff = b.SessionTerminated.Value - b.SessionStarted.Value;
                        worksheet.Cell(string.Format("D{0}", prow)).Value = diff.TotalSeconds.ToString();
                        worksheet.Cell(string.Format("D{0}", prow)).DataType = XLCellValues.Number;
                        worksheet.Cell(string.Format("E{0}", prow)).Value = b.BytesIn.ToString();
                        worksheet.Cell(string.Format("E{0}", prow)).DataType = XLCellValues.Number;
                        worksheet.Cell(string.Format("F{0}", prow)).Value = b.BytesOut.ToString();
                        worksheet.Cell(string.Format("F{0}", prow)).DataType = XLCellValues.Number;
                        worksheet.Cell(string.Format("G{0}", prow)).Value = (b.BytesIn + b.BytesOut).ToString();
                        worksheet.Cell(string.Format("G{0}", prow)).DataType = XLCellValues.Text;
                        worksheet.Cell(string.Format("H{0}", prow)).Value = b.CallerID;
                        worksheet.Cell(string.Format("H{0}", prow)).DataType = XLCellValues.Text;
                        worksheet.Cell(string.Format("I{0}", prow)).Value = b.LocationIdentifier;
                        worksheet.Cell(string.Format("I{0}", prow)).DataType = XLCellValues.Text;
                        if (hotel != null)
                        {
                            if (hotel.IdVendor.Equals(2))
                            {
                                List<MerakiLocationsGPS> merakiLocations = BillingController.SelectMerakiLocationsGPS(hotel.IdHotel);
                                MerakiLocationsGPS mlgps = (from t in merakiLocations where t.NAS_MAC.Equals(b.NAS_MAC) select t).FirstOrDefault();
                                if (mlgps != null)
                                {
                                    worksheet.Cell(string.Format("J{0}", prow)).Value = b.NAS_MAC;
                                    worksheet.Cell(string.Format("J{0}", prow)).DataType = XLCellValues.Text;
                                    worksheet.Cell(string.Format("K{0}", prow)).Value = mlgps.Description;
                                    worksheet.Cell(string.Format("K{0}", prow)).DataType = XLCellValues.Text;
                                }
                            }
                        }

                        prow++;
                    }
                    
                    workbook.SaveAs(filename);

                    json = "{\"code\":\"OK\",\"message\":\"" + url + "\"}";
                }
            }
            catch (Exception ex)
            {
                json = "{\"code\":\"ERROR\",\"message\":\"We have a problem, try again later.\"}";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();


        }

        private void exportToExcelLoyalty(HttpContext context)
        {
            string json = string.Empty;
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    string start = (context.Request["s"] == null ? string.Empty : context.Request["s"].ToString());
                    string end = (context.Request["e"] == null ? string.Empty : context.Request["e"].ToString());
                    string idmember = (context.Request["idmember"] == null ? string.Empty : context.Request["idmember"].ToString());
                    string idmembertype = (context.Request["idmembertype"] == null ? string.Empty : context.Request["idmembertype"].ToString());

                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    Hotels hotel = null;
                    Locations2 location = null;
                    FDSGroups group = null;

                    SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group);

                    string page = (context.Request["page"] == null ? string.Empty : context.Request["page"].ToString());

                    if ((start.Equals("undefined")) || string.IsNullOrEmpty(start))
                    {
                        start = DateTime.Now.AddDays(-1).ToString();
                        end = DateTime.Now.ToString();

                    }
                    else
                    {
                        start = DateTime.Parse(start).AddSeconds(-siteGMT.gmtoffset).ToString();
                        end = DateTime.Parse(end).AddSeconds(-siteGMT.gmtoffset).ToString();
                    }

                    int p = 0;
                    if (Int32.TryParse(page, out p))
                        PageIndex = p;

                    List<Loyalties2> list = null;
                    string ListHoteles = ListadoHoteles(group);
                    if (int.Parse(idmember) != 0)
                    {
                        int type = int.Parse(idmembertype);
                        switch (type)
                        {
                            case 1:
                                FDSGroups temp = BillingController.GetGroup(int.Parse(idmember));
                                ListHoteles = ListadoHoteles(temp); break;
                            case 2: hotel = BillingController.GetHotel(int.Parse(idmember));
                                ListHoteles = idmember; break;
                            case 3:
                                location = BillingController.GetLocation(int.Parse(idmember));
                                hotel = BillingController.GetHotel(location.IdHotel); break;

                        }
                    }

                    if (location != null)
                        list = BillingController.GetLoyatiesLocation(location.IdLocation, DateTime.Parse(start), DateTime.Parse(end));
                    else if(hotel != null)
                        list = BillingController.GetLoyaties(hotel.IdHotel, DateTime.Parse(start), DateTime.Parse(end));
                    else
                        list = BillingController.GetLoyaties(ListHoteles, DateTime.Parse(start), DateTime.Parse(end));

                    List<Locations2> locations = new List<Locations2>();
                    if (!string.IsNullOrEmpty(ListHoteles))
                    {
                        foreach (string id in ListHoteles.Split(','))
                        {
                            locations = locations.Concat(BillingController.GetLocations(int.Parse(id))).ToList();
                        }
                    }

                    string folder_name = string.Empty;
                    if (hotel != null)
                        folder_name = hotel.IdHotel.ToString();
                    else
                        folder_name = string.Format("group_{0}", group.IdGroup);

                    if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Format("../resources/docs/{0}", folder_name))))
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Format("../resources/docs/{0}", folder_name)));

                    string filename = HttpContext.Current.Server.MapPath(string.Format("../resources/docs/{0}/loyalties_{1}_{2}.xlsx", folder_name, DateTime.Parse(start).ToString("ddMMyy"), DateTime.Parse(end).ToString("ddMMyy")));
                    string url = string.Format("../resources/docs/{0}/loyalties_{1}_{2}.xlsx", folder_name, DateTime.Parse(start).ToString("ddMMyy"), DateTime.Parse(end).ToString("ddMMyy"));
                    var workbook = new XLWorkbook();
                    var worksheet = workbook.Worksheets.Add("Hoja1");

                    worksheet.Cell("A1").Value = "Date";
                    worksheet.Cell("B1").Value = "Hotel ID";
                    worksheet.Cell("C1").Value = "Name";
                    worksheet.Cell("D1").Value = "Surname";
                    worksheet.Cell("E1").Value = "Email";
                    worksheet.Cell("F1").Value = "Birthday";
                    worksheet.Cell("G1").Value = "Country";
                    worksheet.Cell("H1").Value = "City";
                    worksheet.Cell("I1").Value = "Address";
                    worksheet.Cell("J1").Value = "ZipCode";
                    worksheet.Cell("K1").Value = "Gender";
                    worksheet.Cell("L1").Value = "Language";
                    worksheet.Cell("M1").Value = "Linkedin";
                    worksheet.Cell("N1").Value = "Facebook";
                    worksheet.Cell("O1").Value = "Google";
                    worksheet.Cell("P1").Value = "Twitter";
                    worksheet.Cell("Q1").Value = "Instagram";
                    worksheet.Cell("R1").Value = "Mobile_Prefix";
                    worksheet.Cell("S1").Value = "Mobile";
                    worksheet.Cell("T1").Value = "Landline_Prefix";
                    worksheet.Cell("U1").Value = "Landline";
                    worksheet.Cell("V1").Value = "Accept_Emails";
                    worksheet.Cell("W1").Value = "RAW";

                    int prow = 2;
                    foreach (Loyalties2 l in list)
                    {
                        worksheet.Cell(string.Format("A{0}", prow)).Value = l.Date.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");

                        if (l.IdLocatin != null)
                        {
                            Locations2 a = (from t in locations where t.IdLocation.Equals(l.IdLocatin) select t).FirstOrDefault();
                            FDSLocationsIdentifier locationIdentifies = BillingController.GetFDSLocationIdentifier(a.IdLocation);
                            if (locationIdentifies.Id != 0)
                            {
                                worksheet.Cell(string.Format("B{0}", prow)).Value = locationIdentifies.Identifier;
                                worksheet.Cell(string.Format("B{0}", prow)).DataType = XLCellValues.Text;
                            }
                            else
                                worksheet.Cell(string.Format("B{0}", prow)).Value = a.Description;
                        }
                        if (l.Email.Contains('@'))
                            worksheet.Cell(string.Format("E{0}", prow)).Value = l.Email;
                        string[] platform = l.Survey.Split(':');
                        if (platform.Length > 1)
                        {
                            string[] fields = platform[1].Split('|');
                            if (platform[0].Equals("facebook"))
                            {
                                foreach (string field in fields)
                                {
                                    string[] values = field.Split('=');
                                    switch (values[0].Trim())
                                    {
                                        case "nombre": worksheet.Cell(string.Format("C{0}", prow)).Value = values[1]; break;
                                        case "apellidos": worksheet.Cell(string.Format("D{0}", prow)).Value = values[1]; break;
                                        case "sexo": worksheet.Cell(string.Format("K{0}", prow)).Value = values[1].ToUpper().Equals("MALE") ? "M" : "F"; break;
                                        case "nacimiento":
                                            string[] fechanacimiento = values[1].Split('/');
                                            worksheet.Cell(string.Format("F{0}", prow)).Value = string.Format("{0}/{1}/{2}", fechanacimiento[1], fechanacimiento[0], fechanacimiento[2]); break;
                                        case "language": worksheet.Cell(string.Format("L{0}", prow)).Value = values[1].Trim().Substring(0,2).ToUpper(); 
                                                         worksheet.Cell(string.Format("G{0}", prow)).Value = values[1].Trim().Substring(0,2).ToUpper(); break;
                                        case "id": worksheet.Cell(string.Format("N{0}", prow)).Value = values[1]; break;
                                    }
                                }
                            }
                            else if (platform[0].Equals("instagram"))
                            {
                                foreach (string field in fields)
                                {
                                    string[] values = field.Split('=');
                                    switch (values[0].Trim())
                                    {
                                        case "nombre": worksheet.Cell(string.Format("C{0}", prow)).Value = values[1]; break;
                                        case "language":
                                            worksheet.Cell(string.Format("L{0}", prow)).Value = values[1].Trim().Substring(0, 2).ToUpper();
                                            worksheet.Cell(string.Format("G{0}", prow)).Value = values[1].Trim().Substring(0, 2).ToUpper(); break;
                                        case "id": worksheet.Cell(string.Format("Q{0}", prow)).Value = values[1]; break;
                                    }
                                }
                            }
                            else if (platform[0].Equals("loginform"))
                            {
                                foreach (string field in fields)
                                {
                                    if (!string.IsNullOrEmpty(field) && !field.Equals(" "))
                                    {
                                        string[] values = field.Split('=');
                                        if (!string.IsNullOrEmpty(values[1]) && !values[1].Equals(" "))
                                        {
                                            switch (values[0].Trim().ToLower())
                                            {
                                                case "nombre": worksheet.Cell(string.Format("C{0}", prow)).Value = values[1]; break;
                                                case "apellidos": worksheet.Cell(string.Format("D{0}", prow)).Value = values[1]; break;
                                                case "codigo postal": worksheet.Cell(string.Format("J{0}", prow)).Value = values[1]; break;
                                                case "pais": if (platform.Length.Equals(2))
                                                    {
                                                        worksheet.Cell(string.Format("G{0}", prow)).Value = string.Format("{0}", values[1]);
                                                        worksheet.Cell(string.Format("L{0}", prow)).Value = values[1].ToUpper(); break;
                                                    }
                                                    else
                                                    {
                                                        worksheet.Cell(string.Format("G{0}", prow)).Value = string.Format("{0}:{1}", values[1], platform[2]);
                                                        worksheet.Cell(string.Format("L{0}", prow)).Value = string.Format("{0}:{1}", values[1], platform[2]);
                                                    }
                                                    break;
                                                case "language": worksheet.Cell(string.Format("L{0}", prow)).Value = values[1].ToUpper(); break;
                                            }
                                        }
                                    }
                                }
                            }

                            worksheet.Cell(string.Format("W{0}", prow)).Value = string.Format("{0}", l.Survey);
                            prow = prow + 1;
                        }
                    }

                    workbook.SaveAs(filename);

                    json = "{\"code\":\"OK\",\"message\":\"" + url + "\"}";
                }
            }
            catch (Exception ex)
            {
                json = "{\"code\":\"ERROR\",\"message\":\"We have a problem, try again later.\"}";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();
        }

        private static FDSUserResult GetFDSUserForId(HttpContext context)
        {
            FDSUserResult response = new FDSUserResult();
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    string idFdsUser = (context.Request["id"] == null ? string.Empty : context.Request["id"].ToString());

                    FDSUsers user = BillingController.GetFDSUser(Int32.Parse(idFdsUser));
                    Rols rol = BillingController.GetFDSRol(user.IdRol);
                    FDSUsersType type = BillingController.GetFDSUserType(user.IdType);

                    response.idFDSUser = user.IdUser;
                    response.IdType = user.IdType;
                    response.IdRol = user.IdRol;
                    response.Name = user.Name;
                    response.Password = user.Password;
                    response.UserName = user.Username;
                    response.type = string.Format("{0} ({1})", type.Name, type.Descpription);
                    response.rol = string.Format("{0} ({1})", rol.Name, rol.Description);

                    FDSUsersAccess useraccess = BillingController.GetFDSUserAccess(user.IdUser);
                                        
                    switch (user.IdType)
                    {
                        case 1:
                            response.idmember = useraccess.IdGroup;                            
                            break;
                        case 2:
                            response.idmember = useraccess.IdSite;
                            break;
                        case 3:
                            response.idmember = useraccess.IdLocation;
                            break;                        
                    }                 
                }
            }
            catch (Exception ex)
            {

            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private static FDSUserResult GetFDSUser(HttpContext context)
        {
            FDSUserResult response = new FDSUserResult();
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    HttpCookie coockie = HttpContext.Current.Request.Cookies["FDSUser"];
                    FDSUsers user = BillingController.GetFDSUser(Int32.Parse(coockie.Value));
                    Rols rol = BillingController.GetFDSRol(user.IdRol);
                    FDSUsersType type = BillingController.GetFDSUserType(user.IdType);

                    response.idFDSUser = user.IdUser;
                    response.IdType = user.IdType;
                    response.IdRol = user.IdRol;
                    response.Name = user.Name;
                    response.Password = user.Password;
                    response.UserName = user.Username;
                    response.type = string.Format("{0} ({1})", type.Name, type.Descpription);
                    response.rol = string.Format("{0} ({1})", rol.Name, rol.Description);

                }
            }
            catch (Exception ex)
            {
                
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private ListFDSUsersResult GetFDSUsers(HttpContext context)
        {
            ListFDSUsersResult response = new ListFDSUsersResult();
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    string idmember = (context.Request["idmember"] == null ? string.Empty : context.Request["idmember"].ToString());
                    string idmembertype = (context.Request["idmembertype"] == null ? string.Empty : context.Request["idmembertype"].ToString());
                    string start = (context.Request["start"] == null ? string.Empty : context.Request["start"].ToString());
                    string end = (context.Request["end"] == null ? string.Empty : context.Request["end"].ToString());

                    Hotels hotel = null;
                    Locations2 location = null;
                    FDSGroups group = null;
                    SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group);


                    FDSGroups groupMember = null;
                    List<FDSUsers> list = new List<FDSUsers>();
                    if (string.IsNullOrEmpty(idmember))
                    {
                        if (location != null)
                            list = BillingController.GetFDSUsersLocation(location.IdLocation);
                        else if (hotel != null)
                            list = BillingController.GetFDSUsersSite(hotel.IdHotel);
                    }
                    else
                    {
                        switch (idmembertype)
                        {
                            case "2": list = BillingController.GetFDSUsersSite(int.Parse(idmember)); break;
                            case "3": list = BillingController.GetFDSUsersLocation(int.Parse(idmember)); break;
                            case "1": groupMember = BillingController.GetGroup(int.Parse(idmember)); break;
                        }
                    }


                    string listHoteles = ListadoHoteles(groupMember);
                    if (string.IsNullOrEmpty(listHoteles))
                    {
                        switch(idmembertype)
                        {
                            case "2": listHoteles = idmember.ToString(); break;
                            
                        }
                    }

                    response.list = new List<FDSUserResult>();
                    foreach (FDSUsers b in list.OrderBy(a => a.Name))
                    {
                        FDSUserResult br = new FDSUserResult();
                        br.idFDSUser = b.IdUser;
                        br.Name = b.Name;

                        response.list.Add(br);
                    }
                    
                    if ((start.Equals("undefined")) || string.IsNullOrEmpty(start))
                    {
                        start = DateTime.Now.ToUniversalTime().AddDays(-1).ToString();
                        end = DateTime.Now.ToUniversalTime().ToString();

                    }
                    else
                    {
                        start = DateTime.Parse(start).AddSeconds(-siteGMT.gmtoffset).ToString();
                        end = DateTime.Parse(end).AddSeconds(-siteGMT.gmtoffset).ToString();
                    }

                        
                    List<FDSUsers> users_in_billing = BillingController.GetFDSUsersInBillings(listHoteles, DateTime.Parse(start), DateTime.Parse(end));
                    foreach(FDSUsers user in users_in_billing)
                    {
                        if (response.list.Find(x => x.idFDSUser.Equals(user.IdUser)) == null)
                        {
                            FDSUserResult br = new FDSUserResult();
                            br.idFDSUser = user.IdUser;
                            br.Name = user.Name;

                            response.list.Add(br);
                        }
                    }
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;


        }
        private ListFilterProfileResult filterprofilesFilter(HttpContext context)
        {
            ListFilterProfileResult response = new ListFilterProfileResult();
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    string idmember = (context.Request["idmember"] == null ? string.Empty : context.Request["idmember"].ToString());
                    string idmembertype = (context.Request["idmembertype"] == null ? string.Empty : context.Request["idmembertype"].ToString());

                    List<FilteringProfiles> list = new List<FilteringProfiles>();
                    switch (idmembertype)
                    {
                        case "2": list = BillingController.GetFilteringProfiles(int.Parse(idmember)); break;
                        case "3":
                            Locations2 loca = BillingController.GetLocation(int.Parse(idmember));
                            Hotels h = BillingController.GetHotel(loca.IdHotel);
                            list = BillingController.GetFilteringProfiles(h.IdHotel); break;
                    }
                    response.list = new List<FilterProfileResult>();

                    foreach (FilteringProfiles b in list.OrderBy(a => a.IdFilter))
                    {
                        FilterProfileResult br = new FilterProfileResult();
                        br.IdFilter = b.IdFilter;
                        br.Description = b.Description;

                        response.list.Add(br);
                    }
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;


        }

        private ListFilterProfileResult filterprofilesUserFilter(HttpContext context)
        {
            ListFilterProfileResult response = new ListFilterProfileResult();
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    string iduser = (context.Request["iduser"] == null ? string.Empty : context.Request["iduser"].ToString());

                    Users user = BillingController.GetRadiusUser(int.Parse(iduser));
                    List<FilteringProfiles> list = BillingController.GetFilteringProfiles(user.IdHotel);
                    response.list = new List<FilterProfileResult>();

                    foreach (FilteringProfiles b in list.OrderBy(a => a.IdFilter))
                    {
                        FilterProfileResult br = new FilterProfileResult();
                        br.IdFilter = b.IdFilter;
                        br.Description = b.Description;

                        response.list.Add(br);
                    }
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;


        }
        private ListFilterProfileResult filterprofiles(HttpContext context)
        {
            ListFilterProfileResult response = new ListFilterProfileResult();
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    //if (HttpContext.Current.Request.Cookies["site"] != null)
                    //    fdsUser.IdHotel = Int32.Parse(HttpContext.Current.Request.Cookies["site"].Value);

                    Hotels hotel = null;
                    Locations2 location = null;
                    FDSGroups group = null;
                    SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group);
                    string listHoteles = ListadoHoteles(group);
                    List<FilteringProfiles> list = new List<FilteringProfiles>();
                    if (!string.IsNullOrEmpty(listHoteles))
                    {
                        string[] ids = listHoteles.Split(',');
                        foreach (string id in ids)
                            list = list.Concat(BillingController.GetFilteringProfiles(int.Parse(id))).ToList();
                    }
                    else
                        list = BillingController.GetFilteringProfiles(hotel.IdHotel);

                    //Obtenemos IdSite -> desde sitesetting
                    string idsite = (context.Request["idsite"] == null ? string.Empty : context.Request["idsite"].ToString());
                    int idsi = 0;
                    if (Int32.TryParse(idsite, out idsi))
                    {
                        list = BillingController.GetFilteringProfiles(idsi);
                    }
                    
                    response.list = new List<FilterProfileResult>();

                    foreach (FilteringProfiles b in list.OrderBy(a => a.IdFilter))
                    {
                        FilterProfileResult br = new FilterProfileResult();
                        br.IdFilter = b.IdFilter;
                        br.Description = b.Description;

                        response.list.Add(br);
                    }
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;


        }

        private FilterProfileResult filterprofile(HttpContext context)
        {
            FilterProfileResult response = new FilterProfileResult();
            try
            {
                string id = (context.Request["id"] == null ? string.Empty : context.Request["id"].ToString());

                Wifi360.Data.Model.FilteringProfiles obj = BillingController.GetFilteringProfile(Int32.Parse(id));

                response.IdFilter = obj.IdFilter;
                response.Description = obj.Description;

            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private ListUserActivityResult graphic(HttpContext context)
        {
            ListUserActivityResult response = new ListUserActivityResult();
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    //if (HttpContext.Current.Request.Cookies["site"] != null)
                    //    fdsUser.IdHotel = Int32.Parse(HttpContext.Current.Request.Cookies["site"].Value);

                    string idUser = (context.Request["id"] == null ? string.Empty : context.Request["id"].ToString());
                    string mac = (context.Request["mac"] == null ? string.Empty : context.Request["mac"].ToString());

                    Users u = BillingController.GetRadiusUser(Int32.Parse(idUser));

                    BillingTypes billigType = BillingController.ObtainBillingType(u.IdBillingType);
                    Rooms room = BillingController.GetRoom(u.IdRoom);

                    List<Accounting> actividad = BillingController.Accounting(u.Name, u.ValidSince, mac);
                    response.list = new List<UserActivityResult>();
                    foreach (Accounting a in actividad.OrderBy(b => b.DateInserted))
                    {
                        UserActivityResult obj = new UserActivityResult();
                        System.TimeSpan span = new System.TimeSpan(System.DateTime.Parse("1/1/1970").Ticks);
                        System.DateTime time = a.DateInserted.Value.Subtract(span);
                        obj.Date = (time.Ticks / 10000).ToString();

                        obj.BytesIn = a.BytesIn.Value;
                        obj.BytesOut = a.BytesOut.Value;

                        response.list.Add(obj);
                    }
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private ListMemberGroupResult GroupMembers(HttpContext context)
        {
            ListMemberGroupResult response = new ListMemberGroupResult();
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    Hotels hotel = null;
                    Locations2 location = null;
                    FDSGroups group = null;
                    SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group);

                    response.list = new List<MemberGroupResult>();
                    List<FDSGroupsMembers> list = new List<FDSGroupsMembers>();
                    if (group != null)
                        list = BillingController.SelectFDSGroupsMembers(group.IdGroup);

                    if (list.Count() > 0)
                    {
                        foreach (FDSGroupsMembers l in list)
                        {
                            MemberGroupResult r = new MemberGroupResult();
                            r.IdMemberType = l.IdMemberType;
                            r.IdMember = l.IdMember;
                            switch(l.IdMemberType)
                            {
                                case 1: r.Name = string.Format("(G) - {0} ", BillingController.GetGroup(l.IdMember).Name); break;
                                case 2: r.Name = string.Format("(S) - {0} ", BillingController.GetHotel(l.IdMember).Name); break;
                                case 3: r.Name = string.Format("(L) - {0} ", BillingController.GetLocation(l.IdMember).Description); break;
                            }

                            response.list.Add(r);
                        }
                    }
                    else
                    {
                        MemberGroupResult result = new MemberGroupResult();
                        result.IdMember = hotel.IdHotel;
                        result.IdMemberType = 2;
                        result.Name = hotel.Name;
                        response.list.Add(result);
                    }
                }

            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private ListUserManagementResult CreateTicket(HttpContext context, Dictionary<string, string> sData)
        {
            ListUserManagementResult response = new ListUserManagementResult();
            try
            {
                //OBTENEMOS EL TIPO DE USUARIOS
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    Hotels hotel = null;
                    Locations2 location = null;
                    SitesGMT siteGMT = GetSiteGMt(out location, out hotel);

                    //PARAMETRO OBLIGATORIOS

                    string idroom = sData["idroom"];
                    string idbillingtype = sData["idbillingtype"];
                    string numero = sData["numero"];
                    string username = sData["username"];
                    string password = sData["password"];
                    string mail = sData["mail"];
                    string start = sData["start"];
                    string end = sData["end"];
                    string comment = sData["comment"];

                    Rooms room = BillingController.GetRoom(Int32.Parse(idroom));
                    BillingTypes billingType = BillingController.ObtainBillingType(Int32.Parse(idbillingtype));
                    if (location == null)
                        location = BillingController.GetLocation(billingType.IdLocation);
                    List<ParametrosConfiguracion> parametros = BillingController.GetParametrosConfiguracion(hotel.IdHotel);
                    int numeroTickets = Int32.Parse(numero);
                    if (numeroTickets > 3000)
                        numeroTickets = 3000;
                    int longUserName = 3;
                    int longPassword = 3;
                    string prefixUserName = string.Empty;

                    foreach (ParametrosConfiguracion obj in parametros)
                    {
                        switch (obj.Key.ToUpper())
                        {
                            case "LONGUSERNAME": longUserName = Int32.Parse(obj.value); break;
                            case "LONGPASSWORD": longPassword = Int32.Parse(obj.value); break;
                            case "USERNAMEPREFIX": prefixUserName = obj.value; break;
                        }
                    }

                    if (longPassword < 3)
                        longPassword = 3;
                    if (longUserName < 3)
                        longUserName = 3;

                    response.list = new List<UserManagementResult>();

                    FDSGroups GroupRealmShare = BillingController.GetGroupShareRealm(hotel.IdHotel);
                    string listadoHoteles = string.Empty;
                    if (GroupRealmShare.IdGroup != 0)
                        listadoHoteles = ListadoHoteles(GroupRealmShare);

                    //TICKET SIMPLE
                    if (numeroTickets.Equals(1))
                    {
                        #region generación de ticket unitario

                        UserManagementResult u = new UserManagementResult();
                        Users existuser = null;
                        if (!string.IsNullOrEmpty(username))
                        {
                            if (string.IsNullOrEmpty(listadoHoteles))
                                existuser = BillingController.GetRadiusUser(username.ToUpper(), hotel.IdHotel);
                            else
                            {
                                string[] ids = listadoHoteles.Split(',');
                                foreach (string id in ids)
                                {
                                    existuser = BillingController.GetRadiusUser(username.ToUpper(), Int32.Parse(id));
                                    if (!existuser.IdUser.Equals(0) || existuser.Enabled)
                                        break;
                                }
                            }
                        }
                        else
                        {
                            existuser = new Users();
                        }


                        if (existuser.IdUser.Equals(0) || !existuser.Enabled)
                        {
                            Users u2 = null;
                            if (string.IsNullOrEmpty(username))
                            {
                                string _user = string.Empty;
                                do
                                {
                                    _user = string.Format("{0}{1}", prefixUserName, util.createRandomUserNumber(longUserName));
                                    if (string.IsNullOrEmpty(listadoHoteles))
                                        u2 = BillingController.GetRadiusUser(_user, hotel.IdHotel);
                                    else
                                    {
                                        string[] ids = listadoHoteles.Split(',');
                                        foreach(string id in ids)
                                        {
                                            u2 = BillingController.GetRadiusUser(_user, Int32.Parse(id));
                                            if (u2.Enabled)
                                                break;
                                        }
                                    }
                                        

                                } while (u2.Enabled);

                                username = _user.ToUpper();
                            }

                            if (string.IsNullOrEmpty(password))
                                password = util.CreateRandomPassword(longPassword);

                            Users user = new Users();


                            user.IdHotel = hotel.IdHotel;
                            user.IdRoom = room.IdRoom;
                            user.IdBillingType = billingType.IdBillingType;
                            user.BWDown = billingType.BWDown;
                            user.BWUp = billingType.BWUp;
                            user.IdLocation = location.IdLocation;

                            user.ValidSince = string.IsNullOrEmpty(start) ? DateTime.Now.ToUniversalTime() : DateTime.Parse(start).AddSeconds(-siteGMT.gmtoffset);
                            user.ValidTill = string.IsNullOrEmpty(end) ? user.ValidSince.AddHours(billingType.ValidTill) : DateTime.Parse(end).AddSeconds(-siteGMT.gmtoffset);

                            //CALCULAMOS EL TIMECREDIT PARA RECORTARLO EN TAL CASO
                            int credit = billingType.TimeCredit / billingType.MaxDevices; //Calculamos el tiempo por dispositivo
                            if (user.ValidTill < user.ValidSince.AddSeconds(credit))
                            {
                                TimeSpan diff = user.ValidTill - user.ValidSince;
                                user.TimeCredit = billingType.MaxDevices * (int)diff.TotalSeconds;
                            }
                            else
                                user.TimeCredit = billingType.TimeCredit;

                            user.MaxDevices = billingType.MaxDevices;
                            user.Priority = billingType.IdPriority;
                            user.VolumeDown = billingType.VolumeDown;
                            user.VolumeUp = billingType.VolumeUp;
                            user.Enabled = true;
                            user.Name = username.ToUpper();
                            user.Password = password.ToUpper();
                            user.CHKO = true;
                            user.BuyingFrom = 0;
                            user.BuyingCallerID = "NOT APPLICABLE";
                            user.Filter_Id = billingType.Filter_Id;
                            user.Comment = comment;
                            user.IdFDSUser = fdsUser.IdUser;

                            BillingController.SaveUser(user);
                            //SE AÑADE A LA TABLA DE BILLING SI ES PREMIUM O SI ES CUSTOM Y PRECIO > 0 O SI ES LOGIN Y PRECIO > 0 O SI ES VOUCHER Y PRECIO > 0
                            if (billingType.IdBillingModule.Equals(2) || billingType.Price > 0)
                            { 
                                Billings bill = new Billings();

                                bill.IdHotel = hotel.IdHotel;
                                bill.IdBillingType = billingType.IdBillingType;
                                bill.IdRoom = room.IdRoom;
                                bill.Amount = billingType.Price;
                                bill.BillingDate = DateTime.Now.ToUniversalTime();
                                bill.BillingCharge = bill.BillingDate;
                                bill.Flag = 3;
                                bill.IdLocation = location.IdLocation;
                                bill.IdUser = user.IdUser;
                                bill.IdFDSUser = fdsUser.IdUser;
                                if (billingType.IdBillingModule.Equals(2) || billingType.IdBillingModule.Equals(4))
                                    bill.Billable = true;

                                BillingController.SaveBilling(bill);
                            }

                            u.AccessType = hotel.Name;
                            u.UserName = username.ToUpper();
                            u.Password = password.ToUpper();
                            u.IdUser = hotel.IdHotel.ToString();
                            u.ValidSince = user.ValidSince.AddSeconds(siteGMT.gmtoffset).ToString();
                            u.ValidTill = user.ValidTill.AddSeconds(siteGMT.gmtoffset).ToString();
                            u.MaxDevices = user.MaxDevices.ToString();
                            u.BWDown = user.BWDown.ToString();
                            u.BWUp = user.BWUp.ToString();
                            u.VolumeDown = (user.VolumeDown / 1048576).ToString(); //CONVERTIMOS LOS BYTES EN GB
                            u.VolumeUp = (user.VolumeUp / 1048576).ToString();

                            TimeSpan t = TimeSpan.FromSeconds(user.TimeCredit / user.MaxDevices);
                            string time = string.Empty;
                            if (user.TimeCredit >= 86400)
                                time = string.Format("{0}d: {1:D2}h: {2:D2} m: {3:D2}s", t.Days, t.Hours, t.Minutes, t.Seconds);
                            else
                                time = string.Format("{0:D2}h: {1:D2}m: {2:D2}s", t.Hours, t.Minutes, t.Seconds);
                            u.TimeCredit = time;

                            if (util.ValidateEmail(mail))
                                util.sendMail(hotel.IdHotel, billingType.IdBillingType, mail, username.ToUpper(), password.ToUpper());
                        }
                        else
                        {

                            u.IdUser = "0";
                            //NOMBRE DE USUARIO NO DISPONIBLE
                        }

                        response.list.Add(u);

                        #endregion
                    }
                    else //TICKETS MASIVOS
                    {
                        for (int i = 0; i < numeroTickets; i++)
                        {
                            string user = string.Empty;
                            Users u2 = null;
                            do
                            {
                                user = string.Format("{0}{1}", prefixUserName, util.createRandomUserNumber(longUserName));
                                if (string.IsNullOrEmpty(listadoHoteles))
                                    u2 = BillingController.GetRadiusUser(user, hotel.IdHotel);
                                else
                                {
                                    string[] ids = listadoHoteles.Split(',');
                                    foreach (string id in ids)
                                    {
                                        u2 = BillingController.GetRadiusUser(user, Int32.Parse(id));
                                        if (u2.Enabled)
                                            break;
                                    }
                                }
                            } while (u2.Enabled);

                            Users randUser = new Users();
                            randUser.Name = user;
                            randUser.Password = util.CreateRandomPassword(longPassword);

                            randUser.IdHotel = hotel.IdHotel;
                            randUser.IdRoom = room.IdRoom;
                            randUser.IdBillingType = billingType.IdBillingType;
                            randUser.BWDown = billingType.BWDown;
                            randUser.BWUp = billingType.BWUp;

                            randUser.ValidSince = string.IsNullOrEmpty(start) ? DateTime.Now.ToUniversalTime() : DateTime.Parse(start).AddSeconds(-siteGMT.gmtoffset);
                            randUser.ValidTill = string.IsNullOrEmpty(end) ? randUser.ValidSince.AddHours(billingType.ValidTill) : DateTime.Parse(end).AddSeconds(-siteGMT.gmtoffset);
                            randUser.IdLocation = location.IdLocation;

                            int credit = billingType.TimeCredit / billingType.MaxDevices; //Calculamos el tiempo por dispositivo
                            if (randUser.ValidTill < randUser.ValidSince.AddSeconds(credit))
                            {
                                TimeSpan diff = randUser.ValidTill - randUser.ValidSince;
                                randUser.TimeCredit = billingType.MaxDevices * (int)diff.TotalSeconds;
                            }
                            else
                                randUser.TimeCredit = billingType.TimeCredit;

                            randUser.MaxDevices = billingType.MaxDevices;
                            randUser.Priority = billingType.IdPriority;
                            randUser.VolumeDown = billingType.VolumeDown;
                            randUser.VolumeUp = billingType.VolumeUp;
                            randUser.Enabled = true;
                            randUser.CHKO = true;
                            randUser.BuyingFrom = 1;
                            randUser.BuyingCallerID = "00-00-00-00-00-00";
                            randUser.Filter_Id = billingType.Filter_Id;
                            randUser.Comment = comment;
                            randUser.IdFDSUser = fdsUser.IdUser;

                            BillingController.SaveUser(randUser);

                            //SE AÑADE A LA TABLA DE BILLING SI ES PREMIUM O SI ES CUSTOM Y PRECIO > 0 O SI ES LOGIN Y PRECIO > 0 O SI ES VOUCHER Y PRECIO > 0
                            if (billingType.IdBillingModule.Equals(2) || billingType.Price > 0)
                            {
                                Billings bill = new Billings();

                                bill.IdHotel = hotel.IdHotel;
                                bill.IdBillingType = billingType.IdBillingType;
                                bill.IdRoom = room.IdRoom;
                                bill.Amount = billingType.Price;
                                bill.BillingDate = DateTime.Now.ToUniversalTime();
                                bill.BillingCharge = bill.BillingDate;
                                bill.Flag = 3;
                                bill.IdLocation = location.IdLocation;
                                bill.IdUser = randUser.IdUser;
                                bill.IdFDSUser = fdsUser.IdUser;
                                if (billingType.IdBillingModule.Equals(2) || billingType.IdBillingModule.Equals(4))
                                    bill.Billable = true;

                                BillingController.SaveBilling(bill);
                            }

                            UserManagementResult ur = new UserManagementResult();
                            ur.UserName = randUser.Name.ToUpper();
                            ur.Password = randUser.Password.ToUpper();
                            ur.IdUser = hotel.IdHotel.ToString();
                            ur.ValidSince = randUser.ValidSince.AddSeconds(siteGMT.gmtoffset).ToString();
                            ur.ValidTill = randUser.ValidTill.AddSeconds(siteGMT.gmtoffset).ToString();
                            ur.MaxDevices = randUser.MaxDevices.ToString();
                            ur.BWDown = randUser.BWDown.ToString();
                            ur.BWUp = randUser.BWUp.ToString();
                            ur.VolumeDown = (randUser.VolumeDown / 1048576).ToString(); //CONVERTIMOS LOS BYTES EN GB
                            ur.VolumeUp = (randUser.VolumeUp / 1048576).ToString();


                            TimeSpan t = TimeSpan.FromSeconds(randUser.TimeCredit / randUser.MaxDevices);
                            string time = string.Empty;
                            if (randUser.TimeCredit >= 86400)
                                time = string.Format("{0}d: {1:D2}h: {2:D2} m: {3:D2}s", t.Days, t.Hours, t.Minutes, t.Seconds);
                            else
                                time = string.Format("{0:D2}h: {1:D2}m: {2:D2}s", t.Hours, t.Minutes, t.Seconds);
                            ur.TimeCredit = time;
                            ur.AccessType = billingType.Description;

                            response.list.Add(ur);
                        }

                        response.pageNumber = hotel.IdHotel;
                    }
                }
            }
            catch (Exception ex)
            {
                CaptivePortalLogInternal internalLog = new CaptivePortalLogInternal();
                internalLog.NSEId = "";
                internalLog.CallerID = "";
                internalLog.Date = DateTime.Now;
                internalLog.UserName = "";
                internalLog.Password = "";
                internalLog.Page = "Create Ticket";
                internalLog.IdLocation = 0;
                internalLog.NomadixResponse = "ERROR NO CONTEMPLADO - " + ex.Message;
                internalLog.RadiusResponse = "OK";

                BillingController.SaveCaptivePortalLogInternal(internalLog);
                //json = "{\"code\":\"ERROR\",\"message\":\"Se ha producido un error en el envio. Inténtelo más tarde.\"}";
            }


            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private ListUserManagementResult CreateVoucher(HttpContext context, Dictionary<string, string> sData)
        {
            ListUserManagementResult response = new ListUserManagementResult();
            try
            {
                //OBTENEMOS EL TIPO DE USUARIOS
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    Hotels hotel = null;
                    Locations2 location = null;
                    SitesGMT siteGMT = GetSiteGMt(out location, out hotel);
                    //PARAMETRO OBLIGATORIOS

                    string idroom = sData["idroom"];
                    string idbillingtype = sData["idbillingtype"];
                    string numero = sData["numero"];
                    string username = sData["username"];
                    string mail = sData["mail"];
                    string start = sData["start"];
                    string end = sData["end"];
                    string comment = sData["comment"];

                    Rooms room = BillingController.GetRoom(Int32.Parse(idroom));
                    BillingTypes billingType = BillingController.ObtainBillingType(Int32.Parse(idbillingtype));
                    if (location == null)
                        location = BillingController.GetLocation(billingType.IdLocation);

                    List<ParametrosConfiguracion> parametros = BillingController.GetParametrosConfiguracion(hotel.IdHotel);
                    int numeroTickets = Int32.Parse(numero);
                    if (numeroTickets > 5000)
                        numeroTickets = 5000;
                    int longUserName = 3;
                    string prefixUserName = string.Empty;

                    foreach (ParametrosConfiguracion obj in parametros)
                    {
                        switch (obj.Key.ToUpper())
                        {
                            case "LONGVOUCHERUSER": longUserName = Int32.Parse(obj.value); break;
                            case "VOUCHERPREFIX": prefixUserName = obj.value; break;
                        }
                    }

                    if (longUserName < 3)
                        longUserName = 3;

                    response.list = new List<UserManagementResult>();

                    FDSGroups GroupRealmShare = BillingController.GetGroupShareRealm(hotel.IdHotel);
                    string listadoHoteles = string.Empty;
                    if (GroupRealmShare.IdGroup != 0)
                        listadoHoteles = ListadoHoteles(GroupRealmShare);

                    //TICKET SIMPLE
                    if (numeroTickets.Equals(1))
                    {
                        #region generación de ticket unitario

                        UserManagementResult u = new UserManagementResult();
                        Users existuser = null;
                        if (!string.IsNullOrEmpty(username))
                        {
                            if (string.IsNullOrEmpty(listadoHoteles))
                                existuser = BillingController.GetRadiusUser(username.ToUpper(), hotel.IdHotel);
                            else
                            {
                                string[] ids = listadoHoteles.Split(',');
                                foreach (string id in ids)
                                {
                                    existuser = BillingController.GetRadiusUser(username.ToUpper(), Int32.Parse(id));
                                    if (!existuser.IdUser.Equals(0) || existuser.Enabled)
                                        break;
                                }
                            }
                        }
                        else
                            existuser = new Users();
                     

                        if (existuser.IdUser.Equals(0) || !existuser.Enabled)
                        {
                            Users u2 = null;
                            if (string.IsNullOrEmpty(username))
                            {
                                string _user = string.Empty;
                                do
                                {
                                    _user = string.Format("{0}{1}", prefixUserName, util.createRandomUserNumber(longUserName));
                                    if (string.IsNullOrEmpty(listadoHoteles))
                                        u2 = BillingController.GetRadiusUser(_user, hotel.IdHotel);
                                    else
                                    {
                                        string[] ids = listadoHoteles.Split(',');
                                        foreach (string id in ids)
                                        {
                                            u2 = BillingController.GetRadiusUser(_user, Int32.Parse(id));
                                            if (u2.Enabled)
                                                break;
                                        }
                                    }


                                } while (u2.Enabled);

                                username = _user.ToUpper();
                            }

                            Users user = new Users();


                            user.IdHotel = hotel.IdHotel;
                            user.IdRoom = room.IdRoom;
                            user.IdBillingType = billingType.IdBillingType;
                            user.BWDown = billingType.BWDown;
                            user.BWUp = billingType.BWUp;
                            user.IdLocation = location.IdLocation;

                            user.ValidSince = string.IsNullOrEmpty(start) ? DateTime.Now.ToUniversalTime() : DateTime.Parse(start).AddSeconds(-siteGMT.gmtoffset);
                            user.ValidTill = string.IsNullOrEmpty(end) ? user.ValidSince.AddHours(billingType.ValidTill) : DateTime.Parse(end).AddSeconds(-siteGMT.gmtoffset);

                            //CALCULAMOS EL TIMECREDIT PARA RECORTARLO EN TAL CASO
                            int credit = billingType.TimeCredit / billingType.MaxDevices; //Calculamos el tiempo por dispositivo
                            if (user.ValidTill < user.ValidSince.AddSeconds(credit))
                            {
                                TimeSpan diff = user.ValidTill - user.ValidSince;
                                user.TimeCredit = billingType.MaxDevices * (int)diff.TotalSeconds;
                            }
                            else
                                user.TimeCredit = billingType.TimeCredit;

                            user.MaxDevices = billingType.MaxDevices;
                            user.Priority = billingType.IdPriority;
                            user.VolumeDown = billingType.VolumeDown;
                            user.VolumeUp = billingType.VolumeUp;
                            user.Enabled = true;
                            user.Name = username.ToUpper();
                            user.Password = username.ToUpper();
                            user.CHKO = true;
                            user.BuyingFrom = 0;
                            user.BuyingCallerID = "NOT APPLICABLE";
                            user.Filter_Id = billingType.Filter_Id;
                            user.Comment = comment;
                            user.IdFDSUser = fdsUser.IdUser;

                            BillingController.SaveUser(user);

                            //SE AÑADE A LA TABLA DE BILLING SI ES PREMIUM O SI ES CUSTOM Y PRECIO > 0 O SI ES LOGIN Y PRECIO > 0 O SI ES VOUCHER Y PRECIO > 0
                            if (billingType.IdBillingModule.Equals(2) || billingType.Price > 0)
                            {
                                Billings bill = new Billings();

                                bill.IdHotel = hotel.IdHotel;
                                bill.IdBillingType = billingType.IdBillingType;
                                bill.IdRoom = room.IdRoom;
                                bill.Amount = billingType.Price;
                                bill.BillingDate = DateTime.Now.ToUniversalTime();
                                bill.BillingCharge = bill.BillingDate;
                                bill.Flag = 3;
                                bill.IdLocation = location.IdLocation;
                                bill.IdUser = user.IdUser;
                                bill.IdFDSUser = fdsUser.IdUser;
                                if (billingType.IdBillingModule.Equals(2) || billingType.IdBillingModule.Equals(4))
                                    bill.Billable = true;

                                BillingController.SaveBilling(bill);
                            }

                            u.AccessType = hotel.Name;
                            u.UserName = username.ToUpper();
                            u.Password = username.ToUpper();
                            u.IdUser = hotel.IdHotel.ToString();
                            u.ValidSince = user.ValidSince.AddSeconds(siteGMT.gmtoffset).ToString();
                            u.ValidTill = user.ValidTill.AddSeconds(siteGMT.gmtoffset).ToString();
                            u.MaxDevices = user.MaxDevices.ToString();
                            u.BWDown = user.BWDown.ToString();
                            u.BWUp = user.BWUp.ToString();
                            u.VolumeDown = (user.VolumeDown / 1048576).ToString(); //CONVERTIMOS LOS BYTES EN GB
                            u.VolumeUp = (user.VolumeUp / 1048576).ToString();

                            TimeSpan t = TimeSpan.FromSeconds(user.TimeCredit / user.MaxDevices);
                            string time = string.Empty;
                            if (user.TimeCredit >= 86400)
                                time = string.Format("{0}d: {1:D2}h: {2:D2} m: {3:D2}s", t.Days, t.Hours, t.Minutes, t.Seconds);
                            else
                                time = string.Format("{0:D2}h: {1:D2}m: {2:D2}s", t.Hours, t.Minutes, t.Seconds);
                            u.TimeCredit = time;

                            if (util.ValidateEmail(mail))
                                util.sendMail(hotel.IdHotel, billingType.IdBillingType, mail, username.ToUpper(), username.ToUpper());
                        }
                        else
                        {

                            u.IdUser = "0";
                            //NOMBRE DE USUARIO NO DISPONIBLE
                        }

                        response.list.Add(u);

                        #endregion
                    }
                    else //TICKETS MASIVOS
                    {
                        for (int i = 0; i < numeroTickets; i++)
                        {
                            string user = string.Empty;
                            Users u2 = null;
                            do
                            {
                                user = string.Format("{0}{1}", prefixUserName, util.createRandomUserNumber(longUserName));
                                if (string.IsNullOrEmpty(listadoHoteles))
                                    u2 = BillingController.GetRadiusUser(user, hotel.IdHotel);
                                else
                                {
                                    string[] ids = listadoHoteles.Split(',');
                                    foreach (string id in ids)
                                    {
                                        u2 = BillingController.GetRadiusUser(user, Int32.Parse(id));
                                        if (u2.Enabled)
                                            break;
                                    }
                                }
                            } while (u2.Enabled);

                            Users randUser = new Users();
                            randUser.Name = user;
                            randUser.Password = user;

                            randUser.IdHotel = hotel.IdHotel;
                            randUser.IdRoom = room.IdRoom;
                            randUser.IdBillingType = billingType.IdBillingType;
                            randUser.BWDown = billingType.BWDown;
                            randUser.BWUp = billingType.BWUp;

                            randUser.ValidSince = string.IsNullOrEmpty(start) ? DateTime.Now.ToUniversalTime() : DateTime.Parse(start).AddSeconds(-siteGMT.gmtoffset);
                            randUser.ValidTill = string.IsNullOrEmpty(end) ? randUser.ValidSince.AddHours(billingType.ValidTill) : DateTime.Parse(end).AddSeconds(-siteGMT.gmtoffset);
                            randUser.IdLocation = location.IdLocation;

                            int credit = billingType.TimeCredit / billingType.MaxDevices; //Calculamos el tiempo por dispositivo
                            if (randUser.ValidTill < randUser.ValidSince.AddSeconds(credit))
                            {
                                TimeSpan diff = randUser.ValidTill - randUser.ValidSince;
                                randUser.TimeCredit = billingType.MaxDevices * (int)diff.TotalSeconds;
                            }
                            else
                                randUser.TimeCredit = billingType.TimeCredit;

                            randUser.MaxDevices = billingType.MaxDevices;
                            randUser.Priority = billingType.IdPriority;
                            randUser.VolumeDown = billingType.VolumeDown;
                            randUser.VolumeUp = billingType.VolumeUp;
                            randUser.Enabled = true;
                            randUser.CHKO = true;
                            randUser.BuyingFrom = 1;
                            randUser.BuyingCallerID = "00-00-00-00-00-00";
                            randUser.Filter_Id = billingType.Filter_Id;
                            randUser.Comment = comment;
                            randUser.IdFDSUser = fdsUser.IdUser;

                            BillingController.SaveUser(randUser);

                            //SE AÑADE A LA TABLA DE BILLING SI ES PREMIUM O SI ES CUSTOM Y PRECIO > 0 O SI ES LOGIN Y PRECIO > 0 O SI ES VOUCHER Y PRECIO > 0
                            if (billingType.IdBillingModule.Equals(2) || billingType.Price > 0)
                            {
                                Billings bill = new Billings();

                                bill.IdHotel = hotel.IdHotel;
                                bill.IdBillingType = billingType.IdBillingType;
                                bill.IdRoom = room.IdRoom;
                                bill.Amount = billingType.Price;
                                bill.BillingDate = DateTime.Now.ToUniversalTime();
                                bill.BillingCharge = bill.BillingDate;
                                bill.Flag = 3;
                                bill.IdLocation = location.IdLocation;
                                bill.IdUser = randUser.IdUser;
                                bill.IdFDSUser = fdsUser.IdUser;
                                if (billingType.IdBillingModule.Equals(2) || billingType.IdBillingModule.Equals(4))
                                    bill.Billable = true;

                                BillingController.SaveBilling(bill);
                            }

                            UserManagementResult ur = new UserManagementResult();
                            ur.UserName = randUser.Name.ToUpper();
                            ur.Password = randUser.Password.ToUpper();
                            ur.IdUser = hotel.IdHotel.ToString();
                            ur.ValidSince = randUser.ValidSince.AddSeconds(siteGMT.gmtoffset).ToString();
                            ur.ValidTill = randUser.ValidTill.AddSeconds(siteGMT.gmtoffset).ToString();
                            ur.MaxDevices = randUser.MaxDevices.ToString();
                            ur.BWDown = randUser.BWDown.ToString();
                            ur.BWUp = randUser.BWUp.ToString();
                            ur.VolumeDown = (randUser.VolumeDown / 1048576).ToString(); //CONVERTIMOS LOS BYTES EN GB
                            ur.VolumeUp = (randUser.VolumeUp / 1048576).ToString();


                            TimeSpan t = TimeSpan.FromSeconds(randUser.TimeCredit / randUser.MaxDevices);
                            string time = string.Empty;
                            if (randUser.TimeCredit >= 86400)
                                time = string.Format("{0}d: {1:D2}h: {2:D2} m: {3:D2}s", t.Days, t.Hours, t.Minutes, t.Seconds);
                            else
                                time = string.Format("{0:D2}h: {1:D2}m: {2:D2}s", t.Hours, t.Minutes, t.Seconds);
                            ur.TimeCredit = time;
                            ur.AccessType = billingType.Description;

                            response.list.Add(ur);
                        }

                        response.pageNumber = hotel.IdHotel;
                    }
                }
            }
            catch (Exception ex)
            {
                CaptivePortalLogInternal internalLog = new CaptivePortalLogInternal();
                internalLog.NSEId = "";
                internalLog.CallerID = "";
                internalLog.Date = DateTime.Now;
                internalLog.UserName = "";
                internalLog.Password = "";
                internalLog.Page = "Create Voucher";
                internalLog.IdLocation = 0;
                internalLog.NomadixResponse = "ERROR NO CONTEMPLADO - " + ex.Message;
                internalLog.RadiusResponse = "OK";

                BillingController.SaveCaptivePortalLogInternal(internalLog);
                //json = "{\"code\":\"ERROR\",\"message\":\"Se ha producido un error en el envio. Inténtelo más tarde.\"}";
            }


            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private void ExportVoucher(HttpContext context, Dictionary<string, string> sData)
        {
            string json = string.Empty;
            ListUserManagementResult response = new ListUserManagementResult();
            DataTable table = new DataTable();
            table.Columns.Add("IdUser");
            table.Columns.Add("UserName");
            table.Columns.Add("ValidSince");
            table.Columns.Add("ValidTill");
            table.Columns.Add("TimeCredit");
            table.Columns.Add("MaxDevices");
            table.Columns.Add("BWDown");
            table.Columns.Add("BWUp");
            table.Columns.Add("VolumeDown");
            table.Columns.Add("VolumeUp");
            table.Columns.Add("AccessType");
            

            try
            {
                //OBTENEMOS EL TIPO DE USUARIOS
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    Hotels hotel = null;
                    Locations2 location = null;
                    SitesGMT siteGMT = GetSiteGMt(out location, out hotel);
                    //PARAMETRO OBLIGATORIOS

                    string idroom = sData["idroom"];
                    string idbillingtype = sData["idbillingtype"];
                    string numero = sData["numero"];
                    string username = sData["username"];
                    string mail = sData["mail"];
                    string start = sData["start"];
                    string end = sData["end"];
                    string comment = sData["comment"];

                    Rooms room = BillingController.GetRoom(Int32.Parse(idroom));
                    BillingTypes billingType = BillingController.ObtainBillingType(Int32.Parse(idbillingtype));
                    if (location == null)
                        location = BillingController.GetLocation(billingType.IdLocation);

                    List<ParametrosConfiguracion> parametros = BillingController.GetParametrosConfiguracion(hotel.IdHotel);
                    int numeroTickets = Int32.Parse(numero);
                    if (numeroTickets > 5000)
                        numeroTickets = 5000;
                    int longUserName = 3;
                    string prefixUserName = string.Empty;

                    foreach (ParametrosConfiguracion obj in parametros)
                    {
                        switch (obj.Key.ToUpper())
                        {
                            case "LONGVOUCHERUSER": longUserName = Int32.Parse(obj.value); break;
                            case "VOUCHERPREFIX": prefixUserName = obj.value; break;
                        }
                    }

                    if (longUserName < 3)
                        longUserName = 3;

                    response.list = new List<UserManagementResult>();

                    FDSGroups GroupRealmShare = BillingController.GetGroupShareRealm(hotel.IdHotel);
                    string listadoHoteles = string.Empty;
                    if (GroupRealmShare.IdGroup != 0)
                        listadoHoteles = ListadoHoteles(GroupRealmShare);

                    //TICKET SIMPLE
                    if (numeroTickets.Equals(1))
                    {
                        #region generación de ticket unitario

                        UserManagementResult u = new UserManagementResult();
                        Users existuser = null;
                        if (!string.IsNullOrEmpty(username))
                        {
                            if (string.IsNullOrEmpty(listadoHoteles))
                                existuser = BillingController.GetRadiusUser(username.ToUpper(), hotel.IdHotel);
                            else
                            {
                                string[] ids = listadoHoteles.Split(',');
                                foreach (string id in ids)
                                {
                                    existuser = BillingController.GetRadiusUser(username.ToUpper(), Int32.Parse(id));
                                    if (!existuser.IdUser.Equals(0) || existuser.Enabled)
                                        break;
                                }
                            }
                        }
                        else
                            existuser = new Users();


                        if (existuser.IdUser.Equals(0) || !existuser.Enabled)
                        {
                            Users u2 = null;
                            if (string.IsNullOrEmpty(username))
                            {
                                string _user = string.Empty;
                                do
                                {
                                    _user = string.Format("{0}{1}", prefixUserName, util.createRandomUserNumber(longUserName));
                                    if (string.IsNullOrEmpty(listadoHoteles))
                                        u2 = BillingController.GetRadiusUser(_user, hotel.IdHotel);
                                    else
                                    {
                                        string[] ids = listadoHoteles.Split(',');
                                        foreach (string id in ids)
                                        {
                                            u2 = BillingController.GetRadiusUser(_user, Int32.Parse(id));
                                            if (u2.Enabled)
                                                break;
                                        }
                                    }


                                } while (u2.Enabled);

                                username = _user.ToUpper();
                            }

                            Users user = new Users();
                           

                            user.IdHotel = hotel.IdHotel;
                            user.IdRoom = room.IdRoom;
                            user.IdBillingType = billingType.IdBillingType;
                            user.BWDown = billingType.BWDown;
                            user.BWUp = billingType.BWUp;
                            user.IdLocation = location.IdLocation;

                            user.ValidSince = string.IsNullOrEmpty(start) ? DateTime.Now.ToUniversalTime() : DateTime.Parse(start).AddSeconds(-siteGMT.gmtoffset);
                            user.ValidTill = string.IsNullOrEmpty(end) ? user.ValidSince.AddHours(billingType.ValidTill) : DateTime.Parse(end).AddSeconds(-siteGMT.gmtoffset);

                            //CALCULAMOS EL TIMECREDIT PARA RECORTARLO EN TAL CASO
                            int credit = billingType.TimeCredit / billingType.MaxDevices; //Calculamos el tiempo por dispositivo
                            if (user.ValidTill < user.ValidSince.AddSeconds(credit))
                            {
                                TimeSpan diff = user.ValidTill - user.ValidSince;
                                user.TimeCredit = billingType.MaxDevices * (int)diff.TotalSeconds;
                            }
                            else
                                user.TimeCredit = billingType.TimeCredit;

                            user.MaxDevices = billingType.MaxDevices;
                            user.Priority = billingType.IdPriority;
                            user.VolumeDown = billingType.VolumeDown;
                            user.VolumeUp = billingType.VolumeUp;
                            user.Enabled = true;
                            user.Name = username.ToUpper();
                            user.Password = username.ToUpper();
                            user.CHKO = true;
                            user.BuyingFrom = 0;
                            user.BuyingCallerID = "NOT APPLICABLE";
                            user.Filter_Id = billingType.Filter_Id;
                            user.Comment = comment;
                            user.IdFDSUser = fdsUser.IdUser;

                            BillingController.SaveUser(user);

                            //SE AÑADE A LA TABLA DE BILLING SI ES PREMIUM O SI ES CUSTOM Y PRECIO > 0 O SI ES LOGIN Y PRECIO > 0 O SI ES VOUCHER Y PRECIO > 0
                            if (billingType.IdBillingModule.Equals(2) || billingType.Price > 0)
                            {
                                Billings bill = new Billings();

                                bill.IdHotel = hotel.IdHotel;
                                bill.IdBillingType = billingType.IdBillingType;
                                bill.IdRoom = room.IdRoom;
                                bill.Amount = billingType.Price;
                                bill.BillingDate = DateTime.Now.ToUniversalTime();
                                bill.BillingCharge = bill.BillingDate;
                                bill.Flag = 3;
                                bill.IdLocation = location.IdLocation;
                                bill.IdUser = user.IdUser;
                                bill.IdFDSUser = fdsUser.IdUser;
                                if (billingType.IdBillingModule.Equals(2) || billingType.IdBillingModule.Equals(4))
                                    bill.Billable = true;

                                BillingController.SaveBilling(bill);
                            }

                            u.AccessType = hotel.Name;
                            u.UserName = username.ToUpper();
                            u.Password = username.ToUpper();
                            u.IdUser = hotel.IdHotel.ToString();
                            u.ValidSince = user.ValidSince.AddSeconds(siteGMT.gmtoffset).ToString();
                            u.ValidTill = user.ValidTill.AddSeconds(siteGMT.gmtoffset).ToString();
                            u.MaxDevices = user.MaxDevices.ToString();
                            u.BWDown = user.BWDown.ToString();
                            u.BWUp = user.BWUp.ToString();
                            u.VolumeDown = (user.VolumeDown / 1048576).ToString(); //CONVERTIMOS LOS BYTES EN GB
                            u.VolumeUp = (user.VolumeUp / 1048576).ToString();

                            TimeSpan t = TimeSpan.FromSeconds(user.TimeCredit / user.MaxDevices);
                            string time = string.Empty;
                            if (user.TimeCredit >= 86400)
                                time = string.Format("{0}d: {1:D2}h: {2:D2} m: {3:D2}s", t.Days, t.Hours, t.Minutes, t.Seconds);
                            else
                                time = string.Format("{0:D2}h: {1:D2}m: {2:D2}s", t.Hours, t.Minutes, t.Seconds);
                            u.TimeCredit = time;

                            DataRow row = table.NewRow();
                            row["IdUser"] = user.IdUser;
                            row["UserName"] = username.ToUpper();
                            row["ValidSince"] = user.ValidSince.AddSeconds(siteGMT.gmtoffset).ToString();
                            row["ValidTill"] = user.ValidTill.AddSeconds(siteGMT.gmtoffset).ToString();
                            row["MaxDevices"] = user.MaxDevices;
                            row["BWUp"] = user.BWUp;
                            row["BWDown"] = user.BWDown;
                            row["VolumeUp"] = (user.VolumeUp / 1048576).ToString();
                            row["VolumeDown"] = (user.VolumeDown / 1048576).ToString();
                            row["AccessType"] = billingType.Description;
                            row["TimeCredit"] = time;

                            table.Rows.Add(row);
                        }
                        else
                        {
                            u.IdUser = "0";
                            //NOMBRE DE USUARIO NO DISPONIBLE
                        }

                        response.list.Add(u);

                        #endregion
                    }
                    else //TICKETS MASIVOS
                    {

                        #region generación de tickets masiva
                        for (int i = 0; i < numeroTickets; i++)
                        {
                            string user = string.Empty;
                            Users u2 = null;
                            do
                            {
                                user = string.Format("{0}{1}", prefixUserName, util.createRandomUserNumber(longUserName));
                                if (string.IsNullOrEmpty(listadoHoteles))
                                    u2 = BillingController.GetRadiusUser(user, hotel.IdHotel);
                                else
                                {
                                    string[] ids = listadoHoteles.Split(',');
                                    foreach (string id in ids)
                                    {
                                        u2 = BillingController.GetRadiusUser(user, Int32.Parse(id));
                                        if (u2.Enabled)
                                            break;
                                    }
                                }
                            } while (u2.Enabled);

                            Users randUser = new Users();
                            randUser.Name = user;
                            randUser.Password = user;

                            randUser.IdHotel = hotel.IdHotel;
                            randUser.IdRoom = room.IdRoom;
                            randUser.IdBillingType = billingType.IdBillingType;
                            randUser.BWDown = billingType.BWDown;
                            randUser.BWUp = billingType.BWUp;

                            randUser.ValidSince = string.IsNullOrEmpty(start) ? DateTime.Now.ToUniversalTime() : DateTime.Parse(start).AddSeconds(-siteGMT.gmtoffset);
                            randUser.ValidTill = string.IsNullOrEmpty(end) ? randUser.ValidSince.AddHours(billingType.ValidTill) : DateTime.Parse(end).AddSeconds(-siteGMT.gmtoffset);
                            randUser.IdLocation = location.IdLocation;

                            int credit = billingType.TimeCredit / billingType.MaxDevices; //Calculamos el tiempo por dispositivo
                            if (randUser.ValidTill < randUser.ValidSince.AddSeconds(credit))
                            {
                                TimeSpan diff = randUser.ValidTill - randUser.ValidSince;
                                randUser.TimeCredit = billingType.MaxDevices * (int)diff.TotalSeconds;
                            }
                            else
                                randUser.TimeCredit = billingType.TimeCredit;

                            randUser.MaxDevices = billingType.MaxDevices;
                            randUser.Priority = billingType.IdPriority;
                            randUser.VolumeDown = billingType.VolumeDown;
                            randUser.VolumeUp = billingType.VolumeUp;
                            randUser.Enabled = true;
                            randUser.CHKO = true;
                            randUser.BuyingFrom = 1;
                            randUser.BuyingCallerID = "00-00-00-00-00-00";
                            randUser.Filter_Id = billingType.Filter_Id;
                            randUser.Comment = comment;
                            randUser.IdFDSUser = fdsUser.IdUser;
                            BillingController.SaveUser(randUser);

                            //SE AÑADE A LA TABLA DE BILLING SI ES PREMIUM O SI ES CUSTOM Y PRECIO > 0 O SI ES LOGIN Y PRECIO > 0 O SI ES VOUCHER Y PRECIO > 0
                            if (billingType.IdBillingModule.Equals(2) ||billingType.Price > 0)
                            {
                                Billings bill = new Billings();

                                bill.IdHotel = hotel.IdHotel;
                                bill.IdBillingType = billingType.IdBillingType;
                                bill.IdRoom = room.IdRoom;
                                bill.Amount = billingType.Price;
                                bill.BillingDate = DateTime.Now.ToUniversalTime();
                                bill.BillingCharge = bill.BillingDate;
                                bill.Flag = 3;
                                bill.IdLocation = location.IdLocation;
                                bill.IdUser = randUser.IdUser;
                                bill.IdFDSUser = fdsUser.IdUser;
                                if (billingType.IdBillingModule.Equals(2) || billingType.IdBillingModule.Equals(4))
                                    bill.Billable = true;

                                BillingController.SaveBilling(bill);
                            }

                            UserManagementResult ur = new UserManagementResult();
                            ur.UserName = randUser.Name.ToUpper();
                            ur.Password = randUser.Password.ToUpper();
                            ur.IdUser = hotel.IdHotel.ToString();
                            ur.ValidSince = randUser.ValidSince.AddSeconds(siteGMT.gmtoffset).ToString();
                            ur.ValidTill = randUser.ValidTill.AddSeconds(siteGMT.gmtoffset).ToString();
                            ur.MaxDevices = randUser.MaxDevices.ToString();
                            ur.BWDown = randUser.BWDown.ToString();
                            ur.BWUp = randUser.BWUp.ToString();
                            ur.VolumeDown = (randUser.VolumeDown / 1048576).ToString(); //CONVERTIMOS LOS BYTES EN GB
                            ur.VolumeUp = (randUser.VolumeUp / 1048576).ToString();


                            TimeSpan t = TimeSpan.FromSeconds(randUser.TimeCredit / randUser.MaxDevices);
                            string time = string.Empty;
                            if (randUser.TimeCredit >= 86400)
                                time = string.Format("{0}d: {1:D2}h: {2:D2} m: {3:D2}s", t.Days, t.Hours, t.Minutes, t.Seconds);
                            else
                                time = string.Format("{0:D2}h: {1:D2}m: {2:D2}s", t.Hours, t.Minutes, t.Seconds);
                            ur.TimeCredit = time;
                            ur.AccessType = billingType.Description;

                            DataRow row = table.NewRow();
                            row["IdUser"] = randUser.IdUser;
                            row["UserName"] = user.ToUpper();
                            row["ValidSince"] = randUser.ValidSince.AddSeconds(siteGMT.gmtoffset).ToString();
                            row["ValidTill"] = randUser.ValidTill.AddSeconds(siteGMT.gmtoffset).ToString();
                            row["MaxDevices"] = randUser.MaxDevices;
                            row["BWUp"] = randUser.BWUp;
                            row["BWDown"] = randUser.BWDown;
                            row["VolumeUp"] = (randUser.VolumeUp / 1048576).ToString();
                            row["VolumeDown"] = (randUser.VolumeDown / 1048576).ToString();
                            row["AccessType"] = billingType.Description;
                            row["TimeCredit"] = time;

                            table.Rows.Add(row);

                            response.list.Add(ur);
                        }

                        #endregion  

                        response.pageNumber = hotel.IdHotel;
                    }

                    string folder_name = string.Empty;
                    if (HttpContext.Current.Request.Cookies["site"] != null)
                        folder_name = HttpContext.Current.Request.Cookies["site"].Value;
                    else if (HttpContext.Current.Request.Cookies["location"] != null)
                    {
                        folder_name = location.IdHotel.ToString();
                    }
                    else
                        folder_name = string.Format("group_{0}", HttpContext.Current.Request.Cookies["group"].Value);

                    if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Format("../resources/docs/{0}", folder_name))))
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Format("../resources/docs/{0}", folder_name)));

                    string filename = HttpContext.Current.Server.MapPath(string.Format("../resources/docs/{0}/vouchers.xlsx", folder_name));
                    string url = string.Format("/resources/docs/{0}/vouchers.xlsx", folder_name);

                    var workbook = new XLWorkbook();
                    var worksheet = workbook.Worksheets.Add("Hoja1");

                    worksheet.Cell("A1").Value = "Identificador";
                    worksheet.Cell("B1").Value = "Nombre de usuario";
                    worksheet.Cell("C1").Value = "Inicio validez";
                    worksheet.Cell("D1").Value = "Fin de validez";
                    worksheet.Cell("E1").Value = "Crédito de tiempo";
                    worksheet.Cell("F1").Value = "Máximo de Dispositivos";
                    worksheet.Cell("G1").Value = "Volumen subida";
                    worksheet.Cell("H1").Value = "Volumen descarga";
                    worksheet.Cell("I1").Value = "Ancho de banda subida";
                    worksheet.Cell("J1").Value = "Ancho de banda descarga";
                    worksheet.Cell("K1").Value = "Tipo de Acceso";

                    int prow = 2;
                    foreach (DataRow row in table.Rows)
                    {
                        worksheet.Cell(string.Format("A{0}", prow)).Value = row["IdUser"].ToString();
                        worksheet.Cell(string.Format("B{0}", prow)).Value = row["UserName"].ToString();
                        worksheet.Cell(string.Format("C{0}", prow)).Value = row["ValidSince"].ToString();
                        worksheet.Cell(string.Format("C{0}", prow)).DataType = XLCellValues.DateTime;
                        worksheet.Cell(string.Format("D{0}", prow)).Value = row["ValidTill"].ToString();
                        worksheet.Cell(string.Format("D{0}", prow)).DataType = XLCellValues.DateTime;
                        worksheet.Cell(string.Format("E{0}", prow)).Value = row["TimeCredit"].ToString();
                        worksheet.Cell(string.Format("F{0}", prow)).Value = row["MaxDevices"].ToString();
                        worksheet.Cell(string.Format("G{0}", prow)).Value = row["VolumeUp"].ToString();
                        worksheet.Cell(string.Format("G{0}", prow)).DataType = XLCellValues.Text;
                        worksheet.Cell(string.Format("H{0}", prow)).Value = row["VolumeDown"].ToString();
                        worksheet.Cell(string.Format("H{0}", prow)).DataType = XLCellValues.Text;
                        worksheet.Cell(string.Format("I{0}", prow)).Value = row["BWUp"].ToString();
                        worksheet.Cell(string.Format("J{0}", prow)).Value = row["BWDown"].ToString();
                        worksheet.Cell(string.Format("K{0}", prow)).Value = row["AccessType"].ToString();

                        prow = prow + 1;
                    }

                    workbook.SaveAs(filename);

                    json = "{\"code\":\"OK\",\"message\":\"" + url + "\"}";
                }
            }
            catch (Exception ex)
            {
                CaptivePortalLogInternal internalLog = new CaptivePortalLogInternal();
                internalLog.NSEId = "";
                internalLog.CallerID = "";
                internalLog.Date = DateTime.Now;
                internalLog.UserName = "";
                internalLog.Password = "";
                internalLog.Page = "Export Voucher";
                internalLog.IdLocation = 0;
                internalLog.NomadixResponse = "ERROR NO CONTEMPLADO - " + ex.Message;
                internalLog.RadiusResponse = "OK";

                BillingController.SaveCaptivePortalLogInternal(internalLog);
                json = "{\"code\":\"ERROR\",\"message\":\"0x9999\"}";
            }


            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();
        }

        private ListCurrenciesResult currencies(HttpContext context)
        {
            ListCurrenciesResult response = new ListCurrenciesResult();
            try
            {
                //OBTENEMOS EL TIPO DE USUARIOS
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));

                    response.list = new List<CurrencyResult>();

                    List<Currencies2> list = BillingController.SelectCurrencies();
                    foreach(Currencies2 obj in list)
                    {
                        CurrencyResult r = new CurrencyResult();
                        r.Id = obj.Id;
                        r.Code = obj.Code;
                        r.Number = obj.Number;
                        r.Symbol = obj.Symbol;
                        r.Badge = obj.Badge;

                        response.list.Add(r);

                    }
                }
            }
            catch (Exception ex)
            {

            }


            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private ListLanguageResult languages(HttpContext context)
        {
            ListLanguageResult response = new ListLanguageResult();
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    //if (HttpContext.Current.Request.Cookies["site"] != null)
                    //    fdsUser.IdHotel = Int32.Parse(HttpContext.Current.Request.Cookies["site"].Value);

                    List<Languages> list = BillingController.GetLanguages();

                    response.list = new List<LanguageResult>();

                    foreach (Languages l in list.OrderBy(a => a.Name))
                    {
                        LanguageResult r = new LanguageResult();
                        r.IdLanguage = l.IdLanguage;
                        r.Code = l.Code;
                        r.Name = l.Name;

                        response.list.Add(r);
                    }
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private LocationResult Location(HttpContext context)
        {
            LocationResult response = new LocationResult();
            try
            {
                string id = (context.Request["id"] == null ? string.Empty : context.Request["id"].ToString());

                Locations2 obj = BillingController.ObtainLocation(Int32.Parse(id));
                Hotels hotel = BillingController.GetHotel(obj.IdHotel);

                List<ParametrosConfiguracion> parametros = BillingController.GetParametrosConfiguracion(0);
                ParametrosConfiguracion key = (from t in parametros where t.Key.ToUpper().Equals("ENCRYPTIONKEY") select t).FirstOrDefault();

                response.Idlocation = obj.IdLocation;
                response.Value = obj.Value;
                response.Description = obj.Description;
                response.UrlLanding = obj.UrlLanding;
                response.DefaultModule = obj.DefaultModule;
                response.DisclaimerMandatory = obj.Disclaimer;
                response.Default = obj.Default;
                response.Latitude = obj.Latitude;
                response.Longitude = obj.Longitude;
                response.Currency = obj.Currency;
                Currencies2 currency = BillingController.Currency(obj.Currency);
                response.IdCurrency = currency.Id;
                response.CurrencyExchange = obj.ExchangeCurrency;
                response.CurrencySite = hotel.CurrencyCode;
                response.Currencysymbol = currency.Symbol;
                response.GMT = obj.GMT;
                response.LoginModule = obj.LoginModule;
                response.VoucherModule = obj.VoucherModule;
                response.PayAccessModule = obj.PayAccessModule;
                response.FreeAccessModule = obj.FreeAccessModule;
                response.SocialNetworkModule = obj.SocialNetworksModule;
                response.CustomModule = obj.CustomAccessModule;
                response.MandatorySurvey = obj.MandatorySurvey;
                response.DelaySurvey = obj.DelaySurvey;

                string token_decript = string.Format("nt_st={0}|nt_lt={1}", obj.IdHotel, obj.IdLocation);
                response.token = Wifi360.Data.Class.Misc.Encrypt(token_decript, key.value);

                //  response.deviceid = obj.deviceId;

                List<SocialNetworks> socialNetworks = BillingController.GetSocialNetwoks();
                List<LocationsSocialNetworks> lSN = BillingController.GetLocationSocialNetwoks(obj.IdLocation);

                response.redes = new List<SocialNetworkResult>();
                foreach (LocationsSocialNetworks sn in lSN)
                {
                    SocialNetworkResult rs = new SocialNetworkResult();
                    rs.IdSocialNetwork = sn.IdLocationSocialNetwork;
                    SocialNetworks s = (from t in socialNetworks where t.IdSocialNetwork.Equals(sn.IdSocialNetwork) select t).FirstOrDefault();
                    rs.Name = s.Name;
                    rs.Active = sn.Active;

                    response.redes.Add(rs);
                }

                List<Languages> languajes = BillingController.GetLanguages();
                List<Surveys2> surveys = BillingController.GetSurveyLocation(obj.IdLocation);
                response.encuesta = new List<SurveyResult>();
                foreach (Surveys2 s in surveys)
                {
                    SurveyResult r = new SurveyResult();
                    r.IdHotel = s.IdHotel;
                    r.IdLanguage = s.IdLanguage;
                    Languages lang = (from t in languajes where t.IdLanguage.Equals(s.IdLanguage) select t).FirstOrDefault();
                    if (lang != null)
                        r.Language = lang.Name;
                    r.IdLocation = s.IdLocation;
                    r.IdSurvey = s.IdSurvey;
                    r.Order = s.Order;
                    r.Question = s.Question;

                    string[] cadenas = s.Type.Split('-');
                    r.Type = cadenas[0].Trim();
                    if (cadenas.Length.Equals(1))
                        r.Values = string.Empty;
                    else
                        r.Values = cadenas[1];

                    response.encuesta.Add(r);
                }

            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private ListLocationResult Locations(HttpContext context)
        {
            ListLocationResult response = new ListLocationResult();
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    string idsite = (context.Request["idsite"] == null ? string.Empty : context.Request["idsite"].ToString());

                    Hotels hotel = null;
                    Locations2 location = null;
                    List<Locations2> list = null;
                    if (string.IsNullOrEmpty(idsite) || idsite.ToUpper().Equals("UNDEFINED"))
                    {
                        if (HttpContext.Current.Request.Cookies["location"] != null)
                        {
                            location = BillingController.GetLocation(Int32.Parse(HttpContext.Current.Request.Cookies["location"].Value));
                            hotel = BillingController.GetHotel(location.IdHotel);
                            list = new List<Locations2>();
                            list.Add(location);
                        }
                        else
                        {
                            hotel = BillingController.GetHotel(Int32.Parse(HttpContext.Current.Request.Cookies["site"].Value));
                            list = BillingController.GetLocations(Int32.Parse(HttpContext.Current.Request.Cookies["site"].Value));
                        }
                    }
                    else
                    {
                        hotel = BillingController.GetHotel(int.Parse(idsite));
                        list = BillingController.GetLocations(hotel.IdHotel);
                    }

                    response.list = new List<LocationResult>();

                    foreach (Locations2 b in list.OrderBy(a => a.Value))
                    {
                        LocationResult br = new LocationResult();                        
                        br.Idlocation = b.IdLocation;
                        br.Value = b.Value;
                        br.Description = b.Description;
                        br.UrlLanding = b.UrlLanding;
                        br.Default = b.Default;
                        br.Latitude = b.Latitude;
                        br.Longitude = b.Longitude;
                        br.Currency = b.Currency;
                        Currencies2 currency = BillingController.Currency(b.Currency);
                        br.IdCurrency = currency.Id;
                        br.CurrencyExchange = b.ExchangeCurrency;
                        br.CurrencySite = hotel.CurrencyCode;
                        br.GMT = b.GMT;

                        if (b.Default)
                            response.Default = b.IdLocation;

                        response.list.Add(br);
                    }

                    response.currency = hotel.CurrencyCode;
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;


        }

        private ListLocationResult LocationsNoAllZones(HttpContext context)
        {
            ListLocationResult response = new ListLocationResult();
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                   FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                   List<Locations2> list = null;
                   if (HttpContext.Current.Request.Cookies["location"] != null)
                   {
                       Locations2 location = BillingController.GetLocation(Int32.Parse(HttpContext.Current.Request.Cookies["location"].Value));
                       list = new List<Data.Model.Locations2>();
                       list.Add(location);
                   }
                   else if (HttpContext.Current.Request.Cookies["site"] != null)
                       list = BillingController.GetLocations(Int32.Parse(HttpContext.Current.Request.Cookies["site"].Value));

                   else if (HttpContext.Current.Request.Cookies["group"] != null)
                       list = BillingController.GetLocations(Int32.Parse(HttpContext.Current.Request.Cookies["site"].Value));

                    response.list = new List<LocationResult>();


                    foreach (Locations2 b in list.OrderBy(a => a.IdLocation))
                    {
                        if (!b.Description.ToUpper().Equals("ALL ZONES"))
                        {
                            LocationResult br = new LocationResult();
                            br.Idlocation = b.IdLocation;
                            br.Description = b.Description;
                            br.Value = b.Value;
                            br.UrlLanding = b.UrlLanding;

                            if (b.Default)
                                response.Default = b.IdLocation;

                            response.list.Add(br);
                        }
                    }
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;


        }

        private LocationTextResult LocationText(HttpContext context)
        {
            LocationTextResult response = new LocationTextResult();
            try
            {
                string id = (context.Request["id"] == null ? string.Empty : context.Request["id"].ToString());

                LocationsTexts obj = BillingController.GetLocationText(Int32.Parse(id));

                response.idlocationText = obj.IdLocationText;
                response.idlocation = obj.IdLocation;
                response.idlanguage = obj.IdLanguage;
                response.text = obj.Text;
                response.language = BillingController.GetLanguage(obj.IdLanguage).Name;
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private void LocationModuleTextsave(HttpContext context, Dictionary<string, string> sData)
        {
            string json = string.Empty;
            try
            {
                string id = sData["id"];
                string text = sData["text"];

                LocationModuleText obj = BillingController.GetLocationModuleText(Int32.Parse(id));

                obj.Text = text;

                BillingController.SaveLocationModuleText(obj);

                json = "{\"code\":\"OK\",\"message\":\"0x0027\"}";
            }
            catch (Exception ex)
            {
                json = "{\"code\":\"ERROR\",\"message\":\"0x9999\"}";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();
        }

        private void LocationTextsave(HttpContext context, Dictionary<string, string> sData)
        {
            string json = string.Empty;
            try
            {
                string id = sData["id"];
                string text = sData["text"];

                LocationsTexts obj = BillingController.GetLocationText(Int32.Parse(id));

                obj.Text = text;

                BillingController.SaveLocationText(obj);

                json = "{\"code\":\"OK\",\"message\":\"0x0027\"}";
            }
            catch (Exception ex)
            {
                json = "{\"code\":\"ERROR\",\"message\":\"0x9999\"}";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();
        }

        private ListLocationTextResult LocationsText(HttpContext context)
        {
            ListLocationTextResult response = new ListLocationTextResult();
            try
            {
                string id = (context.Request["id"] == null ? string.Empty : context.Request["id"].ToString());

                List<LocationsTexts> list = BillingController.GetLocationsText(Int32.Parse(id));

                response.list = new List<LocationTextResult>();

                foreach (LocationsTexts b in list)
                {
                    LocationTextResult br = new LocationTextResult();
                    br.idlocation = b.IdLocation;
                    br.idlocationText = b.IdLocationText;
                    br.idlanguage = b.IdLanguage;
                    br.text = b.Text;
                    br.language = BillingController.GetLanguage(b.IdLanguage).Name;

                    response.list.Add(br);
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private LocationModuleTextResult LocationModuleText(HttpContext context)
        {
            LocationModuleTextResult response = new LocationModuleTextResult();
            try
            {
                string id = (context.Request["id"] == null ? string.Empty : context.Request["id"].ToString());

                LocationModuleText obj = BillingController.GetLocationModuleText(Int32.Parse(id));

                response.text = obj.Text;
                response.idbillingModule = obj.IdBillingModule;
                response.idlanguage = obj.IdLanguage;
                response.language = BillingController.GetLanguage(obj.IdLanguage).Name;
                response.billingModule = BillingController.ObtainBillingModule(obj.IdBillingModule).Name;
                response.idlocationText = obj.IdLocationModuleText;
                response.idlocation = obj.IdLocation;
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private ListLocationModuleTextResult LocationsModuleText(HttpContext context)
        {
            ListLocationModuleTextResult response = new ListLocationModuleTextResult();
            try
            {
                string id = (context.Request["id"] == null ? string.Empty : context.Request["id"].ToString());

                List<LocationModuleText> list = BillingController.GetLocationModuleTexts(Int32.Parse(id));

                response.list = new List<LocationModuleTextResult>();

                foreach (LocationModuleText b in list)
                {
                    LocationModuleTextResult br = new LocationModuleTextResult();
                    br.idlocation = b.IdLocation;
                    br.idlocationText = b.IdLocationModuleText;
                    br.idlanguage = b.IdLanguage;
                    br.idbillingModule = b.IdBillingModule;
                    br.text = b.Text;
                    br.language = BillingController.GetLanguage(b.IdLanguage).Name;
                    br.billingModule = BillingController.ObtainBillingModule(b.IdBillingModule).Name;

                    response.list.Add(br);
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private void CheckCurrencies()
        {
            //UPDATE TABLE CURRENCIE (LAS 24H) FROM API (MEJORA20190521)
            //OBTAIN AFECTED LOCATIONS
            Hotels hotel = null;
            Locations2 location = null;
            FDSGroups group = null;
            SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group);
            string listadosHoteles = string.Empty;
            if (group != null)
                listadosHoteles = ListadoHoteles(group);
            else
                if (hotel != null)
                    listadosHoteles = hotel.IdHotel.ToString();
                else
                    listadosHoteles = location.IdHotel.ToString();

            //CHECK UPDATE 
            bool update_is_necesary = false;
            List<Exchanges> check_tipos_de_moneda = BillingController.GetTiposDeMoneda(listadosHoteles);
            foreach (Exchanges check_item in check_tipos_de_moneda)
            {
                Currencies2 valor = new Currencies2();
                valor = BillingController.Currency(check_item.Currency);
                if (valor.LastUpdate != null)
                {
                    TimeSpan check_tiempo_temp = DateTime.Now.ToUniversalTime() - valor.LastUpdate.Value;
                    if (check_tiempo_temp.TotalHours >= 24)
                        update_is_necesary = true;
                }else
                {
                    update_is_necesary = true;
                }
            }

            if (update_is_necesary) { 
                //EXCHANGE ONLINE APY CURRENCIES
                List<Exchanges> tipos_de_moneda = BillingController.GetTiposDeMoneda(listadosHoteles);
                GetCurrenciesOnlineExchange(ref tipos_de_moneda, "EUR"); //Actualmente la api sólo nos permite usar como base el EUR
                foreach (Exchanges item in tipos_de_moneda)
                {
                    Currencies2 valor = new Currencies2();
                    valor = BillingController.Currency(item.Currency);

                    if (valor.LastUpdate != null)
                    {
                        TimeSpan tiempo_temp = DateTime.Now.ToUniversalTime() - valor.LastUpdate.Value;
                        if ((tiempo_temp.TotalHours >= 24) || (valor.Exchange == null))
                        {
                            //UPDATE CURRENCIE
                            valor.Exchange = (1/item.Exchange);
                            valor.LastUpdate = DateTime.Now.ToUniversalTime();

                            //SAVE CURRENCIE
                            BillingController.SaveCurrencie(valor);
                        }
                    }
                    else
                    {
                        valor.Exchange = (1/item.Exchange);
                        valor.LastUpdate = DateTime.Now.ToUniversalTime();
                        BillingController.SaveCurrencie(valor);
                    }

                }             
            }
        }

        private void login(HttpContext context)
        {
            string json = string.Empty;
            try
            {
                string username = (context.Request["u"] == null ? string.Empty : context.Request["u"].ToString());
                string password = (context.Request["p"] == null ? string.Empty : context.Request["p"].ToString());

                FDSUsers user = BillingController.GetFDSUser(username, password);
                FDSUsersAccess userAccess = BillingController.GetFDSUserAccess(user.IdUser);

                if (!user.IdUser.Equals(0))
                {
                    switch(user.IdType)
                    {
                        //USUARIO DE GRUPO
                        case 1:
                            json = "{\"code\":\"OK\",\"message\":\"Default.aspx\",\"idtype\":\"1\",\"idsite\":\"null\",\"idgroup\":\"" + userAccess.IdGroup + "\",\"idlocation\":\"null\",\"iduser\":\"" + user.IdUser + "\"}";
                            break;
                        //USUARIO DE SITIO
                        case 2:
                            json = "{\"code\":\"OK\",\"message\":\"Default.aspx\",\"idtype\":\"2\",\"idsite\":\"" + userAccess.IdSite + "\",\"idgroup\":\"null\",\"idlocation\":\"null\",\"iduser\":\"" + user.IdUser + "\"}";
                            break;
                        //USUARIO DE LOCATION
                        case 3:
                            json = "{\"code\":\"OK\",\"message\":\"Default.aspx\",\"idtype\":\"3\",\"idsite\":\"null\",\"idgroup\":\"null\",\"idlocation\":\"" + userAccess.IdLocation + "\",\"iduser\":\"" + user.IdUser + "\"}";
                            break;
                    }
                    
                    HttpCookie cookie = new HttpCookie("FDSUser");
                    cookie.Name = "FDSUser";
                    cookie.Value = user.IdUser.ToString();
                    cookie.Expires = DateTime.Now.AddDays(1);

                    HttpCookie cookieRol = new HttpCookie("FDSUserRol");
                    cookieRol.Name = "FDSUserRol";
                    cookieRol.Value = user.IdRol.ToString();
                    cookieRol.Expires = DateTime.Now.AddDays(1);

                    HttpContext.Current.Response.Cookies.Add(cookie);
                    HttpContext.Current.Response.Cookies.Add(cookieRol);


                    

                }
                else
                    json = "{\"code\":\"ERROR\",\"message\":\"0x0002\"}";
            }
            catch (Exception ex)         
            {
                json = "{\"code\":\"ERROR\",\"message\":\"0x0001\"}";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();
        }

        private void logout(HttpContext context)
        {
            string json = string.Empty;
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    //FormsAuthentication.SignOut();

                    HttpContext.Current.Request.Cookies.Remove("FDSUser");
                    HttpContext.Current.Request.Cookies.Remove("FDSUserRol");
                    HttpContext.Current.Response.Cookies.Remove("FDSUser");
                    HttpContext.Current.Response.Cookies.Remove("FDSUserRol");
                    HttpContext.Current.Response.Cookies.Remove("site");
                    HttpContext.Current.Request.Cookies.Remove("site");
                    HttpContext.Current.Response.Cookies.Remove("location");
                    HttpContext.Current.Request.Cookies.Remove("location");

                    json = "{\"code\":\"OK\",\"message\":\"" + FormsAuthentication.LoginUrl + "\"}";


                }
                else

                    json = "{\"code\":\"ERROR\",\"message\":\"Username or password wrong.\"}";
            }
            catch
            {
                json = "{\"code\":\"ERROR\",\"message\":\"Se ha producido un error en el envio. Inténtelo más tarde.\"}";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();
        }


        
        private ListPriorityResult PrioritiesFilter(HttpContext context)
        {
            ListPriorityResult response = new ListPriorityResult();
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    string idmember = (context.Request["idmember"] == null ? string.Empty : context.Request["idmember"].ToString());
                    string idmembertype = (context.Request["idmembertype"] == null ? string.Empty : context.Request["idmembertype"].ToString());

                    List<Wifi360.Data.Model.Priorities> list = new List<Wifi360.Data.Model.Priorities>();
                    switch (idmembertype)
                    {
                        case "2": list = BillingController.GetPriorities(int.Parse(idmember)); break;
                        case "3":
                            Locations2 loca = BillingController.GetLocation(int.Parse(idmember));
                            Hotels h = BillingController.GetHotel(loca.IdHotel);
                            list = BillingController.GetPriorities(h.IdHotel); break;
                    }

                    response.list = new List<PriorityResult>();

                    foreach (Wifi360.Data.Model.Priorities b in list.OrderBy(a => a.IdPriority))
                    {
                        PriorityResult br = new PriorityResult();
                        br.IdPriority = b.IdPriority;
                        br.Description = b.Description;

                        response.list.Add(br);
                    }
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;


        }

        private ListPriorityResult prioritiesUserFilter(HttpContext context)
        {
            ListPriorityResult response = new ListPriorityResult();
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    string iduser = (context.Request["iduser"] == null ? string.Empty : context.Request["iduser"].ToString());

                    Users user = BillingController.GetRadiusUser(int.Parse(iduser));
                    List<Wifi360.Data.Model.Priorities> list =BillingController.GetPriorities(user.IdHotel); 
                    response.list = new List<PriorityResult>();

                    foreach (Wifi360.Data.Model.Priorities b in list.OrderBy(a => a.IdPriority))
                    {
                        PriorityResult br = new PriorityResult();
                        br.IdPriority = b.IdPriority;
                        br.Description = b.Description;

                        response.list.Add(br);
                    }
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;


        }

        private ListPriorityResult Priorities(HttpContext context)
        {
            ListPriorityResult response = new ListPriorityResult();
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    Hotels hotel = null;
                    Locations2 location = null;
                    FDSGroups group = null;
                    SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group);                    

                    string ListHoteles = ListadoHoteles(group);

                    List<Wifi360.Data.Model.Priorities> list = new List<Data.Model.Priorities>();

                    if (!string.IsNullOrEmpty(ListHoteles))
                    {
                        string[] ids = ListHoteles.Split(',');
                        foreach (string id in ids)
                            list = list.Concat(BillingController.GetPriorities(int.Parse(id))).ToList();
                    }
                    else
                        list = BillingController.GetPriorities(hotel.IdHotel);

                    string idsite = (context.Request["idsite"] == null ? string.Empty : context.Request["idsite"].ToString());
                    int idsi = 0;
                    if (Int32.TryParse(idsite, out idsi))
                        list = BillingController.GetPriorities(idsi);

                    response.list = new List<PriorityResult>();

                    foreach (Wifi360.Data.Model.Priorities b in list.OrderBy(a => a.IdPriority))
                    {
                        PriorityResult br = new PriorityResult();
                        br.IdPriority = b.IdPriority;
                        br.Description = b.Description;

                        response.list.Add(br);
                    }
                }
            }
            catch
            {   
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private PriorityResult Priority(HttpContext context)
        {
            PriorityResult response = new PriorityResult();
            try
            {
                string id = (context.Request["id"] == null ? string.Empty : context.Request["id"].ToString());

                Wifi360.Data.Model.Priorities obj = BillingController.ObtainPriority(Int32.Parse(id));

                response.IdPriority = obj.IdPriority;
                response.Description = obj.Description;

            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private ListRoomResult Rooms(HttpContext context)
        {
            ListRoomResult response = new ListRoomResult();
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    string idmember = (context.Request["idmember"] == null ? string.Empty : context.Request["idmember"].ToString());
                    string idmembertype = (context.Request["idmembertype"] == null ? string.Empty : context.Request["idmembertype"].ToString());

                    List<Rooms> list = new List<Rooms>();
                    switch (idmembertype)
                    {
                        case "2": list = BillingController.ObtainRooms(int.Parse(idmember)); break;
                    }


                    response.list = new List<RoomResult>();

                    foreach (Rooms b in list.OrderBy(a => a.IdRoom))
                    {
                        RoomResult br = new RoomResult();
                        br.IdRoom = b.IdRoom;
                        br.Name = b.Name;
                        if (b.Default)
                            response.Default = b.IdRoom;

                        response.list.Add(br);
                    }
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private ListRoomResult RoomsNoFilter(HttpContext context)
        {
            ListRoomResult response = new ListRoomResult();
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    Hotels hotel = null;
                    Locations2 location = null;
                    SitesGMT siteGMT = GetSiteGMt(out location, out hotel);

                    List<Rooms> list = BillingController.ObtainRooms(hotel.IdHotel); 

                    response.list = new List<RoomResult>();

                    foreach (Rooms b in list.OrderBy(a => a.IdRoom))
                    {
                        RoomResult br = new RoomResult();
                        br.IdRoom = b.IdRoom;
                        br.Name = b.Name;
                        if (b.Default)
                            response.Default = b.IdRoom;

                        response.list.Add(br);
                    }
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private void saveBillingType(HttpContext context)
        {
            string json = string.Empty;
            try
            {
                //OBTENEMOS EL TIPO DE USUARIOS
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    //if (HttpContext.Current.Request.Cookies["site"] != null)
                    //    fdsUser.IdHotel = Int32.Parse(HttpContext.Current.Request.Cookies["site"].Value);

                    string paypaldescription = (context.Request["paypaldescription"] == null ? string.Empty : context.Request["paypaldescription"].ToString());
                    string description = (context.Request["description"] == null ? string.Empty : context.Request["description"].ToString());
                    string price = (context.Request["price"] == null ? string.Empty : context.Request["price"].ToString());
                    string timecredit = (context.Request["timecredit"] == null ? string.Empty : context.Request["timecredit"].ToString());
                    string validtill = (context.Request["validtill"] == null ? string.Empty : context.Request["validtill"].ToString());
                    string maxdevices = (context.Request["maxdevices"] == null ? string.Empty : context.Request["maxdevices"].ToString());
                    string bwdown = (context.Request["bwdown"] == null ? string.Empty : context.Request["bwdown"].ToString());
                    string bwup = (context.Request["bwup"] == null ? string.Empty : context.Request["bwup"].ToString());
                    string priority = (context.Request["priority"] == null ? string.Empty : context.Request["priority"].ToString());
                    string freeaccess = (context.Request["freeaccess"] == null ? string.Empty : context.Request["freeaccess"].ToString());
                    string visible = (context.Request["visible"] == null ? string.Empty : context.Request["visible"].ToString());
                    string order = (context.Request["order"] == null ? string.Empty : context.Request["order"].ToString());
                    string enabled = (context.Request["enabled"] == null ? string.Empty : context.Request["enabled"].ToString());
                    string volumeup = (context.Request["volumeup"] == null ? string.Empty : context.Request["volumeup"].ToString());
                    string volumedown = (context.Request["volumedown"] == null ? string.Empty : context.Request["volumedown"].ToString());
                    string location = (context.Request["location"] == null ? string.Empty : context.Request["location"].ToString());
                    string billingmodule = (context.Request["billingmodule"] == null ? string.Empty : context.Request["billingmodule"].ToString());
                    string fastticket = (context.Request["fastticket"] == null ? string.Empty : context.Request["fastticket"].ToString());
                    string urllanding = (context.Request["urllanding"] == null ? string.Empty : context.Request["urllanding"].ToString());
                    string filterid = (context.Request["filterid"] == null ? string.Empty : context.Request["filterid"].ToString());

                    string id = string.Empty;
                    id = (context.Request["id"] == null ? string.Empty : context.Request["id"].ToString());

                    BillingTypes type = null;
                    if (string.IsNullOrEmpty(id) || id.Equals("undefined"))
                        type = new BillingTypes();
                    else
                        type = BillingController.ObtainBillingType(Int32.Parse(id));

                    type.IdHotel = Int32.Parse(HttpContext.Current.Request.Cookies["site"].Value);
                    type.Description = description;
                    type.PayPalDescription = paypaldescription;
                    type.Price = Double.Parse(price);
                    type.ValidTill = Int32.Parse(validtill);
                    type.MaxDevices = Int32.Parse(maxdevices);

                    if (Int64.Parse(timecredit) >= Int32.MaxValue)
                        type.TimeCredit = Int32.MaxValue;
                    else
                    {
                        Int64 valorMultiplicacion = Int64.Parse(timecredit) * Int64.Parse(maxdevices);
                        if ((Int32.Parse(maxdevices) != 0) && (Int32.Parse(timecredit) != 0))
                        {
                            if (valorMultiplicacion <= Int32.MaxValue)
                                type.TimeCredit = Int32.Parse(timecredit) * Int32.Parse(maxdevices);
                            else
                                type.TimeCredit = Int32.MaxValue;
                        }
                        else
                            type.TimeCredit = Int32.Parse(timecredit);
                    }

                    var iVolumeDown = System.Numerics.BigInteger.Parse(volumedown);
                    if (iVolumeDown > Int64.MaxValue)
                        type.VolumeDown = Int64.MaxValue;
                    else
                    {
                        var iMultiplicacion = System.Numerics.BigInteger.Parse(volumedown) * type.MaxDevices;
                        if (iMultiplicacion > Int64.MaxValue)
                            type.VolumeDown = Int64.MaxValue;
                        else
                            type.VolumeDown = Int64.Parse(volumedown) * type.MaxDevices;
                    }

                    var iVolumeUp = System.Numerics.BigInteger.Parse(volumeup);
                    if (iVolumeUp > Int64.MaxValue)
                        type.VolumeUp = Int64.MaxValue;
                    else
                    {
                        var iMultiplicacion = System.Numerics.BigInteger.Parse(volumeup) * type.MaxDevices;
                        if (iMultiplicacion > Int64.MaxValue)
                            type.VolumeUp = Int64.MaxValue;
                        else
                            type.VolumeUp = Int64.Parse(volumedown) * type.MaxDevices;
                    }

                    type.BWDown = Int32.Parse(bwdown);
                    type.BWUp = Int32.Parse(bwup);
                    type.IdPriority = Int32.Parse(priority);
                    type.IdLocation = Int32.Parse(location);
                    type.FreeAccess = Boolean.Parse(freeaccess);
                    type.Visible = Boolean.Parse(visible);
                    type.Order = Int32.Parse(order);
                    type.Enabled = Boolean.Parse(enabled);
                    type.IdBillingModule = Int32.Parse(billingmodule);
                    type.DefaultFastTicket = Boolean.Parse(fastticket);
                    type.UrlLanding = urllanding;
                    //type.Filter_Id = Int32.Parse(filterid);

                    if (type.DefaultFastTicket)
                    {
                        BillingTypes temp = BillingController.ObtainBillingTypeDefaultFastTicket(Int32.Parse(HttpContext.Current.Request.Cookies["site"].Value));
                        if (temp.IdBillingType.Equals(0) || temp.IdBillingType.Equals(type.IdBillingType))
                        {
                            BillingController.SaveBillingType(type);
                            json = "{\"code\":\"OK\",\"message\":\" \"}";
                        }
                        else
                        {
                            json = "{\"code\":\"ERROR\",\"message\":\"There is already a default fast access type selected. Please, select only one default access type. \"}";
                        }
                    }
                    else
                    {
                        BillingController.SaveBillingType(type);
                        json = "{\"code\":\"OK\",\"message\":\" \"}";
                    }
                }
            }
            catch
            {
                json = "{\"code\":\"ERROR\",\"message\":\"Se ha producido un error en el envio. Inténtelo más tarde.\"}";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();


        }

        private void saveBillingType(HttpContext context, Dictionary<string, string> sData)
        {
            string json = string.Empty;
            try
            {
                //OBTENEMOS EL TIPO DE USUARIOS
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));

                    string paypaldescription = sData["paypaldescription"];
                    string description = sData["description"];
                    string price = sData["price"];
                    string timecredit = sData["timecredit"];
                    string validtill = sData["validtill"];
                    string validafterfirstuse = sData["validafterfirstuse"];
                    string maxdevices = sData["maxdevices"];
                    string bwdown = sData["bwdown"];
                    string bwup = sData["bwup"];
                    string priority = sData["priority"];
                    string freeaccess = sData["freeaccess"];
                    string visible = sData["visible"];
                    string order = sData["order"];
                    string enabled = sData["enabled"];
                    string volumeup = sData["volumeup"];
                    string volumedown = sData["volumedown"];
                    string location = sData["location"];
                    string billingmodule = sData["billingmodule"];
                    string fastticket = sData["fastticket"];
                    string urllanding = sData["urllanding"];
                    string filterid = sData["filterid"];
                    string iot = sData["iot"];
                    string nextBillingType = sData["nextBillingType"];
                    string volumecombined = sData["volumecombined"];

                    string id = sData["id"];

                    BillingTypes2 type = null;
                    if (string.IsNullOrEmpty(id) || id.Equals("undefined"))
                        type = new BillingTypes2();
                    else
                        type = BillingController.ObtainBillingType2(Int32.Parse(id));

                    Hotels h = null;

                    if (HttpContext.Current.Request.Cookies["location"] != null)
                    {
                        Locations2 l = BillingController.GetLocation(Int32.Parse(HttpContext.Current.Request.Cookies["location"].Value));
                        h = BillingController.GetHotel(l.IdHotel);

                    }
                    else
                    {
                        h = BillingController.GetHotel(Int32.Parse(HttpContext.Current.Request.Cookies["site"].Value));
                    }

                    type.IdHotel = h.IdHotel;
                    type.Description = description;
                    type.PayPalDescription = paypaldescription;
                    type.Price = Double.Parse(price);
                    type.ValidTill = Int32.Parse(validtill);
                    type.MaxDevices = Int32.Parse(maxdevices);
                    type.ValidAfterFirstUse = Int32.Parse(validafterfirstuse);

                    List<ParametrosConfiguracion> parametros = BillingController.GetParametrosConfiguracion(h.IdHotel);
                    bool isVolumeCombined = false;
                    ParametrosConfiguracion p = (from t in parametros where t.Key.Equals("VOLUMECOMBINED") select t).FirstOrDefault();
                    if (p != null)
                        isVolumeCombined = Boolean.Parse(p.value);

                    if (Double.Parse(timecredit.Replace('.',',')) >= Int32.MaxValue)
                        type.TimeCredit = Int32.MaxValue;
                    else
                    {
                        Double valorMultiplicacion = Double.Parse(timecredit.Replace('.', ',')) * Int32.Parse(maxdevices);
                        if ((Int32.Parse(maxdevices) != 0) && (Double.Parse(timecredit) != 0))
                        {
                            if (valorMultiplicacion <= Int32.MaxValue)
                                type.TimeCredit = Int32.Parse(Math.Round(valorMultiplicacion, 0).ToString());
                            else
                                type.TimeCredit = Int32.MaxValue;
                        }
                        else
                            type.TimeCredit = Int32.Parse(timecredit.Replace('.', ','));
                    }

                    if (!isVolumeCombined)
                    {
                        Double iVolumeDown = Double.Parse(volumedown.Replace('.', ','));
                        if (iVolumeDown > Int64.MaxValue)
                            type.VolumeDown = Int64.MaxValue;
                        else
                        {
                            Double iMultiplicacion = Double.Parse(volumedown.Replace('.', ',')) * type.MaxDevices;
                            if (Double.Parse(Math.Round(iMultiplicacion, 0).ToString("0")) > Int64.MaxValue)
                                type.VolumeDown = Int64.MaxValue;
                            else
                            {

                                type.VolumeDown = Int64.Parse(Math.Round(iMultiplicacion, 0).ToString("0"));

                            }
                        }

                        Double iVolumeUp = Double.Parse(volumeup.Replace('.', ','));
                        if (iVolumeUp > Int64.MaxValue)
                            type.VolumeUp = Int64.MaxValue;
                        else
                        {
                            Double iMultiplicacion = Double.Parse(volumeup.Replace('.', ',')) * type.MaxDevices;
                            if (Double.Parse(Math.Round(iMultiplicacion, 0).ToString("0")) > Int64.MaxValue)
                                type.VolumeUp = Int64.MaxValue;
                            else
                            {

                                type.VolumeUp = Int64.Parse(Math.Round(iMultiplicacion, 0).ToString("0"));

                            }
                        }
                    }
                    else
                    {
                        //VOLUME COMBINED

                        Double iVolumeCombined = Double.Parse(volumecombined.Replace('.', ','));
                        if (iVolumeCombined > Int64.MaxValue)
                            type.VolumeUp = Int64.MaxValue;
                        else
                        {
                            Double iMultiplicacion = Double.Parse(volumecombined.Replace('.', ',')) * type.MaxDevices;
                            if (Double.Parse(Math.Round(iMultiplicacion, 0).ToString("0")) > Int64.MaxValue)
                            {
                                type.VolumeDown = Int64.MaxValue;
                                type.VolumeUp = Int64.MaxValue;
                            }
                            else
                            {

                                type.VolumeDown = Int64.Parse(Math.Round(iMultiplicacion, 0).ToString("0"));
                                type.VolumeUp = Int64.Parse(Math.Round(iMultiplicacion, 0).ToString("0"));
                            }
                        }
                    }

                    if (Double.Parse(bwdown.Replace('.', ',')) >= Int64.MaxValue)
                        type.BWDown = Int64.MaxValue;
                    else
                        type.BWDown = Int64.Parse(Math.Round(Double.Parse(bwdown.Replace('.', ',')), 0).ToString("0"));

                    if (Double.Parse(bwup.Replace('.', ',')) >= Int64.MaxValue)
                        type.BWUp = Int64.MaxValue;
                    else
                        type.BWUp = Int64.Parse(Math.Round(Double.Parse(bwup.Replace('.', ',')), 0).ToString("0"));

                    
                    type.IdPriority = Int32.Parse(priority);
                    type.IdLocation = Int32.Parse(location);
                    type.FreeAccess = Boolean.Parse(freeaccess);
                    type.Visible = Boolean.Parse(visible);
                    type.Order = Int32.Parse(order);
                    type.Enabled = Boolean.Parse(enabled);
                    type.IdBillingModule = Int32.Parse(billingmodule);
                    type.DefaultFastTicket = Boolean.Parse(fastticket);
                    type.UrlLanding = urllanding;
                    type.Filter_Id = Int32.Parse(filterid);
                    type.IoT = Boolean.Parse(iot);
                    if (!nextBillingType.Equals("0"))
                        type.NextBillingType = int.Parse(nextBillingType);
                    else
                        type.NextBillingType = null;

                    if (type.DefaultFastTicket)
                    {
                        BillingTypes temp = BillingController.ObtainBillingTypeDefaultFastTicket(h.IdHotel);
                        if (temp.IdBillingType.Equals(0) || temp.IdBillingType.Equals(type.IdBillingType))
                        {
                            BillingController.SaveBillingType(type);
                            json = "{\"code\":\"OK\",\"message\":\"0x0009\"}";
                        }
                        else
                        {
                            json = "{\"code\":\"NO\",\"message\":\"0x0010\"}";
                        }
                    }
                    else
                    {
                        BillingController.SaveBillingType(type);
                        json = "{\"code\":\"OK\",\"message\":\"0x0009\"}";
                    }
                }
            }
            catch
            {
                json = "{\"code\":\"ERROR\",\"message\":\"0x9999\"}";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();


        }

        private void saveFilterProfile(HttpContext context, Dictionary<string, string> sData)
        {
            string json = string.Empty;
            try
            {
                //OBTENEMOS EL TIPO DE USUARIOS
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));

                    string description = sData["description"];

                    string id = sData["id"];

                    Wifi360.Data.Model.FilteringProfiles type = null;
                    if (string.IsNullOrEmpty(id) || id.Equals("undefined"))
                        type = new Wifi360.Data.Model.FilteringProfiles();
                    else
                        type = BillingController.GetFilteringProfile(Int32.Parse(id));

                    Hotels h = null;

                    if (HttpContext.Current.Request.Cookies["location"] != null)
                    {
                        Locations2 l = BillingController.GetLocation(Int32.Parse(HttpContext.Current.Request.Cookies["location"].Value));
                        h = BillingController.GetHotel(l.IdHotel);

                    }
                    else
                    {
                        h = BillingController.GetHotel(Int32.Parse(HttpContext.Current.Request.Cookies["site"].Value));
                    }

                    type.IdSite = h.IdHotel;
                    type.Description = description;

                    BillingController.SaveFilterProfile(type);
                    json = "{\"code\":\"OK\",\"message\":\"0x0029\"}";
                }
            }
            catch
            {
                json = "{\"code\":\"ERROR\",\"message\":\"0x9999\"}";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();


        }

        private void savelocation(HttpContext context, Dictionary<string, string> sData)
        {
            string json = string.Empty;

            try
            {
                string urllanding = sData["urllanding"];
                string idlocation = sData["idlocation"];
                string defaultmodule = sData["defaultmodule"];
                string disclaimermandatory = sData["disclaimermandatory"];
                string Default = sData["default"];
                //    string deviceid = sData["deviceid"];
                string socialNetworks = sData["SocialNetworks"];
                string currency = sData["currency"];
                string exchange = sData["exchange"];
                string latitude = sData["latitude"];
                string longitude = sData["longitude"];

                string loginModule = sData["loginModule"];
                string socialNetworkModule = sData["socialNetworksModule"];
                string voucherModule = sData["voucherModule"];
                string payAccessModule = sData["payAccessModule"];
                string freeAccessModule = sData["freeAccessModule"];
                string CustomModule = sData["customModule"];
                string mandatorySurvey = sData["mandatorysurvey"];
                string delaysurvey = sData["delaysurvey"];

                Locations2 location = BillingController.ObtainLocation(int.Parse(idlocation));

                location.UrlLanding = urllanding;
                location.DefaultModule = Int32.Parse(defaultmodule);
                location.Disclaimer = Boolean.Parse(disclaimermandatory);
                location.Default = Boolean.Parse(Default);
                //     location.deviceId = deviceid;
                location.Latitude = latitude;
                location.Longitude = longitude;
                location.GMT = 0;
                location.Currency = currency;
                location.ExchangeCurrency = double.Parse(exchange.Replace('.', ','));
                location.LoginModule = bool.Parse(loginModule);
                location.PayAccessModule = bool.Parse(payAccessModule);
                location.FreeAccessModule = bool.Parse(freeAccessModule);
                location.VoucherModule = bool.Parse(voucherModule);
                location.SocialNetworksModule = bool.Parse(socialNetworkModule);
                location.CustomAccessModule = bool.Parse(CustomModule);
                location.MandatorySurvey = bool.Parse(mandatorySurvey);
                location.DelaySurvey = int.Parse(delaysurvey);

                BillingController.SaveLocation(location);

                List<LocationsSocialNetworks> locationSocialNetwoks = BillingController.GetLocationSocialNetwoks(location.IdLocation);
                string[] valuesSN = socialNetworks.Split('|');
                foreach (string valueSN in valuesSN)
                {
                    if (!string.IsNullOrEmpty(valueSN))
                    {
                        string[] values = valueSN.Split('=');
                        LocationsSocialNetworks sSN = (from t in locationSocialNetwoks where t.IdSocialNetwork.Equals(Int32.Parse(values[0])) select t).FirstOrDefault();
                        sSN.Active = Boolean.Parse(values[1]);

                        BillingController.SaveLocationSocialNetwork(sSN);
                    }

                }
                if (!string.IsNullOrEmpty(latitude) && !string.IsNullOrEmpty(longitude))
                {
                    SitesGMT siteGMT = BillingController.SiteGMTSitebyLocation(location.IdLocation);
                    HttpClient client = new HttpClient();
                    string _address = "http://api.timezonedb.com/v2.1/get-time-zone?key=YSFN7EK1ONKS&format=xml&by=position&lat=" + latitude + "&lng=" + longitude;
                    var response = client.GetAsync(_address).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        // Modify as per your requirement    
                        string responseString = response.Content.ReadAsStringAsync().Result;
                        // Your code goes here      
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(responseString);

                        XmlNodeList xmlnode = doc.GetElementsByTagName("gmtOffset");

                        XmlNodeList xmlnodeDST = doc.GetElementsByTagName("dst");

                        siteGMT.gmtoffset = Int32.Parse(xmlnode[0].ChildNodes.Item(0).InnerText);
                        siteGMT.dst = Int32.Parse(xmlnodeDST[0].ChildNodes.Item(0).InnerText);
                        siteGMT.IdSite = 0;
                        siteGMT.IdLocation = location.IdLocation;
                        siteGMT.lastupdate = DateTime.Now.ToUniversalTime();

                        BillingController.SaveSiteGMT(siteGMT);
                    }

                }

                json = "{\"code\":\"OK\",\"message\":\"0x0026\"}";
            }
            catch (Exception ex)
            {
                json = "{\"code\":\"ERROR\",\"message\":\" " + ex.Message + ".\"}";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();
        }

        private void savePriority(HttpContext context, Dictionary<string, string> sData)
        {
            string json = string.Empty;
            try
            {
                //OBTENEMOS EL TIPO DE USUARIOS
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));

                    string description = sData["description"];

                    string id = sData["id"];

                    Wifi360.Data.Model.Priorities type = null;
                    if (string.IsNullOrEmpty(id) || id.Equals("undefined"))
                        type = new Wifi360.Data.Model.Priorities();
                    else
                        type = BillingController.ObtainPriority(Int32.Parse(id));

                    Hotels h = null;

                    if (HttpContext.Current.Request.Cookies["location"] != null)
                    {
                        Locations2 l = BillingController.GetLocation(Int32.Parse(HttpContext.Current.Request.Cookies["location"].Value));
                        h = BillingController.GetHotel(l.IdHotel);

                    }
                    else
                    {
                        h = BillingController.GetHotel(Int32.Parse(HttpContext.Current.Request.Cookies["site"].Value));
                    }

                    type.IdHotel = h.IdHotel;
                    type.Description = description;

                    BillingController.SavePriority(type);
                    json = "{\"code\":\"OK\",\"message\":\"0x0028\"}";
                }
            }
            catch
            {
                json = "{\"code\":\"ERROR\",\"message\":\"0x9999\"}";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();


        }

        private void savesettings(HttpContext context, Dictionary<string, string> sData)
        {
            string json = string.Empty;

            try
            {
                FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));

                string id = string.Empty;
                if (HttpContext.Current.Request.Cookies["site"] != null)
                    id = HttpContext.Current.Request.Cookies["site"].Value;

                if (sData["id"] != "0")
                    id = sData["id"];
                string name = sData["name"];
                string nse = sData["nse"];
                string nseprincipalip = sData["nseprincipalip"];
                string bandwidth = sData["bandwidth"];
                string bandwidthup = sData["bandwidthup"];
                string paypaluser = sData["paypaluser"];
                string currencycode = sData["currencycode"];
                string mailfrom = sData["mailfrom"];
                string smtp = sData["smtp"];
                string smtpport = sData["smtpport"];
                string smtpuser = sData["smtpuser"];
                string smtppassword = sData["smtppassword"];
                string smtpssl = sData["smtpssl"];
                string urlhotel = sData["urlhotel"];
                string macblocking = sData["macblocking"];
                string macbannedinterval = sData["macbannedinterval"];
                string maxmacattemps = sData["maxmacattemps"];
                string macattempsinterval = sData["macattempsinterval"];
                string macauthenticate = sData["macauthenticate"];
                string freeaccessrepeattime = sData["freeaccessrepeattime"];
                string urlreturnpaypal = sData["urlreturnpaypal"];
                string freeaccessmodule = sData["freeaccessmodule"];
                string payaccessmodule = sData["payaccessmodule"];
                string socialnetworkmodule = sData["socialnetworkmodule"];
                string customaccessmodule = sData["customaccessmodule"];
                string voucheraccessmodule = sData["voucheraccessmodule"];
                string idlanguagedefault = sData["idlanguagedefault"];
                string loginmodule = sData["loginmodule"];
                string survey = sData["survey"];
                string printlogo = sData["printlogo"];
                string printvolume = sData["printvolume"];
                string printspeed = sData["printspeed"];
                string prefix = sData["prefix"];
                string longusername = sData["longusername"];
                string longpassword = sData["longpassword"];
                string socialNetwoks = sData["SocialNetworks"];
                string enableddaylypass = sData["enableddaylypass"];
                string valuedaylypass = sData["valuedaylypass"];
                string voucherprefix = sData["voucherprefix"];
                string longvoucherusername = sData["longvoucherusername"];
                string vendor = sData["vendor"];
                string filtercontents = sData["filtercontents"];
                string latitude = sData["latitude"];
                string longitude = sData["longitude"];
                string volumecombined = sData["volumecombined"];

                int idhotel = 0;
                Hotels h = null;
                if (Int32.TryParse(id, out idhotel))
                    h = BillingController.GetHotel(idhotel);
                else
                    h = BillingController.GetHotel(Int32.Parse(HttpContext.Current.Request.Cookies["site"].Value));


                h.Name = name;
                h.NSE = nse;
                h.PrincipalNSEIP = nseprincipalip;
                h.Bandwidth = Int64.Parse(bandwidth);
                h.BandwidthUp = Int64.Parse(bandwidthup);
                h.PayPalUser = paypaluser;
                h.CurrencyCode = currencycode;
                h.MailFrom = mailfrom;
                h.SMTP = smtp;
                h.SmtpPassword = smtppassword;
                h.SmtpPort = smtpport;
                h.SmtpSSL = Boolean.Parse(smtpssl);
                h.SmtpUser = smtpuser;
                h.UrlHotel = urlhotel;
                h.GMT = 0;
                h.MACBlocking = Boolean.Parse(macblocking);
                h.MACAttempsInterval = Int32.Parse(macattempsinterval);
                h.MACBannedInterval = Int32.Parse(macbannedinterval);
                h.MaxMACAttemps = Int32.Parse(maxmacattemps);
                h.MacAuthenticate = Boolean.Parse(macauthenticate);
                h.FreeAccessRepeatTime = Int32.Parse(freeaccessrepeattime);
                h.PrintLogo = Boolean.Parse(printlogo);
                h.PrintVolume = Boolean.Parse(printvolume);
                h.PrintSpeed = Boolean.Parse(printspeed);
                h.UrlReturnPaypal = urlreturnpaypal;
                h.FreeAccessModule = Boolean.Parse(freeaccessmodule);
                h.PayAccessModule = Boolean.Parse(payaccessmodule);
                h.SocialNetworksModule = Boolean.Parse(socialnetworkmodule);
                h.CustomAccessModule = Boolean.Parse(customaccessmodule);
                h.VoucherModule = Boolean.Parse(voucheraccessmodule);
                h.LoginModule = Boolean.Parse(loginmodule);
                h.IdLanguage = Int32.Parse(idlanguagedefault);
                h.Survey = Boolean.Parse(survey);
                h.IdVendor = Int32.Parse(vendor);
                h.FilterContents = Boolean.Parse(filtercontents);
                h.Longitude = longitude;
                h.Latitude = latitude;

                List<SitesSocialNetworks> siteSocialNetwoks = BillingController.GetSiteSocialNetwoks(h.IdHotel);
                string[] valuesSN = socialNetwoks.Split('|');
                foreach (string valueSN in valuesSN)
                {
                    if (!string.IsNullOrEmpty(valueSN))
                    {
                        string[] values = valueSN.Split('=');
                        SitesSocialNetworks sSN = (from t in siteSocialNetwoks where t.IdSiteSocialNetwork.Equals(Int32.Parse(values[0])) select t).FirstOrDefault();
                        sSN.Active = Boolean.Parse(values[1]);

                        BillingController.SaveSiteSocialNetwork(sSN);
                    }

                }

                BillingController.SaveHotel(h);

                List<ParametrosConfiguracion> parametros = BillingController.GetParametrosConfiguracion(idhotel);
                ParametrosConfiguracion parameter = (from t in parametros where t.Key.ToUpper().Equals("LONGUSERNAME") select t).FirstOrDefault();
                if (parameter == null)
                {
                    ParametrosConfiguracion p = new ParametrosConfiguracion();
                    p.IdParameter = 0;
                    p.value = "3";
                    p.IdSite = h.IdHotel;
                    p.Type = "N";
                    p.Key = "LONGUSERNAME";
                    p.Description = "Longitud del usuario generado automaticamente";
                    parametros.Add(p);
                }

                ParametrosConfiguracion parameter2 = (from t in parametros where t.Key.ToUpper().Equals("LONGPASSWORD") select t).FirstOrDefault();
                if (parameter2 == null)
                {
                    ParametrosConfiguracion p = new ParametrosConfiguracion();
                    p.IdParameter = 0;
                    p.value = "3";
                    p.IdSite = h.IdHotel;
                    p.Type = "N";
                    p.Key = "LONGPASSWORD";
                    p.Description = "Longitud de la contraseña generado automaticamente";
                    parametros.Add(p);
                }

                ParametrosConfiguracion parameter3 = (from t in parametros where t.Key.ToUpper().Equals("USERNAMEPREFIX") select t).FirstOrDefault();
                if (parameter3 == null)
                {
                    ParametrosConfiguracion p = new ParametrosConfiguracion();
                    p.IdParameter = 0;
                    p.value = string.Empty;
                    p.IdSite = h.IdHotel;
                    p.Type = "S";
                    p.Key = "USERNAMEPREFIX";
                    p.Description = "Prefijo de los nombres de usuarios generados automaticamente";
                    parametros.Add(p);
                }

                ParametrosConfiguracion parameter4 = (from t in parametros where t.Key.ToUpper().Equals("SITEPASSENABLE") select t).FirstOrDefault();
                if (parameter4 == null)
                {
                    ParametrosConfiguracion p = new ParametrosConfiguracion();
                    p.IdParameter = 0;
                    p.value = string.Empty;
                    p.IdSite = h.IdHotel;
                    p.Type = "B";
                    p.Key = "SITEPASSENABLE";
                    p.Description = "Activa o desactiva la variable de clave diaria";
                    parametros.Add(p);
                }

                ParametrosConfiguracion parameter5 = (from t in parametros where t.Key.ToUpper().Equals("SITEPASSVALUE") select t).FirstOrDefault();
                if (parameter5 == null)
                {
                    ParametrosConfiguracion p = new ParametrosConfiguracion();
                    p.IdParameter = 0;
                    p.value = string.Empty;
                    p.IdSite = h.IdHotel;
                    p.Type = "S";
                    p.Key = "SITEPASSVALUE";
                    p.Description = "Valor de la clave diaria.";
                    parametros.Add(p);
                }

                ParametrosConfiguracion parameter6 = (from t in parametros where t.Key.ToUpper().Equals("LONGVOUCHERUSER") select t).FirstOrDefault();
                if (parameter6 == null)
                {
                    ParametrosConfiguracion p = new ParametrosConfiguracion();
                    p.IdParameter = 0;
                    p.value = "3";
                    p.IdSite = h.IdHotel;
                    p.Type = "N";
                    p.Key = "LONGVOUCHERUSER";
                    p.Description = "Longitud del usuario voucher generado automaticamente.";
                    parametros.Add(p);
                }

                ParametrosConfiguracion parameter7 = (from t in parametros where t.Key.ToUpper().Equals("VOUCHERPREFIX") select t).FirstOrDefault();
                if (parameter7 == null)
                {
                    ParametrosConfiguracion p = new ParametrosConfiguracion();
                    p.IdParameter = 0;
                    p.value = string.Empty;
                    p.IdSite = h.IdHotel;
                    p.Type = "S";
                    p.Key = "VOUCHERPREFIX";
                    p.Description = "Prefijo de los voucher generados automaticamente.";
                    parametros.Add(p);
                }

                ParametrosConfiguracion parameterVolumeCombined = (from t in parametros where t.Key.ToUpper().Equals("VOLUMECOMBINED") select t).FirstOrDefault();
                if (parameterVolumeCombined == null)
                {
                    ParametrosConfiguracion p = new ParametrosConfiguracion();
                    p.IdParameter = 0;
                    p.value = "False";
                    p.IdSite = h.IdHotel;
                    p.Type = "B";
                    p.Key = "VOLUMECOMBINED";
                    p.Description = "Se utilizará como parametro el volumen combinado de subida y bajada de datos";
                    parametros.Add(p);
                }

                foreach (ParametrosConfiguracion obj in parametros)
                {
                    switch (obj.Key.ToUpper())
                    {
                        case "LONGUSERNAME": obj.value = longusername; break;
                        case "LONGPASSWORD": obj.value = longpassword; break;
                        case "USERNAMEPREFIX": obj.value = prefix;  break;
                        case "SITEPASSENABLE": obj.value = enableddaylypass;  break;
                        case "SITEPASSVALUE": obj.value = valuedaylypass;  break;
                        case "LONGVOUCHERUSER": obj.value = longvoucherusername;  break;
                        case "VOUCHERPREFIX": obj.value = voucherprefix; break;
                        case "VOLUMECOMBINED": obj.value = volumecombined; break;
                    }

                    BillingController.SaveParametroConfiguracion(obj);
                }

                if (!string.IsNullOrEmpty(latitude) && !string.IsNullOrEmpty(longitude))
                {
                    SitesGMT siteGMT = BillingController.SiteGMTSitebySite(h.IdHotel);
                    HttpClient client = new HttpClient();
                    string _address = "http://api.timezonedb.com/v2.1/get-time-zone?key=YSFN7EK1ONKS&format=xml&by=position&lat=" + latitude + "&lng=" + longitude;
                    var response = client.GetAsync(_address).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        // Modify as per your requirement    
                        string responseString = response.Content.ReadAsStringAsync().Result;
                        // Your code goes here      
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(responseString);

                        XmlNodeList xmlnode = doc.GetElementsByTagName("gmtOffset");

                        XmlNodeList xmlnodeDST = doc.GetElementsByTagName("dst");

                        siteGMT.gmtoffset = Int32.Parse(xmlnode[0].ChildNodes.Item(0).InnerText);
                        siteGMT.dst = Int32.Parse(xmlnodeDST[0].ChildNodes.Item(0).InnerText);
                        siteGMT.IdSite = h.IdHotel;
                        siteGMT.IdLocation = 0;
                        siteGMT.lastupdate = DateTime.Now.ToUniversalTime();

                        BillingController.SaveSiteGMT(siteGMT);
                    }

                }

                json = "{\"code\":\"OK\",\"message\":\"0x0025\"}";
            }
            catch (Exception ex)
            {
                json = "{\"code\":\"ERROR\",\"message\":\"0x9999\"}";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();
        }

        private void savesurveyQuestion(HttpContext context)
        {
            string json = string.Empty;
            try
            {
                //OBTENEMOS EL TIPO DE USUARIOS
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));

                    string language = (context.Request["language"] == null ? string.Empty : context.Request["language"].ToString());
                    string question = (context.Request["question"] == null ? string.Empty : context.Request["question"].ToString());
                    string type = (context.Request["type"] == null ? string.Empty : context.Request["type"].ToString());
                    string values = (context.Request["values"] == null ? string.Empty : context.Request["values"].ToString());
                    string order = (context.Request["order"] == null ? string.Empty : context.Request["order"].ToString());
                    string idlocation = (context.Request["idlocation"] == null ? string.Empty : context.Request["idlocation"].ToString());

                    string id = string.Empty;
                    id = (context.Request["id"] == null ? string.Empty : context.Request["id"].ToString());

                    Surveys2 survey = null;
                    if (string.IsNullOrEmpty(id) || id.Equals("undefined"))
                        survey = new Surveys2();
                    else
                        survey = BillingController.GetSurvey(Int32.Parse(id));

                    survey.IdHotel = Int32.Parse(HttpContext.Current.Request.Cookies["site"].Value);
                    survey.IdLanguage = Int32.Parse(language);
                    survey.Order = Int32.Parse(order);
                    survey.Type = string.Format("{0} - {1}", type, values);
                    if (!string.IsNullOrEmpty(idlocation))
                        survey.IdLocation = int.Parse(idlocation);

                    survey.Question = question;

                    BillingController.SaveSurvey(survey);

                    json = "{\"code\":\"OK\",\"message\":\"0x0031\"}";
                }
            }
            catch
            {
                json = "{\"code\":\"ERROR\",\"message\":\"0x9999\"}";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();
        }

        private ListBillingResult searchBilling(HttpContext context)
        {
            ListBillingResult response = new ListBillingResult();
            try
            {
                int PageNumber = 0;
                //OBTENEMOS EL TIPO DE USUARIOS
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));

                    Hotels hotel = null;
                    Locations2 location = null;
                    FDSGroups group = null;
                    SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group);

                    string idroom = (context.Request["r"] == null ? string.Empty : context.Request["r"].ToString());
                    string start = (context.Request["s"] == null ? string.Empty : context.Request["s"].ToString());
                    string end = (context.Request["e"] == null ? string.Empty : context.Request["e"].ToString());
                    string billing = (context.Request["b"] == null ? string.Empty : context.Request["b"].ToString());
                    string idmember = (context.Request["idmember"] == null ? string.Empty : context.Request["idmember"].ToString());
                    string idmembertype = (context.Request["idmembertype"] == null ? string.Empty : context.Request["idmembertype"].ToString());
                    string idfdsuser = (context.Request["idfdsuser"] == null ? string.Empty : context.Request["idfdsuser"].ToString());

                    string page = (context.Request["page"] == null ? string.Empty : context.Request["page"].ToString());

                    if (string.IsNullOrEmpty(start))
                    {
                        start = DateTime.Now.ToUniversalTime().AddDays(-1).ToString();
                        end = DateTime.Now.ToUniversalTime().ToString();

                    }else
                    {
                        start = DateTime.Parse(start).AddSeconds(-siteGMT.gmtoffset).ToString();
                        end = DateTime.Parse(end).AddSeconds(-siteGMT.gmtoffset).ToString();
                    }

                    int p = 0;
                    if (Int32.TryParse(page, out p))
                        PageIndex = p;

                    double total = 0;
                    List<BillingInfo> list = null;
                    List<BillingInfo> listCompleta = null;

                    Currencies2 currency = null;
                    if (group != null)
                        currency = BillingController.Currency(group.Currency);
                    else if (hotel != null)
                        currency = BillingController.Currency(hotel.CurrencyCode);
                    else if (location != null)
                        currency = BillingController.Currency(location.Currency);

                    string listadosHoteles = ListadoHoteles(group);
                    
                    if (int.Parse(idmember) != 0)
                    {
                        int type = int.Parse(idmembertype);
                        switch (type)
                        {
                            case 1:
                                FDSGroups temp = BillingController.GetGroup(int.Parse(idmember));
                                listadosHoteles = ListadoHoteles(temp); break;
                            case 2: hotel = BillingController.GetHotel(int.Parse(idmember)); break;
                            case 3:
                                location = BillingController.GetLocation(int.Parse(idmember));
                                hotel = BillingController.GetHotel(location.IdHotel); break;

                        }
                    }

                    int idfdsuserfilter = 0;
                    if (!string.IsNullOrEmpty(idfdsuser))
                        idfdsuserfilter = int.Parse(idfdsuser);

                    if (fdsUser.IdRol.Equals(3))
                        idfdsuserfilter = fdsUser.IdUser;

                    FDSUsersAccess fdsuserAccess = BillingController.GetFDSUserAccess(fdsUser.IdUser);
                    List<FDSUsers> listFDSUser = null;
                    switch (fdsUser.IdType)
                    {
                        case 2: listFDSUser = BillingController.GetFDSUsersSite(fdsuserAccess.IdSite); break;
                        case 3: listFDSUser = BillingController.GetFDSUsersLocation(fdsuserAccess.IdLocation); break;
                        case 1: listFDSUser = BillingController.GetFDSUsers(); break;
                    }

                    if (location != null)
                    {
                        list = BillingController.produccionLocation(location.IdLocation, listFDSUser, idroom, billing, idfdsuserfilter, start, end, PageIndex, PageSize, ref PageNumber, ref total);
                        listCompleta = BillingController.produccionLocation(location.IdLocation, int.Parse(idroom), billing, idfdsuserfilter, start, end);
                    }
                    else if (hotel != null)
                    {
                        list = BillingController.produccion(hotel.IdHotel, listFDSUser, int.Parse(idroom), start, end, idfdsuserfilter, PageIndex, PageSize, ref PageNumber, ref total);
                        listCompleta = BillingController.produccion(hotel.IdHotel, int.Parse(idroom), start, end, idfdsuserfilter);

                    }
                    else { 
                        list = BillingController.produccion(listadosHoteles, listFDSUser, int.Parse(idroom), start, end, idfdsuserfilter, PageIndex, PageSize, ref PageNumber, ref total);
                        listCompleta = BillingController.produccion(listadosHoteles, DateTime.Parse(start), DateTime.Parse(end), int.Parse(idroom), idfdsuserfilter);
                    }

                    //MEJORA20190521

                    response.list = new List<BillingResult>();
                    response.pageNumber = PageNumber;

                    total = 0;
                    List<FDSUsers> fdsUsers = BillingController.GetFDSUsers();
                    List<FDSUsers> fdsUsersExternos = new List<FDSUsers>();
                    List<FDSUsersAccess> fdsUsersAccess = BillingController.GetFDSUserAccesses();

                    string currency_base = ConfigurationManager.AppSettings["CurrencyBase"].ToString();

                    foreach (BillingInfo b in list.OrderBy(a => a.BillingCharge))
                    {
                        if (HttpContext.Current.Request.Cookies["location"] != null)
                        {
                            if (location.IdLocation.Equals(b.IdLocation))
                            {
                                BillingResult br = new BillingResult();
                                br.idbilling = b.IdBilling;
                                br.Date = b.BillingCharge.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                                br.Room = b.Room;
                                br.Product = b.BillingType;
                                br.Amount = string.Format("{0}", Math.Round(b.Amount, 2).ToString());

                                if (b.Flag.Equals(3))
                                    br.Flag = "<i class=\"fa fa-desktop\"></i>";
                                if (b.Flag.Equals(4))
                                    br.Flag = "<i class=\"fa fa-exclamation-circle\"></i>";
                                else if (b.Flag.Equals(5))
                                    br.Flag = "<i class=\"fa fa-hand-paper\"></i>";
                                else if (b.Flag.Equals(6))
                                    br.Flag = "<i class=\"fa fa-cc-paypal\"></i>";
                                else if (b.Flag.Equals(8))
                                    br.Flag = "<i class=\"fa fa-cc-visa\"></i>";

                                br.iduser = b.IdUser;
                                br.idfdsuser = b.IdFDSUser;

                                FDSUsers fdsUserBill = (from t in fdsUsers where t.IdUser.Equals(b.IdFDSUser) select t).FirstOrDefault();
                                if (fdsUserBill != null)
                                    br.fdsusername = fdsUserBill.Name;

                                br.username = (b.IdUser.Equals(0) ? string.Empty : b.Name);
                                if (b.IdFDSUser.Equals(fdsUser.IdUser))
                                    br.propierty = true;
                                if (!fdsUser.IdRol.Equals(3) || (fdsUser.IdRol.Equals(3) && br.propierty))
                                    response.list.Add(br);

                                //listCompleta.Add(b);
                                
                            }
                        }
                        else
                        {
                            BillingResult br = new BillingResult();
                            br.idbilling = b.IdBilling;
                            br.Date = b.BillingCharge.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                            br.Room = b.Room;
                            br.Product = b.BillingType;
                            br.Amount = string.Format("{0}", Math.Round(b.Amount, 2).ToString());
                            br.idfdsuser = b.IdFDSUser;

                            if (!b.IdLocation.Equals(0))
                            {
                                Locations2 temp_location = BillingController.GetLocation(b.IdLocation);
                                Currencies2 moneda = BillingController.Currency(temp_location.Currency);
                                double cambio = 1;
                                if (!temp_location.Currency.Equals(currency.Code))
                                {
                                    if (!currency_base.Equals(currency.Code)){
                                        Currencies2 moneda_nueva_base = BillingController.Currency(currency.Code);
                                        cambio = ((1 / moneda_nueva_base.Exchange.Value) * moneda.Exchange.Value);
                                    }
                                    else
                                    {
                                        cambio = moneda.Exchange.Value;
                                    }
                                    br.Amount = string.Format("{0}", Math.Round(b.Amount * cambio, 2).ToString());
                                }
                                    
                            }

                            if (b.Flag.Equals(3))
                                br.Flag = "<i class=\"fa fa-desktop\"></i>";
                            if (b.Flag.Equals(4))
                                br.Flag = "<i class=\"fa fa-exclamation-circle\"></i>";
                            else if (b.Flag.Equals(5))
                                br.Flag = "<i class=\"fa fa-hand-paper\"></i>";
                            else if (b.Flag.Equals(6))
                                br.Flag = "<i class=\"fa fa-cc-paypal\"></i>";
                            else if (b.Flag.Equals(8))
                                br.Flag = "<i class=\"fa fa-cc-visa\"></i>";

                            br.iduser = b.IdUser;
                            br.username = (b.IdUser.Equals(0) ? string.Empty : b.Name);
                            FDSUsers fdsUserBill = (from t in fdsUsers where t.IdUser.Equals(b.IdFDSUser) select t).FirstOrDefault();
                            if (fdsUserBill != null)
                                br.fdsusername = fdsUserBill.Name;
                            if (b.IdFDSUser.Equals(fdsUser.IdUser))
                                    br.propierty = true;
                            if (!fdsUser.IdRol.Equals(3) || (fdsUser.IdRol.Equals(3) && br.propierty))
                                     response.list.Add(br);
                        }
                    }

                    response.sellers = new List<FDSUserResult>();
                    List<FDSUsers> all_FDSUsers = BillingController.GetFDSUsers();

                    foreach (BillingInfo b in listCompleta.OrderBy(a => a.BillingCharge))
                    {
                        FDSUsers u = (from t in all_FDSUsers where t.IdUser.Equals(b.IdFDSUser) select t).FirstOrDefault();
                        if (u != null)
                        {
                            FDSUserResult new_seller = new FDSUserResult();
                            new_seller.idFDSUser = u.IdUser;
                            new_seller.Name = u.Name;
                            if (response.sellers.Find(x => x.idFDSUser.Equals(new_seller.idFDSUser)) == null)
                                response.sellers.Add(new_seller);
                            
                        }


                        if (!b.IdLocation.Equals(0))
                        {
                            if (location != null)
                            {
                                //FDSUsers u = (from t in listFDSUser where t.IdUser.Equals(b.IdFDSUser) select t).FirstOrDefault();
                                //if (u != null)
                                //{
                                    Locations2 temp_location = BillingController.GetLocation(b.IdLocation);
                                    Currencies2 moneda = BillingController.Currency(temp_location.Currency);
                                    double cambio = 0;
                                    if (!temp_location.Currency.Equals(currency.Code)){ 
                                        if (!currency_base.Equals(currency.Code)){
                                            Currencies2 moneda_nueva_base = BillingController.Currency(currency.Code);
                                            cambio = ((1 / moneda_nueva_base.Exchange.Value) * moneda.Exchange.Value);
                                        }
                                        else
                                        {
                                            cambio = moneda.Exchange.Value;
                                        }
                                        total += b.Amount * cambio;
                                    }
                                    else
                                        total += b.Amount;
                                //}
                            }
                            else if (hotel != null)
                            {
                                //FDSUsers u = (from t in listFDSUser where t.IdUser.Equals(b.IdFDSUser) select t).FirstOrDefault();
                                //if (u != null)
                                //{
                                    Locations2 temp_location = BillingController.GetLocation(b.IdLocation);
                                    Currencies2 moneda = BillingController.Currency(temp_location.Currency);
                                    double cambio = 0;
                                    if (!temp_location.Currency.Equals(currency.Code))
                                    {
                                        if (!currency_base.Equals(currency.Code))
                                        {
                                            Currencies2 moneda_nueva_base = BillingController.Currency(currency.Code);
                                            cambio = ((1 / moneda_nueva_base.Exchange.Value) * moneda.Exchange.Value);
                                        }
                                        else
                                        {
                                            cambio = moneda.Exchange.Value;
                                        }
                                        total += b.Amount * cambio;
                                    }
                                    else
                                        total += b.Amount;
                                //}
                            }
                            else
                            {
                                Locations2 loc = BillingController.GetLocation(b.IdLocation);
                                Currencies2 moneda = BillingController.Currency(loc.Currency);
                                double cambio = 0;
                                if (!loc.Currency.Equals(currency.Code))
                                {
                                    if (!currency_base.Equals(currency.Code))
                                    {
                                        Currencies2 moneda_nueva_base = BillingController.Currency(currency.Code);
                                        cambio = ((1 / moneda_nueva_base.Exchange.Value) * moneda.Exchange.Value);
                                    }
                                    else
                                    {
                                        cambio = moneda.Exchange.Value;
                                    }
                                    total += b.Amount * cambio;
                                }
                                else
                                    total += b.Amount;
                            }
                        }
                    }

                        response.total = string.Format("{1} :  {0}", Math.Round(total, 2).ToString(), currency.Symbol);
                }
            }
            catch (Exception ex)
            {
                ;
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private ListFailedRequestResult searchFailedRequest(HttpContext context)
        {
            ListFailedRequestResult response = new ListFailedRequestResult();
            try
            {
                int PageNumber = 0;

                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    Hotels hotel = null;
                    Locations2 location = null;
                    FDSGroups group = null;
                    SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group);

                    string start = (context.Request["s"] == null ? string.Empty : context.Request["s"].ToString());
                    string end = (context.Request["e"] == null ? string.Empty : context.Request["e"].ToString());
                    string page = (context.Request["page"] == null ? string.Empty : context.Request["page"].ToString());
                    string idmember = (context.Request["idmember"] == null ? string.Empty : context.Request["idmember"].ToString());
                    string idmembertype = (context.Request["idmembertype"] == null ? string.Empty : context.Request["idmembertype"].ToString());

                    if ((string.IsNullOrEmpty(start)) && (string.IsNullOrEmpty(end)))
                    {
                        start = "01/01/1970";
                        end = DateTime.Now.ToString();

                    }
                    else
                    {
                        start = DateTime.Parse(start).AddHours(-siteGMT.dst).AddSeconds(-siteGMT.gmtoffset).ToString();
                        end = DateTime.Parse(end).AddHours(-siteGMT.dst).AddSeconds(-siteGMT.gmtoffset).ToString();
                    }

                    int p = 0;
                    if (Int32.TryParse(page, out p))
                        PageIndex = p;

                    List<Wifi360.Data.Model.FailedRequests> list = null;
                    string ListHoteles = ListadoHoteles(group);
                    if (int.Parse(idmember) != 0)
                    {
                        int type = int.Parse(idmembertype);
                        switch (type)
                        {
                            case 1:
                                FDSGroups temp = BillingController.GetGroup(int.Parse(idmember));
                                ListHoteles = ListadoHoteles(temp); break;
                            case 2: hotel = BillingController.GetHotel(int.Parse(idmember)); break;
                            case 3:
                                location = BillingController.GetLocation(int.Parse(idmember));
                                hotel = BillingController.GetHotel(location.IdHotel); break;

                        }
                    }
                    if (location != null)
                    {
                        FDSLocationsIdentifier identifier = BillingController.GetFDSLocationIdentifier(Int32.Parse(HttpContext.Current.Request.Cookies["location"].Value));
                        list = BillingController.GetFailedRequest(identifier.Identifier, start, end, PageIndex, PageSize, ref PageNumber);
                    }
                    else if (hotel != null)
                    {
                        list = BillingController.GetFailedRequest(hotel.IdHotel, start, end, PageIndex, PageSize, ref PageNumber);
                    }
                    else { 
                        list = BillingController.GetFailedRequestHoteles(ListHoteles, start, end, PageIndex, PageSize, ref PageNumber);
                    }

                    response.list = new List<FailedRequestResult>();
                    response.pageNumber = PageNumber;

                    foreach (Wifi360.Data.Model.FailedRequests f in list.OrderBy(a => a.MessageDate))
                    {
                        FailedRequestResult r = new FailedRequestResult();
                        r.Date = f.MessageDate.Value.AddSeconds(siteGMT.gmtoffset).ToString();
                        r.UserName = f.UserName;
                        r.Message = f.ReplyMessage;
                        r.MAC = f.CallerID;

                        response.list.Add(r);
                    }
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private ListCaptivePortalLogResult searchLog(HttpContext context)
        {
            ListCaptivePortalLogResult response = new ListCaptivePortalLogResult();
            try
            {
                int PageNumber = 0;

                //OBTENEMOS EL TIPO DE USUARIOS
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));

                    Hotels hotel = null;
                    Locations2 location = null;
                    FDSGroups group = null;
                    SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group);

                    string start = (context.Request["s"] == null ? string.Empty : context.Request["s"].ToString());
                    string end = (context.Request["e"] == null ? string.Empty : context.Request["e"].ToString());
                    string idmember = (context.Request["idmember"] == null ? string.Empty : context.Request["idmember"].ToString());
                    string idmembertype = (context.Request["idmembertype"] == null ? string.Empty : context.Request["idmembertype"].ToString());

                    string page = (context.Request["page"] == null ? string.Empty : context.Request["page"].ToString());

                    if ((start.Equals("undefined")) || string.IsNullOrEmpty(start))
                    {
                        start = "01/01/1970 00:00:00";
                        end = DateTime.Now.ToString();

                    }
                    else
                    {
                        start = DateTime.Parse(start).AddSeconds(-siteGMT.gmtoffset).ToString();
                        end = DateTime.Parse(end).AddSeconds(-siteGMT.gmtoffset).ToString();
                    }

                    int p = 0;
                    if (Int32.TryParse(page, out p))
                        PageIndex = p;

                    List<CaptivePortalLogInfo> list = null;

                    string ListHoteles = ListadoHoteles(group);
                    if (int.Parse(idmember) != 0)
                    {
                        int type = int.Parse(idmembertype);
                        switch (type)
                        {
                            case 1:
                                FDSGroups temp = BillingController.GetGroup(int.Parse(idmember));
                                ListHoteles = ListadoHoteles(temp); break;
                            case 2: hotel = BillingController.GetHotel(int.Parse(idmember)); break;
                            case 3:
                                location = BillingController.GetLocation(int.Parse(idmember));
                                hotel = BillingController.GetHotel(location.IdHotel); break;

                        }
                    }
                    if (location != null)
                        list = BillingController.GetCaptivePortalLogLocation(location.IdLocation, DateTime.Parse(start), DateTime.Parse(end), PageIndex, PageSize, ref PageNumber);
                    else if (hotel != null)
                        list = BillingController.GetCaptivePortalLog(hotel.IdHotel, DateTime.Parse(start), DateTime.Parse(end), PageIndex, PageSize, ref PageNumber);
                    else
                        list = BillingController.GetCaptivePortalLog(ListHoteles, DateTime.Parse(start), DateTime.Parse(end), PageIndex, PageSize, ref PageNumber);

                    response.list = new List<CaptivePortalLogResult>();
                    response.pageNumber = PageNumber;

                    foreach (CaptivePortalLogInfo l in list)
                    {
                        CaptivePortalLogResult r = new CaptivePortalLogResult();
                        r.Location = l.Location;
                        r.UserName = l.UserName;
                        r.Password = l.Password;
                        r.RadiusResponse = l.RadiusResponse;
                        r.NomadixResponse = l.NomadixResponse;
                        r.CallerID = l.CallerID;
                        r.Page = l.Page;
                        r.Date = l.Date.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss"); ;

                        response.list.Add(r);

                    }
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private ListLoyaltyResult searchLoyalty(HttpContext context)
        {
            ListLoyaltyResult response = new ListLoyaltyResult();
            try
            {
                int PageNumber = 0;

                //OBTENEMOS EL TIPO DE USUARIOS
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    Hotels hotel = null;
                    Locations2 location = null;
                    FDSGroups group = null;
                    SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group);

                    string start = (context.Request["s"] == null ? string.Empty : context.Request["s"].ToString());
                    string end = (context.Request["e"] == null ? string.Empty : context.Request["e"].ToString());
                    string page = (context.Request["page"] == null ? string.Empty : context.Request["page"].ToString());
                    string idmember = (context.Request["idmember"] == null ? string.Empty : context.Request["idmember"].ToString());
                    string idmembertype = (context.Request["idmembertype"] == null ? string.Empty : context.Request["idmembertype"].ToString());

                    if ((start.Equals("undefined")) || string.IsNullOrEmpty(start))
                    {
                        start = DateTime.Now.ToUniversalTime().AddDays(-1).ToString();
                        end = DateTime.Now.ToUniversalTime().ToString();

                    }
                    else
                    {
                        start = DateTime.Parse(start).AddSeconds(-siteGMT.gmtoffset).ToString();
                        end = DateTime.Parse(end).AddSeconds(-siteGMT.gmtoffset).ToString();
                    }

                    int p = 0;
                    if (Int32.TryParse(page, out p))
                        PageIndex = p;

                    List<Loyalties2> list = null;
                    string listadoHoteles = ListadoHoteles(group);
                    if (int.Parse(idmember) != 0)
                    {
                        int type = int.Parse(idmembertype);
                        switch (type)
                        {
                            case 1:
                                FDSGroups temp = BillingController.GetGroup(int.Parse(idmember));
                                listadoHoteles = ListadoHoteles(temp); break;
                            case 2: hotel = BillingController.GetHotel(int.Parse(idmember)); break;
                            case 3:
                                location = BillingController.GetLocation(int.Parse(idmember));
                                hotel = BillingController.GetHotel(location.IdHotel); break;

                        }
                    }

                    if (location != null)
                        list = BillingController.GetLoyatiesLocation(location.IdLocation, DateTime.Parse(start), DateTime.Parse(end), PageIndex, PageSize, ref PageNumber);
                    else if (hotel != null)
                        list = BillingController.GetLoyaties(hotel.IdHotel, DateTime.Parse(start), DateTime.Parse(end), PageIndex, PageSize, ref PageNumber);
                    else
                        list = BillingController.GetLoyaties(listadoHoteles, DateTime.Parse(start), DateTime.Parse(end), PageIndex, PageSize, ref PageNumber);

                    response.list = new List<LoyaltyResult>();
                    response.pageNumber = PageNumber;

                    foreach (Loyalties2 l in list)
                    {
                        LoyaltyResult r = new LoyaltyResult();
                        r.Name = l.Name;
                        r.Surname = l.Surname;
                        r.Email = l.Email;
                        r.Date = l.Date.AddSeconds(siteGMT.gmtoffset).ToString();
                        r.Survey = l.Survey.Replace("Select", " - ");
                        response.list.Add(r);
                    }
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;

        }

        private ListUserPeriodResponse searchUsersPeriod(HttpContext context)
        {
            ListUserPeriodResponse response = new ListUserPeriodResponse();
            try
            {
                int PageNumber = 0;

                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));

                    Hotels hotel = null;
                    Locations2 location = null;
                    FDSGroups group = null;
                    SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group);

                    string start = (context.Request["s"] == null ? string.Empty : context.Request["s"].ToString());
                    string end = (context.Request["e"] == null ? string.Empty : context.Request["e"].ToString());
                    string page = (context.Request["page"] == null ? string.Empty : context.Request["page"].ToString());
                    string username = (context.Request["username"] == null ? string.Empty : context.Request["username"].ToString());
                    string idmember = (context.Request["idmember"] == null ? string.Empty : context.Request["idmember"].ToString());
                    string idmembertype = (context.Request["idmembertype"] == null ? string.Empty : context.Request["idmembertype"].ToString());

                    if (string.IsNullOrEmpty(start))
                    {
                        start = "01/01/1970 00:00:00";
                        end = DateTime.Now.ToString();

                    }
                    else
                    {
                        start = DateTime.Parse(start).AddSeconds(-siteGMT.gmtoffset).ToString();
                        end = DateTime.Parse(end).AddSeconds(-siteGMT.gmtoffset).ToString();
                    }

                    int p = 0;
                    if (Int32.TryParse(page, out p))
                        PageIndex = p;

                    List<Users> list = null;
                    string ListHoteles = ListadoHoteles(group);
                    if (int.Parse(idmember) != 0)
                    {
                        int type = int.Parse(idmembertype);
                        switch (type)
                        {
                            case 1:
                                FDSGroups temp = BillingController.GetGroup(int.Parse(idmember));
                                ListHoteles = ListadoHoteles(temp); break;
                            case 2: hotel = BillingController.GetHotel(int.Parse(idmember)); break;
                            case 3:
                                location = BillingController.GetLocation(int.Parse(idmember));
                                hotel = BillingController.GetHotel(location.IdHotel); break;

                        }
                    }
                    if (location != null)
                    {
                        //FDSLocationsIdentifier locationIdentifier = BillingController.GetFDSLocationIdentifier(location.IdLocation);
                        list = BillingController.GetUsersPeriodLocation(location.IdLocation, hotel.IdHotel, DateTime.Parse(start), DateTime.Parse(end), username, PageIndex, PageSize, ref PageNumber);
                    }
                    else if (hotel != null)
                    {
                        list = BillingController.GetUsersPeriod(hotel.IdHotel, DateTime.Parse(start), DateTime.Parse(end), username, PageIndex, PageSize, ref PageNumber);
                    }
                    else
                    {
                        list = BillingController.GetUsersPeriod(ListHoteles, DateTime.Parse(start), DateTime.Parse(end), username, PageIndex, PageSize, ref PageNumber);
                    }

                    response.list = new List<UserPeriodResult>();
                    response.pageNumber = PageNumber;


                    foreach (Users u in list.OrderBy(a => a.ValidSince))
                    {
                        BillingTypes type = BillingController.ObtainBillingType(u.IdBillingType);
                        Rooms room = BillingController.GetRoom(u.IdRoom);

                        UserPeriodResult r = new UserPeriodResult();
                        r.IdUser = u.IdUser;
                        r.Room = room.Name;
                        r.UserName = u.Name;
                        r.Password = u.Password;
                        r.AccessType = type.Description;
                        r.ValidSince = u.ValidSince.AddSeconds(siteGMT.gmtoffset).ToString();
                        r.ValidTill = u.ValidTill.AddSeconds(siteGMT.gmtoffset).ToString();
                        r.TimeCredit = u.TimeCredit.ToString();

                        response.list.Add(r);
                    }
                }

            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private ListHotelsResult selectSites(HttpContext context)
        {
            ListHotelsResult response = new ListHotelsResult();
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {

                    string IdUser = (context.Request["iduser"] == null ? string.Empty : context.Request["iduser"].ToString());

                    if (string.IsNullOrEmpty(IdUser))
                        IdUser = HttpContext.Current.Request.Cookies["FDSUser"].Value;

                    //List<FDSUsersAccess> access = BillingController.GetFDSUserAccesses(Int32.Parse(IdUser));

                    //List<Hotels> list = BillingController.GetSitesUser(Int32.Parse(IdUser)); ;

                    response.list = new List<HotelResult>();

                    //foreach (Hotels f in list.OrderBy(a => a.Name))
                    //{
                    //    HotelResult r = new HotelResult();
                    //    r.Name = f.Name;
                    //    r.IdHotel = f.IdHotel;

                    //    response.list.Add(r);
                    //}

                    if (HttpContext.Current.Request.Cookies["group"] != null)
                    {
                        FDSGroups group = BillingController.GetGroup(int.Parse(HttpContext.Current.Request.Cookies["group"].Value));
                        HotelResult rp = new HotelResult();
                        rp.IdHotel = group.IdGroup;
                        rp.Name = group.Name;
                        response.list.Add(rp);
                        List<FDSGroupsMembers> members = BillingController.SelectFDSGroupsMembers(group.IdGroup);
                        foreach(FDSGroupsMembers member in members)
                        {
                            HotelResult r = new HotelResult();
                            switch (member.IdMemberType)
                            {
                                //tipo grupo
                                case 1:
                                    r.IdHotel = member.IdMember;
                                    FDSGroups gtemp = BillingController.GetGroup(member.IdMember);
                                    r.Name = string.Format("--G - {0}", gtemp.Name);
                                    break;
                                //tipo site
                                case 2:
                                    r.IdHotel = member.IdMember;
                                    Hotels htemp = BillingController.GetHotel(member.IdMember);
                                    r.Name = string.Format("--S - {0}", htemp.Name);
                                    break;
                                //tipo location
                                case 3:
                                    r.IdHotel = member.IdMember;
                                    Locations2 ltemp = BillingController.GetLocation(member.IdMember);
                                    r.Name = string.Format("----L - {0}", ltemp.Description);
                                    break;

                            }
                            response.list.Add(r);
                        }
                    }
                        
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private ListMemberResult selectMembers(HttpContext context)
        {
            ListMemberResult response = new ListMemberResult();
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {

                    string IdUser = (context.Request["iduser"] == null ? string.Empty : context.Request["iduser"].ToString());

                    if (string.IsNullOrEmpty(IdUser))
                        IdUser = HttpContext.Current.Request.Cookies["FDSUser"].Value;

                    response.list = new List<MemberResult>();

                    if (HttpContext.Current.Request.Cookies["group"] != null)
                    {
                        FDSGroups groupParent = BillingController.GetGroup(int.Parse(HttpContext.Current.Request.Cookies["parent"].Value));
                        FDSGroups group = BillingController.GetGroup(int.Parse(HttpContext.Current.Request.Cookies["group"].Value));
                        List<FDSGroupLevel> levels = new List<FDSGroupLevel>();
                        FDSGroupLevel rootLevel = new FDSGroupLevel();
                        rootLevel.idGroup = groupParent.IdGroup;
                        rootLevel.level = 0;
                        levels.Add(rootLevel);
                        
                        List<FDSGroupsMembers> members = new List<FDSGroupsMembers>();
                        List<FDSMemberGroupInfo> membersInfo = new List<FDSMemberGroupInfo>();
                        FDSGroupsMembers root = new FDSGroupsMembers();
                        root.IdGroup = groupParent.IdGroup;
                        FDSMemberGroupInfo rootInfo = new FDSMemberGroupInfo();
                        Wifi360.Data.Common.Util.Map(root, rootInfo);
                        rootInfo.Name = groupParent.Name;
                        members.Add(root);
                        membersInfo.Add(rootInfo);
                        List<FDSGroupsMembers> membersRoot = BillingController.SelectFDSGroupsMembers(groupParent.IdGroup);
                        int level = 1;
                        int idlevel = groupParent.IdGroup;
                        for (int i = 0; i < membersRoot.Count; i++)
                        {
                            FDSGroupsMembers child = new FDSGroupsMembers();
                            child.IdGroup = membersRoot[i].IdGroup;
                            child.IdMember = membersRoot[i].IdMember;
                            child.IdMemberType = membersRoot[i].IdMemberType;
                            child.Id = membersRoot[i].Id;
                            FDSMemberGroupInfo childInfo = new FDSMemberGroupInfo();
                            Wifi360.Data.Common.Util.Map(child, childInfo);
                            members.Add(child);

                            switch (membersRoot[i].IdMemberType)
                            {
                                //location:
                                case 1:
                                    childInfo.Name = BillingController.GetGroup(child.IdMember).Name;
                                    List<FDSGroupsMembers> listchildgroup = BillingController.SelectFDSGroupsMembers(membersRoot[i].IdMember);
                                    FDSGroupLevel auxLevel = new FDSGroupLevel();
                                    FDSGroupLevel tempLevel = (from t in levels where t.idGroup.Equals(child.IdGroup) select t).FirstOrDefault();
                                    if (tempLevel != null)
                                        auxLevel.level = tempLevel.level + 1;
                                    else {
                                        auxLevel.level = level;
                                    }
                                    auxLevel.idGroup = child.IdMember;
                                    levels.Add(auxLevel);
                                    foreach (FDSGroupsMembers aux in listchildgroup)
                                    {
                                        membersRoot.Add(aux);
                                    }
                            
                                    break;
                                case 2:
                                    childInfo.Name = BillingController.GetHotel(child.IdMember).Name; break;
                                case 3:
                                    childInfo.Name = BillingController.GetLocation(child.IdMember).Description; break;
                            }

                            membersInfo.Add(childInfo);
                        }

                        for (int i = 0; i < membersInfo.Count(); i++)
                        {
                            FDSMemberGroupInfo m = membersInfo[i];
                            MemberResult rs = new MemberResult();
                            rs.Id = m.IdGroup;
                            rs.IdMembertype = "1";
                            rs.Name = groupParent.Name;
                            response.list.Add(rs);
                            membersInfo.Remove(m);
                            List<FDSMemberGroupInfo> mgs = (from t in membersInfo where t.IdGroup.Equals(m.IdGroup) && !t.IdMember.Equals(0) select t).ToList();

                            for (int j = 0; j < mgs.Count(); j++)
                            {
                                MemberResult mresult = new MemberResult();

                                FDSMemberGroupInfo aux = mgs[j];
                                mresult.Id = aux.IdMember;
                                mresult.IdMembertype = aux.IdMemberType.ToString();
                                mresult.Name = aux.IdMember.ToString();
                                mresult.IdParent = aux.IdGroup;

                                FDSGroupLevel levelInfo = (from t in levels where t.idGroup.Equals(aux.IdGroup) select t).FirstOrDefault();
                                string prefix = "--";
                                if (levelInfo != null)
                                    prefix = prefix.PadLeft(2 + levelInfo.level * 2, '-');
                                switch (aux.IdMemberType)
                                {
                                    case 1: mresult.Name = string.Format("{1}> (G) - {0}", aux.Name, prefix); break;
                                    case 2: mresult.Name = string.Format("{1}> (S) - {0}", aux.Name, prefix); break;
                                    case 3: mresult.Name = string.Format("{1}> (L) - {0}", aux.Name, prefix); break;
                                }

                                response.list.Add(mresult);
                                membersInfo.Remove(aux);
                                FDSMemberGroupInfo auxDelete = (from t in membersInfo where t.Id.Equals(aux.Id) select t).FirstOrDefault();
                                membersInfo.Remove(auxDelete);
                                if (aux.IdMemberType.Equals(1))
                                {
                                    int k = 1;
                                    List<FDSGroupsMembers> childs = BillingController.SelectFDSGroupsMembers(aux.IdMember);
                                    foreach (FDSGroupsMembers childAux in childs)
                                    {
                                        FDSMemberGroupInfo childAuxInfo = new FDSMemberGroupInfo();
                                        Wifi360.Data.Common.Util.Map(childAux, childAuxInfo);
                                        switch (childAux.IdMemberType)
                                        {
                                            case 1:
                                                childAuxInfo.Name = BillingController.GetGroup(childAux.IdMember).Name;
                                                break;
                                            case 2:
                                                childAuxInfo.Name = BillingController.GetHotel(childAux.IdMember).Name; break;
                                            case 3:
                                                childAuxInfo.Name = BillingController.GetLocation(childAux.IdMember).Description; break;
                                        }
                                        mgs.Insert(j + k, childAuxInfo);
                                        k++;
                                    }
                                }

                            }
                        }

                    }
                    else if (HttpContext.Current.Request.Cookies["site"] != null)
                    {
                        Hotels hotel = BillingController.GetHotel(Int32.Parse(HttpContext.Current.Request.Cookies["site"].Value.ToString()));
                        List<FDSLocationsIdentifier> locations = BillingController.GetFDSLocationsIdentifiers(hotel.IdHotel);
                        if (locations.Count > 0)
                        {
                            MemberResult r = new MemberResult();
                            r.IdMembertype = "2";
                            r.Name = string.Format("{0}", hotel.Name);
                            r.Id = hotel.IdHotel;
                            response.list.Add(r);
                            foreach(FDSLocationsIdentifier identifier in locations)
                            {
                                Locations2 l = BillingController.GetLocation(identifier.IdLocation);
                                MemberResult rl = new MemberResult();
                                rl.IdMembertype = "3";
                                rl.Name = string.Format("(L) --> {0}", l.Description);
                                rl.Id = identifier.IdLocation;

                                response.list.Add(rl);
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private ListLocationResult selectLocationsBrowse(HttpContext context)
        {
            ListLocationResult response = new ListLocationResult();
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {

                    string site = (HttpContext.Current.Request.Cookies["site"].Value);
                    string IdUser = HttpContext.Current.Request.Cookies["FDSUser"].Value;

                    List<FDSLocationsIdentifier> locations = BillingController.GetFDSLocationsIdentifiers(Int32.Parse(site));
                    
                    List<FDSUsersAccess> access = BillingController.GetFDSUserAccesses(Int32.Parse(IdUser));

                    List<Locations2> list = new List<Locations2>();

                    foreach (FDSUsersAccess acc in access)
                    {
                        List<FDSSitesGroups> sites = BillingController.GetFDSSitesGroup(acc.IdGroup);

                        foreach (FDSLocationsIdentifier location in locations)
                        {
                            Locations2 l = BillingController.GetLocation(location.IdLocation);
                            list.Add(l);
                        }

                    }

                    response.list = new List<LocationResult>();

                    foreach (Locations2 f in list.OrderBy(a => a.Description))
                    {
                        LocationResult r = new LocationResult();
                        r.Description = f.Description;
                        r.Idlocation = f.IdLocation;

                        response.list.Add(r);
                    }
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private ListSurveyResult selectSurvey(HttpContext context)
        {
            ListSurveyResult response = new ListSurveyResult();
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    //if (HttpContext.Current.Request.Cookies["site"] != null)
                    //    fdsUser.IdHotel = Int32.Parse(HttpContext.Current.Request.Cookies["site"].Value);

                    //Obtenemos IdSite -> desde sitesetting
                    string idsite = (context.Request["idsite"] == null ? string.Empty : context.Request["idsite"].ToString());

                    int id = 0;
                    List<Surveys2> list = null;
                    if (Int32.TryParse(idsite, out id)){
                        list = BillingController.GetSurveys(id);
                    }
                    else
                    {
                        list = BillingController.GetSurveys(Int32.Parse(HttpContext.Current.Request.Cookies["site"].Value));
                    }
                    
                    response.list = new List<SurveyResult>();

                    foreach (Surveys2 obj in list.OrderBy(a => a.IdLanguage).OrderBy(b => b.Order))
                    {
                        Languages lang = BillingController.GetLanguage(obj.IdLanguage);
                        SurveyResult r = new SurveyResult();
                        r.IdSurvey = obj.IdSurvey;
                        r.IdHotel = obj.IdHotel;
                        r.IdLanguage = obj.IdLanguage;
                        r.Language = lang.Name;
                        r.Question = obj.Question;
                        string[] cadenas = obj.Type.Split('-');
                        r.Type = cadenas[0].Trim();
                        if (cadenas.Length.Equals(1))
                            r.Values = string.Empty;
                        else
                            r.Values = cadenas[1];
                        r.Order = obj.Order;

                        response.list.Add(r);
                    }
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private HotelResult sitesetting(HttpContext context)
        {
            HotelResult response = new HotelResult();
            try
            {
                //OBTENEMOS EL TIPO DE USUARIOS
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));

                    string id = (context.Request["id"] == null ? string.Empty : context.Request["id"].ToString());
                                        
                    Hotels hotel = null;

                    int idHotel = 0;
                    if (Int32.TryParse(id,out idHotel))
                    {
                        hotel = BillingController.GetHotel(idHotel);
                    }
                    else
                    {                     
                        if (HttpContext.Current.Request.Cookies["site"] != null)
                            hotel = BillingController.GetHotel(Int32.Parse(HttpContext.Current.Request.Cookies["site"].Value));
                        else if (HttpContext.Current.Request.Cookies["location"] != null)
                        {
                            Locations2 location = BillingController.GetLocation(Int32.Parse(HttpContext.Current.Request.Cookies["location"].Value));
                            hotel = BillingController.GetHotel(location.IdHotel);

                        }

                    }
                    
                    if (hotel != null)
                    {
                        List<ParametrosConfiguracion> parametros = BillingController.GetParametrosConfiguracion(hotel.IdHotel);
                        foreach (ParametrosConfiguracion obj in parametros)
                        {
                            switch (obj.Key.ToUpper())
                            {
                                case "LONGUSERNAME": response.longusername = Int32.Parse(obj.value); break;
                                case "LONGPASSWORD": response.longpassword = Int32.Parse(obj.value); break;
                                case "USERNAMEPREFIX": response.prefix = obj.value; break;
                                case "SITEPASSENABLE": response.EnabledDaylyPass = Boolean.Parse(obj.value); break;
                                case "SITEPASSVALUE": response.valueDaylyPass = obj.value; break;
                                case "LONGVOUCHERUSER": response.longvoucherusername = Int32.Parse(obj.value); break;
                                case "VOUCHERPREFIX": response.voucherprefix = obj.value; break;
                                case "VOLUMECOMBINED": response.volumecombined = Boolean.Parse(obj.value); break;
                            }
                        }


                        List<SocialNetworks> socialNetworks = BillingController.GetSocialNetwoks();
                        List<SitesSocialNetworks> siteSN = BillingController.GetSiteSocialNetwoks(hotel.IdHotel);
                        response.redes = new List<SocialNetworkResult>();
                        foreach (SitesSocialNetworks sSN in siteSN)
                        {
                            SocialNetworks sn = (from t in socialNetworks where t.IdSocialNetwork.Equals(sSN.IdSocialNetwork) select t).FirstOrDefault();
                            SocialNetworkResult rsn = new SocialNetworkResult();
                            rsn.IdSocialNetwork = sSN.IdSiteSocialNetwork;
                            rsn.Name = sn.Name;
                            rsn.Active = sSN.Active;

                            response.redes.Add(rsn);
                        }

                        Currencies2 currency = BillingController.Currency(hotel.CurrencyCode);
                        response.IdHotel = hotel.IdHotel;
                        response.BandWidth = hotel.Bandwidth.ToString();
                        response.BandwidthUp = hotel.BandwidthUp.ToString();
                        response.CurrencyCode = hotel.CurrencyCode;
                        response.CurrencySymbol = currency.Symbol;
                        response.GMT = hotel.GMT.ToString();
                        response.MailFrom = hotel.MailFrom;
                        response.Name = hotel.Name;
                        response.NSE = hotel.NSE;
                        response.PayPalUser = hotel.PayPalUser;
                        response.PrincipalNSEIP = hotel.PrincipalNSEIP;
                        response.SMTP = hotel.SMTP;
                        response.SmtpUser = hotel.SmtpUser;
                        response.SmtpPassword = hotel.SmtpPassword;
                        response.SmtpPort = hotel.SmtpPort;
                        response.SmtpSSL = hotel.SmtpSSL;
                        response.UrlHotel = hotel.UrlHotel;
                        response.MacBlocking = hotel.MACBlocking;
                        response.macauthenticate = hotel.MacAuthenticate;
                        response.MACAttempsInterval = hotel.MACAttempsInterval.ToString();
                        response.MacBlockingInterval = hotel.MACBannedInterval.ToString();
                        response.MaxMacAttemps = hotel.MaxMACAttemps.ToString();
                        response.FreeAccessRepeatTime = hotel.FreeAccessRepeatTime.ToString();
                        response.EnabledFasTicket = hotel.EnabledFastTicket;
                        response.UrlReturnPaypal = hotel.UrlReturnPaypal;
                        response.FreeAccessModule = hotel.FreeAccessModule;
                        response.PayAccessModule = hotel.PayAccessModule;
                        response.SocialNetworksModule = hotel.SocialNetworksModule;
                        response.CustomAccessModule = hotel.CustomAccessModule;
                        response.VoucherAccessModule = hotel.VoucherModule;
                        response.IdLanguageDefault = hotel.IdLanguage;
                        response.Survey = hotel.Survey;
                        response.LoginModule = hotel.LoginModule;
                        response.PrintLogo = hotel.PrintLogo;
                        response.PrintVolume = hotel.PrintVolume;
                        response.PrintSpeed = hotel.PrintSpeed;
                        response.vendor = hotel.IdVendor;
                        response.filtercontents = hotel.FilterContents;
                        response.latitude = hotel.Latitude;
                        response.longitude = hotel.Longitude;
                    }
                }

            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private void sitesettingsave(HttpContext context)
        {
            string json = string.Empty;
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    //if (HttpContext.Current.Request.Cookies["site"] != null)
                    //    fdsUser.IdHotel = Int32.Parse(HttpContext.Current.Request.Cookies["site"].Value);

                    string name = (context.Request["name"] == null ? string.Empty : context.Request["name"].ToString());
                    string nse = (context.Request["nse"] == null ? string.Empty : context.Request["nse"].ToString());
                    string nseprincipalip = (context.Request["nseprincipalip"] == null ? string.Empty : context.Request["nseprincipalip"].ToString());
                    string bandwidth = (context.Request["bandwidth"] == null ? string.Empty : context.Request["bandwidth"].ToString());
                    string bandwidthup = (context.Request["bandwidthup"] == null ? string.Empty : context.Request["bandwidthup"].ToString());
                    string paypaluser = (context.Request["paypaluser"] == null ? string.Empty : context.Request["paypaluser"].ToString());
                    string currencycode = (context.Request["currencycode"] == null ? string.Empty : context.Request["currencycode"].ToString());
                    string mailfrom = (context.Request["mailfrom"] == null ? string.Empty : context.Request["mailfrom"].ToString());
                    string smtp = (context.Request["smtp"] == null ? string.Empty : context.Request["smtp"].ToString());
                    string smtpport = (context.Request["smtpport"] == null ? string.Empty : context.Request["smtpport"].ToString());
                    string smtpuser = (context.Request["smtpuser"] == null ? string.Empty : context.Request["smtpuser"].ToString());
                    string smtppassword = (context.Request["smtppassword"] == null ? string.Empty : context.Request["smtppassword"].ToString());
                    string smtpssl = (context.Request["smtpssl"] == null ? string.Empty : context.Request["smtpssl"].ToString());
                    string gmt = (context.Request["gmt"] == null ? string.Empty : context.Request["gmt"].ToString());
                    string urlhotel = (context.Request["urlhotel"] == null ? string.Empty : context.Request["urlhotel"].ToString());
                    string macblocking = (context.Request["macblocking"] == null ? string.Empty : context.Request["macblocking"].ToString());
                    string macbannedinterval = (context.Request["macbannedinterval"] == null ? string.Empty : context.Request["macbannedinterval"].ToString());
                    string maxmacattemps = (context.Request["maxmacattemps"] == null ? string.Empty : context.Request["maxmacattemps"].ToString());
                    string macattempsinterval = (context.Request["macattempsinterval"] == null ? string.Empty : context.Request["macattempsinterval"].ToString());
                    string freeaccessrepeattime = (context.Request["freeaccessrepeattime"] == null ? string.Empty : context.Request["freeaccessrepeattime"].ToString());
                    string enabledfastticket = (context.Request["enabledfastticket"] == null ? string.Empty : context.Request["enabledfastticket"].ToString());
                    string urlreturnpaypal = (context.Request["urlreturnpaypal"] == null ? string.Empty : context.Request["urlreturnpaypal"].ToString());
                    string freeaccessmodule = (context.Request["freeaccessmodule"] == null ? string.Empty : context.Request["freeaccessmodule"].ToString());
                    string payaccessmodule = (context.Request["payaccessmodule"] == null ? string.Empty : context.Request["payaccessmodule"].ToString());
                    string socialnetworkmodule = (context.Request["socialnetworkmodule"] == null ? string.Empty : context.Request["socialnetworkmodule"].ToString());
                    string customaccessmodule = (context.Request["customaccessmodule"] == null ? string.Empty : context.Request["customaccessmodule"].ToString());
                    string idlanguagedefault = (context.Request["idlanguagedefault"] == null ? string.Empty : context.Request["idlanguagedefault"].ToString());
                    string loginmodule = (context.Request["loginmodule"] == null ? string.Empty : context.Request["loginmodule"].ToString());
                    string survey = (context.Request["survey"] == null ? string.Empty : context.Request["survey"].ToString());
                    string printlogo = (context.Request["printlogo"] == null ? string.Empty : context.Request["printlogo"].ToString());
                    string printvolume = (context.Request["printvolume"] == null ? string.Empty : context.Request["printvolume"].ToString());
                    string printspeed = (context.Request["printspeed"] == null ? string.Empty : context.Request["printspeed"].ToString());
                    string enabledmassiveticket = (context.Request["enabledmassiveticket"] == null ? string.Empty : context.Request["enabledmassiveticket"].ToString());
                    string prefix = (context.Request["prefix"] == null ? string.Empty : context.Request["prefix"].ToString());
                    string longusername = (context.Request["longusername"] == null ? string.Empty : context.Request["longusername"].ToString());
                    string longpassword = (context.Request["longpassword"] == null ? string.Empty : context.Request["longpassword"].ToString());
                    string enableddaylypass = (context.Request["enableddaylypass"] == null ? string.Empty : context.Request["enableddaylypass"].ToString());
                    string valuedaylypass = (context.Request["valuedaylypass"] == null ? string.Empty : context.Request["valuedaylypass"].ToString());

                    Hotels h = BillingController.GetHotel(Int32.Parse(HttpContext.Current.Request.Cookies["site"].Value));

                    h.Name = name;
                    h.NSE = nse;
                    h.PrincipalNSEIP = nseprincipalip;
                    h.Bandwidth = Int64.Parse(bandwidth);
                    h.BandwidthUp = Int64.Parse(bandwidthup);
                    h.PayPalUser = paypaluser;
                    h.CurrencyCode = currencycode;
                    h.MailFrom = mailfrom;
                    h.SMTP = smtp;
                    h.SmtpPassword = smtppassword;
                    h.SmtpPort = smtpport;
                    h.SmtpSSL = Boolean.Parse(smtpssl);
                    h.SmtpUser = smtpuser;
                    h.UrlHotel = urlhotel;
                    h.GMT = Int32.Parse(gmt);
                    h.MACBlocking = Boolean.Parse(macblocking);
                    h.MACAttempsInterval = Int32.Parse(macattempsinterval);
                    h.MACBannedInterval = Int32.Parse(macbannedinterval);
                    h.MaxMACAttemps = Int32.Parse(maxmacattemps);
                    h.FreeAccessRepeatTime = Int32.Parse(freeaccessrepeattime);
                    h.EnabledFastTicket = Boolean.Parse(enabledfastticket);
                    h.PrintLogo = Boolean.Parse(printlogo);
                    h.PrintVolume = Boolean.Parse(printvolume);
                    h.PrintSpeed = Boolean.Parse(printspeed);
                    h.UrlReturnPaypal = urlreturnpaypal;
                    h.FreeAccessModule = Boolean.Parse(freeaccessmodule);
                    h.PayAccessModule = Boolean.Parse(payaccessmodule);
                    h.SocialNetworksModule = Boolean.Parse(socialnetworkmodule);
                    h.CustomAccessModule = Boolean.Parse(customaccessmodule);
                    h.LoginModule = Boolean.Parse(loginmodule);
                    h.IdLanguage = Int32.Parse(idlanguagedefault);
                    h.Survey = Boolean.Parse(survey);
                    h.EnabledMassiveTicket = Boolean.Parse(enabledmassiveticket);

                    BillingController.SaveHotel(h);

                    List<ParametrosConfiguracion> parametros = BillingController.GetParametrosConfiguracion(Int32.Parse(HttpContext.Current.Request.Cookies["site"].Value));
                    foreach (ParametrosConfiguracion obj in parametros)
                    {
                        switch (obj.Key.ToUpper())
                        {
                            case "LONGUSERNAME": obj.value = longusername; BillingController.SaveParametroConfiguracion(obj); break;
                            case "LONGPASSWORD": obj.value = longpassword; BillingController.SaveParametroConfiguracion(obj); break;
                            case "USERNAMEPREFIX": obj.value = prefix; BillingController.SaveParametroConfiguracion(obj); break;
                            case "DAYPASSENABLE": obj.value = enableddaylypass; BillingController.SaveParametroConfiguracion(obj); break;
                            case "DAYPASSVALUE": obj.value = valuedaylypass; BillingController.SaveParametroConfiguracion(obj); break;
                        }
                    }

                    json = "{\"code\":\"OK\",\"message\":\"Settings save successfully\"}";
                }
                else

                    json = "{\"code\":\"ERROR\",\"message\":\"Error, please try again later.\"}";
            }
            catch (Exception ex)
            {
                json = "{\"code\":\"ERROR\",\"message\":\"" + ex.Message + ".\"}";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();
        }

        private SurveyResult surveyQuestion(HttpContext context)
        {
            SurveyResult response = new SurveyResult();
            try
            {
                string id = (context.Request["id"] == null ? string.Empty : context.Request["id"].ToString());

                Surveys2 obj = BillingController.GetSurvey(Int32.Parse(id));

                response.Question = obj.Question;
                response.Order = obj.Order;
                string[] cadenas = obj.Type.Split('-');
                response.Type = cadenas[0].Trim();
                if (cadenas.Length.Equals(1))
                    response.Values = string.Empty;
                else
                    response.Values = cadenas[1];
                response.IdLanguage = obj.IdLanguage;
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private UserManagementResult user(HttpContext context)
        {
            UserManagementResult response = new UserManagementResult();
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {

                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    Locations2 location = null;
                    Hotels hotel = null;
                    FDSGroups group = null;
                    SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group);

                    string idUser = (context.Request["id"] == null ? string.Empty : context.Request["id"].ToString());

                    UserAll u = BillingController.GetRadiusUserAll(Int32.Parse(idUser));

                    response.UserName = u.Name;
                    response.Password = u.Password;
                    response.Room = u.RoomName;
                    response.AccessType = u.BillingType;
                    response.ValidSince = u.ValidSince.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm");
                    response.ValidTill = u.ValidTill.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm");
                    response.TimeCredit = u.TimeCredit.ToString();
                    response.IdUser = u.IdUser.ToString();
                    response.IdPriority = u.Priority;
                    response.Priority = u.PriorityName;
                    response.BWDown = u.BWDown.ToString();
                    response.BWUp = u.BWUp.ToString();
                    response.VolumeUp = (u.VolumeUp / (1024 * 1024)).ToString();
                    response.VolumeDown = (u.VolumeDown / (1024 * 1024)).ToString();
                    response.MaxDevices = u.MaxDevices.ToString();
                    response.Comment = u.Comment;
                    response.filterid = u.FilterId;

                    List<UserAll> users = BillingController.GetRadiusUsers(u.IdHotel, u.Name, u.ValidSince);
                    foreach(UserAll other in users)
                    {
                        if (other.ValidTill > u.ValidTill)
                            response.ValidTill = other.ValidTill.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm");
                    }

                    List<Accounting> actividad = BillingController.Accounting(u.Name, u.ValidSince);

                    var macs = (from a in actividad select a.CallerID).Distinct();
                    response.macs = new List<string>();
                    foreach (var mac in macs)
                    {
                        string m = mac;
                        response.macs.Add(m);
                    }

                    var actSessions = (from a in actividad select a.AcctSessionId).Distinct();
                    Int64 bytesIn = 0;
                    Int64 bytesOut = 0;
                    Int32 seconds = 0;
                    foreach (var act in actSessions)
                    {
                        var s = (from a in actividad where a.AcctSessionId.Equals(act) select a.SessionTime).Max();
                        seconds += Int32.Parse(s.ToString());
                        var i = (from a in actividad where a.AcctSessionId.Equals(act) select a.BytesIn).Max();
                        bytesIn += Int64.Parse(i.ToString());
                        var o = (from a in actividad where a.AcctSessionId.Equals(act) select a.BytesOut).Max();
                        bytesOut += Int64.Parse(o.ToString());
                    }

                    response.TotalTime = seconds.ToString();
                    response.TotalBytesIn = bytesIn.ToString();
                    response.TotalBytesOut = bytesOut.ToString();

                    TimeSpan t = TimeSpan.FromSeconds(u.TimeCredit / u.MaxDevices);
                    string time2 = string.Empty;
                    if (u.TimeCredit >= 86400)
                        time2 = string.Format("{0}d : {1:D2}h : {2:D2}m : {3:D2}s", t.Days, t.Hours, t.Minutes, t.Seconds);
                    else
                        time2 = string.Format("{0:D2}h : {1:D2}m: {2:D2}s", t.Hours, t.Minutes, t.Seconds);
                    response.TimeCreditFormatted = time2;

                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private void userdelete(HttpContext context)
        {
            string json = string.Empty;
            try
            {

                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    
                    string idUser = (context.Request["id"] == null ? string.Empty : context.Request["id"].ToString());

                    Users u = BillingController.GetRadiusUser(Int32.Parse(idUser));
                    u.Enabled = false;

                    if (BillingController.UpdateUser(u))
                        json = "{\"code\":\"OK\",\"message\":\"0x0003\"}";
                    else
                        json = "{\"code\":\"ERROR\",\"message\":\"0x9999\"}";
                }
            }
            catch
            {
                json = "{\"code\":\"ERROR\",\"message\":\"0x9999\"}";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();


        }
        
        private UserManagementResult userdetails(HttpContext context)
        {
            UserManagementResult response = new UserManagementResult();
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));

                    Hotels hotel = null;
                    Locations2 location = null;
                    SitesGMT siteGMT = GetSiteGMt(out location, out hotel);

                    string idUser = (context.Request["id"] == null ? string.Empty : context.Request["id"].ToString());

                    Users u = BillingController.GetRadiusUser(Int32.Parse(idUser));

                    BillingTypes billigType = BillingController.ObtainBillingType(u.IdBillingType);
                    Rooms room = BillingController.GetRoom(u.IdRoom);

                    response.UserName = u.Name;
                    response.Password = u.Password;
                    response.Room = room.Name;
                    response.AccessType = billigType.Description;
                    response.ValidSince = u.ValidSince.AddSeconds(siteGMT.gmtoffset).ToString();
                    response.ValidTill = u.ValidTill.AddSeconds(siteGMT.gmtoffset).ToString();
                    response.TimeCredit = u.TimeCredit.ToString();
                    response.IdUser = u.IdUser.ToString();
                    response.Priority = BillingController.GetPriority(u.Priority).Description;
                    response.BWDown = u.BWDown.ToString(); // billigType.BWDown.ToString();
                    response.BWUp = u.BWUp.ToString(); // billigType.BWUp.ToString();
                    response.VolumeUp = (u.VolumeUp / (1024 * 1024)).ToString();
                    response.VolumeDown = (u.VolumeDown / (1024 * 1024)).ToString();
                    response.MaxDevices = u.MaxDevices.ToString(); // billigType.MaxDevices.ToString();

                    List<ClosedSessions2> actividad = BillingController.GetClosedSession(u.IdHotel, u.Name, u.ValidSince, u.ValidTill);
                    response.TotalBytesIn = actividad.Sum(a => a.BytesIn.Value).ToString();
                    response.TotalBytesOut = actividad.Sum(a => a.BytesOut.Value).ToString();
                    response.TotalMacs = actividad.Select(a => a.CallerID).Distinct().Count().ToString();
                  //  response.filterid = u.Filter_Id;
            //        response.filterdescription = BillingController.GetFilteringProfile(u.Filter_Id).Description;

                    TimeSpan t = new TimeSpan();
                    foreach (ClosedSessions2 a in actividad)
                    {
                        t += a.SessionTerminated.Value - a.SessionStarted.Value;

                    }

                    response.TotalTime = t.TotalSeconds.ToString();
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private ListUserManagementResult userManagement(HttpContext context)
        {
            ListUserManagementResult response = new ListUserManagementResult();
            try
            {
                int PageNumber = 0;

                //OBTENEMOS EL TIPO DE USUARIOS
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));

                    Hotels hotel = null;
                    Locations2 location = null;
                    FDSGroups group = null;
                    SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group);

                    string page = (context.Request["page"] == null ? string.Empty : context.Request["page"].ToString());
                    string username = (context.Request["username"] == null ? string.Empty : context.Request["username"].ToString());
                    string start = (context.Request["start"] == null ? string.Empty : context.Request["start"].ToString());
                    string end = (context.Request["end"] == null ? string.Empty : context.Request["end"].ToString());
                    string billing = (context.Request["billing"] == null ? string.Empty : context.Request["billing"].ToString());
                    string room = (context.Request["room"] == null ? string.Empty : context.Request["room"].ToString());
                    string prioriti = (context.Request["priority"] == null ? string.Empty : context.Request["priority"].ToString());
                    string filterprofile = (context.Request["filterprofile"] == null ? string.Empty : context.Request["filterprofile"].ToString());
                    string iotfilter = (context.Request["iot"] == null ? string.Empty : context.Request["iot"].ToString());
                    string idmember = (context.Request["idmember"] == null ? string.Empty : context.Request["idmember"].ToString());
                    string idmembertype = (context.Request["idmembertype"] == null ? string.Empty : context.Request["idmembertype"].ToString());
                    string idfdsuser = (context.Request["idfdsuser"] == null ? string.Empty : context.Request["idfdsuser"].ToString());

                    int p = 0;
                    if (Int32.TryParse(page, out p))
                        PageIndex = p;

                    DateTime startfilter = (string.IsNullOrEmpty(start)) ? DateTime.MaxValue : DateTime.Parse(start);
                    DateTime endfilter = (string.IsNullOrEmpty(end)) ? DateTime.MaxValue : DateTime.Parse(end);
                    int roomFilter = (string.IsNullOrEmpty(room)) ? 0 : Int32.Parse(room);
                    int billingFilter = (string.IsNullOrEmpty(billing)) ? 0 : Int32.Parse(billing);
                    int priorityFilter = (string.IsNullOrEmpty(prioriti)) ? 0 : Int32.Parse(prioriti);
                    int filterprofileFilter = (string.IsNullOrEmpty(filterprofile)) ? 0 : Int32.Parse(filterprofile);
                    int? iot = null;
                    int idfdsuserfilter = (string.IsNullOrEmpty(idfdsuser)) ? 0 : Int32.Parse(idfdsuser);

                    if (fdsUser.IdRol.Equals(3))
                        idfdsuserfilter = fdsUser.IdUser;

                    switch (iotfilter)
                    {
                        case "1": iot = 0; break;
                        case "2": iot = 1; break;
                        default: iot = null; break;
                    }

                    List<UserInfo> list = null;

                    string listadosHoteles = ListadoHoteles(group);
                    List<Wifi360.Data.Model.Priorities> priorities = null; 
                    
                    List<FilteringProfiles> profiles = null;
                    if (int.Parse(idmember) != 0)
                    {
                        int type = int.Parse(idmembertype);
                        switch(type)
                        {
                            case 1: FDSGroups temp = BillingController.GetGroup(int.Parse(idmember));
                                listadosHoteles = ListadoHoteles(temp); break;
                            case 2: hotel = BillingController.GetHotel(int.Parse(idmember));break;
                            case 3: location = BillingController.GetLocation(int.Parse(idmember));
                                hotel = BillingController.GetHotel(location.IdHotel); break;

                        }
                    }

                    FDSUsersAccess fdsuserAccess = BillingController.GetFDSUserAccess(fdsUser.IdUser);
                    List<FDSUsers> listFDSUser = null;
                    switch (fdsUser.IdType)
                    {
                        case 2: listFDSUser = BillingController.GetFDSUsersSite(fdsuserAccess.IdSite); break;
                        case 3: listFDSUser = BillingController.GetFDSUsersLocation(fdsuserAccess.IdLocation); break;
                        case 1: listFDSUser = BillingController.GetFDSUsers(); break;
                    }

                    if (location != null)
                    {
                        list = BillingController.GetActiveUsersLocation(location.IdLocation, listFDSUser, username, startfilter, endfilter, billingFilter, roomFilter, priorityFilter, filterprofileFilter, iot, idfdsuserfilter, PageIndex, PageSize, ref PageNumber);
                        priorities = BillingController.GetPriorities(hotel.IdHotel);
                        profiles = BillingController.GetFilteringProfiles(hotel.IdHotel);
                    }
                    else if (hotel != null)
                    {
                        list = BillingController.GetActiveUsers(hotel.IdHotel, listFDSUser, username, startfilter, endfilter, billingFilter, roomFilter, priorityFilter, filterprofileFilter, iot, idfdsuserfilter, PageIndex, PageSize, ref PageNumber);
                        priorities = BillingController.GetPriorities(hotel.IdHotel);
                        profiles = BillingController.GetFilteringProfiles(hotel.IdHotel);
                    }
                    else
                    {
                        list = BillingController.GetActiveUsers(listadosHoteles, username, startfilter, endfilter, billingFilter, roomFilter, priorityFilter, filterprofileFilter, iot, PageIndex, PageSize, ref PageNumber);
                        priorities = BillingController.GetPriorities(listadosHoteles);
                        profiles = BillingController.GetFilteringProfiles(listadosHoteles);
                    }

                    response.list = new List<UserManagementResult>();
                    response.pageNumber = PageNumber;

                    List<FDSUsers> fdsusers = BillingController.GetFDSUsers();

                    foreach (UserInfo u in list.OrderBy(a => a.ValidSince))
                    {
                        if (HttpContext.Current.Request.Cookies["location"] != null)
                        {
                            if (location.IdLocation.Equals(u.IdLocation))
                            {
                                UserManagementResult r = new UserManagementResult();
                                r.UserName = u.UserName;
                                r.Password = u.Password;
                                r.Room = u.Room;
                                r.AccessType = u.BillingType;
                                r.ValidSince = u.ValidSince.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                                r.ValidTill = u.ValidTill.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                                r.TimeCredit = u.TimeCredit.ToString();
                                r.IdUser = u.IdUser.ToString();
                                Wifi360.Data.Model.Priorities priority = (from t in priorities where t.IdPriority.Equals(u.IdPriority) select t).FirstOrDefault();
                                if (priority != null)
                                    r.Priority = priority.Description;
                                else
                                    r.Priority = string.Empty;

                                FilteringProfiles filterProdile = (from t in profiles where t.IdFilter.Equals(u.IdFilterProfile) select t).FirstOrDefault();
                                if (filterProdile != null)
                                    r.filterdescription = filterProdile.Description;
                                else
                                    r.filterdescription = string.Empty;
                                r.Comment = u.Comment;

                                r.idfdsuser = u.IdFDSUser;
                                FDSUsers seller = (from t in fdsusers where t.IdUser.Equals(u.IdFDSUser) select t).FirstOrDefault();
                                if (seller != null)
                                    r.fdsuser = seller.Name;
                                else
                                    r.fdsuser = string.Empty;
                                    
                                response.list.Add(r);

                            }
                        }
                        else
                        {
                            UserManagementResult r = new UserManagementResult();
                            r.UserName = u.UserName;
                            r.Password = u.Password;
                            r.Room = u.Room;
                            r.AccessType = u.BillingType;
                            r.ValidSince = u.ValidSince.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                            r.ValidTill = u.ValidTill.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                            r.TimeCredit = u.TimeCredit.ToString();
                            r.IdUser = u.IdUser.ToString();
                            Wifi360.Data.Model.Priorities priority = (from t in priorities where t.IdPriority.Equals(u.IdPriority) select t).FirstOrDefault();
                            if (priority != null)
                                r.Priority = priority.Description;
                            else
                                r.Priority = string.Empty;
                            FilteringProfiles filterProdile = (from t in profiles where t.IdFilter.Equals(u.IdFilterProfile) select t).FirstOrDefault();
                            if (filterProdile != null)
                                r.filterdescription = filterProdile.Description;
                            else
                                r.filterdescription = string.Empty;

                            r.Comment = u.Comment;

                            r.idfdsuser = u.IdFDSUser;
                            FDSUsers seller = (from t in fdsusers where t.IdUser.Equals(u.IdFDSUser) select t).FirstOrDefault();
                            if (seller != null)
                                r.fdsuser = seller.Name;
                            else
                                r.fdsuser = string.Empty;
                            response.list.Add(r);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private static string ListadoHoteles(Hotels hotel)
        {
            List<Hotels> hoteles = new List<Hotels>();
            hoteles.Add(hotel);
            List<Hotels> hijos = BillingController.GetSitesByParent(hotel.IdHotel);
            for (int i = 0; i < hijos.Count; i++)
            {
                Hotels aux = (from t in hoteles where t.IdHotel.Equals(hijos[i].IdHotel) select t).FirstOrDefault();
                if (aux == null)
                    hoteles.Add(hijos[i]);
                List<Hotels> hijosTemp = BillingController.GetSitesByParent(hijos[i].IdHotel);
                foreach (Hotels ht in hijosTemp)
                {
                    hoteles.Add(ht);
                    hijos.Add(ht);
                }
            }
            string listadosHoteles = string.Empty;
            foreach (Hotels h in hoteles)
                listadosHoteles += string.Format("{0},", h.IdHotel);
            listadosHoteles = listadosHoteles.Substring(0, listadosHoteles.Length - 1);
            return listadosHoteles;
        }

        private static string ListadoHoteles(FDSGroups group)
        {
            if (group != null)
            {
                List<Hotels> hoteles = new List<Hotels>();
                List<FDSGroupsMembers> members = BillingController.SelectFDSGroupsMembers(group.IdGroup);
                for (int i = 0; i < members.Count; i++)
                {
                    switch (members[i].IdMemberType)
                    {
                        //tipo grupo
                        case 1:
                            List<FDSGroupsMembers> auxs = BillingController.SelectFDSGroupsMembers(members[i].IdMember);
                            foreach (FDSGroupsMembers aux in auxs)
                                members.Add(aux);
                            break;
                        //tipo site
                        case 2:
                            hoteles.Add(BillingController.GetHotel(members[i].IdMember));
                            break;
                        //tipo location
                        case 3: break;

                    }
                }

                string listadosHoteles = string.Empty;
                foreach (Hotels h in hoteles)
                    listadosHoteles += string.Format("{0},", h.IdHotel);
                listadosHoteles = listadosHoteles.Substring(0, listadosHoteles.Length - 1);
                return listadosHoteles;
            }
            else
                return string.Empty;
        }

        private ListUserManagementResult userManagementIoT(HttpContext context)
        {
            ListUserManagementResult response = new ListUserManagementResult();
            try
            {
                int PageNumber = 0;

                //OBTENEMOS EL TIPO DE USUARIOS
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    Hotels hotel = null;
                    Locations2 location = null;
                    SitesGMT siteGMT = GetSiteGMt(out location, out hotel);

                    string page = (context.Request["page"] == null ? string.Empty : context.Request["page"].ToString());
                    string username = (context.Request["username"] == null ? string.Empty : context.Request["username"].ToString());
                    string start = (context.Request["start"] == null ? string.Empty : context.Request["start"].ToString());
                    string end = (context.Request["end"] == null ? string.Empty : context.Request["end"].ToString());
                    string billing = (context.Request["billing"] == null ? string.Empty : context.Request["billing"].ToString());
                    string room = (context.Request["room"] == null ? string.Empty : context.Request["room"].ToString());
                    string prioriti = (context.Request["priority"] == null ? string.Empty : context.Request["priority"].ToString());
                    string filterprofile = (context.Request["filterprofile"] == null ? string.Empty : context.Request["filterprofile"].ToString());

                    int p = 0;
                    if (Int32.TryParse(page, out p))
                        PageIndex = p;

                    DateTime date = DateTime.Now.AddHours(hotel.GMT);
                    DateTime startfilter = (string.IsNullOrEmpty(start)) ? DateTime.MaxValue : DateTime.Parse(start);
                    DateTime endfilter = (string.IsNullOrEmpty(end)) ? DateTime.MaxValue : DateTime.Parse(end);
                    int roomFilter = (string.IsNullOrEmpty(room)) ? 0 : Int32.Parse(room);
                    int billingFilter = (string.IsNullOrEmpty(billing)) ? 0 : Int32.Parse(billing);
                    int priorityFilter = (string.IsNullOrEmpty(prioriti)) ? 0 : Int32.Parse(prioriti);
                    int filterprofileFilter = (string.IsNullOrEmpty(filterprofile)) ? 0 : Int32.Parse(filterprofile);

                    List<UserInfo> list = null;

                    if (HttpContext.Current.Request.Cookies["location"] != null)
                    {
                        list = BillingController.GetActiveUsersLocationIoT(location.IdLocation, username, startfilter, endfilter, billingFilter, roomFilter, priorityFilter, filterprofileFilter, PageIndex, PageSize, ref PageNumber);
                    }
                    else
                        list = BillingController.GetActiveUsersIoT(hotel.IdHotel, username, startfilter, endfilter, billingFilter, roomFilter, priorityFilter, filterprofileFilter, PageIndex, PageSize, ref PageNumber);

                    response.list = new List<UserManagementResult>();
                    response.pageNumber = PageNumber;

                    List<Wifi360.Data.Model.Priorities> priorities = BillingController.GetPriorities(hotel.IdHotel);
                    List<FilteringProfiles> profiles = BillingController.GetFilteringProfiles(hotel.IdHotel);

                    foreach (UserInfo u in list.OrderBy(a => a.ValidSince))
                    {
                        if (HttpContext.Current.Request.Cookies["location"] != null)
                        {
                            if (location.IdLocation.Equals(u.IdLocation))
                            {
                                UserManagementResult r = new UserManagementResult();
                                r.UserName = u.UserName;
                                r.Password = u.Password;
                                r.Room = u.Room;
                                r.AccessType = u.BillingType;
                                r.ValidSince = u.ValidSince.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                                r.ValidTill = u.ValidTill.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                                r.TimeCredit = u.TimeCredit.ToString();
                                r.IdUser = u.IdUser.ToString();
                                r.Comment = u.Comment;
                                Wifi360.Data.Model.Priorities priority = (from t in priorities where t.IdPriority.Equals(u.IdPriority) select t).FirstOrDefault();
                                r.Priority = priority.Description;
                                FilteringProfiles filterProdile = (from t in profiles where t.IdFilter.Equals(u.IdFilterProfile) select t).FirstOrDefault();
                                if (filterProdile != null)
                                    r.filterdescription = filterProdile.Description;
                                else
                                    r.filterdescription = string.Empty;
                                r.Comment = u.Comment;
                                response.list.Add(r);

                            }
                        }
                        else
                        {
                            UserManagementResult r = new UserManagementResult();
                            r.UserName = u.UserName;
                            r.Password = u.Password;
                            r.Room = u.Room;
                            r.AccessType = u.BillingType;
                            r.ValidSince = u.ValidSince.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                            r.ValidTill = u.ValidTill.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                            r.TimeCredit = u.TimeCredit.ToString();
                            r.IdUser = u.IdUser.ToString();
                            r.Comment = u.Comment;
                            Wifi360.Data.Model.Priorities priority = (from t in priorities where t.IdPriority.Equals(u.IdPriority) select t).FirstOrDefault();
                            r.Priority = priority.Description;
                            FilteringProfiles filterProdile = (from t in profiles where t.IdFilter.Equals(u.IdFilterProfile) select t).FirstOrDefault();
                            if (filterProdile != null)
                                r.filterdescription = filterProdile.Description;
                            else
                                r.filterdescription = string.Empty;

                            r.Comment = u.Comment;

                            response.list.Add(r);

                        }
                    }
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private ListOnlineUserResult usersonline(HttpContext context)
        {
            ListOnlineUserResult response = new ListOnlineUserResult();
            string json = string.Empty;
            try
            {
                int PageNumber = 0;
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    Hotels hotel = null;
                    Locations2 location = null;
                    FDSGroups group = null;
                    SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group);

                    string idbillingtypefilter = (context.Request["idbillingtype"] == null ? string.Empty : context.Request["idbillingtype"].ToString());
                    string username = (context.Request["username"] == null ? string.Empty : context.Request["username"].ToString());
                    string iotfilter = (context.Request["iot"] == null ? string.Empty : context.Request["iot"].ToString());
                    string idpriorityfilter = (context.Request["idpriority"] == null ? string.Empty : context.Request["idpriority"].ToString());
                    string idfilterfilter = (context.Request["idfilter"] == null ? string.Empty : context.Request["idfilter"].ToString());
                    string page = (context.Request["page"] == null ? string.Empty : context.Request["page"].ToString());
                    string idmember = (context.Request["idmember"] == null ? string.Empty : context.Request["idmember"].ToString());
                    string idmembertype = (context.Request["idmembertype"] == null ? string.Empty : context.Request["idmembertype"].ToString());
                    string idfdsuser = (context.Request["idfdsuser"] == null ? string.Empty : context.Request["idfdsuser"].ToString());

                    int p = 0;
                    if (Int32.TryParse(page, out p))
                        PageIndex = p;
                    
                    List<UsersOnlineInfo> list = null;

                    bool? iot = null;
                    int? idbillingtype = null;
                    int? idpriority = null;
                    int? idfilter = null;
                    int idfdsuserfilter = 0;

                    switch (iotfilter) {
                        case "1": iot = false; break;
                        case "2": iot = true; break;
                        default: iot = null; break;
                    }
                    if (string.IsNullOrEmpty(idbillingtypefilter)) idbillingtypefilter = "0";
                    if (idbillingtypefilter != "0") idbillingtype = Int32.Parse(idbillingtypefilter);
                    if (idpriorityfilter != "0") idpriority = Int32.Parse(idpriorityfilter);
                    if (idfilterfilter != "0") idfilter = Int32.Parse(idfilterfilter);
                    if (!string.IsNullOrEmpty(idfdsuser)) idfdsuserfilter = int.Parse(idfdsuser);

                   // if (fdsUser.IdRol.Equals(3))
                   //     idfdsuserfilter = fdsUser.IdUser;

                    string ListHoteles = ListadoHoteles(group);
                    if (int.Parse(idmember) != 0)
                    {
                        int type = int.Parse(idmembertype);
                        switch (type)
                        {
                            case 1:
                                FDSGroups temp = BillingController.GetGroup(int.Parse(idmember));
                                ListHoteles = ListadoHoteles(temp); break;
                            case 2: hotel = BillingController.GetHotel(int.Parse(idmember)); break;
                            case 3:
                                location = BillingController.GetLocation(int.Parse(idmember));
                                hotel = BillingController.GetHotel(location.IdHotel); break;

                        }
                    }

                    FDSUsersAccess fdsuserAccess = BillingController.GetFDSUserAccess(fdsUser.IdUser);
                    List<FDSUsers> listFDSUser = null;
                    switch (fdsUser.IdType)
                    {
                        case 2: listFDSUser = BillingController.GetFDSUsersSite(fdsuserAccess.IdSite); break;
                        case 3: listFDSUser = BillingController.GetFDSUsersLocation(fdsuserAccess.IdLocation); break;
                        case 1: listFDSUser = BillingController.GetFDSUsers();break;
                    }


                    if (location != null)
                    {
                        FDSLocationsIdentifier locationIdentifier = BillingController.GetFDSLocationIdentifier(location.IdLocation);
                        list = BillingController.OnlineUsersFilter(locationIdentifier.Identifier, listFDSUser, iot, username, idbillingtype, idfdsuserfilter, PageIndex, PageSize, ref PageNumber);
                    }
                    else if (hotel != null)
                    {
                        list = BillingController.OnlineUsersFilter(hotel.IdHotel, listFDSUser, iot, username, idbillingtype, idpriority, idfilter, idfdsuserfilter, PageIndex, PageSize, ref PageNumber);
                    }
                    else { 

                        list = BillingController.OnlineUsersFilter(ListHoteles, iot, username, idbillingtype, idpriority, idfilter, PageIndex, PageSize, ref PageNumber);
                    }

                    response.list = new List<OnlineUserResult>();
                    response.pageNumber = PageNumber;
                    List<FDSUsers> fdsusers = BillingController.GetFDSUsers();
                    foreach (UsersOnlineInfo u in list)
                    {
                        OnlineUserResult r = new OnlineUserResult();

                        r.Initiated = u.SessionStarted.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                        r.UserName = u.UserName;
                        r.Room = u.Room;
                        r.AccessType = u.AccessType;
                        r.TotalDown = u.BytesIn.ToString();
                        r.TotalUp = u.BytesOut.ToString();
                        r.MAC = u.CallerID;
                        r.Comment = u.Comment;
                        r.IdUser = u.IdUser;
                        r.Filter = u.Filter;
                        r.Priority = u.Priority;

                        r.IdFDSUser = u.IdFDSUser;
                        FDSUsers fds = (from t in fdsusers where t.IdUser.Equals(u.IdFDSUser) select t).FirstOrDefault();
                        if (fds != null)
                            r.fdsuser = fds.Name;
                        else
                            r.fdsuser = string.Empty;

                        if (u.IdFDSUser.Equals(fdsUser.IdUser))
                            r.property = true;

                        response.list.Add(r);
                    }
                }
            }
            catch (Exception ex)
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;


        }

        private ListOnlineUserResult usersonlineIoT(HttpContext context)
        {
            ListOnlineUserResult response = new ListOnlineUserResult();
            string json = string.Empty;
            try
            {
                int PageNumber = 0;
                CultureInfo provider = CultureInfo.InvariantCulture;

                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    Hotels hotel = null;
                    Locations2 location = null;
                    SitesGMT siteGMT = GetSiteGMt(out location, out hotel);

                    string idroom = (context.Request["r"] == null ? string.Empty : context.Request["r"].ToString());
                    string start = (context.Request["s"] == null ? string.Empty : context.Request["s"].ToString());
                    string end = (context.Request["e"] == null ? string.Empty : context.Request["e"].ToString());
                    string user = (context.Request["u"] == null ? string.Empty : context.Request["u"].ToString());

                    string page = (context.Request["page"] == null ? string.Empty : context.Request["page"].ToString());

                    int p = 0;
                    if (Int32.TryParse(page, out p))
                        PageIndex = p;

                    List<UsersOnlineInfo> list = null;

                    if (HttpContext.Current.Request.Cookies["location"] != null)
                    {
                        FDSLocationsIdentifier locationIdentifier = BillingController.GetFDSLocationIdentifier(location.IdLocation);
                        list = BillingController.OnlineUsersIoT(locationIdentifier.Identifier, PageIndex, PageSize, ref PageNumber);
                    }
                    else
                        list = BillingController.OnlineUsersIoT(hotel.IdHotel, PageIndex, PageSize, ref PageNumber);


                    response.list = new List<OnlineUserResult>();
                    response.pageNumber = PageNumber;

                    foreach (UsersOnlineInfo u in list)
                    {
                        OnlineUserResult r = new OnlineUserResult();

                        r.Initiated = u.SessionStarted.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                        r.UserName = u.UserName;
                        r.Room = u.CallerID;
                        r.AccessType = u.Room;
                        r.TotalDown = u.BytesIn.ToString();
                        r.TotalUp = u.BytesOut.ToString();
                        r.MAC = u.AccessType;
                        r.Comment = u.Comment;

                        response.list.Add(r);
                    }
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;


        }

        private void usersave(HttpContext context)
        {
            string json = string.Empty;
            try
            {
                //OBTENEMOS EL TIPO DE USUARIOS
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    //if (HttpContext.Current.Request.Cookies["site"] != null)
                    //    fdsUser.IdHotel = Int32.Parse(HttpContext.Current.Request.Cookies["site"].Value);


                    string idUser = (context.Request["id"] == null ? string.Empty : context.Request["id"].ToString());
                    string user = (context.Request["user"] == null ? string.Empty : context.Request["user"].ToString());
                    string password = (context.Request["password"] == null ? string.Empty : context.Request["password"].ToString());

                    Users u = BillingController.GetRadiusUser(Int32.Parse(idUser));
                    Users u2 = BillingController.GetRadiusUser(user, Int32.Parse(HttpContext.Current.Request.Cookies["site"].Value));

                    if (u2.IdUser.Equals(0) || u2.IdUser.Equals(u.IdUser) || !u2.Enabled)
                    {
                        u.Name = user.ToUpper();
                        u.Password = password.ToUpper();

                        if (BillingController.UpdateUser(u))
                            json = "{\"code\":\"OK\",\"message\":\" \"}";
                        else
                            json = "{\"code\":\"ERROR\",\"message\":\"Se ha producido un error en el envio. Inténtelo más tarde.\"}";
                    }
                    else
                    {
                        json = "{\"code\":\"NO\",\"message\":\" \"}";
                    }
                }
            }
            catch
            {
                json = "{\"code\":\"ERROR\",\"message\":\"Se ha producido un error en el envio. Inténtelo más tarde.\"}";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();


        }

        private void usersave(HttpContext context, Dictionary<string, string> sData)
        {
            string json = string.Empty;
            try
            {
                //OBTENEMOS EL TIPO DE USUARIOS
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));

                    string id = sData["id"];
                    string user = sData["user"];
                    string password = sData["password"];
                    string priority = sData["priority"];
                    string filterid = sData["filterid"];
                    string validsince = sData["validsince"];
                    string validtill = sData["validtill"];
                    string comment = sData["comment"];

                    Hotels hotel= null;
                    Locations2 location = null;
                    SitesGMT siteGME = GetSiteGMt(out location, out hotel);
                    Users u = BillingController.GetRadiusUser(Int32.Parse(id));
                    Users u2 = BillingController.GetRadiusUser(user, hotel.IdHotel);


                    if (u2.IdUser.Equals(0) || u2.IdUser.Equals(u.IdUser) || !u2.Enabled)
                    {
                        u.Name = user.ToUpper();
                        u.Password = password.ToUpper();
                        u.Comment = comment;
                        u.Priority = Int32.Parse(priority);
                        u.Filter_Id = Int32.Parse(filterid);
                        u.ValidSince = DateTime.Parse(validsince).AddSeconds(-siteGME.gmtoffset);
                        u.ValidTill = DateTime.Parse(validtill).AddSeconds(-siteGME.gmtoffset);

                        if (BillingController.UpdateUser(u))
                            json = "{\"code\":\"OK\",\"message\":\"0x0004\"}";
                        else
                            json = "{\"code\":\"ERROR\",\"message\":\"0x9999\"}";
                    }
                    else
                    {
                        json = "{\"code\":\"NO\",\"message\":\"0x0005\"}";
                    }
                }
            }
            catch
            {
                json = "{\"code\":\"ERROR\",\"message\":\"0x9999\"}";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();


        }

        private void usersaveexteded(HttpContext context)
        {
            string json = string.Empty;
            try
            {
                //OBTENEMOS EL TIPO DE USUARIOS
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    //if (HttpContext.Current.Request.Cookies["site"] != null)
                    //    fdsUser.IdHotel = Int32.Parse(HttpContext.Current.Request.Cookies["site"].Value);

                    Hotels hotel = BillingController.GetHotel(Int32.Parse(HttpContext.Current.Request.Cookies["site"].Value));

                    string idUser = (context.Request["id"] == null ? string.Empty : context.Request["id"].ToString());
                    string accessType = (context.Request["accessType"] == null ? string.Empty : context.Request["accessType"].ToString());

                    Users u = BillingController.GetRadiusUser(Int32.Parse(idUser));

                    Users exitsUser = BillingController.GetRadiusUser(u.Name, hotel.IdHotel);
                    if (exitsUser.IdUser.Equals(0) || exitsUser.IdUser.Equals(u.IdUser) || !exitsUser.Enabled)
                    { 
                        BillingTypes b = BillingController.ObtainBillingType(Int32.Parse(accessType));

                        Billings billing = new Billings();
                        billing.IdHotel = u.IdHotel;
                        billing.IdRoom = u.IdRoom;
                        billing.IdBillingType = b.IdBillingType;
                        billing.Amount = b.Price;
                        billing.BillingDate = DateTime.Now.ToUniversalTime();
                        billing.BillingCharge = DateTime.Now.ToUniversalTime();
                        billing.Flag = 3;
                        billing.IdLocation = b.IdLocation;
                        if (b.IdBillingModule.Equals(2) || b.IdBillingModule.Equals(4))
                            billing.Billable = true;
                        billing.IdFDSUser = fdsUser.IdUser;
                        billing.IdUser = u.IdUser;

                        u.TimeCredit += b.TimeCredit;
                        if (u.TimeCredit < 0)
                            u.TimeCredit = Int32.MaxValue;

                        if (u.ValidTill >= DateTime.Now)
                            u.ValidTill = u.ValidTill.AddHours(b.ValidTill);
                        else
                            u.ValidTill = DateTime.Now.AddHours(b.ValidTill);

                        u.VolumeDown += b.VolumeDown;

                        if (u.VolumeDown < 0)
                            u.VolumeDown = long.MaxValue;

                        u.VolumeUp += b.VolumeUp;
                        if (u.VolumeUp < 0)
                            u.VolumeUp = long.MaxValue;

                        u.Enabled = true;

                        if (BillingController.SaveBilling(billing))
                        {
                            if (BillingController.UpdateUser(u))
                                json = "{\"code\":\"OK\",\"message\":\"0x0007\"}";
                            else
                                json = "{\"code\":\"ERROR\",\"message\":\"0x9999\"}";
                        }
                        else
                            json = "{\"code\":\"ERROR\",\"message\":\"0x9999\"}";
                    }
                    else
                        json = "{\"code\":\"NO\",\"message\":\"0x0005\"}";
                }
            }
            catch
            {
                json = "{\"code\":\"ERROR\",\"message\":\"0x9999\"}";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();


        }

        private void usersaveupgrade(HttpContext context)
        {
            string json = string.Empty;
            try
            {
                //OBTENEMOS EL TIPO DE USUARIOS
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));
                    //if (HttpContext.Current.Request.Cookies["site"] != null)
                    //    fdsUser.IdHotel = Int32.Parse(HttpContext.Current.Request.Cookies["site"].Value);

                    Hotels hotel = BillingController.GetHotel(Int32.Parse(HttpContext.Current.Request.Cookies["site"].Value));

                    string idUser = (context.Request["id"] == null ? string.Empty : context.Request["id"].ToString());
                    string accessType = (context.Request["accessType"] == null ? string.Empty : context.Request["accessType"].ToString());

                    Users u = BillingController.GetRadiusUser(Int32.Parse(idUser));
                    BillingTypes b = BillingController.ObtainBillingType(Int32.Parse(accessType));

                    Billings billing = new Billings();
                    billing.IdHotel = u.IdHotel;
                    billing.IdRoom = u.IdRoom;
                    billing.IdBillingType = b.IdBillingType;
                    billing.Amount = b.Price;
                    billing.BillingDate = DateTime.Now.ToUniversalTime();
                    billing.BillingCharge = DateTime.Now.ToUniversalTime();
                    billing.Flag = 3;
                    billing.IdLocation = b.IdLocation;
                    if (b.IdBillingModule.Equals(2) || b.IdBillingModule.Equals(4))
                        billing.Billable = true;
                    billing.IdFDSUser = fdsUser.IdUser;
                    billing.IdUser = u.IdUser;


                    u.MaxDevices = b.MaxDevices;
                    if (u.TimeCredit <= b.TimeCredit)
                        u.TimeCredit = b.TimeCredit;

                    if (u.ValidTill < u.ValidSince.AddHours(b.ValidTill))
                        u.ValidTill = u.ValidSince.AddHours(b.ValidTill);

                    u.VolumeDown = b.VolumeDown;
                    u.VolumeUp = b.VolumeUp;
                    u.BWDown = b.BWDown;
                    u.BWUp = b.BWUp;
                    u.Priority = b.IdPriority;
                    u.Name = u.Name;
                    u.Password = u.Password;
                    u.IdBillingType = b.IdBillingType;
                    u.Filter_Id = b.Filter_Id;

                    if (BillingController.SaveBilling(billing))
                    {
                        if (BillingController.UpdateUser(u))
                            json = "{\"code\":\"OK\",\"message\":\"0x0007\"}";
                        else
                            json = "{\"code\":\"ERROR\",\"message\":\"0x9999\"}";
                    }
                    else
                        json = "{\"code\":\"ERROR\",\"message\":\"0x9999\"}";
                }
            }
            catch
            {
                json = "{\"code\":\"ERROR\",\"message\":\"0x9999\"}";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();


        }

        private void UsersOnlineLocation(HttpContext context)
        {
            string json = string.Empty;
            try
            {
                List<UsersOnlineCoordinadesInfo> list = BillingController.SelectUsersOnlineCoordinades();
                json = "{\"message\": \"ok\", \"items\": [";
                // visualización de los datos
                foreach (UsersOnlineCoordinadesInfo u in list)
                {
                    if (!string.IsNullOrEmpty(u.Latitude))
                        json += "[ " + u.Latitude + "," + u.Longitude + ", \"" + u.CallerID + "\"],";
                }
                json = json.Substring(0, json.Length - 1);
                json += "]}";


            }
            catch (Exception ex)
            {
            }

            // json = "{\"code\":\"OK\",\"message\":\"\"}";
            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();

        }

        private ListVendorsResult Vendors(HttpContext context)
        {
            ListVendorsResult response = new ListVendorsResult();
            try
            {
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
 
                    response.list = new List<VendorsResult>();
                    List<Vendors> list = BillingController.SelectVendors();

                    foreach (Vendors b in list.OrderBy(a => a.Name))
                    {
                        VendorsResult br = new VendorsResult();
                        br.IdVendor = b.IdVendor;
                        br.Name = b.Name;

                        response.list.Add(br);
                    }
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;


        }

        private ListUserManagementResult VouchersManagement(HttpContext context)
        {
            ListUserManagementResult response = new ListUserManagementResult();
            try
            {
                int PageNumber = 0;

                //OBTENEMOS EL TIPO DE USUARIOS
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));

                    Hotels hotel = null;
                    Locations2 location = null;
                    FDSGroups group = null;
                    SitesGMT siteGMT = GetSiteGMt(out location, out hotel, out group);

                    string page = (context.Request["page"] == null ? string.Empty : context.Request["page"].ToString());
                    string username = (context.Request["username"] == null ? string.Empty : context.Request["username"].ToString());
                    string billing = (context.Request["billing"] == null ? string.Empty : context.Request["billing"].ToString());
                    string idmember = (context.Request["idmember"] == null ? string.Empty : context.Request["idmember"].ToString());
                    string idmembertype = (context.Request["idmembertype"] == null ? string.Empty : context.Request["idmembertype"].ToString());
                    string idfdsuser = (context.Request["idfdsuser"] == null ? string.Empty : context.Request["idfdsuser"].ToString());
                    string start = (context.Request["start"] == null ? string.Empty : context.Request["start"].ToString());
                    string end = (context.Request["end"] == null ? string.Empty : context.Request["end"].ToString());
                    string billable = (context.Request["billable"] == null ? string.Empty : context.Request["billable"].ToString());
                    string enabled = (context.Request["enabled"] == null ? string.Empty : context.Request["enabled"].ToString());

                    int p = 0;
                    if (Int32.TryParse(page, out p))
                        PageIndex = p;

                    int billingFilter = (string.IsNullOrEmpty(billing)) ? 0 : Int32.Parse(billing);
                    int idfdsuserfilter = (string.IsNullOrEmpty(idfdsuser)) ? 0 : Int32.Parse(idfdsuser);

                    if (fdsUser.IdRol.Equals(3))
                        idfdsuserfilter = fdsUser.IdUser;

                    DateTime startfilter = DateTime.Now.ToUniversalTime().AddDays(-7).AddSeconds(-siteGMT.gmtoffset);
                    DateTime endfilter = DateTime.Now.ToUniversalTime().AddSeconds(-siteGMT.gmtoffset);
                    if (!string.IsNullOrEmpty(start) && !string.IsNullOrEmpty(end))
                    {
                        startfilter = DateTime.Parse(start).AddSeconds(-siteGMT.gmtoffset);
                        endfilter = DateTime.Parse(end).AddSeconds(-siteGMT.gmtoffset);
                    }

                    bool? billablefilter = null;
                    if (!billable.Equals("0"))
                        billablefilter = bool.Parse(billable);

                    bool? enabledfilter = null;
                    if (!enabled.Equals("0"))
                        enabledfilter = bool.Parse(enabled);

                    List<UserInfo> list = null;

                    string listadosHoteles = ListadoHoteles(group);
                    if (int.Parse(idmember) != 0)
                    {
                        int type = int.Parse(idmembertype);
                        switch (type)
                        {
                            case 1:
                                FDSGroups temp = BillingController.GetGroup(int.Parse(idmember));
                                listadosHoteles = ListadoHoteles(temp); break;
                            case 2: hotel = BillingController.GetHotel(int.Parse(idmember)); break;
                            case 3:
                                location = BillingController.GetLocation(int.Parse(idmember));
                                hotel = BillingController.GetHotel(location.IdHotel); break;

                        }
                    }

                    FDSUsersAccess fdsuserAccess = BillingController.GetFDSUserAccess(fdsUser.IdUser);
                    List<FDSUsers> listFDSUser = null;
                    switch (fdsUser.IdType)
                    {
                        case 2: listFDSUser = BillingController.GetFDSUsersSite(fdsuserAccess.IdSite); break;
                        case 3: listFDSUser = BillingController.GetFDSUsersLocation(fdsuserAccess.IdLocation); break;
                    }

                    if (location != null)
                    {
                         list = BillingController.GetVouchersUsersLocation(location.IdLocation, username, billingFilter, startfilter, endfilter, enabledfilter, billablefilter, idfdsuserfilter, PageIndex, PageSize, ref PageNumber);
                    }
                    else if (hotel != null)
                    {
                        list = BillingController.GetVouchersUsers(hotel.IdHotel, username, billingFilter, startfilter, endfilter, enabledfilter, billablefilter, idfdsuserfilter, PageIndex, PageSize, ref PageNumber);
                    }
                    else
                    {
                        list = BillingController.GetVouchersUsers(listadosHoteles, username, billingFilter, startfilter, endfilter, enabledfilter, billablefilter, idfdsuserfilter, PageIndex, PageSize, ref PageNumber);
                    }

                    response.list = new List<UserManagementResult>();
                    response.pageNumber = PageNumber;

                    List<FDSUsers> fdsusers = BillingController.GetFDSUsers();

                    foreach (UserInfo u in list.OrderBy(a => a.ValidSince))
                    {
                        if (HttpContext.Current.Request.Cookies["location"] != null)
                        {
                            if (location.IdLocation.Equals(u.IdLocation))
                            {
                                UserManagementResult r = new UserManagementResult();
                                r.UserName = u.UserName;
                                r.Password = u.Password;
                                r.Room = u.Room;
                                r.AccessType = u.BillingType;
                                r.ValidSince = u.ValidSince.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                                r.ValidTill = u.ValidTill.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                                r.TimeCredit = u.TimeCredit.ToString();
                                r.IdUser = u.IdUser.ToString();
                                r.Comment = u.Comment;
                                r.Enabled = u.Enabled;
                                r.Billable = u.Billable;
                                if (u.Billable)
                                {
                                    r.BillingCharge = u.BillingCharge.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                                    r.validAfterFirstUse = u.BillingCharge.AddHours(u.ValidAfterFirstUse).AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                                }
                                else
                                {
                                    r.BillingCharge = string.Empty;
                                    r.validAfterFirstUse = string.Empty;
                                }

                                r.idfdsuser = u.IdFDSUser;
                                FDSUsers seller = (from t in fdsusers where t.IdUser.Equals(u.IdFDSUser) select t).FirstOrDefault();
                                if (seller != null)
                                    r.fdsuser = seller.Name;
                                else
                                    r.fdsuser = string.Empty;

                                response.list.Add(r);

                            }
                        }
                        else
                        {
                            UserManagementResult r = new UserManagementResult();
                            r.UserName = u.UserName;
                            r.Password = u.Password;
                            r.Room = u.Room;
                            r.AccessType = u.BillingType;
                            r.ValidSince = u.ValidSince.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                            r.ValidTill = u.ValidTill.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                            r.TimeCredit = u.TimeCredit.ToString();
                            r.IdUser = u.IdUser.ToString();
                            r.Comment = u.Comment;
                            r.Enabled = u.Enabled;
                            r.Billable = u.Billable;
                            if (u.Billable)
                            {
                                r.BillingCharge = u.BillingCharge.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                                r.validAfterFirstUse = u.BillingCharge.AddHours(u.ValidAfterFirstUse).AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                            }
                            else
                            {
                                r.BillingCharge = string.Empty;
                                r.validAfterFirstUse = string.Empty;
                            }
                            
                                

                            r.idfdsuser = u.IdFDSUser;
                            FDSUsers seller = (from t in fdsusers where t.IdUser.Equals(u.IdFDSUser) select t).FirstOrDefault();
                            if (seller != null)
                                r.fdsuser = seller.Name;
                            else
                                r.fdsuser = string.Empty;
                            response.list.Add(r);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private void fdsusersave(HttpContext context, Dictionary<string, string> sData)
        {
            string json = string.Empty;
            try
            {
                //OBTENEMOS EL TIPO DE USUARIOS
                if (HttpContext.Current.Request.Cookies["FDSUser"] != null)
                {
                    FDSUsers fdsUser = BillingController.GetFDSUser(Int32.Parse(HttpContext.Current.Request.Cookies["FDSUser"].Value));

                    string oldpassword = sData["oldpassword"];
                    string password = sData["newpassword"];
                    if (fdsUser.Password.Equals(oldpassword))
                    {
                        fdsUser.Password = password;

                        if (BillingController.SaveFDSUser(fdsUser))
                            json = "{\"code\":\"OK\",\"message\":\"0x0034\"}";
                        else
                            json = "{\"code\":\"ERROR\",\"message\":\"0x9999\"}";
                    }
                    else
                        json = "{\"code\":\"NO\",\"message\":\"0x0035\"}";
                }

            }
            catch
            {
                json = "{\"code\":\"ERROR\",\"message\":\"0x9999\"}";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();


        }

        public void ProcessEmtpy(HttpContext context)
        {
            // Set the content type and encoding for JSON
            context.Response.ContentType = "application/json";
            context.Response.ContentEncoding = Encoding.UTF8;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public object internalLogUserss { get; private set; }
    }
}