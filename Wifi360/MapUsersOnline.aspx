﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Site.Master" AutoEventWireup="true" CodeBehind="MapUsersOnline.aspx.cs" Inherits="Wifi360.MapUsersOnline" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <script src='https://api.mapbox.com/mapbox.js/v3.1.1/mapbox.js'></script>
    <link href='https://api.mapbox.com/mapbox.js/v3.1.1/mapbox.css' rel='stylesheet' />

    <script src='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v1.0.0/leaflet.markercluster.js'></script>
    <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v1.0.0/MarkerCluster.css' rel='stylesheet' />
    <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v1.0.0/MarkerCluster.Default.css' rel='stylesheet' />
        <script>
        $(document).ready(function (e) {
            var dataString = 'action=UsersOnlineMap';
            $.ajax({
                url: "handlers/SMIHandler.ashx",
                data: dataString,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $("#myModalWait").modal('show');
                },
                complete: function () {
                    $("#myModalWait").modal('hide');
                },
                success: function (pReturn) {
                    L.mapbox.accessToken = 'pk.eyJ1IjoiamF2aWFycm9jaGEiLCJhIjoiY2ptaXY2Ymw5MDhndTNxbmptcGo1cnAycCJ9.YMORyd3rDYpXSDmIVeuiCw';
                    var map = L.mapbox.map('map', 'mapbox.streets')
                        .setView([40.4378698, -3.8196191], 4);
               
                    var markers = new L.MarkerClusterGroup();

                    for (var i = 0; i < pReturn.items.length; i++) {
                        var a = pReturn.items[i];
                        var title = a[2];
                        var marker = L.marker(new L.LatLng(a[0], a[1]), {
                            icon: L.mapbox.marker.icon({ 'marker-symbol': 'mobilephone', 'marker-color': '0044FF' }),
                            title: title
                        });
                        marker.bindPopup(title);
                        markers.addLayer(marker);
                    }

                    map.addLayer(markers);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#message").removeClass().addClass("alert alert-danger");
                    $("#message").html(xhr.statusText);
                    $("#myModalMessage").modal('show');

                }
            });
        });

    </script>
    <style>
        body { margin:0; padding:0; }
        #map { position:absolute; top:50px; bottom:0; width:100%; }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="map"></div>
</asp:Content>
