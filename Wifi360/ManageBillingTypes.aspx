﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Site.Master" AutoEventWireup="true" CodeBehind="ManageBillingTypes.aspx.cs" Inherits="Wifi360.ManageBillingTypes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <script type="text/javascript">

            function readCookie(name) {
                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
                }
                return null;
            };

            function bindSelect(id, valueMember, displayMember, source) {
                $("#" + id).find('option').remove().end();
                var cookie = readCookie("wifi360-language");
                if (cookie != null) {
                    switch (cookie) {
                        case "es": $("#" + id).append('<option value="0">Seleccionar</option>'); break;
                        default: $("#" + id).append('<option value="0">Select</option>'); break;
                    }
                }
                else
                    $("#" + id).append('<option value="0">Select</option>');

                $.each(source, function (index, item) {
                    $("#" + id).append('<option value="' + item[valueMember] + '"> ' + item[displayMember] + '</option>');
                });
            };

            $(document).ready(function () {

                var page = 0;
                var isVolumeCombined = false;
                var dataString2 = 'action=sitesetting';
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataString2,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    success: function (pReturn) {
                        var lReturn = JSON.parse(pReturn);
                        isVolumeCombined = lReturn.volumecombined;
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#modal-loading").hide();
                        $("#searchResult").html(xhr.statusText)
                    }
                });

                var cookie = readCookie("wifi360-language");

                var dataString3 = 'action=billingmodules';
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataString3,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    success: function (pReturn) {
                        var lReturn = JSON.parse(pReturn);
                        bindSelect('billingmodule', 'IdBillingModule', 'Name', lReturn.list);
                        var dataString = 'action=billingTypesFull&page=' + page + '&filter=' + $("#description").val() + '&status=' + $("#status").val() + "&module=" + $("#billingmodule").val() + '&iot=' + $("#iot").val();
                        $.ajax({
                            url: "handlers/SMIHandler.ashx",
                            data: dataString,
                            contentType: "application/json; charset=utf-8",
                            dataType: "text",
                            beforeSend: function () {
                                $("#myModalWait").modal('show');
                            },
                            complete: function () {
                                $("#myModalWait").modal('hide');
                            },
                            success: function (pReturn) {
                                var lReturn = JSON.parse(pReturn);
                                var table = "";
                                if (cookie != null) {
                                    switch (cookie) {
                                        case "es":
                                            if (!isVolumeCombined)
                                                table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Descripción</th><th class='hidden-xs'>Ubicación</th><th>Precio</th><th class='hidden-xs'>Tiempo de crédito</th><th class='hidden-xs'>Horas de validez</th><th class='hidden-xs'>BW Bajada</th><th class='hidden-xs'>BW Subida</th><th class='hidden-xs'>Volumen Bajada</th><th class='hidden-xs'>Volumen Subida</th><th>Módulo</th><th>Ticket Rápido</th><th></th></tr>";
                                            else
                                                table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Descripción</th><th class='hidden-xs'>Ubicación</th><th>Precio</th><th class='hidden-xs'>Tiempo de crédito</th><th class='hidden-xs'>Horas de validez</th><th class='hidden-xs'>BW Bajada</th><th class='hidden-xs'>BW Subida</th><th class='hidden-xs'>Volumen Agregado</th><th>Módulo</th><th>Ticket Rápido</th><th></th></tr>";
                                            break;
                                        default:
                                            if (!isVolumeCombined)
                                                table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Description</th><th class='hidden-xs'>Location</th><th>Price</th><th class='hidden-xs'>Time Credit</th><th class='hidden-xs'>Valid Hours</th><th class='hidden-xs'>BW Download</th><th class='hidden-xs'>BW Upload</th><th class='hidden-xs'>Volume Down</th><th class='hidden-xs'>Volume Up</th><th>Module</th><th>Fast Ticket</th><th></th></tr>";
                                            else
                                                table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Description</th><th class='hidden-xs'>Location</th><th>Price</th><th class='hidden-xs'>Time Credit</th><th class='hidden-xs'>Valid Hours</th><th class='hidden-xs'>BW Download</th><th class='hidden-xs'>BW Upload</th><th class='hidden-xs'>Volume Aggregate</th><th>Module</th><th>Fast Ticket</th><th></th></tr>";
                                            break;
                                    }
                                }
                                else {
                                    if (!isVolumeCombined)
                                        table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Description</th><th class='hidden-xs'>Location</th><th>Price</th><th class='hidden-xs'>Time Credit</th><th class='hidden-xs'>Valid Hours</th><th class='hidden-xs'>BW Download</th><th class='hidden-xs'>BW Upload</th><th class='hidden-xs'>Volume Down</th><th class='hidden-xs'>Volume Up</th><th>Module</th><th>Fast Ticket</th><th></th></tr>";
                                    else
                                        table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Description</th><th class='hidden-xs'>Location</th><th>Price</th><th class='hidden-xs'>Time Credit</th><th class='hidden-xs'>Valid Hours</th><th class='hidden-xs'>BW Download</th><th class='hidden-xs'>BW Upload</th><th class='hidden-xs'>Volume Aggregate</th><th>Module</th><th>Fast Ticket</th><th></th></tr>";
                                }

                                $.each(lReturn.list, function (index, item) {
                                    if (lReturn.isAdmin == true) {
                                        table += '<tr><td>' + item["Description"] + '</td><td class="hidden-xs">' + item["LocationDescription"] + '</td><td>' + item["Price"] + ' ' + item["currency"] + '</td><td class="hidden-xs">' + item["TimeCredit"] + '</td><td class="hidden-xs">' + item["ValidTill"] + '</td><td class="hidden-xs">' + item["BWDown"] + '</td><td class="hidden-xs">' + item["BWUp"] + '</td>';
                                        if (!isVolumeCombined)
                                            table += '<td class="hidden-xs">' + item["VolumeDown"] + '</td><td class="hidden-xs">' + item["VolumeUp"] + '</td>';
                                        else
                                            table += '<td class="hidden-xs">' + item["VolumeDown"] + '</td>';
                                        
                                        table += '<td>' + item["BillingModule"] + '</td><td><input type="checkbox" disabled="disabled" '
                                        if (item["DefaultFastTicket"] == true)
                                            table += 'checked="checked"';
                                        table += '/></td><td><a href="/billingType.aspx?id=' + item["IdBillingType"] + '"><i class="fa fa-pencil"></i></a></td></tr>';

                                    } else {
                                        table += '<tr><td>' + item["Description"] + '</td><td class="hidden-xs">' + item["LocationDescription"] + '</td><td>' + item["Price"] + '</td><td class="hidden-xs">' + item["TimeCredit"] + '</td><td class="hidden-xs">' + item["ValidTill"] + '</td><td class="hidden-xs">' + item["BWDown"] + '</td><td class="hidden-xs">' + item["BWUp"] + '</td>';
                                        if (!isVolumeCombined)
                                            table += '<td class="hidden-xs">' + item["VolumeDown"] + '</td><td class="hidden-xs">' + item["VolumeUp"] + '</td>';
                                        else
                                            table += '<td class="hidden-xs">' + item["VolumeDown"] + '</td>';

                                        table += '<td>' + item["BillingModule"] + '</td><td><input type="checkbox" disabled="disabled" ';
                                        if (item["DefaultFastTicket"] == true)
                                            table += 'checked="checked"';
                                        table += '/></td><td></td></tr>';

                                    }
                                });

                                
                                if (lReturn.isAdmin) {
                                    $("#new").show();
                                }

                                table += "</table>";

                                $("#searchResult").html(table);

                                var pagination = "<ul class='pagination'>";
                                var points1 = 0;
                                var points2 = 0;
                                x = parseInt(page);
                                for (var index = 0; index < lReturn.pageNumber; index++) {
                                    if (lReturn.pageNumber <= 10) {
                                        if (page == index)
                                            pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                        else
                                            pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                    }
                                    else {
                                        if (page == index) {
                                            pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                        }
                                        else if (index == 0) {
                                            pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                        }
                                        else if (index == (lReturn.pageNumber - 1)) {
                                            pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                        }
                                        else if (index < (x - 3)) {
                                            if (points1 == 0) {
                                                pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                                points1 = 1;
                                            }
                                        }
                                        else if (index > (x + 3)) {
                                            if (points2 == 0) {
                                                pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                                points2 = 1;
                                            }
                                        }
                                        else
                                            pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                    }
                                }

                                pagination += "</ul>";

                                $("#searchResult").append(pagination);

                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                $("#modal-loading").hide();
                                $("#searchResult").html(xhr.statusText)
                            }
                        });
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#modal-loading").hide();
                        $("#searchResult").html(xhr.statusText)
                    }
                });

                $("#search").click(function (e) {
                    e.preventDefault();

                    var page = 0;
                    var isVolumeCombined = false;
                    var dataString2 = 'action=sitesetting';
                    $.ajax({
                        url: "handlers/SMIHandler.ashx",
                        data: dataString2,
                        contentType: "application/json; charset=utf-8",
                        dataType: "text",
                        success: function (pReturn) {
                            var lReturn = JSON.parse(pReturn);
                            isVolumeCombined = lReturn.volumecombined;

                            var cookie = readCookie("wifi360-language");

                            var dataString = 'action=billingTypesFull&page=' + page + '&filter=' + $("#description").val() + '&status=' + $("#status").val() + "&module=" + $("#billingmodule").val() + '&iot=' + $("#iot").val();
                            $.ajax({
                                url: "handlers/SMIHandler.ashx",
                                data: dataString,
                                contentType: "application/json; charset=utf-8",
                                dataType: "text",
                                beforeSend: function () {
                                    $("#myModalWait").modal('show');
                                },
                                complete: function () {
                                    $("#myModalWait").modal('hide');
                                },
                                success: function (pReturn) {
                                    var lReturn = JSON.parse(pReturn);
                                    var table = "";
                                    if (cookie != null) {
                                        switch (cookie) {
                                            case "es":
                                                if (!isVolumeCombined)
                                                    table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Descripción</th><th class='hidden-xs'>Ubicación</th><th>Precio</th><th class='hidden-xs'>Tiempo de crédito</th><th class='hidden-xs'>Horas de validez</th><th class='hidden-xs'>BW Bajada</th><th class='hidden-xs'>BW Subida</th><th class='hidden-xs'>Volumen Bajada</th><th class='hidden-xs'>Volumen Subida</th><th>Módulo</th><th>Ticket Rápido</th><th></th></tr>";
                                                else
                                                    table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Descripción</th><th class='hidden-xs'>Ubicación</th><th>Precio</th><th class='hidden-xs'>Tiempo de crédito</th><th class='hidden-xs'>Horas de validez</th><th class='hidden-xs'>BW Bajada</th><th class='hidden-xs'>BW Subida</th><th class='hidden-xs'>Volumen Agregado</th><th>Módulo</th><th>Ticket Rápido</th><th></th></tr>";
                                                break;
                                            default:
                                                if (!isVolumeCombined)
                                                    table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Description</th><th class='hidden-xs'>Location</th><th>Price</th><th class='hidden-xs'>Time Credit</th><th class='hidden-xs'>Valid Hours</th><th class='hidden-xs'>BW Download</th><th class='hidden-xs'>BW Upload</th><th class='hidden-xs'>Volume Down</th><th class='hidden-xs'>Volume Up</th><th>Module</th><th>Fast Ticket</th><th></th></tr>";
                                                else
                                                    table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Description</th><th class='hidden-xs'>Location</th><th>Price</th><th class='hidden-xs'>Time Credit</th><th class='hidden-xs'>Valid Hours</th><th class='hidden-xs'>BW Download</th><th class='hidden-xs'>BW Upload</th><th class='hidden-xs'>Volume Aggregate</th><th>Module</th><th>Fast Ticket</th><th></th></tr>";
                                                break;
                                        }
                                    }
                                    else {
                                        if (!isVolumeCombined)
                                            table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Description</th><th class='hidden-xs'>Location</th><th>Price</th><th class='hidden-xs'>Time Credit</th><th class='hidden-xs'>Valid Hours</th><th class='hidden-xs'>BW Download</th><th class='hidden-xs'>BW Upload</th><th class='hidden-xs'>Volume Down</th><th class='hidden-xs'>Volume Up</th><th>Module</th><th>Fast Ticket</th><th></th></tr>";
                                        else
                                            table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Description</th><th class='hidden-xs'>Location</th><th>Price</th><th class='hidden-xs'>Time Credit</th><th class='hidden-xs'>Valid Hours</th><th class='hidden-xs'>BW Download</th><th class='hidden-xs'>BW Upload</th><th class='hidden-xs'>Volume Aggregate</th><th>Module</th><th>Fast Ticket</th><th></th></tr>";
                                    }

                                    $.each(lReturn.list, function (index, item) {
                                        if (lReturn.isAdmin == true) {
                                            table += '<tr><td>' + item["Description"] + '</td><td class="hidden-xs">' + item["LocationDescription"] + '</td><td>' + item["Price"] + ' ' + item["currency"] + '</td><td class="hidden-xs">' + item["TimeCredit"] + '</td><td class="hidden-xs">' + item["ValidTill"] + '</td><td class="hidden-xs">' + item["BWDown"] + '</td><td class="hidden-xs">' + item["BWUp"] + '</td>';
                                            if (!isVolumeCombined)
                                                table += '<td class="hidden-xs">' + item["VolumeDown"] + '</td><td class="hidden-xs">' + item["VolumeUp"] + '</td>';
                                            else
                                                table += '<td class="hidden-xs">' + item["VolumeDown"] + '</td>';
                                        
                                            table += '<td>' + item["BillingModule"] + '</td><td><input type="checkbox" disabled="disabled" '
                                            if (item["DefaultFastTicket"] == true)
                                                table += 'checked="checked"';
                                            table += '/></td><td><a href="/billingType.aspx?id=' + item["IdBillingType"] + '"><i class="fa fa-pencil"></i></a></td></tr>';

                                        } else {
                                            table += '<tr><td>' + item["Description"] + '</td><td class="hidden-xs">' + item["LocationDescription"] + '</td><td>' + item["Price"] + '</td><td class="hidden-xs">' + item["TimeCredit"] + '</td><td class="hidden-xs">' + item["ValidTill"] + '</td><td class="hidden-xs">' + item["BWDown"] + '</td><td class="hidden-xs">' + item["BWUp"] + '</td>';
                                            if (!isVolumeCombined)
                                                table += '<td class="hidden-xs">' + item["VolumeDown"] + '</td><td class="hidden-xs">' + item["VolumeUp"] + '</td>';
                                            else
                                                table += '<td class="hidden-xs">' + item["VolumeDown"] + '</td>';

                                            table += '<td>' + item["BillingModule"] + '</td><td><input type="checkbox" disabled="disabled" ';
                                            if (item["DefaultFastTicket"] == true)
                                                table += 'checked="checked"';
                                            table += '/></td><td></td></tr>';

                                        }
                                    });
                                    table += "</table>";

                                    if (lReturn.isAdmin) {
                                        $("#new").show();
                                    }

                                    $("#searchResult").html(table);

                                    var pagination = "<ul class='pagination'>";
                                    var points1 = 0;
                                    var points2 = 0;
                                    x = parseInt(page);
                                    for (var index = 0; index < lReturn.pageNumber; index++) {
                                        if (lReturn.pageNumber <= 10) {
                                            if (page == index)
                                                pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                            else
                                                pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                        }
                                        else {
                                            if (page == index) {
                                                pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                            }
                                            else if (index == 0) {
                                                pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                            }
                                            else if (index == (lReturn.pageNumber - 1)) {
                                                pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                            }
                                            else if (index < (x - 3)) {
                                                if (points1 == 0) {
                                                    pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                                    points1 = 1;
                                                }
                                            }
                                            else if (index > (x + 3)) {
                                                if (points2 == 0) {
                                                    pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                                    points2 = 1;
                                                }
                                            }
                                            else
                                                pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                        }
                                    }

                                    pagination += "</ul>";

                                    $("#searchResult").append(pagination);

                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    $("#modal-loading").hide();
                                    $("#searchResult").html(xhr.statusText)
                                }
                            });
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#modal-loading").hide();
                            $("#searchResult").html(xhr.statusText)
                        }
                    });
                });

                
            });

            $(document).on("click", 'a[rel^="page"]', function (e) {
                e.preventDefault();
                var page = $(this).attr("attr");

                var isVolumeCombined = false;
                var dataString2 = 'action=sitesetting';
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataString2,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    success: function (pReturn) {
                        var lReturn = JSON.parse(pReturn);
                        isVolumeCombined = lReturn.volumecombined;
                    
                        var cookie = readCookie("wifi360-language");

                        var dataString = 'action=billingTypesFull&page=' + page + '&filter=' + $("#description").val() + '&status=' + $("#status").val() + "&module=" + $("#billingmodule").val() + '&iot=' + $("#iot").val();
                        $.ajax({
                            url: "handlers/SMIHandler.ashx",
                            data: dataString,
                            contentType: "application/json; charset=utf-8",
                            dataType: "text",
                            beforeSend: function () {
                                $("#myModalWait").modal('show');
                            },
                            complete: function () {
                                $("#myModalWait").modal('hide');
                            },
                            success: function (pReturn) {
                                var lReturn = JSON.parse(pReturn);
                                var table = "";
                                if (cookie != null) {
                                    switch (cookie) {
                                        case "es":
                                            if (!isVolumeCombined)
                                                table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Descripción</th><th class='hidden-xs'>Ubicación</th><th>Precio</th><th class='hidden-xs'>Tiempo de crédito</th><th class='hidden-xs'>Horas de validez</th><th class='hidden-xs'>BW Bajada</th><th class='hidden-xs'>BW Subida</th><th class='hidden-xs'>Volumen Bajada</th><th class='hidden-xs'>Volumen Subida</th><th>Módulo</th><th>Ticket Rápido</th><th></th></tr>";
                                            else
                                                table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Descripción</th><th class='hidden-xs'>Ubicación</th><th>Precio</th><th class='hidden-xs'>Tiempo de crédito</th><th class='hidden-xs'>Horas de validez</th><th class='hidden-xs'>BW Bajada</th><th class='hidden-xs'>BW Subida</th><th class='hidden-xs'>Volumen Agregado</th><th>Módulo</th><th>Ticket Rápido</th><th></th></tr>";
                                            break;
                                        default:
                                            if (!isVolumeCombined)
                                                table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Description</th><th class='hidden-xs'>Location</th><th>Price</th><th class='hidden-xs'>Time Credit</th><th class='hidden-xs'>Valid Hours</th><th class='hidden-xs'>BW Download</th><th class='hidden-xs'>BW Upload</th><th class='hidden-xs'>Volume Down</th><th class='hidden-xs'>Volume Up</th><th>Module</th><th>Fast Ticket</th><th></th></tr>";
                                            else
                                                table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Description</th><th class='hidden-xs'>Location</th><th>Price</th><th class='hidden-xs'>Time Credit</th><th class='hidden-xs'>Valid Hours</th><th class='hidden-xs'>BW Download</th><th class='hidden-xs'>BW Upload</th><th class='hidden-xs'>Volume Aggregate</th><th>Module</th><th>Fast Ticket</th><th></th></tr>";
                                            break;
                                    }
                                }
                                else {
                                    if (!isVolumeCombined)
                                        table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Description</th><th class='hidden-xs'>Location</th><th>Price</th><th class='hidden-xs'>Time Credit</th><th class='hidden-xs'>Valid Hours</th><th class='hidden-xs'>BW Download</th><th class='hidden-xs'>BW Upload</th><th class='hidden-xs'>Volume Down</th><th class='hidden-xs'>Volume Up</th><th>Module</th><th>Fast Ticket</th><th></th></tr>";
                                    else
                                        table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Description</th><th class='hidden-xs'>Location</th><th>Price</th><th class='hidden-xs'>Time Credit</th><th class='hidden-xs'>Valid Hours</th><th class='hidden-xs'>BW Download</th><th class='hidden-xs'>BW Upload</th><th class='hidden-xs'>Volume Aggregate</th><th>Module</th><th>Fast Ticket</th><th></th></tr>";
                                }

                                $.each(lReturn.list, function (index, item) {
                                    if (lReturn.isAdmin == true) {
                                        table += '<tr><td>' + item["Description"] + '</td><td class="hidden-xs">' + item["LocationDescription"] + '</td><td>' + item["Price"] + ' ' + item["currency"] + '</td><td class="hidden-xs">' + item["TimeCredit"] + '</td><td class="hidden-xs">' + item["ValidTill"] + '</td><td class="hidden-xs">' + item["BWDown"] + '</td><td class="hidden-xs">' + item["BWUp"] + '</td>';
                                        if (!isVolumeCombined)
                                            table += '<td class="hidden-xs">' + item["VolumeDown"] + '</td><td class="hidden-xs">' + item["VolumeUp"] + '</td>';
                                        else
                                            table += '<td class="hidden-xs">' + item["VolumeDown"] + '</td>';

                                        table += '<td>' + item["BillingModule"] + '</td><td><input type="checkbox" disabled="disabled" '
                                        if (item["DefaultFastTicket"] == true)
                                            table += 'checked="checked"';
                                        table += '/></td><td><a href="/billingType.aspx?id=' + item["IdBillingType"] + '"><i class="fa fa-pencil"></i></a></td></tr>';

                                    } else {
                                        table += '<tr><td>' + item["Description"] + '</td><td class="hidden-xs">' + item["LocationDescription"] + '</td><td>' + item["Price"] + '</td><td class="hidden-xs">' + item["TimeCredit"] + '</td><td class="hidden-xs">' + item["ValidTill"] + '</td><td class="hidden-xs">' + item["BWDown"] + '</td><td class="hidden-xs">' + item["BWUp"] + '</td>';
                                        if (!isVolumeCombined)
                                            table += '<td class="hidden-xs">' + item["VolumeDown"] + '</td><td class="hidden-xs">' + item["VolumeUp"] + '</td>';
                                        else
                                            table += '<td class="hidden-xs">' + item["VolumeDown"] + '</td>';

                                        table += '<td>' + item["BillingModule"] + '</td><td><input type="checkbox" disabled="disabled" ';
                                        if (item["DefaultFastTicket"] == true)
                                            table += 'checked="checked"';
                                        table += '/></td><td></td></tr>';

                                    }
                                });

                                if (lReturn.isAdmin) {
                                    $("#new").show();
                                }

                                $("#searchResult").html(table);

                                var pagination = "<ul class='pagination'>";
                                var points1 = 0;
                                var points2 = 0;
                                x = parseInt(page);
                                for (var index = 0; index < lReturn.pageNumber; index++) {
                                    if (lReturn.pageNumber <= 10) {
                                        if (page == index)
                                            pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                        else
                                            pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                    }
                                    else {
                                        if (page == index) {
                                            pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                        }
                                        else if (index == 0) {
                                            pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                        }
                                        else if (index == (lReturn.pageNumber - 1)) {
                                            pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                        }
                                        else if (index < (x - 3)) {
                                            if (points1 == 0) {
                                                pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                                points1 = 1;
                                            }
                                        }
                                        else if (index > (x + 3)) {
                                            if (points2 == 0) {
                                                pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                                points2 = 1;
                                            }
                                        }
                                        else
                                            pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                    }
                                }

                                pagination += "</ul>";

                                $("#searchResult").append(pagination);

                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                $("#modal-loading").hide();
                                $("#searchResult").html(xhr.statusText)
                            }
                        });

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#modal-loading").hide();
                        $("#searchResult").html(xhr.statusText)
                    }
                });
            });



    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 id="headerpagemanagebillingtypes">Access Type Management</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="default.aspx">Dashboard</a>
                </li>
                <li>
                    Tickets
                </li>
                <li class="active">
                    <strong id="breadlabelmanagebillingtypes">Access Type Management</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5 id="labelboxheaderbillingtypes">Access types<small>  Internet access types Management </small></h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <a href="billingType.aspx" id="new" class="btn btn-primary" style="display:none;"><i class="fa fa-plus"></i><span id="buttonnewbillingtype"> New</span></a>
                        </div>
                        <div class="col-lg-12">
                                                <div class="ibox border-bottom">
                    <div class="ibox-title">
                        <h5 id="headerboxfilterusersvalid">Search filters</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-down"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content" style="display: none;">
                        <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto;">
                            <div class="scroll_content" style="overflow: hidden; width: auto; ">
                                <div role="form">
                            <div class="form-group"><label for="username" id="labelformbillingtypesdescription">Description</label>
                                <input type="text" id="description" class="form-control"/>
                            </div>
                            <div class="form-group"><label for="username" id="labelformbillingtypesmodule">Module</label>
                                <select id="billingmodule" class="form-control"></select>
                            </div>
                            <div class="form-group"><label for="username" id="labelformbillingtypestatus">Status</label>
                                <select id="status" class="form-control">
                                    <option value="true" selected="selected">Active</option>
                                    <option value="false">Inactive</option>
                                </select>
                            </div>
                            <div class="form-group"><label for="username" id="labelformbillingtypeiot">User type</label>
                                <select id="iot" class="form-control">
                                    <option value="0" selected="selected">All</option>
                                    <option value="1">Personal devices</option>
                                    <option value="2">Internet of Things</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <a href="#" id="search" class="btn btn-w-m btn-primary" ><i class="fa fa-search"></i><span id="labelformusermanagementbuttonsearch"> Search </span></a>
                            </div>
                        </div>
                        </div><div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 198.02px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                    </div>
                </div>
                        </div>
                        <div class="col-lg-12">
                            <div id="searchResult"></div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

                    <div class="modal inmodal fade" id="myModalMessage" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                            </div>
                            <div class="modal-body">
                                <div id="message">
                                    
                                </div>
                            </div>
                          <div class="modal-footer">
                            <a href="#" class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Volver</a>
                          </div>
                        </div>
                    </div>
                </div>
        </div>
</asp:Content>
