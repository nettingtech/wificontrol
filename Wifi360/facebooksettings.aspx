﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Site.Master" AutoEventWireup="true" CodeBehind="facebooksettings.aspx.cs" Inherits="Wifi360.facebooksettings" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script type="text/javascript">

         function readCookie(name) {
             var nameEQ = name + "=";
             var ca = document.cookie.split(';');
             for (var i = 0; i < ca.length; i++) {
                 var c = ca[i];
                 while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                 if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
             }
             return null;
         };


         $(document).ready(function () {
             var page = 0;

             $("#lisettings").attr("class", "active");
             $("#ulsettings").attr("class", "nav nav-second-level collapse in");
             $("#lifacebooksettings").attr("class", "active");
             $("#lidashboard").removeClass();


             var dataString = 'action=SELECTFACEBOOKPROMOTIONS&page=' + page;
             $.ajax({
                 url: "handlers/SMIHandler.ashx",
                 data: dataString,
                 contentType: "application/json; charset=utf-8",
                 dataType: "text",
                 beforeSend: function () {
                     $("#myModalWait").modal('show');
                 },
                 complete: function () {
                     $("#myModalWait").modal('hide');
                 },
                 success: function (pReturn) {
                     var lReturn = JSON.parse(pReturn);
                     var table = "";
                     var cookie = readCookie("wifi360-language");
                     if (cookie != null) {
                         switch (cookie) {
                             case "es": table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Promoción</th><th>Localización</th><th>Tipo de acceso</th><th class='hidden-xs'>Visible</th><th></th></tr>"; break;
                             default: table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Promotion</th><th>Location</th><th>Access Type</th><th class='hidden-xs'>Visible</th><th></th></tr>"; break;
                         }
                     }
                     else
                         table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Promotion</th><th>Location</th><th>Access Type</th><th class='hidden-xs'>Visible</th><th></th></tr>";
                     $.each(lReturn.list, function (index, item) {
                         table += '<tr><td>' + item["PageName"] + '</td><td>' + item["Location"] + '</td><td class="hidden-xs">' + item["BillingType"] + '</td><td class="hidden-xs">' + item["Visible"] + '</td><td><a href="/facebookpromotion.aspx?id=' + item["IdFacebookPromotion"] + '"><i class="fa fa-pencil"></i> </a></td></tr>';
                     });

                     table += "</table>";

                     $("#searchResult").html(table);

                 },
                 error: function (xhr, ajaxOptions, thrownError) {
                     $("#modal-loading").hide();
                     $("#searchResult").html(xhr.statusText)
                 }
             });
         });

                </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 id="headerpagelocationsetting">Settings</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="default.aspx">Dashboard</a>
                </li>
                <li id="breadpagefatherlocationsetting">
                    Settings
                </li>
                <li class="active">
                    <strong id="breadpagelocationsetting">Facebook Promotions</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5 id="labelboxheaderfacebookpromotions">Facebook promotions<small> Manage facebook promotions settings</small></h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <a href="facebookpromotion.aspx" id="new" class="btn btn-primary"><i class="fa fa-plus"></i> <span id="labelformfacebookpromotionbuttonnew">New</span></a>
                        </div>
                        <div class="col-lg-12">
                            <div id="searchResult"></div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

                                <div class="modal inmodal fade" id="myModalMessage" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                            </div>
                            <div class="modal-body">
                                <div id="message">
                                    
                                </div>
                            </div>
                          <div class="modal-footer">
                            <a href="#" class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Back</a>
                          </div>
                        </div>
                    </div>
                </div>
        </div>
</asp:Content>
