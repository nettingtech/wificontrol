﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Site.Master" AutoEventWireup="true" CodeBehind="DisabledUsers.aspx.cs" Inherits="Wifi360.DisabledUsers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/calendar.css" rel="stylesheet" />
    <script src="js/calendar.full.min.js"></script>
    <script src="js/moment.js"></script>

        <script type="text/javascript">

            function readCookie(name) {
                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
                }
                return null;
            };

            function bindSelectAttribute(id, valueMember, displayMember, attribute, source) {
                $("#" + id).find('option').remove().end();
                var cookie = readCookie("wifi360-language");
                if (cookie != null) {
                    switch (cookie) {
                        case "es": $("#" + id).append('<option value="0">Seleccionar</option>'); break;
                        default: $("#" + id).append('<option value="0">Select </option>'); break;
                    }
                }
                else
                    $("#" + id).append('<option value="0">Select </option>');
                $.each(source, function (index, item) {
                    $("#" + id).append('<option value="' + item[valueMember] + '" rel="' + item[attribute] + '"> ' + item[displayMember] + '</option>');
                });
            };

            $(document).on("click", 'a[rel^="page"]', function (e) {
                e.preventDefault();
                var page = $(this).attr("attr");
                var username = $("#username").val();
                var start = $("#start").val();
                var end = $("#end").val();
                var billing = $("#billingtypes").val();
                var room = $("#rooms").val();
                var idmember = $("#members").val();
                var idmembertype = $('option:selected', $("#members")).attr('rel');
                var idfdsuser = $("#sellers").val();

                var cookieRol = readCookie("FDSUserRol");
                var dataString = 'action=DISABLEDUSERS&page=' + page + '&username=' + username + '&start=' + start + '&end=' + end + '&billing=' + billing + '&room=' + room + '&idmember=' + idmember + '&idmembertype=' + idmembertype + '&idfdsuser=' + idfdsuser;
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    beforeSend: function () {
                        $("#myModalWait").modal('show');
                    },
                    complete: function () {
                        $("#myModalWait").modal('hide');
                    },

                    success: function (pReturn) {
                        var lReturn = JSON.parse(pReturn);
                        var table = "";
                        var cookie = readCookie("wifi360-language");
                        if (cookie != null) {
                            switch (cookie) {
                                case "es": table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><th>Nombre de usuario</th><th>Motivo</th><th class='hidden-xs'>Fecha de inicio</th><th class='hidden-xs'>Fecha de finalización</th><th class='hidden-xs'>Crédito de tiempo</th><th class='hidden-xs'>Volumen Descarga</th><th class='hidden-xs'>Volumen Subida</th>";
                                    if (cookieRol != "3") table += "<th class='hidden-xs'>Vendedor</th>";
                                    table += "<th></th>"; break;
                                default: table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><th>Username</th><th>Reason</th><th class='hidden-xs'>Valid Since</th><th class='hidden-xs'>Valid Till</th><th class='hidden-xs'>Time Credit</th><th class='hidden-xs'>Volume Down</th><th class='hidden-xs'>Volume Up</th>";
                                    if (cookieRol != "3") table += "<th class='hidden-xs'>Seller</th>";
                                    table += "<th></th>"; break;
                            }
                        }
                        else {
                            table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><th>Username</th><th>Reason</th><th class='hidden-xs'>Valid Since</th><th class='hidden-xs'>Valid Till</th><th class='hidden-xs'>Time Credit</th><th class='hidden-xs'>Volume Down</th><th class='hidden-xs'>Volume Up</th>";
                            if (cookieRol != "3") table += "<th class='hidden-xs'>Seller</th>";
                            table += "<th></th>";
                        }
                        $.each(lReturn.list, function (index, item) {
                            table += '<tr><td>' + item["UserName"] + '</td><td>' + item["Room"] + '</td><td class="hidden-xs">' + item["ValidSince"] + '</td><td class="hidden-xs">' + item["ValidTill"] + '</td><td class="hidden-xs">' + item["TimeCredit"] + '</td><td class="hidden-xs">' + item["VolumeDown"] + '</td><td class="hidden-xs">' + item["VolumeUp"] + '</td>';
                            if (cookieRol != "3") table += '<td class="hidden-xs">' + item["fdsuser"] + '</td>';
                            table += '<td class="text-center"><a href="/User.aspx?id=' + item["IdUser"] + '&return=DisabledUsers.aspx"><i class=\"fa fa-pencil\"></i></td></tr>';
                        });
                        table += "</table>";

                        $("#searchResult").html(table);

                        var pagination = "<ul class='pagination'>";
                        var points1 = 0;
                        var points2 = 0;
                        x = parseInt(page);
                        for (var index = 0; index < lReturn.pageNumber; index++) {
                            if (lReturn.pageNumber <= 10) {
                                if (page == index)
                                    pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                else
                                    pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                            }
                            else {
                                if (page == index) {
                                    pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                                else if (index == 0) {
                                    pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                                else if (index == (lReturn.pageNumber - 1)) {
                                    pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                                else if (index < (x - 3)) {
                                    if (points1 == 0) {
                                        pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                        points1 = 1;
                                    }
                                }
                                else if (index > (x + 3)) {
                                    if (points2 == 0) {
                                        pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                        points2 = 1;
                                    }
                                }
                                else
                                    pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                            }
                        }

                        pagination += "</ul>";

                        $("#searchResult").append(pagination);

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#modal-loading").hide();
                        $("#searchResult").html(xhr.statusText)
                    }
                });
            });

            $(document).ready(function () {
                var page = 0;

                var option;
                var cookie = readCookie("wifi360-language");
                if (cookie != null) {
                    switch (cookie) {
                        case "es": var calendar = new CalendarPopup('#start', { dayOfWeekStart: 1, showMask: false, locale: 'es', showWeekIndex: true, format: 'DD/MM/YYYY HH:mm', i18n: { es: { "Chose": "Elegir", "Close": "Cerrar", "Time": "Hora" } } });
                            var calendar2 = new CalendarPopup('#end', {dayOfWeekStart: 1, showMask: false, locale: 'es', showWeekIndex: true, format: 'DD/MM/YYYY HH:mm', i18n: { es: { "Chose": "Elegir", "Close": "Cerrar", "Time": "Hora" } } });
                            option = '<option value="0">No disponible</option>'; break;
                        default: var calendar = new CalendarPopup('#start', { showMask: false, showWeekIndex: true, format: 'DD/MM/YYYY HH:mm' });
                            var calendar2 = new CalendarPopup('#end', { showMask: false, showWeekIndex: true, format: 'DD/MM/YYYY HH:mm' });
                            option = '<option value="0">Not avalaible</option>'; break;
                    }
                }
                else {
                    var calendar = new CalendarPopup('#start', {dayOfWeekStart: 1, showMask: false, showWeekIndex: true, format: 'DD/MM/YYYY HH:mm' });
                    var calendar2 = new CalendarPopup('#end', {dayOfWeekStart: 1, showMask: false, showWeekIndex: true, format: 'DD/MM/YYYY HH:mm' });
                    option = '<option value="0">Not avalaible</option>';
                }

                $(".calendar-popup-input-wrapper").attr("style", "display:block;")

                var cookieRol = readCookie("FDSUserRol");
                if (cookieRol == "3")
                    $("#sellers").attr("disabled", "disabled");

                var page = 0;
                var username = "";
                var start = "";
                var end = "";
                var billing = "";
                var room = "";

                $("#billingtypes").append(option);
                $("#rooms").append(option);
                $("#Priority").append(option);
                $("#filterprofile").append(option);
                $("#sellers").append(option);


                function bindSelect(id, valueMember, displayMember, source) {
                    $("#" + id).find('option').remove().end();
                    var cookie = readCookie("wifi360-language");
                    if (cookie != null) {
                        switch (cookie) {
                            case "es": $("#" + id).append('<option value="0">Seleccionar</option>'); break;
                            default: $("#" + id).append('<option value="0">Select </option>'); break;
                        }
                    }
                    else
                        $("#" + id).append('<option value="0">Select </option>');
                    $.each(source, function (index, item) {
                        $("#" + id).append('<option value="' + item[valueMember] + '"> ' + item[displayMember] + '</option>');
                    });
                };

                var dataStringGroup = 'action=GROUPMEMBERS';
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataStringGroup,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    success: function (pReturn) {
                        var lReturn = JSON.parse(pReturn);
                        bindSelectAttribute("members", "IdMember", "Name", "IdMemberType", lReturn.list);
                        if (lReturn.list.length == 1) {
                            $("#members").val(lReturn.list[0].IdMember).change();;
                            ChangeMember();
                        }

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#modal-loading").hide();
                        $("#searchResult").html(xhr.statusText)
                    }
                });

                var cookieRol = readCookie("FDSUserRol");
                var dataString = 'action=DISABLEDUSERS&page=' + page + '&username=' + username + '&start=' + start + '&end=' + end + '&billing=' + billing + '&room=' + room + '&idmember=0&idmembertype=0';
                $.ajax({
                    url: "handlers/SMIHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    beforeSend: function () {
                        $("#myModalWait").modal('show');
                    },
                    complete: function () {
                        $("#myModalWait").modal('hide');
                    },
                    success: function (pReturn) {
                        var lReturn = JSON.parse(pReturn);
                        var table = "";
                        var cookie = readCookie("wifi360-language");
                        if (cookie != null) {
                            switch (cookie) {
                                case "es": table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><th>Nombre de usuario</th><th>Motivo</th><th class='hidden-xs'>Fecha de inicio</th><th class='hidden-xs'>Fecha de finalización</th><th class='hidden-xs'>Crédito de tiempo</th><th class='hidden-xs'>Volumen Descarga</th><th class='hidden-xs'>Volumen Subida</th>";
                                    if (cookieRol != "3") table += "<th class='hidden-xs'>Vendedor</th>";
                                    table += "<th></th>"; break;
                                default: table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><th>Username</th><th>Reason</th><th class='hidden-xs'>Valid Since</th><th class='hidden-xs'>Valid Till</th><th class='hidden-xs'>Time Credit</th><th class='hidden-xs'>Volume Down</th><th class='hidden-xs'>Volume Up</th>";
                                    if (cookieRol != "3") table += "<th class='hidden-xs'>Seller</th>";
                                    table += "<th></th>"; break;
                            }
                        }
                        else {
                            table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><th>Username</th><th>Reason</th><th class='hidden-xs'>Valid Since</th><th class='hidden-xs'>Valid Till</th><th class='hidden-xs'>Time Credit</th><th class='hidden-xs'>Volume Down</th><th class='hidden-xs'>Volume Up</th>";
                            if (cookieRol != "3") table += "<th class='hidden-xs'>Seller</th>";
                            table += "<th></th>";
                        }
                        $.each(lReturn.list, function (index, item) {
                            table += '<tr><td>' + item["UserName"] + '</td><td>' + item["Room"] + '</td><td class="hidden-xs">' + item["ValidSince"] + '</td><td class="hidden-xs">' + item["ValidTill"] + '</td><td class="hidden-xs">' + item["TimeCredit"] + '</td><td class="hidden-xs">' + item["VolumeDown"] + '</td><td class="hidden-xs">' + item["VolumeUp"] + '</td>';
                            if (cookieRol != "3") table += '<td class="hidden-xs">' + item["fdsuser"] + '</td>';
                            table += '<td class="text-center"><a href="/User.aspx?id=' + item["IdUser"] + '&return=DisabledUsers.aspx"><i class=\"fa fa-pencil\"></i></td></tr>';
                        });
                        table += "</table>";



                        $("#searchResult").html(table);

                        var pagination = "<ul class='pagination'>";
                        var points1 = 0;
                        var points2 = 0;
                        x = parseInt(page);
                        for (var index = 0; index < lReturn.pageNumber; index++) {
                            if (lReturn.pageNumber <= 10) {
                                if (page == index)
                                    pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                else
                                    pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                            }
                            else {
                                if (page == index) {
                                    pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                                else if (index == 0) {
                                    pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                                else if (index == (lReturn.pageNumber - 1)) {
                                    pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                }
                                else if (index < (x - 3)) {
                                    if (points1 == 0) {
                                        pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                        points1 = 1;
                                    }
                                }
                                else if (index > (x + 3)) {
                                    if (points2 == 0) {
                                        pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                        points2 = 1;
                                    }
                                }
                                else
                                    pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                            }
                        }

                        pagination += "</ul>";

                        $("#searchResult").append(pagination);

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#modal-loading").hide();
                        $("#searchResult").html(xhr.statusText)
                    }
                });

                $("#members").change(function (e) {
                    ChangeMember();
                });

                function ChangeMember() {
                    var idmember = $("#members").val();
                    var idmembertype = $('option:selected', $("#members")).attr('rel');
                    if (idmember != "0") {
                        if (idmembertype != "1") {

                            var dataString = 'action=billingTypesregular&idmember=' + idmember + '&idmembertype=' + idmembertype;
                            $.ajax({
                                url: "handlers/SMIHandler.ashx",
                                data: dataString,
                                contentType: "application/json; charset=utf-8",
                                dataType: "text",
                                success: function (pReturn) {
                                    var lReturn = JSON.parse(pReturn);
                                    bindSelect("billingtypes", "IdBillingType", "Description", lReturn.list);

                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    $("#modal-loading").hide();
                                    $("#searchResult").html(xhr.statusText)
                                }
                            });

                            var dataString3 = 'action=Rooms&idmember=' + idmember + '&idmembertype=' + idmembertype;
                            $.ajax({
                                url: "handlers/SMIHandler.ashx",
                                data: dataString3,
                                contentType: "application/json; charset=utf-8",
                                dataType: "text",
                                success: function (pReturn) {
                                    var lReturn = JSON.parse(pReturn);
                                    bindSelect("rooms", "IdRoom", "Name", lReturn.list);
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    $("#message").removeClass().addClass("alert alert-error");
                                    $("#message").html(xhr.statusText);
                                    $("#message").show();
                                    $("#modal-loading").hide();
                                }
                            });

                            var cookieLocation = readCookie("location");
                            if (cookieLocation != null)
                                idmember = "";
                            var dataStringGroup = 'action=FDSUSERS&idmember=' + idmember + '&idmembertype=' + idmembertype;
                            $.ajax({
                                url: "handlers/SMIHandler.ashx",
                                data: dataStringGroup,
                                contentType: "application/json; charset=utf-8",
                                dataType: "text",
                                success: function (pReturn) {
                                    var lReturn = JSON.parse(pReturn);
                                    bindSelect("sellers", "idFDSUser", "Name", lReturn.list);

                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    $("#modal-loading").hide();
                                    $("#searchResult").html(xhr.statusText)
                                }
                            });
                          
                        }
                        else {
                            $("#sellers").empty();
                            $("#billingtypes").empty();
                            $("#rooms").empty();
                            $("#Priority").empty();
                            $("#filterprofile").empty();
                            var cookie = readCookie("wifi360-language");

                            var option;
                            if (cookie != null) {
                                switch (cookie) {
                                    case "es": option = '<option value="0">No disponible para grupos</option>'; break;
                                    default: option = '<option value="0">Not avalaible for groups </option>'; break;
                                }
                            }
                            else
                                option = '<option value="0">Not avalaible for groups </option>';

                            $("#billingtypes").append(option);
                            $("#rooms").append(option);
                            $("#Priority").append(option);
                            $("#filterprofile").append(option);
                            $("#sellers").append(option);
                        }
                    }
                    else {
                        $("#billingtypes").empty();
                        $("#rooms").empty();
                        $("#Priority").empty();
                        $("#filterprofile").empty();
                        $("#sellers").empty();

                        var cookie = readCookie("wifi360-language");

                        var option;
                        if (cookie != null) {
                            switch (cookie) {
                                case "es": option = '<option value="0">No disponible</option>'; break;
                                default: option = '<option value="0">Not avalaible</option>'; break;
                            }
                        }
                        else
                            option = '<option value="0">Not avalaible</option>';

                        $("#billingtypes").append(option);
                        $("#rooms").append(option);
                        $("#Priority").append(option);
                        $("#filterprofile").append(option);
                        $("#sellers").append(option);
                    }
                };

                $('#search').click(function (e) {
                    e.preventDefault();

                    var start = $("#start").val();
                    var end = $("#end").val();

                    var s = moment(start, "DD/MM/yyyy HH:mm");
                    var e = moment(end, "DD/MM/yyyy HH:mm");
                    if ((($("#start").val() == '') && ($("#end").val() == '')) || (s <= e) && ($("#start").val() != '') && ($("#end").val() != '')) {

                        var page = 0;
                        var username = $("#username").val();
                        var start = $("#start").val();
                        var end = $("#end").val();
                        var billing = $("#billingtypes").val();
                        var room = $("#rooms").val();
                        var idmember = $("#members").val();
                        var idmembertype = $('option:selected', $("#members")).attr('rel');
                        var idfdsuser = $("#sellers").val();
                        var cookieRol = readCookie("FDSUserRol");

                        var dataString = 'action=DISABLEDUSERS&page=' + page + '&username=' + username + '&start=' + start + '&end=' + end + '&billing=' + billing + '&room=' + room + '&idmember=' + idmember + '&idmembertype=' + idmembertype + '&idfdsuser=' + idfdsuser;
                        $.ajax({
                            url: "handlers/SMIHandler.ashx",
                            data: dataString,
                            contentType: "application/json; charset=utf-8",
                            dataType: "text",
                            beforeSend: function () {
                                $("#myModalWait").modal('show');
                            },
                            complete: function () {
                                $("#myModalWait").modal('hide');
                            },

                            success: function (pReturn) {
                                var lReturn = JSON.parse(pReturn);
                                var table = "";
                                var cookie = readCookie("wifi360-language");
                                if (cookie != null) {
                                    switch (cookie) {
                                        case "es": table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><th>Nombre de usuario</th><th>Motivo</th><th class='hidden-xs'>Fecha de inicio</th><th class='hidden-xs'>Fecha de finalización</th><th class='hidden-xs'>Crédito de tiempo</th><th class='hidden-xs'>Volumen Descarga</th><th class='hidden-xs'>Volumen Subida</th>";
                                            if (cookieRol != "3") table += "<th class='hidden-xs'>Vendedor</th>";
                                            table += "<th></th>"; break;
                                        default: table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><th>Username</th><th>Reason</th><th class='hidden-xs'>Valid Since</th><th class='hidden-xs'>Valid Till</th><th class='hidden-xs'>Time Credit</th><th class='hidden-xs'>Volume Down</th><th class='hidden-xs'>Volume Up</th>";
                                            if (cookieRol != "3") table += "<th class='hidden-xs'>Seller</th>";
                                            table += "<th></th>"; break;
                                    }
                                }
                                else {
                                    table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><th>Username</th><th>Reason</th><th class='hidden-xs'>Valid Since</th><th class='hidden-xs'>Valid Till</th><th class='hidden-xs'>Time Credit</th><th class='hidden-xs'>Volume Down</th><th class='hidden-xs'>Volume Up</th>";
                                    if (cookieRol != "3") table += "<th class='hidden-xs'>Seller</th>";
                                    table += "<th></th>";
                                }

                                $.each(lReturn.list, function (index, item) {
                                    table += '<tr><td>' + item["UserName"] + '</td><td>' + item["Room"] + '</td><td class="hidden-xs">' + item["ValidSince"] + '</td><td class="hidden-xs">' + item["ValidTill"] + '</td><td class="hidden-xs">' + item["TimeCredit"] + '</td><td class="hidden-xs">' + item["VolumeDown"] + '</td><td class="hidden-xs">' + item["VolumeUp"] + '</td>';
                                    if (cookieRol != "3") table += '<td class="hidden-xs">' + item["fdsuser"] + '</td>';
                                    table += '<td class="text-center"><a href="/User.aspx?id=' + item["IdUser"] + '&return=DisabledUsers.aspx"><i class=\"fa fa-pencil\"></i></td></tr>';
                                });
                                table += "</table>";



                                $("#searchResult").html(table);

                                var pagination = "<ul class='pagination'>";
                                var points1 = 0;
                                var points2 = 0;
                                x = parseInt(page);
                                for (var index = 0; index < lReturn.pageNumber; index++) {
                                    if (lReturn.pageNumber <= 10) {
                                        if (page == index)
                                            pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                        else
                                            pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                    }
                                    else {
                                        if (page == index) {
                                            pagination += "<li class='paginate_button active'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                        }
                                        else if (index == 0) {
                                            pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                        }
                                        else if (index == (lReturn.pageNumber - 1)) {
                                            pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                        }
                                        else if (index < (x - 3)) {
                                            if (points1 == 0) {
                                                pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                                points1 = 1;
                                            }
                                        }
                                        else if (index > (x + 3)) {
                                            if (points2 == 0) {
                                                pagination += "<li class='paginate_button'><a href='#'>...</a></li>";
                                                points2 = 1;
                                            }
                                        }
                                        else
                                            pagination += "<li class='paginate_button'><a href='#' rel='page' attr='" + index + "'>" + (index + 1) + "</a></li>";
                                    }
                                }

                                pagination += "</ul>";

                                $("#searchResult").append(pagination);
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                $("#modal-loading").hide();
                                $("#searchResult").html(xhr.statusText)
                            }
                        });
                    }
                    else {
                        $("#message").html(GetMessage('0x0022'));
                        $("#myModal").modal('show');
                    }
                });

            });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 id="headerpageusersdisabled">Users management</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="default.aspx">Dashboard</a>
                </li>
                <li>
                    <a id="breadpagefatherusersdisabled">Users management</a>
                </li>
                <li class="active">
                    <strong id="breadpageusersdisabled">Disabled Users</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5 id="labelboxheaderusersdisabled">Disabled Users<small> List of disabled users</small></h5>
                </div>
                <div class="ibox-content">

                    <div class="ibox border-bottom">
                    <div class="ibox-title">
                        <h5 id="headerboxfilterusersdisabled">Search filters</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-down"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content" style="display: none;">
                        <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto;">
                            <div class="scroll_content" style="overflow: hidden; width: auto; ">
                            <div role="form">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-9">
                                            <label for="username" id="labelformusersdisableduser">Users</label>
                                            <input type="text" id="username" class="form-control"/>
                                        </div>
                                        <div class="col-sm-3">
                                            <label for="username" id="labelformusermanagementseller">Sellers</label>
                                            <select id="sellers" class="form-control"></select>
                                        </div>
                                    </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label for="username" id="labelforusersdisabledgroupmembers">Group Members</label>
                                        <select id="members" class="form-control"></select>
                                    </div>

                                    <div class="col-sm-4">
                                        <label for="username" id="labelforusersdisabledbillingtype">Access type</label>
                                        <select id="billingtypes" class="form-control"></select>
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="username" id="labelformusersdisabledroom">Room</label>
                                        <select id="rooms" class="form-control"></select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="data_5">
                                <label id="labelformusersdisableddaterange"> Date range</label>
                                <div class="input-daterange input-group" id="datepicker">
                                        <span class="input-group-addon" id="labelformusersdisableddatefrom"> From </span>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" id="start" name="start" value=""/>
                                    <span class="input-group-addon" id="labelformusersdisableddatesince"> to </span>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" id="end" name="end" value="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <a href="#" id="search" class="btn btn-w-m btn-primary" ><i class="fa fa-search"></i> <span id="labelformusersdisabledbuttonsearch">Search</span> </a>
                            </div>
                        </div>
                        </div><div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 198.02px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                    </div>
                </div>
                    
                    <div id="searchResult"></div>

                        </div>
                    </div>
                </div>
            </div>

                <div class="modal inmodal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Cerrar</span></button>
                                <h4 class="modal-title">Error</h4>
                            </div>
                            <div class="modal-body">
                                <p class="alert alert-danger" id="message"></p>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

        </div>
</asp:Content>
