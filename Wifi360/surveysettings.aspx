﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Site.Master" AutoEventWireup="true" CodeBehind="surveysettings.aspx.cs" Inherits="Wifi360.surveysettings" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>

        function readCookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        };

        function getParameters() {
            var searchString = window.location.search.substring(1)
              , params = searchString.split("&")
              , hash = {}
            ;

            for (var i = 0; i < params.length; i++) {
                var val = params[i].split("=");
                hash[unescape(val[0])] = unescape(val[1]);
            }
            return hash;
        }
        var parameters = getParameters();

        $(document).ready(function (e) {

            var dataString3 = 'action=selectSurvey' + '&idsite=' + parameters.idsite;
            $.ajax({
                url: "handlers/SMIHandler.ashx",
                data: dataString3,
                contentType: "application/json; charset=utf-8",
                dataType: "text",
                beforeSend: function () {
                    $("#myModalWait").modal('show');
                },
                complete: function () {
                    $("#myModalWait").modal('hide');
                },
                success: function (pReturn) {
                    var lReturn = JSON.parse(pReturn);
                    var table = "";
                    var cookie = readCookie("wifi360-language");
                    if (cookie != null) {
                        switch (cookie) {
                            case "es": table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>Idioma</th><th class='hidden-xs'>Orden</th><th>Pregunta</th><th class='hidden-xs'>Tipo</th><th class='hidden-xs'>Valores</th><th></th></tr>"; break;
                            default: table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>language</th><th class='hidden-xs'>Order</th><th>Question</th><th class='hidden-xs'>Type</th><th class='hidden-xs'>Values</th><th></th></tr>"; break;
                        }
                    }
                    else
                        table += "<table class='table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline'><tr><th>language</th><th class='hidden-xs'>Order</th><th>Question</th><th class='hidden-xs'>Type</th><th class='hidden-xs'>Values</th><th></th></tr>";
                    $.each(lReturn.list, function (index, item) {
                        table += '<tr><td>' + item["Language"] + '</td><td class="hidden-xs">' + item["Order"] + '</td><td>' + item["Question"] + '</td><td class="hidden-xs">' + item["Type"] + '</td><td class="hidden-xs">' + item["Values"] + '</td><td><a href="/surveyquestion.aspx?id=' + item["IdSurvey"] + '&idsite=' + parameters.idsite + '&return=' + parameters.return + '"><i class="fa fa-pencil"></i></a></td></tr>';
                    });

                    table += "</table>";

                    $("#searchResult").html(table);


                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#modal-loading").hide();
                    $("#searchResult").html(xhr.statusText)
                }
            });

            
            //Return Control
            if (parameters.idsite != undefined) {
                $("#div_buttons").show();
            }
            else {
                $("#div_buttons").hide();
            }

            $("#back").click(function (e) {
                e.preventDefault();                               
                parameters = getParameters();
                url = "location.aspx";
                if( parameters.id != undefined)
                    url = "location.aspx?id=" + parameters.id;

                if (parameters.idsite != undefined) {
                    url = parameters.return + "?id=" + parameters.idsite;
                    if (parameters.idlocation != undefined)
                        url += "?idlocation=" + parameters.idlocation;
                }
                window.location.href = url;
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 id="headerpagelocationsetting">Settings</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="default.aspx">Dashboard</a>
                </li>
                <li id="breadpagefatherlocationsetting">
                    <a>Settings</a>
                </li>
                <li class="active">
                    <strong id="breadpagesurveysetting">Survey settings</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                    <h5 id="labelboxheadersurvey">Survey settings<small> Manage survey settings.</small></h5>
                </div>
                    <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <a href="surveyquestion.aspx" id="new" class="btn btn-primary"><i class="fa fa-plus"></i> <span id="labelformsurveybuttonnew">New</span></a>
                        </div>
                        <div class="col-lg-12">
                            <div id="searchResult"></div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>

            <div id="div_buttons" class="col-lg-3">
                    <div class="ibox-title">
                        <h5 id="labelboxheaderbuttonsettings">Actions over locations</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <p>
                            <a href="#" role="button" class="btn btn-w-m btn-lg btn-default btn-block" id="back"><i class="fa fa-backward"></i> <span id="labelsettingsbuttonback">Back </span></a>

                        </p>
                    </div>
                </div>

            </div>
            </div>


            <div class="modal inmodal fade" id="myModalMessage" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                            </div>
                            <div class="modal-body">
                                <div id="message">
                                    
                                </div>
                            </div>
                          <div class="modal-footer">
                            <a href="#" class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Volver</a>
                          </div>
                        </div>
                    </div>
                </div>
</asp:Content>
