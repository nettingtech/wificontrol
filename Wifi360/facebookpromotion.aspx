﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Site.Master" AutoEventWireup="true" CodeBehind="facebookpromotion.aspx.cs" Inherits="Wifi360.facebookpromotion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/plugins/datapicker/datepicker3.css" rel="stylesheet" />
    <script type="text/javascript">

        function readCookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        };


        $(document).ready(function () {

            $("#lisettings").attr("class", "active");
            $("#ulsettings").attr("class", "nav nav-second-level collapse in");
            $("#lifacebooksettings").attr("class", "active");
            $("#lidashboard").removeClass();
            var cookie = readCookie("wifi360-language");
            if (cookie != null) {
                switch (cookie) {
                    case "es": $('#date_1 .input-daterange').datepicker({
                        keyboardNavigation: false,
                        forceParse: false,
                        autoclose: true,
                        format: "dd/mm/yy",
                        language: "es",
                        todayHighlight: true,
                        weekStart: 1
                    });
                        $('#date_2 .input-daterange').datepicker({
                            keyboardNavigation: false,
                            forceParse: false,
                            autoclose: true,
                            format: "dd/mm/yy",
                            language: "es",
                            todayHighlight: true,
                            weekStart: 1
                        });
                        break;
                    default: $('#date_1 .input-daterange').datepicker({
                        keyboardNavigation: false,
                        forceParse: false,
                        autoclose: true,
                        format: "dd/mm/yy",
                        language: "e",
                        todayHighlight: true,
                        weekStart: 1
                    });
                        $('#date_2 .input-daterange').datepicker({
                            keyboardNavigation: false,
                            forceParse: false,
                            autoclose: true,
                            format: "dd/mm/yy",
                            language: "en",
                            todayHighlight: true,
                            weekStart: 1
                        });
                        break;
                }
            }
            else {
                $('#date_1 .input-daterange').datepicker({
                    keyboardNavigation: false,
                    forceParse: false,
                    autoclose: true,
                    format: "dd/mm/yy",
                    language: "e",
                    todayHighlight: true,
                    weekStart: 1
                });
                $('#date_2 .input-daterange').datepicker({
                    keyboardNavigation: false,
                    forceParse: false,
                    autoclose: true,
                    format: "dd/mm/yy",
                    language: "en",
                    todayHighlight: true,
                    weekStart: 1
                });
            }


                    var dataString2 = 'action=locations';
                    $.ajax({
                        url: "handlers/SMIHandler.ashx",
                        data: dataString2,
                        contentType: "application/json; charset=utf-8",
                        dataType: "text",
                        beforeSend: function () {
                            $("#myModalWait").modal('show');
                        },
                        complete: function () {
                            $("#myModalWait").modal('hide');
                        },
                        success: function (pReturn) {
                            var lReturn = JSON.parse(pReturn);
                            bindSelect('location', 'Idlocation', 'Description', lReturn.list);
                            var dataString3 = 'action=billingTypesModule&id=3';
                            $.ajax({
                                url: "handlers/SMIHandler.ashx",
                                data: dataString3,
                                contentType: "application/json; charset=utf-8",
                                dataType: "text",
                               
                                success: function (pReturn) {
                                    var lReturn = JSON.parse(pReturn);
                                    bindSelect('billingtype', 'IdBillingType', 'Description', lReturn.list);

                                    var parameters = getParameters();

                                    if (parameters.id != undefined) {

                                        var dataString = 'action=selectfacebookpromotion&id=' + parameters.id;
                                        $.ajax({
                                            url: "handlers/SMIHandler.ashx",
                                            data: dataString,
                                            contentType: "application/json; charset=utf-8",
                                            dataType: "text",
                                           
                                            success: function (pReturn) {
                                                var lReturn = JSON.parse(pReturn);
                                                $("#pageName").val(lReturn.PageName);
                                                $("#page").val(lReturn.Page);
                                                $("#pageID").val(lReturn.PageID);
                                                $("#validdays").val(lReturn.ValidDays);
                                                $("#location").val(lReturn.IdLocation);
                                                $("#billingtype").val(lReturn.IdBillingType);
                                                $("#visible").attr('checked', lReturn.Visible);
                                                $("#creationdate").val(lReturn.CreationDate);
                                                $("#enddate").val(lReturn.EndDate);
                                                $("#order").val(lReturn.Order);

                                                $("#myModalWait").modal('hide');

                                            },
                                            error: function (xhr, ajaxOptions, thrownError) {
                                                $("#modal-loading").hide();
                                                $("#searchResult").html(xhr.statusText)
                                            }
                                        });
                                    }

                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    $("#modal-loading").hide();
                                    $("#searchResult").html(xhr.statusText)
                                }
                            });

                           
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#modal-loading").hide();
                            $("#searchResult").html(xhr.statusText)
                        }
                    });

                    function save() {

                        var parameters = getParameters();
                        var data = {};
                        data["action"] = 'SAVEFACEBOOKPROMOTION';

                        data["id"] = parameters.id;

                        if (!$.isNumeric(parameters.id))
                            data["id"] = "undefined";

                        data["page"] = $("#page").val();
                        data["pageName"] = $("#pageName").val();
                        data["pageID"] = $("#pageID").val();
                        data["validdays"] = $("#validdays").val();
                        data["location"] = $("#location").val();
                        data["creationdate"] = $("#creationdate").val();
                        data["enddate"] = $("#enddate").val();
                        data["billingtype"] = $("#billingtype").val();
                        data["order"] = $("#order").val();
                        data["visible"] = $("#visible").is(":checked") ? "True" : "False";

                       // var dataString = 'action=savefacebookpromotion&id=' + parameters.id + '&location=' + location + '&billingtype=' + billingtype + '&page=' + page + '&pageName=' + pageName + '&pageID=' + pageID + '&validdays=' + validdays + '&visible=' + visible + '&creationdate=' + creationdate + '&enddate=' + enddate + '&order=' + order;
                        $.ajax({
                            url: "handlers/SMIHandler.ashx",
                            type: "POST",
                            data: JSON.stringify(data),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            beforeSend: function () {
                                $("#myModalWait").modal('show');
                            },
                            complete: function () {
                                $("#myModalWait").modal('hide');
                            },
                            success: function (msg) {
                                if (msg.code == 'OK') {
                                    $("#message").removeClass().addClass("alert alert-success");
                                    $("#message").html("Promoción de Facebook guardada correctamente.");
                                }
                                else {
                                    $("#message").removeClass().addClass("alert alert-danger");
                                    $("#message").html(msg.message);
                                }

                                $("#myModalMessage").modal('show');

                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                $("#message").removeClass().addClass("alert alert-error");
                                $("#message").html(xhr.statusText);
                                $("#message").show();
                                $("#modal-loading").hide();

                            }
                        });
                    };

                    $("#save").on("click", function (e) {
                        e.preventDefault();
                        save();
                    });

                    $("#back").on("click", function (e) {
                        e.preventDefault();
                        window.location = "facebooksettings.aspx";
                    });
                });

                function getParameters() {
                    var searchString = window.location.search.substring(1)
                      , params = searchString.split("&")
                      , hash = {}
                    ;

                    for (var i = 0; i < params.length; i++) {
                        var val = params[i].split("=");
                        hash[unescape(val[0])] = unescape(val[1]);
                    }
                    return hash;
                }

                function bindSelect(id, valueMember, displayMember, source) {
                    $("#" + id).find('option').remove().end();
                    var cookie = readCookie("wifi360-language");
                    if (cookie != null) {
                        switch (cookie) {
                            case "es": $("#" + id).append('<option value="0">Seleccionar</option>');break;
                            default: $("#" + id).append('<option value="0">Select</option>');break;
                        }
                    }
                    else
                         $("#" + id).append('<option value="0">Seleccionar</option>');
                    $.each(source, function (index, item) {
                        $("#" + id).append('<option value="' + item[valueMember] + '"> ' + item[displayMember] + '</option>');
                    });
                };

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 id="headerpagelocationsetting">Settings</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="default.aspx">Dashboard</a>
                </li>
                <li>
                    <a id="breadpagefatherlocationsetting">Settings</a>
                </li>
                <li>
                    <a href="facebooksettings.aspx" id="breadpagefacebooksetting">Facebook promotions</a>
                </li>
                <li class="active" id="breadpagefacebooksettingactive">
                    <strong>Facebook Promotion</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-9">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                    <h5 id="labelboxheaderfacebookpromotion">Facebook Promotion<small> Details of promotion settings</small></h5>
                </div>
                    <div class="ibox-content">
                     <div class="form-horizontal">
                    <div class="form-group"> <label class="control-label col-sm-4" id="labelformfacebookpromotionpagename">Page name</label><div class="col-sm-8"><input type="text" class="form-control" id="pageName" /></div></div>
                    <div class="form-group"> <label class="control-label col-sm-4" id="labelformfacebookpromotionpageurl">Page URL</label><div class="col-sm-8"><input type="text" class="form-control" id="page" /></div></div>
                    <div class="form-group"> <label class="control-label col-sm-4" id="labelformfacebookpromotionpageid">Page Id</label><div class="col-sm-8"><input type="text" class="form-control" id="pageID" /></div></div>
                    <div class="form-group" id="date_1"> <label class="control-label col-sm-4" id="labelformfacebookpromotiondatestart">Start Date</label><div class="col-sm-8"><div class="input-daterange input-group" id="Div1"><span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text"class="form-control"  id="creationdate" /></div></div></div>
                    <div class="form-group" id="date_2"> <label class="control-label col-sm-4" id="labelformfacebookpromotiondateend">End Date</label><div class="col-sm-8"><div class="input-daterange input-group" id="datepicker"><span class="input-group-addon"><i class="fa fa-calendar"></i></span> <input type="text" class="form-control"  id="enddate" /></div></div></div>
                    <div class="form-group"> <label class="control-label col-sm-4" id="labelformfacebookpromotionvaliddays">Valid days</label><div class="col-sm-8"><input type="number" id="validdays" class="form-control" /></div></div>
                    <div class="form-group"> <label class="control-label col-sm-4" id="labelformfacebookpromotionlocation">Location</label><div class="col-sm-8"><select id="location" class="form-control" ></select></div></div>
                    <div class="form-group"> <label class="control-label col-sm-4" id="labelformfacebookpromotionaccesstype">Access Type</label><div class="col-sm-8"><select id="billingtype" class="form-control" ></select></div></div>
                    <div class="form-group"> <label class="control-label col-sm-4" id="labelformfacebookpromotionorder">Order</label><div class="col-sm-8"><input type="number" id="order" class="form-control" /></div></div>
                    <div class="form-group"> <label class="control-label col-sm-4">Visible</label><div class="col-sm-8"><input type="checkbox" id="visible" /></div></div>


                </div>
                </div>
            </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5 id="labelboxheaderacitonfacebookpromotion">Accions over promotion</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <p>
                            <a href="#" class="btn btn-w-m btn-lg btn-primary btn-block" id="save"><i class="fa fa-save"></i> <span id="labelformlocationsettingsbuttonsave">Save</span></a>
                            <a href="#" role="button" class="btn btn-w-m btn-lg btn-default btn-block" id="back" data-toggle="modal"><i class="fa fa-backward"></i><span id="labelformlocationsettingsbuttonback">Back</span></a>

                        </p>
                    </div>
                </div>
            </div>

            <div class="modal inmodal fade" id="myModalMessage" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                            </div>
                            <div class="modal-body">
                                <div id="message">
                                    
                                </div>
                            </div>
                          <div class="modal-footer">
                            <a href="#" class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Volver</a>
                          </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>

</asp:Content>
