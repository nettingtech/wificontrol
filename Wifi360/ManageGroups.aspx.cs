﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Wifi360
{
    public partial class ManageGroups : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ;
            try
            {
                HttpCookie cookieRol = Request.Cookies["FDSUserRol"];
                if (!cookieRol.Value.Equals("4"))
                    HttpContext.Current.Response.Redirect("default.aspx", false);
            }
            catch
            {
                HttpContext.Current.Response.Redirect("login.aspx", false);
            }
        }
    }
}