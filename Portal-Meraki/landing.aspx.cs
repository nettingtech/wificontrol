﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Text;
using System.Net;
using System.IO;
using Wifi360.Data.Class;
using Wifi360.Data.Controllers;
using Wifi360.Data.Model;

namespace Portal_Meraki
{
    public partial class landing : System.Web.UI.Page
    {

        protected void Page_Init(object sender, EventArgs e)
        {     
            List<ParametrosConfiguracion> parametros = BillingController.GetParametrosConfiguracion(0);
            ParametrosConfiguracion key = (from t in parametros where t.Key.ToUpper().Equals("ENCRYPTIONKEY") select t).FirstOrDefault();
            try
            {
                string nt_token = Request.Params["token"];
                string[] nt_token_decrypt = Misc.Decrypt(nt_token,key.value).Split('|');
                string[] nt_token_decrypt_A = nt_token_decrypt[0].Split('=');              
                string[] nt_token_decrypt_B = nt_token_decrypt[1].Split('=');
                if (nt_token_decrypt_A[0].Equals("nt_st") && nt_token_decrypt_B[0].Equals("nt_lt"))
                {

                    Locations2 nt_location = new Locations2();
                    nt_location = BillingController.GetLocation(Int32.Parse(nt_token_decrypt_B[1]));
                    if (nt_location.IdHotel != Int32.Parse(nt_token_decrypt_A[1]))
                    {
                        Response.Redirect("error.aspx");
                    }
                    var ap_mac = Request.Params["ap_mac"];
                    var ap_ssid = Request.Params["ap_name"];
                    var macs = Request.Params["client_mac"];
                    var origin = Request.Params["login_url"].Split('&')[0];
                    var urlDest = Request.Params["continue_url"];
                    var error = Request.Params["error_message"];
                    var nt_crc_plaint = nt_token_decrypt[0].ToString() +"|"+ nt_token_decrypt[1].ToString() + " |nt_client_mac=" + macs + "|nt_origin=" + origin + "|nt_urlDest=" + urlDest + "|nt_error=" + error + "|nt_ap_mac=" + ap_mac + "|nt_app_ssid=" + ap_ssid + "|epoch=" + DateTime.Now.ToUniversalTime();
                    var nt_crc = Misc.Encrypt(nt_crc_plaint,key.value);

                    Session["nt_crc"] = nt_crc;
                    Response.Redirect("~/welcome.aspx", false);
                }
                else
                    Response.Redirect("error.aspx");

            }
            catch
            {
                Response.Redirect("error.aspx");
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //83% of people not see this
        }
    }
}