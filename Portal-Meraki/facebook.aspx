﻿<%@ Page Title="" Language="C#" MasterPageFile="~/resources/master/MerakiSecureMaster.Master" AutoEventWireup="true" CodeBehind="facebook.aspx.cs" Inherits="Portal_Meraki.facebook" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <script type="text/javascript">
            $(document).ready(function () {
                var cookie = getCookie("meraki");
                if (cookie != "") {
                    $("#idsocialbillintype").val(cookie.split('=')[1]);
                }
                else
                    alert("No hay cookie");

                var lang = navigator.browserLanguage ? navigator.browserLanguage : navigator.language;
                var langcode = lang.substring(0, 2);
                if ((langcode != 'es') && (langcode != 'en'))
                    langcode = $("#languagecode").val();
             

                $("#email").val($("#facebookemail").val());
                var survey = 'facebook: nombre=' + $("#facebookname").val() + '|' + 'apellidos=' + $("#facebooksurname").val() + '|';
                if ($("#facebookgender").val() != "") {
                    survey += 'sexo=' + $("#facebookgender").val() + '|';
                }
                if ($("#facebookbirthday").val()  != "") {
                    survey += 'nacimiento=' + $("#facebookbirthday").val() + '|';
                }
                survey += 'id=' + $("#facebookid").val() + '|';

                if (lang.substring(0, 2) == 'es') {
                    $("#userInformation").html("");
                    $("#userInformation").append("<h3>Información del Usuario</h3>");
                    $("#userInformation").append("Email: <b>" + $("#facebookemail").val() + "</b><br />");
                    $("#userInformation").append("Nombre: <b>" +$("#facebookname").val() + "</b><br />");
                    $("#userInformation").append("Apellidos: <b>" + $("#facebooksurname").val()+ "</b><br />");
                }
                else {
                    $("#userInformation").html("");
                    $("#userInformation").append("<h3>User Information </h3>");
                    $("#userInformation").append("Email: <b>" + $("#facebookemail").val() + "</b><br />");
                    $("#userInformation").append("First Name: <b>" + $("#facebookname").val()+ "</b><br />");
                    $("#userInformation").append("Last Name: <b>" + $("#facebooknsurame").val() + "</b><br />");
                }


                $("#surveytext").val(survey);
                var dataStringm = 'action=socialnetwork&email=' + $("#facebookemail").val() + '&mac=' + $("#mac").val() + '&idhotel=' + $("#idhotel").val() + '&idlocation=' + $("#idlocation").val() + '&origin=' + $("#origin").val() + '&survey=' + survey + "&lang=" + langcode + "&idbillingtype=" + $("#idsocialbillintype").val();
                $.ajax({
                    url: "../resources/handlers/MerakiHandler.ashx",
                    data: dataStringm,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    beforeSend: function () {
                        $("#MyModalWait").modal('show');
                    },
                    complete: function () {
                        $("#MyModalWait").modal('hide');
                    },
                    success: function (pResult) {
                        var lResult = JSON.parse(pResult);
                        if (lResult.IdUser == -1) {
                            if (langcode != 'es') {
                                $("#message").html("No more free access are allowed now.");
                            }
                            else {
                                $("#message").html("No se permiten más accesos gratuitos en este periodo.");
                            }
                            $("#myModalError").modal('show');
                        }
                        else if (lResult.IdUser == -3) {
                            if (langcode != 'es') {
                                $("#message").html("We are currently experiencing a problem, please try again later.");
                            }
                            else {
                                $("#message").html("Tenemos un problema temporal, intentelo de nuevo más tarde.");
                            }
                            $("#myModalError").modal('show');
                        }
                        else if (lResult.IdUser == 0) {
                            if (langcode != 'es') {
                                $("#message").html(lResult.UserName);
                            }
                            else {
                                $("#message").html(lResult.UserName);
                            }
                            $("#myModalError").modal('show');
                        }
                        else {
                            $("#finishbutton").attr("href", lResult.Url);
                            $("#finishInformation").append($("#socialaccessinformation").html());

                            if (langcode == 'es') {
                                $("#finishlabel").html("Bienvenido de nuevo " + $("#facebookname").val());
                                $("#finishInformation").append('<b>Nombre de usuario: </b>' + lResult.UserName + '<br />');
                                $("#finishInformation").append('<b>Contraseña: </b>' + lResult.Password + '<br />');
                                $("#finishInformation").append('<b>Fecha de expiración: </b>' + lResult.ValidTill + '<br />');
                                $("#finishbutton").html("Continuar navegando");
                            }
                            else {
                                $("#finishlabel").html("Welcome again " + $("#facebookname").val());
                                $("#finishInformation").append('<b>UserName: </b>' + lResult.UserName + '<br />');
                                $("#finishInformation").append('<b>Password: </b>' + lResult.Password + '<br />');
                                $("#finishInformation").append('<b>Valid Till: </b>' + lResult.ValidTill + '<br />');
                                $("#finishbutton").html("Continue browsing");
                            }

                            var val = "username=" + lResult.UserName + "&password=" + lResult.Password + "&validtill=" + lResult.ValidTill + "&continue_url=" + lResult.Url;
                            createCookie("merakiuser", val, 1);

                            var continue_url = window.location.origin + "/Success.aspx?username=" + lResult.UserName + "&continue_url=" + $("#origin").val();
                            var action = $("#origin").val();
                            var newForm = jQuery('<form>', {
                                'action': action,
                                'method': 'POST',
                            }).append(jQuery('<input>', {
                                'name': 'email',
                                'value': lResult.UserName,
                                'type': 'hidden'
                            })).append(jQuery('<input>', {
                                'name': 'password',
                                'value': lResult.Password,
                                'type': 'hidden'
                            })).append(jQuery('<input>', {
                                'name': 'success_url',
                                'value': continue_url,
                                'type': 'hidden'
                            })).append(jQuery('<input>', {
                                'name': 'continue_url',
                                'value': lResult.message,
                                'type': 'hidden'

                            }));
                            $(document.body).append(newForm);
                            newForm.submit();
                        }

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#message2").html("We have a problem, please try again later.");
                        alert('error: ' + xhr.statusText);
                    }
                });
            });


                $("#finishbutton").click(function (e) {
                    e.preventDefault();
                    var url = $("#finishbutton").attr("href");
                    window.location.href = url;

                });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input type="hidden" name="code" id="idhotel" value="" />
        <input type="hidden" name="code" id="idlocation" value="" />
        <input type="hidden" name="code" id="mac" value="" />
        <input type="hidden" name="code" id="origin" value="" />
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">

            </div>
        </div>
    </div>

    <div class="modal fade" id="myModalError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title text-center" id="H2">Error</h4>
          </div>
          <div class="modal-body">
            <div id="message" class="alert alert-danger text-center"></div> 
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="errorbutton">Close</button>
          </div>
        </div>
      </div>
    </div>

        <div class="modal fade" id="myModalFinish" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title text-center" id="finishlabel">Congratulations</h4>
          </div>
          <div class="modal-body">
                <div id="finishInformation" style="text-align:center" ><img src="resources/images/no-disponoble.png" alt="logo hotel" id="logofinish" style="width:150px"  /><br /><br /></div>
                            <br />
                <input type="button" id="finishbutton" class="btn btn-success btn-block" value="Continue browsing" /> 
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="MyModalWait" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center" id="waitheader">Please, wait a moment</h4>
                </div>
                <div class="modal-body">
                    <div class="progress">
                        <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                            <span class="sr-only">Waiting please</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <div id="socialaccessinformation" style="display:none"></div>
        <input type="text" hidden="hidden" id="idsocialbillintype" />
        <input type="text" hidden="hidden" id="surveytext" />
        <input type="text" hidden="hidden" id="email" />
        <input type="text" hidden="hidden"  id="facebookid" value="<%Response.Write(facebookid);%>" />
        <input type="text" hidden="hidden"  id="facebookemail" value="<%Response.Write(facebookemail);%>"  />
        <input type="text" hidden="hidden" id="facebookname" value="<%Response.Write(facebookname);%>"  />
        <input type="text" hidden="hidden" id="facebooksurname"  value="<%Response.Write(facebooksurname);%>" />
        <input type="text" hidden="hidden" id="facebookgender" value="<%Response.Write(facebookgender);%>"  />
        <input type="text" hidden="hidden"  id="facebooklocale" value="<%Response.Write(facebooklocale);%>"  />
        <input type="text" hidden="hidden"  id="facebookbirthday" value="<%Response.Write(facebookbirthday);%>"  />
</asp:Content>
