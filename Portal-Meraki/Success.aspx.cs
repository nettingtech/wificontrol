﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Wifi360.Data.Controllers;
using Wifi360.Data.Model;

namespace Portal_Meraki
{
    public partial class Success : System.Web.UI.Page
    {

        protected string username = string.Empty;
        protected string validtill = string.Empty;
        protected string timecredit = string.Empty;
        protected string volumecredit = string.Empty;
        protected string accesstype = string.Empty;
        protected string continue_url = string.Empty;
        protected string logout_url = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                HttpCookie meraki = Context.Request.Cookies["meraki"];
                HttpCookie merakiUser = Context.Request.Cookies["merakiuser"];

                MerakiLogoutUrl obj = new MerakiLogoutUrl();
                obj.Idhotel = Int32.Parse(meraki.Values["idhotel"]);
                obj.Idlocation = Int32.Parse(meraki.Values["idlocation"]);
                obj.MAC = meraki.Values["mac"];
                obj.Url = Context.Request.Params["logout_url"];
                obj.DateInserted = DateTime.Now.ToUniversalTime();
                obj.IP = Context.Request.UserHostAddress;
                obj.UserName = merakiUser.Values["username"];
                continue_url = Context.Request.Params["logout_url"];
                logout_url = Context.Request.Params["continue_url"].ToString();

                BillingController.SaveMerakiLogoutUrl(obj);

                SitesGMT siteGMT = GetSiteGMt(obj.Idlocation, obj.Idhotel);

                Users user = BillingController.GetRadiusUser(obj.UserName, obj.Idhotel);
                if (user.IdUser != 0)
                {
                    BillingTypes2 billingtype = BillingController.ObtainBillingType2(user.IdBillingType);
                    username = user.Name;

                    DateTime validTill = DateTime.MinValue;
                    List<ClosedSessions2> closedSessions = BillingController.GetClosedSession(obj.Idhotel, obj.UserName, user.ValidSince, user.ValidTill);
                    if (closedSessions.Count > 0)
                    {
                        ClosedSessions2 closed = (from d in closedSessions orderby d.SessionTerminated descending select d).FirstOrDefault();
                        if (closed != null)
                            validTill = closed.SessionTerminated.Value.AddHours(billingtype.ValidAfterFirstUse);
                    }
                    else
                    {
                        ActiveSessions active = BillingController.GetActiveSession(obj.MAC, obj.Idhotel);
                        if (!active.IdActiveSession.Equals(0))
                            validTill = active.SessionStarted.Value.AddHours(billingtype.ValidAfterFirstUse);
                        else
                            validTill = DateTime.Now.ToUniversalTime().AddHours(billingtype.ValidAfterFirstUse);
                    }

                    if (validTill > user.ValidTill)
                        validtill = user.ValidTill.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                    else
                        validtill = validTill.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");

                    TimeSpan t = TimeSpan.FromSeconds(user.TimeCredit / user.MaxDevices);
                    string time = string.Empty;
                    if (user.TimeCredit >= 86400)
                        time = string.Format("{0} d: {1:D2} h: {2:D2} m: {3:D2} s", t.Days, t.Hours, t.Minutes, t.Seconds);
                    else
                        time = string.Format("{0:D2} h: {1:D2} m: {2:D2} s", t.Hours, t.Minutes, t.Seconds);
                    timecredit = time;
                    volumecredit = string.Format("{0:0.00} MB", Math.Round(double.Parse((user.VolumeDown / 1048576).ToString()), 2));
                    accesstype = billingtype.Description;
                    continue_url = obj.Url;
                }
            }
            catch (Exception ex)
            {
                CaptivePortalLogInternal internallog = new CaptivePortalLogInternal();

                int idhotel = int.Parse(Context.Request.Params["idhotel"].ToString());
                int idlocation = int.Parse(Context.Request.Params["idlocation"].ToString());
                string mac = Context.Request.Params["mac"].ToString();
                string user_name = Context.Request.Params["username"].ToString();
                continue_url = Context.Request.Params["continue_url"].ToString();
                internallog.IdHotel = idhotel;
                internallog.IdLocation = idlocation;
                internallog.CallerID = mac;
                internallog.NomadixResponse = continue_url;
                internallog.RadiusResponse = ex.Message + " trace: " + ex.StackTrace;
                internallog.UserName = user_name;
                internallog.Page = "Success.aspx";
                internallog.NSEId = string.Empty;
                internallog.Password = string.Empty;

                BillingController.SaveCaptivePortalLogInternal(internallog);
            }
        }


        private static SitesGMT GetSiteGMt(int idlocation, int idhotel)
        {
            SitesGMT siteGMT = null;
            Locations2 location = null;
            Hotels hotel = null;
            try
            {
                if (idlocation != 0)
                {
                    location = BillingController.GetLocation(idlocation);
                    hotel = BillingController.GetHotel(location.IdHotel);
                    siteGMT = BillingController.SiteGMTSitebyLocation(location.IdLocation);
                    if (siteGMT.Id.Equals(0))
                        siteGMT = BillingController.SiteGMTSitebySite(location.IdHotel);
                }
                else
                {
                    siteGMT = BillingController.SiteGMTSitebySite(idhotel);
                }
        
                return siteGMT;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        protected void logout_asp_button_Click(object sender, EventArgs e)
        {
            try
            {
                logout_url = Context.Request.Params["logout_url"].ToString();
                logout_url += "&continue_url=" + Context.Request.Url.Scheme + "://" + Context.Request.Url.Host + "/logout.aspx";

                Response.Redirect(logout_url, false);
            }
            catch (Exception ex)
            {
                ;
            }

        }

        protected void change_asp_button_Click(object sender, EventArgs e)
        {
            try
            {
                int idhotel = int.Parse(Context.Request.Params["idhotel"].ToString());
                int idlocation = int.Parse(Context.Request.Params["idlocation"].ToString());
                string mac = Context.Request.Params["mac"].ToString();

                BillingController.DeleteRememberMeTableUser(mac, idhotel);

                logout_url = Context.Request.Params["logout_url"].ToString();
                logout_url += "&continue_url=" + Context.Request.Url.Scheme + "://" + Context.Request.Url.Host + "/logout.aspx";
                Response.Redirect(logout_url, false);
            }
            catch (Exception ex)
            {
                ;
            }
        }
    }
}