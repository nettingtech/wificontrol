﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Portal_Meraki.resources
{
    public partial class error : System.Web.UI.Page
    {
        protected string logout_url = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            logout_url += "&continue_url=" + Context.Request.Url.Scheme + "://" + Context.Request.Url.Host + "/logout.aspx";
        }

        protected void logout_asp_button_Click(object sender, EventArgs e)
        {
            try
            {
                logout_url = Context.Request.Params["logout_url"].ToString();
                logout_url += "&continue_url=" + Context.Request.Url.Scheme + "://" + Context.Request.Url.Host + "/logout.aspx";

                Response.Redirect(logout_url, false);
            }
            catch (Exception ex)
            {
                ;
            }

        }
    }
}