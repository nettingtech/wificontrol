﻿
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="error.aspx.cs" Inherits="Portal_Meraki.resources.error" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <!-- FONT AWESOME CARGADA DESDE SU WEB --> 
    <link href="../../font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- SCRIPTS -->
    <script src="../resources/scripts/jquery.js"></script>
    <script src="../resources/scripts/bootstrap.js" ></script>
    <script src="../resources/scripts/utils.js"></script>
    <script src="../resources/scripts/translate.js"></script>

    <!-- CSS -->
    <link href="/resources/css/bootstrap.css" rel="stylesheet" />
    <link href="/resources/css/bootstrap-theme.css" rel="stylesheet" />
    <title>Error</title>    
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
        <div class="row" id="principal">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="text-center" id="logohead"> </div>
                        <div class="alert alert-error text-center">
                            <h1 id="error_head">¡Vaya!</h1><h2 id="error_line">Si estás en esta página es porque algo no ha funcionado correctamente</h2>
                            <h3 id="error_info">Por favor cierra el navegador desconéctate del wifi y vuelve a conectarte, si el error persiste contacta con tu proveedor de servicios</h3>
                            
                        </div>
                    </div>
                    
                </div>
                <p>Powered by Future Broadband S.L. | <a href="aviso-legal.aspx">Aviso Legal</a></p>
            </div>
            
        </div>
            
    </div>
        
    </form>
</body>
</html>


