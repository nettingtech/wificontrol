﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Portal_Meraki
{
    public partial class hello : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string paramam = string.Empty;
                for (int i = 0; i < Request.Params.Count; i++)
                {
                    paramam += string.Format("{0} | ", Request.Params[i]);
                }

                label.Text = paramam;
            }
            catch(Exception ex)
            {
                label.Text = ex.Message;
            }

        }
    }
}