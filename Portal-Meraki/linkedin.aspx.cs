﻿using Newtonsoft.Json;
using Portal_Meraki.clases;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Portal_Meraki
{
    public partial class linkedin : System.Web.UI.Page
    {
        protected string Linkedin_id = string.Empty;
        protected string Linkedin_name = string.Empty;
        protected string Linkedin_surname = string.Empty;
        protected string Linkedin_email = string.Empty;
        protected string Linkedin_locale = string.Empty;
        protected string Linkedin_gender = string.Empty;
        protected string Linkedin_birthday = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["code"] != "")
            {
                var obj = GetLinkedinUserData(Request.QueryString["code"]);
            }
        }

        protected Social.User GetLinkedinUserData(string code)
        {
            Social.User currentUser = new Social.User();
            try
            {                                
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                string authUrl = "https://www.linkedin.com/uas/oauth2/accessToken";
                string redirectUrl = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host + "/linkedin.aspx";
                var sign = "grant_type=authorization_code&code=" + HttpUtility.UrlEncode(code) + "&redirect_uri=" + HttpUtility.HtmlEncode(redirectUrl) + "&client_id=" + ConfigurationManager.AppSettings["LinkedinAppid"] + "&client_secret=" + ConfigurationManager.AppSettings["LinkedinAppSecret"];
                //byte[] byteArray = Encoding.UTF8.GetBytes(sign);
                
                HttpWebRequest webRequest = WebRequest.Create(authUrl + "?" + sign) as HttpWebRequest;
                webRequest.Method = "POST";
                webRequest.Host = "www.linkedin.com";
                webRequest.ContentType = "application/x-www-form-urlencoded";

                Stream dataStream = webRequest.GetRequestStream();
                String postData = String.Empty;
                byte[] postArray = Encoding.ASCII.GetBytes(postData);
                dataStream.Write(postArray, 0, postArray.Length);
                dataStream.Close();

                WebResponse response = webRequest.GetResponse();
                dataStream = response.GetResponseStream();
                StreamReader responseReader = new StreamReader(dataStream);
                String returnVal = responseReader.ReadToEnd();

                Social.Token Jdata = JsonConvert.DeserializeObject<Social.Token>(returnVal);
                string Linkedin_authorization_code = Jdata.access_token;

                responseReader.Close();
                dataStream.Close();
                response.Close();

                //Obtenemos Basic DATA

                Uri UserUri = new Uri("https://api.linkedin.com/v2/me");
                HttpWebRequest RequestLinkedin = (HttpWebRequest)HttpWebRequest.Create(UserUri);
                RequestLinkedin.Method = "GET";
                RequestLinkedin.Host = "api.linkedin.com";              
                RequestLinkedin.KeepAlive = true;
                RequestLinkedin.Headers.Add("Authorization: Bearer " + Linkedin_authorization_code);               
                StreamReader userInfo = new StreamReader(RequestLinkedin.GetResponse().GetResponseStream());
                string returnVal2 = userInfo.ReadToEnd().ToString();
                JavaScriptSerializer js = new JavaScriptSerializer();
                Social.LinkedinUser.Welcome linkedinUser = js.Deserialize<Social.LinkedinUser.Welcome>(returnVal2);
                if (linkedinUser != null)
                {
                    currentUser.id = linkedinUser.Id;                    
                    currentUser.first_name = linkedinUser.LocalizedFirstName;
                    currentUser.last_name = linkedinUser.LocalizedLastName;                    
                }
                userInfo.Close();

                //Obtenemos Email

                Uri UserUri_mail = new Uri("https://api.linkedin.com/v2/clientAwareMemberHandles?q=members&projection=(elements*(primary,type,handle~))");
                HttpWebRequest RequestLinkedin_mail = (HttpWebRequest)HttpWebRequest.Create(UserUri_mail);
                RequestLinkedin_mail.Method = "GET";
                RequestLinkedin_mail.Host = "api.linkedin.com";
                RequestLinkedin_mail.Headers.Add("Authorization: Bearer " + Linkedin_authorization_code);
                StreamReader userInfo_mail = new StreamReader(RequestLinkedin_mail.GetResponse().GetResponseStream());
                String returnVal2_mail = userInfo_mail.ReadToEnd().ToString();

                string[] combined = returnVal2_mail.Split(':');
                string mail_user = combined[8];

                var charsToRemove = new string[] { "\\", ",", "\"", ";", "'" };
                foreach (var c in charsToRemove)
                {
                    mail_user = mail_user.Replace(c, string.Empty);
                }

                string[] combined_mail = mail_user.Split('}');
                currentUser.email = combined_mail[0];

                Linkedin_id = currentUser.id;
                Linkedin_name = currentUser.first_name;
                Linkedin_surname = currentUser.last_name;
                Linkedin_email = currentUser.email;

                userInfo_mail.Close();

                return currentUser;
                
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}