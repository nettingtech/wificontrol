﻿<%@ Page Title="" Language="C#" MasterPageFile="~/resources/master/MerakiSecureMaster.Master" AutoEventWireup="true" CodeBehind="linkedin.aspx.cs" Inherits="Portal_Meraki.linkedin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <script>
     $(document).ready(function () {

         var cookie = getCookie("Meraki");
         if (cookie != "") {
             values = cookie.split('&');

             $("#idhotel").val(values[0].split('=')[1]);
             $("#idlocation").val(values[1].split('=')[1]);
             $("#mac").val(values[2].split('=')[1]);
             $("#origin").val(values[3].split('=')[1]);
             $("#idsocialbillintype").val(values[4].split('=')[1]);
             $("#token").val(values[5].split('=')[1]);
             //$("#ga_ssid").val(values[6].split('=')[1]);
             //$("#ga_nas_id").val(values[7].split('=')[1]);
             //$("#ga_ap_mac").val(values[8].split('=')[1]);
             //$("#ga_cmac").val(values[9].split('=')[1]);
         }
         else
             alert("No hay cookie");

         var lang = navigator.browserLanguage ? navigator.browserLanguage : navigator.language;
         var langcode = lang.substring(0, 2);
         if ((langcode != 'es') && (langcode != 'en'))
             langcode = $("#languagecode").val();

         $("#email").val($("#Linkedin_email").val());
         var survey = 'Linkedin: nombre=' + $("#Linkedin_name").val() + '|' + 'apellidos=' + $("#Linkedin_surname").val() + '|';        
         survey += 'id=' + $("#Linkedin_id").val() + '|';
         
         if (lang.substring(0, 2) == 'es') {
             $("#userInformation").html("");
             $("#userInformation").append("<h3>Información del Usuario</h3>");
             $("#userInformation").append("Email: <b>" + $("#Linkedin_email").val() + "</b><br />");
             $("#userInformation").append("Nombre: <b>" + $("#Linkedin_name").val() + "</b><br />");
             $("#userInformation").append("Apellidos: <b>" + $("#Linkedin_surname").val() + "</b><br />");
         }
         else {
             $("#userInformation").html("");
             $("#userInformation").append("<h3>User Information </h3>");
             $("#userInformation").append("Email: <b>" + $("#Linkedin_email").val() + "</b><br />");
             $("#userInformation").append("First Name: <b>" + $("#Linkedin_name").val() + "</b><br />");
             $("#userInformation").append("Last Name: <b>" + $("#Linkedin_surame").val() + "</b><br />");
         }


         $("#surveytext").val(survey);
         var dataStringm = 'action=socialnetwork&email=' + $("#Linkedin_email").val() + '&mac=' + $("#mac").val() + '&idhotel=' + $("#idhotel").val() + '&idlocation=' + $("#idlocation").val() + '&origin=' + $("#origin").val() + '&survey=' + survey + "&lang=" + langcode + "&idbillingtype=" + $("#idsocialbillintype").val();
         $.ajax({
             url: "../resources/handlers/MerakiHandler.ashx",
             data: dataStringm,
             contentType: "application/json; charset=utf-8",
             dataType: "text",
             beforeSend: function () {
                 $("#MyModalWait").modal('show');
             },
             complete: function () {
                 $("#MyModalWait").modal('hide');
             },
             success: function (pResult) {
                 var lResult = JSON.parse(pResult);
                 if (lResult.IdUser == -1) {
                     if (langcode != 'es') {
                         $("#message").html("No more free access are allowed now.");
                     }
                     else {
                         $("#message").html("No se permiten más accesos gratuitos en este periodo.");
                     }
                     $("#myModalError").modal('show');
                 }
                 else if (lResult.IdUser == -3) {
                     if (langcode != 'es') {
                         $("#message").html("We are currently experiencing a problem, please try again later.");
                     }
                     else {
                         $("#message").html("Tenemos un problema temporal, intentelo de nuevo más tarde.");
                     }
                     $("#myModalError").modal('show');
                 }
                 else if (lResult.IdUser == 0) {
                     if (langcode != 'es') {
                         $("#message").html(lResult.UserName);
                     }
                     else {
                         $("#message").html(lResult.UserName);
                     }
                     $("#myModalError").modal('show');
                 }
                 else {
                     $("#finishbutton").attr("href", lResult.Url);
                     $("#finishInformation").append($("#socialaccessinformation").html());

                     if (langcode == 'es') {
                         $("#finishlabel").html("Bienvenido de nuevo " + $("#Linkedin_name").val());
                         $("#finishInformation").append('<b>Nombre de usuario: </b>' + lResult.UserName + '<br />');
                         $("#finishInformation").append('<b>Contraseña: </b>' + lResult.Password + '<br />');
                         $("#finishInformation").append('<b>Fecha de expiración: </b>' + lResult.ValidTill + '<br />');
                         $("#finishbutton").html("Continuar navegando");
                     }
                     else {
                         $("#finishlabel").html("Welcome again " + $("#Linkedin_name").val());
                         $("#finishInformation").append('<b>UserName: </b>' + lResult.UserName + '<br />');
                         $("#finishInformation").append('<b>Password: </b>' + lResult.Password + '<br />');
                         $("#finishInformation").append('<b>Valid Till: </b>' + lResult.ValidTill + '<br />');
                         $("#finishbutton").html("Continue browsing");
                     }

                     var val = "username=" + lResult.UserName + "&password=" + lResult.Password + "&validtill=" + lResult.ValidTill + "&continue_url=" + lResult.Url;
                     createCookie("merakiuser", val, 1);

                     var continue_url = window.location.origin + "/Success.aspx?username=" + lResult.UserName + "&continue_url=" + $("#origin").val();
                     var action = $("#origin").val();
                     var newForm = jQuery('<form>', {
                         'action': action,
                         'method': 'POST',
                     }).append(jQuery('<input>', {
                         'name': 'email',
                         'value': lResult.UserName,
                         'type': 'hidden'
                     })).append(jQuery('<input>', {
                         'name': 'password',
                         'value': lResult.Password,
                         'type': 'hidden'
                     })).append(jQuery('<input>', {
                         'name': 'success_url',
                         'value': continue_url,
                         'type': 'hidden'
                     })).append(jQuery('<input>', {
                         'name': 'continue_url',
                         'value': lResult.message,
                         'type': 'hidden'

                     }));
                     $(document.body).append(newForm);
                     newForm.submit();
                 }

             },
             error: function (xhr, ajaxOptions, thrownError) {
                 $("#message2").html("We have a problem, please try again later.");
                 alert('error: ' + xhr.statusText);
             }
         });
     });

</script>
</asp:Content>                         
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input type="hidden" name="code" id="idhotel" value="" />
        <input type="hidden" name="code" id="idlocation" value="" />
        <input type="hidden" name="code" id="mac" value="" />
        <input type="hidden" name="code" id="origin" value="" />
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">

            </div>
        </div>
    </div>

    <div class="modal fade" id="myModalError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title text-center" id="H2">Error</h4>
          </div>
          <div class="modal-body">
            <div id="message" class="alert alert-danger text-center"></div> 
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="errorbutton">Close</button>
          </div>
        </div>
      </div>
    </div>

        <div class="modal fade" id="myModalFinish" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title text-center" id="finishlabel">Congratulations</h4>
          </div>
          <div class="modal-body">
                <div id="finishInformation" style="text-align:center" ><img src="resources/images/no-disponoble.png" alt="logo hotel" id="logofinish" style="width:150px"  /><br /><br /></div>
                            <br />
                <input type="button" id="finishbutton" class="btn btn-success btn-block" value="Continue browsing" /> 
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="MyModalWait" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center" id="waitheader">Please, wait a moment</h4>
                </div>
                <div class="modal-body">
                    <div class="progress">
                        <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                            <span class="sr-only">Waiting please</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <div id="socialaccessinformation" style="display:none"></div>
        <input type="text" hidden="hidden" id="idsocialbillintype" />
        <input type="text" hidden="hidden" id="surveytext" />
        <input type="text" hidden="hidden" id="email" />
        <input type="text" hidden="hidden" id="Linkedin_id" value="<%Response.Write(Linkedin_id);%>" />
        <input type="text" hidden="hidden" id="Linkedin_email" value="<%Response.Write(Linkedin_email);%>"  />
        <input type="text" hidden="hidden" id="Linkedin_name" value="<%Response.Write(Linkedin_name);%>"  />
        <input type="text" hidden="hidden" id="Linkedin_surname"  value="<%Response.Write(Linkedin_surname);%>" />
        <input type="text" hidden="hidden" id="ga_ssid" />
        <input type="text" hidden="hidden" id="ga_nas_id" />
        <input type="text" hidden="hidden" id="ga_ap_mac" />
        <input type="text" hidden="hidden" id="ga_cmac" />
        <input type="text" hidden="hidden" id="token" />
</asp:Content>
