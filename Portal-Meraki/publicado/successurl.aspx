﻿<%@ Page Title="" Language="C#" MasterPageFile="~/resources/master/MerakiSecureMaster.Master" AutoEventWireup="true" CodeBehind="successurl.aspx.cs" Inherits="Portal_Meraki.successurl" %>

<asp:Content ContentPlaceHolderID="head" ID="Content1" runat="server">    
    <script>        

        $(document).ready(function (e) {
            $("#logohead").html('<img id="logo" style="width:150px;" src="/resources/images/' + <%=nt_st%> + '/' + <%=nt_lt%> + '-logo.png" />');
        });
       
    </script>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1">
    <div class="container">
        <div class="row" id="principal">
            <div class="col-xs-12 col-md-8 col-lg-8 col-lg-offset-1 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="text-center" id="logohead"> </div>
                        <div class="alert alert-success text-center">
                            <label id="labelusername">Código de Acceso: <span style='color:#004085;'><%= username %> </span></label><br />
                            <label id="labelvalidtill">Vádido hasta: <span style='color:#004085;'><%= validtill %> </span></label><br />
                            <label id="labeltimecredit">Crédito de tiempo: <span style='color:#004085;'><%= timecredit %> </span></label><br />
                            <label id="labelvolumedown">Crédito de volumen: <span style='color:#004085;'><%= volumecredit %> </span></label><br />
                            <label id="labelaccesstype">Tipo de acceso: <span style='color:#004085;'><%= accesstype %> </span></label><br />
                        </div>

                        <asp:Button runat="server" ID="logout_asp_button" CssClass="btn btn-danger btn-block" Text="Desconectar" OnClick="logout_asp_button_Click" /><br />
                        <asp:Button runat="server" ID="change_asp_button" CssClass="btn btn-warning btn-block" Text="Usar otro código de acceso" OnClick="change_asp_button_Click" /><br />
                        <a href="<%= continueurl %>" id="buttonContinue" target="_blank" class="btn btn-default btn-block">Continuar a internet</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer id="footer">
        <hr />
            <p>Powered by Future Broadband S.L. | <a href="aviso-legal.aspx">Aviso Legal</a></p>
    </footer>
</asp:Content>