﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Success.aspx.cs" Inherits="Portal_Meraki.Success" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <title></title>
    <!-- FONT AWESOME CARGADA DESDE SU WEB --> 
    <link href="../font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- SCRIPTS -->
    <script src="../resources/scripts/jquery.js"></script>
    <script src="../resources/scripts/bootstrap.js" ></script>
    <script src="../resources/scripts/utils.js"></script>
    <script src="../resources/scripts/translate.js"></script>
   
    <!-- CSS -->
    <link href="/resources/css/style.css" rel="stylesheet" />
    <link href="/resources/css/bootstrap.css" rel="stylesheet" />
    <link href="/resources/css/bootstrap-theme.css" rel="stylesheet" />
        <style>
        #footer { position:absolute; width:100%; height:100px; }
        #footer p {
            text-align: center;
            font-size: 9px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
         <div class="container">
        <div class="row" id="principal">
            <div class="col-xs-12 col-md-8 col-lg-8 col-lg-offset-1 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="text-center" id="logohead"> </div>
                        <div class="alert alert-success text-center">
                            <label id="labelusername">Código de Acceso: <span style='color:#004085;'><%= username %> </span></label><br />
                            <label id="labelvalidtill">Vádido hasta: <span style='color:#004085;'><%= validtill %> </span></label><br />
                            <label id="labeltimecredit">Crédito de tiempo: <span style='color:#004085;'><%= timecredit %> </span></label><br />
                            <label id="labelvolumedown">Crédito de volumen: <span style='color:#004085;'><%= volumecredit %> </span></label><br />
                            <label id="labelaccesstype">Tipo de acceso: <span style='color:#004085;'><%= accesstype %> </span></label><br />
                        </div>

                        <asp:Button runat="server" ID="logout_asp_button" CssClass="btn btn-danger btn-block" Text="Desconectar" OnClick="logout_asp_button_Click" /><br />
                        <asp:Button runat="server" ID="change_asp_button" CssClass="btn btn-warning btn-block" Text="Usar otro código de acceso" OnClick="change_asp_button_Click" /><br />
                        <a href="<%= continue_url %>" id="buttonContinue" target="_blank" class="btn btn-default btn-block">Continuar a internet</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <footer id="footer">
        <hr />
            <p>Powered by Future Broadband S.L. | <a href="aviso-legal.aspx">Aviso Legal</a></p>
    </footer>
    <script>
        $(document).ready(function (e) {

            x = 1000// $('#container').height() + 20; // +20 gives space between div and footer
            y = $(window).height();
            if (y <= 350) {
                x = 1090;
            }
            if (x + 100 <= y) { // 100 is the height of your footer
                $('#footer').css('top', y + 20 + 'px');// again 100 is the height of your footer
                $('#footer').css('display', 'block');
            } else {
                $('#footer').css('top', x + 'px');
                $('#footer').css('display', 'block');
            }

        });
    </script>
    </form>
</body>
</html>