﻿<%@ Page Title="" Language="C#" MasterPageFile="~/resources/master/MerakiMaster.Master" AutoEventWireup="true" CodeBehind="logout.aspx.cs" Inherits="Portal_Meraki.logout" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        $(document).ready(function (e) {
            $("#logo").attr("src", "/resources/images/" + $("#idhotel").val() + "/" + $("#idlocation").val() + "-logo.png");

        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div class="container">
        <div class="row" id="principal">
            <div class="col-xs-12 col-md-8 col-lg-8 col-lg-offset-1 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                         <p class="text-center"> <img src="#" id="logo" style="width:150px;" /></p>
                        <div class="alert alert-success text-center">
                          <p><b>Usted está desconectado</b></p>
                            <p>Por favor, espere unos minutos antes de conectarse de nuevo.</p>
                          <p>Gracias</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
