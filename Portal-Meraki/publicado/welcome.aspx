﻿<%@ Page Title="" Language="C#" MasterPageFile="~/resources/master/MerakiSecureMaster.Master" AutoEventWireup="true" CodeBehind="welcome.aspx.cs" Inherits="Portal_Meraki.wellcome" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        var validar = false;
        var googleUser = {};
        var startApp = function () {
            gapi.load('auth2', function () {
                auth2 = gapi.auth2.init({
                    client_id: '149929614640-f70mvs70julfeccro3d04vdf6s9p1uqg.apps.googleusercontent.com',
                });
                attachSignin(document.getElementById('customBtn'));
            });
        };       

        function attachSignin(element) {
            console.log(element.id);
            auth2.attachClickHandler(element, {},
                function (googleUser) {
                    onSignIn(googleUser);
                }, function (error) {
                    alert(JSON.stringify(error, undefined, 2));
                });
        }


        function signOut() {
            var auth2 = gapi.auth2.getAuthInstance();
            auth2.signOut().then(function () {
                console.log('User signed out.');
            });
        }

        function onSignIn(googleUser) {
            if (validar) {
                var profile = googleUser.getBasicProfile();
                console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
                console.log('Name: ' + profile.getName());
                console.log('Image URL: ' + profile.getImageUrl());
                console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.

                var dataString = 'action=allow&idhotel=' + $("#idhotel").val() + '&idlocation=' + $("#idlocation").val() + '&mac=' + $("#mac").val() + '&origin=' + $("#origin").val();
                $.ajax({
                    url: "../resources/handlers/MerakiHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    beforeSend: function () {
                        $("#MyModalWait").modal('show');
                    },
                    complete: function () {
                        $("#MyModalWait").modal('hide');
                    },
                    success: function (pResult) {
                        var lResult = JSON.parse(pResult);

                        if (lResult.allow == 1) {
                            $("#finishbutton").attr("href", lResult.Url);
                            $("#finishInformation").append($("#freeaccessinformation").html());
                            var val = "username=" + lResult.UserName.toUpperCase() + "&password=" + lResult.Password.toUpperCase() + "&validtill=" + lResult.ValidTill + "&continue_url=" + lResult.Url;
                            createCookie("Meraki", val, 1);

                            //var parameters = getParameters();
                            //var parameters_logoutUrl = '&ga_ssid=' + parameters.ga_ssid + '&ga_nas_id=' + parameters.ga_nas_id + '&ga_ap_mac=' + parameters.ga_ap_mac + '&ga_cmac=' + parameters.ga_cmac;
                            //var continue_url = window.location.origin + "/successurl.aspx?username=" + lResult.UserName.toUpperCase() + "&continue_url=" + lResult.Url + "&token=" + $("#token").val() + '&origin=' + $("#origin").val() + parameters_logoutUrl;
                            //var action = "http://" + $("#origin").val() + ":880/cgi-bin/hotspot_login.cgi";

                            var continue_url = window.location.origin + "/successurl.aspx?username=" + lResult.UserName.toUpperCase() + "&continue_url=" + lResult.Url;

                            var action = $("#origin").val();
                            var newForm = jQuery('<form>', {
                                'action': action,
                                'method': 'POST',
                            }).append(jQuery('<input>', {
                                'name': 'email',
                                'value': lResult.UserName.toUpperCase(),
                                'type': 'hidden'
                            })).append(jQuery('<input>', {
                                'name': 'password',
                                'value': lResult.Password.toUpperCase(),
                                'type': 'hidden'
                            })).append(jQuery('<input>', {
                                'name': 'success_url',
                                'value': continue_url,
                                'type': 'hidden'
                            })).append(jQuery('<input>', {
                                'name': 'continue_url',
                                'value': lResult.Url,
                                'type': 'hidden'
                            }));
                            $(document.body).append(newForm);
                            newForm.submit();
                        }
                        else if (lResult.allow == 2) {
                            if (langcode != 'es') {
                                $("#message").html("No more free access allowed now.");
                            }
                            else {
                                $("#message").html("No se permiten más accesos gratuitos en este periodo.");
                            }
                            $("#myModalError").modal('show');
                        }
                        else if (lResult.allow == 0) {

                            //var parameters = getParameters();
                            //var parameters_logoutUrl = '&ga_ssid=' + parameters.ga_ssid + '&ga_nas_id=' + parameters.ga_nas_id + '&ga_ap_mac=' + parameters.ga_ap_mac + '&ga_cmac=' + parameters.ga_cmac + "&email=" + profile.getEmail() + "&name=" + profile.getName();
                            //var va2 = "idhotel=" + $("#idhotel").val() + "&idlocation=" + $("#idlocation").val() + "&mac=" + $("#mac").val() + "&origin=" + $("#origin").val() + "&idbillingtype=" + $("#socialaccess").val() + "&token=" + $("#token").val() + parameters_logoutUrl;
                            //var val = "idhotel=" + $("#idhotel").val() + ",idlocation=" + $("#idlocation").val() + ",mac=" + $("#mac").val() + ",origin=" + $("#origin").val() + ",continue_url=" + $("#urlDest").val();
                            
                            var uri = window.location.origin + "/google.aspx";
                            
                            var nt_bt = "idbillingtype=" + $("#socialaccess").val() + "&email=" + profile.getEmail() + "&name=" + profile.getName();
                            createCookie("Meraki", nt_bt, 1);
                            
                            window.location = encodeURI(uri);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#loading").html("Su ha producido un error durante el envio. Por favor intentelo más tarde.");
                        alert('error: ' + xhr.statusText);
                        $("#nextstep3").show();
                        $("#backstep3").show();

                    }
                });
            }
        }

        $(document).ready(function (e) {

            startApp();

            if ($("#cloudre").val() != '') {
                $("#principal").hide();
                $("#myModal").modal('hide');
                $("#MyModalWait").modal('show');

                var continue_url = window.location.origin + "/successurl.aspx?username=" + +$("#username").val() + "&continue_url=" + $("#urlDest").val();
                var action = $("#origin").val();
                var newForm = jQuery('<form>', {
                    'action': action,
                    'method': 'POST',
                }).append(jQuery('<input>', {
                    'name': 'email',
                    'value': $("#username").val(),
                    'type': 'hidden'
                })).append(jQuery('<input>', {
                    'name': 'password',
                    'value': $("#password").val(),
                    'type': 'hidden'
                })).append(jQuery('<input>', {
                    'name': 'success_url',
                    'value': continue_url,
                    'type': 'hidden'
                })).append(jQuery('<input>', {
                    'name': 'continue_url',
                    'value': $("#urlDest").val(),
                    'type': 'hidden'
                }));
                $(document.body).append(newForm);
                newForm.submit();
            }
            else
            {        
                var scrollcomplete = false;
                var parameters = getParameters();
                var val = "idhotel=" + $("#idhotel").val() + "&idlocation=" + $("#idlocation").val() + "&mac=" + $("#mac").val() + "&origin=" + $("#origin").val();
                createCookie("meraki", val, 1);
        
                var lang = navigator.browserLanguage ? navigator.browserLanguage : navigator.language;
                var langcode = lang.substring(0, 2);
        
                if ((langcode != 'es') && (langcode != 'en'))
                    langcode = $("#lang").val();
        
                var dataString = 'action=fullsite&idhotel=' + $("#idhotel").val() + "&idlocation=" + $("#idlocation").val() + "&lang=" + langcode + '&mac=' + $("#mac").val();
                $.ajax({
                    url: "../resources/handlers/MerakiHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    success: function (pReturn) {
                        var lReturn = JSON.parse(pReturn);
                        $("#logofinish").attr("src", "resources/images/" + $("#idhotel").val() + "/" + $("#idlocation").val() + "-logo.png");
                        $("#freeimage").attr("src", "resources/images/" + $("#idhotel").val() + "/" + $("#idlocation").val() + "-logo.png");
                        $("#premiumimage").attr("src", "resources/images/" + $("#idhotel").val() + "/" + $("#idlocation").val() + "-logo.png");
                        $("#socialimage").attr("src", "resources/images/" + $("#idhotel").val() + "/" + $("#idlocation").val() + "-logo.png");
                        $("#disclaimerimage").attr("src", "resources/images/" + $("#idhotel").val() + "/" + $("#idlocation").val() + "-logo.png");
                        $("#noacceptimage").attr("src", "resources/images/" + $("#idhotel").val() + "/" + $("#idlocation").val() + "-logo.png");
                        $("#loginimage").attr("src", "resources/images/" + $("#idhotel").val() + "/" + $("#idlocation").val() + "-logo.png");
                        $("#voucherimage").attr("src", "resources/images/" + $("#idhotel").val() + "/" + $("#idlocation").val() + "-logo.png");
                        $("#imagehome").attr("src", "resources/images/" + $("#idhotel").val() + "/" + $("#idlocation").val() + "-home.jpg");
                        $("#freetext").html(lReturn.FreeText);
                        $("#premiumtext").html(lReturn.PremiumText);
                        $("#socialText").html(lReturn.SocialText);
                        $("#disclaimertext").html(lReturn.Disclaimer);
                        $("#disclaimermandatorytext").html(lReturn.Disclaimer);
        
                        $("#mandatorysurvey").val(lReturn.MandatorySurvey);
        
                        if (lReturn.FreeAccessModule) {
                            $("#freeaccess").val(lReturn.freeAccess.IdBillingType);
                            $("#tab-free").show();
                            if (langcode == 'es') {
                                $("#freeaccessinformation").html('<b>Tiempo de Conexión:</b> ' + lReturn.freeAccess.TimeCredit + ' <br/><b>Validez del Ticket:</b> ' + lReturn.freeAccess.ValidTill + ' horas<br/><b>Número máximo de dispositivos:</b> ' + lReturn.freeAccess.MaxDevices + '<br/><b>Velocidad de Subida:</b> ' + lReturn.freeAccess.BWUp + ' kbps<br/><b>Velocidad de Bajada:</b> ' + lReturn.freeAccess.BWDown + ' kbps<br/>');
                            }
                            else {
                                $("#freeaccessinformation").html('<b>Time Credit:</b> ' + lReturn.freeAccess.TimeCredit + ' <br/><b>Valid Till:</b> ' + lReturn.freeAccess.ValidTill + ' hours<br/><b>Max devices:</b> ' + lReturn.freeAccess.MaxDevices + '<br/><b>Bandwidth Up:</b> ' + lReturn.freeAccess.BWUp + ' kbps<br/><b>Bandwidth Down:</b> ' + lReturn.freeAccess.BWDown + ' kbps<br/>');
                            }
                        }
        
                        if (lReturn.LoginModule) {
                            $("#tab-login").show();
                            $("#logintext").html(lReturn.LoginText);
                        }
        
                        if (lReturn.PayAccessModule) {
                            $("#tab-payaccess").show();
        
                            $("#accesstypes").html("");
                            $('#accesstype').find('option').remove();
        
                            if (langcode == 'es') {
                                $("#accesstype").append('<option value="0" >Seleccione un acceso</option>');
                                $.each(lReturn.listPremium, function (index, item) {
                                    $("#accesstype").append('<option value="' + item["IdBillingType"] + '" >' + item["Description"] + ' - ' + item["Currency"] + '</option>');
                                    $("#accesstypes").append('<div id="accesstype_details_' + item["IdBillingType"] + '" style="display:none;"><b>Tiempo de Conexión:</b> ' + item["TimeCredit"] + ' <br/><b>Validez del Ticket:</b> ' + item["ValidTill"] + ' horas<br/><b>Número máximo de dispositivos:</b> ' + item["MaxDevices"] + '<br/><b>Velocidad de Subida:</b> ' + item["BWUp"] + ' kbps<br/><b>Velocidad de Bajada:</b> ' + item["BWDown"] + ' kbps<br/></div>');
        
                                });
                            }
                            else {
                                $("#accesstype").append('<option value="0" >Select one</option>');
                                $.each(lReturn.listPremium, function (index, item) {
                                    $("#accesstype").append('<option value="' + item["IdBillingType"] + '" >' + item["Description"] + ' - ' + item["Currency"] + '</option>');
                                    $("#accesstypes").append('<div id="accesstype_details_' + item["IdBillingType"] + '" style="display:none;"><b>Time Credit:</b> ' + item["TimeCredit"] + ' <br/><b>Valid Till:</b> ' + item["ValidTill"] + ' hours<br/><b>Max devices:</b> ' + item["MaxDevices"] + '<br/><b>Bandwidth Up:</b> ' + item["BWUp"] + ' kbps<br/><b>Bandwidth Down:</b> ' + item["BWDown"] + ' kbps<br/></div>');
                                });
                            }
        
                        }
        
                        if (lReturn.SocialNetworksModule) {
        
                            $("#socialaccess").val(lReturn.socialAccess.IdBillingType);
                            $("#tab-social").show();
                            if (langcode == 'es') {
                                $("#socialaccessinformation").html('<b>Tiempo de Conexión:</b> ' + lReturn.socialAccess.TimeCredit + ' <br/><b>Validez del Ticket:</b> ' + lReturn.socialAccess.ValidTill + ' horas<br/><b>Número máximo de dispositivos:</b> ' + lReturn.socialAccess.MaxDevices + '<br/><b>Velocidad de Subida:</b> ' + lReturn.socialAccess.BWUp + ' kbps<br/><b>Velocidad de Bajada:</b> ' + lReturn.socialAccess.BWDown + ' kbps<br/>');
                            }
                            else {
                                $("#socialaccessinformation").html('<b>Time Credit:</b> ' + lReturn.socialAccess.TimeCredit + ' <br/><b>Valid Till:</b> ' + lReturn.socialAccess.ValidTill + ' hours<br/><b>Max devices:</b> ' + lReturn.socialAccess.MaxDevices + '<br/><b>Bandwidth Up:</b> ' + lReturn.socialAccess.BWUp + ' kbps<br/><b>Bandwidth Down:</b> ' + lReturn.socialAccess.BWDown + ' kbps<br/>');
                            }
        
                            $("#idsocialbillintype").val(lReturn.socialAccess.IdBillingType);
                        }
        
                        if (lReturn.VoucherModule) {
        
                            $("#tab-voucher").show();
                            $("#vouchertext").html(lReturn.VoucherText);
                        }
        
                        switch (lReturn.DefaultModule) {
                            case 1:
                                $("#tab-free").addClass("active"); $("#free").addClass("active in"); break;
                            case 2:
                                $("#tab-payaccess").addClass("active"); $("#pay").addClass("active in"); break;
                            case 3:
                                $("#tab-social").addClass("active"); $("#facebook").addClass("active in"); break;
                            case 5:
                                $("#tab-login").addClass("active"); $("#login").addClass("active in"); break;
                            case 6:
                                $("#tab-voucher").addClass("active"); $("#voucher").addClass("active in"); break;
                        }
        
        
                        var text = '';
                        text += '<div role="form">';                       
                        $.each(lReturn.survey, function (index, item) {
                            text += '<div class="form-group"><label id="labelemail">' + item["Question"] + '</label>';
                            if (item["Type"] == 'text')
                                text += '<input type="text" id="' + item["Order"] + '" rel="survey" class="form-control" placeholder="' + item["Question"] + '" question="' + item["Question"] + '" /></div>';
                            else if (item["Type"] == 'select') {
                                var parts = item["Values"].split('/');
                                text += "<td><select id='" + item["Order"] + "' rel='survey' class='form-control' question='" + item["Question"] + "' >";
                                $.each(parts, function (key, value) {
                                    text += "<option value='" + value + "'>" + value + "</option>";
                                });
        
                                text += '</select></div>';
                            }
                        });
                        text += '</div>';
        
                        $("#survey").html(text);
        
                        if ($("#error").val() != "") {
                            if (langcode == 'es') {
                                switch ($("#error").val().toUpperCase()) {
                                    case 'UNKNOWN USER': $("#message").html('Usuario Desconocido'); break;
                                    case 'NO TIME CREDIT': $("#message").html('Tiempo de conexión agotado'); break;
                                    case 'TICKET EXPIRED': $("#message").html('Ticket Caducado'); break;
                                    case 'TOO MANY DEVICES': $("#message").html('Demasiados dispositivos'); break;
                                    case 'SORRY, NO MORE USERS ALLOWED': $("#message").html('Lo sentimos, no se permiten más usuarios'); break;
                                    case 'WRONG PASSWORD': $("#message").html('Contraseña incorrecta'); break;
                                    case 'UPLOAD QUOTA EXCEEDED': $("#message").html('Cuota de datos de subida agotada'); break;
                                    case 'DOWNLOAD QUOTA EXCEEDED': $("#message").html('Cuota de datos de bajada agotada'); break;
        
                                    default: $("#message").html($("#error").val());
                                }
                            }
                            else
                                $("#message").html($("#error").val());
        
                            $("#myModalError").modal('show');
                            $("#myModal").modal('hide');
                            $("#noaccept").fadeOut("fast");
                            $("#principal").fadeIn("slow");
                            $("#wait").fadeOut("slow");
                        }
                        else
                        {
                            if (lReturn.DisclaimerMandatory) {
                                $("#wait").fadeOut("slow");
                                $("#noaccept").fadeIn("slow");
                                $("#myModal").modal('show');
                            }
                            else {
                                $("#disclaimermandatory").show();
                                $("#myModal").modal('hide');
                                $("#wait").fadeOut("slow");
                                $("#noaccept").fadeOut("fast");
                                $("#principal").fadeIn("slow");
                            }
                        }        
                 },
                 error: function (xhr, ajaxOptions, thrownError) {
                     $("#loading").html("Se ha producido un error durante el envio. Por favor inténtelo más tarde.");
                     alert('error: ' + xhr.statusText);
                 }        
             });
        
            }
                       
            $("#welcome_socialbutton").click(function (e) {
                var dataString = 'action=allow&idhotel=' + $("#idhotel").val() + '&idlocation=' + $("#idlocation").val() + '&mac=' + $("#mac").val() + '&origin=' + $("#origin").val();
                $.ajax({
                    url: "../resources/handlers/MerakiHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    beforeSend: function () {
                        $("#MyModalWait").modal('show');
                    },
                    complete: function () {
                        $("#MyModalWait").modal('hide');
                    },
                    success: function (pResult) {
                        var lResult = JSON.parse(pResult);

                        if (lResult.allow == 1) {
                            $("#finishbutton").attr("href", lResult.Url);
                            $("#finishInformation").append($("#freeaccessinformation").html());

                            //var val = "username=" + lResult.UserName.toUpperCase() + "&password=" + lResult.Password.toUpperCase() + "&validtill=" + lResult.ValidTill + "&continue_url=" + lResult.Url;
                            //createCookie("merakiuser", val, 1);

                            var continue_url = window.location.origin + "/successurl.aspx?username=" + lResult.UserName.toUpperCase() + "&continue_url=" + lResult.Url;
                            var action = $("#origin").val();
                            var newForm = jQuery('<form>', {
                                'action': action,
                                'method': 'POST',
                            }).append(jQuery('<input>', {
                                'name': 'email',
                                'value': lResult.UserName.toUpperCase(),
                                'type': 'hidden'
                            })).append(jQuery('<input>', {
                                'name': 'password',
                                'value': lResult.Password.toUpperCase(),
                                'type': 'hidden'
                            })).append(jQuery('<input>', {
                                'name': 'success_url',
                                'value': continue_url,
                                'type': 'hidden'
                            })).append(jQuery('<input>', {
                                'name': 'continue_url',
                                'value': lResult.Url,
                                'type': 'hidden'

                            }));
                            $(document.body).append(newForm);
                            newForm.submit();
                        }
                        else if (lResult.allow == 2) {
                            if (langcode != 'es') {
                                $("#message").html("No more free access allowed now.");
                            }
                            else {
                                $("#message").html("No se permiten más accesos gratuitos en este periodo.");
                            }
                            $("#myModalError").modal('show');
                        }
                        else if (lResult.allow == 0) {
                            e.preventDefault();
                            var parameters = getParameters();

                            var uri = window.location.origin + "/facebook.aspx";
                            var nt_bt = "idbillingtype=" + $("#socialaccess").val();
                            createCookie("meraki", nt_bt, 1);
                            var facebookUri = "https://www.facebook.com/v2.11/dialog/oauth?client_id=233231503535149&redirect_uri=" + uri + "&response_type=code&scope=email,public_profile,user_birthday,user_likes&state=\"{" + val + "\"}";
                            window.location = encodeURI(facebookUri);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#loading").html("Su ha producido un error durante el envio. Por favor intentelo más tarde.");
                        alert('error: ' + xhr.statusText);
                        $("#nextstep3").show();
                        $("#backstep3").show();

                    }
                });
            });

            $("#welcome_instagrambutton").click(function (e) {
                var dataString = 'action=allow&idhotel=' + $("#idhotel").val() + '&idlocation=' + $("#idlocation").val() + '&mac=' + $("#mac").val() + '&origin=' + $("#origin").val();
                $.ajax({
                    url: "../resources/handlers/MerakiHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    beforeSend: function () {
                        $("#MyModalWait").modal('show');
                    },
                    complete: function () {
                        $("#MyModalWait").modal('hide');
                    },
                    success: function (pResult) {
                        var lResult = JSON.parse(pResult);

                        if (lResult.allow == 1) {
                            $("#finishbutton").attr("href", lResult.Url);
                            $("#finishInformation").append($("#freeaccessinformation").html());

                            var continue_url = window.location.origin + "/successurl.aspx?continue_url=" + $("#urlDest").val();
                            var action = $("#origin").val();
                            var newForm = jQuery('<form>', {
                                'action': action,
                                'method': 'POST',
                            }).append(jQuery('<input>', {
                                'name': 'email',
                                'value': lResult.UserName.toUpperCase(),
                                'type': 'hidden'
                            })).append(jQuery('<input>', {
                                'name': 'password',
                                'value': lResult.Password.toUpperCase(),
                                'type': 'hidden'
                            })).append(jQuery('<input>', {
                                'name': 'success_url',
                                'value': continue_url,
                                'type': 'hidden'
                            })).append(jQuery('<input>', {
                                'name': 'continue_url',
                                'value': lResult.Url,
                                'type': 'hidden'

                            }));
                            $(document.body).append(newForm);
                            newForm.submit();
                        }
                        else if (lResult.allow == 2) {
                            if (langcode != 'es') {
                                $("#message").html("No more free access allowed now.");
                            }
                            else {
                                $("#message").html("No se permiten más accesos gratuitos en este periodo.");
                            }
                            $("#myModalError").modal('show');
                        }
                        else if (lResult.allow == 0) {
                            var nt_bt = "idbillingtype=" + $("#socialaccess").val();
                            createCookie("meraki", nt_bt, 1);
                            var uri = window.location.origin + "/instagram.aspx";
                            var url = "https://www.instagram.com/oauth/authorize/?client_id=a3d2212db0a1455388b370858fe80e64&redirect_uri=" + uri + "&response_type=token";
                            window.location.href = url;
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#loading").html("Su ha producido un error durante el envio. Por favor intentelo más tarde.");
                        alert('error: ' + xhr.statusText);
                        $("#nextstep3").show();
                        $("#backstep3").show();

                    }
                });
            });

            $("#welcome_linkedinsocialbutton").click(function (e) {
                var dataString = 'action=allow&idhotel=' + $("#idhotel").val() + '&idlocation=' + $("#idlocation").val() + '&mac=' + $("#mac").val() + '&origin=' + $("#origin").val();
                $.ajax({
                    url: "../resources/handlers/MerakiHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    beforeSend: function () {
                        $("#MyModalWait").modal('show');
                    },
                    complete: function () {
                        $("#MyModalWait").modal('hide');
                    },
                    success: function (pResult) {
                        var lResult = JSON.parse(pResult);

                        if (lResult.allow == 1) {
                            $("#finishbutton").attr("href", lResult.Url);
                            $("#finishInformation").append($("#freeaccessinformation").html());

                            //var val = "username=" + lResult.UserName.toUpperCase() + "&password=" + lResult.Password.toUpperCase() + "&validtill=" + lResult.ValidTill + "&continue_url=" + lResult.Url;
                            //var parameters = getParameters();
                            //var parameters_logoutUrl = '&ga_ssid=' + parameters.ga_ssid + '&ga_nas_id=' + parameters.ga_nas_id + '&ga_ap_mac=' + parameters.ga_ap_mac + '&ga_cmac=' + parameters.ga_cmac;
                            //var continue_url = window.location.origin + "/success.aspx?username=" + lResult.UserName.toUpperCase() + "&idhotel=" + $("#idhotel").val() + '&idlocation=' + $("#idlocation").val() + '&mac=' + $("#mac").val() + "&continue_url=" + lResult.Url + "&token=" + $("#token").val() + '&origin=' + $("#origin").val() + parameters_logoutUrl;
                            //createCookie("Merakiuser", val, 1);
                            var continue_url = window.location.origin + "/successurl.aspx?username=" + lResult.UserName.toUpperCase() + "&continue_url=" + lResult.Url;

                            var action = $("#origin").val();
                            var newForm = jQuery('<form>', {
                                'action': action,
                                'method': 'POST',
                            }).append(jQuery('<input>', {
                                'name': 'email',
                                'value': lResult.UserName.toUpperCase(),
                                'type': 'hidden'
                            })).append(jQuery('<input>', {
                                'name': 'password',
                                'value': lResult.Password.toUpperCase(),
                                'type': 'hidden'
                            })).append(jQuery('<input>', {
                                'name': 'success_url',
                                'value': continue_url,
                                'type': 'hidden'
                            })).append(jQuery('<input>', {
                                'name': 'continue_url',
                                'value': lResult.Url,
                                'type': 'hidden'

                            }));
                            $(document.body).append(newForm);
                            newForm.submit();
                        }
                        else if (lResult.allow == 2) {
                            if (langcode != 'es') {
                                $("#message").html("No more free access allowed now.");
                            }
                            else {
                                $("#message").html("No se permiten más accesos gratuitos en este periodo.");
                            }
                            $("#myModalError").modal('show');
                        }
                        else if (lResult.allow == 0) {
                            e.preventDefault();
                            //var parameters = getParameters();
                            //var parameters_logoutUrl = '&ga_ssid=' + parameters.ga_ssid + '&ga_nas_id=' + parameters.ga_nas_id + '&ga_ap_mac=' + parameters.ga_ap_mac + '&ga_cmac=' + parameters.ga_cmac;
                            var va2 = "idhotel=" + $("#idhotel").val() + "&idlocation=" + $("#idlocation").val() + "&mac=" + $("#mac").val() + "&origin=" + $("#origin").val() + "&idbillingtype=" + $("#socialaccess").val() + "&token=" + $("#token").val(); //+ parameters_logoutUrl;
                            var val = "idhotel=" + $("#idhotel").val() + ",idlocation=" + $("#idlocation").val() + ",mac=" + $("#mac").val() + ",origin=" + $("#origin").val() + ",continue_url=" + $("#urlDest").val();
                            var uri = window.location.origin + "/linkedin.aspx";
                            createCookie("Meraki", va2, 1);
                            var linkedinUri = "https://www.linkedin.com/oauth/v2/authorization?client_id=776339j9ooziyz&redirect_uri=" + uri + "&response_type=code&scope=r_emailaddress,r_liteprofile&state=\"{" + val + "\"}";
                            window.location = encodeURI(linkedinUri);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#loading").html("Su ha producido un error durante el envio. Por favor intentelo más tarde.");
                        alert('error: ' + xhr.statusText);
                        $("#nextstep3").show();
                        $("#backstep3").show();

                    }
                });
            });

            $('#welcome_freebutton').click(function (e) {
                var dataString = 'action=allow&idhotel=' + $("#idhotel").val() + '&idlocation=' + $("#idlocation").val() + '&mac=' + $("#mac").val() + '&origin=' + $("#origin").val();
                $.ajax({
                    url: "../resources/handlers/MerakiHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    beforeSend: function () {
                        $("#MyModalWait").modal('show');
                    },
                    complete: function () {
                        $("#MyModalWait").modal('hide');
                    },
                    success: function (pResult) {
                        var lResult = JSON.parse(pResult);

                        if (lResult.allow == 1) {
                            $("#finishbutton").attr("href", lResult.Url);
                            $("#finishInformation").append($("#freeaccessinformation").html());

                            var continue_url = window.location.origin + "/successurl.aspx?continue_url=" + lResult.Url;
                            var action = $("#origin").val();
                            var newForm = jQuery('<form>', {
                                'action': action,
                                'method': 'POST',
                            }).append(jQuery('<input>', {
                                'name': 'email',
                                'value': lResult.UserName.toUpperCase(),
                                'type': 'hidden'
                            })).append(jQuery('<input>', {
                                'name': 'password',
                                'value': lResult.Password.toUpperCase(),
                                'type': 'hidden'
                            })).append(jQuery('<input>', {
                                'name': 'success_url',
                                'value': continue_url,
                                'type': 'hidden'
                            })).append(jQuery('<input>', {
                                'name': 'continue_url',
                                'value': lResult.Url,
                                'type': 'hidden'
                            }));
                            $(document.body).append(newForm);
                            newForm.submit();
                        }
                        else if (lResult.allow == 2) {
                            if (langcode != 'es') {
                                $("#message").html("No more free access allowed now.");
                            }
                            else {
                                $("#message").html("No se permiten más accesos gratuitos en este periodo.");
                            }
                            $("#myModalError").modal('show');
                        }
                        else if (lResult.allow == 0) {
                            if ($("#mandatorysurvey").val() != "true") {
                                e.preventDefault();

                                var dataString = 'action=freeaccessnosurvey&idhotel=' + $("#idhotel").val() + '&idlocation=' + $("#idlocation").val() + '&mac=' + $("#mac").val() + '&origin=' + $("#origin").val() + "&idbilling=" + $("#freeaccess").val();
                                $.ajax({
                                    url: "../resources/handlers/MerakiHandler.ashx",
                                    data: dataString,
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "text",
                                    beforeSend: function () {
                                        $("#MyModalWait").modal('show');
                                    },
                                    complete: function () {
                                        $("#MyModalWait").modal('hide');
                                    },
                                    success: function (pResult) {
                                        var lResult = JSON.parse(pResult);

                                        if (lResult.IdUser == -1) {
                                            if (langcode != 'es') {
                                                $("#message").html("No more free access are allowed now.");
                                            }
                                            else {
                                                $("#message").html("No se permiten más accesos gratuitos en este periodo.");
                                            }
                                            $("#myModalError").modal('show');
                                        }
                                        else if (lResult.IdUser == -3) {
                                            if (langcode != 'es') {
                                                $("#message").html("We are currently experiencing a problem, please try again later.");
                                            }
                                            else {
                                                $("#message").html("Tenemos un problema temporal, intentelo de nuevo más tarde.");
                                            }
                                            $("#myModalError").modal('show');
                                        }
                                        else if (lResult.IdUser == 0) {
                                            if (langcode != 'es') {
                                                $("#message").html("We are currently experiencing a problem, please try again later.");
                                            }
                                            else {
                                                $("#message").html("Tenemos un problema temporal, intentelo de nuevo más tarde.");
                                            }
                                            $("#myModalError").modal('show');
                                        }
                                        else {
                                            $("#finishbutton").attr("href", lResult.Url);
                                            $("#finishInformation").append($("#freeaccessinformation").html());

                                            var continue_url = window.location.origin + "/successurl.aspx?continue_url=" + lResult.Url;
                                            var action = $("#origin").val();
                                            var newForm = jQuery('<form>', {
                                                'action': action,
                                                'method': 'POST',
                                            }).append(jQuery('<input>', {
                                                'name': 'email',
                                                'value': lResult.UserName.toUpperCase(),
                                                'type': 'hidden'
                                            })).append(jQuery('<input>', {
                                                'name': 'password',
                                                'value': lResult.Password.toUpperCase(),
                                                'type': 'hidden'
                                            })).append(jQuery('<input>', {
                                                'name': 'success_url',
                                                'value': continue_url,
                                                'type': 'hidden'
                                            })).append(jQuery('<input>', {
                                                'name': 'continue_url',
                                                'value': lResult.Url,
                                                'type': 'hidden'

                                            }));
                                            $(document.body).append(newForm);
                                            newForm.submit();

                                            if (langcode == 'es') {
                                                $("#finishlabel").html("Enhorabuena");
                                                $("#finishInformation").append('<b>Nombre de usuario: </b>' + lResult.UserName + '<br />');
                                                $("#finishInformation").append('<b>Contraseña: </b>' + lResult.Password + '<br />');
                                                $("#finishInformation").append('<b>Fecha de expiración: </b>' + lResult.ValidTill + '<br />');
                                                $("#finishbutton").val("Continuar navegando");
                                            }
                                            else {
                                                $("#finishInformation").append('<b>UserName: </b>' + lResult.UserName + '<br />');
                                                $("#finishInformation").append('<b>Password: </b>' + lResult.Password + '<br />');
                                                $("#finishInformation").append('<b>Valid Till: </b>' + lResult.ValidTill + '<br />');
                                                $("#finishbutton").val("Continue browsing");
                                            }
                                        }
                                    },
                                    error: function (xhr, ajaxOptions, thrownError) {
                                        $("#loading").html("Se ha producido un error durante el envio. Por favor inténtelo más tarde.");
                                        alert('error: ' + xhr.statusText);
                                        $("#nextstep3").show();
                                        $("#backstep3").show();

                                    }
                                });
                            }
                            else {
                                $("#myModalSurvey").modal('show');
                            }

                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#loading").html("Se ha producido un error durante el envio. Por favor inténtelo más tarde.");
                        alert('error: ' + xhr.statusText);
                        $("#nextstep3").show();
                        $("#backstep3").show();

                    }
                });
            });

            $('#welcome_loginbutton').click(function (e) {
                e.preventDefault();
                var user = $("#email_field").val().toUpperCase();
                var password = $("#password_field").val().toUpperCase();               

                if ((ilegalCharacter(user) != false) && (ilegalCharacter(password) != false)) {
                    if (user == "" || password == "") {
                        if (langcode == 'es') {
                            $("#message").html("Es necesario introducir usuario y contraseña.");
                        }
                        else {
                            $("#message").html("UserName and Password are mandatory.");
                        }
                        $("#myModalError").modal('show');
                    }
                    else {
                        var origin = $("#origin").val();
                        var urlDestino = $("#urlHotel").val();
                        
                        var dataString = 'action=login&username=' + user + '&password=' + password + '&idhotel=' + $("#idhotel").val() + '&idlocation=' + $("#idlocation").val() + '&mac=' + $("#mac").val();
                        $.ajax({
                            url: "../resources/handlers/MerakiHandler.ashx",
                            data: dataString,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            beforeSend: function () {
                                $("#MyModalWait").modal('show');
                            },
                            complete: function () {
                                $("#MyModalWait").modal('hide');
                            },
                            success: function (pReturn) {
                                if (pReturn.code == 'OK') {                                        
                                    var continue_url = window.location.origin + "/successurl.aspx?username=" + $("#email_field").val().toUpperCase() + "&continue_url=" + pReturn.message;                                        
                                    var action = $("#origin").val();
                                    var newForm = jQuery('<form>', {
                                        'action': action,
                                        'method': 'POST',
                                    }).append(jQuery('<input>', {
                                        'name': 'email',
                                        'value': $("#email_field").val().toUpperCase(),
                                        'type': 'hidden'
                                    })).append(jQuery('<input>', {
                                        'name': 'password',
                                        'value': $("#password_field").val().toUpperCase(),
                                        'type': 'hidden'
                                    })).append(jQuery('<input>', {
                                        'name': 'success_url',
                                        'value': continue_url,
                                        'type': 'hidden'
                                    })).append(jQuery('<input>', {
                                        'name': 'continue_url',
                                        'value': pReturn.message,
                                        'type': 'hidden'

                                    }));
                                    $(document.body).append(newForm);
                                    newForm.submit();

                                }
                                else {
                                    if (langcode == 'es') {
                                        $("#message").html(pReturn.message);
                                    }
                                    else {
                                        $("#message").html(pReturn.message);
                                    }
                                    $("#myModalError").modal('show');
                                }
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                $("#loading").html("We have a problem, please try again later.");
                                alert('error: ' + xhr.statusText);
                            }
                        });


                    }
                }
                else {
                    if (langcode == 'es') {
                        $("#message").html("El nombre de usuario y contraseña solo puede tener caracteres del 0 - 9, A - Z.");
                    }
                    else {
                        $("#message").html("Username and password can only contain characters 0-9 ans A-Z.");
                    }
                    $("#myModalError").modal('show');
                }
                });

            $('#welcome_voucherbutton').click(function (e) {
                    e.preventDefault();
                    var user = $("#email_field_voucher").val().toUpperCase();
                    var password = $("#email_field_voucher").val().toUpperCase();                  

                    if ((ilegalCharacter(user) != false) && (ilegalCharacter(password) != false)) {
                        if (user == "") {
                            if (langcode == 'es') {
                                $("#message").html("Es necesario introducir usuario.");
                            }
                            else {
                                $("#message").html("UserName is mandatory.");
                            }
                            $("#myModalError").modal('show');
                        }
                        else {
                            var origin = $("#origin").val();
                            var urlDestino = $("#urlHotel").val();

                            var dataString = 'action=voucher&username=' + user + '&idhotel=' + $("#idhotel").val() + '&idlocation=' + $("#idlocation").val() + '&mac=' + $("#mac").val();
                            $.ajax({
                                url: "../resources/handlers/MerakiHandler.ashx",
                                data: dataString,
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                beforeSend: function () {
                                    $("#MyModalWait").modal('show');
                                },
                                complete: function () {
                                    $("#MyModalWait").modal('hide');
                                },
                                success: function (pReturn) {
                                    if (pReturn.code == 'OK') {
                                        var continue_url = window.location.origin + "/successurl.aspx?username=" + $("#email_field_voucher").val().toUpperCase() + "&continue_url=" + pReturn.message;
                                        var action = $("#origin").val();
                                        var newForm = jQuery('<form>', {
                                            'action': action,
                                            'method': 'POST',
                                        }).append(jQuery('<input>', {
                                            'name': 'email',
                                            'value': $("#email_field_voucher").val().toUpperCase(),
                                            'type': 'hidden'
                                        })).append(jQuery('<input>', {
                                            'name': 'password',
                                            'value': $("#email_field_voucher").val().toUpperCase(),
                                            'type': 'hidden'
                                        })).append(jQuery('<input>', {
                                            'name': 'success_url',
                                            'value': continue_url,
                                            'type': 'hidden'
                                        })).append(jQuery('<input>', {
                                            'name': 'continue_url',
                                            'value': pReturn.message,
                                            'type': 'hidden'

                                        }));
                                        $(document.body).append(newForm);
                                        newForm.submit();

                                    }
                                    else {
                                        if (langcode == 'es') {
                                            $("#message").html(pReturn.message);
                                        }
                                        else {
                                            $("#message").html(pReturn.message);
                                        }
                                        $("#myModalError").modal('show');
                                    }
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    $("#loading").html("We have a problem, please try again later.");
                                    alert('error: ' + xhr.statusText);
                                }
                            });


                        }
                    }
                    else {
                        if (langcode == 'es') {
                            $("#message").html("El nombre de usuario y contraseña solo puede tener caracteres del 0 - 9, A - Z.");
                        }
                        else {
                            $("#message").html("Username and password can only contain characters 0-9 ans A-Z.");
                        }
                        $("#myModalError").modal('show');
                    }
            });


                $('#myTab a').click(function (e) {
                    e.preventDefault()
                    $(this).tab('show')
                })

                $('#freeread').click(function (e) {
                    e.preventDefault();
                    $("#myModal").modal('show');

                });

                $('#premiumread').click(function (e) {
                    e.preventDefault();
                    $("#myModal").modal('show');

                });

                $('#socialread').click(function (e) {
                    e.preventDefault();
                    $("#myModal").modal('show');

                });

                $('#surveyfreebutton').click(function (e) {
                    e.preventDefault();
                    $("#myModal2").modal('show');
                });

                $('#surveysave').click(function (e) {
                    e.preventDefault();

                    var survey = 'loginform:';
                    var parameters = getParameters();

                    var valid = true;
                    $('input[rel^="survey"]').each(function (input) {
                        survey += $(this).attr('question') + '=' + $(this).val() + '|';
                        if ($(this).val() == '')
                            valid = false;
                    });

                    $('select[rel^="survey"]').each(function (input) {
                        survey += $(this).attr('question') + '=' + $(this).val() + '|';
                        if ($(this).val() == '')
                            valid = false;
                    });

                    if (valid == false) {
                        if (langcode == 'es') {
                            $("#message2").html("Todos los campos son obligatorios.").removeClass().addClass('alert alert-danger').show();
                        }
                        else {
                            $("#message2").html("All fields are mandatory.").removeClass().addClass('alert alert-danger').show();
                        }
                    }
                    else {

                        $("#myModalSurvey").modal('hide');

                        var dataString = 'action=freeaccess&idhotel=' + $("#idhotel").val() + '&survey=' + survey + '&idlocation=' + $("#idlocation").val() + '&mac=' + $("#mac").val() + '&origin=' + $("#origin").val() + "&idbilling=" + $("#freeaccess").val();
                        $.ajax({
                            url: "../resources/handlers/MerakiHandler.ashx",
                            data: dataString,
                            contentType: "application/json; charset=utf-8",
                            dataType: "text",
                            beforeSend: function () {
                                $("#MyModalWait").modal('show');
                            },
                            complete: function () {
                                $("#MyModalWait").modal('hide');
                            },
                            success: function (pResult) {
                                var lResult = JSON.parse(pResult);

                                if (lResult.IdUser == -1) {
                                    if (langcode != 'es') {
                                        $("#message").html("No more free access are allowed now.");
                                    }
                                    else {
                                        $("#message").html("No se permiten más accesos gratuitos en este periodo.");
                                    }
                                    $("#myModalError").modal('show');
                                }
                                else if (lResult.IdUser == -3) {
                                    if (langcode != 'es') {
                                        $("#message").html("We are currently experiencing a problem, please try again later.");
                                    }
                                    else {
                                        $("#message").html("Tenemos un problema temporal, intentelo de nuevo más tarde.");
                                    }
                                    $("#myModalError").modal('show');
                                }
                                else if (lResult.IdUser == 0) {
                                    if (langcode != 'es') {
                                        $("#message").html("We are currently experiencing a problem, please try again later.");
                                    }
                                    else {
                                        $("#message").html("Tenemos un problema temporal, intentelo de nuevo más tarde.");
                                    }
                                    $("#myModalError").modal('show');
                                }
                                else {
                                    var continue_url = window.location.origin + "/successurl.aspx?username=" + lResult.UserName.toUpperCase() + "&continue_url=" + $("#urlDest").val();
                                    var action = $("#origin").val();
                                    var newForm = jQuery('<form>', {
                                        'action': action,
                                        'method': 'POST',
                                    }).append(jQuery('<input>', {
                                        'name': 'email',
                                        'value': lResult.UserName.toUpperCase(),
                                        'type': 'hidden'
                                    })).append(jQuery('<input>', {
                                        'name': 'password',
                                        'value': lResult.Password.toUpperCase(),
                                        'type': 'hidden'
                                    })).append(jQuery('<input>', {
                                        'name': 'success_url',
                                        'value': continue_url,
                                        'type': 'hidden'
                                    })).append(jQuery('<input>', {
                                        'name': 'continue_url',
                                        'value': lResult.Url,
                                        'type': 'hidden'

                                    }));
                                    $(document.body).append(newForm);
                                    newForm.submit();

                                    }

                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    $("#loading").html("Su ha producido un error durante el envio. Por favor intentelo más tarde.");
                                    alert('error: ' + xhr.statusText);
                                    $("#nextstep3").show();
                                    $("#backstep3").show();

                                }
                            });
                        }
                });

                $('#premiumbutton').click(function (e) {
                    e.preventDefault();
                    var dataString = 'action=allowpay&idhotel=' + $("#idhotel").val() + '&idlocation=' + $("#idlocation").val() + '&mac=' + $("#mac").val() + '&origin=' + $("#origin").val();
                    $.ajax({
                        url: "../resources/handlers/MerakiHandler.ashx",
                        data: dataString,
                        contentType: "application/json; charset=utf-8",
                        dataType: "text",
                        beforeSend: function () {
                            $("#MyModalWait").modal('show');
                        },
                        complete: function () {
                            $("#MyModalWait").modal('hide');
                        },
                        success: function (pResult) {
                            var lResult = JSON.parse(pResult);

                            if (lResult.allow == 1) {
                                $("#finishbutton").attr("href", lResult.Url);
                                $("#finishInformation").append($("#freeaccessinformation").html());

                                var continue_url = window.location.origin + "/successurl.aspx?username=" + lResult.UserName.toUpperCase() + "&continue_url=" + $("#urlDest").val();
                                var action = $("#origin").val();
                                var newForm = jQuery('<form>', {
                                    'action': action,
                                    'method': 'POST',
                                }).append(jQuery('<input>', {
                                    'name': 'email',
                                    'value': lResult.UserName.toUpperCase(),
                                    'type': 'hidden'
                                })).append(jQuery('<input>', {
                                    'name': 'password',
                                    'value': lResult.Password.toUpperCase(),
                                    'type': 'hidden'
                                })).append(jQuery('<input>', {
                                    'name': 'success_url',
                                    'value': continue_url,
                                    'type': 'hidden'
                                })).append(jQuery('<input>', {
                                    'name': 'continue_url',
                                    'value': lResult.Url,
                                    'type': 'hidden'

                                }));
                                $(document.body).append(newForm);
                                newForm.submit();
                            }
                            else {
                                var billingType = $("#accesstype").val();
                                if (billingType == "0") {
                                    if (langcode == 'es') {
                                        $("#message").html("Debe seleccionar un tipo de acceso premium.");
                                    }
                                    else {
                                        $("#message").html("You must select an access premium type to continue.");
                                    }
                                    $("#myModalError").modal('show');

                                }
                                else {
                                    $("#myModalPremium").modal('show');
                                }
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#loading").html("Su ha producido un error durante el envio. Por favor intentelo más tarde.");
                            alert('error: ' + xhr.statusText);
                            $("#nextstep3").show();
                            $("#backstep3").show();

                        }
                    });


                });

                $("#ContinuePremium").click(function (e) {
                    var billingType = $("#accesstype").val();
                    var mail = $("#premiummail").val();
                    if (isValidEmailAddress(mail) == false || mail == '') {
                        if (langcode == 'es') {
                            $("#message").html("El correo electrónico no tiene el formato correcto.");
                        }
                        else {
                            $("#message").html("Email format is not correct.");
                        }
                        $("#myModalError").modal('show');
                    }
                    else {

                        var parameters = getParameters();
                        var dataString = 'action=paypalrequestnosurvey&idhotel=' + $("#idhotel").val() + '&IdBillingType=' + billingType + '&mac=' + $("#mac").val() + '&idlocation=' + $("#idlocation").val() + '&lang=' + langcode + '&origin=' + $("#origin").val() + '&email=' + mail;
                        $.ajax({
                            url: "../resources/handlers/MerakiHandler.ashx",
                            data: dataString,
                            contentType: "application/json; charset=utf-8",
                            dataType: "text",
                            beforeSend: function () {
                                $("#MyModalWait").modal('show');
                            },
                            complete: function () {
                                $("#MyModalWait").modal('hide');
                            },
                            success: function (pResult) {
                                var lResult = JSON.parse(pResult);

                                var url = 'paypal.aspx?IdTransaction=' + lResult.IdTransaction + '&item_name=' + lResult.Description + '&amount=' + lResult.Price + '&idhotel=' + $("#idhotel").val() + '&idlocation=' + $("#idlocation").val() + '&mac=' + $("#mac").val() + '&origin=' + $("#origin").val() + '&lang=' + langcode
                                window.location.href = url;

                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                $("#loading").html("We are currently experiencing a problem, please try again later.");
                                alert('error: ' + xhr.statusText);
                            }
                        });
                    }
                });

                $('#accesstype').change(function (e) {
                    e.preventDefault();
                    if ($("#accesstype").val() != "0") {
                        var html = $("#accesstype_details_" + $("#accesstype").val()).html();
                        $("#connectioninfo").html("<small>" + html + "</small>");
                    }
                    else {
                        $("#connectioninfo").html("");
                    }

                });

                $("#finishbutton").click(function (e) {
                    e.preventDefault();
                    var url = $("#finishbutton").attr("href");
                    window.location.href = url;

                });

                $("#rejectconditions").click(function (e) {
                    $("#principal").fadeOut("slow");
                    $("#noaccept").fadeIn("slow");
                    $("#myModal").modal('hide');
                });

                $("#retrybutton").click(function (e) {
                    $("#myModal").modal('show');
                });

                $("#disclaimerMandatoryButton").click(function (e) {
                    $("#myModalMandatoryDisclaimer").modal('show');
                });

                $("#modalscroll").bind('scroll', chk_scroll);

                $("#acceptconditionsbutton").click(function (e) {
                    if ($("#acceptcheck").is(":checked") && $("#olderthancheck").is(":checked") && scrollcomplete == true) 
                    {
                        $("#myModal").modal('hide');
                        $("#noaccept").fadeOut("fast");
                        $("#principal").fadeIn("slow");

                        validar = true;
                    }
                    else {
                        switch (lang.substring(0, 2)) {
                            case 'es': $("#message").html("Por favor, antes de Aceptar debe leer totalmente el texto (utilice el control deslizante) y aceptar la Politica de Privacidad y las Condiciones de Uso del servicio."); break;
                            default: $("#message").html("Please, before proceeding you must read completely the legal text and accept the Privacy Policy and the Terms of Use."); break;
                        }
                        $("#myModalError").modal("show");
                    }
                });

                function chk_scroll(e) {
                    var elem = $(e.currentTarget);

                    if (elem.scrollTop() >= (elem[0].scrollHeight - elem.outerHeight()) * 0.8) {
                        scrollcomplete = true;
                    }

                }

                $('#myModal').on('show.bs.modal', function () {
                    $('#modalscroll').css('overflow-y', 'auto');
                    $('#modalscroll').css('max-height', $(window).height() * 0.25);
                });

                $("#errorbutton").click(function (e) {
                    e.preventDefault();
                    $("#myModalError").modal('hide');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                });
            });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
            <div id="fb-root"></div>
    <div class="container">
        <div class="row" id="wait">
            <div class="col-xs-12 col-sm-12 col-md-12">
                  <div id="borderbox" class="panel panel-default">
                  <div class="panel-body">
                    <div class="progress">
                            <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                <span class="sr-only">Loading - Waiting please</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" id="noaccept" style="display:none;">
            <div class="col-xs-12 col-sm-12 col-md-12">
                  <div class="panel panel-default">
                  <div class="panel-body text-center">
                      <p class="text-center" id="sorrytext">We are sorry</p>  
                      <p class="text-center"> <img src="resources/images/no-disponible.png" style="width:150px" id="noacceptimage"/></p> 
                        <p class="text-center" id="noaccepttext">In order to use the service you must accept the Terms of Use</p>
                        <p class="text-center"><button type="button" class="btn btn-primary"  id="retrybutton">Try Again</button></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" id="principal" style="display:none">
            <div class="hidden-xs hidden-sm col-md-8 col-lg-8">
                <div class="panel panel-default">
                  <div class="panel-body">
                        <img src="resources/images/no-disponible.png" id="imagehome" class="img-responsive img-rounded" />
                  </div>
                </div>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="panel panel-default">
                  <div class="panel-body">
                    <ul class="nav nav-tabs" role="tablist" id="myTab">
                      <li id="tab-social" style="display:none;"><a href="#facebook" id="sociallabel">Social</a></li>
                      <li id="tab-payaccess" style="display:none;"><a href="#pay" id="premiumlabel">Premium</a></li>
                      <li id="tab-free" style="display:none;"><a href="#free" id="freelabel">Free</a></li>
                      <li id="tab-login" style="display:none;"><a href="#login" id="loginlabel">Login</a></li>
                      <li id="tab-voucher" style="display:none;"><a href="#voucher" id="voucherlabel">Voucher</a></li>
                    </ul>
                      <!-- Tab panes -->
                    <div id="myTabContent" class="tab-content">
                     <div class="tab-pane fade" id="login">
                          <p class="text-center"> <img src="resources/images/no-disponible.png" style="width:150px" id="loginimage"/></p> 
                         <div class="panel panel-info"><div class="panel-body" id="logintext"></div></div>
                            <div role="form">
                                <div class="form-group">
                                <label for="username" id="usernamelabel">Username</label>
                                <input type="text" class="form-control" id="email_field" name="email" placeholder="Username"/>
                                </div>
                                <div class="form-group">
                                <label for="password" id="passwordlabel">Password</label>
                                <input type="password" class="form-control" id="password_field" name="password" placeholder="Password"/>
                                </div>
                               <input type="submit" class="btn btn-primary btn-lg btn-block" id="welcome_loginbutton" value="login" />
                            </div>                     
                     </div>
                    <div class="tab-pane fade" id="voucher">
                          <p class="text-center"> <img src="resources/images/no-disponible.png" style="width:150px" id="voucherimage"/></p> 
                         <div class="panel panel-info"><div class="panel-body" id="vouchertext"></div></div>
                            <div role="form">
                                <div class="form-group">
                                <label for="username" id="usernamevoucherlabel">Access-Code</label>
                                <input type="text" class="form-control" id="email_field_voucher" name="email" placeholder="Access-Code"/>
                                </div>
                               <input type="submit" class="btn btn-primary btn-lg btn-block" id="welcome_voucherbutton" value="login" />
                            </div>                     
                     </div>
                      <div class="tab-pane fade" id="free">
                          <p class="text-center"> <img src="resources/images/no-disponible.png" style="width:150px" id="freeimage"/></p> 
                          <div class="panel panel-info"><div class="panel-body" id="freetext"></div></div>
                          
                          <button type="button" class="btn btn-primary btn-lg btn-block" id="welcome_freebutton">Get a free access!</button>

                          
                      </div>

                      <div class="tab-pane fade" id="pay">
                            <p class="text-center"> <img src="resources/images/no-disponible.png" style="width:150px" id="premiumimage"/></p> 
                          <div class="panel panel-info"><div class="panel-body" id="premiumtext"></div></div>
                          <div role="form">
                                <div class="form-group">
                                <label for="accesstype" id="premiumaccesstype">Select your access type</label>
                                <select class="form-control" id="accesstype"><option value="0">Option 1  - 12 €</option><option value="0">Option 1  - 12 €</option><option value="0">Option 1  - 12 €</option></select>
                                </div>
                                <div class="form-group">
                                    <div class="panel panel-warning"><div class="panel-body" id="connectioninfo"></div></div>
                                </div>

                                <button type="button" class="btn btn-primary btn-lg btn-block" id="premiumbutton">Buy</button>
                            </div>
                      </div>
                      <div class="tab-pane fade" id="facebook">
                           <p class="text-center"> <img src="resources/images/no-disponible.png" style="width:150px" id="socialimage"/></p> 
                          <div class="panel panel-info"><div class="panel-body" id="socialText"></div></div>
                          <div class="panel panel-default text-center" id="socialdiv" style="display:none"><div class="panel panel-heading text-center"><h3 id="socialPageName"></h3></div><div class="panel-body" id="panelMeGusta"></div></div>
                          <button type="button" id="welcome_socialbutton" class="btn btn-primary btn-lg btn-block"><i class="fa fa-facebook-square"></i> Facebook</button> 
                          <button type="button" id="welcome_instagrambutton" class="btn btn-warning btn-lg btn-block"><i class="fa fa-instagram"></i> Instagram</button> 
                          <div id="gSignInWrapper" >
                            <div id="customBtn" class="customGPlusSignIn" >
                              <span class="icon"></span>
                              <span class="buttonText">Sign in with Google</span>
                            </div>
                          </div>
                          <button type="button" id="welcome_linkedinsocialbutton" class="btn btn-info btn-lg btn-block"><i class="fa fa-linkedin"></i> LinkedIn</button> 
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" id="disclaimermandatory" style="display:none;">
                <a href="#" id="disclaimerMandatoryButton" class="btn btn-default">Legal Advice</a>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="overflow-y: initial;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title text-center" id="welcometext">Welcome</h4>
                    <p class="text-center"> <img src="resources/images/no-disponible.png" style="height:55px" id="disclaimerimage"/></p> 
                    <p class="text-center" id="disclaimerwelcome">Please read the text below and confirm you agree with it.</p>
                </div>
                <div class="modal-body" id="modalscroll">
                    <div class="panel-body" id="disclaimertext"></div>
                </div>
                <div class="modal-footer">
                    <div class="form-check text-left">
                        <input type="checkbox" id="olderthancheck" class="form-check-input"  /><label  class="form-check-label" for="olderthancheck" id="labeloltherthan" style="font-size:12px; display:initial;"> I accept the <a href='legal.aspx?iddisclaimertype=1' id='olderlink' target='_blank'>Privacy Policy</a> and I am over 16-years or have been authorized by my legal tutor</label><br />
                        <input type="checkbox" id="acceptcheck" class="form-check-input"  /><label class="form-check-label" for="acceptcheck" id="labelacceptcheck" style="font-size:12px; display:initial;"> I accept the  <a href='legal.aspx?iddisclaimertype=2' id='termslink' target='_blank'>Terms and Conditions of Use</a></label>
                    </div>
                    <br />
                    <div class="col-xs-6 col-sm-6 col-md-6"><button type="button" class="btn btn-default btn-lg btn-block btn-sm" id="rejectconditions">Decline</button></div>
                    <div class="col-xs-6 col-sm-6 col-md-6"><button type="button" class="btn btn-primary btn-lg btn-block btn-sm" id="acceptconditionsbutton">Accept</button></div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade in" id="myModalSurvey" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title text-center" id="surveyheader">User Data</h4>
                </div>
                <div class="modal-body" id="survey">
                </div>
                <div class="modal-footer">
                    <div id="message2"></div>
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="surveyclose">Close</button>
                    <button type="button" class="btn btn-primary" id="surveysave">Continue</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModalPremium" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center" id="premiummailHeader">Email</h4>
                </div>
            <div class="modal-body" id="Div2">
                <div id="premiummailtext">Please provide your email address so we can send the access credentials</div>
                <div class="form-group"><label id="labelpremiumemail">Email</label><input name="email" id="premiummail" type="text" class="form-control" placeholder="Email" /></div>
            </div>
            <div class="modal-footer">
                <div id="message3"></div>
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="CancelPremium">Close</button>
                    <button type="button" class="btn btn-primary" id="ContinuePremium">Continue</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModalError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title text-center" id="H2">Error</h4>
                </div>
                <div class="modal-body">
                    <div id="message" class="alert alert-danger text-center"></div> 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="errorbutton">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModalMandatoryDisclaimer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title text-center" id="disclaimermandatoryheader">Legal Advice</h4>
                </div>
                <div class="modal-body" id="disclaimermandatorytext">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="diclaimermandatoruclose">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModalFinish" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center" id="finishlabel">Congratulations</h4>
                </div>
                <div class="modal-body">
                    <div id="finishInformation" style="text-align:center" ><img src="resources/images/no-disponible.png" alt="logo hotel" id="logofinish" style="width:150px"  /><br /><br /></div>
                    <br />
                    <input type="button" id="finishbutton" class="btn btn-success btn-block" value="Continue browsing" /> 
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="MyModalWait" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center" id="waitheader">Please, wait a moment</h4>
                </div>
                <div class="modal-body">
                    <div class="progress">
                        <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                            <span class="sr-only">Waiting please</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="accesstypes" style="display:none"></div>
        <div id="freeaccessinformation" style="display:none"></div>
        <div id="socialaccessinformation" style="display:none"></div>
        <input type="text" hidden="hidden" id="idsocialbillintype" />
        <input type="text" hidden="hidden" id="mandatorysurvey" />
        <input type="text" hidden="hidden" id="freeaccess" />
        <input type="text" hidden="hidden" id="socialaccess" />
</asp:Content>
