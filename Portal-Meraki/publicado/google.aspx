﻿<%@ Page Title="" Language="C#" MasterPageFile="~/resources/master/MerakiSecureMaster.Master" AutoEventWireup="true" CodeBehind="google.aspx.cs" Inherits="Portal_Meraki.google" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>

       
    $(document).ready(function () {
        var cookie = getCookie("Meraki");
         if (cookie != "") {
             values = cookie.split('&');
        
             $("#idhotel").val(values[0].split('=')[1]);
             $("#idlocation").val(values[1].split('=')[1]);
             $("#mac").val(values[2].split('=')[1]);
             $("#origin").val(values[3].split('=')[1]);
             $("#idsocialbillintype").val(values[4].split('=')[1]);
             $("#token").val(values[5].split('=')[1]);
             //$("#ga_ssid").val(values[6].split('=')[1]);
             //$("#ga_nas_id").val(values[7].split('=')[1]);
             //$("#ga_ap_mac").val(values[8].split('=')[1]);
             //$("#ga_cmac").val(values[9].split('=')[1]);
             var billingType = values[4].split('=')[1];
             var google_email = values[6].split('=')[1];
             var google_name = values[7].split('=')[1];
         }
         else
             alert("No hay cookie");
        
         var lang = navigator.browserLanguage ? navigator.browserLanguage : navigator.language;
         var langcode = lang.substring(0, 2);
         if ((langcode != 'es') && (langcode != 'en'))
             langcode = $("#languagecode").val();      
        
         var survey = 'Google: nombre=' + google_name;
         if (lang.substring(0, 2) == 'es') {
             $("#userInformation").html("");
             $("#userInformation").append("<h3>Información del Usuario</h3>");
             $("#userInformation").append("Email: <b>" + google_email + "</b><br />");
             $("#userInformation").append("Nombre: <b>" + google_name + "</b><br />");

         }
         else {
             $("#userInformation").html("");
             $("#userInformation").append("<h3>User Information </h3>");
             $("#userInformation").append("Email: <b>" + google_email + "</b><br />");
             $("#userInformation").append("Name: <b>" + google_name + "</b><br />");
         }

         $("#surveytext").val(survey);
         var dataStringm = 'action=socialnetwork&email=' + google_email + '&mac=' + $("#mac").val() + '&idhotel=' + $("#idhotel").val() + '&idlocation=' + $("#idlocation").val() + '&origin=' + $("#origin").val() + '&survey=' + survey + "&lang=" + langcode + "&idbillingtype=" + billingType;
         $.ajax({
             url: "../resources/handlers/MerakiHandler.ashx",
             data: dataStringm,
             contentType: "application/json; charset=utf-8",
             dataType: "text",
             beforeSend: function () {
                 $("#MyModalWait").modal('show');
             },
             complete: function () {
                 $("#MyModalWait").modal('hide');
             },
             success: function (pResult) {
                 var lResult = JSON.parse(pResult);
                 if (lResult.IdUser == -1) {
                     if (langcode != 'es') {
                         $("#message").html("No more free access are allowed now.");
                     }
                     else {
                         $("#message").html("No se permiten más accesos gratuitos en este periodo.");
                     }
                     $("#myModalError").modal('show');
                 }
                 else if (lResult.IdUser == -3) {
                     if (langcode != 'es') {
                         $("#message").html("We are currently experiencing a problem, please try again later.");
                     }
                     else {
                         $("#message").html("Tenemos un problema temporal, intentelo de nuevo más tarde.");
                     }
                     $("#myModalError").modal('show');
                 }
                 else if (lResult.IdUser == 0) {
                     if (langcode != 'es') {
                         $("#message").html(lResult.UserName);
                     }
                     else {
                         $("#message").html(lResult.UserName);
                     }
                     $("#myModalError").modal('show');
                 }
                 else {
                     $("#finishbutton").attr("href", lResult.Url);
                     $("#finishInformation").append($("#socialaccessinformation").html());
        
                     if (langcode == 'es') {
                         $("#finishlabel").html("Bienvenido de nuevo " + $("#Google_name").val());
                         $("#finishInformation").append('<b>Nombre de usuario: </b>' + lResult.UserName + '<br />');
                         $("#finishInformation").append('<b>Contraseña: </b>' + lResult.Password + '<br />');
                         $("#finishInformation").append('<b>Fecha de expiración: </b>' + lResult.ValidTill + '<br />');
                         $("#finishbutton").html("Continuar navegando");
                     }
                     else {
                         $("#finishlabel").html("Welcome again " + $("#Google_name").val());
                         $("#finishInformation").append('<b>UserName: </b>' + lResult.UserName + '<br />');
                         $("#finishInformation").append('<b>Password: </b>' + lResult.Password + '<br />');
                         $("#finishInformation").append('<b>Valid Till: </b>' + lResult.ValidTill + '<br />');
                         $("#finishbutton").html("Continue browsing");
                     }
        
                     var val = "username=" + lResult.UserName + "&password=" + lResult.Password + "&validtill=" + lResult.ValidTill + "&continue_url=" + lResult.Url;
                     createCookie("Meraki", val, 1);

                     var continue_url = window.location.origin + "/Success.aspx?idhotel=" + $("#idhotel").val() + '&idlocation=' + $("#idlocation").val() + '&mac=' + $("#mac").val() + "&username=" + lResult.UserName + "&continue_url=" + $("#origin").val();
                     var action = $("#origin").val();
                     var newForm = jQuery('<form>', {
                         'action': action,
                         'method': 'POST',
                     }).append(jQuery('<input>', {
                         'name': 'email',
                         'value': lResult.UserName,
                         'type': 'hidden'
                     })).append(jQuery('<input>', {
                         'name': 'password',
                         'value': lResult.Password,
                         'type': 'hidden'
                     })).append(jQuery('<input>', {
                         'name': 'success_url',
                         'value': continue_url,
                         'type': 'hidden'
                     })).append(jQuery('<input>', {
                         'name': 'continue_url',
                         'value': lResult.message,
                         'type': 'hidden'
        
                     }));
                     $(document.body).append(newForm);
                     newForm.submit();
                 }
        
             },
             error: function (xhr, ajaxOptions, thrownError) {
                 $("#message2").html("We have a problem, please try again later.");
                 alert('error: ' + xhr.statusText);
             }
         });
      });

    $("#finishbutton").click(function (e) {
        e.preventDefault();
        var url = $("#finishbutton").attr("href");
        window.location.href = url;
    });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
            <input type="text" hidden="hidden" id="ga_ssid" />
        <input type="text" hidden="hidden" id="ga_nas_id" />
        <input type="text" hidden="hidden" id="ga_ap_mac" />
        <input type="text" hidden="hidden" id="ga_cmac" />
        <input type="text" hidden="hidden" id="token" />
</asp:Content>
