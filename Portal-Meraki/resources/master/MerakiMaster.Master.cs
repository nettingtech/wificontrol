﻿using Portal.resources.utils;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using Wifi360.Data.Class;
using Wifi360.Data.Controllers;
using Wifi360.Data.Model;
using System.Security.Cryptography;

namespace Portal.resources.master
{
    public partial class MerakiMaster : System.Web.UI.MasterPage
    {
        protected string language = string.Empty;
        protected string idhotel = string.Empty;
        protected string idlocation = string.Empty;
        protected string mac = string.Empty;
        protected string origin = string.Empty;
        protected string error = string.Empty;
        protected string urlHotel = string.Empty;
        protected string urlDest = string.Empty;
        protected string username = string.Empty;
        protected string password = string.Empty;
        protected string cloudre = string.Empty;
        protected string validtill = string.Empty;
        private Hotels hotel = null;
        private Locations2 location = null;
        protected Guid idproceso = Guid.Empty;

        //static string key { get; set; } = "A!9HHhi%XjjYY4YP2@Nob009X";


        private static LogDaily log = null;
        private bool isLog = false;


        /// <summary>
        /// En el método preInit se realizan las comprobaciones para saber si el acceso al portal es autorizada o no.
        /// Obtenemos todos los parametros que necesitamos para identificar el site y la location
        /// Comprobamos y guardamos el USERAGENT
        /// En caso de no ser autorizado o el usergent estar bloqueado, se finaliza la ejecución
        /// Esta Master es para equipos de MIKROTIK
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                try
                {
                    isLog = Boolean.Parse(ConfigurationManager.AppSettings["isLog"].ToString());
                    idproceso = Guid.NewGuid();

                    if (isLog)
                    {
                        log = new LogDaily("Meraki-Captive", HttpContext.Current.Server.MapPath("/log"));
                        log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Página: {1} ", idproceso, Page.Page));
                        log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Inicio del proceso", idproceso));
                    }

                    if (HttpContext.Current.Request.Params["mikrotik"] != null)
                        if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Parametros de vuelta del facebook: {1} ", idproceso, Page.Page));

                    #region OBTENER PARAMETRO DE ENTRADA (HOTEL, LOCATION, MAC, ORIGEN, ETC)
                    //OBTENEMOS LOS PARAMETRO POR EL METODO POST
                    string hotelid = Request.Params["idhotel"];
                    string locationid = Request.Params["idlocation"];
                    string macs = string.Empty;
                    if (Request.Params["client_mac"] != null)
                        macs = Request.Params["client_mac"];
                    if (Request.Params["login_url"] != null)
                        origin = Request.Params["login_url"].Split('&')[0];
                    urlDest = Request.Params["continue_url"];
                    if (Request.Params["error_message"] != null)
                        error = Request.Params["error_message"];

                    
                    if (Session["hotelid"] == null)
                        Session["hotelid"] = hotelid;
                    if (Session["locationid"] == null)
                        Session["locationid"] = locationid;
                    if (Session["mac"] == null)
                        Session["mac"] = macs;
                    if (Session["origin"] == null)
                        Session["origin"] = origin;
                    if (Session["error"] == null)
                        Session["error"] = error;

                    if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Variable de session hotelid: {1}", idproceso, Session["hotelid"].ToString()));

                    if (isLog)
                    {
                        log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Lectura parámetros método POST.", idproceso));
                        log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Parámetros: hotelid --> {1} ; locationid = {2} ; macs = {3} ; origin = {4} ; error = {5} ; urlDest = {6}.", idproceso, hotelid, locationid, macs, origin, error, urlDest));
                    }

                    if (string.IsNullOrEmpty(hotelid)) //EN CASO DE NO TENER POST PROBAMOS POR GET O COOKIE
                    {
                        if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Lectura parámetros método POST sin resultados.", idproceso));
                        if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Lectura parámetros método GET.", idproceso));

                        if (HttpContext.Current.Request["idhotel"] != null)
                        {
                            hotelid = HttpContext.Current.Request["idhotel"].ToString();
                            locationid = HttpContext.Current.Request["idlocation"].ToString();
                            macs = HttpContext.Current.Request["mac"].ToString();
                            origin = HttpContext.Current.Request["origin"].ToString();
                            error = HttpContext.Current.Request["error"].ToString();

                            if (isLog)
                            {
                                log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Parámetros: hotelid --> {1} ; locationid = {2} ; macs = {3} ; origin = {4} ; error = {5}.", idproceso, hotelid, locationid, macs, origin, error));
                                if (!string.IsNullOrEmpty(hotelid))
                                    log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Lectura parámetros método GET COMPLETADA.", idproceso));
                                else
                                    log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Lectura parámetros método GET COMPLETADA sin resultados.", idproceso));
                            }
                        }
                        else if (Session["hotelid"] != null)
                        {
                            if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Lectura parámetros método GET sin resultados.", idproceso));
                            if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Lectura parámetros método VARIABLE DE SESSION.", idproceso));

                            hotelid = Session["hotelid"].ToString();
                            locationid = Session["locationid"].ToString();
                            macs = Session["mac"].ToString();
                            origin = Session["origin"].ToString();
                            error = Session["error"].ToString();

                            if (isLog)
                            {
                                if (!string.IsNullOrEmpty(hotelid))
                                    log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Lectura parámetros método VARIABLE DE SESSION COMPLETADA.", idproceso));
                                else
                                    log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Lectura parámetros método VARIABLE DE SESSION sin resultados.", idproceso));
                            }
                        }
                        else
                        {
                            if (isLog)
                            {
                                log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Lectura parámetros VARIABLE DE SESSION sin resultados.", idproceso));
                                log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Lectura parámetros de COOKIE.", idproceso));
                            }
                            if (HttpContext.Current.Request.Cookies["meraki"] != null)
                            {

                                HttpCookie cookie = HttpContext.Current.Request.Cookies["meraki"];
                                if (cookie != null)
                                {
                                    if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}| COOKIE VALUE : {1}", idproceso, cookie.Value));
                                    hotelid = cookie.Values["idhotel"].ToString();
                                    locationid = cookie.Values["idlocation"].ToString();
                                    macs = cookie.Values["mac"].ToString();
                                    origin = cookie.Values["origin"].ToString();
                                }
                                else
                                    if (isLog) log.FileWriteEntry(ELogType.Error, string.Format("{0}| COOKIE NO EXISTE O VACIA", idproceso));

                                if (isLog)
                                {

                                    log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Parámetros: hotelid --> {1} ; locationid = {2} ; macs = {3} ; origin = {4} ; error = {5}.", idproceso, hotelid, locationid, macs, origin, error));
                                    if (!string.IsNullOrEmpty(hotelid))
                                        log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Lectura parámetros de COOKIE COMPLETADA.", idproceso));
                                }
                            }
                            else if (HttpContext.Current.Response.Cookies["meraki"] != null)
                            {
                                HttpCookie cookie = HttpContext.Current.Request.Cookies["meraki"];
                                if (cookie != null)
                                {
                                    if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}| COOKIE VALUE : {1}", idproceso, cookie.Value));

                                    hotelid = cookie.Values["idhotel"].ToString();
                                    locationid = cookie.Values["idlocation"].ToString();
                                    macs = cookie.Values["mac"].ToString();
                                    origin = cookie.Values["origin"].ToString();
                                }
                                else
                                {
                                    if (isLog) log.FileWriteEntry(ELogType.Error, string.Format("{0}| COOKIE NO EXISTE O VACIA", idproceso));
                                }

                                if (isLog)
                                {

                                    log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Parámetros: hotelid --> {1} ; locationid = {2} ; macs = {3} ; origin = {4} ; error = {5}.", idproceso, hotelid, locationid, macs, origin, error));
                                    if (!string.IsNullOrEmpty(hotelid))
                                        log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Lectura parámetros de COOKIE COMPLETADA.", idproceso));
                                }
                            }
                            else
                            {

                                if (isLog) log.FileWriteEntry(ELogType.Warning, string.Format("{0}|Fin del proceso: NO HAY PARÁMETROS QUE LEER", idproceso));
                                //Response.End();
                            }
                        }
                    }
                    else
                    {
                        if (isLog)
                        {
                            log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Lectura parámetros método POST COMPLETADA.", idproceso));
                        }
                    }
                    #endregion

                    string userAgent = Request.UserAgent;

                    if (!string.IsNullOrEmpty(macs))
                    {
                        string[] macpre = macs.Split(',');

                        if (macpre.Length >= 1)
                        {
                            mac = macpre[0].Replace(':', '-').ToUpper();
                        }
                        else
                            mac = macs;
                    }

                    if (!string.IsNullOrEmpty(hotelid)) //COMPROBAMOS QUE AUNQUE VENGA HOTEL NO VENGA VACIO
                    {
                        #region COMPROBAMOS USERAGENT Y MAC

                        if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|PROCESANDO MAC --> {1}", idproceso, mac));

                        //COMPROBAMOS SI EL USERAGENT ESTÁ EN LA LISTA NEGRA
                        BlackListUserAgents blackUA = BillingController.GetBlackUserAgent(userAgent);

                        if (blackUA.IdBlackUserAgent.Equals(0)) //SI NO ESTÁ EN LA LISTA NEGRA, VAMOS A COMPROBAR LA LISTA NEGRA DE LAS MACS
                        {
                            if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|USERAGENT PERMITIDO --> {1}", idproceso, userAgent));
                            hotel = BillingController.GetHotel(Int32.Parse(hotelid));
                            location = BillingController.GetLocation(Int32.Parse(locationid));
                            //FDSLocationsIdentifier locationIdentifier2 = BillingController.GetFDSLocationIdentifier(locationid);
                            //if (!locationIdentifier2.Id.Equals(0))
                            //{
                            //    hotel = BillingController.GetHotel(locationIdentifier2.IdSite);
                            //    location = BillingController.GetLocation(locationIdentifier2.IdLocation);
                            //}
                            //else
                            //{
                            //    location = BillingController.GetLocation(Int32.Parse(locationid));
                            //    hotel = BillingController.GetHotel(location.IdHotel);

                            //}


                            if (hotel.MACBlocking) //SI TIENE ACTIVADO EL BLOQUEO POR MACS
                            {
                                if (!string.IsNullOrEmpty(mac))
                                {
                                    #region MACBLOCKING
                                    //COMPROBAMOS SI LA MAC ESTÁ BLOQUEADA
                                    BlackListMACs blackMAC = BillingController.GetBlackListMac(mac, DateTime.Now);

                                    if (blackMAC.IdMACBlackList.Equals(0)) //SI LA MAC NO ESTÁ BLOQUEADA
                                    {
                                        if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|MAC PERMITIDA --> {1}", idproceso, mac));
                                        //COMPROBAMOS SI HAY QUE METER LA MAC EN LA LISTA NEGRA
                                        if (hotel.MaxMACAttemps <= BillingController.GetUserAgentsTemps(mac, hotel.MACAttempsInterval)) //SI HAY QUE METERLA LA AGREGAMOS, Y MATAMOS LA CONEXION
                                        {
                                            BlackListMACs newBLM = new BlackListMACs();
                                            newBLM.IdHotel = hotel.IdHotel;
                                            newBLM.MAC = mac;
                                            newBLM.BanningStart = DateTime.Now;
                                            newBLM.BanningEnd = DateTime.Now.AddSeconds(hotel.MACBannedInterval);

                                            BillingController.SaveBlackListMac(newBLM);

                                            if (isLog) log.FileWriteEntry(ELogType.Warning, string.Format("{0}|Fin del proceso: NUEVA MAC BLOQUEADA --> {1}", idproceso, mac));

                                            Response.End();
                                        }
                                    }
                                    else //SI ESTÁ EN LA LISTA NEGRA DE MAC, MATAMOS EL PROCESO
                                    {
                                        if (isLog) log.FileWriteEntry(ELogType.Warning, string.Format("{0}|Fin del proceso: MAC BLOQUEADA --> {1}", idproceso, mac));
                                        Response.End();
                                    }

                                    #endregion
                                }
                            }
                            else
                            {
                                if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|BLOQUEO POR MAC NO HABILITADO", idproceso));
                            }

                            UserAgents uA = BillingController.SalvarTransaccionado(userAgent);

                            if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|UserAgent actualizado en base de datos: {1}", idproceso, userAgent));

                            //DAMOS ENTRADA AL NUEVO USERAGENT Y LO GUARDAMOS EN LA BASE DE DATOS TEMPORAL PARA VER SI ES VÁLIDO.
                            if (!uA.IdUserAgent.Equals(0))
                            {
                                UserAgentsTemp temp = new UserAgentsTemp();
                                temp.IdUserAgent = uA.IdUserAgent;
                                temp.MAC = mac;
                                temp.AttemptTime = DateTime.Now;

                                BillingController.SaveUserAgentTemp(temp);

                                if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|UserAgentTemp insertado en base de datos: ID--> {1}  IDUSERAGENT --> {2}", idproceso, temp.IdTemp, temp.IdUserAgent));
                            }
                            else
                                if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Intento Insersión UserAgentTemp con IdUserAgent = 0: MAC--> {1}  IDUSERAGENT --> {2}", idproceso, mac, uA.String));

                        }
                        else //SI ESTÁ EN LA LISTA NEGRA DE USERAGENT, MATAMOS EL PROCESO
                        {
                            if (isLog) log.FileWriteEntry(ELogType.Warning, string.Format("{0}|Fin del proceso: USERAGENT BLOQUEADO --> {1}", idproceso, userAgent));
                            Response.End();
                        }

                        #endregion

                        #region OBTENER DE LA BASE DE DATOS, LOS PARAMETROS DEL HOTEL PARA TRABAJAR LUEGO CON ELLOS CON EL JAVASCRIPT

                        urlHotel = hotel.UrlHotel;
                        //FDSLocationsIdentifier locationIdentifier = BillingController.GetFDSLocationIdentifier(locationid);
                        //location =  BillingController.ObtainLocation(locationIdentifier.IdLocation);

                        int idlanguage = hotel.IdLanguage;

                        Languages languageDefault = BillingController.GetLanguage(idlanguage);

                        language = languageDefault.Code;
                        idhotel = hotel.IdHotel.ToString();
                        idlocation = location.IdLocation.ToString() ;

                        #endregion

                        if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Obtener datos site: IDHOTEL--> {1}  IDLOCATION --> {2}", idproceso, idhotel, idlocation));
                    }
                    else
                    {
                        #region SALVAMOS EL USERAGENT PARA PREVENIR ATAQUES
                        //SALVAMOS EL USERAGENT PARA PREVENIR ATAQUES
                        UserAgents uA = BillingController.SalvarTransaccionado(userAgent);

                        if (isLog) log.FileWriteEntry(ELogType.Warning, string.Format("{0}|Fin del proceso: USERAGENT ACTUALIZADO --> {1}", idproceso, userAgent));

                        Response.End();

                        #endregion
                    }

                    #region APPLEURL
                    //GUARDAMOS LA APPLE URL
                    if (!string.IsNullOrEmpty(urlDest))
                    {
                        AppleUrls appleUrl = new AppleUrls();
                        appleUrl.useragent = userAgent;
                        appleUrl.url = urlDest;
                        appleUrl.DateTime = DateTime.Now;
                        appleUrl.IdHotel = hotel.IdHotel;
                        appleUrl.IdLocation = location.IdLocation;
                        appleUrl.CallerID = mac;
                        BillingController.SaveAppleUrl(appleUrl);

                        if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Guardamos AppleURL.", idproceso));
                    }

                    #endregion
                }
                catch (Exception ex)
                {
                    if (isLog) log.FileWriteEntry(ELogType.Error, string.Format("{0}|{1}", idproceso, ex.Message));

                    Response.End();
                }
                finally
                {
                    if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Fin del Proceso PREINIT", idproceso));
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    if (File.Exists(Path.Combine(Server.MapPath(string.Format("~/resources/css/{0}/{0}.css", idhotel)))))
                    {
                        var link = new HtmlLink();

                        link.Href = string.Format("~/resources/css/{0}/{0}.css", idhotel);
                        link.Attributes.Add("rel", "stylesheet");
                        link.Attributes.Add("type", "text/css");
                        Page.Header.Controls.Add(link);
                    }
                    Page.Title = hotel.Name + " | Wifi Access provider by Future Broadband";
                    
                    isLog = Boolean.Parse(ConfigurationManager.AppSettings["isLog"].ToString());
                    if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Inicio del proceso LOAD", idproceso));

                    //ActiveSessions active = BillingController.GetActiveSession(mac, hotel.IdHotel);

                    //if ((active.IdHotel != 0) && (!Page.AppRelativeVirtualPath.Contains("re_logout")))
                    //{
                    //    Response.Redirect("re_logout.aspx");
                    //}

                    if (hotel.MacAuthenticate)
                    {
                        if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|INICIO DEL PROCESO MAC AUTHENTICATE", idproceso));
                        RememberMeTable remember = BillingController.GetRememberMeTableUser(mac, hotel.IdHotel);

                        if (!remember.IdRememberMe.Equals(0))
                        {
                            Users user = BillingController.GetRadiusUser(remember.UserName, hotel.IdHotel);
                            if (user.Enabled)
                            {
                                if (isLog)
                                {
                                    log.FileWriteEntry(ELogType.Inform, string.Format("{0}|USUARIO VALIDO PARA MAC {1} --> USUARIO:  {2}", idproceso, mac, user.Name));
                                    log.FileWriteEntry(ELogType.Inform, string.Format("{0}|SE LANZA PROCESO DE LOGIN", idproceso, mac, user.Name));
                                }

                                string urlDestino = string.Empty;

                                BillingTypes billing = BillingController.ObtainBillingType(user.IdBillingType);
                                if (string.IsNullOrEmpty(billing.UrlLanding))
                                {
                                    Locations2 location = BillingController.GetLocation(Int32.Parse(idlocation));
                                    if (string.IsNullOrEmpty(location.UrlLanding))
                                        urlDestino = hotel.UrlHotel;
                                    else
                                        urlDestino = location.UrlLanding;
                                }
                                else
                                    urlDestino = billing.UrlLanding;

                                int HourDayLightSaving = 0;

                                if (util.isDayLightSaving(DateTime.Now))
                                    HourDayLightSaving = 1;

                                SitesGMT siteGMT = BillingController.SiteGMTSitebyLocation(location.IdLocation);

                                username = user.Name;
                                password = user.Password;
                                urlDest = urlDestino;
                                validtill = user.ValidTill.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                                if (Context.Request.Params["error_message"] == null)
                                    cloudre = "true";

                            }
                            else
                            {
                                if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|SIN USUARIO VALIDO PARA MAC {1}", idproceso, mac));
                            }
                        }
                        else
                        {
                            if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|SIN ENTRADA EN REMEMBERMETBL MAC {1}", idproceso, mac));
                        }

                        if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|FIN DEL PROCESO MAC AUTHENTICATE", idproceso));
                    }
                    else
                    {
                        if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|PROCESO MAC AUTENTICATE DESHABILITADO", idproceso));
                    }

                    string cheerfy = Request.Form["cheerfy"];

                    string usernameCheerfy = string.Empty;
                    if (HttpContext.Current.Request["username"] != null)
                        usernameCheerfy = HttpContext.Current.Request["username"].ToString();

                    if (!string.IsNullOrEmpty(cheerfy) && string.IsNullOrEmpty(usernameCheerfy))
                    {

                        Boolean isCheerfy = Boolean.Parse(cheerfy);
                        if (isCheerfy)
                        {
                            string cheerfy_ap_id = Request.Form["cheerfy_ap_id"];
                            string redirect_uri = string.Format("http://{0}/cheerfy.aspx", HttpContext.Current.Request.Url.Host);
                            string macCheerfy = mac.Replace('-', ':');
                            string uri = string.Format("https://portal.cheerfy.com/?user_mac={0}&ap_id={1}&user_url={2}&redirect_uri={3}", macCheerfy, cheerfy_ap_id, System.Web.HttpUtility.UrlEncode(urlDest), System.Web.HttpUtility.UrlEncode(redirect_uri));
                            Response.Redirect(uri);
                        }
                    }

                }
                catch (Exception ex)
                {
                    if (isLog) log.FileWriteEntry(ELogType.Error, string.Format("{0}|{1}", idproceso, ex.Message));
                    Response.End();
                }
                finally
                {
                    if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Fin del Proceso LOAD", idproceso));
                }
            }
        }

        /// <summary>
        /// Los siguientes métodos asignan la urlbase de localización en disco de los recursos de imagenes, scripts y css.
        /// </summary>
        public virtual string PathToCss
        {
            get
            {
                if (CacheHelper.Current.GetItem("PathToCss") == null)
                {
                    string absoluteWebRoot = UrlHelper.AbsoluteWebRoot.ToString();
                    CacheHelper.Current.AddItem("PathToCss", UrlHelper.Combine(absoluteWebRoot, @"resources/css"));

                }
                return CacheHelper.Current.GetItem("PathToCss").ToString();
            }
        }

        public virtual string PathToScripts
        {
            get
            {
                if (CacheHelper.Current.GetItem("PathToScripts") == null)
                {
                    string absoluteWebRoot = UrlHelper.AbsoluteWebRoot.ToString();
                    CacheHelper.Current.AddItem("PathToScripts", UrlHelper.Combine(absoluteWebRoot, @"resources/scripts"));

                }
                return CacheHelper.Current.GetItem("PathToScripts").ToString();
            }
        }

        public virtual string PathToImages
        {
            get
            {
                if (CacheHelper.Current.GetItem("PathToImages") == null)
                {
                    string absoluteWebRoot = UrlHelper.AbsoluteWebRoot.ToString();
                    CacheHelper.Current.AddItem("PathToImages", UrlHelper.Combine(absoluteWebRoot, @"resources/images"));

                }
                return CacheHelper.Current.GetItem("PathToImages").ToString();
            }
        }     
    }
}