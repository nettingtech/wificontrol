﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Wifi360.Data.Class;
using Wifi360.Data.Controllers;
using Wifi360.Data.Model;
using System.Security.Cryptography;
using System.Configuration;
using System.IO;
using Portal.resources.utils;
using System.Web.UI.HtmlControls;

namespace Portal_Meraki.resources
{
    public partial class MerakiSecureMaster : System.Web.UI.MasterPage
    {
        public string language = string.Empty;
        public string idhotel = string.Empty;
        public string idhotel_token = string.Empty;
        public string idlocation = string.Empty;
        public string mac = string.Empty;
        public string origin = string.Empty;
        public string error = string.Empty;
        public string urlHotel = string.Empty;
        public string urlDest = string.Empty;
        public string username = string.Empty;
        public string password = string.Empty;
        public string cloudre = string.Empty;
        public string validtill = string.Empty;
        public string ap_mac = string.Empty;
        public string ap_ssid = string.Empty;
        private Hotels hotel = null;
        private Locations2 location = null;
        protected Guid idproceso = Guid.Empty;
        private static DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        private static LogDaily log = null;
        private bool isLog = false;


        /// <summary>
        /// En el método preInit se realizan las comprobaciones para saber si el acceso al portal es autorizada o no.
        /// Obtenemos todos los parametros que necesitamos para identificar el site y la location
        /// Comprobamos y guardamos el USERAGENT
        /// En caso de no ser autorizado o el usergent estar bloqueado, se finaliza la ejecución
        /// Esta Master es para equipos de MIKROTIK
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                try
                {
                    var ap_device_macs = string.Empty;
                    var ap_origin = string.Empty;
                    var ap_urlDest = string.Empty;
                    var ap_error = string.Empty;

                    if (Session["nt_crc"] != null)
                    {
                        #region DECRYPT TOKEN
                        string nt_token = Session["nt_crc"].ToString();
                        List<ParametrosConfiguracion> parametros = BillingController.GetParametrosConfiguracion(0);
                        ParametrosConfiguracion key = (from t in parametros where t.Key.ToUpper().Equals("ENCRYPTIONKEY") select t).FirstOrDefault();
                        string[] nt_token_decrypt = Misc.Decrypt(nt_token, key.value).Split('|');

                        string[] nt_token_decrypt_A = nt_token_decrypt[0].Split('=');
                        string[] nt_token_decrypt_B = nt_token_decrypt[1].Split('=');
                        string[] nt_token_decrypt_C = nt_token_decrypt[2].Split('=');
                        string[] nt_token_decrypt_D = nt_token_decrypt[3].Split('=');
                        string[] nt_token_decrypt_E = nt_token_decrypt[4].Split('=');
                        string[] nt_token_decrypt_F = nt_token_decrypt[5].Split('=');
                        string[] nt_token_decrypt_G = nt_token_decrypt[6].Split('=');
                        string[] nt_token_decrypt_H = nt_token_decrypt[7].Split('=');
                        string[] nt_token_decrypt_i = nt_token_decrypt[8].Split('=');

                        if (nt_token_decrypt_A[0].Equals("nt_st") && nt_token_decrypt_B[0].Equals("nt_lt") && nt_token_decrypt_C[0].Equals("nt_client_mac") &&
                            nt_token_decrypt_D[0].Equals("nt_origin") && nt_token_decrypt_E[0].Equals("nt_urlDest") && nt_token_decrypt_F[0].Equals("nt_error") &&
                            nt_token_decrypt_G[0].Equals("nt_ap_mac") && nt_token_decrypt_H[0].Equals("nt_app_ssid") && nt_token_decrypt_i[0].Equals("epoch"))
                        {
                            DateTime Now_Datetime = DateTime.Now.ToUniversalTime();
                            DateTime crc_Date = DateTime.Parse(nt_token_decrypt_i[1]);
                            long epoch = Misc.DateTimeToUnixTime(Now_Datetime) - Misc.DateTimeToUnixTime(crc_Date);
                            if (epoch > 4000)
                            {
                                Response.Redirect("error.aspx");
                            }

                            idhotel_token = nt_token_decrypt_A[1];

                            Locations2 nt_location = new Locations2();
                            nt_location = BillingController.GetLocation(Int32.Parse(nt_token_decrypt_B[1]));
                            if (nt_location.IdHotel != Int32.Parse(idhotel_token))
                            {
                                Response.Redirect("error.aspx");
                            }

                            ap_device_macs = nt_token_decrypt_C[1];
                            ap_origin = nt_token_decrypt_D[1];
                            ap_urlDest = nt_token_decrypt_E[1];
                            ap_error = nt_token_decrypt_F[1];
                            ap_mac = nt_token_decrypt_G[1];
                            ap_ssid = nt_token_decrypt_H[1];


                            isLog = Boolean.Parse(ConfigurationManager.AppSettings["isLog"].ToString());
                            idproceso = Guid.NewGuid();

                            if (isLog)
                            {
                                log = new LogDaily("Meraki-Captive", HttpContext.Current.Server.MapPath("/log"));
                                log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Página: {1} ", idproceso, Page.Page));
                                log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Inicio del proceso", idproceso));
                            }

                            #region PARAMETRO DE ENTRADA
                            //PARAMETRO POR EL METODO POST
                            string hotelid = nt_location.IdHotel.ToString();
                            string locationid = nt_location.IdLocation.ToString();
                            string macs = ap_device_macs;
                            origin = ap_origin;
                            urlDest = ap_urlDest;
                            error = ap_error;


                            if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Variable de session hotelid: {1}", idproceso, nt_location.IdHotel.ToString()));

                            if (isLog)
                            {
                                log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Lectura parámetros método POST.", idproceso));
                                log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Parámetros: hotelid --> {1} ; locationid = {2} ; macs = {3} ; origin = {4} ; error = {5} ; SSID = {6} ; urlDest = {7}.", idproceso, hotelid, locationid, macs, origin, error, ap_ssid, urlDest));
                            }

                            if (string.IsNullOrEmpty(hotelid)) //EN CASO DE NO TENER POST PROBAMOS COOKIE
                            {
                                if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Lectura parámetros método POST sin resultados.", idproceso));

                                if (isLog)
                                {
                                    log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Lectura parámetros de COOKIE.", idproceso));
                                }
                                if (HttpContext.Current.Request.Cookies["meraki"] != null)
                                {
                                    HttpCookie cookie = HttpContext.Current.Request.Cookies["meraki"];
                                    if (cookie != null)
                                    {
                                        if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}| COOKIE VALUE : {1}", idproceso, cookie.Value));
                                        hotelid = cookie.Values["idhotel"].ToString();
                                        locationid = cookie.Values["idlocation"].ToString();
                                        macs = cookie.Values["mac"].ToString();
                                        origin = cookie.Values["origin"].ToString();
                                    }
                                    else
                                        if (isLog) log.FileWriteEntry(ELogType.Error, string.Format("{0}| COOKIE NO EXISTE O VACIA", idproceso));

                                    if (isLog)
                                    {

                                        log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Parámetros: hotelid --> {1} ; locationid = {2} ; macs = {3} ; origin = {4} ; error = {5}.", idproceso, hotelid, locationid, macs, origin, error));
                                        if (!string.IsNullOrEmpty(hotelid))
                                            log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Lectura parámetros de COOKIE COMPLETADA.", idproceso));
                                    }
                                }
                                else
                                {
                                    if (isLog) log.FileWriteEntry(ELogType.Warning, string.Format("{0}|Fin del proceso: NO HAY PARÁMETROS QUE LEER", idproceso));
                                }
                            }
                            else
                            {
                                if (isLog)
                                {
                                    log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Lectura parámetros método POST COMPLETADA.", idproceso));
                                }
                            }
                            #endregion

                            #region COMPROBAMOS USERAGENT Y MAC

                            string userAgent = Request.UserAgent;

                            if (!string.IsNullOrEmpty(macs))
                            {
                                string[] macpre = macs.Split(',');

                                if (macpre.Length >= 1)
                                {
                                    mac = macpre[0].Replace(':', '-').ToUpper();
                                }
                                else
                                    mac = macs;
                            }

                            if (!string.IsNullOrEmpty(hotelid)) //COMPROBAMOS QUE AUNQUE VENGA HOTEL NO VENGA VACIO
                            {
                                if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|PROCESANDO MAC --> {1}", idproceso, mac));
                                //COMPROBAMOS SI EL USERAGENT ESTÁ EN LA LISTA NEGRA
                                BlackListUserAgents blackUA = BillingController.GetBlackUserAgent(userAgent);

                                if (blackUA.IdBlackUserAgent.Equals(0)) //SI NO ESTÁ EN LA LISTA NEGRA, VAMOS A COMPROBAR LA LISTA NEGRA DE LAS MACS
                                {
                                    if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|USERAGENT PERMITIDO --> {1}", idproceso, userAgent));
                                    hotel = BillingController.GetHotel(Int32.Parse(hotelid));
                                    location = BillingController.GetLocation(Int32.Parse(locationid));

                                    if (hotel.MACBlocking) //SI TIENE ACTIVADO EL BLOQUEO POR MACS
                                    {
                                        if (!string.IsNullOrEmpty(mac))
                                        {
                                            #region MACBLOCKING
                                            //COMPROBAMOS SI LA MAC ESTÁ BLOQUEADA
                                            BlackListMACs blackMAC = BillingController.GetBlackListMac(mac, DateTime.Now);
                                            if (blackMAC.IdMACBlackList.Equals(0)) //SI LA MAC NO ESTÁ BLOQUEADA
                                            {
                                                if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|MAC PERMITIDA --> {1}", idproceso, mac));
                                                //COMPROBAMOS SI HAY QUE METER LA MAC EN LA LISTA NEGRA
                                                if (hotel.MaxMACAttemps <= BillingController.GetUserAgentsTemps(mac, hotel.MACAttempsInterval)) //SI HAY QUE METERLA LA AGREGAMOS, Y MATAMOS LA CONEXION
                                                {
                                                    BlackListMACs newBLM = new BlackListMACs();
                                                    newBLM.IdHotel = hotel.IdHotel;
                                                    newBLM.MAC = mac;
                                                    newBLM.BanningStart = DateTime.Now;
                                                    newBLM.BanningEnd = DateTime.Now.AddSeconds(hotel.MACBannedInterval);

                                                    BillingController.SaveBlackListMac(newBLM);
                                                    if (isLog) log.FileWriteEntry(ELogType.Warning, string.Format("{0}|Fin del proceso: NUEVA MAC BLOQUEADA --> {1}", idproceso, mac));
                                                    Response.End();
                                                }
                                            }
                                            else //SI ESTÁ EN LA LISTA NEGRA DE MAC, MATAMOS EL PROCESO
                                            {
                                                if (isLog) log.FileWriteEntry(ELogType.Warning, string.Format("{0}|Fin del proceso: MAC BLOQUEADA --> {1}", idproceso, mac));
                                                Response.End();
                                            }
                                            #endregion
                                        }
                                    }
                                    else
                                    {
                                        if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|BLOQUEO POR MAC NO HABILITADO", idproceso));
                                    }

                                    UserAgents uA = BillingController.SalvarTransaccionado(userAgent);
                                    if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|UserAgent actualizado en base de datos: {1}", idproceso, userAgent));

                                    //DAMOS ENTRADA AL NUEVO USERAGENT Y LO GUARDAMOS EN LA BASE DE DATOS TEMPORAL PARA VER SI ES VÁLIDO.
                                    if (!uA.IdUserAgent.Equals(0))
                                    {
                                        UserAgentsTemp temp = new UserAgentsTemp();
                                        temp.IdUserAgent = uA.IdUserAgent;
                                        temp.MAC = mac;
                                        temp.AttemptTime = DateTime.Now;

                                        BillingController.SaveUserAgentTemp(temp);

                                        if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|UserAgentTemp insertado en base de datos: ID--> {1}  IDUSERAGENT --> {2}", idproceso, temp.IdTemp, temp.IdUserAgent));
                                    }
                                    else
                                        if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Intento Insersión UserAgentTemp con IdUserAgent = 0: MAC--> {1}  IDUSERAGENT --> {2}", idproceso, mac, uA.String));

                                }
                                else //SI ESTÁ EN LA LISTA NEGRA DE USERAGENT, MATAMOS EL PROCESO
                                {
                                    if (isLog) log.FileWriteEntry(ELogType.Warning, string.Format("{0}|Fin del proceso: USERAGENT BLOQUEADO --> {1}", idproceso, userAgent));
                                    Response.End();
                                }

                                #endregion

                                #region OBTENER DE LA BASE DE DATOS, LOS PARAMETROS DEL HOTEL PARA TRABAJAR LUEGO CON ELLOS CON EL JAVASCRIPT

                                urlHotel = hotel.UrlHotel;

                                int idlanguage = hotel.IdLanguage;

                                Languages languageDefault = BillingController.GetLanguage(idlanguage);

                                language = languageDefault.Code;
                                idhotel = hotel.IdHotel.ToString();
                                idlocation = location.IdLocation.ToString();

                                #endregion

                                if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Obtener datos site: IDHOTEL--> {1}  IDLOCATION --> {2}", idproceso, idhotel, idlocation));
                            }
                            else
                            {
                                #region SALVAMOS EL USERAGENT PARA PREVENIR ATAQUES
                                //SALVAMOS EL USERAGENT PARA PREVENIR ATAQUES
                                UserAgents uA = BillingController.SalvarTransaccionado(userAgent);

                                if (isLog) log.FileWriteEntry(ELogType.Warning, string.Format("{0}|Fin del proceso: USERAGENT ACTUALIZADO --> {1}", idproceso, userAgent));

                                Response.End();

                                #endregion
                            }

                            #region APPLEURL
                            //GUARDAMOS LA APPLE URL
                            if (!string.IsNullOrEmpty(urlDest))
                            {
                                AppleUrls appleUrl = new AppleUrls();
                                appleUrl.useragent = userAgent;
                                appleUrl.url = urlDest;
                                appleUrl.DateTime = DateTime.Now;
                                appleUrl.IdHotel = hotel.IdHotel;
                                appleUrl.IdLocation = location.IdLocation;
                                appleUrl.CallerID = mac;
                                BillingController.SaveAppleUrl(appleUrl);

                                if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Guardamos AppleURL.", idproceso));
                            }

                            #endregion
                        }
                        else
                            Response.Redirect("error.aspx");
                        #endregion
                    }
                    else
                    {
                        if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|No trae CRC PRE_INIT", idproceso));
                        Response.Redirect("error.aspx");
                        Response.End();
                    }
                }
                catch (Exception ex)
                {
                    if (isLog) log.FileWriteEntry(ELogType.Error, string.Format("{0}|{1}", idproceso, ex.Message));
                    Response.Redirect("error.aspx");
                    Response.End();
                }
                //finally
                //{
                //    if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Datos CRC PRE_INIT OK", idproceso));
                //}
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                
                    if (File.Exists(Path.Combine(Server.MapPath(string.Format("~/resources/css/{0}/{0}.css", idhotel)))))
                    {
                        var link = new HtmlLink();

                        link.Href = string.Format("~/resources/css/{0}/{0}.css", idhotel);
                        link.Attributes.Add("rel", "stylesheet");
                        link.Attributes.Add("type", "text/css");
                        Page.Header.Controls.Add(link);
                    }
                    Page.Title = hotel.Name + " | Wifi Access provider by Future Broadband";

                    isLog = Boolean.Parse(ConfigurationManager.AppSettings["isLog"].ToString());
                    if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Inicio del proceso LOAD", idproceso));


                    if (hotel.MacAuthenticate)
                    {
                        if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|INICIO DEL PROCESO MAC AUTHENTICATE", idproceso));
                        RememberMeTable remember = BillingController.GetRememberMeTableUser(mac, hotel.IdHotel);

                        if (!remember.IdRememberMe.Equals(0))
                        {
                            //REVISAR ESTA CONDICION PORQUE ENTRA SI HAY USUARIO VALIDO EN EL IDHOTEL QUE PASAMOS POR TOKEN.
                            Users user = BillingController.GetRadiusUser(remember.UserName, hotel.IdHotel);
                            if (user.Enabled)
                            {
                                int User_BTM = BillingController.ObtainBillingType(user.IdBillingType).IdBillingModule;
                                if (User_BTM == 2 || User_BTM == 4 || User_BTM == 5 || User_BTM == 6)
                                {

                                    ActiveSessions Real_session = new ActiveSessions();
                                    try
                                    {
                                        Real_session = BillingController.GetActiveSessionMAC(mac);
                                        if (!Real_session.IdHotel.Equals(idhotel_token.ToString()))
                                        {
                                            //POCA BROMA SARDINAS CUCA!
                                            CaptivePortalLogAttack new_attack = new CaptivePortalLogAttack();
                                            new_attack.idLog = 0;
                                            new_attack.device_mac = mac;
                                            new_attack.ip = Context.Request.UserHostAddress;
                                            new_attack.info = "Se intenta acceder al SITE: " + idhotel_token.ToString() + " pero en el ACTIVESSESION está en SITE: " + Real_session.IdHotel.ToString();
                                            new_attack.nas_mac_correcta = Real_session.NAS_MAC;
                                            new_attack.username = remember.UserName;
                                            new_attack.nas_mac_origen = "No registrado"; 
                                            new_attack.portal = "MERAKI";
                                            new_attack.date = DateTime.Now.ToUniversalTime();

                                            BillingController.SaveCaptivePortalLogAttack(new_attack);
                                            //Response.Redirect("~/error.aspx", false);
                                            //Response.End();
                                        }
                                    }
                                    catch
                                    {
                                        //Response.Redirect("~/error.aspx", false);
                                        //Response.End();
                                        ;
                                    }
                                }

                                if (isLog)
                                {
                                    log.FileWriteEntry(ELogType.Inform, string.Format("{0}|USUARIO VALIDO PARA MAC {1} --> USUARIO:  {2}", idproceso, mac, user.Name));
                                    log.FileWriteEntry(ELogType.Inform, string.Format("{0}|SE LANZA PROCESO DE LOGIN", idproceso, mac, user.Name));
                                }

                                string urlDestino = string.Empty;

                                BillingTypes billing = BillingController.ObtainBillingType(user.IdBillingType);
                                if (string.IsNullOrEmpty(billing.UrlLanding))
                                {
                                    Locations2 location = BillingController.GetLocation(Int32.Parse(idlocation));
                                    if (string.IsNullOrEmpty(location.UrlLanding))
                                        urlDestino = hotel.UrlHotel;
                                    else
                                        urlDestino = location.UrlLanding;
                                }
                                else
                                    urlDestino = billing.UrlLanding;

                                SitesGMT siteGMT = BillingController.SiteGMTSitebyLocation(location.IdLocation);

                                username = user.Name;
                                password = user.Password;
                                urlDest = urlDestino;
                                validtill = user.ValidTill.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                                if (Context.Request.Params["error_message"] == null)
                                    cloudre = "true";

                            }
                            else
                            {
                                if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|SIN USUARIO VALIDO PARA MAC {1}", idproceso, mac));
                            }
                        }
                        else
                        {
                            if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|SIN ENTRADA EN REMEMBERMETBL MAC {1}", idproceso, mac));
                        }

                        if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|FIN DEL PROCESO MAC AUTHENTICATE", idproceso));
                    }
                    else
                    {
                        if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|PROCESO MAC AUTENTICATE DESHABILITADO", idproceso));
                    }

                    string cheerfy = Request.Form["cheerfy"];

                    string usernameCheerfy = string.Empty;
                    if (HttpContext.Current.Request["username"] != null)
                        usernameCheerfy = HttpContext.Current.Request["username"].ToString();

                    if (!string.IsNullOrEmpty(cheerfy) && string.IsNullOrEmpty(usernameCheerfy))
                    {

                        Boolean isCheerfy = Boolean.Parse(cheerfy);
                        if (isCheerfy)
                        {
                            string cheerfy_ap_id = Request.Form["cheerfy_ap_id"];
                            string redirect_uri = string.Format("http://{0}/cheerfy.aspx", HttpContext.Current.Request.Url.Host);
                            string macCheerfy = mac.Replace('-', ':');
                            string uri = string.Format("https://portal.cheerfy.com/?user_mac={0}&ap_id={1}&user_url={2}&redirect_uri={3}", macCheerfy, cheerfy_ap_id, System.Web.HttpUtility.UrlEncode(urlDest), System.Web.HttpUtility.UrlEncode(redirect_uri));
                            Response.Redirect(uri);
                        }
                    }

                }                   
                catch (Exception ex)
                {
                    if (isLog) log.FileWriteEntry(ELogType.Error, string.Format("{0}|{1}", idproceso, ex.Message));
                    Response.End();
                }
                finally
                {
                    if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Fin del Proceso LOAD", idproceso));
                }
            }
        }

        /// <summary>
        /// Los siguientes métodos asignan la urlbase de localización en disco de los recursos de imagenes, scripts y css.
        /// </summary>
        public virtual string PathToCss
        {
            get
            {
                if (CacheHelper.Current.GetItem("PathToCss") == null)
                {
                    string absoluteWebRoot = UrlHelper.AbsoluteWebRoot.ToString();
                    CacheHelper.Current.AddItem("PathToCss", UrlHelper.Combine(absoluteWebRoot, @"resources/css"));

                }
                return CacheHelper.Current.GetItem("PathToCss").ToString();
            }
        }

        public virtual string PathToScripts
        {
            get
            {
                if (CacheHelper.Current.GetItem("PathToScripts") == null)
                {
                    string absoluteWebRoot = UrlHelper.AbsoluteWebRoot.ToString();
                    CacheHelper.Current.AddItem("PathToScripts", UrlHelper.Combine(absoluteWebRoot, @"resources/scripts"));

                }
                return CacheHelper.Current.GetItem("PathToScripts").ToString();
            }
        }

        public virtual string PathToImages
        {
            get
            {
                if (CacheHelper.Current.GetItem("PathToImages") == null)
                {
                    string absoluteWebRoot = UrlHelper.AbsoluteWebRoot.ToString();
                    CacheHelper.Current.AddItem("PathToImages", UrlHelper.Combine(absoluteWebRoot, @"resources/images"));

                }
                return CacheHelper.Current.GetItem("PathToImages").ToString();
            }
        }
    }
}