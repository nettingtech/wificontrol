﻿using Portal.resources.response;
using Portal.resources.utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using Wifi360.Data.Controllers;
using Wifi360.Data.Model;

namespace Portal.resources.handlers
{
    /// <summary>
    /// Descripción breve de MerakiHandler
    /// </summary>
    public class MerakiHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            if (string.IsNullOrEmpty(context.Request.Params["action"])) ProcessEmtpy(context);
            var jss = new JavaScriptSerializer();
            string json = new StreamReader(context.Request.InputStream).ReadToEnd();
            Dictionary<string, string> sData = jss.Deserialize<Dictionary<string, string>>(json);
            if (string.IsNullOrEmpty(json))
            {
                switch (context.Request.Params["action"].ToUpper())
                {
                    case "ALLOW": allowfree(context); break;
                    case "ALLOWPAY": allowpay(context); break;
                    case "FULLSITE": fullsite(context); break;
                    case "FREEACCESS": freeaccess(context); break;
                    case "FREEACCESSNOSURVEY": freeaccessnosurvey(context); break;
                    case "INSTAGRAMACCESS": instagramaccess(context); break;
                    case "LOGIN": login(context); break;
                    case "PAYPALREQUESTNOSURVEY": paypalrequestnosurvey(context); break;
                    case "SOCIALNETWORK": socialnetwork(context); break;
                    case "USERNAME": username(context); break;
                    case "VOUCHER": voucher(context); break;
                    case "AVISOLEGAL": avisolegal(context); break;
                    case "DISCLAIMER": disclaimer(context); break;
                    case "GETUSER": getuser(context); break;
                    case "DELETEREMEMBER": deleteremember(context); break;
                        

                    default: ProcessEmtpy(context); break;
                }
            }
            else
            {
                switch (sData["action"].ToUpper())
                {
                    case "SAVELOGOUTURL": saveLogoutUrl(context, sData); break;
                }
            }

        }

        private UserResponse allowfree(HttpContext context)
        {
            CaptivePortalLog log = new CaptivePortalLog();
            UserResponse response = new UserResponse();
            try
            {
                string idhotel = (context.Request["idhotel"] == null ? string.Empty : context.Request["idhotel"].ToString());
                string mac = (context.Request["mac"] == null ? string.Empty : context.Request["mac"].ToString());
                string idlocation = (context.Request["idlocation"] == null ? string.Empty : context.Request["idlocation"].ToString());
                string mailing = (context.Request["mailing"] == null ? string.Empty : context.Request["mailing"].ToString());
                string lang = (context.Request["lang"] == null ? string.Empty : context.Request["lang"].ToString());
                string origin = (context.Request["origin"] == null ? string.Empty : context.Request["origin"].ToString());

                Hotels hotel = BillingController.GetHotel(Int32.Parse(idhotel));
                Locations2 location = BillingController.ObtainLocation(Int32.Parse(idlocation));

                log.IdHotel = hotel.IdHotel;
                log.CallerID = mac;
                log.Date = DateTime.Now.ToUniversalTime();
                log.UserName = "";
                log.Password = "";
                log.Page = "freeaccess reconnect";
                log.IdLocation = Int32.Parse(idlocation);
                log.IdHotel = hotel.IdHotel;
                log.NSEId = "Meraki - IdHotel: " + hotel.IdHotel;
                log.RadiusResponse = "NOT APPLICABLE";
                log.NomadixResponse = "NOT APPLICABLE";

                Users lastUser = BillingController.GetRadiusUsermac(mac, hotel.IdHotel);
                if (!lastUser.IdUser.Equals(0))
                {
                    BillingTypes bt = BillingController.ObtainBillingType(lastUser.IdBillingType);
                    log.IdLocation = bt.IdLocation;
                        if ((lastUser.Enabled))
                        {
                            response.allow = 1; //PASA DIRECTAMENTE
                            response.IdUser = 1;
                            response.UserName = lastUser.Name;
                            response.Password = lastUser.Password;
                            log.UserName = lastUser.Name.ToUpper();
                            log.Password = lastUser.Password.ToUpper();
                            response.ValidTill = lastUser.ValidTill.AddHours(hotel.GMT).ToString("dd/MM/yyyy HH:mm:ss");
                            if (string.IsNullOrEmpty(bt.UrlLanding))
                            {
                                if (string.IsNullOrEmpty(location.UrlLanding))
                                    response.Url = hotel.UrlHotel;
                                else
                                    response.Url =location.UrlLanding;
                            }
                            else
                            {
                                response.Url = bt.UrlLanding;
                            }
                            //DEMO
                            //response.Url = location.UrlLanding;
                        }
                        else if (lastUser.ValidSince.AddHours(hotel.FreeAccessRepeatTime) > DateTime.Now.ToUniversalTime())
                        {
                            response.allow = 2; //SI RESPONSE ALLOW = 2, NO PUEDE PASAR

                            log.Page = "No more free access allowed";
                            log.RadiusResponse = "No more free access allowed";
                            log.NomadixResponse = "NOT APPLICABLE";
                            log.UserName = lastUser.Name.ToUpper();
                            log.Password = lastUser.Password.ToUpper();
                            log.NSEId = "Meraki - IdHotel: " + hotel.IdHotel;
                            log.CallerID = mac;
                            log.Date = DateTime.Now.ToUniversalTime();
                        }
                        else
                        {
                            response.allow = 0; //SI RESPONSE ALLOW = 0, HAY USUARIO ANTERIOR PERO ESTA FUERA DEL PLAZO O NO ES GRATUITO
                        }
                }
                else
                {
                    response.allow = 0; //ALLOW = 0, NO HAY UN USUARIO ANTERIOR
                }
            }
            catch
            {

            }

            BillingController.SaveCaptivePortalLog(log);

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private UserResponse allowpay(HttpContext context)
        {
            CaptivePortalLog log = new CaptivePortalLog();
            UserResponse response = new UserResponse();
            try
            {
                string idhotel = (context.Request["idhotel"] == null ? string.Empty : context.Request["idhotel"].ToString());
                string mac = (context.Request["mac"] == null ? string.Empty : context.Request["mac"].ToString());
                string idlocation = (context.Request["idlocation"] == null ? string.Empty : context.Request["idlocation"].ToString());
                string mailing = (context.Request["mailing"] == null ? string.Empty : context.Request["mailing"].ToString());
                string lang = (context.Request["lang"] == null ? string.Empty : context.Request["lang"].ToString());
                string origin = (context.Request["origin"] == null ? string.Empty : context.Request["origin"].ToString());

                Hotels hotel = BillingController.GetHotel(Int32.Parse(idhotel));
                Locations2 location = BillingController.ObtainLocation(Int32.Parse(idlocation));

                log.IdHotel = hotel.IdHotel;
                log.CallerID = mac;
                log.Date = DateTime.Now.ToUniversalTime();
                log.UserName = "";
                log.Password = "";
                log.Page = "payaccess reconnect";
                log.IdLocation = Int32.Parse(idlocation);
                log.IdHotel = hotel.IdHotel;
                log.NSEId = "Meraki - IdHotel: " + hotel.IdHotel;
                log.RadiusResponse = "NOT APPLICABLE";
                log.NomadixResponse = "NOT APPLICABLE";

                Users lastUser = BillingController.GetRadiusUsermac(mac, hotel.IdHotel, 2);
                if (!lastUser.IdUser.Equals(0))
                {
                    BillingTypes bt = BillingController.ObtainBillingType(lastUser.IdBillingType);
                    log.IdLocation = bt.IdLocation;
                        if ((lastUser.Enabled))
                        {
                            response.allow = 1; //PASA DIRECTAMENTE
                            response.IdUser = 1;
                            response.UserName = lastUser.Name;
                            response.Password = lastUser.Password;
                            log.UserName = lastUser.Name.ToUpper();
                            log.Password = lastUser.Password.ToUpper();
                            response.ValidTill = lastUser.ValidTill.AddHours(hotel.GMT).ToString("dd/MM/yyyy HH:mm:ss");
                            if (string.IsNullOrEmpty(bt.UrlLanding))
                            {
                                if (string.IsNullOrEmpty(location.UrlLanding))
                                    response.Url = hotel.UrlHotel;
                                else
                                    response.Url = location.UrlLanding;
                            }
                            else
                            {
                                response.Url = bt.UrlLanding;
                            }
                            //DEMO
                            //response.Url = location.UrlLanding;
                        }
                        else
                        {
                            response.allow = 0; //SI RESPONSE ALLOW = 0, HAY USUARIO ANTERIOR PERO ESTA FUERA DEL PLAZO O NO ES GRATUITO
                        }
                }
                else
                {
                    response.allow = 0; //ALLOW = 0, NO HAY UN USUARIO ANTERIOR
                }
            }
            catch
            {

            }

            BillingController.SaveCaptivePortalLog(log);

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private UserResponse freeaccess(HttpContext context)
        {
            CaptivePortalLog log = new CaptivePortalLog();
            UserResponse response = new UserResponse();
            try
            {
                string name = (context.Request["name"] == null ? string.Empty : context.Request["name"].ToString());
                string surname = (context.Request["surname"] == null ? string.Empty : context.Request["surname"].ToString());
         //       string mail = (context.Request["email"] == null ? string.Empty : context.Request["email"].ToString());
                string username = (context.Request["username"] == null ? string.Empty : context.Request["username"].ToString());
                string password = (context.Request["password"] == null ? string.Empty : context.Request["password"].ToString());
                string survey = (context.Request["survey"] == null ? string.Empty : context.Request["survey"].ToString());

                string idhotel = (context.Request["idhotel"] == null ? string.Empty : context.Request["idhotel"].ToString());
                string mac = (context.Request["mac"] == null ? string.Empty : context.Request["mac"].ToString());
                string idlocation = (context.Request["idlocation"] == null ? string.Empty : context.Request["idlocation"].ToString());
                string mailing = (context.Request["mailing"] == null ? string.Empty : context.Request["mailing"].ToString());
                string lang = (context.Request["lang"] == null ? string.Empty : context.Request["lang"].ToString());
                string origin = (context.Request["origin"] == null ? string.Empty : context.Request["origin"].ToString());
                string idbillingtype = (context.Request["idbilling"] == null ? string.Empty : context.Request["idbilling"].ToString());

                Hotels hotel = BillingController.GetHotel(Int32.Parse(idhotel));
                Locations2 location = BillingController.ObtainLocation(Int32.Parse(idlocation));

                SitesGMT siteGMT = BillingController.SiteGMTSitebyLocation(location.IdLocation);
                if (siteGMT.Id.Equals(0))
                    siteGMT = BillingController.SiteGMTSitebySite(location.IdHotel);

                log.NSEId = string.Format("Meraki - Idhotel:{0}", idhotel);
                log.CallerID = mac;
                log.Date = DateTime.Now.ToUniversalTime();
                log.UserName = "";
                log.Password = "";
                log.Page = "freeaccess";
                log.IdLocation = Int32.Parse(idlocation);
                log.IdHotel = hotel.IdHotel;
                log.RadiusResponse = "NOT APPLICABLE";
                log.NomadixResponse = "NOT APPLICABLE";

                BillingTypes billingType = BillingController.ObtainBillingType(Int32.Parse(idbillingtype));

                //if (util.ValidateEmail(mail))
                //{
                    bool allow = true;
                    Users lastUser = BillingController.GetRadiusUsermac(mac, hotel.IdHotel);
                    if (!lastUser.IdUser.Equals(0))
                    {
                        BillingTypes bt = BillingController.ObtainBillingType(lastUser.IdBillingType);
                        if ((bt.IdBillingModule.Equals(1) || bt.IdBillingModule.Equals(3)) && lastUser.ValidSince > DateTime.Now.ToUniversalTime().AddHours(-hotel.FreeAccessRepeatTime))
                            allow = false;
                    }

                    if (allow)
                    {
                        List<ParametrosConfiguracion> parametros = BillingController.GetParametrosConfiguracion(hotel.IdHotel);
                        int longUserName = 3;
                        int longPassword = 3;
                        string prefixUserName = string.Empty;

                        foreach (ParametrosConfiguracion obj in parametros)
                        {
                            switch (obj.Key.ToUpper())
                            {
                                case "LONGUSERNAME": longUserName = Int32.Parse(obj.value); break;
                                case "LONGPASSWORD": longPassword = Int32.Parse(obj.value); break;
                                case "USERNAMEPREFIX": prefixUserName = obj.value; break;
                            }
                        }

                        if (longPassword < 3)
                            longPassword = 3;
                        if (longUserName < 3)
                            longUserName = 3;

                        Users existuser = null;
                        username = string.Empty;
                        do
                        {
                            username = string.Format("{0}{1}", prefixUserName, util.createRandomUserNumber(longUserName)).ToUpper();
                            existuser = BillingController.GetRadiusUser(username.ToUpper(), hotel.IdHotel);

                        } while (existuser.Enabled);

                        password = util.CreateRandomPassword(longPassword).ToUpper();

                        log.UserName = username.ToUpper();
                        log.Password = password.ToUpper();

                        Rooms room = BillingController.GetRoom(Int32.Parse(idhotel), "DEFAULT");
                        Users user = new Users();
                        user.IdHotel = Int32.Parse(idhotel);
                        user.IdRoom = room.IdRoom;
                        user.IdBillingType = billingType.IdBillingType;
                        user.BWDown = billingType.BWDown;
                        user.BWUp = billingType.BWUp;
                        user.ValidSince = DateTime.Now.ToUniversalTime(); // bill.BillingDate;
                        user.ValidTill = DateTime.Now.ToUniversalTime().AddHours(billingType.ValidTill); // bill.BillingDate.AddHours(billingType.ValidTill);
                        user.TimeCredit = billingType.TimeCredit;
                        user.MaxDevices = billingType.MaxDevices;
                        user.Priority = billingType.IdPriority;
                        user.VolumeDown = billingType.VolumeDown;
                        user.VolumeUp = billingType.VolumeUp;
                        user.Enabled = true;
                        user.Name = username.ToUpper();
                        user.Password = password.ToUpper();
                        user.CHKO = true;
                        user.BuyingFrom = 1;
                        user.BuyingCallerID = mac;
                        user.Comment = string.Empty;
                        user.IdLocation = location.IdLocation;
                        user.Filter_Id = billingType.Filter_Id;

                        Loyalties2 loyalty = new Loyalties2();
                        loyalty.IdHotel = user.IdHotel;
                        loyalty.Name = name.ToUpper();
                        loyalty.Surname = surname.ToUpper();
                        loyalty.Email = string.Empty;
                        loyalty.Date = DateTime.Now.ToUniversalTime();
                        loyalty.Survey = survey;
                        loyalty.IdLocatin = Int32.Parse(idlocation);
                        loyalty.CallerId = mac;
                        loyalty.UserName = user.Name;

                        BillingController.SaveLoyalty(loyalty);

                        BillingController.SaveUser(user);

                        response.IdUser = 1;
                        response.UserName = user.Name;
                        response.Password = user.Password;

                        TimeSpan diferencia = user.ValidTill - user.ValidSince;
                        
                        if (diferencia.TotalSeconds > user.TimeCredit)
                            response.ValidTill = user.ValidSince.AddSeconds(user.TimeCredit).AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                        else
                            response.ValidTill = user.ValidSince.AddSeconds(diferencia.TotalSeconds).AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");

                        if (string.IsNullOrEmpty(billingType.UrlLanding))
                        {
                            if (string.IsNullOrEmpty(location.UrlLanding))
                                response.Url = hotel.UrlHotel;
                            else
                                response.Url = location.UrlLanding;
                        }
                        else
                        {
                            response.Url = billingType.UrlLanding;
                        }

                        //DEMO
                        //response.Url = location.UrlLanding;

                        //if (string.IsNullOrEmpty(lang))
                        //    lang = "en";
                        ////log.RadiusResponse = "OK";
                        //util.sendMail(user.IdHotel, user.IdBillingType, mail, username.ToUpper(), password.ToUpper(), lang.Substring(0, 2));


                        //VALIDAMOS EL USERAGENT
                        UserAgentsTemp temp = BillingController.GetUserAgentTemp(mac);
                        if (temp.IdTemp != 0)
                        {
                            UserAgents userAgent = BillingController.GetUserAgent(temp.IdUserAgent);
                            List<UserAgents> listUA = BillingController.GetUserAgents(userAgent.String);

                            userAgent.Valid = userAgent.Valid + 1; ;
                            BillingController.SaveUserAgent(userAgent);
                            foreach (UserAgents x in listUA)
                            {
                                x.Valid = x.Valid + 1; ;
                                BillingController.SaveUserAgent(x);
                            }

                            //BillingController.DeleteUserAgentTemp(temp);
                        }

                        List<UserAgents> uAtoBlackList = BillingController.GetUserAgentsToBlackList(Int32.Parse(ConfigurationManager.AppSettings["MaxAttemps"].ToString()));
                        foreach (UserAgents uA in uAtoBlackList)
                        {
                            if (uA.Valid.Equals(0))
                            {
                                BlackListUserAgents bUA = BillingController.GetBlackUserAgent(uA.String);
                                if (bUA.IdBlackUserAgent.Equals(0))
                                {
                                    BlackListUserAgents obj = new BlackListUserAgents();
                                    obj.String = uA.String;
                                    BillingController.SaveBlackListUserAgent(obj);
                                }
                            }
                        }
                    }
                    else
                    {
                        response.IdUser = -1;
                        log.NomadixResponse = "Only one free access in 24 hours";
                        log.RadiusResponse = "Only one free access in 24 hours";
                    }

                //}
                //else
                //{
                //    response.IdUser = -2;
                //    log.NomadixResponse = "Email's format is not correct";
                //    log.RadiusResponse = "Email's format is not correct";

                //}
            }
            catch (Exception ex)
            {
                if (ex.GetType().Namespace.Contains("IO"))
                {
                    log.NomadixResponse = "OK";
                    log.RadiusResponse = "OK";

                    CaptivePortalLogInternal internalLog = new CaptivePortalLogInternal();
                    internalLog.NSEId = log.NSEId;
                    internalLog.CallerID = log.CallerID;
                    internalLog.Date = DateTime.Now.ToUniversalTime();
                    internalLog.UserName = "";
                    internalLog.Password = "";
                    internalLog.Page = "freeaccess";
                    internalLog.IdLocation = 0;

                    internalLog.NomadixResponse = "ERROR - CAN`T LOAD EMAIL TEMPLATE";
                    internalLog.RadiusResponse = "OK";

                    BillingController.SaveCaptivePortalLogInternal(internalLog);

                }
                else if (ex.GetType().Namespace.ToUpper().Contains("MAIL"))
                {
                    CaptivePortalLogInternal internalLog = new CaptivePortalLogInternal();
                    internalLog.NSEId = log.NSEId;
                    internalLog.CallerID = log.CallerID;
                    internalLog.Date = DateTime.Now.ToUniversalTime();
                    internalLog.UserName = "";
                    internalLog.Password = "";
                    internalLog.Page = "freeaccess";
                    internalLog.IdLocation = 0;
                    internalLog.NomadixResponse = "ERROR - CAN`T SEND EMAIL TO CLIENT";
                    internalLog.RadiusResponse = "OK";

                    BillingController.SaveCaptivePortalLogInternal(internalLog);

                    log.NomadixResponse = "OK ";
                    log.RadiusResponse = "OK";

                }
                else
                {
                    log.NomadixResponse = "OK";
                    log.RadiusResponse = "OK";

                    CaptivePortalLogInternal internalLog = new CaptivePortalLogInternal();
                    internalLog.NSEId = log.NSEId;
                    internalLog.CallerID = log.CallerID;
                    internalLog.Date = DateTime.Now.ToUniversalTime();
                    internalLog.UserName = "";
                    internalLog.Password = "";
                    internalLog.Page = "freeaccess";
                    internalLog.IdLocation = 0;
                    internalLog.NomadixResponse = "ERROR NO CONTEMPLADO - " + ex.Message;
                    internalLog.RadiusResponse = "OK";

                    BillingController.SaveCaptivePortalLogInternal(internalLog);
                }

            }

            BillingController.SaveCaptivePortalLog(log);

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private UserResponse freeaccessnosurvey(HttpContext context)
        {
            CaptivePortalLog log = new CaptivePortalLog();
            UserResponse response = new UserResponse();
            try
            {
                string name = (context.Request["name"] == null ? string.Empty : context.Request["name"].ToString());
                string surname = (context.Request["surname"] == null ? string.Empty : context.Request["surname"].ToString());

                string idhotel = (context.Request["idhotel"] == null ? string.Empty : context.Request["idhotel"].ToString());
                string mac = (context.Request["mac"] == null ? string.Empty : context.Request["mac"].ToString());
                string idlocation = (context.Request["idlocation"] == null ? string.Empty : context.Request["idlocation"].ToString());
                string mailing = (context.Request["mailing"] == null ? string.Empty : context.Request["mailing"].ToString());
                string origin = (context.Request["origin"] == null ? string.Empty : context.Request["origin"].ToString());
                string idbillingtype = (context.Request["idbilling"] == null ? string.Empty : context.Request["idbilling"].ToString());

                Hotels hotel = BillingController.GetHotel(Int32.Parse(idhotel));
                Locations2 location = BillingController.ObtainLocation(Int32.Parse(idlocation));

                SitesGMT siteGMT = BillingController.SiteGMTSitebyLocation(location.IdLocation);
                if (siteGMT.Id.Equals(0))
                    siteGMT = BillingController.SiteGMTSitebySite(location.IdHotel);

                log.NSEId = string.Format("Meraki - Idhotel:{0}", idhotel);
                log.CallerID = mac;
                log.Date = DateTime.Now.ToUniversalTime();
                log.UserName = "";
                log.Password = "";
                log.Page = "freeaccess no survey";
                log.IdLocation = Int32.Parse(idlocation);
                log.RadiusResponse = "NOT APPLICABLE";
                log.NomadixResponse = "NOT APPLICABLE";

                log.IdHotel = hotel.IdHotel;
                BillingTypes billingType = BillingController.ObtainBillingType(Int32.Parse(idbillingtype));

                bool allow = true;
                Users lastUser = BillingController.GetRadiusUsermac(mac, hotel.IdHotel);
                if (!lastUser.IdUser.Equals(0))
                {
                    BillingTypes bt = BillingController.ObtainBillingType(lastUser.IdBillingType);
                    if ((bt.IdBillingModule.Equals(1) || bt.IdBillingModule.Equals(3)) && lastUser.ValidSince > DateTime.Now.ToUniversalTime().AddHours(-hotel.FreeAccessRepeatTime))
                        allow = false;
                }

                if (allow)
                {
                    Rooms room = BillingController.GetRoom(Int32.Parse(idhotel), "DEFAULT");
                    List<ParametrosConfiguracion> parametros = BillingController.GetParametrosConfiguracion(hotel.IdHotel);
                    int longUserName = 3;
                    int longPassword = 3;
                    string prefixUserName = string.Empty;

                    foreach (ParametrosConfiguracion obj in parametros)
                    {
                        switch (obj.Key.ToUpper())
                        {
                            case "LONGUSERNAME": longUserName = Int32.Parse(obj.value); break;
                            case "LONGPASSWORD": longPassword = Int32.Parse(obj.value); break;
                            case "USERNAMEPREFIX": prefixUserName = obj.value; break;
                        }
                    }

                    if (longPassword < 3)
                        longPassword = 3;
                    if (longUserName < 3)
                        longUserName = 3;

                    Users existuser = null;
                    string username = string.Empty;
                    do
                    {
                        username = string.Format("{0}{1}", prefixUserName, util.createRandomUserNumber(longUserName)).ToUpper();
                        existuser = BillingController.GetRadiusUser(username.ToUpper(), hotel.IdHotel);

                    } while (existuser.Enabled);

                    string password = util.CreateRandomPassword(longPassword).ToUpper();

                    log.UserName = username.ToUpper();
                    log.Password = password.ToUpper();

                    Users user = new Users();
                    user.IdHotel = Int32.Parse(idhotel);
                    user.IdRoom = room.IdRoom;
                    user.IdBillingType = billingType.IdBillingType;
                    user.BWDown = billingType.BWDown;
                    user.BWUp = billingType.BWUp;
                    user.ValidSince = DateTime.Now.ToUniversalTime(); // bill.BillingDate;
                    user.ValidTill = DateTime.Now.ToUniversalTime().AddHours(billingType.ValidTill); // bill.BillingDate.AddHours(billingType.ValidTill);
                    user.TimeCredit = billingType.TimeCredit;
                    user.MaxDevices = billingType.MaxDevices;
                    user.Priority = billingType.IdPriority;
                    user.VolumeDown = billingType.VolumeDown;
                    user.VolumeUp = billingType.VolumeUp;
                    user.Enabled = true;
                    user.Name = username.ToUpper();
                    user.Password = password.ToUpper();
                    user.CHKO = true;
                    user.BuyingFrom = 1;
                    user.BuyingCallerID = mac;
                    user.Comment = string.Empty;
                    user.IdLocation = location.IdLocation;
                    user.Filter_Id = billingType.Filter_Id;

                    BillingController.SaveUser(user);

                    response.IdUser = 1;
                    response.UserName = user.Name;
                    response.Password = user.Password;

                    TimeSpan diferencia = user.ValidTill - user.ValidSince;

                    if (diferencia.TotalSeconds > user.TimeCredit)
                        response.ValidTill = user.ValidSince.AddSeconds(user.TimeCredit).AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                    else
                        response.ValidTill = user.ValidSince.AddSeconds(diferencia.TotalSeconds).AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");


                    if (string.IsNullOrEmpty(billingType.UrlLanding))
                    {
                        if (string.IsNullOrEmpty(location.UrlLanding))
                            response.Url = hotel.UrlHotel;
                        else
                            response.Url = location.UrlLanding;
                    }
                    else
                    {
                        response.Url = billingType.UrlLanding;
                    }
                    //DEMO
                    //response.Url = location.UrlLanding;

                    log.RadiusResponse = "OK";

                    //VALIDAMOS EL USERAGENT
                    UserAgentsTemp temp = BillingController.GetUserAgentTemp(mac);
                    if (temp.IdTemp != 0)
                    {
                        UserAgents userAgent = BillingController.GetUserAgent(temp.IdUserAgent);
                        List<UserAgents> listUA = BillingController.GetUserAgents(userAgent.String);

                        userAgent.Valid = userAgent.Valid + 1; ;
                        BillingController.SaveUserAgent(userAgent);
                        foreach (UserAgents x in listUA)
                        {
                            x.Valid = x.Valid + 1; ;
                            BillingController.SaveUserAgent(x);
                        }
                    }

                    List<UserAgents> uAtoBlackList = BillingController.GetUserAgentsToBlackList(Int32.Parse(ConfigurationManager.AppSettings["MaxAttemps"].ToString()));
                    foreach (UserAgents uA in uAtoBlackList)
                    {
                        if (uA.Valid.Equals(0))
                        {
                            BlackListUserAgents bUA = BillingController.GetBlackUserAgent(uA.String);
                            if (bUA.IdBlackUserAgent.Equals(0))
                            {
                                BlackListUserAgents obj = new BlackListUserAgents();
                                obj.String = uA.String;
                                BillingController.SaveBlackListUserAgent(obj);
                            }
                        }
                    }
                }
                else
                {
                    response.IdUser = -1;
                    log.NomadixResponse = "Only one free access in 24 hours";
                    log.RadiusResponse = "Only one free access in 24 hours";
                }
            }
            catch (Exception ex)
            {
                log.NomadixResponse = "OK";
                log.RadiusResponse = "OK";

                CaptivePortalLogInternal internalLog = new CaptivePortalLogInternal();
                internalLog.NSEId = log.NSEId;
                internalLog.CallerID = log.CallerID;
                internalLog.Date = DateTime.Now.ToUniversalTime();
                internalLog.UserName = "";
                internalLog.Password = "";
                internalLog.Page = "freeaccess";
                internalLog.IdLocation = 0;
                internalLog.NomadixResponse = "ERROR NO CONTEMPLADO - " + ex.Message;
                internalLog.RadiusResponse = "OK";

                BillingController.SaveCaptivePortalLogInternal(internalLog);

            }

            BillingController.SaveCaptivePortalLog(log);

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private FullSiteResponse fullsite(HttpContext context)
        {
            FullSiteResponse response = new FullSiteResponse();
            try
            {
                string idhotel = (context.Request["idhotel"] == null ? string.Empty : context.Request["idhotel"].ToString());
                string idlocation = (context.Request["idlocation"] == null ? string.Empty : context.Request["idlocation"].ToString());
                string lang = (context.Request["lang"] == null ? string.Empty : context.Request["lang"].ToString());
                string origin = (context.Request["origin"] == null ? string.Empty : context.Request["origin"].ToString());
                string mac = (context.Request["mac"] == null ? string.Empty : context.Request["mac"].ToString());

                // OBTENEMOS LOS DATOS DEL HOTEL Y EL IDIOMA QUE TENEMOS
                Hotels hotel = BillingController.GetHotel(Int32.Parse(idhotel));
                response.IdHotel = hotel.IdHotel;
                Languages language = BillingController.GetLanguage(lang);
                if (language.IdLanguage.Equals(0))
                    language.IdLanguage = hotel.IdLanguage;

                response.IdLanguageDefault = hotel.IdLanguage;

                //OBTENEMOS LOS DATOS DE PRIVACIDAD Y AVISO LEGAL Y LOS CONCATENAMOS
                Disclaimers dis1 = BillingController.GetDisclaimer(response.IdHotel, Int32.Parse(idlocation), language.IdLanguage, 1);
                Disclaimers dis2 = BillingController.GetDisclaimer(response.IdHotel, Int32.Parse(idlocation), language.IdLanguage, 2);
                if (dis1.IdDisclaimer != 0)
                    response.Disclaimer = dis1.Text;
                if (dis2.IdDisclaimer != 0)
                    response.Disclaimer += dis2.Text;

                //ACTIVAMOS LOS DIFERENTES MÓDULOS DE ACCESO
                response.MandatorySurvey = hotel.Survey;
                response.LoginModule = hotel.LoginModule;
                response.FreeAccessModule = hotel.FreeAccessModule;
                response.PayAccessModule = hotel.PayAccessModule;
                response.SocialNetworksModule = hotel.SocialNetworksModule;
                response.CustomAccessModule = hotel.CustomAccessModule;
                response.VoucherModule = hotel.VoucherModule;

                //OBTENEMOS LA LOCALIZACION Y EMPEZAMOS A CONFIGURAR LA MISMA
                Locations2 location = location = BillingController.ObtainLocation(Int32.Parse(idlocation));
                response.DefaultModule = location.DefaultModule;
                response.DisclaimerMandatory = location.Disclaimer;

                //AQUI COMPROBAMOS SEGURIDAD LOCATION -> MAC
                //consulta en base de datos BSID del ap asociada al site y location
                //comprobamos dicha BSID con la del AP
                //si son iguales continuamos el proceso.
                //si son distintas bloqueamos MAC del dispositivo con advertencia.

                //ASIGNAMOS LOS TEXTOS POR MODULO E IDIOMA
                List<LocationModuleText> locationTexts = BillingController.GetLocationModuleTexts(location.IdLocation, language.IdLanguage);
                foreach (LocationModuleText obj in locationTexts)
                {
                    switch (obj.IdBillingModule)
                    {
                        case 1: response.FreeText = obj.Text; break;
                        case 2: response.PremiumText = obj.Text; break;
                        case 3: response.SocialText = obj.Text; break;
                        case 5: response.LoginText = obj.Text; break;
                        case 6: response.VoucherText = obj.Text; break;
                        case 4: response.CustomText = obj.Text; break;

                    }
                }

                Locations2 locationAllZones = BillingController.ObtainLocation(hotel.IdHotel, "All_Zones");

                #region freeaccess
                if (hotel.FreeAccessModule)
                {
                    List<BillingTypes> list = BillingController.ObtainBillingTypes(location.IdLocation, 1);
                    //SI NO HAY TIPO DE ACCESO PARA EL MÓDULO EN LA LOCALIZACIÓN
                    if (list.Count.Equals(0))
                    {
                        //COMPROBAMOS QUE HAYA TIPO PARA EL MÓDULO EN ALL LOCATIONS
                        List<BillingTypes> listAllZones = BillingController.ObtainBillingTypes(locationAllZones.IdLocation, 1);
                        if (listAllZones.Count.Equals(0))
                        {
                            //COMPROBAMOS QUE TENGA PADRE Y HEREDE DEL MISMO.
                            if (hotel.IdParent.Equals(0) || (!hotel.IdParent.Equals(0) && !hotel.Inherit)) //SI NO TIENE PADRE O BIEN TIENE PERO NO HEREDA
                                response.FreeAccessModule = false;                                         // SE DESHABILITA EL MODULO
                            else
                            {
                                Hotels parent = null;
                                Hotels parentaux = hotel;
                                Locations2 allzonesParent = null;
                                bool end_bucle = false;

                                do
                                {
                                    parent = BillingController.GetHotel(parentaux.IdParent);
                                    allzonesParent = BillingController.ObtainLocation(parent.IdHotel, "All_Zones");
                                    List<BillingTypes> parentBillingAllzones = BillingController.ObtainBillingTypes(allzonesParent.IdLocation, 1);
                                    if (parentBillingAllzones.Count.Equals(0)) //EN EL PADRE TAMPOCO HAY TIPOS DE ACCESO, COMPROBAMOS QUE PODAMOS SEGUIR SUBIENDO
                                    {
                                        if (!parent.IdParent.Equals(0) && parent.Inherit)
                                        {
                                            parentaux = parent;
                                        }
                                        else
                                        {
                                            response.FreeAccessModule = false;
                                            end_bucle = true;
                                        }
                                    }
                                    else
                                    {
                                        BillingTypeResponse btfree = new BillingTypeResponse();
                                        btfree.IdBillingType = parentBillingAllzones[0].IdBillingType;
                                        btfree.Description = parentBillingAllzones[0].Description;
                                        btfree.Price = parentBillingAllzones[0].Price;
                                        btfree.ValidTill = parentBillingAllzones[0].ValidTill;
                                        btfree.BWDown = parentBillingAllzones[0].BWDown;
                                        btfree.BWUp = parentBillingAllzones[0].BWUp;
                                        btfree.VolumeDown = parentBillingAllzones[0].VolumeDown;
                                        btfree.VolumeUp = parentBillingAllzones[0].VolumeUp;
                                        btfree.MaxDevices = parentBillingAllzones[0].MaxDevices;
                                        TimeSpan t = TimeSpan.FromSeconds(parentBillingAllzones[0].TimeCredit / parentBillingAllzones[0].MaxDevices);
                                        if (parentBillingAllzones[0].TimeCredit >= 86400)
                                            btfree.TimeCredit += string.Format("{0}d ", t.Days);
                                        if (t.Hours > 0)
                                            btfree.TimeCredit += string.Format("{0:D2}h ", t.Hours);
                                        if (t.Minutes > 0)
                                            btfree.TimeCredit += string.Format("{0:D2}m ", t.Minutes);
                                        if (t.Seconds > 0)
                                            btfree.TimeCredit += string.Format("{0:D2}s ", t.Seconds);

                                        if (parentBillingAllzones[0].VolumeUp.Equals(0))
                                            btfree.LimitUp = "--";
                                        else
                                            btfree.LimitUp = string.Format("{0} Mb", (parentBillingAllzones[0].VolumeUp / (1024 * 1024)));

                                        if (parentBillingAllzones[0].VolumeDown.Equals(0))
                                            btfree.LimitDown = "--";
                                        else
                                            btfree.LimitDown = string.Format("{0} Mb", (parentBillingAllzones[0].VolumeDown / (1024 * 1024)));

                                        response.freeAccess = btfree;

                                        end_bucle = true;
                                    }

                                } while (!end_bucle);
                            }
                        }
                        else
                        {
                            BillingTypeResponse btfree = new BillingTypeResponse();
                            btfree.IdBillingType = listAllZones[0].IdBillingType;
                            btfree.Description = listAllZones[0].Description;
                            btfree.Price = listAllZones[0].Price;
                            btfree.ValidTill = listAllZones[0].ValidTill;
                            btfree.BWDown = listAllZones[0].BWDown;
                            btfree.BWUp = listAllZones[0].BWUp;
                            btfree.VolumeDown = listAllZones[0].VolumeDown;
                            btfree.VolumeUp = listAllZones[0].VolumeUp;
                            btfree.MaxDevices = listAllZones[0].MaxDevices;
                            TimeSpan t = TimeSpan.FromSeconds(listAllZones[0].TimeCredit / listAllZones[0].MaxDevices);
                            if (listAllZones[0].TimeCredit >= 86400)
                                btfree.TimeCredit += string.Format("{0}d ", t.Days);
                            if (t.Hours > 0)
                                btfree.TimeCredit += string.Format("{0:D2}h ", t.Hours);
                            if (t.Minutes > 0)
                                btfree.TimeCredit += string.Format("{0:D2}m ", t.Minutes);
                            if (t.Seconds > 0)
                                btfree.TimeCredit += string.Format("{0:D2}s ", t.Seconds);

                            if (listAllZones[0].VolumeUp.Equals(0))
                                btfree.LimitUp = "--";
                            else
                                btfree.LimitUp = string.Format("{0} Mb", (listAllZones[0].VolumeUp / (1024 * 1024)));

                            if (listAllZones[0].VolumeDown.Equals(0))
                                btfree.LimitDown = "--";
                            else
                                btfree.LimitDown = string.Format("{0} Mb", (listAllZones[0].VolumeDown / (1024 * 1024)));

                            response.freeAccess = btfree;
                        }
                    }
                    else
                    {
                        BillingTypeResponse btfree = new BillingTypeResponse();
                        btfree.IdBillingType = list[0].IdBillingType;
                        btfree.Description = list[0].Description;
                        btfree.Price = list[0].Price;
                        btfree.ValidTill = list[0].ValidTill;
                        btfree.BWDown = list[0].BWDown;
                        btfree.BWUp = list[0].BWUp;
                        btfree.VolumeDown = list[0].VolumeDown;
                        btfree.VolumeUp = list[0].VolumeUp;
                        btfree.MaxDevices = list[0].MaxDevices;
                        TimeSpan t = TimeSpan.FromSeconds(list[0].TimeCredit / list[0].MaxDevices);
                        if (list[0].TimeCredit >= 86400)
                            btfree.TimeCredit += string.Format("{0}d ", t.Days);
                        if (t.Hours > 0)
                            btfree.TimeCredit += string.Format("{0:D2}h ", t.Hours);
                        if (t.Minutes > 0)
                            btfree.TimeCredit += string.Format("{0:D2}m ", t.Minutes);
                        if (t.Seconds > 0)
                            btfree.TimeCredit += string.Format("{0:D2}s ", t.Seconds);

                        if (list[0].VolumeUp.Equals(0))
                            btfree.LimitUp = "--";
                        else
                            btfree.LimitUp = string.Format("{0} Mb", (list[0].VolumeUp / (1024 * 1024)));

                        if (list[0].VolumeDown.Equals(0))
                            btfree.LimitDown = "--";
                        else
                            btfree.LimitDown = string.Format("{0} Mb", (list[0].VolumeDown / (1024 * 1024)));

                        response.freeAccess = btfree;
                    }
                }

                #endregion

                #region Premium Access
                if (hotel.PayAccessModule)
                {
                    List<BillingTypes> list = BillingController.ObtainBillingTypes(location.IdLocation, 2);
                    if (list.Count.Equals(0))
                    {
                        list = BillingController.ObtainBillingTypes(locationAllZones.IdLocation, 2);
                        if (list.Count.Equals(0))
                        {
                            if (hotel.IdParent.Equals(0) || (!hotel.IdParent.Equals(0) && !hotel.Inherit)) //SI NO TIENE PADRE O BIEN TIENE PERO NO HEREDA
                                response.PayAccessModule = false;                                          //SE DESHABILITA EL MODULO
                            else
                            {
                                Hotels parent = null;
                                Hotels parentaux = hotel;
                                Locations2 allzonesParent = null;
                                bool end_bucle = false;

                                do
                                {
                                    parent = BillingController.GetHotel(parentaux.IdParent);
                                    allzonesParent = BillingController.ObtainLocation(parent.IdHotel, "All_Zones");
                                    List<BillingTypes> parentBillingAllzones = BillingController.ObtainBillingTypes(allzonesParent.IdLocation, 2);
                                    if (parentBillingAllzones.Count.Equals(0)) //EN EL PADRE TAMPOCO HAY TIPOS DE ACCESO, COMPROBAMOS QUE PODAMOS SEGUIR SUBIENDO
                                    {
                                        if (!parent.IdParent.Equals(0) && parent.Inherit)
                                        {
                                            parentaux = parent;
                                        }
                                        else
                                        {
                                            response.PayAccessModule = false;
                                            end_bucle = true;
                                        }
                                    }
                                    else
                                    {
                                        response.listPremium = new List<BillingTypeResponse>();

                                        foreach (BillingTypes b in parentBillingAllzones.OrderBy(a => a.Order))
                                        {
                                            if ((!b.FreeAccess) && (b.Enabled))
                                            {
                                                BillingTypeResponse br = BindBillingTypeResponse(hotel, b);

                                                response.listPremium.Add(br);
                                            }
                                        }

                                        end_bucle = true;
                                    }

                                } while (!end_bucle);
                            }
                        }
                        else
                        {
                            response.listPremium = new List<BillingTypeResponse>();

                            foreach (BillingTypes b in list.OrderBy(a => a.Order))
                            {
                                if ((!b.FreeAccess) && (b.Enabled))
                                {
                                    BillingTypeResponse br = BindBillingTypeResponse(hotel, b);
                                    //new BillingTypeResponse();
                                    //br.IdBillingType = b.IdBillingType;
                                    //br.Description = b.Description;
                                    //br.Price = b.Price;
                                    //br.ValidTill = b.ValidTill;
                                    //br.BWDown = b.BWDown;
                                    //br.BWUp = b.BWUp;
                                    //br.VolumeDown = b.VolumeDown;
                                    //br.VolumeUp = b.VolumeUp;
                                    //br.MaxDevices = b.MaxDevices;
                                    //TimeSpan t = TimeSpan.FromSeconds(b.TimeCredit / b.MaxDevices);
                                    //if (b.TimeCredit >= 86400)
                                    //    br.TimeCredit += string.Format("{0}d ", t.Days);
                                    //if (t.Hours > 0)
                                    //    br.TimeCredit += string.Format("{0:D2}h ", t.Hours);
                                    //if (t.Minutes > 0)
                                    //    br.TimeCredit += string.Format("{0:D2}m ", t.Minutes);
                                    //if (t.Seconds > 0)
                                    //    br.TimeCredit += string.Format("{0:D2}s ", t.Seconds);
                                    //Currencies currency = BillingController.Currency(hotel.CurrencyCode);
                                    //br.Currency = string.Format("{0} {1}", b.Price, currency.Symbol);


                                    //if (b.VolumeUp.Equals(0))
                                    //    br.LimitUp = "--";
                                    //else
                                    //    br.LimitUp = string.Format("{0} Mb", (b.VolumeUp / (1024 * 1024)));

                                    //if (b.VolumeDown.Equals(0))
                                    //    br.LimitDown = "--";
                                    //else
                                    //    br.LimitDown = string.Format("{0} Mb", (b.VolumeDown / (1024 * 1024)));

                                    response.listPremium.Add(br);
                                }
                            }
                        }
                    }
                    else
                    {
                        response.listPremium = new List<BillingTypeResponse>();

                        foreach (BillingTypes b in list.OrderBy(a => a.Order))
                        {
                            if ((!b.FreeAccess) && (b.Enabled))
                            {
                                BillingTypeResponse br = BindBillingTypeResponse(hotel, b);
                                //br.IdBillingType = b.IdBillingType;
                                //br.Description = b.Description;
                                //br.Price = b.Price;
                                //br.ValidTill = b.ValidTill;
                                //br.BWDown = b.BWDown;
                                //br.BWUp = b.BWUp;
                                //br.VolumeDown = b.VolumeDown;
                                //br.VolumeUp = b.VolumeUp;
                                //br.MaxDevices = b.MaxDevices;
                                //TimeSpan t = TimeSpan.FromSeconds(b.TimeCredit / b.MaxDevices);
                                //if (b.TimeCredit >= 86400)
                                //    br.TimeCredit += string.Format("{0}d ", t.Days);
                                //if (t.Hours > 0)
                                //    br.TimeCredit += string.Format("{0:D2}h ", t.Hours);
                                //if (t.Minutes > 0)
                                //    br.TimeCredit += string.Format("{0:D2}m ", t.Minutes);
                                //if (t.Seconds > 0)
                                //    br.TimeCredit += string.Format("{0:D2}s ", t.Seconds);

                                //Currencies currency = BillingController.Currency(hotel.CurrencyCode);
                                //br.Currency = string.Format("{0} {1}", b.Price, currency.Symbol);

                                //if (b.VolumeUp.Equals(0))
                                //    br.LimitUp = "--";
                                //else
                                //    br.LimitUp = string.Format("{0} Mb", (b.VolumeUp / (1024 * 1024)));

                                //if (b.VolumeDown.Equals(0))
                                //    br.LimitDown = "--";
                                //else
                                //    br.LimitDown = string.Format("{0} Mb", (b.VolumeDown / (1024 * 1024)));

                                response.listPremium.Add(br);
                            }
                        }
                    }
                }

                #endregion

                #region SOCIAL ACCESS
                //ACCESO SOCIAL
                if (hotel.SocialNetworksModule)
                {
                    List<BillingTypes> fbpromotions = BillingController.ObtainBillingTypes(location.IdLocation, 3);
                    if (fbpromotions.Count.Equals(0))
                    {
                        if (hotel.IdParent.Equals(0) || (!hotel.IdParent.Equals(0) && !hotel.Inherit)) //SI NO TIENE PADRE O BIEN TIENE PERO NO HEREDA
                            response.SocialNetworksModule = false;                                     // SE DESHABILITA EL MODULO
                        else
                        {
                            Hotels parent = null;
                            Hotels parentaux = hotel;
                            Locations2 allzonesParent = null;
                            bool end_bucle = false;

                            do
                            {
                                parent = BillingController.GetHotel(parentaux.IdParent);
                                allzonesParent = BillingController.ObtainLocation(parent.IdHotel, "All_Zones");
                                List<BillingTypes> parentBillingAllzones = BillingController.ObtainBillingTypes(allzonesParent.IdLocation, 3);
                                if (parentBillingAllzones.Count.Equals(0)) //EN EL PADRE TAMPOCO HAY TIPOS DE ACCESO, COMPROBAMOS QUE PODAMOS SEGUIR SUBIENDO
                                {
                                    if (!parent.IdParent.Equals(0) && parent.Inherit)
                                    {
                                        parentaux = parent;
                                    }
                                    else
                                    {
                                        response.SocialNetworksModule = false;
                                        end_bucle = true;
                                    }
                                }
                                else
                                {
                                    BillingTypeResponse btfree = BindBillingTypeResponse(hotel, parentBillingAllzones[0]);
                                    //btfree.IdBillingType = parentBillingAllzones[0].IdBillingType;
                                    //btfree.Description = parentBillingAllzones[0].Description;
                                    //btfree.Price = parentBillingAllzones[0].Price;
                                    //btfree.ValidTill = parentBillingAllzones[0].ValidTill;
                                    //btfree.BWDown = parentBillingAllzones[0].BWDown;
                                    //btfree.BWUp = parentBillingAllzones[0].BWUp;
                                    //btfree.VolumeDown = parentBillingAllzones[0].VolumeDown;
                                    //btfree.VolumeUp = parentBillingAllzones[0].VolumeUp;
                                    //btfree.MaxDevices = parentBillingAllzones[0].MaxDevices;
                                    //TimeSpan t = TimeSpan.FromSeconds(parentBillingAllzones[0].TimeCredit / parentBillingAllzones[0].MaxDevices);
                                    //if (parentBillingAllzones[0].TimeCredit >= 86400)
                                    //    btfree.TimeCredit += string.Format("{0}d ", t.Days);
                                    //if (t.Hours > 0)
                                    //    btfree.TimeCredit += string.Format("{0:D2}h ", t.Hours);
                                    //if (t.Minutes > 0)
                                    //    btfree.TimeCredit += string.Format("{0:D2}m ", t.Minutes);
                                    //if (t.Seconds > 0)
                                    //    btfree.TimeCredit += string.Format("{0:D2}s ", t.Seconds);

                                    //if (parentBillingAllzones[0].VolumeUp.Equals(0))
                                    //    btfree.LimitUp = "--";
                                    //else
                                    //    btfree.LimitUp = string.Format("{0} Mb", (parentBillingAllzones[0].VolumeUp / (1024 * 1024)));

                                    //if (parentBillingAllzones[0].VolumeDown.Equals(0))
                                    //    btfree.LimitDown = "--";
                                    //else
                                    //    btfree.LimitDown = string.Format("{0} Mb", (parentBillingAllzones[0].VolumeDown / (1024 * 1024)));

                                    response.socialAccess = btfree;

                                    end_bucle = true;
                                }

                            } while (!end_bucle);
                        }
                    }
                    else
                    {
                        
                        BillingTypes b = BillingController.ObtainBillingType(fbpromotions[0].IdBillingType);
                        BillingTypeResponse btsocial = BindBillingTypeResponse(hotel, b);

                        btsocial.IdBillingType = b.IdBillingType;
                        btsocial.Description = b.Description;
                        btsocial.Price = b.Price;
                        btsocial.ValidTill = b.ValidTill;
                        btsocial.BWDown = b.BWDown;
                        btsocial.BWUp = b.BWUp;
                        btsocial.VolumeDown = b.VolumeDown;
                        btsocial.VolumeUp = b.VolumeUp;
                        btsocial.MaxDevices = b.MaxDevices;
                        TimeSpan t = TimeSpan.FromSeconds(b.TimeCredit / b.MaxDevices);
                        if (b.TimeCredit >= 86400)
                            btsocial.TimeCredit += string.Format("{0}d ", t.Days);
                        if (t.Hours > 0)
                            btsocial.TimeCredit += string.Format("{0:D2}h ", t.Hours);
                        if (t.Minutes > 0)
                            btsocial.TimeCredit += string.Format("{0:D2}m ", t.Minutes);
                        if (t.Seconds > 0)
                            btsocial.TimeCredit += string.Format("{0:D2}s ", t.Seconds);

                        if (b.VolumeUp.Equals(0))
                            btsocial.LimitUp = "--";
                        else
                            btsocial.LimitUp = string.Format("{0} Mb", (b.VolumeUp / (1024 * 1024)));

                        if (b.VolumeDown.Equals(0))
                            btsocial.LimitDown = "--";
                        else
                            btsocial.LimitDown = string.Format("{0} Mb", (b.VolumeDown / (1024 * 1024)));

                        response.socialAccess = btsocial;

                    }
                }

                #endregion

                #region Custom Access

                if (hotel.CustomAccessModule)
                {
                    List<BillingTypes> list = BillingController.ObtainBillingTypes(location.IdLocation, 4);
                    if (list.Count.Equals(0))
                    {
                        List<BillingTypes> listAllZones = BillingController.ObtainBillingTypes(locationAllZones.IdLocation, 4);
                        if (listAllZones.Count.Equals(0))
                            response.CustomAccessModule = false;
                    }
                }

                #endregion

                response.survey = new List<SurveyResponse>();

                if (hotel.Survey)
                {
                    response.MandatorySurvey = location.MandatorySurvey;
                    
                    if (location.MandatorySurvey)
                    {
                        bool loadSurvey = true;
                        List<Loyalties2> loyalties = BillingController.GetLoyatiesMacLocation(mac, location.IdLocation);
                        if (loyalties.Count > 0)
                        {
                            Loyalties2 lastUpdate = loyalties[0];
                            TimeSpan diff = DateTime.Now.ToUniversalTime() - lastUpdate.Date;
                            if (diff.TotalHours < location.DelaySurvey)
                                response.MandatorySurvey = false;
                        }
                      

                        if (loadSurvey)
                        {
                            List<Surveys2> listsurvey = null; BillingController.GetSurveys(hotel.IdHotel, language.IdLanguage);

                            listsurvey = BillingController.GetSurveyLocation(location.IdLocation, language.IdLanguage);

                            if (listsurvey.Count.Equals(0))
                                listsurvey = BillingController.GetSurveys(hotel.IdHotel, language.IdLanguage);

                            foreach (Surveys2 b in listsurvey.OrderBy(a => a.Order))
                            {
                                SurveyResponse s = new SurveyResponse();
                                s.IdSurvey = b.IdSurvey;
                                s.IdHotel = b.IdHotel;
                                s.Question = b.Question;
                                string[] types = b.Type.Split('-');
                                s.Type = types[0].Trim();
                                if (types.Length > 1)
                                {
                                    s.Values = types[1];
                                }
                                s.Order = b.Order;

                                response.survey.Add(s);
                            }
                        }
                    }
                }
            }
            catch
            {
                ;
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private static BillingTypeResponse BindBillingTypeResponse(Hotels hotel, BillingTypes b)
        {
            BillingTypeResponse br = new BillingTypeResponse();
            br.IdBillingType = b.IdBillingType;
            br.Description = b.Description;
            br.Price = b.Price;
            br.ValidTill = b.ValidTill;
            br.BWDown = b.BWDown;
            br.BWUp = b.BWUp;
            br.VolumeDown = b.VolumeDown;
            br.VolumeUp = b.VolumeUp;
            br.MaxDevices = b.MaxDevices;
            TimeSpan t = TimeSpan.FromSeconds(b.TimeCredit / b.MaxDevices);
            if (b.TimeCredit >= 86400)
                br.TimeCredit += string.Format("{0}d ", t.Days);
            if (t.Hours > 0)
                br.TimeCredit += string.Format("{0:D2}h ", t.Hours);
            if (t.Minutes > 0)
                br.TimeCredit += string.Format("{0:D2}m ", t.Minutes);
            if (t.Seconds > 0)
                br.TimeCredit += string.Format("{0:D2}s ", t.Seconds);
            Currencies2 currency = BillingController.Currency(hotel.CurrencyCode);
            br.Currency = string.Format("{0} {1}", b.Price, currency.Symbol);


            if (b.VolumeUp.Equals(0))
                br.LimitUp = "--";
            else
                br.LimitUp = string.Format("{0} Mb", (b.VolumeUp / (1024 * 1024)));

            if (b.VolumeDown.Equals(0))
                br.LimitDown = "--";
            else
                br.LimitDown = string.Format("{0} Mb", (b.VolumeDown / (1024 * 1024)));
            return br;
        }

        private UserResponse instagramaccess(HttpContext context)
        {
            CaptivePortalLog log = new CaptivePortalLog();
            UserResponse response = new UserResponse();
            try
            {
                string id = (context.Request["id"] == null ? string.Empty : context.Request["id"].ToString());

                string idhotel = (context.Request["idhotel"] == null ? string.Empty : context.Request["idhotel"].ToString());
                string mac = (context.Request["mac"] == null ? string.Empty : context.Request["mac"].ToString());
                string idlocation = (context.Request["idlocation"] == null ? string.Empty : context.Request["idlocation"].ToString());

                string lang = (context.Request["lang"] == null ? string.Empty : context.Request["lang"].ToString());
                string survey = (context.Request["survey"] == null ? string.Empty : context.Request["survey"].ToString());
                string origin = (context.Request["origin"] == null ? string.Empty : context.Request["origin"].ToString());
                string idbillingtype = (context.Request["idbillingtype"] == null ? string.Empty : context.Request["idbillingtype"].ToString());

                log.NSEId = string.Format("Meraki - Idhotel:{0}", idhotel);
                log.CallerID = mac;
                log.Date = DateTime.Now.ToUniversalTime();
                log.UserName = "";
                log.Password = "";
                log.Page = "instagram";
                log.IdLocation = Int32.Parse(idlocation);
                log.IdHotel = Int32.Parse(idhotel);
                log.RadiusResponse = "NOT APPLICABLE";
                log.NomadixResponse = "NOT APPLICABLE";

                    BillingTypes billingType = BillingController.ObtainBillingType(Int32.Parse(idbillingtype));
                    Hotels hotel = BillingController.GetHotel(Int32.Parse(idhotel));
                    Locations2 location = BillingController.ObtainLocation(Int32.Parse(idlocation));

                    bool allow = true;
                    Users lastUser = BillingController.GetRadiusUsermac(mac, hotel.IdHotel);
                    if (!lastUser.IdUser.Equals(0))
                    {
                        BillingTypes bt = BillingController.ObtainBillingType(lastUser.IdBillingType);
                        if ((bt.IdBillingModule.Equals(1) || bt.IdBillingModule.Equals(3)) && lastUser.ValidSince > DateTime.Now.ToUniversalTime().AddHours(-hotel.FreeAccessRepeatTime))
                            allow = false;
                    }

                    if (allow)
                    {

                        Rooms room = BillingController.GetRoom(Int32.Parse(idhotel), "DEFAULT");

                        Users user = null;

                        bool existe = false;

                        if (id.ToUpper().Equals("UNDEFINED") || string.IsNullOrEmpty(id))  //EL USUARIO NO HA CONCEDIDO SU CORREO ELECTRÓNICO
                        {
                            id = string.Format("{0}{1}", "INS", util.createRandomUserNumber(8)).ToUpper(); //GENERAMOS UN USUARIO NUEVO
                            existe = true;

                            do
                            {
                                user = BillingController.GetRadiusUser(id.ToUpper(), hotel.IdHotel);  //COMPROBAMOS QUE NO EXISTA EN LA BASE DE DATOS
                                if (user.IdUser.Equals(0))
                                    existe = false; // SI NO EXISTE, CONTINUAMOS 
                                else
                                {
                                    if (user.Enabled) // SI EXISTE Y ESTÁ ACTIVO, GENERAMOS UNO NUEVO Y EMPEZAMOS EL PROCESO DE NUEVO
                                    {
                                        id = string.Format("{0}{1}", "INS", util.createRandomUserNumber(8)).ToUpper();
                                    }
                                    else
                                    {//SI NO ESTÁ ACTIVO, CONTINUAMOS CON USUARIO GENERADO Y CREAMOS UN USUARIO NUEVO
                                        existe = false;
                                        user = new Users();
                                    }
                                }

                            } while (existe);
                        }
                        else
                            user = BillingController.GetRadiusUser(id.ToUpper(), hotel.IdHotel);

                        if (user.IdUser.Equals(0))
                            user = new Users();

                        user.IdHotel = Int32.Parse(idhotel);
                        user.IdRoom = room.IdRoom;
                        user.IdBillingType = billingType.IdBillingType;
                        user.BWDown = billingType.BWDown;
                        user.BWUp = billingType.BWUp;
                        user.ValidSince = DateTime.Now.ToUniversalTime(); // bill.BillingDate;
                        user.ValidTill = DateTime.Now.ToUniversalTime().AddHours(billingType.ValidTill); // bill.BillingDate.AddHours(billingType.ValidTill);
                        user.TimeCredit = billingType.TimeCredit;
                        user.MaxDevices = billingType.MaxDevices;
                        user.Priority = billingType.IdPriority;
                        user.VolumeDown = billingType.VolumeDown;
                        user.VolumeUp = billingType.VolumeUp;
                        user.Enabled = true;
                        user.Name = id.ToUpper();
                        user.Password = id.ToUpper();
                        user.CHKO = true;
                        user.BuyingFrom = 1;
                        user.BuyingCallerID = mac;
                        user.IdLocation = location.IdLocation;
                        user.Comment = string.Empty;
                        user.Filter_Id = billingType.Filter_Id;

                        Loyalties2 loyalty = new Loyalties2();
                        loyalty.IdHotel = user.IdHotel;
                        loyalty.Name = string.Empty;
                        loyalty.Surname = string.Empty;
                        loyalty.Email = id;
                        loyalty.Date = DateTime.Now.ToUniversalTime();
                        loyalty.Survey = survey;
                        loyalty.IdLocatin = Int32.Parse(idlocation);
                        loyalty.CallerId = mac;
                    loyalty.UserName = user.Name;

                        BillingController.SaveLoyalty(loyalty);

                        BillingController.SaveUser(user);

                        response.IdUser = 1;
                        response.UserName = user.Name;
                        response.Password = user.Password;

                        SitesGMT siteGMT = BillingController.SiteGMTSitebyLocation(location.IdLocation);
                        if (siteGMT.Id.Equals(0))
                            siteGMT = BillingController.SiteGMTSitebySite(location.IdHotel);

                        TimeSpan diferencia = user.ValidTill - user.ValidSince;

                        if (diferencia.TotalSeconds > user.TimeCredit)
                            response.ValidTill = user.ValidSince.AddSeconds(user.TimeCredit).AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                        else
                            response.ValidTill = user.ValidSince.AddSeconds(diferencia.TotalSeconds).AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");

                        if (string.IsNullOrEmpty(billingType.UrlLanding))
                        {
                            if (string.IsNullOrEmpty(location.UrlLanding))
                                response.Url = hotel.UrlHotel;
                            else
                                response.Url = location.UrlLanding;
                        }
                        else
                            response.Url = billingType.UrlLanding;

                        if (string.IsNullOrEmpty(lang))
                            lang = "en";
                    }
                else
                {
                    response.IdUser = -1;
                    log.NomadixResponse = "Only one free access in hotel period";
                    log.RadiusResponse = "Only one free access in hotel period";
                }
               
            }
            catch (Exception ex)
            {
                if (ex.GetType().Name.ToUpper().Contains("URI"))
                {
                    log.NomadixResponse = "ERROR - NOMADIX NO COMUNICATIONS";
                    log.RadiusResponse = "NOT APPLICABLE";
                }
                else
                {
                    log.NomadixResponse = "OK";
                    log.RadiusResponse = "OK";

                    CaptivePortalLogInternal internalLog = new CaptivePortalLogInternal();
                    internalLog.NSEId = log.NSEId;
                    internalLog.CallerID = log.CallerID;
                    internalLog.Date = DateTime.Now.ToUniversalTime();
                    internalLog.UserName = "";
                    internalLog.Password = "";
                    internalLog.Page = "socialnetwork";
                    internalLog.IdLocation = 0;
                    internalLog.NomadixResponse = "ERROR NO CONTEMPLADO - " + ex.Message;
                    internalLog.RadiusResponse = "OK";

                    BillingController.SaveCaptivePortalLogInternal(internalLog);
                }

            }

            BillingController.SaveCaptivePortalLog(log);

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private void login(HttpContext context)
        {
            string json = string.Empty;
            CaptivePortalLog log = new CaptivePortalLog();
            try
            {
                string username = (context.Request["username"] == null ? string.Empty : context.Request["username"].ToString());
                string password = (context.Request["password"] == null ? string.Empty : context.Request["password"].ToString());
                string idhotel = (context.Request["idhotel"] == null ? string.Empty : context.Request["idhotel"].ToString());
                string idlocation = (context.Request["idlocation"] == null ? string.Empty : context.Request["idlocation"].ToString());
                string mac = (context.Request["mac"] == null ? string.Empty : context.Request["mac"].ToString());

                log.NSEId = string.Format("Meraki - Idhotel:{0}", idhotel);
                log.CallerID = mac;
                log.Date = DateTime.Now.ToUniversalTime();
                log.UserName = username.ToUpper();
                log.Password = password.ToUpper();
                log.Page = "login";
                log.IdLocation = Int32.Parse(idlocation);
                log.IdHotel = Int32.Parse(idhotel);
                log.RadiusResponse = string.Empty;
                log.NomadixResponse = string.Empty;

                Hotels hotel = BillingController.GetHotel(Int32.Parse(idhotel));
                Locations2 location = BillingController.ObtainLocation(Int32.Parse(idlocation));
                Users user = BillingController.GetRadiusUser(username, hotel.IdHotel);
                SitesGMT siteGMT = BillingController.SiteGMTSitebyLocation(location.IdLocation);
                if (siteGMT.Id.Equals(0))
                    siteGMT = BillingController.SiteGMTSitebySite(location.IdHotel);

                string allow = "allow";

                if (hotel.CustomAccessModule)
                {
                    CustomAccess ca = BillingController.GetCustomAccess(hotel.IdHotel);

                }

                if (allow.ToUpper().Equals("ALLOW"))
                {
                    string validtill = string.Empty;
                    TimeSpan diferencia = user.ValidTill - user.ValidSince;

                    if (diferencia.TotalSeconds > user.TimeCredit)
                        validtill = user.ValidSince.AddSeconds(user.TimeCredit).AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                    else
                        validtill = user.ValidSince.AddSeconds(diferencia.TotalSeconds).AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");

                    if (!user.IdUser.Equals(0))
                    {
                        BillingTypes bt = BillingController.ObtainBillingType(user.IdBillingType);

                        if (!string.IsNullOrEmpty(bt.UrlLanding))
                            json = "{\"code\":\"OK\",\"message\":\"" + bt.UrlLanding + "\",\"ValidTill\":\"" + validtill + "\"}";
                        else
                        {
                            if (!string.IsNullOrEmpty(location.UrlLanding))
                                json = "{\"code\":\"OK\",\"message\":\"" + location.UrlLanding + "\",\"ValidTill\":\"" + validtill + "\"}";
                            else
                                json = "{\"code\":\"OK\",\"message\":\"" + hotel.UrlHotel + "\",\"ValidTill\":\"" + validtill + "\"}";

                        }
                    }
                    else
                    {
                        json = "{\"code\":\"OK\",\"message\":\"" + hotel.UrlHotel + "\",\"ValidTill\":\"" + validtill + "\"}";
                    }
                }
                else
                {
                    json = "{\"code\":\"ERROR\",\"message\":\"" + allow + "\"}";
                    log.NomadixResponse = "WS ERROR RESPONSE - " + allow;
                    log.RadiusResponse = "NOT APPLICABLE";
                }

                //VALIDAMOS EL USERAGENT
                UserAgentsTemp temp = BillingController.GetUserAgentTemp(mac);
                if (temp.IdTemp != 0)
                {
                    UserAgents userAgent = BillingController.GetUserAgent(temp.IdUserAgent);
                    List<UserAgents> listUA = BillingController.GetUserAgents(userAgent.String);

                    userAgent.Valid = userAgent.Valid + 1; ;
                    BillingController.SaveUserAgent(userAgent);
                    foreach (UserAgents x in listUA)
                    {
                        x.Valid = x.Valid + 1; ;
                        BillingController.SaveUserAgent(x);
                    }

                    //BillingController.DeleteUserAgentTemp(temp);
                }

                //METEMOS LOS USERAGENTS EN LA LISTA NEGRA
                List<UserAgents> uAtoBlackList = BillingController.GetUserAgentsToBlackList(Int32.Parse(ConfigurationManager.AppSettings["MaxAttemps"].ToString()));
                foreach (UserAgents uA in uAtoBlackList)
                {
                    if (uA.Valid.Equals(0))
                    {
                        BlackListUserAgents bUA = BillingController.GetBlackUserAgent(uA.String);
                        if (bUA.IdBlackUserAgent.Equals(0))
                        {
                            BlackListUserAgents obj = new BlackListUserAgents();
                            obj.String = uA.String;
                            BillingController.SaveBlackListUserAgent(obj);
                        }
                    }
                }

                BillingController.SaveCaptivePortalLog(log);
            }
            catch (Exception ex)
            {
                json = "{\"code\":\"ERROR\",\"message\":\" " + ex.Message + ".\"}";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();
        }

        private PayPalResponse paypalrequestnosurvey(HttpContext context)
        {
            PayPalResponse response = new PayPalResponse();
            try
            {
                string origin = (context.Request["origin"] == null ? string.Empty : context.Request["origin"].ToString());

                string idhotel = (context.Request["idhotel"] == null ? string.Empty : context.Request["idhotel"].ToString());
                string mac = (context.Request["mac"] == null ? string.Empty : context.Request["mac"].ToString());
                string idlocation = (context.Request["idlocation"] == null ? string.Empty : context.Request["idlocation"].ToString());
                string idBillingType = (context.Request["IdBillingType"] == null ? string.Empty : context.Request["IdBillingType"].ToString());
                string mail = (context.Request["email"] == null ? string.Empty : context.Request["email"].ToString());

                Hotels hotel = BillingController.GetHotel(Int32.Parse(idhotel));
                Locations2 location = BillingController.ObtainLocation(Int32.Parse(idlocation));

                if (location != null)
                {
                    BillingTypes bt = BillingController.ObtainBillingType(Int32.Parse(idBillingType));

                    PayPalTransactions transaction = new PayPalTransactions();
                    transaction.IdHotel = location.IdHotel;
                    transaction.IdBillingType = bt.IdBillingType;
                    transaction.Price = bt.Price;
                    transaction.CreateDate = DateTime.Now.ToUniversalTime();
                    transaction.UpdateDate = DateTime.Now.ToUniversalTime();
                    transaction.Status = 0;
                    transaction.NameClient = "NO SURVEY";
                    transaction.Email = mail;
                    transaction.Survey = "NO MANDATORY";
                    transaction.CallerIP = origin;
                    transaction.IdLocation = location.IdLocation;

                    List<ParametrosConfiguracion> parametros = BillingController.GetParametrosConfiguracion(hotel.IdHotel);
                    int longUserName = 3;
                    int longPassword = 3;
                    string prefixUserName = string.Empty;

                    foreach (ParametrosConfiguracion obj in parametros)
                    {
                        switch (obj.Key.ToUpper())
                        {
                            case "LONGUSERNAME": longUserName = Int32.Parse(obj.value); break;
                            case "LONGPASSWORD": longPassword = Int32.Parse(obj.value); break;
                            case "USERNAMEPREFIX": prefixUserName = obj.value; break;
                        }
                    }

                    if (longPassword < 3)
                        longPassword = 3;
                    if (longUserName < 3)
                        longUserName = 3;

                    Users existuser = null;
                    string username = string.Empty;
                    do
                    {
                        username = string.Format("{0}{1}", prefixUserName, util.createRandomUserNumber(longUserName)).ToUpper();
                        existuser = BillingController.GetRadiusUser(username.ToUpper(), hotel.IdHotel);

                    } while (existuser.Enabled);

                    string password = util.CreateRandomPassword(longPassword).ToUpper();

                    transaction.UserName = username.ToUpper();
                    transaction.Password = password.ToUpper();
                    transaction.MAC = mac;
                    transaction.InvoiceNo = "";
                    transaction.UI = idhotel;
                    transaction.OS = "";

                    BillingController.SavePayPalTransaction(transaction);

                    response.IdTransaction = transaction.IdTransaction;
                    response.Description = bt.PayPalDescription;
                    response.Price = bt.Price.ToString();
                    response.Price = response.Price.Replace(',', '.');
                }
            }
            catch
            {

            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private UserResponse socialnetwork(HttpContext context)
        {
            CaptivePortalLog log = new CaptivePortalLog();
            UserResponse response = new UserResponse();
            try
            {
                string mail = (context.Request["email"] == null ? string.Empty : context.Request["email"].ToString());

                string idhotel = (context.Request["idhotel"] == null ? string.Empty : context.Request["idhotel"].ToString());
                string mac = (context.Request["mac"] == null ? string.Empty : context.Request["mac"].ToString());
                string idlocation = (context.Request["idlocation"] == null ? string.Empty : context.Request["idlocation"].ToString());

                string lang = (context.Request["lang"] == null ? string.Empty : context.Request["lang"].ToString());
                string idpromotion = (context.Request["idpromotion"] == null ? string.Empty : context.Request["idpromotion"].ToString());
                string survey = (context.Request["survey"] == null ? string.Empty : context.Request["survey"].ToString());
                string origin = (context.Request["origin"] == null ? string.Empty : context.Request["origin"].ToString());
                string idbillingtype = (context.Request["idbillingtype"] == null ? string.Empty : context.Request["idbillingtype"].ToString());

                log.NSEId = string.Format("Meraki - Idhotel:{0}", idhotel);
                log.CallerID = mac;
                log.Date = DateTime.Now.ToUniversalTime();
                log.UserName = "";
                log.Password = "";
                log.Page = "socialnetworkmegusta";
                log.IdLocation = Int32.Parse(idlocation);
                log.IdHotel = Int32.Parse(idhotel);
                log.RadiusResponse = "NOT APPLICABLE";
                log.NomadixResponse = "NOT APPLICABLE";

                Hotels hotel = BillingController.GetHotel(Int32.Parse(idhotel));
                Locations2 location = BillingController.ObtainLocation(Int32.Parse(idlocation));

                
                BillingTypes billingType = BillingController.ObtainBillingType(Int32.Parse(idbillingtype));

                bool allow = true;
                Users lastUser = BillingController.GetRadiusUsermac(mac, hotel.IdHotel);
                if (!lastUser.IdUser.Equals(0))
                {
                    BillingTypes bt = BillingController.ObtainBillingType(lastUser.IdBillingType);
                    if ((bt.IdBillingModule.Equals(1) || bt.IdBillingModule.Equals(3)) && lastUser.ValidSince > DateTime.Now.ToUniversalTime().AddHours(-hotel.FreeAccessRepeatTime))
                        allow = false;
                }

                if (allow)
                {

                    Rooms room = BillingController.GetRoom(Int32.Parse(idhotel), "DEFAULT");

                    Users user = null;

                    bool existe = false;

                    if (mail.ToUpper().Equals("UNDEFINED") || string.IsNullOrEmpty(mail))  //EL USUARIO NO HA CONCEDIDO SU CORREO ELECTRÓNICO
                    {
                        mail = string.Format("{0}{1}", "SN", util.createRandomUserNumber(8)).ToUpper(); //GENERAMOS UN USUARIO NUEVO
                        existe = true;

                        do
                        {
                            user = BillingController.GetRadiusUser(mail.ToUpper(), hotel.IdHotel);  //COMPROBAMOS QUE NO EXISTA EN LA BASE DE DATOS
                            if (user.IdUser.Equals(0))
                                existe = false; // SI NO EXISTE, CONTINUAMOS 
                            else
                            {
                                if (user.Enabled) // SI EXISTE Y ESTÁ ACTIVO, GENERAMOS UNO NUEVO Y EMPEZAMOS EL PROCESO DE NUEVO
                                {
                                    mail = string.Format("{0}{1}", "SN", util.createRandomUserNumber(8)).ToUpper();
                                }
                                else
                                {//SI NO ESTÁ ACTIVO, CONTINUAMOS CON USUARIO GENERADO Y CREAMOS UN USUARIO NUEVO
                                    existe = false;
                                    user = new Users();
                                }
                            }

                        } while (existe);
                    }
                    else
                        user = BillingController.GetRadiusUser(mail.ToUpper(), hotel.IdHotel);

                    if (user.IdUser.Equals(0))
                        user = new Users();

                    user.IdHotel = Int32.Parse(idhotel);
                    user.IdRoom = room.IdRoom;
                    user.IdBillingType = billingType.IdBillingType;
                    user.BWDown = billingType.BWDown;
                    user.BWUp = billingType.BWUp;
                    user.ValidSince = DateTime.Now.ToUniversalTime(); // bill.BillingDate;
                    user.ValidTill = DateTime.Now.ToUniversalTime().AddHours(billingType.ValidTill); // bill.BillingDate.AddHours(billingType.ValidTill);
                    user.TimeCredit = billingType.TimeCredit;
                    user.MaxDevices = billingType.MaxDevices;
                    user.Priority = billingType.IdPriority;
                    user.VolumeDown = billingType.VolumeDown;
                    user.VolumeUp = billingType.VolumeUp;
                    user.Enabled = true;
                    user.Name = mail.ToUpper();
                    user.Password = mail.ToUpper();
                    user.CHKO = true;
                    user.BuyingFrom = 1;
                    user.BuyingCallerID = mac;
                    user.Comment = string.Empty;
                    user.IdLocation = location.IdLocation;
                    user.Filter_Id = billingType.Filter_Id;

                    Loyalties2 loyalty = new Loyalties2();
                    loyalty.IdHotel = user.IdHotel;
                    loyalty.Name = string.Empty;
                    loyalty.Surname = string.Empty;
                    loyalty.Email = mail;
                    loyalty.Date = DateTime.Now.ToUniversalTime();
                    loyalty.Survey = survey;
                    loyalty.CallerId = mac;
                    loyalty.UserName = user.Name;

                    BillingController.SaveLoyalty(loyalty);

                    BillingController.SaveUser(user);

                    SitesGMT siteGMT = BillingController.SiteGMTSitebyLocation(location.IdLocation);
                    if (siteGMT.Id.Equals(0))
                        siteGMT = BillingController.SiteGMTSitebySite(location.IdHotel);

                    response.IdUser = 1;
                    response.UserName = user.Name;
                    response.Password = user.Password;

                    TimeSpan diferencia = user.ValidTill - user.ValidSince;

                    if (diferencia.TotalSeconds > user.TimeCredit)
                        response.ValidTill = user.ValidSince.AddSeconds(user.TimeCredit).AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                    else
                        response.ValidTill = user.ValidSince.AddSeconds(diferencia.TotalSeconds).AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");


                    if (string.IsNullOrEmpty(billingType.UrlLanding))
                    {
                        if (string.IsNullOrEmpty(location.UrlLanding))
                            response.Url = hotel.UrlHotel;
                        else
                            response.Url = location.UrlLanding;
                    }
                    else
                        response.Url = billingType.UrlLanding;

                    //DEMO
                    //response.Url = location.UrlLanding;

                    if (string.IsNullOrEmpty(lang))
                        lang = "en";
                    //log.RadiusResponse = "OK";
                    util.sendMail(user.IdHotel, user.IdBillingType, user.Name, user.Name.ToUpper(), user.Password.ToUpper(), lang.Substring(0, 2));
                }
                else
                {
                    response.IdUser = -1;
                    log.NomadixResponse = "Only one free access in hotel period";
                    log.RadiusResponse = "Only one free access in hotel period";
                }
            }
            catch (Exception ex)
            {
                if (ex.GetType().Name.ToUpper().Contains("URI"))
                {
                    log.NomadixResponse = "ERROR - NOMADIX NO COMUNICATIONS";
                    log.RadiusResponse = "NOT APPLICABLE";
                }
                else
                {
                    log.NomadixResponse = "OK";
                    log.RadiusResponse = "OK";

                    CaptivePortalLogInternal internalLog = new CaptivePortalLogInternal();
                    internalLog.NSEId = log.NSEId;
                    internalLog.CallerID = log.CallerID;
                    internalLog.Date = DateTime.Now.ToUniversalTime();
                    internalLog.UserName = "";
                    internalLog.Password = "";
                    internalLog.Page = "socialnetwork";
                    internalLog.IdLocation = 0;
                    internalLog.NomadixResponse = "ERROR NO CONTEMPLADO - " + ex.Message;
                    internalLog.RadiusResponse = "OK";

                    BillingController.SaveCaptivePortalLogInternal(internalLog);
                }

            }
            BillingController.SaveCaptivePortalLog(log);

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private void username(HttpContext context)
        {
            string json = string.Empty;
            try
            {
                string username = (context.Request["u"] == null ? string.Empty : context.Request["u"].ToString());
                string idhotel = (context.Request["idhotel"] == null ? string.Empty : context.Request["idhotel"].ToString());

                Hotels hotel = BillingController.GetHotel(Int32.Parse(idhotel));
                Users user = BillingController.GetRadiusUser(username, hotel.IdHotel);

                if (user.Enabled)
                    json = "{\"code\":\"ERROR\",\"message\":\"UserName not avalaible\"}";
                else
                    json = "{\"code\":\"OK\",\"message\":\"UserName it is free\"}";
            }
            catch (Exception ex)
            {
                json = "{\"code\":\"ERROR\",\"message\":\" " + ex.Message + ".\"}";
            }
            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();
        }

        private void voucher(HttpContext context)
        {
            string json = string.Empty;
            CaptivePortalLog log = new CaptivePortalLog();
            try
            {
                string username = (context.Request["username"] == null ? string.Empty : context.Request["username"].ToString());
                string idhotel = (context.Request["idhotel"] == null ? string.Empty : context.Request["idhotel"].ToString());
                string idlocation = (context.Request["idlocation"] == null ? string.Empty : context.Request["idlocation"].ToString());
                string mac = (context.Request["mac"] == null ? string.Empty : context.Request["mac"].ToString());

                log.NSEId = string.Format("Meraki - Idhotel:{0}", idhotel);
                log.CallerID = mac;
                log.Date = DateTime.Now.ToUniversalTime();
                log.UserName = username.ToUpper();
                log.Password = username.ToUpper();
                log.Page = "voucher";
                log.IdLocation = Int32.Parse(idlocation);
                log.IdHotel = Int32.Parse(idhotel);
                log.RadiusResponse = string.Empty;
                log.NomadixResponse = string.Empty;

                Hotels hotel = BillingController.GetHotel(Int32.Parse(idhotel));
                Locations2 location = BillingController.ObtainLocation(Int32.Parse(idlocation));
                Users user = BillingController.GetRadiusUser(username, hotel.IdHotel);

                if (!user.IdUser.Equals(0)) { 
                    SitesGMT siteGMT = BillingController.SiteGMTSitebyLocation(location.IdLocation);
                    if (siteGMT.Id.Equals(0))
                        siteGMT = BillingController.SiteGMTSitebySite(location.IdHotel);
                    string allow = "allow";

                    if (hotel.CustomAccessModule)
                    {
                        CustomAccess ca = BillingController.GetCustomAccess(hotel.IdHotel);
                    }

                    if (allow.ToUpper().Equals("ALLOW"))
                    {
                        string validtill = string.Empty;
                        TimeSpan diferencia = user.ValidTill - user.ValidSince;

                        if (diferencia.TotalSeconds > user.TimeCredit)
                            validtill = user.ValidSince.AddSeconds(user.TimeCredit).AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                        else
                            validtill = user.ValidSince.AddSeconds(diferencia.TotalSeconds).AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");

                        if (!user.IdUser.Equals(0))
                        {
                            BillingTypes bt = BillingController.ObtainBillingType(user.IdBillingType);

                            if (!string.IsNullOrEmpty(bt.UrlLanding))
                                json = "{\"code\":\"OK\",\"message\":\"" + bt.UrlLanding + "\",\"ValidTill\":\"" + validtill + "\"}";
                            else
                            {
                                if (!string.IsNullOrEmpty(location.UrlLanding))
                                    json = "{\"code\":\"OK\",\"message\":\"" + location.UrlLanding + "\",\"ValidTill\":\"" + validtill + "\"}";
                                else
                                    json = "{\"code\":\"OK\",\"message\":\"" + hotel.UrlHotel + "\",\"ValidTill\":\"" + validtill + "\"}";

                            }
                        }
                        else
                        {
                            json = "{\"code\":\"OK\",\"message\":\"" + hotel.UrlHotel + "\",\"ValidTill\":\"" + validtill + "\"}";
                        }
                    }
                    else
                    {
                        json = "{\"code\":\"ERROR\",\"message\":\"" + allow + "\"}";
                        log.NomadixResponse = "WS ERROR RESPONSE - " + allow;
                        log.RadiusResponse = "NOT APPLICABLE";
                    }

                    //VALIDAMOS EL USERAGENT
                    UserAgentsTemp temp = BillingController.GetUserAgentTemp(mac);
                    if (temp.IdTemp != 0)
                    {
                        UserAgents userAgent = BillingController.GetUserAgent(temp.IdUserAgent);
                        List<UserAgents> listUA = BillingController.GetUserAgents(userAgent.String);

                        userAgent.Valid = userAgent.Valid + 1; ;
                        BillingController.SaveUserAgent(userAgent);
                        foreach (UserAgents x in listUA)
                        {
                            x.Valid = x.Valid + 1; ;
                            BillingController.SaveUserAgent(x);
                        }                    
                    }

                    //METEMOS LOS USERAGENTS EN LA LISTA NEGRA
                    List<UserAgents> uAtoBlackList = BillingController.GetUserAgentsToBlackList(Int32.Parse(ConfigurationManager.AppSettings["MaxAttemps"].ToString()));
                    foreach (UserAgents uA in uAtoBlackList)
                    {
                        if (uA.Valid.Equals(0))
                        {
                            BlackListUserAgents bUA = BillingController.GetBlackUserAgent(uA.String);
                            if (bUA.IdBlackUserAgent.Equals(0))
                            {
                                BlackListUserAgents obj = new BlackListUserAgents();
                                obj.String = uA.String;
                                BillingController.SaveBlackListUserAgent(obj);
                            }
                        }
                    }

                    BillingController.SaveCaptivePortalLog(log);

                }
                else
                {
                    json = "{\"code\":\"OK\",\"message\":\" Invalid User.\"}";
                }
            }
            catch (Exception ex)
            {
                json = "{\"code\":\"ERROR\",\"message\":\" " + ex.Message + ".\"}";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();
        }


        private DisclaimerResult disclaimer(HttpContext context)
        {
            DisclaimerResult response = new DisclaimerResult();
            try
            {
                string idhotel = (context.Request["idhotel"] == null ? string.Empty : context.Request["idhotel"].ToString());
                string idlocation = (context.Request["idlocation"] == null ? string.Empty : context.Request["idlocation"].ToString());
                string lang = (context.Request["lang"] == null ? string.Empty : context.Request["lang"].ToString());
                string type = (context.Request["type"] == null ? string.Empty : context.Request["type"].ToString());

                Hotels hotel = BillingController.GetHotel(Int32.Parse(idhotel));

                Languages language = BillingController.GetLanguage(lang);
                if (language.IdLanguage.Equals(0))
                    language.IdLanguage = hotel.IdLanguage;

                Disclaimers dis = BillingController.GetDisclaimer(hotel.IdHotel, Int32.Parse(idlocation), language.IdLanguage, Int32.Parse(type));
                if (dis.IdDisclaimer != 0)
                    response.Text = dis.Text;
            }
            catch
            {
                ;
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private DisclaimerResult avisolegal(HttpContext context)
        {
            DisclaimerResult response = new DisclaimerResult();
            try
            {
                string lang = (context.Request["lang"] == null ? string.Empty : context.Request["lang"].ToString());

                Disclaimers dis = BillingController.GetDisclaimer(0, 0, 0, 0);
                if (dis.IdDisclaimer != 0)
                    response.Text = dis.Text;

            }
            catch
            {
                ;
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private UserResponse getuser(HttpContext context)
        {
            UserResponse response = new UserResponse();
            try
            {
                string username = (context.Request["username"] == null ? string.Empty : context.Request["username"].ToString());
                string mac = (context.Request["mac"] == null ? string.Empty : context.Request["mac"].ToString());
                string idhotel = (context.Request["idhotel"] == null ? string.Empty : context.Request["idhotel"].ToString());
                string idlocation = (context.Request["idlocation"] == null ? string.Empty : context.Request["idlocation"].ToString());

                SitesGMT siteGMT = GetSiteGMt(idlocation,idhotel);

                Users user = BillingController.GetRadiusUser(username, int.Parse(idhotel));
                if (user.IdUser != 0)
                {
                    BillingTypes2 billingtype = BillingController.ObtainBillingType2(user.IdBillingType);
                    response.UserName = user.Name;
                    response.Password = user.Password;

                    List<ClosedSessions2> clossedSessions = BillingController.GetClosedSession(int.Parse(idhotel), username, user.ValidSince, user.ValidTill);

                    response.ValidTill = user.ValidTill.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");

                    TimeSpan t = TimeSpan.FromSeconds(user.TimeCredit / user.MaxDevices);
                    string time = string.Empty;
                    if (user.TimeCredit >= 86400)
                        time = string.Format("{0} d: {1:D2} h: {2:D2} m: {3:D2} s", t.Days, t.Hours, t.Minutes, t.Seconds);
                    else
                        time = string.Format("{0:D2} h: {1:D2} m: {2:D2} s", t.Hours, t.Minutes, t.Seconds);
                    response.TimeCredit = time;
                    response.VolumeDown = string.Format("{0:0.00} MB", Math.Round(double.Parse((user.VolumeDown/1048576).ToString()),2));
                    response.VolumeUp = string.Format("{0:0.00} MB", Math.Round(double.Parse((user.VolumeUp / 1048576).ToString()), 2));
                    response.AccessType = billingtype.Description;
                    response.Url = string.Empty;
                }
            }
            catch (Exception ex)
            {
                ;
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private void saveLogoutUrl(HttpContext context, Dictionary<string, string> sData)
        {
            string json = string.Empty;
            try
            {
                //OBTENEMOS EL TIPO DE USUARIOS
                string mac = sData["mac"];
                string user = sData["user"];
                string url = sData["url"];
                string idhotel = sData["idhotel"];
                string idlocation = sData["idlocation"];

                MerakiLogoutUrl obj = new MerakiLogoutUrl();
                obj.Idhotel = Int32.Parse(idhotel);
                obj.Idlocation = Int32.Parse(idlocation);
                obj.MAC = mac;
                obj.Url = url;
                obj.DateInserted = DateTime.Now.ToUniversalTime();
                obj.IP = context.Request.UserHostAddress;
                obj.UserName = user;



                //if (BillingController.SaveMerakiLogoutUrl(obj))
                //    json = "{\"code\":\"OK\",\"message\":\" \"}";
                //else
                //    json = "{\"code\":\"ERROR\",\"message\":\"Se ha producido un error en el envio. Inténtelo más tarde.\"}";
            }
            catch
            {
                json = "{\"code\":\"ERROR\",\"message\":\"Se ha producido un error en el envio. Inténtelo más tarde.\"}";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();


        }

        private void deleteremember(HttpContext context)
        {
            string json = string.Empty;
            CaptivePortalLog log = new CaptivePortalLog();
            try
            {
                string username = (context.Request["username"] == null ? string.Empty : context.Request["username"].ToString());
                string mac = (context.Request["mac"] == null ? string.Empty : context.Request["mac"].ToString());
                string idhotel = (context.Request["idhotel"] == null ? string.Empty : context.Request["idhotel"].ToString());
                string idlocation = (context.Request["idlocation"] == null ? string.Empty : context.Request["idlocation"].ToString());

                BillingController.DeleteRememberMeTableUser(mac, int.Parse(idhotel));

                json = "{\"code\":\"OK\",\"message\":\"\"}";
            }
            catch (Exception ex)
            {
                json = "{\"code\":\"ERROR\",\"message\":\" " + ex.Message + ".\"}";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();
        }

        public void ProcessEmtpy(HttpContext context)
        {
            // Set the content type and encoding for JSON
            context.Response.ContentType = "application/json";
            context.Response.ContentEncoding = Encoding.UTF8;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private static SitesGMT GetSiteGMt(string idlocation, string idhotel)
        {
            SitesGMT siteGMT = null;
            Locations2 location = null;
            Hotels hotel = null;
            try
            {
                if (!string.IsNullOrEmpty(idlocation))
                {
                    location = BillingController.GetLocation(int.Parse(idlocation));
                    hotel = BillingController.GetHotel(location.IdHotel);
                    siteGMT = BillingController.SiteGMTSitebyLocation(location.IdLocation);
                    if (siteGMT.Id.Equals(0))
                        siteGMT = BillingController.SiteGMTSitebySite(location.IdHotel);
                }
                else
                {
                    siteGMT = BillingController.SiteGMTSitebySite(int.Parse(idhotel));
                }

                return siteGMT;
            }
            catch (Exception ex)
            {
                return null;
            }


        }
    }
}