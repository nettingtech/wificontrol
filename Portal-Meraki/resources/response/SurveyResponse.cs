﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.resources.response
{
    public class SurveyResponse
    {
        public int IdSurvey { get; set; }
        public int IdHotel { get; set; }
        public string Question { get; set; }
        public string Type { get; set; }
        public string Values { get; set; }
        public int Order { get; set; }
    }
}