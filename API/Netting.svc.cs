﻿using API.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Wifi360.Data.Controllers;
using Wifi360.Data.Model;

namespace API
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código, en svc y en el archivo de configuración.
    // NOTE: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Service1.svc o Service1.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Netting : INetting
    {

        public SiteApi GetSiteInfo(string token, int? location = 0)
        {
            SiteApi response = new SiteApi();
            ApiTokens ApiToken = BillingController.SelectAccessWithToken(token);
            if (!ApiToken.Id.Equals(0))
            {
                if (ApiToken.Read)
                {
                    Hotels h = BillingController.GetHotel(ApiToken.IdSite);
                    response.IdSite = h.IdHotel;
                    response.Name = h.Name;
                    response.Latitude = h.Latitude;
                    response.Longitude = h.Longitude;
                    response.Locations = new List<LocationApi>();
                    if (location.HasValue)
                    {
                        CargarLocation(location.Value, response);
                    }
                    else
                    {
                        List<Locations2> locations = BillingController.GetLocations(h.IdHotel);
                        foreach (Locations2 obj in locations)
                            CargarLocation(obj.IdLocation, response);
                    }
                    if (response.Locations.Count().Equals(0))
                    {
                        response.Error = "0x00006";
                        response.RespuestaError = "Invalid Location";
                    }
                }
                else
                {
                    response.Error = "0x00002";
                    response.RespuestaError = "Invalid Permission";
                }

            }
            else
            {
                response.Error = "0x00001";
                response.RespuestaError = "Invalid Token";
            }
            return response;
        }

        private static void CargarLocation(int location, SiteApi response)
        {
            try
            {
                Locations2 loc = BillingController.GetLocation(location);
                if (!loc.IdLocation.Equals(0))
                {
                    LocationApi locApi = new LocationApi();
                    Utils.Map(loc, locApi);
                    List<BillingTypes> billingTypes = BillingController.ObtainBillingTypesLocation(loc.IdLocation);
                    locApi.billingTypes = new List<BillingTypesApi>();
                    foreach (BillingTypes obj in billingTypes)
                    {
                        BillingTypesApi b = new BillingTypesApi();
                        Utils.Map(obj, b);
                        locApi.billingTypes.Add(b);
                    }
                    response.Locations.Add(locApi);
                }
            }
            catch(Exception ex)
            {
                throw;
            }
        }

        //public List<BillingTypesApi> GetBillingTypes(string token, int? location = 0)
        //{
        //    List<BillingTypesApi> response = new List<BillingTypesApi>();
        //    ApiTokens ApiToken = BillingController.SelectAccessWithToken(token);
        //    if (!ApiToken.Id.Equals(0))
        //    {
        //        if (ApiToken.Read)
        //        {
        //            if (location.HasValue)
        //            {
        //                List<BillingTypes> billingTypes = BillingController.ObtainBillingTypesLocation(location.Value);
        //            }
        //            else
        //            {
        //                //comprobamos si el usuario es de group, site o location
        //                List<BillingTypes> billingTypes = BillingController.ObtainBillingTypes(ApiToken.IdSite);
        //                foreach (BillingTypes obj in billingTypes)
        //                {

        //                }
        //            }
        //        }
        //    }
        //    return response;
        //}

        public UserApi CreateUser(string token, int AccessProfile, string AccessCode = "", string Username = "", string password = "", string ValidSince = "", string ValidTill = "")
        {
            UserApi response = new UserApi();
            ApiTokens ApiToken = BillingController.SelectAccessWithToken(token);
            if (!ApiToken.Id.Equals(0))
            {
                if (ApiToken.Write)
                {
                    //CultureInfo provider = CultureInfo.InvariantCulture;
                    //DateTime since = DateTime.MinValue;
                    //DateTime till = DateTime.MinValue;

                    //if ((string.IsNullOrEmpty(ValidSince) || DateTime.TryParseExact(ValidSince, "DD/MM/YYYY HH:mm:ss", provider, DateTimeStyles.None, out since)) 
                    //    || (string.IsNullOrEmpty(ValidTill) || DateTime.TryParseExact(ValidTill, "DD/MM/YYYY HH:mm:ss", provider, DateTimeStyles.None, out till))
                    //    || (DateTime.TryParseExact(ValidTill, "DD/MM/YYYY HH:mm:ss", provider, DateTimeStyles.None, out till) && DateTime.TryParseExact(ValidTill, "DD/MM/YYYY HH:mm:ss", provider, DateTimeStyles.None, out till) &&
                    //     DateTime.ParseExact(ValidSince, "DD/MM/YYYY HH:mm_ss", provider) < DateTime.ParseExact(ValidSince, "DD/MM/YYYY HH:mm_ss", provider))) {

                        BillingTypes billingType = BillingController.ObtainBillingType(AccessProfile);
                        if (!billingType.IdBillingType.Equals(0))
                        {
                            Locations2 location = BillingController.GetLocation(billingType.IdLocation);
                            Hotels hotel = BillingController.GetHotel(billingType.IdHotel);
                            FDSGroups GroupRealmShare = BillingController.GetGroupShareRealm(hotel.IdHotel);

                            List<ParametrosConfiguracion> parametros = BillingController.GetParametrosConfiguracion(hotel.IdHotel);
                            int longUserName = 3;
                            int longPassword = 3;
                            int longPassVoucher = 3;
                            string prefixUserName = string.Empty;
                            string prefixVoucher = string.Empty;

                            foreach (ParametrosConfiguracion obj in parametros)
                            {
                                switch (obj.Key.ToUpper())
                                {
                                    case "LONGUSERNAME": longUserName = Int32.Parse(obj.value); break;
                                    case "LONGPASSWORD": longPassword = Int32.Parse(obj.value); break;
                                    case "USERNAMEPREFIX": prefixUserName = obj.value; break;
                                    case "LONGVOUCHERUSER": longPassVoucher = Int32.Parse(obj.value); break;
                                    case "VOUCHERPREFIX": prefixVoucher = obj.value; break;
                                }
                            }

                            if (longPassword < 3)
                                longPassword = 3;
                            if (longUserName < 3)
                                longUserName = 3;
                            if (longPassVoucher < 3)
                                longPassVoucher = 3;

                            string listadoHoteles = string.Empty;
                            if (GroupRealmShare.IdGroup != 0)
                                listadoHoteles = ListadoHoteles(GroupRealmShare);

                            Users existuser = null;

                            if (string.IsNullOrEmpty(Username) && string.IsNullOrEmpty(AccessCode))
                            {
                                string prefix = billingType.IdBillingModule.Equals(6) ? prefixVoucher : prefixUserName;

                                string _user = string.Empty;
                                Users u2 = null;
                                do
                                {
                                    _user = string.Format("{0}{1}", prefix, Utils.createRandomUserNumber(longUserName));
                                    if (string.IsNullOrEmpty(listadoHoteles))
                                        u2 = BillingController.GetRadiusUser(_user, hotel.IdHotel);
                                    else
                                    {
                                        string[] ids = listadoHoteles.Split(',');
                                        foreach (string id in ids)
                                        {
                                            u2 = BillingController.GetRadiusUser(_user, Int32.Parse(id));
                                            if (u2.Enabled)
                                                break;
                                        }
                                    }
                                } while (u2.Enabled);


                                Users users = new Users();
                                SitesGMT siteGMT = GetSiteGMt(location, hotel);
                                //PARAMETRO OBLIGATORIOS

                                Rooms room = BillingController.GetRoom(hotel.IdHotel, "DEFAULT");

                                Users user = new Users();
                                if (!billingType.FreeAccess)
                                {
                                    Billings bill = new Billings();

                                    bill.IdHotel = hotel.IdHotel;
                                    bill.IdBillingType = billingType.IdBillingType;
                                    bill.IdRoom = room.IdRoom;
                                    bill.Amount = billingType.Price;
                                    bill.BillingDate = DateTime.Now.ToUniversalTime();
                                    bill.BillingCharge = DateTime.Now.ToUniversalTime();
                                    bill.Flag = 3;
                                    bill.IdLocation = location.IdLocation;

                                    BillingController.SaveBilling(bill);
                                }

                                user.IdHotel = hotel.IdHotel;
                                user.IdRoom = room.IdRoom;
                                user.IdBillingType = billingType.IdBillingType;
                                user.BWDown = billingType.BWDown;
                                user.BWUp = billingType.BWUp;
                                user.IdLocation = location.IdLocation;
                                user.Name = _user.ToUpper();
                                user.Password = billingType.IdBillingModule.Equals(6) ? _user.ToUpper() : Utils.CreateRandomPassword(longPassword);

                                user.ValidSince = string.IsNullOrEmpty(ValidSince) ? DateTime.Now.ToUniversalTime() : DateTime.Parse(ValidSince).AddSeconds(-siteGMT.gmtoffset);
                                user.ValidTill = string.IsNullOrEmpty(ValidTill) ? user.ValidSince.AddHours(billingType.ValidTill) : DateTime.Parse(ValidTill).AddSeconds(-siteGMT.gmtoffset);

                                //CALCULAMOS EL TIMECREDIT PARA RECORTARLO EN TAL CASO
                                int credit = billingType.TimeCredit / billingType.MaxDevices; //Calculamos el tiempo por dispositivo
                                if (user.ValidTill < user.ValidSince.AddSeconds(credit))
                                {
                                    TimeSpan diff = user.ValidTill - user.ValidSince;
                                    user.TimeCredit = billingType.MaxDevices * (int)diff.TotalSeconds;
                                }
                                else
                                    user.TimeCredit = billingType.TimeCredit;

                                user.MaxDevices = billingType.MaxDevices;
                                user.Priority = billingType.IdPriority;
                                user.VolumeDown = billingType.VolumeDown;
                                user.VolumeUp = billingType.VolumeUp;
                                user.Enabled = true;
                                user.CHKO = true;
                                user.BuyingFrom = 3;
                                user.BuyingCallerID = "NOT APPLICABLE";
                                user.Filter_Id = billingType.Filter_Id;
                                user.Comment = "CREATED THRU API";

                                BillingController.SaveUser(user);

                                response.Username = user.Name.ToUpper();
                                response.Password = user.Password.ToUpper();
                                response.IdUser = user.IdUser;
                                response.ValidSince = user.ValidSince.AddSeconds(siteGMT.gmtoffset);
                                response.ValidTill = user.ValidTill.AddSeconds(siteGMT.gmtoffset);
                                response.MaxDevices = user.MaxDevices;
                                response.BWDown = user.BWDown;
                                response.BWUp = user.BWUp;
                                response.VolumeDown = (user.VolumeDown / 1048576); //CONVERTIMOS LOS BYTES EN GB
                                response.VolumeUp = (user.VolumeUp / 1048576);
                                response.IdLocation = location.IdLocation;
                                response.IdSite = hotel.IdHotel;
                                TimeSpan t = TimeSpan.FromSeconds(user.TimeCredit / user.MaxDevices);
                                string time = string.Empty;
                                if (user.TimeCredit >= 86400)
                                    time = string.Format("{0}d: {1:D2}h: {2:D2} m: {3:D2}s", t.Days, t.Hours, t.Minutes, t.Seconds);
                                else
                                    time = string.Format("{0:D2}h: {1:D2}m: {2:D2}s", t.Hours, t.Minutes, t.Seconds);
                                response.TimeCredit = time;
                            }
                            else if (!string.IsNullOrEmpty(Username))
                            {
                                if (!billingType.IdBillingModule.Equals(6))
                                {

                                    if (string.IsNullOrEmpty(listadoHoteles))
                                        existuser = BillingController.GetRadiusUser(Username.ToUpper(), hotel.IdHotel);
                                    else
                                    {
                                        string[] ids = listadoHoteles.Split(',');
                                        foreach (string id in ids)
                                        {
                                            existuser = BillingController.GetRadiusUser(Username.ToUpper(), Int32.Parse(id));
                                            if (!existuser.IdUser.Equals(0) || existuser.Enabled)
                                                break;
                                        }
                                    }

                                    if (existuser.IdUser.Equals(0) || !existuser.Enabled) //el usuario no existe, continuamos
                                    {
                                        //creamos el usuario
                                        Users users = new Users();
                                        SitesGMT siteGMT = GetSiteGMt(location, hotel);
                                        //PARAMETRO OBLIGATORIOS

                                        Rooms room = BillingController.GetRoom(hotel.IdHotel, "DEFAULT");

                                        #region generación de ticket unitario

                                        Users user = new Users();
                                        if (!billingType.FreeAccess)
                                        {
                                            Billings bill = new Billings();

                                            bill.IdHotel = hotel.IdHotel;
                                            bill.IdBillingType = billingType.IdBillingType;
                                            bill.IdRoom = room.IdRoom;
                                            bill.Amount = billingType.Price;
                                            bill.BillingDate = DateTime.Now.ToUniversalTime();
                                            bill.BillingCharge = DateTime.Now.ToUniversalTime();
                                            bill.Flag = 3;
                                            bill.IdLocation = location.IdLocation;

                                            BillingController.SaveBilling(bill);
                                        }

                                        user.IdHotel = hotel.IdHotel;
                                        user.IdRoom = room.IdRoom;
                                        user.IdBillingType = billingType.IdBillingType;
                                        user.BWDown = billingType.BWDown;
                                        user.BWUp = billingType.BWUp;
                                        user.IdLocation = location.IdLocation;
                                        user.Name = Username.ToUpper();
                                        if (billingType.IdBillingModule.Equals(6))
                                            user.Password = Username.ToUpper();
                                        else
                                        {
                                            if (string.IsNullOrEmpty(password))
                                            {
                                                if (longPassword < 3)
                                                    longPassword = 3;

                                                user.Password = Utils.CreateRandomPassword(longPassword);
                                            }
                                            else
                                                user.Password = password.ToUpper();
                                        }

                                        user.ValidSince = string.IsNullOrEmpty(ValidSince) ? DateTime.Now.ToUniversalTime() : DateTime.Parse(ValidSince).AddSeconds(-siteGMT.gmtoffset);
                                        user.ValidTill = string.IsNullOrEmpty(ValidTill) ? user.ValidSince.AddHours(billingType.ValidTill) : DateTime.Parse(ValidTill).AddSeconds(-siteGMT.gmtoffset);

                                        //CALCULAMOS EL TIMECREDIT PARA RECORTARLO EN TAL CASO
                                        int credit = billingType.TimeCredit / billingType.MaxDevices; //Calculamos el tiempo por dispositivo
                                        if (user.ValidTill < user.ValidSince.AddSeconds(credit))
                                        {
                                            TimeSpan diff = user.ValidTill - user.ValidSince;
                                            user.TimeCredit = billingType.MaxDevices * (int)diff.TotalSeconds;
                                        }
                                        else
                                            user.TimeCredit = billingType.TimeCredit;

                                        user.MaxDevices = billingType.MaxDevices;
                                        user.Priority = billingType.IdPriority;
                                        user.VolumeDown = billingType.VolumeDown;
                                        user.VolumeUp = billingType.VolumeUp;
                                        user.Enabled = true;
                                        user.CHKO = true;
                                        user.BuyingFrom = 3;
                                        user.BuyingCallerID = "NOT APPLICABLE";
                                        user.Filter_Id = billingType.Filter_Id;
                                        user.Comment = "Buy from API";

                                        BillingController.SaveUser(user);

                                        response.Username = user.Name.ToUpper();
                                        response.Password = user.Password.ToUpper();
                                        response.IdUser = user.IdUser;
                                        response.ValidSince = user.ValidSince.AddSeconds(siteGMT.gmtoffset);
                                        response.ValidTill = user.ValidTill.AddSeconds(siteGMT.gmtoffset);
                                        response.MaxDevices = user.MaxDevices;
                                        response.BWDown = user.BWDown;
                                        response.BWUp = user.BWUp;
                                        response.VolumeDown = (user.VolumeDown / 1048576); //CONVERTIMOS LOS BYTES EN GB
                                        response.VolumeUp = (user.VolumeUp / 1048576);
                                        response.IdLocation = location.IdLocation;
                                        response.IdSite = hotel.IdHotel;

                                        TimeSpan t = TimeSpan.FromSeconds(user.TimeCredit / user.MaxDevices);
                                        string time = string.Empty;
                                        if (user.TimeCredit >= 86400)
                                            time = string.Format("{0}d: {1:D2}h: {2:D2} m: {3:D2}s", t.Days, t.Hours, t.Minutes, t.Seconds);
                                        else
                                            time = string.Format("{0:D2}h: {1:D2}m: {2:D2}s", t.Hours, t.Minutes, t.Seconds);
                                        response.TimeCredit = time;

                                        #endregion
                                    }
                                    else
                                    {
                                        response.Error = "0x00004";
                                        response.RespuestaError = "Invalid UserName";
                                    }
                                }
                                else
                                {
                                    response.Error = "0x00005";
                                    response.RespuestaError = "Invalid AccessProfile and UserName";
                                }

                            }
                            else if (!string.IsNullOrEmpty(AccessCode))
                            {
                                if (billingType.IdBillingModule.Equals(6))
                                {

                                    if (string.IsNullOrEmpty(listadoHoteles))
                                        existuser = BillingController.GetRadiusUser(AccessCode.ToUpper(), hotel.IdHotel);
                                    else
                                    {
                                        string[] ids = listadoHoteles.Split(',');
                                        foreach (string id in ids)
                                        {
                                            existuser = BillingController.GetRadiusUser(AccessCode.ToUpper(), Int32.Parse(id));
                                            if (!existuser.IdUser.Equals(0) || existuser.Enabled)
                                                break;
                                        }
                                    }

                                    if (existuser.IdUser.Equals(0) || !existuser.Enabled) //el usuario no existe, continuamos
                                    {
                                        //creamos el usuario
                                        Users users = new Users();
                                        SitesGMT siteGMT = GetSiteGMt(location, hotel);
                                        //PARAMETRO OBLIGATORIOS

                                        Rooms room = BillingController.GetRoom(hotel.IdHotel, "DEFAULT");

                                        #region generación de ticket unitario

                                        Users user = new Users();
                                        if (!billingType.FreeAccess)
                                        {
                                            Billings bill = new Billings();

                                            bill.IdHotel = hotel.IdHotel;
                                            bill.IdBillingType = billingType.IdBillingType;
                                            bill.IdRoom = room.IdRoom;
                                            bill.Amount = billingType.Price;
                                            bill.BillingDate = DateTime.Now.ToUniversalTime();
                                            bill.BillingCharge = DateTime.Now.ToUniversalTime();
                                            bill.Flag = 3;
                                            bill.IdLocation = location.IdLocation;

                                            BillingController.SaveBilling(bill);
                                        }

                                        user.IdHotel = hotel.IdHotel;
                                        user.IdRoom = room.IdRoom;
                                        user.IdBillingType = billingType.IdBillingType;
                                        user.BWDown = billingType.BWDown;
                                        user.BWUp = billingType.BWUp;
                                        user.IdLocation = location.IdLocation;
                                        user.Name = AccessCode.ToUpper();
                                        user.Password = AccessCode.ToUpper();

                                        user.ValidSince = string.IsNullOrEmpty(ValidSince) ? DateTime.Now.ToUniversalTime() : DateTime.Parse(ValidSince).AddSeconds(-siteGMT.gmtoffset);
                                        user.ValidTill = string.IsNullOrEmpty(ValidTill) ? user.ValidSince.AddHours(billingType.ValidTill) : DateTime.Parse(ValidTill).AddSeconds(-siteGMT.gmtoffset);

                                        //CALCULAMOS EL TIMECREDIT PARA RECORTARLO EN TAL CASO
                                        int credit = billingType.TimeCredit / billingType.MaxDevices; //Calculamos el tiempo por dispositivo
                                        if (user.ValidTill < user.ValidSince.AddSeconds(credit))
                                        {
                                            TimeSpan diff = user.ValidTill - user.ValidSince;
                                            user.TimeCredit = billingType.MaxDevices * (int)diff.TotalSeconds;
                                        }
                                        else
                                            user.TimeCredit = billingType.TimeCredit;

                                        user.MaxDevices = billingType.MaxDevices;
                                        user.Priority = billingType.IdPriority;
                                        user.VolumeDown = billingType.VolumeDown;
                                        user.VolumeUp = billingType.VolumeUp;
                                        user.Enabled = true;
                                        user.CHKO = true;
                                        user.BuyingFrom = 3;
                                        user.BuyingCallerID = "NOT APPLICABLE";
                                        user.Filter_Id = billingType.Filter_Id;
                                        user.Comment = "Buy from API";

                                        BillingController.SaveUser(user);

                                        response.Username = user.Name.ToUpper();
                                        response.Password = user.Password.ToUpper();
                                        response.IdUser = user.IdUser;
                                        response.ValidSince = user.ValidSince.AddSeconds(siteGMT.gmtoffset);
                                        response.ValidTill = user.ValidTill.AddSeconds(siteGMT.gmtoffset);
                                        response.MaxDevices = user.MaxDevices;
                                        response.BWDown = user.BWDown;
                                        response.BWUp = user.BWUp;
                                        response.VolumeDown = (user.VolumeDown / 1048576); //CONVERTIMOS LOS BYTES EN GB
                                        response.VolumeUp = (user.VolumeUp / 1048576);
                                        response.IdLocation = location.IdLocation;
                                        response.IdSite = hotel.IdHotel;

                                        TimeSpan t = TimeSpan.FromSeconds(user.TimeCredit / user.MaxDevices);
                                        string time = string.Empty;
                                        if (user.TimeCredit >= 86400)
                                            time = string.Format("{0}d: {1:D2}h: {2:D2} m: {3:D2}s", t.Days, t.Hours, t.Minutes, t.Seconds);
                                        else
                                            time = string.Format("{0:D2}h: {1:D2}m: {2:D2}s", t.Hours, t.Minutes, t.Seconds);
                                        response.TimeCredit = time;

                                        #endregion
                                    }
                                    else
                                    {
                                        response.Error = "0x00004";
                                        response.RespuestaError = "Invalid UserName";
                                    }
                                }
                                else
                                {
                                    response.Error = "0x00005";
                                    response.RespuestaError = "Invalid AccessProfile and UserName";
                                }

                            }
                        //}
                        //else
                        //{
                        //    response.Error = "0x00007";
                        //    response.RespuestaError = "Invalid Date";
                        //}
                    }
                    else
                    {
                        response.Error = "0x00003";
                        response.RespuestaError = "Invalid AccessProfile";
                    }
                }
                else
                {
                    response.Error = "0x00002";
                    response.RespuestaError = "Invalid Permission";
                }
            }
            else
            {
                response.Error = "0x00001";
                response.RespuestaError = "Invalid Token";
            }
            return response;

        }

        private static SitesGMT GetSiteGMt(Locations2 location, Hotels hotel)
        {
            SitesGMT siteGMT = null;
            try
            {
                hotel = BillingController.GetHotel(location.IdHotel);
                siteGMT = BillingController.SiteGMTSitebyLocation(location.IdLocation);
                if (siteGMT.Id.Equals(0))
                    siteGMT = BillingController.SiteGMTSitebySite(location.IdHotel);

                return siteGMT;
            }
            catch (Exception ex)
            {
                return null;
            }


        }

        private static string ListadoHoteles(FDSGroups group)
        {
            if (group != null)
            {
                List<Hotels> hoteles = new List<Hotels>();
                List<FDSGroupsMembers> members = BillingController.SelectFDSGroupsMembers(group.IdGroup);
                for (int i = 0; i < members.Count; i++)
                {
                    switch (members[i].IdMemberType)
                    {
                        //tipo grupo
                        case 1:
                            List<FDSGroupsMembers> auxs = BillingController.SelectFDSGroupsMembers(members[i].IdMember);
                            foreach (FDSGroupsMembers aux in auxs)
                                members.Add(aux);
                            break;
                        //tipo site
                        case 2:
                            hoteles.Add(BillingController.GetHotel(members[i].IdMember));
                            break;
                        //tipo location
                        case 3: break;

                    }
                }

                string listadosHoteles = string.Empty;
                foreach (Hotels h in hoteles)
                    listadosHoteles += string.Format("{0},", h.IdHotel);
                listadosHoteles = listadosHoteles.Substring(0, listadosHoteles.Length - 1);
                return listadosHoteles;
            }
            else
                return string.Empty;
        }
    }
}