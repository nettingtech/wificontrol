﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace API
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IService1" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface INetting
    {
        [OperationContract]
        SiteApi GetSiteInfo(string token, int? location = 0);

        //[OperationContract]
        //List<BillingTypesApi> GetBillingTypes(string token, int? location = 0);

        [OperationContract]
        UserApi CreateUser(string token, int AccessProfile, string AccessCode = "", string Username = "", string password = "", string ValidSince = "", string ValidTill = "");

        // TODO: agregue aquí sus operaciones de servicio
    }


    [DataContract]
    public class Base
    {
        [DataMember]
        public string Error { get; set; }

        [DataMember]
        public string RespuestaError { get; set; }
    }

    [DataContract]
    public class SiteApi : Base
    {
        [DataMember]
        public int IdSite { get; set; }
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Latitude { get; set; }

        [DataMember]
        public string Longitude { get; set; }

        [DataMember]
        public List<LocationApi> Locations { get; set; }
    }

    [DataContract]
    public class LocationApi : Base
    {
        [DataMember]
        public int IdLocation { get; set; }
        [DataMember]
        public int IdHotel { get; set; }

        [DataMember]
        public string Value { get; set; }
        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string UrlLanding { get; set; }

        [DataMember]
        public int DefaultModule { get; set; }

        [DataMember]
        public bool Disclaimer { get; set; }

        [DataMember]
        public bool Default { get; set; }

        [DataMember]
        public string Currency { get; set; }

        [DataMember]
        public float CurrencyExchange { get; set; }

        [DataMember]
        public string Latitude { get; set; }

        [DataMember]
        public string Longitude { get; set; }

        [DataMember]
        public bool LoginModule { get; set; }

        [DataMember]
        public bool VoucherModule { get; set; }

        [DataMember]
        public bool FreeAccessModule { get; set; }

        [DataMember]
        public bool CustomAccessModule { get; set; }

        [DataMember]
        public bool PayAccessModule { get; set; }

        [DataMember]
        public bool SocialNetworksModule { get; set; }

        [DataMember]
        public List<BillingTypesApi> billingTypes { get; set; }
    }

    [DataContract]
    public class BillingTypesApi : Base
    {
        [DataMember]
        public int IdBillingType { get; set; }

        [DataMember]
        public int IdHotel { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public float Price { get; set; }

        [DataMember]
        public int TimeCredit { get; set; }

        [DataMember]
        public int MaxDevices { get; set; }

        [DataMember]
        public int ValidTill { get; set; }

        [DataMember]
        public long BWDown { get; set; }

        [DataMember]
        public long BWUp { get; set; }

        [DataMember]
        public int IdPriority { get; set; }

        [DataMember]
        public bool FreeAccess { get; set; }

        [DataMember]
        public bool Visible { get; set; }

        [DataMember]
        public int Order { get; set; }

        [DataMember]
        public bool Enabled { get; set; }

        [DataMember]
        public long VolumeUp { get; set; }

        [DataMember]
        public long VolumeDown { get; set; }

        [DataMember]
        public int IdLocation { get; set; }

        [DataMember]
        public string PayPalDescription { get; set; }

        [DataMember]
        public int IdBillingModule { get; set; }

        [DataMember]
        public bool DefaultFastTicket { get; set; }

        [DataMember]
        public string UrlLanding { get; set; }

        [DataMember]
        public int FilterId { get; set; }

        [DataMember]
        public bool IoT { get; set; }

        [DataMember]
        public int NextBillingType { get; set; }


    }

    [DataContract]
    public class UserApi : Base
    {
        [DataMember]
        public int IdUser { get; set; }

        [DataMember]
        public int IdSite { get; set; }

        [DataMember]
        public int IdBillingType { get; set; }

        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public DateTime ValidSince { get; set; }

        [DataMember]
        public DateTime ValidTill { get; set; }

        [DataMember]
        public int MaxDevices { get; set; }

        [DataMember]
        public string TimeCredit { get; set; }

        [DataMember]
        public long BWDown { get; set; }

        [DataMember]
        public long BWUp { get; set; }

        [DataMember]
        public long VolumeUp { get; set; }

        [DataMember]
        public long VolumeDown { get; set; }

        [DataMember]
        public int IdLocation { get; set; }


    }

}
