﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace API.Common
{
    public class Utils
    {
        public static void Map(object source, object destiny)
        {
            foreach (PropertyInfo infoSource in source.GetType().GetProperties())
            {
                if (infoSource.CanRead)
                {
                    PropertyInfo infoDestiny = destiny.GetType().GetProperty(infoSource.Name);

                    if (infoDestiny != null)
                    {
                        if ((infoDestiny.CanWrite) && (infoSource.PropertyType == infoDestiny.PropertyType))
                        {
                            object value = infoSource.GetValue(source, null);
                            infoDestiny.SetValue(destiny, value, null);
                        }
                    }
                }
            }
        }

        public static string createRandomUserNumber(int Length)
        {
            string _allowedChars = "ABCDEFGHJKLMNPQRSTUVWXYZ23456789";
            Random randNum = new Random(Guid.NewGuid().GetHashCode());
            char[] chars = new char[Length];
            int allowedCharCount = _allowedChars.Length;

            for (int i = 0; i < Length; i++)
            {
                chars[i] = _allowedChars[(int)((_allowedChars.Length) * randNum.NextDouble())];
            }

            return new string(chars);
        }

        public static string CreateRandomPassword(int PasswordLength)
        {
            string _allowedChars = "23456789";
            Random randNum = new Random(Guid.NewGuid().GetHashCode());
            char[] chars = new char[PasswordLength];
            int allowedCharCount = _allowedChars.Length;

            for (int i = 0; i < PasswordLength; i++)
            {
                chars[i] = _allowedChars[(int)((_allowedChars.Length) * randNum.NextDouble())];
            }

            return new string(chars);
        }
    }
}