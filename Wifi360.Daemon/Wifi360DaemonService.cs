﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Wifi360.Daemon.Common;
using Wifi360.Data.Controllers;
using Wifi360.Data.Model;

namespace Wifi360.Daemon
{
    partial class Wifi360DaemonService : ServiceBase
    {
        #region Variables Privadas y Propiedades

        private bool firstRun;          // 'Flag' para controlar si es el primer 'tick' o no

        private System.Timers.Timer timer = null;

        private bool isExecuting;
        private Guid idthread = Guid.Empty;

        #endregion Variables Privadas y Propiedades

        #region metodos servicio y timer

        public Wifi360DaemonService()
        {
            InitializeComponent();

           // Thread.Sleep(20000);

            firstRun = true;
            timer = new System.Timers.Timer();
            timer.Interval = Int32.Parse(ConfigurationManager.AppSettings["TimerInterval"].ToString()) * 1000;
            timer.Elapsed += new ElapsedEventHandler(TimerElapsed);

            idthread = Guid.NewGuid();
            Context.Log = //new Log(Context.AppName, ELogDest.File);
                new Log(Context.AppName, AppDomain.CurrentDomain.BaseDirectory, ELogType.Debug, idthread);
        }

        protected override void OnStart(string[] args)
        {
            // TODO: agregar código aquí para iniciar el servicio.
            if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "Se inicia el servicio");
            if (firstRun)
            {
                firstRun = false;
                TimerElapsed(null, null);
            }

            timer.Start();
        }

        protected override void OnStop()
        {
            // TODO: agregar código aquí para realizar cualquier anulación necesaria para detener el servicio.
            if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "Se para el servicio");
            timer.Stop();
        }

        private void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            // Para poder editar el app.config en caliente
            try
            {
                ConfigurationManager.RefreshSection("connectionStrings");
                ConfigurationManager.RefreshSection("appSettings");
                idthread = Guid.NewGuid();

                //        Thread.Sleep(10000);

                if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "**Empieza el proceso**");
                if (!isExecuting)
                {
                    isExecuting = true;

                    if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "Conectamos BD y descargamos entradas sin procesar.");
                    List<RadiusCoa> radius = BillingController.SelectRadiusCoaUnProcessed();
                    if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "Entradas sin procesar: " + radius.Count());

                    foreach (RadiusCoa obj in radius)
                    {
                        CoA_Types coa_type = BillingController.SelectCoA_Type(obj.CoAType);
                        CoA_Vendors coa_vendor = BillingController.SelectCoA_Vendor(obj.CoAVendor);
                        string lineLog = string.Format("IdHotel: {0} | UserName: {1} | ActiveSessionID: {2} | Nas-IP: {3} | CoA_Type: {4} | CoA_Vendor : {5}", obj.IdHotel, obj.UserName, obj.AcctSessionID, obj.NAS, coa_type.Action, coa_vendor.Name);
                        if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "Entrada: " + lineLog);

                        string urlCoa = string.Empty;
                        switch (coa_vendor.IdCoAVendors)
                        {
                            case 1:
                                urlCoa = string.Format("http://{0}:{1}/disconnect?Acct-Session-Id={2}&Nas-Ip-address={3}&Framed-ip-address={4}", Context.RadiusIp, Context.RadiusPort, obj.AcctSessionID, obj.NAS, obj.FramedIP); break;
                            case 2:
                                int epoch = (int)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;
                                urlCoa = string.Format("http://{0}:{1}/disconnect?Acct-Session-Id={2}&Nas-Ip-address={3}", Context.RadiusIp, Context.RadiusPort, obj.AcctSessionID, obj.NAS);
                                if (Context.TimeStamp)
                                    urlCoa = urlCoa + "&Event-Timestamp=" + epoch;
                                break;
                        }

                        if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "Url para la llamada: " + urlCoa);

                        try
                        { 
                            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(urlCoa);
                            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                            string content = new StreamReader(response.GetResponseStream()).ReadToEnd();
                            if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "Respuesta de la llamada: " + content);

                            obj.Flag += 1;
                            obj.DateUpdated = DateTime.Now.ToUniversalTime();
                            BillingController.UpdateRadiusCoa(obj);
                            if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, string.Format("Actualizamos el Flag: IdRadiusCoa : {0} | ActSessionID: {1} | Flag: {2} | DatedUpdated: {3}" ,obj.IdRadiusCoa, obj.AcctSessionID, obj.Flag, obj.DateUpdated));
                        }
                        catch(WebException wex )
                        {
                            Context.Log.WriteEntry(ELogType.Inform, Context.AppName, wex.Message) ;
                        }
                    }

                    if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "Conectamos BD y descargamos entradas para comprobar si han sido procesadas correctamente.");
                    List<RadiusCoa> radiusUnfinished = BillingController.SelectRadiusCoaUnFinished();
                    if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "Entradas sin finalizar: " + radiusUnfinished.Count());

                    foreach (RadiusCoa obj in radiusUnfinished)
                    {
                        CoA_Types coa_type = BillingController.SelectCoA_Type(obj.CoAType);
                        CoA_Vendors coa_vendor = BillingController.SelectCoA_Vendor(obj.CoAVendor);
                        ActiveSessions activeSession = BillingController.GetActiveSession(obj.IdActiveSession);
                        if (activeSession.IdActiveSession.Equals(0))
                        {
                            if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "Entrada: " + string.Format("IdHotel: {0} | UserName: {1} | ActiveSessionID: {2} | Nas-IP: {3} | CoA_Type: {4} | CoA_Vendor : {5}", obj.IdHotel, obj.UserName, obj.AcctSessionID, obj.NAS, coa_type.Action, coa_vendor.Name));
                            if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "Procesado correctamente");
                            obj.Flag = 99;
                            obj.DateUpdated = DateTime.Now.ToUniversalTime();
                            BillingController.UpdateRadiusCoa(obj);
                            if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, string.Format("Actualizamos el Flag: IdRadiusCoa : {0} | ActSessionID: {1} | Flag: {2} | DatedUpdated: {3}", obj.IdRadiusCoa, obj.AcctSessionID, obj.Flag, obj.DateUpdated));
                        }
                        else
                        {
                            if (obj.Flag < Context.Intentos)
                            {
                                if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "Entrada: " + string.Format("IdHotel: {0} | UserName: {1} | ActiveSessionID: {2} | Nas-IP: {3} | CoA_Type: {4} | CoA_Vendor : {5}", obj.IdHotel, obj.UserName, obj.AcctSessionID, obj.NAS, coa_type.Action, coa_vendor.Name));
                                if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "No ha sido borrado de la tabla de Accounting, volvemos a enviar la petición al radius");

                                string urlCoA = string.Empty;
                                switch (coa_vendor.IdCoAVendors)
                                {
                                    case 1:
                                        urlCoA = string.Format("http://{0}:{1}/disconnect?Acct-Session-Id={2}&Nas-Ip-address={3}&Framed-ip-address={4}", Context.RadiusIp, Context.RadiusPort, obj.AcctSessionID, obj.NAS, obj.FramedIP); break;
                                    case 2:
                                        int epoch = (int)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;
                                        urlCoA = string.Format("http://{0}:{1}/disconnect?Acct-Session-Id={2}&Nas-Ip-address={3}", Context.RadiusIp, Context.RadiusPort, obj.AcctSessionID, obj.NAS);
                                        if (Context.TimeStamp)
                                            urlCoA = urlCoA + "&Event-Timestamp=" + epoch;
                                        break;
                                }


                                if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "Url para la llamada: " + urlCoA);

                                try
                                {
                                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(urlCoA);
                                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                                    string content = new StreamReader(response.GetResponseStream()).ReadToEnd();
                                    if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "Respuesta de la llamada: " + content);

                                    obj.Flag += 1;
                                    obj.DateUpdated = DateTime.Now.ToUniversalTime();
                                    BillingController.UpdateRadiusCoa(obj);
                                    if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, string.Format("Actualizamos el Flag: IdRadiusCoa : {0} | ActSessionID: {1} | Flag: {2} | DatedUpdated: {3}", obj.IdRadiusCoa, obj.AcctSessionID, obj.Flag, obj.DateUpdated));
                                }
                                catch (WebException wex)
                                {
                                    Context.Log.WriteEntry(ELogType.Inform, Context.AppName, wex.Message);
                                }
                            }
                            else
                            {
                                if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "Entrada: " + string.Format("IdHotel: {0} | UserName: {1} | ActiveSessionID: {2} | Nas-IP: {3} | CoA_Type: {4} | CoA_Vendor : {5}", obj.IdHotel, obj.UserName, obj.AcctSessionID, obj.NAS, coa_type.Action, coa_vendor.Name));
                                if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "Máximo de intentos alcanzados");
                                obj.Flag = 100;
                                obj.DateUpdated = DateTime.Now.ToUniversalTime();
                                BillingController.UpdateRadiusCoa(obj);
                                if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, string.Format("Actualizamos el Flag: IdRadiusCoa : {0} | ActSessionID: {1} | Flag: {2} | DatedUpdated: {3}", obj.IdRadiusCoa, obj.AcctSessionID, obj.Flag, obj.DateUpdated));

                            }
                        }
                    }
                }
                else
                   if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "Está ejecutando un ciclo anterior.");
            }
            catch (Exception ex)
            {
                 Context.Log.WriteEntry(ELogType.Error, Context.AppName, ex.Message);
            }
            finally
            {
                isExecuting = false;
                if (Context.IsLog) Context.Log.WriteEntry(ELogType.Inform, Context.AppName, "**Fin del proceso**");
            }
        }

        #endregion
    }
}
