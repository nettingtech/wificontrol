﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wifi360.Daemon.Common
{
    public class Context
    {
        #region Variables

        private static Log _log = null;
        private static string _appName = string.Empty;
        private static string _radiusIp = string.Empty;
        private static string _radiusPort = string.Empty;
        private static int _intentos = 0;
        private static bool _isLog = false;
        private static bool _timeStamp = false;

        #endregion

        #region Properties

        public static Log Log
        {
            set { _log = value; }
            get { return _log; }
        }

        public static string AppName
        {
            set { _appName = value; }
            get
            {
                _appName = ConfigurationManager.AppSettings["AppName"].ToString();
                return _appName;
            }
        }

        public static string RadiusIp
        {
            set { _radiusIp = value; }
            get
            {
                _radiusIp = ConfigurationManager.AppSettings["RadiusIP"].ToString();
                return _radiusIp;
            }
        }

        public static string RadiusPort
        {
            set { _radiusPort = value; }
            get
            {
                _radiusPort = ConfigurationManager.AppSettings["RadiusPort"].ToString();
                return _radiusPort;
            }
        }

        public static int Intentos
        {
            set { _intentos = value; }
            get
            {
                _intentos = Int32.Parse(ConfigurationManager.AppSettings["Attemps"].ToString());
                return _intentos;
            }
        }

        public static bool IsLog
        {
            set { _isLog = value; }
            get
            {
                _isLog = Boolean.Parse(ConfigurationManager.AppSettings["IsLogin"].ToString());
                return _isLog;
            }
        }

        public static bool TimeStamp
        {
            set { _timeStamp = value; }
            get
            {
                _timeStamp = Boolean.Parse(ConfigurationManager.AppSettings["TimeStamp"].ToString());
                return _timeStamp;
            }
        }

        #endregion
    }
}
