﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wifi360.Daemon.Common
{
    public static class Misc
    {
        private static DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public enum TareaEstado { PEXP, EXP, EXPC, PIMP, IMP, IMPC };

        public enum TareaTipoFichero { ART, CLI, SUR };

        public enum TareaTipo { EXP, IMP };

        /// <summary>
        /// Convierte un "string" a un "short" devolviendo un valor por defecto (defaultValue) en caso de error
        /// </summary>
        /// <param name="inputString">Cadena de entrada a convertir (string)</param>
        /// <param name="defaultValue">Valor por defecto [o 0] devuelto en caso de error (short)</param>
        /// <returns>Valor convertido o 'defaultValue' en caso de error (short)</returns>
        public static short StringToShort(string inputString, short defaultValue = 0)
        {
            try
            {
                return short.Parse(inputString);
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// Comprueba si un "string" se puede convertir o no en un "short"
        /// </summary>
        /// <param name="inputString">Cadena de entrada a convertir (string)</param>
        /// <returns>Si se pudo convertir o no (bool)</returns>
        public static bool StringToShortCheck(string inputString)
        {
            try
            {
                short.Parse(inputString);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Convierte un "string" a un "int" devolviendo un valor por defecto (defaultValue) en caso de error
        /// </summary>
        /// <param name="inputString">Cadena de entrada a convertir (string)</param>
        /// <param name="defaultValue">Valor por defecto [o 0] devuelto en caso de error (int)</param>
        /// <returns>Valor convertido o 'defaultValue' en caso de error (int)</returns>
        public static int StringToInt(string inputString, int defaultValue = 0)
        {
            try
            {
                return int.Parse(inputString);
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// Comprueba si un "string" se puede convertir o no en un "int"
        /// </summary>
        /// <param name="inputString">Cadena de entrada a convertir (string)</param>
        /// <returns>Si se pudo convertir o no (bool)</returns>
        public static bool StringToIntCheck(string inputString)
        {
            try
            {
                int.Parse(inputString);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Convierte un "string" a un "long" devolviendo un valor por defecto (defaultValue) en caso de error
        /// </summary>
        /// <param name="inputString">Cadena de entrada a convertir (string)</param>
        /// <param name="defaultValue">Valor por defecto [o 0] devuelto en caso de error (long)</param>
        /// <returns>Valor convertido o 'defaultValue' en caso de error (long)</returns>
        public static long StringToLong(string inputString, long defaultValue = 0)
        {
            try
            {
                return long.Parse(inputString);
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// Comprueba si un "string" se puede convertir o no en un "long"
        /// </summary>
        /// <param name="inputString">Cadena de entrada a convertir (string)</param>
        /// <returns>Si se pudo convertir o no (bool)</returns>
        public static bool StringToLongCheck(string inputString)
        {
            try
            {
                long.Parse(inputString);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Convierte un "string" a un "float" devolviendo un valor por defecto (defaultValue) en caso de error
        /// </summary>
        /// <param name="inputString">Cadena de entrada a convertir (string)</param>
        /// <param name="defaultValue">Valor por defecto [o 0f] devuelto en caso de error (float)</param>
        /// <returns>Valor convertido o 'defaultValue' en caso de error (float)</returns>
        public static float StringToFloat(string inputString, float defaultValue = 0f)
        {
            try
            {
                return float.Parse(inputString);
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// Comprueba si un "string" se puede convertir o no en un "float"
        /// </summary>
        /// <param name="inputString">Cadena de entrada a convertir (string)</param>
        /// <returns>Si se pudo convertir o no (bool)</returns>
        public static bool StringToFloatCheck(string inputString)
        {
            try
            {
                float.Parse(inputString);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Convierte un "string" a un "double" devolviendo un valor por defecto (defaultValue) en caso de error
        /// </summary>
        /// <param name="inputString">Cadena de entrada a convertir (string)</param>
        /// <param name="defaultValue">Valor por defecto [o 0.0] devuelto en caso de error (double)</param>
        /// <returns>Valor convertido o 'defaultValue' en caso de error (float)</returns>
        public static double StringToDouble(string inputString, double defaultValue = 0.0)
        {
            try
            {
                return double.Parse(inputString);
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// Comprueba si un "string" se puede convertir o no en un "double"
        /// </summary>
        /// <param name="inputString">Cadena de entrada a convertir (string)</param>
        /// <returns>Si se pudo convertir o no (bool)</returns>
        public static bool StringToDoubleCheck(string inputString)
        {
            try
            {
                double.Parse(inputString);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Convierte un "string" a un "decimal" devolviendo un valor por defecto (defaultValue) en caso de error
        /// Utiliza 'CultureInfo.InvariantCulture' para especificar el . como separador decimal y la , como separador de miles
        /// </summary>
        /// <param name="inputString">Cadena de entrada a convertir (string)</param>
        /// <param name="defaultValue">Valor por defecto [o 0.0m] devuelto en caso de error (decimal)</param>
        /// <returns>Valor convertido o 'defaultValue' en caso de error (decimal)</returns>
        public static decimal StringToDecimal(string inputString, decimal defaultValue = 0.0m)
        {
            try
            {
                return decimal.Parse(inputString, CultureInfo.InvariantCulture);
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// Comprueba si un "string" se puede convertir o no en un "decimal"
        /// Utiliza 'CultureInfo.InvariantCulture' para especificar el . como separador decimal y la , como separador de miles
        /// </summary>
        /// <param name="inputString">Cadena de entrada a convertir (string)</param>
        /// <returns>Si se pudo convertir o no (bool)</returns>
        public static bool StringToDecimalCheck(string inputString)
        {
            try
            {
                decimal.Parse(inputString, CultureInfo.InvariantCulture);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Convierte un "string" a un "bool" devolviendo false en caso de error
        /// </summary>
        /// <param name="inputString">Cadena de entrada a convertir (string)</param>
        /// <returns>Valor convertido o false en caso de error (bool)</returns>
        public static bool StringToBool(string inputString)
        {
            try
            {
                return bool.Parse(inputString);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Comprueba si un "string" se puede convertir o no en un "bool"
        /// </summary>
        /// <param name="inputString">Cadena de entrada a convertir (string)</param>
        /// <returns>Si se pudo convertir o no (bool)</returns>
        public static bool StringToBoolCheck(string inputString)
        {
            try
            {
                bool.Parse(inputString);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Convierte un "string" a un "char" devolviendo un valor por defecto (defaultValue) en caso de error
        /// Sólo se toma el primer carácter del string, después de haberle hecho un "Trim"
        /// </summary>
        /// <param name="inputString">Cadena de entrada a convertir (string)</param>
        /// <param name="defaultValue">Valor por defecto [o '\0'] devuelto en caso de error (char)</param>
        /// <returns>Valor convertido o 'defaultValue' en caso de error (char)</returns>
        public static char StringToChar(string inputString, char defaultValue = '\0')
        {
            try
            {
                return char.Parse(inputString.Trim().Substring(0, 1));
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// Comprueba si un "string" se puede convertir o no en un "char" (tomando el 1er carácter)
        /// </summary>
        /// <param name="inputString">Cadena de entrada a convertir (string)</param>
        /// <returns>Si se pudo convertir o no (bool)</returns>
        public static bool StringToCharCheck(string inputString)
        {
            try
            {
                char.Parse(inputString.Trim().Substring(0, 1));
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Convierte un "string" a un "DateTime" dado el formato dado. Devuelve la fecha del sistema en caso de error
        /// </summary>
        /// <param name="inputString">Cadena de entrada a convertir (string)</param>
        /// <param name="dateFormat">Formato de la fecha a convertir, por ejemplo "yyyy-MM-dd" (string)</param>
        /// <returns>Fecha convertida o 'DateTime.MinValue' en caso de error (DateTime)</returns>
        public static DateTime StringToDateTime(string inputString, string dateFormat)
        {
            try
            {
                return DateTime.ParseExact(inputString, dateFormat, System.Globalization.CultureInfo.InvariantCulture);
            }
            catch (Exception)
            {
                return DateTime.MinValue;
            }
        }

        /// <summary>
        /// Convierte un "string" a un "DateTime" dado el formato dado. Devuelve una fecha por defecto (defaultValue) en caso de error
        /// </summary>
        /// <param name="inputString">Cadena de entrada a convertir (string)</param>
        /// <param name="dateFormat">Formato de la fecha a convertir, por ejemplo "yyyy-MM-dd" (string)</param>
        /// <param name="defaultValue">Valor por defecto devuelto en caso de error (DateTime)</param>
        /// <returns>Fecha convertida o 'defaultValue' en caso de error (DateTime)</returns>
        public static DateTime StringToDateTime(string inputString, string dateFormat, DateTime defaultValue)
        {
            try
            {
                return DateTime.ParseExact(inputString, dateFormat, System.Globalization.CultureInfo.InvariantCulture);
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// Comprueba si un "string" se puede convertir o no en un "DateTime" con el formato dado.
        /// </summary>
        /// <param name="inputString">Cadena de entrada a convertir (string)</param>
        /// <param name="dateFormat">Formato de la fecha a convertir, por ejemplo "yyyy-MM-dd" (string)</param>
        /// <returns>Si se pudo convertir o no (bool)</returns>
        public static bool StringToDateTimeCheck(string inputString, string dateFormat)
        {
            try
            {
                DateTime.ParseExact(inputString, dateFormat, System.Globalization.CultureInfo.InvariantCulture);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Convierte un "DateTime" a un "string" dado el formato dado.
        /// </summary>
        /// <param name="inputDateTime">Fecha de entrada a covertir (DateTime)</param>
        /// <param name="dateFormat">Formato de la fecha a convertir, por ejemplo "yyyy-MM-dd" (string)</param>
        /// <returns>Fecha convertida con el formato dado (string)</returns>
        public static string DateTimeToString(DateTime inputDateTime, string dateFormat)
        {
            try
            {
                return inputDateTime.ToString(dateFormat, System.Globalization.CultureInfo.InvariantCulture);
            }
            catch (FormatException)
            {
                return inputDateTime.ToString();
            }
            catch (ArgumentOutOfRangeException)
            {
                return DateTime.Now.ToString();
            }
        }

        /// <summary>
        /// Convierte un fecha dada en un Timestamp tipo UNIX, es decir, los segundos desde el 01/01/1970 hasta la fecha
        /// </summary>
        /// <param name="inputDateTime">Fecha de entrada a covertir (DateTime)</param>
        /// <returns>UNIX Timestamp, segundos desde el 01/01/1970 hasta la fecha dada o 0 en caso de error (long)</returns>
        public static long DateTimeToUnixTime(DateTime inputDateTme)
        {
            try
            {
                return Convert.ToInt64((inputDateTme.ToUniversalTime() - epoch).TotalSeconds);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary>
        /// Convierte un un Timestamp tipo UNIX (segundos desde el 01/01/1970) en un DateTime
        /// </summary>
        /// <param name="inputUnixTime">UNIX Timestamp, segundos desde el 01/01/1970 (long)</param>
        /// <returns>Fecha convertida o 'DateTime.MinValue' en caso de error (DateTime)</returns>
        public static DateTime UnixTimeToDateTime(long inputUnixTime)
        {
            try
            {
                return epoch.AddSeconds(inputUnixTime);
            }
            catch (Exception)
            {
                return DateTime.MinValue;
            }
        }

        /// <summary>
        /// Concatena el string "str1" con el string "str2" (en ese orden) separados por una nueva línea
        /// </summary>
        /// <param name="str1">Primer string</param>
        /// <param name="str2">Segundo string</param>
        /// <returns></returns>
        public static string StringConcatNewLine(string str1, string str2)
        {
            if (string.IsNullOrWhiteSpace(str1))
                return str2;
            else
                return str1.EndsWith(Environment.NewLine) ? str1 + str2 : str1 + Environment.NewLine + str2;
        }

        /// <summary>
        /// Elimina la "\" del final de un path en caso de que la tenga
        /// </summary>
        /// <param name="inputPath">El path de entrada (string)</param>
        /// <returns>El path sin la "\" del final</returns>
        public static string NormalizePath(string inputPath)
        {
            string lastChar = inputPath.Substring(inputPath.Length - 1, 1);

            if ((inputPath.Length > 2) && (lastChar == @"\"))
                return inputPath.Remove(inputPath.Length - 1);
            else if ((inputPath.Length > 1) && (inputPath.Substring(0, 1) == ".") && (lastChar == @"\"))
                return inputPath.Remove(inputPath.Length - 1);
            else
                return inputPath;
        }

        /// <summary>
        /// Comprueba si existe o no un directorio y si no lanza una excepción del tipo LogException
        /// </summary>
        /// <param name="inputPath">El path de entrada (string)</param>
        public static void CheckDir(string inputPath)
        {
            if (!Directory.Exists(inputPath))
                throw new LogException(ELogType.Error, "El directorio '" + inputPath + "' no existe.");
        }

        /// <summary>
        /// Mueve un fichero origen a uno destino borrando el destino previamente en caso de que existiera
        /// </summary>
        /// <param name="sourceFileName">Fichero origen (string)</param>
        /// <param name="destFileName">Fichero destino (string)</param>
        public static void FileMoveOverwrite(string sourceFileName, string destFileName)
        {
            try
            {
                CheckDir(Path.GetDirectoryName(destFileName));

                if (File.Exists(destFileName))
                    File.Delete(destFileName);

                File.Move(sourceFileName, destFileName);
            }
            catch (LogException logEx)
            {
                throw logEx;
            }
            catch (Exception ex)
            {
                throw new LogException(ELogType.Error, "Error moviendo el fichero '" + sourceFileName + "' a '" + destFileName + "'.", ex);
            }
        }

        /// <summary>
        /// Añade el número de página a un documento de itextSharp
        /// </summary>
        /// <param name="path">Ruta del pdf</param>

    }
}
