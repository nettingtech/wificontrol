﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Data.SqlClient;
using System.IO;

namespace Wifi360.Daemon.Common
{
    /// <summary>
    /// Enumerado con las diferentes categorías de Log
    /// </summary>
    public enum ELogType { Debug, Error, Warning, Inform };

    /// <summary>
    /// Enumerado con las diferentes destinos del Log: fichero diario, consola o visor de eventos
    /// </summary>
    public enum ELogDest { File, Console, Viewer };

    /// <summary>
    /// Clase que implementa un log diario en archivo de texto plano. También incorpora métodos alternativos para crear
    /// un log a través del Visor de Eventos de Windows y devuelto en un simple string. Si el log no se puede escribir
    /// en disco por algún error, entonces el mismo log se escribirá en el Visor de Eventos.
    /// </summary>
    public class Log : BusinessBase
    {
        //#region Provider

        //private VespucioProvider _provider = null;

        //public VespucioProvider Provider
        //{
        //    get
        //    {
        //        if (_provider == null)
        //            _provider = VespucioProvider.Instance;
        //        return _provider;
        //    }
        //}

        //#endregion

        #region Variables Privadas

        private string appName;
        private string logDir;
        private ELogDest logDest;
        private const string stackOpen = @" <StackTrace> ";
        private const string stackClose = @" </StackTrace> ";
        private const string stackSeparator = @" <br /> ";
        private ELogType logType;
        private Guid thread = Guid.Empty;
        private string user = string.Empty;
        private string message = string.Empty;
        private ELogType level;

        #endregion Variables Privadas

        #region Propiedades

        public string AppName
        {
            get { return appName; }
            set { appName = value; }
        }

        public string User
        {
            get { return user; }
            set { user = value; PropertyChange("User"); }
        }

        public string Message
        {
            get { return message; }
            set { message = value; PropertyChange("Message"); }
        }

        public string LogDir
        {
            get { return logDir; }
            set { logDir = value; }
        }

        public ELogDest LogDest
        {
            get { return logDest; }
            set { logDest = value; }
        }

        public ELogType LogType
        {
            get { return logType; }
            set { logType = value; }
        }

        public Guid Thread
        {
            get { return thread; }
            set
            {
                thread = value;
                PropertyChange("Thread");
            }
        }

        public ELogType Level
        {
            get { return level; }
            set { level = value; PropertyChange("Level"); }
        }

        #endregion Propiedades

        #region Constructores

        /// <summary>
        /// Constructor por defecto de la clase LogDaily. Si se usa para escribir ficheros de log diario, estos
        /// se escribirán en el mismo directorio en donde "corra" la aplicación.
        /// </summary>
        /// <param name="appName">Nombre de la aplicación con la que "firmará" el log (string)</param>
        /// <param name="logDest">El destino del log: fichero diario, consola o visor de eventos (ELogDest)</param>
        public Log(string appName, ELogDest logDest)
        {
            this.appName = appName;
            this.logDir = string.Empty;
            this.logDest = logDest;
        }

        /// <summary>
        /// Constructor alternativo de la clase LogDaily para especificar un directorio irán los ficheros de log diario.
        /// En este caso se sobrentiende que el destino del log va a ser un fichero diario.
        /// </summary>
        /// <param name="appName">Nombre de la aplicación con la que "firmará" el log (string)</param>
        /// <param name="logDir">Ruta del directorio en donde se generarán los archivos log (string)</param>
        public Log(string appName, string logDir)
        {
            this.appName = appName;
            this.logDir = logDir;
            this.logDest = ELogDest.File;
        }

        /// <summary>
        /// Constructor alternativo de la clase LogDaily para especificar un directorio irán los ficheros de log diario.
        /// En este caso se sobrentiende que el destino del log va a ser un fichero diario.
        /// </summary>
        /// <param name="appName">Nombre de la aplicación con la que "firmará" el log (string)</param>
        /// <param name="logDir">Ruta del directorio en donde se generarán los archivos log (string)</param>
        /// <param name="type">Indicará el nivel de log que tiene la aplicación (ElogType)</param>
        public Log(string appName, string logDir, ELogType type)
        {
            this.appName = appName;
            this.logDir = logDir;
            this.logDest = ELogDest.File;
            this.LogType = type;
        }

        /// <summary>
        /// Constructor alternativo de la clase LogDaily para especificar un directorio irán los ficheros de log diario.
        /// En este caso se sobrentiende que el destino del log va a ser un fichero diario.
        /// </summary>
        /// <param name="appName">Nombre de la aplicación con la que "firmará" el log (string)</param>
        /// <param name="logDir">Ruta del directorio en donde se generarán los archivos log (string)</param>
        /// <param name="type">Indicará el nivel de log que tiene la aplicación (ElogType)</param>
        /// <param name="thread">Indicará el hilo de ejecución de la aplicación</param>
        public Log(string appName, string logDir, ELogType type, Guid thread)
        {
            this.appName = appName;
            this.logDir = logDir;
            this.logDest = ELogDest.File;
            this.LogType = type;
            this.thread = thread;
        }

        #endregion Constructores

        #region Métodos privados generales

        /// <summary>
        /// Comprueba que el directorio 'Log' existe y si no lo crea
        /// </summary>
        /// <param name="logDir">Ruta del directorio (string)</param>
        private void CheckDirLog(string logDir)
        {
            try
            {
                if (!Directory.Exists(logDir))
                {
                    Directory.CreateDirectory(logDir);
                }
            }
            catch (Exception ex)
            {
                ViewerWriteEntry(ELogType.Error, "Error accediendo al directorio del Log: '" + logDir + "'.", ex);
            }
        }

        /// <summary>
        /// Genera un string el nombre de un fichero Log con formato "appname_yyyy-mm-dd.log"
        /// </summary>
        /// <param name="fullPath">Indica si se incluye la ruta completa o no en el nombre del fichero (bool)</param>
        /// <returns>El nombre del fichero (string)</returns>
        private string GenerateDailyFilename(bool fullPath)
        {
            string fileName = appName.ToLower() + "-" + DateTime.Now.ToString("yyyy-MM-dd") + ".log";
            return fullPath ? Path.GetFullPath(Misc.NormalizePath(logDir) + @"\" + fileName) : fileName;
        }


        /// <summary>
        /// Genera un string el nombre de un fichero Log con formato "appname_yyyy-mm-dd.log"
        /// </summary>
        /// <param name="fullPath">Indica si se incluye la ruta completa o no en el nombre del fichero (bool)</param>
        /// <returns>El nombre del fichero (string)</returns>
        private string GenerateFilename(bool fullPath)
        {
            string fileName = appName.ToLower() + "msg.log";
            return fullPath ? Path.GetFullPath(Misc.NormalizePath(logDir) + @"\" + fileName) : fileName;
        }

        /// <summary>
        /// Convierte un tipo enumerado de error propio (ELogType) a un tipo enumerado de EventLogEntryType
        /// </summary>
        /// <param name="entryType">Tipo de error (ELogType)</param>
        /// <returns>Tipo de error(EventLogEntryType)</returns>
        private EventLogEntryType ToEventLogEntryType(ELogType entryType)
        {
            switch (entryType)
            {
                case ELogType.Debug:
                    return EventLogEntryType.Information;

                case ELogType.Error:
                    return EventLogEntryType.Error;

                case ELogType.Warning:
                    return EventLogEntryType.Warning;

                case ELogType.Inform:
                    return EventLogEntryType.Information;

                default:
                    return EventLogEntryType.Error;
            }
        }

        /// <summary>
        /// Crea un Visor de Eventos con el nombre del programa en la categoría "Application"
        /// </summary>
        private void CreateEventSource()
        {
            if (!EventLog.SourceExists(appName))
            {
                EventLog.CreateEventSource(appName, "Application");
            }
        }

        /// <summary>
        /// Convierte un string multilinea a una monolinea
        /// Se hace un 'Trim()' de cada línea y los retornos de carro se sustituyen por 'sep'
        /// </summary>
        /// <param name="input">El texto de entrada (string)</param>
        /// <param name="sep">Separador por el que se sustituirán los retornos de carro (string)</param>
        /// <returns>El texto en una sóla línea (string)</returns>
        private static string ToMonoLine(string input, string sep)
        {
            return string.Join(sep, input.Split(new[] { Environment.NewLine }, StringSplitOptions.None).Select(l => l.Trim()));
        }

        /// <summary>
        /// Genera un string (monolinea) con: fecha_hora, aplicación, tipo y mensaje
        /// </summary>
        /// <param name="logType">Tipo de Log (ELogType)</param>
        /// <param name="message">Mensaje (string)</param>
        /// <returns>El texto del error parseado (string)</returns>
        private string ParseLogInfo(ELogType logType, string message)
        {
            return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss   ") + appName.ToUpper() + " " + logType.ToString().ToUpper().PadRight(9, ' ') + ToMonoLine(message, " ");
        }

        /// <summary>
        /// Genera un string (monolinea) con: fecha_hora, aplicación, tipo y mensaje
        /// </summary>
        /// <param name="logType">Tipo de Log (ELogType)</param>
        /// <param name="message">Mensaje (string)</param>
        /// <param name="user">Usuario que crea la accion (string)</param>
        /// <returns>El texto del error parseado (string)</returns>
        private string ParseLogInfo(ELogType logType, string user, string message)
        {
            return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss   ") + appName.ToUpper() + " " + logType.ToString().ToUpper().PadRight(9, ' ') + user.ToUpper().PadRight(9, ' ') + "    " + ToMonoLine(message, " ");
        }

        /// <summary>
        /// Genera un string (monolinea o no) con: fecha_hora, aplicación, tipo y excepción
        /// </summary>
        /// <param name="logType">Tipo de Log (ELogType)</param>
        /// <param name="eex">Excepción (Exception)</param>
        /// <param name="oneLine">'Flag' para que la salida sea monolínea o no (bool)</param>
        /// <returns>El texto del error parseado (string)</returns>
        private string ParseLogInfo(ELogType logType, Exception ex, bool oneLine)
        {
            if (oneLine)
                return ParseLogInfo(logType, ex.Message) + stackOpen + ToMonoLine(ex.StackTrace, stackSeparator) + stackClose;
            else
                return ParseLogInfo(logType, ex.Message) + Environment.NewLine + ex.StackTrace;
        }

        /// <summary>
        /// Genera un string (monolinea o no) con: fecha_hora, aplicación, tipo, mensaje y excepción
        /// </summary>
        /// <param name="logType">Tipo de Log (ELogType)</param>
        /// <param name="message">Mensaje (string)</param>
        /// <param name="eex">Excepción (Exception)</param>
        /// <param name="oneLine">'Flag' para que la salida sea monolínea o no (bool)</param>
        /// <returns>El texto del error parseado (string)</returns>
        private string ParseLogInfo(ELogType logType, string message, Exception ex, bool oneLine)
        {
            if (oneLine)
                return ParseLogInfo(logType, message) + "   " + ToMonoLine(ex.Message, " ") + stackOpen + ToMonoLine(ex.StackTrace, stackSeparator) + stackClose;
            else
                return ParseLogInfo(logType, message) + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace;
        }

        /// <summary>
        /// Genera un string (monolinea o no) a partir de lo que encuentre en una excepción 'LogDailyException'
        /// </summary>
        /// <param name="logEx">Excepción específica con toda la información del log (LogDailyException)</param>
        /// <param name="oneLine">'Flag' para que la salida sea monolínea o no (bool)</param>
        /// <returns>El texto del error parseado (string)</returns>
        private string ParseLogInfo(LogException logEx, bool oneLine)
        {
            if (logEx.InnerException == null)
            {
                return ParseLogInfo(logEx.LogType, logEx.OuterMessage);
            }
            else
            {
                if (string.IsNullOrWhiteSpace(logEx.OuterMessage))
                {
                    return ParseLogInfo(logEx.LogType, logEx.InnerException, oneLine);
                }
                else
                {
                    return ParseLogInfo(logEx.LogType, logEx.OuterMessage, logEx.InnerException, oneLine);
                }
            }
        }

        #endregion Métodos privados generales

        #region Métodos privados Log en fichero diario

        /// <summary>
        /// Crea una entrada del log en el fichero diario 'yyyy-mm-dd.log' con: fecha_hora, aplicación, tipo y mensaje
        /// </summary>
        /// <param name="logType">Tipo de Log (ELogType)</param>
        /// <param name="message">Mensaje (string)</param>
        private void FileWriteEntry(ELogType logType, string message)
        {
            try
            {
                string fullFileName = GenerateDailyFilename(true);

                CheckDirLog(Path.GetDirectoryName(fullFileName));

                using (StreamWriter sw = new StreamWriter(fullFileName, true))
                {
                    sw.WriteLine(ParseLogInfo(logType, message));
                }
            }
            catch (Exception iex)
            {
                ViewerWriteEntry(
                    ELogType.Error,
                    "Error creando un log en el fichero: '" + GenerateDailyFilename(false) + "'.",
                    iex,
                    ParseLogInfo(logType, message)
                );
            }
        }

        /// <summary>
        /// Crea una entrada del log en el fichero diario 'yyyy-mm-dd.log' con: fecha_hora, aplicación, tipo, usuario y mensaje
        /// </summary>
        /// <param name="logType">Tipo de Log (ELogType)</param>
        /// <param name="message">Mensaje (string)</param>
        /// <param name="user">Usuario que realiza la accion (string)</param>
        private void FileWriteEntry(ELogType logType, string message, string user)
        {
            try
            {
                string fullFileName = GenerateDailyFilename(true);

                CheckDirLog(Path.GetDirectoryName(fullFileName));

                using (StreamWriter sw = new StreamWriter(fullFileName, true))
                {
                    sw.WriteLine(ParseLogInfo(logType, user, message));
                }
            }
            catch (Exception iex)
            {
                ViewerWriteEntry(
                    ELogType.Error,
                    "Error creando un log en el fichero: '" + GenerateDailyFilename(false) + "'.",
                    iex,
                    ParseLogInfo(logType, user, message)
                );
            }
        }

        /// <summary>
        /// Crea una entrada del log en el fichero diario 'yyyy-mm-dd.log' con: fecha_hora, aplicación, tipo y excepción
        /// </summary>
        /// <param name="logType">Tipo de Log (ELogType)</param>
        /// <param name="eex">Excepción (Exception)</param>
        private void FileWriteEntry(ELogType logType, Exception eex)
        {
            try
            {
                string fullFileName = GenerateDailyFilename(true);

                CheckDirLog(Path.GetDirectoryName(fullFileName));

                using (StreamWriter sw = new StreamWriter(fullFileName, true))
                {
                    sw.WriteLine(ParseLogInfo(logType, eex, true));
                }
            }
            catch (Exception iex)
            {
                ViewerWriteEntry(
                    ELogType.Error,
                    "Error creando un log en el fichero: '" + GenerateDailyFilename(false) + "'.",
                    iex,
                    ParseLogInfo(logType, eex, false)
                );
            }
        }

        /// <summary>
        /// Crea una entrada del log en el fichero diario 'yyyy-mm-dd.log' con: fecha_hora, aplicación, tipo, mensaje y excepción
        /// </summary>
        /// <param name="logType">Tipo de Log (ELogType)</param>
        /// <param name="message">Mensaje (string)</param>
        /// <param name="eex">Excepción (Exception)</param>
        private void FileWriteEntry(ELogType logType, string message, Exception eex)
        {
            try
            {
                string fullFileName = GenerateDailyFilename(true);

                CheckDirLog(Path.GetDirectoryName(fullFileName));

                using (StreamWriter sw = new StreamWriter(fullFileName, true))
                {
                    sw.WriteLine(ParseLogInfo(logType, message, eex, true));
                }
            }
            catch (Exception iex)
            {
                ViewerWriteEntry(
                    ELogType.Error,
                    "Error creando un log en el fichero: '" + GenerateDailyFilename(false) + "'.",
                    iex,
                    ParseLogInfo(logType, message, eex, false)
                );
            }
        }

        /// <summary>
        /// Crea una entrada del log en el fichero diario 'yyyy-mm-dd.log' con: fecha_hora, aplicación y el resto que
        /// encuentre en una excepción del tipo 'LogDailyException' (tipo, mensaje y excepción original)
        /// </summary>
        /// <param name="logEx">Excepción específica con toda la información del log (LogDailyException)</param>
        private void FileWriteEntry(LogException logEx)
        {
            try
            {
                string fullFileName = GenerateDailyFilename(true);

                CheckDirLog(Path.GetDirectoryName(fullFileName));

                using (StreamWriter sw = new StreamWriter(fullFileName, true))
                {
                    sw.WriteLine(ParseLogInfo(logEx, true));
                }
            }
            catch (Exception iex)
            {
                ViewerWriteEntry(
                    ELogType.Error,
                    "Error creando un log en el fichero: '" + GenerateDailyFilename(false) + "'.",
                    iex,
                    ParseLogInfo(logEx, false)
                );
            }
        }

        /// <summary>
        /// Crea una entrada de la base de datos : fecha_hora, thread,nivel, usuario y mensaje
        /// </summary>
        /// <param name="logType">Tipo de Log (ELogType)</param>
        /// <param name="message">Mensaje (string)</param>
        /// <param name="user">Usuario que realiza la accion (string)</param>
        private void DataBaseEntry(ELogType logType, string message, string user)
        {
            try
            {
                this.User = user;
                this.Message = message;
                this.level = logType;
                this.Save();
            }
            catch (Exception iex)
            {
                ViewerWriteEntry(
                    ELogType.Error,
                    "Error creando un log en el fichero: '" + GenerateDailyFilename(false) + "'.",
                    iex,
                    ParseLogInfo(logType, user, message)
                );
            }
        }

        /// <summary>
        /// Crea una entrada de la base de datos : fecha_hora, thread,nivel, usuario y mensaje
        /// </summary>
        /// <param name="logType">Tipo de Log (ELogType)</param>
        /// <param name="message">Mensaje (string)</param>
        /// <param name="user">Usuario que realiza la accion (string)</param>
        /// <param name="transaction">Transacción sql usada</param>
        private void DataBaseEntry(ELogType logType, string message, string user, SqlTransaction transaction)
        {
            try
            {
                this.User = user;
                this.Message = message;
                this.level = logType;
                this.Save(transaction);
            }
            catch (Exception iex)
            {
                ViewerWriteEntry(
                    ELogType.Error,
                    "Error creando un log en el fichero: '" + GenerateDailyFilename(false) + "'.",
                    iex,
                    ParseLogInfo(logType, user, message)
                );
            }
        }

        #endregion Métodos privados Log en fichero diario

        #region Métodos privados Log en un string

        /// <summary>
        /// Crea una entrada del log en un string con: fecha_hora, aplicación, tipo y mensaje
        /// </summary>
        /// <param name="logType">Tipo de Log (ELogType)</param>
        /// <param name="message">Mensaje (string)</param>
        /// <returns>El log en un string (string)</returns>
        private string StringWriteEntry(ELogType logType, string message)
        {
            return ParseLogInfo(logType, message);
        }

        /// <summary>
        /// Crea una entrada del log en un string con: fecha_hora, aplicación, tipo y mensaje
        /// </summary>
        /// <param name="logType">Tipo de Log (ELogType)</param>
        /// <param name="user">Usuario que realiza la accion(string)</param>
        ///  <param name="message">Mensaje (string)</param>
        /// <returns>El log en un string (string)</returns>
        private string StringWriteEntry(ELogType logType, string user, string message)
        {
            return ParseLogInfo(logType, user, message);
        }

        /// <summary>
        /// Crea una entrada del log en un string con: fecha_hora, aplicación, tipo y excepción
        /// </summary>
        /// <param name="logType">Tipo de Log (ELogType)</param>
        /// <param name="eex">Excepción (Exception)</param>
        /// <returns>El log en un string (string)</returns>
        private string StringWriteEntry(ELogType logType, Exception eex)
        {
            return ParseLogInfo(logType, eex, true);
        }

        /// <summary>
        /// Crea una entrada del log en un string con: fecha_hora, aplicación, tipo, mensaje y excepción
        /// </summary>
        /// <param name="logType">Tipo de Log (ELogType)</param>
        /// <param name="message">Mensaje (string)</param>
        /// <param name="eex">Excepción (Exception)</param>

        private string StringWriteEntry(ELogType logType, string message, Exception eex)
        {
            return ParseLogInfo(logType, message, eex, true);
        }

        /// <summary>
        /// Crea una entrada del log en un string con: fecha_hora, aplicación, el resto que encuentre en una excepción
        /// del tipo 'LogDailyException' (tipo, mensaje y excepción original)
        /// </summary>
        /// <param name="logEx">Excepción específica con toda la información del log (LogDailyException)</param>
        /// <returns>El log en un string (string)</returns>
        private string StringWriteEntry(LogException logEx)
        {
            return ParseLogInfo(logEx, true);
        }

        #endregion Métodos privados Log en un string

        #region Métodos privados Log en visor de eventos

        /// <summary>
        /// Crea una entrada del log en el visor de eventos con: fecha_hora, aplicación, tipo y mensaje
        /// </summary>
        /// <param name="logType">Tipo de Log (ELogType)</param>
        /// <param name="message">Mensaje (string)</param>
        private void ViewerWriteEntry(ELogType logType, string message)
        {
            try
            {
                CreateEventSource();
                EventLog.WriteEntry(appName, ParseLogInfo(logType, message), ToEventLogEntryType(logType));
            }
            catch (Exception iex)
            {
                Console.WriteLine(
                    ParseLogInfo(ELogType.Error, "Error creando log en visor de eventos.", iex, false) +
                    Environment.NewLine + Environment.NewLine +
                    "=||=    =||=    =||=    =||=    =||=     LOG ORIGINAL:     =||=    =||=    =||=    =||=    =||=" +
                    Environment.NewLine + ParseLogInfo(logType, message)
                );
            }
        }

        /// <summary>
        /// Crea una entrada del log en el visor de eventos con: fecha_hora, aplicación, tipo y excepción
        /// </summary>
        /// <param name="logType">Tipo de Log (ELogType)</param>
        /// <param name="eex">Excepción (Exception)</param>
        private void ViewerWriteEntry(ELogType logType, Exception eex)
        {
            try
            {
                CreateEventSource();
                EventLog.WriteEntry(appName, ParseLogInfo(logType, eex, false), ToEventLogEntryType(logType));
            }
            catch (Exception iex)
            {
                Console.WriteLine(
                    ParseLogInfo(ELogType.Error, "Error creando log en visor de eventos.", iex, false) +
                    Environment.NewLine + Environment.NewLine +
                    "=||=    =||=    =||=    =||=    =||=     LOG ORIGINAL:     =||=    =||=    =||=    =||=    =||=" +
                    Environment.NewLine + ParseLogInfo(logType, eex, false)
                );
            }
        }

        /// <summary>
        /// Crea una entrada del log en el visor de eventos con: fecha_hora, aplicación, tipo, mensaje y excepción
        /// </summary>
        /// <param name="logType">Tipo de Log (ELogType)</param>
        /// <param name="message">Mensaje (string)</param>
        /// <param name="eex">Excepción (Exception)</param>
        private void ViewerWriteEntry(ELogType logType, string message, Exception eex)
        {
            try
            {
                CreateEventSource();
                EventLog.WriteEntry(appName, ParseLogInfo(logType, message, eex, false), ToEventLogEntryType(logType));
            }
            catch (Exception iex)
            {
                Console.WriteLine(
                    ParseLogInfo(ELogType.Error, "Error creando log en visor de eventos.", iex, false) +
                    Environment.NewLine + Environment.NewLine +
                    "=||=    =||=    =||=    =||=    =||=     LOG ORIGINAL:     =||=    =||=    =||=    =||=    =||=" +
                    Environment.NewLine + ParseLogInfo(logType, message, eex, false)
                );
            }
        }

        /// <summary>
        /// Crea una entrada del log en el visor de eventos: fecha_hora, aplicación, y el resto que encuentre en una
        /// excepción del tipo 'LogDailyException' (tipo, mensaje y excepción original)
        /// </summary>
        /// <param name="logEx">Excepción específica con toda la información del log (LogDailyException)</param>
        private void ViewerWriteEntry(LogException logEx)
        {
            try
            {
                CreateEventSource();
                EventLog.WriteEntry(appName, ParseLogInfo(logEx, false), ToEventLogEntryType(logEx.LogType));
            }
            catch (Exception iex)
            {
                Console.WriteLine(
                    ParseLogInfo(ELogType.Error, "Error creando log en visor de eventos.", iex, false) +
                    Environment.NewLine + Environment.NewLine +
                    "=||=    =||=    =||=    =||=    =||=     LOG ORIGINAL:     =||=    =||=    =||=    =||=    =||=" +
                    Environment.NewLine + ParseLogInfo(logEx, false)
                );
            }
        }

        /// <summary>
        /// Crea una entrada del log en el visor de eventos con: fecha_hora, aplicación, tipo, mensaje, excepción y log original
        /// </summary>
        /// <param name="logType">Tipo de Log (ELogType)</param>
        /// <param name="message">Mensaje (string)</param>
        /// <param name="eex">Excepción (Exception)</param>
        /// <param name="origLog">Log original (string)</param>
        private void ViewerWriteEntry(ELogType logType, string message, Exception eex, string origLog)
        {
            try
            {
                CreateEventSource();
                EventLog.WriteEntry(
                    appName,
                    ParseLogInfo(logType, message, eex, false) + Environment.NewLine + Environment.NewLine
                        + "=||=    =||=    =||=    =||=    =||=     LOG ORIGINAL:     =||=    =||=    =||=    =||=    =||="
                        + Environment.NewLine + origLog,
                    ToEventLogEntryType(logType)
                );
            }
            catch (Exception iex)
            {
                Console.WriteLine(
                    ParseLogInfo(ELogType.Error, "Error creando log en visor de eventos.", iex, false) +
                    Environment.NewLine + Environment.NewLine +
                    "=||=    =||=    =||=    =||=    =||=     LOG ORIGINAL HIJO:     =||=    =||=    =||=    =||=    =||=" +
                    Environment.NewLine + ParseLogInfo(logType, message, eex, false) +
                    Environment.NewLine + Environment.NewLine +
                    "=||=    =||=    =||=    =||=    =||=     LOG ORIGINAL PADRE:    =||=    =||=    =||=    =||=    =||=" +
                    Environment.NewLine + origLog
                );
            }
        }

        #endregion Métodos privados Log en visor de eventos

        [Serializable()]
        private class Criteria
        {
            public int IdLog = 0;

            public Criteria(int idLog)
            {
                IdLog = idLog;
            }
        }

        //protected override void Business_Update()
        //{
        //    //Use provider
        //    if (IsNew)
        //        Provider.Insert(this);
        //    //else
        //    //    Provider.Update(this);
        //}

        //protected override void Business_Update(SqlTransaction transaction)
        //{
        //    //Use provider
        //    if (IsNew)
        //        Provider.Insert(this, transaction);
        //    //else
        //    //    Provider.Update(this);
        //}

        #region Métodos públicos Log

        /// <summary>
        /// Crea una entrada del log en el fichero diario 'yyyy-mm-dd.log' con: fecha_hora, aplicación, tipo y mensaje
        /// </summary>
        /// <param name="logType">Tipo de Log (ELogType)</param>
        /// <param name="message">Mensaje (string)</param>
        public void WriteEntry(ELogType logType, string message)
        {
            switch (this.logDest)
            {
                case ELogDest.File:
                    FileWriteEntry(logType, message);
                    break;

                case ELogDest.Console:
                    Console.WriteLine(StringWriteEntry(logType, message));
                    break;

                case ELogDest.Viewer:
                    ViewerWriteEntry(logType, message);
                    break;
            }
        }

        /// <summary>
        /// Crea una entrada del log en el fichero diario 'yyyy-mm-dd.log' con: fecha_hora, aplicación, tipo y mensaje
        /// </summary>
        /// <param name="logType">Tipo de Log (ELogType)</param>
        /// <param name="user">usuario que realiza la accion (string)</param>
        /// <param name="message">Mensaje (string)</param>
        public void WriteEntry(ELogType logType, string user, string message)
        {
            if (this.LogType.Equals(ELogType.Debug)) //MODO DEBUB, ENTRA SIEMPRE
            {
                switch (this.logDest)
                {
                    case ELogDest.File:
                        FileWriteEntry(logType, message, user);
                        break;

                    case ELogDest.Console:
                        Console.WriteLine(StringWriteEntry(logType, message));
                        break;

                    case ELogDest.Viewer:
                        ViewerWriteEntry(logType, message);
                        break;
                }
            }
            else if (this.LogType.Equals(ELogType.Error) && !(logType.Equals(ELogType.Debug)))  // MODO ERROR, ENTRA SIEMBRE SALVO DEBUG
            {
                switch (this.logDest)
                {
                    case ELogDest.File:
                        FileWriteEntry(logType, message, user);
                        break;

                    case ELogDest.Console:
                        Console.WriteLine(StringWriteEntry(logType, message));
                        break;

                    case ELogDest.Viewer:
                        ViewerWriteEntry(logType, message);
                        break;
                }
            }
            else if (this.LogType.Equals(ELogType.Warning) && !(logType.Equals(ELogType.Debug)) && !(logType.Equals(ELogType.Error)))  // MODO ERROR, ENTRA SIEMBRE SALVO DEBUG
            {
                switch (this.logDest)
                {
                    case ELogDest.File:
                        FileWriteEntry(logType, message, user);
                        break;

                    case ELogDest.Console:
                        Console.WriteLine(StringWriteEntry(logType, message));
                        break;

                    case ELogDest.Viewer:
                        ViewerWriteEntry(logType, message);
                        break;
                }
            }

            else if (this.LogType.Equals(ELogType.Inform) && (logType.Equals(ELogType.Inform)))  // MODO ERROR, ENTRA SIEMBRE SALVO DEBUG
            {
                switch (this.logDest)
                {
                    case ELogDest.File:
                        FileWriteEntry(logType, message, user);
                        break;

                    case ELogDest.Console:
                        Console.WriteLine(StringWriteEntry(logType, message));
                        break;

                    case ELogDest.Viewer:
                        ViewerWriteEntry(logType, message);
                        break;
                }
            }

        }

        /// <summary>
        /// Crea una entrada del log en el fichero diario 'yyyy-mm-dd.log' con: fecha_hora, aplicación, tipo y excepción
        /// </summary>
        /// <param name="logType">Tipo de Log (ELogType)</param>
        /// <param name="eex">Excepción (Exception)</param>
        public void WriteEntry(ELogType logType, Exception eex)
        {
            switch (this.logDest)
            {
                case ELogDest.File:
                    FileWriteEntry(logType, eex);
                    break;

                case ELogDest.Console:
                    Console.WriteLine(StringWriteEntry(logType, eex));
                    break;

                case ELogDest.Viewer:
                    ViewerWriteEntry(logType, eex);
                    break;
            }
        }

        /// <summary>
        /// Crea una entrada del log en el fichero diario 'yyyy-mm-dd.log' con: fecha_hora, aplicación, tipo, mensaje y excepción
        /// </summary>
        /// <param name="logType">Tipo de Log (ELogType)</param>
        /// <param name="message">Mensaje (string)</param>
        /// <param name="eex">Excepción (Exception)</param>
        public void WriteEntry(ELogType logType, string message, Exception eex)
        {
            switch (this.logDest)
            {
                case ELogDest.File:
                    FileWriteEntry(logType, message, eex);
                    break;

                case ELogDest.Console:
                    Console.WriteLine(StringWriteEntry(logType, message, eex));
                    break;

                case ELogDest.Viewer:
                    ViewerWriteEntry(logType, message, eex);
                    break;
            }
        }

        /// <summary>
        /// Crea una entrada del log en el fichero diario 'yyyy-mm-dd.log' con: fecha_hora, aplicación y el resto que
        /// encuentre en una excepción del tipo 'LogDailyException' (tipo, mensaje y excepción original)
        /// </summary>
        /// <param name="logEx">Excepción específica con toda la información del log (LogDailyException)</param>
        public void WriteEntry(LogException logEx)
        {
            switch (this.logDest)
            {
                case ELogDest.File:
                    FileWriteEntry(logEx);
                    break;

                case ELogDest.Console:
                    Console.WriteLine(StringWriteEntry(logEx));
                    break;

                case ELogDest.Viewer:
                    ViewerWriteEntry(logEx);
                    break;
            }
        }

        /// <summary>
        /// Crea una entrada del log en Base de datos
        /// </summary>
        public void DBEntry(ELogType logType, string user, string message)
        {
            if (this.LogType.Equals(ELogType.Debug)) //MODO DEBUB, ENTRA SIEMPRE
            {
                DataBaseEntry(logType, message, user);
            }
            else if (this.LogType.Equals(ELogType.Error) && !(logType.Equals(ELogType.Debug)))  // MODO ERROR, ENTRA SIEMBRE SALVO DEBUG
            {
                DataBaseEntry(logType, message, user);
            }
            else if (this.LogType.Equals(ELogType.Warning) && !(logType.Equals(ELogType.Debug)) && !(logType.Equals(ELogType.Error)))  // MODO ERROR, ENTRA SIEMBRE SALVO DEBUG
            {
                DataBaseEntry(logType, message, user);
            }

            else if (this.LogType.Equals(ELogType.Inform) && (logType.Equals(ELogType.Inform)))  // MODO ERROR, ENTRA SIEMBRE SALVO DEBUG
            {
                DataBaseEntry(logType, message, user);
            }
        }

        /// Crea una entrada del log en Base de datos en una transaccion
        /// </summary>
        /// <param name="transaction">Transaccion usada</param>
        public void DBEntry(ELogType logType, string user, string message, SqlTransaction transaction)
        {
            if (this.LogType.Equals(ELogType.Debug)) //MODO DEBUB, ENTRA SIEMPRE
            {
                DataBaseEntry(logType, message, user, transaction);
            }
            else if (this.LogType.Equals(ELogType.Error) && !(logType.Equals(ELogType.Debug)))  // MODO ERROR, ENTRA SIEMBRE SALVO DEBUG
            {
                DataBaseEntry(logType, message, user, transaction);
            }
            else if (this.LogType.Equals(ELogType.Warning) && !(logType.Equals(ELogType.Debug)) && !(logType.Equals(ELogType.Error)))  // MODO ERROR, ENTRA SIEMBRE SALVO DEBUG
            {
                DataBaseEntry(logType, message, user, transaction);
            }

            else if (this.LogType.Equals(ELogType.Inform) && (logType.Equals(ELogType.Inform)))  // MODO ERROR, ENTRA SIEMBRE SALVO DEBUG
            {
                DataBaseEntry(logType, message, user, transaction);
            }
        }

        #endregion Métodos públicos Log

        #region Métodos públicos (estáticos) Log reducido en un string

        /// <summary>
        /// Crea una entrada reducida del log en un string con: mensaje
        /// Método estático, no requiere instanciar ni inicializar la clase porque obvia el nombre de la aplicación
        /// </summary>
        /// <param name="logType">Tipo de Log (ELogType)</param>
        /// <param name="message">Mensaje (string)</param>
        /// <returns>El log reducido en un string (string)</returns>
        public static string StringWriteLiteEntry(string message)
        {
            return ToMonoLine(message, " ");
        }

        /// <summary>
        /// Crea una entrada reducida del log en un string con: excepción
        /// Método estático, no requiere instanciar ni inicializar la clase porque obvia el nombre de la aplicación
        /// </summary>
        /// <param name="eex">Excepción (Exception)</param>
        /// <returns>El log reducido en un string (string)</returns>
        public static string StringWriteLiteEntry(Exception eex)
        {
            return ToMonoLine(eex.Message, " ") + stackOpen + ToMonoLine(eex.StackTrace, stackSeparator) + stackClose;
        }

        /// <summary>
        /// Crea una entrada reducida del log en un string con: mensaje y excepción
        /// Método estático, no requiere instanciar ni inicializar la clase porque obvia el nombre de la aplicación
        /// </summary>
        /// <param name="message">Mensaje (string)</param>
        /// <param name="eex">Excepción (Exception)</param>
        /// <returns>El log reducido en un string (string)</returns>
        public static string StringWriteLiteEntry(string message, Exception eex)
        {
            return ToMonoLine(message, " ") + "   " + StringWriteLiteEntry(eex);
        }

        /// <summary>
        /// Crea una entrada reducida del log en un string con lo que encuentre en una excepción del tipo
        /// 'LogDailyException' (mensaje y excepción original)
        /// Método estático, no requiere instanciar ni inicializar la clase porque obvia el nombre de la aplicación
        /// </summary>
        /// <param name="logEx">Excepción específica con toda la información del log (LogDailyException)</param>
        /// <returns>El log reducido en un string (string)</returns>
        public static string StringWriteLiteEntry(LogException logEx)
        {
            if (logEx.InnerException == null)
            {
                return ToMonoLine(logEx.OuterMessage, " ");
            }
            else
            {
                if (string.IsNullOrWhiteSpace(logEx.OuterMessage))
                {
                    return StringWriteLiteEntry(logEx.InnerException);
                }
                else
                {
                    return StringWriteLiteEntry(logEx.OuterMessage, logEx.InnerException);
                }
            }
        }

        #endregion Métodos públicos (estáticos) Log reducido en un string
    }
}
