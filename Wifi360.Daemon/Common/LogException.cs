﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wifi360.Daemon.Common
{
    /// <summary>
    /// Clase anexa a la clase LogDaily que define una excepción compuesta de:
    ///    - Un tipo de log (enmerado ELogType)
    ///    - Un mensaje (string)
    /// </summary>
    public class LogException : Exception
    {
        #region Variables privadas

        private ELogType logType;
        private string outerMessage;

        #endregion Variables privadas

        #region Propiedades

        public ELogType LogType
        {
            get { return logType; }
        }

        public string OuterMessage
        {
            get { return outerMessage; }
        }

        #endregion Propiedades

        #region Constructores

        /// <summary>
        /// Constructor de la clase, con un tipo de log y un mensaje
        /// </summary>
        /// <param name="logType">Tipo de Log (ELogType)</param>
        /// <param name="outerMessage">Mensaje externo a la excepción interna (string)</param>
        public LogException(ELogType logType, string outerMessage)
        {
            this.logType = logType;
            this.outerMessage = outerMessage;
        }

        /// <summary>
        /// Constructor de la clase, con un tipo de log, un mensaje y una excepción interna
        /// </summary>
        /// <param name="logType">Tipo de Log (ELogType)</param>
        /// <param name="outerMessage">Mensaje externo a la excepción interna (string)</param>
        /// <param name="innerException">Excepción interna (Exception)</param>
        public LogException(ELogType logType, string outerMessage, Exception innerException)
            : base(innerException.Message, innerException)
        {
            this.logType = logType;
            this.outerMessage = outerMessage;
        }

        #endregion Constructores
    }
}
