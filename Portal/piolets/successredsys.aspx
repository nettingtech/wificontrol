﻿<%@ Page Title="" Language="C#" MasterPageFile="~/resources/master/PortalMaster.Master" AutoEventWireup="true" CodeBehind="successredsys.aspx.cs" Inherits="Portal.piolets.successredsys" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function (e) {
            $("#myModalFinish").modal('show');
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row" id="principal" style="display:none">
            <div class="col-xs-12 col-sm-12 col-md-12">
            </div>
            </div>
        </div>
        <div class="modal fade" id="myModalFinish" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header text-center">
            <h4 class="modal-title"><asp:Label runat="server" ID="finishlabel"></asp:Label></h4>
          </div>
          <div class="modal-body">
                <div id="finishInformation" class="text-center"><asp:Literal ID="literalText" runat="server"></asp:Literal></div>
              <br />
                <asp:HyperLink runat="server" ID="finish" CssClass="btn btn-success btn-block" Text="Continue browsing"></asp:HyperLink>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>
</asp:Content>
