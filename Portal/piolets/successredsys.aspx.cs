﻿using Newtonsoft.Json;
using Portal.resources.utils;
using RedsysAPIPrj;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Wifi360.Data.Controllers;
using Wifi360.Data.Model;

namespace Portal.piolets
{
    public partial class successredsys : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string version = string.Empty;
                string data = string.Empty;
                string signatureRecieved = string.Empty;
                string Key = string.Empty;

                HttpCookie myCookie = Request.Cookies["mikrotik-piolets"];

                string idhotel = myCookie.Value;
                Hotels hotel = BillingController.GetHotel(Int32.Parse(idhotel));

                List<ParametrosConfiguracion> parametros = BillingController.GetParametrosConfiguracion(hotel.IdHotel);
                foreach (ParametrosConfiguracion config in parametros)
                {
                    switch (config.Key.ToUpper())
                    {
                        case "KEY": Key = config.value; break;
                    }
                }

                RedsysAPI r = new RedsysAPI();
                if (Request.Params["Ds_SignatureVersion"] != null)
                {
                    version = Request.Params["Ds_SignatureVersion"];
                }
                if (Request.Params["Ds_MerchantParameters"] != null)
                {
                    data = Request.Params["Ds_MerchantParameters"];
                }
                if (Request.Params["Ds_Signature"] != null)
                {
                    signatureRecieved = Request.Params["Ds_Signature"];
                }

                string deco = r.decodeMerchantParameters(data);
                string notif = r.createMerchantSignatureNotif(Key, data);
                string text = string.Empty;

                string lang = string.Empty;
                if (Request.UserLanguages != null)
                    lang = Request.UserLanguages[0].Substring(0, 2);

                if (notif.Equals(signatureRecieved) && !string.IsNullOrEmpty(notif))
                {
                    dynamic stuff = JsonConvert.DeserializeObject(deco);

                    string ds_order = stuff["Ds_Order"].ToString();
                    string[] values = ds_order.Split('_');
                    PayPalTransactions payPalTransaction = BillingController.GetPayPalTransaction(Int32.Parse(values[1]));
                    Locations2 location = BillingController.ObtainLocation(payPalTransaction.IdLocation);
                    SitesGMT siteGMT = BillingController.SiteGMTSitebyLocation(location.IdLocation);
                    if (siteGMT.Id.Equals(0))
                        siteGMT = BillingController.SiteGMTSitebySite(location.IdHotel);

                    if (!lang.Equals("es") && !lang.Equals("en"))
                        lang = BillingController.GetLanguage(hotel.IdLanguage).Code;

                    string macformated = payPalTransaction.MAC;

                    if (!payPalTransaction.Status.Equals(1))
                    {
                        CaptivePortalLog log = new CaptivePortalLog();
                        log.NSEId = string.Format("Mikrotik - Idhotel:{0}", payPalTransaction.IdHotel);
                        log.IdLocation = payPalTransaction.IdLocation;
                        log.UserName = payPalTransaction.UserName;
                        log.Password = payPalTransaction.Password;
                        log.CallerID = payPalTransaction.MAC;
                        log.Date = DateTime.Now.ToUniversalTime();
                        log.Page = "success paypal";
                        log.IdHotel = payPalTransaction.IdHotel;


                        Rooms room = BillingController.GetRoom(payPalTransaction.IdHotel, "DEFAULT");
                        BillingTypes billingType = BillingController.ObtainBillingType(payPalTransaction.IdBillingType);
                        Billings bill = new Billings();

                        bill.IdHotel = payPalTransaction.IdHotel;
                        bill.IdBillingType = payPalTransaction.IdBillingType;
                        bill.IdRoom = room.IdRoom;
                        bill.Amount = billingType.Price;
                        bill.BillingDate = DateTime.Now.ToUniversalTime();
                        bill.BillingCharge = bill.BillingDate;
                        bill.Flag = 8;
                        bill.IdLocation = billingType.IdLocation;
                        bill.Billable = true;

                        if (BillingController.SaveBilling(bill))
                        {
                            Users user = new Users();
                            Users tempU = BillingController.GetRadiusUser(payPalTransaction.UserName, payPalTransaction.IdHotel);
                            if (tempU.IdUser.Equals(0) || !tempU.Enabled)
                            {
                                user.IdHotel = payPalTransaction.IdHotel;
                                user.IdRoom = room.IdRoom;
                                user.IdBillingType = billingType.IdBillingType;
                                user.BWDown = billingType.BWDown;
                                user.BWUp = billingType.BWUp;
                                user.ValidSince = bill.BillingDate;
                                user.ValidTill = bill.BillingDate.AddHours(billingType.ValidTill);
                                user.TimeCredit = billingType.TimeCredit;
                                user.MaxDevices = billingType.MaxDevices;
                                user.Priority = billingType.IdPriority;
                                user.VolumeDown = billingType.VolumeDown;
                                user.VolumeUp = billingType.VolumeUp;
                                user.Enabled = true;
                                user.Name = payPalTransaction.UserName.ToUpper();
                                user.Password = payPalTransaction.Password.ToUpper();
                                user.CHKO = true;
                                user.BuyingCallerID = macformated;
                                user.BuyingFrom = 2;
                                user.Comment = string.Empty;
                                user.IdLocation = location.IdLocation;
                                user.Filter_Id = billingType.Filter_Id;

                                BillingController.SaveUser(user);
                                bill.IdUser = user.IdUser;
                                bill.IdFDSUser = user.IdFDSUser;
                                BillingController.SaveBilling(bill);
                            }

                            payPalTransaction.Status = 1;
                            BillingController.SavePayPalTransaction(payPalTransaction);

                            string timetext = string.Empty;
                            TimeSpan t = TimeSpan.FromSeconds(billingType.TimeCredit / billingType.MaxDevices);
                            if (billingType.TimeCredit >= 86400)
                                timetext += string.Format("{0}d ", t.Days);
                            if (t.Hours > 0)
                                timetext += string.Format("{0:D2}h ", t.Hours);
                            if (t.Minutes > 0)
                                timetext += string.Format("{0:D2}m ", t.Minutes);
                            if (t.Seconds > 0)
                                timetext += string.Format("{0:D2}s ", t.Seconds);

                            string limitUp = string.Empty;
                            if (billingType.VolumeUp.Equals(0))
                                limitUp = "--";
                            else
                                limitUp = string.Format("{0} Mb", billingType.VolumeUp);

                            string limitDown = string.Empty;
                            if (billingType.VolumeDown.Equals(0))
                                limitDown = "--";
                            else
                                limitDown = string.Format("{0} Mb", billingType.VolumeDown);

                            if (lang.Equals("es"))
                            {
                                finishlabel.Text = "Enhorabuena";
                                finish.Text = "Continuar Navegando";
                                //text += string.Format("<b>Tiempo de Conexión:</b> {0}<br/><b>Validez del ticket:</b> {1} horas<br/><b>Número máximo de dispositivo:</b> {2}<br/><b>Velocidad de Subida:</b> {3} kbps<br/><b>Velocidad de Bajada:</b> {4} kbps<br/>",
                                //        timetext, billingType.ValidTill, billingType.MaxDevices, billingType.BWUp, billingType.BWDown);

                                text += string.Format("<b>Nombre de usuario: </b>{0}<br />", user.Name);
                                text += string.Format("<b>Contraseña: </b>{0}<br />", user.Password);
                                text += string.Format("<b>Fecha de expiración:</b> {0} <br/>", user.ValidTill.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss"));
                                text += string.Format("<b>Tiempo de Conexión:</b> {0}<br/>", timetext);
                            }
                            else
                            {
                                finishlabel.Text = "Congratulations";
                                //text += string.Format("<b>Time Credit:</b> {0}<br/><b>Valid Till:</b> {1} hours<br/><b>Max devices:</b> {2}<br/><b>Bandwidth Up:</b> {3} kbps<br/><b>Bandwidth Down:</b> {4} kbps<br/>",
                                //    timetext, billingType.ValidTill, billingType.MaxDevices, billingType.BWUp, billingType.BWDown);
                                text += string.Format("<b>UserName: </b>{0}<br />", user.Name);
                                text += string.Format("<b>Password: </b>{0}<br />", user.Password);
                                text += string.Format("<b>Valid Till:</b> {0}<br/>", user.ValidTill.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss"));
                                text += string.Format("<b>Time Credit:</b> {0}<br/>", timetext);


                            }
                            literalText.Text = string.Format("<img src=\"../resources/images/{0}/{1}-logo.png\" alt=\"logo\" style=\"width:150px\"  /><br /><br />", hotel.IdHotel, payPalTransaction.IdLocation);
                            literalText.Text += text;
                            if (string.IsNullOrEmpty(billingType.UrlLanding))
                            {
                                if (string.IsNullOrEmpty(location.UrlLanding))
                                    finish.NavigateUrl = string.Format("{0}?username={1}&password={2}&dst={3}", payPalTransaction.CallerIP, payPalTransaction.UserName, payPalTransaction.Password, hotel.UrlHotel);
                                else
                                    finish.NavigateUrl = string.Format("{0}?username={1}&password={2}&dst={3}", payPalTransaction.CallerIP, payPalTransaction.UserName, payPalTransaction.Password, location.UrlLanding);
                            }
                            else
                                finish.NavigateUrl = string.Format("{0}?username={1}&password={2}&dst={3}", payPalTransaction.CallerIP, payPalTransaction.UserName, payPalTransaction.Password, billingType.UrlLanding);
                            //finish.NavigateUrl = location.UrlLanding;

                            util.sendMailRoot(payPalTransaction.IdHotel, payPalTransaction.IdBillingType, payPalTransaction.Email, payPalTransaction.UserName, payPalTransaction.Password, lang.Substring(0, 2));

                            log.RadiusResponse = "OK";
                            log.NomadixResponse = "OK";


                            //VALIDAMOS EL USERAGENT
                            UserAgentsTemp temp = BillingController.GetUserAgentTemp(payPalTransaction.MAC);
                            if (temp.IdTemp != 0)
                            {
                                UserAgents userAgent = BillingController.GetUserAgent(temp.IdUserAgent);
                                List<UserAgents> listUA = BillingController.GetUserAgents(userAgent.String);

                                userAgent.Valid = userAgent.Valid + 1; ;
                                BillingController.SaveUserAgent(userAgent);
                                foreach (UserAgents x in listUA)
                                {
                                    x.Valid = x.Valid + 1; ;
                                    BillingController.SaveUserAgent(x);
                                }

                                //BillingController.DeleteUserAgentTemp(temp);
                            }

                            List<UserAgents> uAtoBlackList = BillingController.GetUserAgentsToBlackList(Int32.Parse(ConfigurationManager.AppSettings["MaxAttemps"].ToString()));
                            foreach (UserAgents uA in uAtoBlackList)
                            {
                                BlackListUserAgents bUA = BillingController.GetBlackUserAgent(uA.String);
                                if (bUA.IdBlackUserAgent.Equals(0))
                                {
                                    BlackListUserAgents obj = new BlackListUserAgents();
                                    obj.String = uA.String;
                                    BillingController.SaveBlackListUserAgent(obj);
                                }
                            }
                        }
                        else //EL CARGO NO SE CREO CORRECTAMENTE
                        {
                            log.NomadixResponse = "NA";
                            log.RadiusResponse = "NA";

                            CaptivePortalLogInternal internalLog = new CaptivePortalLogInternal();
                            internalLog.IdHotel = payPalTransaction.IdHotel;
                            internalLog.IdLocation = payPalTransaction.IdLocation;
                            internalLog.NSEId = payPalTransaction.UI;
                            internalLog.UserName = payPalTransaction.UserName;
                            internalLog.Password = payPalTransaction.Password;
                            internalLog.CallerID = payPalTransaction.MAC;
                            internalLog.Date = DateTime.Now.ToUniversalTime();
                            internalLog.Page = "success paypal";

                            internalLog.NomadixResponse = "ERROR EL CARGO NO SE GRABÓ CORRECTAMENTE";
                            internalLog.RadiusResponse = "NA";

                            BillingController.SaveCaptivePortalLogInternal(internalLog);
                        }

                        BillingController.SaveCaptivePortalLog(log);
                    }
                    else //PAYPAL YA NOTIFICÓ EL PAGO y SE LOGUEÓ
                    {
                        Rooms room = BillingController.GetRoom(payPalTransaction.IdHotel, "DEFAULT");
                        BillingTypes billingType = BillingController.ObtainBillingType(payPalTransaction.IdBillingType);
                        Users user = BillingController.GetRadiusUser(payPalTransaction.UserName, payPalTransaction.IdHotel);

                        //ActiveSessions session = BillingController.GetActiveSession(macformated, payPalTransaction.UserName.ToUpper());
                        //if (!string.IsNullOrEmpty(session.AcctSessionID))
                        //{
                        string timetext = string.Empty;
                        TimeSpan t = TimeSpan.FromSeconds(billingType.TimeCredit / billingType.MaxDevices);
                        if (billingType.TimeCredit >= 86400)
                            timetext += string.Format("{0}d ", t.Days);
                        if (t.Hours > 0)
                            timetext += string.Format("{0:D2}h ", t.Hours);
                        if (t.Minutes > 0)
                            timetext += string.Format("{0:D2}m ", t.Minutes);
                        if (t.Seconds > 0)
                            timetext += string.Format("{0:D2}s ", t.Seconds);

                        string limitUp = string.Empty;
                        if (billingType.VolumeUp.Equals(0))
                            limitUp = "--";
                        else
                            limitUp = string.Format("{0} Mb", billingType.VolumeUp);

                        string limitDown = string.Empty;
                        if (billingType.VolumeDown.Equals(0))
                            limitDown = "--";
                        else
                            limitDown = string.Format("{0} Mb", billingType.VolumeDown);

                        if (lang.Equals("es"))
                        {
                            finishlabel.Text = "Enhorabuena";
                            finish.Text = "Continuar Navegando";
                            //text += string.Format("<b>Tiempo de Conexión:</b> {0}<br/><b>Validez del ticket:</b> {1} horas<br/><b>Número máximo de dispositivo:</b> {2}<br/><b>Velocidad de Subida:</b> {3} kbps<br/><b>Velocidad de Bajada:</b> {4} kbps<br/>",
                            //        timetext, billingType.ValidTill, billingType.MaxDevices, billingType.BWUp, billingType.BWDown);
                            text += string.Format("<b>Nombre de usuario: </b>{0}<br />", user.Name);
                            text += string.Format("<b>Contraseña: </b>{0}<br />", user.Password);
                            text += string.Format("<b>Fecha de expiración:</b> {0} <br/>", user.ValidTill.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss"));
                            text += string.Format("<b>Tiempo de Conexión:</b> {0}", timetext);

                        }
                        else
                        {
                            finishlabel.Text = "Congratulations";
                            //text += string.Format("<b>Time Credit:</b> {0}<br/><b>Valid Till:</b> {1} hours<br/><b>Max devices:</b> {2}<br/><b>Bandwidth Up:</b> {3} kbps<br/><b>Bandwidth Down:</b> {4} kbps<br/>",
                            //    timetext, billingType.ValidTill, billingType.MaxDevices, billingType.BWUp, billingType.BWDown);
                            text += string.Format("<b>UserName: </b>{0}<br />", user.Name);
                            text += string.Format("<b>Password: </b>{0}<br />", user.Password);
                            text += string.Format("<b>Valid Till:</b> {0}<br />", user.ValidTill.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss"));
                            text += string.Format("<b>Time Credit:</b> {0}<br/>", timetext);
                        }

                        literalText.Text = string.Format("<img src=\"./resources/images/{0}/{1}-logo.png\" alt=\"logo\" style=\"width:150px\"  /><br /><br />", hotel.IdHotel, payPalTransaction.IdLocation);
                        literalText.Text += text;
                        if (string.IsNullOrEmpty(billingType.UrlLanding))
                        {
                            if (string.IsNullOrEmpty(location.UrlLanding))
                                finish.NavigateUrl = string.Format("{0}?username={1}&password={2}&dst={3}", payPalTransaction.CallerIP, payPalTransaction.UserName, payPalTransaction.Password, hotel.UrlHotel);
                            else
                                finish.NavigateUrl = string.Format("{0}?username={1}&password={2}&dst={3}", payPalTransaction.CallerIP, payPalTransaction.UserName, payPalTransaction.Password, location.UrlLanding);
                        }
                        else
                            finish.NavigateUrl = string.Format("{0}?username={1}&password={2}&dst={3}", payPalTransaction.CallerIP, payPalTransaction.UserName, payPalTransaction.Password, billingType.UrlLanding);

                        //log.RadiusResponse = "OK";
                        //}
                    }

                }

            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}