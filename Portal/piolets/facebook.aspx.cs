﻿using Portal.clases;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Portal.piolets
{
    public partial class facebook : System.Web.UI.Page
    {
        protected string facebookid = string.Empty;
        protected string facebookname = string.Empty;
        protected string facebooksurname = string.Empty;
        protected string facebookemail = string.Empty;
        protected string facebooklocale = string.Empty;
        protected string facebookgender = string.Empty;
        protected string facebookbirthday = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["code"] != "")
            {
                var obj = GetFacebookUserData(Request.QueryString["code"]);
            }
        }

        protected List<Facebook.User> GetFacebookUserData(string code)
        {
            List<Facebook.User> currentUser = new List<Facebook.User>();
            try
            {

                // Exchange the code for an access token
                Uri targetUri = new Uri("https://graph.facebook.com/v2.11/oauth/access_token?client_id=" + ConfigurationManager.AppSettings["FacebookAppId"] + "&client_secret=" + ConfigurationManager.AppSettings["FacebookAppSecret"] + "&redirect_uri=" + HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host + HttpContext.Current.Request.Url.AbsolutePath + "&scope=email,public_profile,user_birthday,user_likes&code=" + code);
                HttpWebRequest at = (HttpWebRequest)HttpWebRequest.Create(targetUri);

                System.IO.StreamReader str = new System.IO.StreamReader(at.GetResponse().GetResponseStream());
                string token = str.ReadToEnd().ToString().Replace("access_token=", "");

                // Split the access token and expiration from the single string
                string[] combined = token.Split('&');
                string accessToken = combined[0];

                // Exchange the code for an extended access token
                Uri eatTargetUri = new Uri("https://graph.facebook.com/v2.11/oauth/access_token?grant_type=fb_exchange_token&client_id=" + ConfigurationManager.AppSettings["FacebookAppId"] + "&client_secret=" + ConfigurationManager.AppSettings["FacebookAppSecret"] + "&fb_exchange_token=" + accessToken.Split(':')[1].Split(',')[0].Replace('"', ' ').Trim());// + accessToken);
                HttpWebRequest eat = (HttpWebRequest)HttpWebRequest.Create(eatTargetUri);

                StreamReader eatStr = new StreamReader(eat.GetResponse().GetResponseStream());
                string eatToken = eatStr.ReadToEnd().ToString().Replace("access_token=", "");

                // Split the access token and expiration from the single string
                string[] eatWords = eatToken.Split('&');
                string extendedAccessToken = eatWords[0];

                // Request the Facebook user information
                Uri targetUserUri = new Uri("https://graph.facebook.com/v2.11/me?fields=email,first_name,last_name,gender,locale,link,birthday&access_token=" + accessToken.Split(':')[1].Split(',')[0].Replace('"', ' ').Trim());// accessToken);
                //Uri targetUserUri = new Uri("https://graph.facebook.com/me?fields=email,public_profile,user_birthday,user_likes&access_token=" + accessToken.Split(':')[1].Split(',')[0].Replace('"', ' ').Trim());// accessToken);
                HttpWebRequest user = (HttpWebRequest)HttpWebRequest.Create(targetUserUri);

                // Read the returned JSON object response
                StreamReader userInfo = new StreamReader(user.GetResponse().GetResponseStream());
                string jsonResponse = string.Empty;
                jsonResponse = userInfo.ReadToEnd();

                // Deserialize and convert the JSON object to the Facebook.User object type
                JavaScriptSerializer sr = new JavaScriptSerializer();
                string jsondata = jsonResponse;
                Facebook.User converted = sr.Deserialize<Facebook.User>(jsondata);

                facebookid = converted.id;
                facebookemail = converted.email;
                facebookname = converted.first_name;
                facebooksurname = converted.last_name;
                facebookgender = converted.gender;
                facebooklocale = converted.locale;
                facebookbirthday = converted.birthday;

                // Write the user data to a List

                currentUser.Add(converted);

                // Return the current Facebook user
                return currentUser;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}