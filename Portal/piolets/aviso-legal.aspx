﻿<%@ Page Title="" Language="C#" MasterPageFile="~/resources/master/PortalMaster.Master" AutoEventWireup="true" CodeBehind="aviso-legal.aspx.cs" Inherits="Portal.piolets.aviso_legal" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        $(document).ready(function (e) {

            var lang = navigator.browserLanguage ? navigator.browserLanguage : navigator.language;
            var langcode = lang.substring(0, 2);
            var parameters = getParameters();
            var dataString = 'action=avisolegal&idhotel=' + $("#idhotel").val() + "&idlocation=" + $("#idlocation").val() + "&lang=" + langcode + '&type=' + parameters.iddisclaimertype;
            $.ajax({
                url: "../resources/handlers/PioletsHandler.ashx",
                data: dataString,
                contentType: "application/json; charset=utf-8",
                dataType: "text",
                success: function (pReturn) {
                    var lReturn = JSON.parse(pReturn);
                    $("#text").html(lReturn.Text)
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#loading").html("Su ha producido un error durante el envio. Por favor intentelo más tarde.");
                    alert('error: ' + xhr.statusText);
                }

            });

        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <div class="container">
           <div class="row">
               <div class="col-sm-8 col-sm-offset-2" id="text"></div>
                <div class="col-sm-4 col-sm-offset-4"><a href="default.aspx" class="btn btn-primary btn-lg btn-block" id="buttonbacklegal">Back</a></div>
           </div>
       </div>
</asp:Content>
