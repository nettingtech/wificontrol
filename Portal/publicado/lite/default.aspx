﻿<%@ Page Title="" Language="C#" MasterPageFile="~/resources/master/PortalMaster.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Portal.lite._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <link href="assets/css/style.css" rel="stylesheet" />
        <script type="text/javascript">

            $(document).ready(function (e) {

                var scrollcomplete = false;
                var parameters = getParameters();
                var val = "idhotel=" + $("#idhotel").val() + "&idlocation=" + $("#idlocation").val() + "&mac=" + $("#mac").val() + "&origin=" + $("#origin").val();
                createCookie("mikrotik", val, 1);

                var lang = navigator.browserLanguage ? navigator.browserLanguage : navigator.language;
                var langcode = lang.substring(0, 2);

                if ((langcode != 'es') && (langcode != 'en'))
                    langcode = $("#lang").val();

                if (langcode == 'es') {
                    //TEXTOS GENERALES
                    $("#freebutton").html('<i class="fa fa-pencil fa-3x"></i><br />Formulario</a>');
                }
                var dataString = 'action=fullsite&idhotel=' + $("#idhotel").val() + "&idlocation=" + $("#idlocation").val() + "&lang=" + langcode;
                $.ajax({
                    url: "../resources/handlers/LiteHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    success: function (pReturn) {
                        var lReturn = JSON.parse(pReturn);
                        $("#locationtext").html(lReturn.LocationText);
                        $("#imagenlogo").attr("src", "/resources/images/" + $("#idhotel").val() + "/" + $("#idlocation").val() + "-logo.png");
                        $("#noacceptimage").attr("src", "/resources/images/" + $("#idhotel").val() + "/" + $("#idlocation").val() + "-logo.png");
                        $("#logomodalfinish").attr("src", "/resources/images/" + $("#idhotel").val() + "/" + $("#idlocation").val() + "-logo.png");
                        $("#disclaimerimage").attr("src", "/resources/images/" + $("#idhotel").val() + "/" + $("#idlocation").val() + "-logo.png");

                        $("#disclaimertext").html(lReturn.Disclaimer);
                        $("#disclaimermandatorytext").html(lReturn.Disclaimer);
                        $("#mandatorysurvey").val(lReturn.MandatorySurvey);
                        $("#freeaccess").val(lReturn.freeAccess.IdBillingType);
                        $("#socialaccess").val(lReturn.socialAccess.IdBillingType);

                        var text = '';
                        text += '<div role="form">';
                        text += '<div class="form-group"><label id="labelemail">Email</label><input name="email" id="email" type="text" class="form-control" placeholder="Email" /></div>';
                        $.each(lReturn.survey, function (index, item) {
                            text += '<div class="form-group"><label id="labelemail">' + item["Question"] + '</label>';
                            if (item["Type"] == 'text')
                                text += '<input type="text" id="' + item["Order"] + '" rel="survey" class="form-control" placeholder="' + item["Question"] + '" question="' + item["Question"] + '" /></div>';
                            else if (item["Type"] == 'select') {
                                var parts = item["Values"].split('/');
                                text += "<td><select id='" + item["Order"] + "' rel='survey' class='form-control' question='" + item["Question"] + "' >";
                                $.each(parts, function (key, value) {
                                    text += "<option value='" + value + "'>" + value + "</option>";
                                });

                                text += '</select></div>';
                            }
                        });
                        text += '</div>';
                        $("#survey").html(text);

                        $.each(lReturn.redes, function (index, item) {
                            if (item["Active"])
                                $("#" + item["Name"] + "row").show();
                            else
                                $("#" + item["Name"] + "row").hide();
                        });

                        if ($("#error").val() != "") {
                            if (langcode == 'es') {
                                switch ($("#error").val().toUpperCase()) {
                                    case 'UNKNOWN USER': $("#message").html('Usuario Desconocido'); break;
                                    case 'NO TIME CREDIT': $("#message").html('Tiempo de conexión agotado'); break;
                                    case 'TICKET EXPIRED': $("#message").html('Ticket Caducado'); break;
                                    case 'TOO MANY DEVICES': $("#message").html('Demasiados dispositivos'); break;
                                    case 'SORRY, NO MORE USERS ALLOWED': $("#message").html('Lo sentimos, no se permiten más usuarios'); break;
                                    case 'WRONG PASSWORD': $("#message").html('Contraseña incorrecta'); break;
                                    case 'UPLOAD QUOTA EXCEEDED': $("#message").html('Cuota de datos de subida agotada'); break;
                                    case 'DOWNLOAD QUOTA EXCEEDED': $("#message").html('Cuota de datos de bajada agotada'); break;

                                    default: $("#message").html($("#error").val());
                                }
                            }
                            else
                                $("#message").html($("#error").val());

                            $("#myModalError").modal('show');
                            $("#myModal").modal('hide');
                            $("#modalWait").modal('hide');
                            $("#principal").fadeIn("slow");

                        }
                        else {
                            if (lReturn.DisclaimerMandatory) {
                                $("#modalWait").fadeOut("slow");
                                $("#noaccept").fadeOut("slow");
                                $("#myModal").modal('show');
                            }
                            else {
                                $("#disclaimermandatory").show();
                                $("#myModal").modal('hide');
                                $("#modalWait").fadeOut("slow");
                                $("#noaccept").fadeOut("fast");
                                $("#principal").fadeIn("slow");
                            }
                        }

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#loading").html("Su ha producido un error durante el envio. Por favor intentelo más tarde.");
                        alert('error: ' + xhr.statusText);
                    }

                });

                $("#freebutton").click(function (e) {
                    $("#myModalSurvey").modal('show');
                });

                $('#surveysave').click(function (e) {
                    $("#myModalSurvey").modal('hide');


                    var dataString = 'action=allow&idhotel=' + $("#idhotel").val() + '&idlocation=' + $("#idlocation").val() + '&mac=' + $("#mac").val() + '&origin=' + $("#origin").val();
                    $.ajax({
                        url: "../resources/handlers/LiteHandler.ashx",
                        data: dataString,
                        contentType: "application/json; charset=utf-8",
                        dataType: "text",
                        beforeSend: function () {
                            $("#MyModalWait").modal('show');
                        },
                        complete: function () {
                            $("#MyModalWait").modal('hide');
                        },
                        success: function (pResult) {
                            var lResult = JSON.parse(pResult);

                            if (lResult.allow == 1) {
                                $("#finishbutton").attr("href", lResult.Url);
                                $("#finishInformation").append($("#freeaccessinformation").html());

                                if (langcode == 'es') {
                                    $("#finishlabel").html("Enhorabuena");
                                    $("#finishInformation").append('<b>Nombre de usuario: </b>' + lResult.UserName + '<br />');
                                    $("#finishInformation").append('<b>Contraseña: </b>' + lResult.Password + '<br />');
                                    $("#finishInformation").append('<b>Fecha de expiración: </b>' + lResult.ValidTill + '<br />');
                                    $("#finishbutton").val("Continuar navegando");
                                }
                                else {
                                    $("#finishInformation").append('<b>UserName: </b>' + lResult.UserName + '<br />');
                                    $("#finishInformation").append('<b>Password: </b>' + lResult.Password + '<br />');
                                    $("#finishInformation").append('<b>Valid Till: </b>' + lResult.ValidTill + '<br />');
                                    $("#finishbutton").val("Continue browsing");
                                }

                                $('#myModalFinish').modal('show');
                            }
                            else if (lResult.allow == 2) {
                                if (langcode != 'es') {
                                    $("#message").html("No more free access allowed now.");
                                }
                                else {
                                    $("#message").html("No se permiten más accesos gratuitos en este periodo.");
                                }
                                $("#myModalError").modal('show');
                            }
                            else if (lResult.allow == 0) {

                                var email = $("#email").val();
                                if (isValidEmailAddress(email) != true) {
                                    if (langcode == 'es') {
                                        $("#message2").html("El formato del correo electrónico no es correcto").removeClass().addClass('alert alert-danger').show();
                                    }
                                    else {
                                        $("#message2").html("Email format is not correct").removeClass().addClass('alert alert-danger').show();
                                    }
                                    $("#myModalMandatory").modal('show');
                                }
                                else {
                                    var survey = 'loginform:';
                                    var parameters = getParameters();
                                    var valid = true;
                                    $('input[rel^="survey"]').each(function (input) {
                                        survey += $(this).attr('question') + '=' + $(this).val() + '|';
                                        if ($(this).val() == '')
                                            valid = false;
                                    });

                                    $('select[rel^="survey"]').each(function (input) {
                                        survey += $(this).attr('question') + '=' + $(this).val() + '|';
                                        if ($(this).val() == '')
                                            valid = false;
                                    });

                                    if (valid == false) {
                                        if (langcode == 'es') {
                                            $("#message2").html("Todos los campos son obligatorios.").removeClass().addClass('alert alert-danger').show();
                                        }
                                        else {
                                            $("#message2").html("All fields are mandatory.").removeClass().addClass('alert alert-danger').show();
                                        }
                                        $("#myModalMandatory").modal('show');
                                    }
                                    else {

                                        $("#myModalSurvey").modal('hide');

                                        var dataString = 'action=freeaccess&idhotel=' + $("#idhotel").val() + '&survey=' + survey + '&email=' + email + '&idlocation=' + $("#idlocation").val() + '&mac=' + $("#mac").val() + '&origin=' + $("#origin").val() + '&language=' + navigator.language + "&idbilling=" + $("#freeaccess").val();
                                        $.ajax({
                                            url: "../resources/handlers/LiteHandler.ashx",
                                            data: dataString,
                                            contentType: "application/json; charset=utf-8",
                                            dataType: "text",
                                            beforeSend: function () {
                                                $("#MyModalWait").modal('show');
                                            },
                                            complete: function () {
                                                $("#MyModalWait").modal('hide');
                                            },
                                            success: function (pResult) {
                                                var lResult = JSON.parse(pResult);

                                                if (lResult.IdUser == -1) {
                                                    if (langcode != 'es') {
                                                        $("#message").html("No more free access are allowed now.");
                                                    }
                                                    else {
                                                        $("#message").html("No se permiten más accesos gratuitos en este periodo.");
                                                    }
                                                    $("#myModalError").modal('show');
                                                }
                                                else if (lResult.IdUser == -3) {
                                                    if (langcode != 'es') {
                                                        $("#message").html("We are currently experiencing a problem, please try again later.");
                                                    }
                                                    else {
                                                        $("#message").html("Tenemos un problema temporal, intentelo de nuevo más tarde.");
                                                    }
                                                    $("#myModalError").modal('show');
                                                }
                                                else if (lResult.IdUser == 0) {
                                                    if (langcode != 'es') {
                                                        $("#message").html("We are currently experiencing a problem, please try again later.");
                                                    }
                                                    else {
                                                        $("#message").html("Tenemos un problema temporal, intentelo de nuevo más tarde.");
                                                    }
                                                    $("#myModalError").modal('show');
                                                }
                                                else {
                                                    $("#finishbutton").attr("href", lResult.Url);
                                                    $("#finishInformation").append($("#freeaccessinformation").html());

                                                    if (langcode == 'es') {
                                                        $("#finishlabel").html("Enhorabuena");
                                                        $("#finishInformation").append('<b>Nombre de usuario: </b>' + lResult.UserName + '<br />');
                                                        $("#finishInformation").append('<b>Contraseña: </b>' + lResult.Password + '<br />');
                                                        $("#finishInformation").append('<b>Fecha de expiración: </b>' + lResult.ValidTill + '<br />');
                                                        $("#finishbutton").val("Continuar navegando");
                                                    }
                                                    else {
                                                        $("#finishInformation").append('<b>UserName: </b>' + lResult.UserName + '<br />');
                                                        $("#finishInformation").append('<b>Password: </b>' + lResult.Password + '<br />');
                                                        $("#finishInformation").append('<b>Valid Till: </b>' + lResult.ValidTill + '<br />');
                                                        $("#finishbutton").val("Continue browsing");
                                                    }

                                                    $('#myModalFinish').modal('show');
                                                }

                                            },
                                            error: function (xhr, ajaxOptions, thrownError) {
                                                $("#loading").html("Su ha producido un error durante el envio. Por favor intentelo más tarde.");
                                                alert('error: ' + xhr.statusText);
                                                $("#nextstep3").show();
                                                $("#backstep3").show();

                                            }
                                        });
                                    }
                                }

                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#loading").html("Su ha producido un error durante el envio. Por favor intentelo más tarde.");
                            alert('error: ' + xhr.statusText);
                            $("#nextstep3").show();
                            $("#backstep3").show();

                        }
                    });

                });

                $("#finishbutton").click(function (e) {
                    e.preventDefault();
                    var url = $("#finishbutton").attr("href");
                    window.location.href = url;

                });

                $("#instagrambutton").click(function (e) {
                    var dataString = 'action=allow&idhotel=' + $("#idhotel").val() + '&idlocation=' + $("#idlocation").val() + '&mac=' + $("#mac").val() + '&origin=' + $("#origin").val();
                    $.ajax({
                        url: "../resources/handlers/LiteHandler.ashx",
                        data: dataString,
                        contentType: "application/json; charset=utf-8",
                        dataType: "text",
                        beforeSend: function () {
                            $("#MyModalWait").modal('show');
                        },
                        complete: function () {
                            $("#MyModalWait").modal('hide');
                        },
                        success: function (pResult) {
                            var lResult = JSON.parse(pResult);

                            if (lResult.allow == 1) {
                                $("#finishbutton").attr("href", lResult.Url);
                                $("#finishInformation").append($("#freeaccessinformation").html());

                                if (langcode == 'es') {
                                    $("#finishlabel").html("Enhorabuena");
                                    $("#finishInformation").append('<b>Nombre de usuario: </b>' + lResult.UserName + '<br />');
                                    $("#finishInformation").append('<b>Contraseña: </b>' + lResult.Password + '<br />');
                                    $("#finishInformation").append('<b>Fecha de expiración: </b>' + lResult.ValidTill + '<br />');
                                    $("#finishbutton").val("Continuar navegando");
                                }
                                else {
                                    $("#finishInformation").append('<b>UserName: </b>' + lResult.UserName + '<br />');
                                    $("#finishInformation").append('<b>Password: </b>' + lResult.Password + '<br />');
                                    $("#finishInformation").append('<b>Valid Till: </b>' + lResult.ValidTill + '<br />');
                                    $("#finishbutton").val("Continue browsing");
                                }

                                $('#myModalFinish').modal('show');
                            }
                            else if (lResult.allow == 2) {
                                if (langcode != 'es') {
                                    $("#message").html("No more free access allowed now.");
                                }
                                else {
                                    $("#message").html("No se permiten más accesos gratuitos en este periodo.");
                                }
                                $("#myModalError").modal('show');
                            }
                            else if (lResult.allow == 0) {
                                var va2 = "idhotel=" + $("#idhotel").val() + "&idlocation=" + $("#idlocation").val() + "&mac=" + $("#mac").val() + "&origin=" + $("#origin").val() + "&idbillingtype=" + $("#socialaccess").val();
                                createCookie("mikrotik", va2, 1);

                                var uri = window.location.origin + "/lite/instagram.aspx";
                                var url = "https://www.instagram.com/oauth/authorize/?client_id=a3d2212db0a1455388b370858fe80e64&redirect_uri=" + uri + "&response_type=token";
                                window.location.href = url;
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#loading").html("Su ha producido un error durante el envio. Por favor intentelo más tarde.");
                            alert('error: ' + xhr.statusText);
                            $("#nextstep3").show();
                            $("#backstep3").show();

                        }
                    });
                });

                $("#facebookbutton").click(function (e) {
                    e.preventDefault();
                    var dataString = 'action=allow&idhotel=' + $("#idhotel").val() + '&idlocation=' + $("#idlocation").val() + '&mac=' + $("#mac").val() + '&origin=' + $("#origin").val();
                    $.ajax({
                        url: "../resources/handlers/LiteHandler.ashx",
                        data: dataString,
                        contentType: "application/json; charset=utf-8",
                        dataType: "text",
                        beforeSend: function () {
                            $("#MyModalWait").modal('show');
                        },
                        complete: function () {
                            $("#MyModalWait").modal('hide');
                        },
                        success: function (pResult) {
                            var lResult = JSON.parse(pResult);

                            if (lResult.allow == 1) {
                                $("#finishbutton").attr("href", lResult.Url);
                                $("#finishInformation").append($("#freeaccessinformation").html());

                                if (langcode == 'es') {
                                    $("#finishlabel").html("Enhorabuena");
                                    $("#finishInformation").append('<b>Nombre de usuario: </b>' + lResult.UserName + '<br />');
                                    $("#finishInformation").append('<b>Contraseña: </b>' + lResult.Password + '<br />');
                                    $("#finishInformation").append('<b>Fecha de expiración: </b>' + lResult.ValidTill + '<br />');
                                    $("#finishbutton").val("Continuar navegando");
                                }
                                else {
                                    $("#finishInformation").append('<b>UserName: </b>' + lResult.UserName + '<br />');
                                    $("#finishInformation").append('<b>Password: </b>' + lResult.Password + '<br />');
                                    $("#finishInformation").append('<b>Valid Till: </b>' + lResult.ValidTill + '<br />');
                                    $("#finishbutton").val("Continue browsing");
                                }

                                $('#myModalFinish').modal('show');
                            }
                            else if (lResult.allow == 2) {
                                if (langcode != 'es') {
                                    $("#message").html("No more free access allowed now.");
                                }
                                else {
                                    $("#message").html("No se permiten más accesos gratuitos en este periodo.");
                                }
                                $("#myModalError").modal('show');
                            }
                            else if (lResult.allow == 0) {
                                e.preventDefault();
                                var parameters = getParameters();

                                window.fbAsyncInit = function () {
                                    FB.init({
                                        appId: '233231503535149',
                                        status: true,
                                        cookie: true,
                                        xfbml: true,
                                        version: 'v2.11' // 
                                    });

                                    var val = "idhotel=" + $("#idhotel").val() + ",idlocation=" + $("#idlocation").val() + ",mac=" + $("#mac").val() + ",origin=" + $("#origin").val() + ",idbillingtype=" + $("#socialaccess").val();
                                    var uri = window.location.origin + "/lite/facebook.aspx";
                                    var va2 = "idhotel=" + $("#idhotel").val() + "&idlocation=" + $("#idlocation").val() + "&mac=" + $("#mac").val() + "&origin=" + $("#origin").val() + "&idbillingtype=" + $("#socialaccess").val();
                                    createCookie("mikrotik", va2, 1);

                                    FB.getLoginStatus(function (response) {
                                        if (response.status === 'connected') {
                                            window.location.href = uri;
                                        } else {
                                            var facebookUri = "https://www.facebook.com/v2.11/dialog/oauth?client_id=233231503535149&redirect_uri=" + uri + "&response_type=code&scope=email,public_profile,user_birthday,user_likes&state=\"{" + val + "\"}";
                                            window.location = encodeURI(facebookUri);
                                        }
                                    }, { scope: 'email,public_profile,user_birthday,user_likes' });
                                };
                                (function (d, s, id) {
                                    var js, fjs = d.getElementsByTagName(s)[0];
                                    if (d.getElementById(id)) { return; }
                                    js = d.createElement(s); js.id = id;
                                    js.src = "../resources/scripts/fb_sdk.js";
                                    fjs.parentNode.insertBefore(js, fjs);
                                }(document, 'script', 'facebook-jssdk'));
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#loading").html("Su ha producido un error durante el envio. Por favor intentelo más tarde.");
                            alert('error: ' + xhr.statusText);
                            $("#nextstep3").show();
                            $("#backstep3").show();

                        }
                    });
                });

                $("#rejectconditions").click(function (e) {
                    $("#principal").fadeOut("slow");
                    $("#noaccept").fadeIn("slow");
                    $("#myModal").modal('hide');
                });

                $("#retrybutton").click(function (e) {
                    $("#myModal").modal('show');
                });

                $("#disclaimerMandatoryButton").click(function (e) {
                    $("#myModalMandatoryDisclaimer").modal('show');
                });

                $("#modalscroll").bind('scroll', chk_scroll);

                $("#acceptconditionsbutton").click(function (e) {
                    if ($("#acceptcheck").is(":checked") && $("#olderthancheck").is(":checked") && scrollcomplete == true) //  ($("#modalscroll")[0].scrollHeight - $("#modalscroll").scrollTop() == $("#modalscroll").outerHeight()))
                    {
                        $("#myModal").modal('hide');
                        $("#noaccept").fadeOut("fast");
                        $("#principal").fadeIn("slow");
                    }
                    else {
                        switch (lang.substring(0, 2)) {
                            case 'es': $("#message").html("Por favor, antes de Aceptar debe leer totalmente el texto (utilice el control deslizante) y aceptar la Politica de Privacidad y las Condiciones de Uso del servicio."); break;
                            default: $("#message").html("Please, before proceeding you must read completely the legal text and accept the Privacy Policy and the Terms of Use."); break;
                        }
                        $("#myModalError").modal("show");
                    }
                });

                function chk_scroll(e) {
                    var elem = $(e.currentTarget);
                    if (elem.scrollTop() >= (elem[0].scrollHeight - elem.outerHeight()) * 0.8) {
                        scrollcomplete = true;
                    }
                }

                $('#myModal').on('show.bs.modal', function () {
                    $('#modalscroll').css('overflow-y', 'auto');
                    $('#modalscroll').css('max-height', $(window).height() * 0.25);
                });


            });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="fb-root"></div>
    <div class="page-wrapper">

    <div class="row" id="noaccept" style="display:none">
            <div class="col-xs-12 col-sm-12 col-md-12">
                  <div class="panel panel-default">
                  <div class="panel-body text-center">
                      <p class="text-center" id="sorrytext">We are sorry</p>  
                      <p class="text-center"> <img src="#" style="width:150px" id="noacceptimage"/></p> 
                        <p class="text-center" id="noaccepttext">In order to use the service you must accept the Terms of Use</p>
                        <p class="text-center"><button type="button" class="btn btn-success"  id="retrybutton">Try Again</button></p>
                    </div>
                </div>
            </div>
        </div>

      <div class="row" id="principal" style="display:none;">
        <div class="col col-l col-50 col-50-md">
            <div class="content-login">
                <div class="logo-wanup__wrapper">
                    <img src="#" id="imagenlogo" alt="" />
                </div>

                <div id="locationtext">
                    <div class="section-title">
                        <span>¡Hola!</span>
                    </div>
                    <div class="section-text">
                        <p><strong>Texto</strong></p> 
                    </div>
            </div>
            </div>

       </div> 
        <div class="col col-r col-50 col-50-md">
            <div class="content-login">
                <div class="social-login">
                <div class="row">
                       <div class="col-sm-12">
                          <div class="alert alert-info text-center"><i class="fa fa-wifi fa-3x"></i> <h4 id="text-choose">Elige cómo acceder a nuestra red WIFI</h4></div> 
                       </div>
                </div>
                <div class="row">
                   <div class="col-sm-4" id="facebookrow">
                       <a href="#" id="facebookbutton" class="social-login-button facebook"><i class="fa fa-facebook-square fa-3x"></i><br />Facebook</a>
                   </div>
                    <div class="col-sm-4" id="instagramrow">
                       <a href="#" id="instagrambutton"  class="social-login-button instagram"><i class="fa fa-instagram fa-3x"></i><br />Instagram</a>
                   </div>
                    <div class="col-sm-4" id="twitterrow">
                       <a href="#" id="twitterbutton" class="btn btn-sm social-login-button twitter"><i class="fa fa-twitter fa-3x"></i><br />Twitter</a>
                   </div>
                    <div class="col-sm-4" id="googleplusrow">
                       <a href="#"  id="googleplusbutton" class="btn btn-sm social-login-button googleplus"><i class="fa fa-google-plus-square fa-3x"></i><br />Google+</a>
                   </div>
                    <div class="col-sm-4" id="linkedinrow">
                       <a href="#" id="linkedinbutton" class="btn btn-sm social-login-button linkedin"><i class="fa fa-linkedin-square fa-3x"></i><br />Linkein</a>
                   </div>
                   <div class="col-sm-4" id="freerow">
                       <a href="#" id="freebutton" class="social-login-button form"><i class="fa fa-pencil fa-3x"></i><br />Formulario</a>
                   </div>
                    <div class="col-sm-4" id="loginrow" style="display:none;">
                       <a href="#" id="login" class="social-login-button login"><i class="fa fa-user fa-3x"></i><br />Login</a>
                   </div>

               </div>
                </div>
            </div>
        </div>
    
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" id="disclaimermandatory" style="display:none;">
            <a href="#" id="disclaimerMandatoryButton" class="btn btn-default">Legal Advice</a>
        </div>
        <!-- Modal -->

       <!-- START MODAL PRIVATE POLITICY -->
       <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog" style="overflow-y: initial;">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                   <h4 class="modal-title text-center" id="welcometext">Welcome</h4>
                  <p class="text-center"> <img src="#" style="height:55px" id="disclaimerimage"/></p> 
                  <p class="text-center" id="disclaimerwelcome">Please read the text below and confirm you agree with it.</p>
              </div>
              <div class="modal-body" id="modalscroll">
                    <div class="panel-body" id="disclaimertext">
                    
                      </div>
              </div>
              <div class="modal-footer">
                    <div class="form-check text-left">
                        <input type="checkbox" id="olderthancheck" class="form-check-input"  /><label  class="form-check-label" for="olderthancheck" id="labeloltherthan" style="font-size:12px; display:initial;"> I accept the <a href='legal.aspx?iddisclaimertype=2' id='olderlink' target='_blank'>Privacy Policy</a> and I am over 16-years or have been authorized by my legal tutor</label><br />
                        <input type="checkbox" id="acceptcheck" class="form-check-input"  /><label class="form-check-label" for="acceptcheck" id="labelacceptcheck" style="font-size:12px; display:initial;"> I accept the  <a href='legal.aspx?iddisclaimertype=1' id='termslink' target='_blank'>Terms and Conditions of Use</a></label>
                    </div>
                  <br />
                  <div class="col-xs-6 col-sm-6 col-md-6"><button type="button" class="btn btn-default btn-lg btn-block btn-sm" id="rejectconditions">Decline</button></div>
                  <div class="col-xs-6 col-sm-6 col-md-6"><button type="button" class="btn btn-primary btn-lg btn-block btn-sm" id="acceptconditionsbutton">Accept</button></div>
              </div>
            </div>
          </div>
        </div>
    <!-- END MODAL PRIVATE POLITICY -->

        <div class="modal fade" id="myModalError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title text-center" id="H2">Error</h4>
              </div>
              <div class="modal-body">
                <div id="message" class="alert alert-danger text-center"></div> 
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="errorButton">Close</button>
              </div>
            </div>
          </div>
        </div>


        <div class="modal fade" id="myModalMandatory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title text-center" id="H1">Error</h4>
              </div>
              <div class="modal-body">
                <div id="message2" class="alert alert-danger text-center"></div> 
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger round" data-dismiss="modal" id="errorbutton2">Close</button>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="modalWait" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body">
                    <div style="text-align:center; margin-bottom: 25px;">
                          <svg class="logo-wanup" aria-hidden="true" width="113" height="33" alt="Wanup Logo">
                            <use id="Use1" xlink:href="#icon-logo-wanup-color"></use>
                        </svg>
                    </div>

                    <div class="progress">
                            <div class="progress-bar progress-bar-danger progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                <span class="sr-only">Loading - Waiting please</span>
                            </div>
                        </div>

              </div>
              <div class="modal-footer">
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade in" id="myModalSurvey" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title text-center" id="surveyheader">User Data</h4>
              </div>
              <div class="modal-body" id="survey">
              </div>
              <div class="modal-footer">
                  <div id="Div1"></div>
                  <div class="col-xs-6 col-sm-6 col-md-6"><button type="button" class="btn btn-danger round" data-dismiss="modal" id="surveyclose">Close</button></div>
                  <div class="col-xs-6 col-sm-6 col-md-6"><button type="button" class="btn btn-success round" id="surveysave">Continue</button></div>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="myModalMandatoryDisclaimer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title text-center" id="disclaimermandatoryheader">Legal Advice</h4>
                    </div>
                    <div class="modal-body" id="disclaimermandatorytext">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default round" data-dismiss="modal" id="diclaimermandatoruclose">Close</button>
                      </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="myModalFinish" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title text-center" id="finishlabel">Congratulations</h4>
              </div>
              <div class="modal-body">
                    <div style="text-align:center">
                          <img src="#" id="logomodalfinish" alt="" />
                    </div>
                    <div id="finishInformation" style="text-align:center; display:none;" ><br /><br /></div>
                  <br />
                    <input type="button" id="finishbutton" class="btn btn-success btn-block round" value="Continue browsing" /> 
              </div>
              <div class="modal-footer">
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="MyModalWait" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title text-center" id="waitheader">Please, wait a moment</h4>
                    </div>
                    <div class="modal-body">
                        <div class="progress">
                            <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                <span class="sr-only">Waiting please</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div id="accesstypes" style="display:none"></div>
        <div id="freeaccessinformation" style="display:none"></div>
        <div id="socialaccessinformation" style="display:none"></div>
        <input type="text" hidden="hidden" id="idsocialbillintype" />
        <input type="text" hidden="hidden" id="facebookPageID" />
        <input type="text" hidden="hidden" id="idpromotion" />
        <input type="text" hidden="hidden" id="mandatorysurvey" />
        <input type="text" hidden="hidden" id="freeaccess" />
        <input type="text" hidden="hidden" id="socialaccess" />

</asp:Content>
