﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="redsys.aspx.cs" Inherits="Portal.piolets.redsys" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
 <script src="../resources/scripts/jquery.js"></script>
<link href="../resources/css/bootstrap.css" rel="stylesheet" />
<script src="../resources/scripts/bootstrap.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />

    <title></title>
 <script>
     function getParameters() {
         var searchString = window.location.search.substring(1)
             , params = searchString.split("&")
             , hash = {}
         ;

         for (var i = 0; i < params.length; i++) {
             var val = params[i].split("=");
             hash[unescape(val[0])] = unescape(val[1]);
         }
         return hash;
     }

     $(document).ready(function () {
         var parameters = getParameters();

         if (parameters.lang == 'es') {
             $("#submit").attr('value', 'Aceptar y continuar');
             $("#alert").html('<p style="text-align:center; font-size:40px">ATENCIÓN</p><p>Su conexión va a ser ahora transferida a la página de Visa para procesar el pago. Por favor, siga las instrucciones y <b>NO CIERRE EL NAVEGADOR (o la pestaña)</b> hasta que el pago se haya completado y se devuelva la conexión a este asistente.</p>');
         }

         $("#submit").click(function () {
             document.forms["payForm"].submit();
             var parameters = getParameters();

             if (parameters.lang == 'es') {
                 $("#submit").attr('value', 'Espere, conectando con Visa');
             }
             else {
                 $("#submit").attr('value', 'Wait, connecting to Visa');
             }
         });
     });
    </script>
</head>
<body>
<form method="post" runat="server"  id="payForm" action="">
<input runat="server" type="text" id="Ds_SignatureVersion" name="Ds_SignatureVersion" value="" hidden="hidden"/> <br />
<input runat="server" size="100" type="text" id="Ds_MerchantParameters" name="Ds_MerchantParameters" value="" hidden="hidden"/> <br />
<input runat="server" type="text"  id="Ds_Signature"  name="Ds_Signature" size="50" value="" hidden="hidden"/><br />
    </form>

    <div class="container">
        <div class="row" id="principal">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="panel panel-warning">
                    <div id="alert" class="panel panel-body text-center" >
                        <p style="font-size:40px">WARNING</p><p> Your connection will now be transferred to the PayPal website for payment processing. Please follow the instructions and <b>DO NOT CLOSE YOUR BROWSER (or browser tab)</b> until the payment is complete and you are redirected back to this wizard.</p>
                    </div>
                </div>

                <input type="button" id="submit" value="Accept & Continue" class="btn btn-primary btn-block" />
            </div>
        </div>
    </div>
</body>
</html>
