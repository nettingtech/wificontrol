﻿<%@ Page Title="" Language="C#" MasterPageFile="~/resources/master/PortalMaster.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Portal.piolets._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<!-- SCRIPTS Y CSS PARA LA FECHA -->
    <script src="../resources/scripts/moment-with-locales.js"></script>
    <script src="../resources/scripts/bootstrap-datetimepicker.js"></script>

    <link href="../resources/css/bootstrap-datetimepicker.css" rel="stylesheet" />

<!-- -->
       <script type="text/javascript">

           function getParameters() {
               var searchString = window.location.search.substring(1)
                   , params = searchString.split("&")
                   , hash = {}
               ;

               for (var i = 0; i < params.length; i++) {
                   var val = params[i].split("=");
                   hash[unescape(val[0])] = unescape(val[1]);
               }
               return hash;
           }

           function isValidEmailAddress(emailAddress) {
               var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
               return pattern.test(emailAddress);
           };

           function bindSelect(firstItem, id, valueMember, displayMember, source) {
               $("#" + id).find('option').remove().end();
               $("#" + id).append('<option value="0">' + firstItem + '</option>');
               $.each(source, function (index, item) {
                   $("#" + id).append('<option value="' + item[valueMember] + '"> ' + item[displayMember] + '</option>');
               });
           };

           function ilegalCharacter(string) {
               if (/^[a-zA-Z0-9-@_.]*$/.test(string) == false) {
                   return false;
               }
           };

           var createCookie = function (name, value, days) {
               var expires;
               if (days) {
                   var date = new Date();
                   date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                   expires = "; expires=" + date.toGMTString();
               }
               else {
                   expires = "";
               }
               document.cookie = name + "=" + value + expires + "; path=/";
           };

           $(document).ready(function (e) {

               var scrollcomplete = false;
               var parameters = getParameters();
               var val = "idhotel=" + $("#idhotel").val() + "&idlocation=" + $("#idlocation").val() + "&mac=" + $("#mac").val() + "&origin=" + $("#origin").val();
               createCookie("mikrotik", val, 1);


               var lang = navigator.browserLanguage ? navigator.browserLanguage : navigator.language;
               var langcode = lang.substring(0, 2);

               if ((langcode != 'es') && (langcode != 'en'))
                   langcode = $("#lang").val();



               $('#datetimepicker1').datetimepicker({
                   locale: 'es',
                   format: 'DD/MM/YY'
               });

               $('#checkin').datetimepicker({
                   locale: 'es',
                   format: 'DD/MM/YY'
               });

               if (langcode == 'es') {
                   //FORMULARIOS
                   $("#password").attr('placeholder', 'Contraseña');
                   $("#username").attr('placeholder', 'Nombre de usuario');
                   $("#usernamelabel").html("Nombre de usuario");
                   $("#passwordlabel").html("Contraseña");
                   $("#premiummail").attr("placeholder", "Correo electrónico");
                   $("#labelpremiumemail").html("Correo electrónico");
                   $("#checkindate").html("Fecha de entrada");
                   $("#room").attr('placeholder', 'Número de habitación');
                   $("#roomlabel").html("Número de habitación");

                   //BOTONES
                   $("#freebutton").html("¡Obtén un acceso gratuito!");
                   $("#loginbutton").html("Acceder");
                   $("#premiumbutton").html("Comprar");
                   $("#surveyfreebutton").html("Datos de Usuario");
                   $("#surveypremiumbutton").html("Datos de Usuario");
                   $("#acceptconditionsbutton").html("Aceptar");
                   $("#rejectconditions").html("Cancelar");
                   $("#retrybutton").html("Reintentar");
                   $("#disclaimerButton").html("Cerrar");
                   $("#ContinuePremium").html("Continuar");
                   $("#CancelPremium").html("Cancelar");
                   $("#customlogin").html("Acceder");

                   //TABS
                   $("#freelabel").html('Gratuito');
                   $("#customlabel").html('Clientes');

                   //ACEPTAR CONDICIONES
                   $("#disclaimeraccept").html("Acepto las condiciones de uso.");

                   //ETIQUETAS GENERAL
                   $("#premiumaccesstype").html("Seleccione su tipo de acceso.");

                   //MODALES
                   $("#disclaimerheader").html("Condiciones de Uso");
                   $("#disclaimermandatoryheader").html("Condiciones de Uso");
                   $("#disclaimerMandatoryButton").html("Condiciones de Uso");
                   $("#surveyheader").html("Datos Personales");
                   $("#disclaimerButton").html("Cerrar");
                   $("#surveyclose").html("Cerrar");
                   $("#errorbutton").html("Cerrar");
                   $("#surveysave").html("Continuar");
                   $("#surveyclosepaypal").html("Cerrar");
                   $("#surveysavepaypal").html("Continuar");

                   //TEXTOS GENERALES
                   $("#noaccepttext").html("Para poder utilizar el servicio, debe aceptar las Condiciones de Uso.");
                   $("#welcometext").html("Bienvenido");
                   $("#sorrytext").html("Lo sentimos");
                   $("#disclaimerwelcome").html("Para continuar, por favor acepte las ");
                   $("#waitheader").html("Espere un momento por favor");
                   $("#premiummailtext").html("Por favor, introduzca su dirección de correo para recibir sus datos de acceso.");
                   $("#premiummailHeader").html("Correo electrónico");
               }

               var dataString = 'action=fullsite&idhotel=' + $("#idhotel").val() + "&idlocation=" + $("#idlocation").val() + "&lang=" + langcode;
               $.ajax({
                   url: "../resources/handlers/PioletsHandler.ashx",
                   data: dataString,
                   contentType: "application/json; charset=utf-8",
                   dataType: "text",
                   success: function (pReturn) {
                       var lReturn = JSON.parse(pReturn);
                       $("#logofinish").attr("src", "../resources/images/" + $("#idhotel").val() + "/" + $("#idlocation").val() + "-logo.png");
                       $("#freeimage").attr("src", "../resources/images/" + $("#idhotel").val() + "/" + $("#idlocation").val() + "-logo.png");
                       $("#premiumimage").attr("src", "../resources/images/" + $("#idhotel").val() + "/" + $("#idlocation").val() + "-logo.png");
                       $("#socialimage").attr("src", "../resources/images/" + $("#idhotel").val() + "/" + $("#idlocation").val() + "-logo.png");
                       $("#disclaimerimage").attr("src", "../resources/images/" + $("#idhotel").val() + "/" + $("#idlocation").val() + "-logo.png");
                       $("#noacceptimage").attr("src", "../resources/images/" + $("#idhotel").val() + "/" + $("#idlocation").val() + "-logo.png");
                       $("#loginimage").attr("src", "../resources/images/" + $("#idhotel").val() + "/" + $("#idlocation").val() + "-logo.png");
                       $("#imagehome").attr("src", "../resources/images/" + $("#idhotel").val() + "/" + $("#idlocation").val() + "-home.jpg");
                       $("#customimage").attr("src", "../resources/images/" + $("#idhotel").val() + "/" + $("#idlocation").val() + "-logo.png");
                       $("#freetext").html(lReturn.FreeText);
                       $("#premiumtext").html(lReturn.PremiumText);
                       $("#socialText").html(lReturn.SocialText);
                       $("#disclaimertext").html(lReturn.Disclaimer);
                       $("#customtext").html(lReturn.CustomText);
                       $("#disclaimermandatorytext").html(lReturn.Disclaimer);

                       $("#mandatorysurvey").val(lReturn.MandatorySurvey);

                       if (lReturn.FreeAccessModule) {
                           $("#freeaccess").val(lReturn.freeAccess.IdBillingType);
                           $("#tab-free").show();
                           if (langcode == 'es') {
                               $("#freeaccessinformation").html('<b>Tiempo de Conexión:</b> ' + lReturn.freeAccess.TimeCredit + ' <br/><b>Validez del Ticket:</b> ' + lReturn.freeAccess.ValidTill + ' horas<br/><b>Número máximo de dispositivos:</b> ' + lReturn.freeAccess.MaxDevices + '<br/><b>Velocidad de Subida:</b> ' + lReturn.freeAccess.BWUp + ' kbps<br/><b>Velocidad de Bajada:</b> ' + lReturn.freeAccess.BWDown + ' kbps<br/>');
                           }
                           else {
                               $("#freeaccessinformation").html('<b>Time Credit:</b> ' + lReturn.freeAccess.TimeCredit + ' <br/><b>Valid Till:</b> ' + lReturn.freeAccess.ValidTill + ' hours<br/><b>Max devices:</b> ' + lReturn.freeAccess.MaxDevices + '<br/><b>Bandwidth Up:</b> ' + lReturn.freeAccess.BWUp + ' kbps<br/><b>Bandwidth Down:</b> ' + lReturn.freeAccess.BWDown + ' kbps<br/>');
                           }
                       }

                       if (lReturn.LoginModule) {
                           $("#tab-login").show();
                           $("#logintext").html(lReturn.LoginText);
                       }

                       if (lReturn.PayAccessModule) {
                           $("#tab-payaccess").show();

                           $("#accesstypes").html("");
                           $('#accesstype').find('option').remove();

                           if (langcode == 'es') {
                               $("#accesstype").append('<option value="0" >Seleccione un acceso</option>');
                               $.each(lReturn.listPremium, function (index, item) {
                                   $("#accesstype").append('<option value="' + item["IdBillingType"] + '" >' + item["Description"] + ' - ' + item["Currency"] + '</option>');
                                   $("#accesstypes").append('<div id="accesstype_details_' + item["IdBillingType"] + '" style="display:none;"><b>Tiempo de Conexión:</b> ' + item["TimeCredit"] + ' <br/><b>Validez del Ticket:</b> ' + item["ValidTill"] + ' horas<br/><b>Número máximo de dispositivos:</b> ' + item["MaxDevices"] + '<br/><b>Velocidad de Subida:</b> ' + item["BWUp"] + ' kbps<br/><b>Velocidad de Bajada:</b> ' + item["BWDown"] + ' kbps<br/></div>');

                               });
                           }
                           else {
                               $("#accesstype").append('<option value="0" >Select one</option>');
                               $.each(lReturn.listPremium, function (index, item) {
                                   $("#accesstype").append('<option value="' + item["IdBillingType"] + '" >' + item["Description"] + ' - ' + item["Currency"] + '</option>');
                                   $("#accesstypes").append('<div id="accesstype_details_' + item["IdBillingType"] + '" style="display:none;"><b>Time Credit:</b> ' + item["TimeCredit"] + ' <br/><b>Valid Till:</b> ' + item["ValidTill"] + ' hours<br/><b>Max devices:</b> ' + item["MaxDevices"] + '<br/><b>Bandwidth Up:</b> ' + item["BWUp"] + ' kbps<br/><b>Bandwidth Down:</b> ' + item["BWDown"] + ' kbps<br/></div>');
                               });
                           }

                       }
                       if (lReturn.SocialNetworksModule) {
                           $("#socialaccess").val(lReturn.socialAccess.IdBillingType);
                           $("#tab-social").show();
                           if (langcode == 'es') {
                               $("#socialaccessinformation").html('<b>Tiempo de Conexión:</b> ' + lReturn.socialAccess.TimeCredit + ' <br/><b>Validez del Ticket:</b> ' + lReturn.socialAccess.ValidTill + ' horas<br/><b>Número máximo de dispositivos:</b> ' + lReturn.socialAccess.MaxDevices + '<br/><b>Velocidad de Subida:</b> ' + lReturn.socialAccess.BWUp + ' kbps<br/><b>Velocidad de Bajada:</b> ' + lReturn.socialAccess.BWDown + ' kbps<br/>');
                           }
                           else {
                               $("#socialaccessinformation").html('<b>Time Credit:</b> ' + lReturn.socialAccess.TimeCredit + ' <br/><b>Valid Till:</b> ' + lReturn.socialAccess.ValidTill + ' hours<br/><b>Max devices:</b> ' + lReturn.socialAccess.MaxDevices + '<br/><b>Bandwidth Up:</b> ' + lReturn.socialAccess.BWUp + ' kbps<br/><b>Bandwidth Down:</b> ' + lReturn.socialAccess.BWDown + ' kbps<br/>');
                           }

                           $("#idsocialbillintype").val(lReturn.socialAccess.IdBillingType);
                       }

                       if (lReturn.CustomAccessModule) {
                           $("#tab-custom").show();
                           $("#customtext").html(lReturn.CustomText);
                       }

                       switch (lReturn.DefaultModule) {
                           case 1:
                               $("#tab-free").addClass("active"); $("#free").addClass("active in"); break;
                           case 2:
                               $("#tab-payaccess").addClass("active"); $("#pay").addClass("active in"); break;
                           case 3:
                               $("#tab-social").addClass("active"); $("#facebook").addClass("active in"); break;
                           case 4:
                               $("#tab-custom").addClass("active"); $("#custom").addClass("active in"); break;
                           case 5:
                               $("#tab-login").addClass("active"); $("#login").addClass("active in"); break;
                       }


                       var text = '';
                       text += '<div role="form">';
                       text += '<div class="form-group"><label id="labelemail">Email</label><input name="email" id="email" type="text" class="form-control" placeholder="Email" /></div>';
                       $.each(lReturn.survey, function (index, item) {
                           text += '<div class="form-group"><label id="labelemail">' + item["Question"] + '</label>';
                           if (item["Type"] == 'text')
                               text += '<input type="text" id="' + item["Order"] + '" rel="survey" class="form-control" placeholder="' + item["Question"] + '" question="' + item["Question"] + '" /></div>';
                           else if (item["Type"] == 'select') {
                               var parts = item["Values"].split('/');
                               text += "<td><select id='" + item["Order"] + "' rel='survey' class='form-control' question='" + item["Question"] + "' >";
                               $.each(parts, function (key, value) {
                                   text += "<option value='" + value + "'>" + value + "</option>";
                               });

                               text += '</select></div>';
                           }
                       });
                       text += '</div>';

                       $("#survey").html(text);

                       if ($("#error").val() != "") {
                           if (langcode == 'es') {
                               switch ($("#error").val().toUpperCase()) {
                                   case 'UNKNOWN USER': $("#message").html('Usuario Desconocido'); break;
                                   case 'NO TIME CREDIT': $("#message").html('Tiempo de conexión agotado'); break;
                                   case 'TICKET EXPIRED': $("#message").html('Ticket Caducado'); break;
                                   case 'TOO MANY DEVICES': $("#message").html('Demasiados dispositivos'); break;
                                   case 'SORRY, NO MORE USERS ALLOWED': $("#message").html('Lo sentimos, no se permiten más usuarios'); break;
                                   case 'WRONG PASSWORD': $("#message").html('Contraseña incorrecta'); break;
                                   case 'UPLOAD QUOTA EXCEEDED': $("#message").html('Cuota de datos de subida agotada'); break;
                                   case 'DOWNLOAD QUOTA EXCEEDED': $("#message").html('Cuota de datos de bajada agotada'); break;

                                   default: $("#message").html($("#error").val());
                               }
                           }
                           else
                               $("#message").html($("#error").val());

                           $("#myModalError").modal('show');
                           $("#myModal").modal('hide');
                           $("#noaccept").fadeOut("fast");
                           $("#principal").fadeIn("slow");
                           $("#wait").fadeOut("slow");
                       }
                       else {
                           if (lReturn.DisclaimerMandatory) {
                               $("#wait").fadeOut("slow");
                               $("#noaccept").fadeIn("slow");
                               $("#myModal").modal('show');
                           }
                           else {
                               $("#disclaimermandatory").show();
                               $("#myModal").modal('hide');
                               $("#wait").fadeOut("slow");
                               $("#noaccept").fadeOut("fast");
                               $("#principal").fadeIn("slow");
                           }
                       }

                   },
                   error: function (xhr, ajaxOptions, thrownError) {
                       $("#loading").html("Su ha producido un error durante el envio. Por favor intentelo más tarde.");
                       alert('error: ' + xhr.statusText);
                   }

               });

               $('#myTab a').click(function (e) {
                   e.preventDefault()
                   $(this).tab('show')
               })

               $('#freeread').click(function (e) {
                   e.preventDefault();
                   $("#myModal").modal('show');

               });

               $('#premiumread').click(function (e) {
                   e.preventDefault();
                   $("#myModal").modal('show');

               });

               $('#socialread').click(function (e) {
                   e.preventDefault();
                   $("#myModal").modal('show');

               });

               $("#instagrambutton").click(function (e) {
                   var dataString = 'action=allow&idhotel=' + $("#idhotel").val() + '&idlocation=' + $("#idlocation").val() + '&mac=' + $("#mac").val() + '&origin=' + $("#origin").val();
                   $.ajax({
                       url: "../resources/handlers/PioletsHandler.ashx",
                       data: dataString,
                       contentType: "application/json; charset=utf-8",
                       dataType: "text",
                       beforeSend: function () {
                           $("#MyModalWait").modal('show');
                       },
                       complete: function () {
                           $("#MyModalWait").modal('hide');
                       },
                       success: function (pResult) {
                           var lResult = JSON.parse(pResult);

                           if (lResult.allow == 1) {
                               $("#finishbutton").attr("href", lResult.Url);
                               $("#finishInformation").append($("#freeaccessinformation").html());

                               if (langcode == 'es') {
                                   $("#finishlabel").html("Enhorabuena");
                                   $("#finishInformation").append('<b>Nombre de usuario: </b>' + lResult.UserName + '<br />');
                                   $("#finishInformation").append('<b>Contraseña: </b>' + lResult.Password + '<br />');
                                   $("#finishInformation").append('<b>Fecha de expiración: </b>' + lResult.ValidTill + '<br />');
                                   $("#finishbutton").val("Continuar navegando");
                               }
                               else {
                                   $("#finishInformation").append('<b>UserName: </b>' + lResult.UserName + '<br />');
                                   $("#finishInformation").append('<b>Password: </b>' + lResult.Password + '<br />');
                                   $("#finishInformation").append('<b>Valid Till: </b>' + lResult.ValidTill + '<br />');
                                   $("#finishbutton").val("Continue browsing");
                               }

                               $('#myModalFinish').modal('show');
                           }
                           else if (lResult.allow == 2) {
                               if (langcode != 'es') {
                                   $("#message").html("No more free access allowed now.");
                               }
                               else {
                                   $("#message").html("No se permiten más accesos gratuitos en este periodo.");
                               }
                               $("#myModalError").modal('show');
                           }
                           else if (lResult.allow == 0) {
                               var va2 = "idhotel=" + $("#idhotel").val() + "&idlocation=" + $("#idlocation").val() + "&mac=" + $("#mac").val() + "&origin=" + $("#origin").val() + "&idbillingtype=" + $("#socialaccess").val();
                               createCookie("mikrotik", va2, 1);

                               var uri = window.location.origin + "/piolets/instagram.aspx";
                               var url = "https://www.instagram.com/oauth/authorize/?client_id=a3d2212db0a1455388b370858fe80e64&redirect_uri=" + uri + "&response_type=token";
                               window.location.href = url;
                           }
                       },
                       error: function (xhr, ajaxOptions, thrownError) {
                           $("#loading").html("Su ha producido un error durante el envio. Por favor intentelo más tarde.");
                           alert('error: ' + xhr.statusText);
                           $("#nextstep3").show();
                           $("#backstep3").show();

                       }
                   });
               });

               $('#surveyfreebutton').click(function (e) {
                   e.preventDefault();
                   $("#myModal2").modal('show');
               });

               $('#surveysave').click(function (e) {
                   e.preventDefault();

                   var survey = 'loginform:';
                   var email = $("#email").val();
                   var parameters = getParameters();

                   if (isValidEmailAddress(email) != true) {
                       if (langcode == 'es') {
                           $("#message2").html("El formato del correo electrónico no es correcto").removeClass().addClass('alert alert-danger').show();
                       }
                       else {
                           $("#message2").html("Email format is not correct").removeClass().addClass('alert alert-danger').show();
                       }
                   }
                   else {
                       var parameters = getParameters();
                       var valid = true;
                       $('input[rel^="survey"]').each(function (input) {
                           survey += $(this).attr('question') + '=' + $(this).val() + '|';
                           if ($(this).val() == '')
                               valid = false;
                       });

                       $('select[rel^="survey"]').each(function (input) {
                           survey += $(this).attr('question') + '=' + $(this).val() + '|';
                           if ($(this).val() == '')
                               valid = false;
                       });

                       if (valid == false) {
                           if (langcode == 'es') {
                               $("#message2").html("Todos los campos son obligatorios.").removeClass().addClass('alert alert-danger').show();
                           }
                           else {
                               $("#message2").html("All fields are mandatory.").removeClass().addClass('alert alert-danger').show();
                           }
                       }
                       else {

                           $("#myModalSurvey").modal('hide');

                           var dataString = 'action=freeaccess&idhotel=' + $("#idhotel").val() + '&survey=' + survey + '&email=' + email + '&idlocation=' + $("#idlocation").val() + '&mac=' + $("#mac").val() + '&origin=' + $("#origin").val() + "&idbilling=" + $("#freeaccess").val();
                           $.ajax({
                               url: "../resources/handlers/PioletsHandler.ashx",
                               data: dataString,
                               contentType: "application/json; charset=utf-8",
                               dataType: "text",
                               beforeSend: function () {
                                   $("#MyModalWait").modal('show');
                               },
                               complete: function () {
                                   $("#MyModalWait").modal('hide');
                               },
                               success: function (pResult) {
                                   var lResult = JSON.parse(pResult);

                                   if (lResult.IdUser == -1) {
                                       if (langcode != 'es') {
                                           $("#message").html("No more free access are allowed now.");
                                       }
                                       else {
                                           $("#message").html("No se permiten más accesos gratuitos en este periodo.");
                                       }
                                       $("#myModalError").modal('show');
                                   }
                                   else if (lResult.IdUser == -3) {
                                       if (langcode != 'es') {
                                           $("#message").html("We are currently experiencing a problem, please try again later.");
                                       }
                                       else {
                                           $("#message").html("Tenemos un problema temporal, intentelo de nuevo más tarde.");
                                       }
                                       $("#myModalError").modal('show');
                                   }
                                   else if (lResult.IdUser == 0) {
                                       if (langcode != 'es') {
                                           $("#message").html("We are currently experiencing a problem, please try again later.");
                                       }
                                       else {
                                           $("#message").html("Tenemos un problema temporal, intentelo de nuevo más tarde.");
                                       }
                                       $("#myModalError").modal('show');
                                   }
                                   else {
                                       $("#finishbutton").attr("href", lResult.Url);
                                       $("#finishInformation").append($("#freeaccessinformation").html());

                                       if (langcode == 'es') {
                                           $("#finishlabel").html("Enhorabuena");
                                           $("#finishInformation").append('<b>Nombre de usuario: </b>' + lResult.UserName + '<br />');
                                           $("#finishInformation").append('<b>Contraseña: </b>' + lResult.Password + '<br />');
                                           $("#finishInformation").append('<b>Fecha de expiración: </b>' + lResult.ValidTill + '<br />');
                                           $("#finishbutton").val("Continuar navegando");
                                       }
                                       else {
                                           $("#finishInformation").append('<b>UserName: </b>' + lResult.UserName + '<br />');
                                           $("#finishInformation").append('<b>Password: </b>' + lResult.Password + '<br />');
                                           $("#finishInformation").append('<b>Valid Till: </b>' + lResult.ValidTill + '<br />');
                                           $("#finishbutton").val("Continue browsing");
                                       }

                                       $('#myModalFinish').modal('show');
                                   }

                               },
                               error: function (xhr, ajaxOptions, thrownError) {
                                   $("#loading").html("Su ha producido un error durante el envio. Por favor intentelo más tarde.");
                                   alert('error: ' + xhr.statusText);
                                   $("#nextstep3").show();
                                   $("#backstep3").show();

                               }
                           });
                       }
                   }
               });

               $('#freebutton').click(function (e) {

                   var dataString = 'action=allow&idhotel=' + $("#idhotel").val() + '&idlocation=' + $("#idlocation").val() + '&mac=' + $("#mac").val() + '&origin=' + $("#origin").val();
                   $.ajax({
                       url: "../resources/handlers/PioletsHandler.ashx",
                       data: dataString,
                       contentType: "application/json; charset=utf-8",
                       dataType: "text",
                       beforeSend: function () {
                           $("#MyModalWait").modal('show');
                       },
                       complete: function () {
                           $("#MyModalWait").modal('hide');
                       },
                       success: function (pResult) {
                           var lResult = JSON.parse(pResult);

                           if (lResult.allow == 1) {
                               $("#finishbutton").attr("href", lResult.Url);
                               $("#finishInformation").append($("#freeaccessinformation").html());

                               if (langcode == 'es') {
                                   $("#finishlabel").html("Enhorabuena");
                                   $("#finishInformation").append('<b>Nombre de usuario: </b>' + lResult.UserName + '<br />');
                                   $("#finishInformation").append('<b>Contraseña: </b>' + lResult.Password + '<br />');
                                   $("#finishInformation").append('<b>Fecha de expiración: </b>' + lResult.ValidTill + '<br />');
                                   $("#finishbutton").val("Continuar navegando");
                               }
                               else {
                                   $("#finishInformation").append('<b>UserName: </b>' + lResult.UserName + '<br />');
                                   $("#finishInformation").append('<b>Password: </b>' + lResult.Password + '<br />');
                                   $("#finishInformation").append('<b>Valid Till: </b>' + lResult.ValidTill + '<br />');
                                   $("#finishbutton").val("Continue browsing");
                               }

                               $('#myModalFinish').modal('show');
                           }
                           else if (lResult.allow == 2) {
                               if (langcode != 'es') {
                                   $("#message").html("No more free access allowed now.");
                               }
                               else {
                                   $("#message").html("No se permiten más accesos gratuitos en este periodo.");
                               }
                               $("#myModalError").modal('show');
                           }
                           else if (lResult.allow == 0) {
                               if ($("#mandatorysurvey").val() != "true") {
                                   e.preventDefault();

                                   var dataString = 'action=freeaccessnosurvey&idhotel=' + $("#idhotel").val() + '&idlocation=' + $("#idlocation").val() + '&mac=' + $("#mac").val() + '&origin=' + $("#origin").val() + "&idbilling=" + $("#freeaccess").val();
                                   $.ajax({
                                       url: "../resources/handlers/PioletsHandler.ashx",
                                       data: dataString,
                                       contentType: "application/json; charset=utf-8",
                                       dataType: "text",
                                       beforeSend: function () {
                                           $("#MyModalWait").modal('show');
                                       },
                                       complete: function () {
                                           $("#MyModalWait").modal('hide');
                                       },
                                       success: function (pResult) {
                                           var lResult = JSON.parse(pResult);

                                           if (lResult.IdUser == -1) {
                                               if (langcode != 'es') {
                                                   $("#message").html("No more free access are allowed now.");
                                               }
                                               else {
                                                   $("#message").html("No se permiten más accesos gratuitos en este periodo.");
                                               }
                                               $("#myModalError").modal('show');
                                           }
                                           else if (lResult.IdUser == -3) {
                                               if (langcode != 'es') {
                                                   $("#message").html("We are currently experiencing a problem, please try again later.");
                                               }
                                               else {
                                                   $("#message").html("Tenemos un problema temporal, intentelo de nuevo más tarde.");
                                               }
                                               $("#myModalError").modal('show');
                                           }
                                           else if (lResult.IdUser == 0) {
                                               if (langcode != 'es') {
                                                   $("#message").html("We are currently experiencing a problem, please try again later.");
                                               }
                                               else {
                                                   $("#message").html("Tenemos un problema temporal, intentelo de nuevo más tarde.");
                                               }
                                               $("#myModalError").modal('show');
                                           }
                                           else {
                                               $("#finishbutton").attr("href", lResult.Url);
                                               $("#finishInformation").append($("#freeaccessinformation").html());

                                               if (langcode == 'es') {
                                                   $("#finishlabel").html("Enhorabuena");
                                                   $("#finishInformation").append('<b>Nombre de usuario: </b>' + lResult.UserName + '<br />');
                                                   $("#finishInformation").append('<b>Contraseña: </b>' + lResult.Password + '<br />');
                                                   $("#finishInformation").append('<b>Fecha de expiración: </b>' + lResult.ValidTill + '<br />');
                                                   $("#finishbutton").val("Continuar navegando");
                                               }
                                               else {
                                                   $("#finishInformation").append('<b>UserName: </b>' + lResult.UserName + '<br />');
                                                   $("#finishInformation").append('<b>Password: </b>' + lResult.Password + '<br />');
                                                   $("#finishInformation").append('<b>Valid Till: </b>' + lResult.ValidTill + '<br />');
                                                   $("#finishbutton").val("Continue browsing");
                                               }

                                               $('#myModalFinish').modal('show');
                                           }

                                       },
                                       error: function (xhr, ajaxOptions, thrownError) {
                                           $("#loading").html("Su ha producido un error durante el envio. Por favor intentelo más tarde.");
                                           alert('error: ' + xhr.statusText);
                                           $("#nextstep3").show();
                                           $("#backstep3").show();

                                       }
                                   });
                               }
                               else {
                                   $("#myModalSurvey").modal('show');
                               }

                           }
                       },
                       error: function (xhr, ajaxOptions, thrownError) {
                           $("#loading").html("Su ha producido un error durante el envio. Por favor intentelo más tarde.");
                           alert('error: ' + xhr.statusText);
                           $("#nextstep3").show();
                           $("#backstep3").show();

                       }
                   });
               });

               $('#loginbutton').click(function (e) {
                   e.preventDefault();
                   var user = $("#username").val().toUpperCase();
                   var password = $("#password").val().toUpperCase();

                   if ((ilegalCharacter(user) != false) && (ilegalCharacter(password) != false)) {
                       if (user == "" || password == "") {
                           if (langcode == 'es') {
                               $("#message").html("Es necesario introducir usuario y contraseña.");
                           }
                           else {
                               $("#message").html("UserName and Password are mandatory.");
                           }
                           $("#myModalError").modal('show');
                       }
                       else {
                           var origin = $("#origin").val();
                           var urlDestino = $("#urlHotel").val();

                           var dataString = 'action=login&username=' + user + '&password=' + password + '&idhotel=' + $("#idhotel").val() + '&idlocation=' + $("#idlocation").val() + '&mac=' + $("#mac").val();
                           $.ajax({
                               url: "../resources/handlers/PioletsHandler.ashx",
                               data: dataString,
                               contentType: "application/json; charset=utf-8",
                               dataType: "json",
                               beforeSend: function () {
                                   $("#MyModalWait").modal('show');
                               },
                               complete: function () {
                                   $("#MyModalWait").modal('hide');
                               },
                               success: function (pReturn) {
                                   if (pReturn.code == 'OK') {
                                       var url = origin + "?username=" + user + "&password=" + password + "&dst=" + pReturn.message;
                                       $("#modal-loading").show();
                                       window.location.href = url;
                                   }
                                   else {
                                       if (langcode == 'es') {
                                           $("#message").html(pReturn.message);
                                       }
                                       else {
                                           $("#message").html(pReturn.message);
                                       }
                                       $("#myModalError").modal('show');
                                   }
                               },
                               error: function (xhr, ajaxOptions, thrownError) {
                                   $("#loading").html("We have a problem, please try again later.");
                                   alert('error: ' + xhr.statusText);
                               }
                           });


                       }
                   }
                   else {
                       if (langcode == 'es') {
                           $("#message").html("El nombre de usuario y contraseña solo puede tener caracteres del 0 - 9, A - Z.");
                       }
                       else {
                           $("#message").html("Username and password can only contain characters 0-9 ans A-Z.");
                       }
                       $("#myModalError").modal('show');
                   }
               });

               $('#premiumbutton').click(function (e) {
                   e.preventDefault();
                   var dataString = 'action=allowpay&idhotel=' + $("#idhotel").val() + '&idlocation=' + $("#idlocation").val() + '&mac=' + $("#mac").val() + '&origin=' + $("#origin").val();
                   $.ajax({
                       url: "../resources/handlers/PioletsHandler.ashx",
                       data: dataString,
                       contentType: "application/json; charset=utf-8",
                       dataType: "text",
                       beforeSend: function () {
                           $("#MyModalWait").modal('show');
                       },
                       complete: function () {
                           $("#MyModalWait").modal('hide');
                       },
                       success: function (pResult) {
                           var lResult = JSON.parse(pResult);

                           if (lResult.allow == 1) {
                               $("#finishbutton").attr("href", lResult.Url);
                               $("#finishInformation").append($("#freeaccessinformation").html());

                               if (langcode == 'es') {
                                   $("#finishlabel").html("Enhorabuena");
                                   $("#finishInformation").append('<b>Nombre de usuario: </b>' + lResult.UserName + '<br />');
                                   $("#finishInformation").append('<b>Contraseña: </b>' + lResult.Password + '<br />');
                                   $("#finishInformation").append('<b>Fecha de expiración: </b>' + lResult.ValidTill + '<br />');
                                   $("#finishbutton").val("Continuar navegando");
                               }
                               else {
                                   $("#finishInformation").append('<b>UserName: </b>' + lResult.UserName + '<br />');
                                   $("#finishInformation").append('<b>Password: </b>' + lResult.Password + '<br />');
                                   $("#finishInformation").append('<b>Valid Till: </b>' + lResult.ValidTill + '<br />');
                                   $("#finishbutton").val("Continue browsing");
                               }

                               $('#myModalFinish').modal('show');
                           }
                           else if (lResult.allow == 2) {
                               if (langcode != 'es') {
                                   $("#message").html("No more free access allowed now.");
                               }
                               else {
                                   $("#message").html("No se permiten más accesos gratuitos en este periodo.");
                               }
                               $("#myModalError").modal('show');
                           }
                           else if (lResult.allow == 0) {
                               var billingType = $("#accesstype").val();
                               if (billingType == "0") {
                                   if (langcode == 'es') {
                                       $("#message").html("Debe seleccionar un tipo de acceso premium.");
                                   }
                                   else {
                                       $("#message").html("You must select an access premium type to continue.");
                                   }
                                   $("#myModalError").modal('show');

                               }
                               else {

                                   $("#myModalPremium").modal('show');
                               }
                           }
                       },
                       error: function (xhr, ajaxOptions, thrownError) {
                           $("#loading").html("Su ha producido un error durante el envio. Por favor intentelo más tarde.");
                           alert('error: ' + xhr.statusText);
                           $("#nextstep3").show();
                           $("#backstep3").show();

                       }
                   });
               });

               $("#ContinuePremium").click(function (e) {

                   var billingType = $("#accesstype").val();
                   var mail = $("#premiummail").val();
                   if (isValidEmailAddress(mail) == false || mail == '') {
                       if (langcode == 'es') {
                           $("#message").html("El correo electrónico no tiene el formato correcto.");
                       }
                       else {
                           $("#message").html("Email format is not correct.");
                       }
                       $("#myModalError").modal('show');
                   }
                   else {

                       var parameters = getParameters();
                       var dataString = 'action=paypalrequestnosurvey&idhotel=' + $("#idhotel").val() + '&IdBillingType=' + billingType + '&mac=' + $("#mac").val() + '&idlocation=' + $("#idlocation").val() + '&lang=' + langcode + '&origin=' + $("#origin").val() + '&email=' + mail;
                       $.ajax({
                           url: "../resources/handlers/PioletsHandler.ashx",
                           data: dataString,
                           contentType: "application/json; charset=utf-8",
                           dataType: "text",
                           beforeSend: function () {
                               $("#MyModalWait").modal('show');
                           },
                           complete: function () {
                               $("#MyModalWait").modal('hide');
                           },
                           success: function (pResult) {
                               var lResult = JSON.parse(pResult);

                               var url = 'redsys.aspx?IdTransaction=' + lResult.IdTransaction + '&item_name=' + lResult.Description + '&amount=' + lResult.Price + '&idhotel=' + $("#idhotel").val() + '&idlocation=' + $("#idlocation").val() + '&mac=' + $("#mac").val() + '&origin=' + $("#origin").val() + '&lang=' + langcode
                               window.location.href = url;

                           },
                           error: function (xhr, ajaxOptions, thrownError) {
                               $("#loading").html("We are currently experiencing a problem, please try again later.");
                               alert('error: ' + xhr.statusText);
                           }
                       });
                   }
               });

               $('#accesstype').change(function (e) {
                   e.preventDefault();
                   if ($("#accesstype").val() != "0") {
                       var html = $("#accesstype_details_" + $("#accesstype").val()).html();
                       $("#connectioninfo").html("<small>" + html + "</small>");
                   }
                   else {
                       $("#connectioninfo").html("");
                   }

               });

               $("#finishbutton").click(function (e) {
                   e.preventDefault();
                   var url = $("#finishbutton").attr("href");
                   window.location.href = url;

               });

               $("#socialbutton").click(function (e) {
                   var dataString = 'action=allow&idhotel=' + $("#idhotel").val() + '&idlocation=' + $("#idlocation").val() + '&mac=' + $("#mac").val() + '&origin=' + $("#origin").val();
                   $.ajax({
                       url: "../resources/handlers/PioletsHandler.ashx",
                       data: dataString,
                       contentType: "application/json; charset=utf-8",
                       dataType: "text",
                       beforeSend: function () {
                           $("#MyModalWait").modal('show');
                       },
                       complete: function () {
                           $("#MyModalWait").modal('hide');
                       },
                       success: function (pResult) {
                           var lResult = JSON.parse(pResult);

                           if (lResult.allow == 1) {
                               $("#finishbutton").attr("href", lResult.Url);
                               $("#finishInformation").append($("#freeaccessinformation").html());

                               if (langcode == 'es') {
                                   $("#finishlabel").html("Enhorabuena");
                                   $("#finishInformation").append('<b>Nombre de usuario: </b>' + lResult.UserName + '<br />');
                                   $("#finishInformation").append('<b>Contraseña: </b>' + lResult.Password + '<br />');
                                   $("#finishInformation").append('<b>Fecha de expiración: </b>' + lResult.ValidTill + '<br />');
                                   $("#finishbutton").val("Continuar navegando");
                               }
                               else {
                                   $("#finishInformation").append('<b>UserName: </b>' + lResult.UserName + '<br />');
                                   $("#finishInformation").append('<b>Password: </b>' + lResult.Password + '<br />');
                                   $("#finishInformation").append('<b>Valid Till: </b>' + lResult.ValidTill + '<br />');
                                   $("#finishbutton").val("Continue browsing");
                               }

                               $('#myModalFinish').modal('show');
                           }
                           else if (lResult.allow == 2) {
                               if (langcode != 'es') {
                                   $("#message").html("No more free access allowed now.");
                               }
                               else {
                                   $("#message").html("No se permiten más accesos gratuitos en este periodo.");
                               }
                               $("#myModalError").modal('show');
                           }
                           else if (lResult.allow == 0) {
                               e.preventDefault();
                               var parameters = getParameters();

                               window.fbAsyncInit = function () {
                                   FB.init({
                                       appId: '233231503535149',
                                       status: true,
                                       cookie: true,
                                       xfbml: true,
                                       version: 'v2.11' // 
                                   });

                                   var val = "idhotel=" + $("#idhotel").val() + ",idlocation=" + $("#idlocation").val() + ",mac=" + $("#mac").val() + ",origin=" + $("#origin").val() + ",idbillingtype=" + $("#socialaccess").val();
                                   var uri = window.location.origin + "/piolets/facebook.aspx";
                                   var va2 = "idhotel=" + $("#idhotel").val() + "&idlocation=" + $("#idlocation").val() + "&mac=" + $("#mac").val() + "&origin=" + $("#origin").val() + "&idbillingtype=" + $("#socialaccess").val();
                                   createCookie("mikrotik", va2, 1);

                                   FB.getLoginStatus(function (response) {
                                       if (response.status === 'connected') {
                                           window.location.href = uri;
                                       } else {
                                           var facebookUri = "https://www.facebook.com/v2.11/dialog/oauth?client_id=233231503535149&redirect_uri=" + uri + "&response_type=code&scope=email,public_profile,user_birthday,user_likes&state=\"{" + val + "\"}";
                                           window.location = encodeURI(facebookUri);
                                       }
                                   }, { scope: 'email,public_profile,user_birthday,user_likes' });
                               };
                               (function (d, s, id) {
                                   var js, fjs = d.getElementsByTagName(s)[0];
                                   if (d.getElementById(id)) { return; }
                                   js = d.createElement(s); js.id = id;
                                   js.src = "../resources/scripts/fb_sdk.js";
                                   fjs.parentNode.insertBefore(js, fjs);
                               }(document, 'script', 'facebook-jssdk'));
                           }
                       },
                       error: function (xhr, ajaxOptions, thrownError) {
                           $("#loading").html("Su ha producido un error durante el envio. Por favor intentelo más tarde.");
                           alert('error: ' + xhr.statusText);
                           $("#nextstep3").show();
                           $("#backstep3").show();

                       }
                   });
               });

               $("#rejectconditions").click(function (e) {
                   $("#principal").fadeOut("slow");
                   $("#noaccept").fadeIn("slow");
                   $("#myModal").modal('hide');
               });

               $("#retrybutton").click(function (e) {
                   $("#myModal").modal('show');
               });

               $("#disclaimerMandatoryButton").click(function (e) {
                   $("#myModalMandatoryDisclaimer").modal('show');
               });

               $('#customlogin').click(function (e) {
                   e.preventDefault();
                   var room = $("#room").val();
                   var checkin = $("#checkin").val();

                   if (room == "" || checkin == "") {
                       if (langcode == 'es') {
                           $("#message").html("Es necesario introducir número de habitación, fecha de entrada.");
                       }
                       else {
                           $("#message").html("Room Number, Checkin date are mandatory.");
                       }
                       $("#myModalError").modal('show');
                   }
                   else {
                       var urlDestino = $("#urlHotel").val();

                       var dataString = 'action=loginpiolets&room=' + room + '&checkin=' + checkin + '&idhotel=' + $("#idhotel").val() + '&idlocation=' + $("#idlocation").val() + '&mac=' + $("#mac").val() + '&origin=' + $("#origin").val();
                       $.ajax({
                           url: "../resources/handlers/PioletsHandler.ashx",
                           data: dataString,
                           contentType: "application/json; charset=utf-8",
                           dataType: "text",
                           beforeSend: function () {
                               $("#MyModalWait").modal('show');
                           },
                           complete: function () {
                               $("#MyModalWait").modal('hide');
                           },
                           success: function (pReturn) {
                               var lResult = JSON.parse(pReturn);

                               if (lResult.IdUser == 1) {
                                   $("#finishbutton").attr("href", lResult.Url);
                                   $("#finishInformation").append($("#freeaccessinformation").html());

                                   if (langcode == 'es') {
                                       $("#finishlabel").html("Enhorabuena");
                                       $("#finishInformation").append('<b>Nombre de usuario: </b>' + lResult.UserName + '<br />');
                                       $("#finishInformation").append('<b>Contraseña: </b>' + lResult.Password + '<br />');
                                       $("#finishInformation").append('<b>Fecha de expiración: </b>' + lResult.ValidTill + '<br />');
                                       $("#finishbutton").val("Continuar navegando");
                                   }
                                   else {
                                       $("#finishInformation").append('<b>UserName: </b>' + lResult.UserName + '<br />');
                                       $("#finishInformation").append('<b>Password: </b>' + lResult.Password + '<br />');
                                       $("#finishInformation").append('<b>Valid Till: </b>' + lResult.ValidTill + '<br />');
                                       $("#finishbutton").val("Continue browsing");
                                   }

                                   $('#myModalFinish').modal('show');
                               }

                               else {
                                   if (langcode == 'es') {
                                       switch (lResult.Url.toUpperCase()) {
                                           case 'UNKNOWN USER': $("#message").html('Usuario Desconocido'); break;
                                           case 'NO TIME CREDIT': $("#message").html('Tiempo de conexión agotado'); break;
                                           case 'TIME EXPIRED': $("#message").html('Ticket Caducado'); break;
                                           case 'TOO MANY DEVICES': $("#message").html('Demasiados dispositivos'); break;
                                           case 'SORRY, NO MORE USERS ALLOWED': $("#message").html('Lo sentimos, no se permiten más usuarios'); break;
                                           case 'WRONG PASSWORD': $("#message").html('Contraseña incorrecta'); break;
                                           case 'UPLOAD QUOTA EXCEEDED': $("#message").html('Cuota de datos de subida agotada'); break;
                                           case 'DOWNLOAD QUOTA EXCEEDED': $("#message").html('Cuota de datos de bajada agotada'); break;
                                           case 'NO BILLING TYPE AVALAIBLES, PLEASE CONTACT FRONT DESK': $("#message").html('No hay tipos de acceso disponibles, por favor pongase en contacto con recepción'); break;
                                           case 'THE DATA ENTERED DO NOT MATCH ANY GUEST. PLEASE CONTACT FRONT DESK': $("#message").html('Los datos introducidos no coinciden con ningún cliente, por favor pongase en contacto con recepción'); break;
                                           default: $("#message").html(lResult.Url); break;
                                       }
                                   }
                                   else {
                                       $("#message").html(lResult.Url);
                                   }
                                   $("#myModalError").modal('show');
                               }
                           },
                           error: function (xhr, ajaxOptions, thrownError) {
                               $("#loading").html("We have a problem, please try again later.");
                               alert('error: ' + xhr.statusText);
                           }
                       });
                   }

               });

               $("#modalscroll").bind('scroll', chk_scroll);

               $("#acceptconditionsbutton").click(function (e) {
                   if ($("#acceptcheck").is(":checked") && $("#olderthancheck").is(":checked") && scrollcomplete == true) //  ($("#modalscroll")[0].scrollHeight - $("#modalscroll").scrollTop() == $("#modalscroll").outerHeight()))
                   {
                       $("#myModal").modal('hide');
                       $("#noaccept").fadeOut("fast");
                       $("#principal").fadeIn("slow");
                   }
                   else {
                       switch (lang.substring(0, 2)) {
                           case 'es': $("#message").html("Por favor, antes de Aceptar debe leer totalmente el texto (utilice el control deslizante) y aceptar la Politica de Privacidad y las Condiciones de Uso del servicio."); break;
                           default: $("#message").html("Please, before proceeding you must read completely the legal text and accept the Privacy Policy and the Terms of Use."); break;
                       }
                       $("#myModalError").modal("show");
                   }
               });

               function chk_scroll(e) {
                   var elem = $(e.currentTarget);
                   if (elem.scrollTop() >= (elem[0].scrollHeight - elem.outerHeight()) * 0.8) {
                       scrollcomplete = true;
                   }

               }

               $('#myModal').on('show.bs.modal', function () {
                   $('#modalscroll').css('overflow-y', 'auto');
                   $('#modalscroll').css('max-height', $(window).height() * 0.25);
               });

           });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div id="fb-root"></div>
    <div class="container">
        <div class="row" id="wait">
            <div class="col-xs-12 col-sm-12 col-md-12">
                  <div class="panel panel-default">
                  <div class="panel-body">
                    <div class="progress">
                            <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                <span class="sr-only">Loading - Waiting please</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" id="noaccept" style="display:none;">
            <div class="col-xs-12 col-sm-12 col-md-12">
                  <div class="panel panel-default">
                  <div class="panel-body text-center">
                      <p class="text-center" id="sorrytext">We are sorry</p>  
                      <p class="text-center"> <img src="#" style="width:150px" id="noacceptimage"/></p> 
                        <p class="text-center" id="noaccepttext">In order to use the service you must accept the Terms of Use</p>
                        <p class="text-center"><button type="button" class="btn btn-primary"  id="retrybutton">Try Again</button></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" id="principal" style="display:none">
            <div class="hidden-xs hidden-sm col-md-8 col-lg-8">
                <div class="panel panel-default">
                  <div class="panel-body">
                        <img src="" id="imagehome" class="img-responsive img-rounded" />
                  </div>
                </div>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="panel panel-default">
                  <div class="panel-body">
                    <ul class="nav nav-tabs" role="tablist" id="myTab">
                      <li id="tab-social" style="display:none;"><a href="#facebook" id="sociallabel">Social</a></li>
                      <li id="tab-payaccess" style="display:none;"><a href="#pay" id="premiumlabel">Premium</a></li>
                      <li id="tab-free" style="display:none;"><a href="#free" id="freelabel">Free</a></li>
                      <li id="tab-login" style="display:none;"><a href="#login" id="loginlabel">Login</a></li>
                      <li id="tab-custom" style="display:none;"><a href="#custom" id="customlabel">Guests</a></li>
                    </ul>
                      <!-- Tab panes -->
                    <div id="myTabContent" class="tab-content">
                     <div class="tab-pane fade" id="login">
                          <p class="text-center"> <img src="#" style="width:150px" id="loginimage"/></p> 
                         <div class="panel panel-info"><div class="panel-body" id="logintext"></div></div>
                            <div role="form">
                                <div class="form-group">
                                <label for="username" id="usernamelabel">Username</label>
                                <input type="text" class="form-control" id="username" placeholder="Username"/>
                                </div>
                                <div class="form-group">
                                <label for="password" id="passwordlabel">Password</label>
                                <input type="password" class="form-control" id="password" placeholder="Password"/>
                                </div>
                               <button type="button" class="btn btn-primary btn-lg btn-block" id="loginbutton" >Login</button>
                            </div>                     
                     </div>
                      <div class="tab-pane fade" id="free">
                          <p class="text-center"> <img src="#" style="width:150px" id="freeimage"/></p> 
                          <div class="panel panel-info"><div class="panel-body" id="freetext"></div></div>
                          
                          <button type="button" class="btn btn-primary btn-lg btn-block" id="freebutton">Get a free access!</button>

                          
                      </div>

                      <div class="tab-pane fade" id="pay">
                            <p class="text-center"> <img src="#" style="width:150px" id="premiumimage"/></p> 
                          <div class="panel panel-info"><div class="panel-body" id="premiumtext"></div></div>
                          <div role="form">
                                <div class="form-group">
                                <label for="accesstype" id="premiumaccesstype">Select your access type</label>
                                <select class="form-control" id="accesstype"><option value="0">Option 1  - 12 €</option><option value="0">Option 1  - 12 €</option><option value="0">Option 1  - 12 €</option></select>
                                </div>
                                <div class="form-group">
                                    <div class="panel panel-warning"><div class="panel-body" id="connectioninfo"></div></div>
                                </div>

                                <button type="button" class="btn btn-primary btn-lg btn-block" id="premiumbutton">Buy</button>
                            </div>
                      </div>
                      <div class="tab-pane fade" id="facebook">
                           <p class="text-center"> <img style="width:150px" id="socialimage"/></p> 
                          <div class="panel panel-info"><div class="panel-body" id="socialText"></div></div>
                          <div class="panel panel-default text-center" id="socialdiv" style="display:none"><div class="panel panel-heading text-center"><h3 id="socialPageName"></h3></div><div class="panel-body" id="panelMeGusta"></div></div>
                          <button type="button" id="socialbutton" class="btn btn-primary btn-lg btn-block"><i class="fa fa-facebook-square"></i> Facebook</button> 
                          <!--<button type="button" id="instagrambutton" class="btn btn-warning btn-lg btn-block"><i class="fa fa-instagram"></i> Instragram</button> -->
                      </div>
                    <div class="tab-pane fade" id="custom">
                          <p class="text-center"> <img src="../resources/images/23/logo.png" style="width:150px" id="customimage"/></p> 
                         <div class="panel panel-info"><div class="panel-body" id="customtext"></div></div>
                            <div role="form">
                                <div class="form-group">
                                <label for="room" id="roomlabel">Room Number</label>
                                <input type="text" class="form-control" id="room" placeholder="Room Number"/>
                                </div>
                                <div class="form-group">
                                    <label for="surname" id="checkindate">Check in Date</label>
                                     <div class='input-group date' id='datetimepicker1'>
                                        <input type='text' class="form-control" id="checkin" />
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                               <button type="button" class="btn btn-primary btn-lg btn-block" id="customlogin" >Login</button>
                            </div>      
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" id="disclaimermandatory" style="display:none;">
                <a href="#" id="disclaimerMandatoryButton" class="btn btn-default">Legal Advice</a>
            </div>
        </div>
    </div>
        <!-- Modal -->
     <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="overflow-y: initial;">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
               <h4 class="modal-title text-center" id="welcometext">Welcome</h4>
              <p class="text-center"> <img src="#" style="height:55px" id="disclaimerimage"/></p> 
              <p class="text-center" id="disclaimerwelcome">Please read the text below and confirm you agree with it.</p>
          </div>
          <div class="modal-body" id="modalscroll">
                <div class="panel-body" id="disclaimertext">
                    
                  </div>
          </div>
          <div class="modal-footer">
                <div class="form-check text-left">
                    <input type="checkbox" id="olderthancheck" class="form-check-input"  /><label  class="form-check-label" for="olderthancheck" id="labeloltherthan" style="font-size:12px; display:initial;"> I accept the <a href='legal.aspx?iddisclaimertype=1' id='olderlink' target='_blank'>Privacy Policy</a> and I am over 16-years or have been authorized by my legal tutor</label><br />
                    <input type="checkbox" id="acceptcheck" class="form-check-input"  /><label class="form-check-label" for="acceptcheck" id="labelacceptcheck" style="font-size:12px; display:initial;"> I accept the  <a href='legal.aspx?iddisclaimertype=2' id='termslink' target='_blank'>Terms and Conditions of Use</a></label>
                </div>
              <br />
              <div class="col-xs-6 col-sm-6 col-md-6"><button type="button" class="btn btn-default btn-lg btn-block btn-sm" id="rejectconditions">Decline</button></div>
              <div class="col-xs-6 col-sm-6 col-md-6"><button type="button" class="btn btn-primary btn-lg btn-block btn-sm" id="acceptconditionsbutton">Accept</button></div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade in" id="myModalSurvey" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title text-center" id="surveyheader">User Data</h4>
          </div>
          <div class="modal-body" id="survey">
          </div>
          <div class="modal-footer">
              <div id="message2"></div>
            <button type="button" class="btn btn-default" data-dismiss="modal" id="surveyclose">Close</button>
              <button type="button" class="btn btn-primary" id="surveysave">Continue</button>
          </div>
        </div>
      </div>
    </div>

        <div class="modal fade" id="myModalPremium" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title text-center" id="premiummailHeader">Email</h4>
          </div>
          <div class="modal-body" id="Div2">
              <div id="premiummailtext">Please provide your email address so we can send the access credentials</div>
              <div class="form-group"><label id="labelpremiumemail">Email</label><input name="email" id="premiummail" type="text" class="form-control" placeholder="Email" /></div>
          </div>
          <div class="modal-footer">
              <div id="message3"></div>
            <button type="button" class="btn btn-default" data-dismiss="modal" id="CancelPremium">Close</button>
              <button type="button" class="btn btn-primary" id="ContinuePremium">Continue</button>
          </div>
        </div>
      </div>
    </div>

        <div class="modal fade" id="myModalError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title text-center" id="H2">Error</h4>
          </div>
          <div class="modal-body">
            <div id="message" class="alert alert-danger text-center"></div> 
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="errorbutton">Close</button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="myModalMandatoryDisclaimer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title text-center" id="disclaimermandatoryheader">Legal Advice</h4>
                </div>
                <div class="modal-body" id="disclaimermandatorytext">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="diclaimermandatoruclose">Close</button>
                  </div>
            </div>
        </div>
    </div>

        <div class="modal fade" id="myModalFinish" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title text-center" id="finishlabel">Congratulations</h4>
          </div>
          <div class="modal-body">
                <div id="finishInformation" style="text-align:center" ><img src="#" alt="logo hotel" id="logofinish" style="width:150px"  /><br /><br /></div>
              <br />
                <input type="button" id="finishbutton" class="btn btn-success btn-block" value="Continue browsing" /> 
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="MyModalWait" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center" id="waitheader">Please, wait a moment</h4>
                </div>
                <div class="modal-body">
                    <div class="progress">
                        <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                            <span class="sr-only">Waiting please</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="accesstypes" style="display:none"></div>
        <div id="freeaccessinformation" style="display:none"></div>
        <div id="socialaccessinformation" style="display:none"></div>
        <input type="text" hidden="hidden" id="idsocialbillintype" />
        <input type="text" hidden="hidden" id="facebookPageID" />
        <input type="text" hidden="hidden" id="idpromotion" />
        <input type="text" hidden="hidden" id="mandatorysurvey" />
        <input type="text" hidden="hidden" id="freeaccess" />
        <input type="text" hidden="hidden" id="socialaccess" />

</asp:Content>
