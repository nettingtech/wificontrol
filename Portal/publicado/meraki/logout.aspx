﻿<%@ Page Title="" Language="C#" MasterPageFile="~/resources/master/MerakiMaster.Master" AutoEventWireup="true" CodeBehind="logout.aspx.cs" Inherits="Portal.meraki.logout" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        $(document).ready(function (e) {
            $("#logo").attr("src", "/meraki/resources/images/" + $("#idhotel").val() + "/" + $("#idlocation").val() + "-logo.png");

            var continue_url = window.location.origin + "/meraki/default.aspx";
            $("#buttonContinue").attr("href", continue_url);
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div class="container">
        <div class="row" id="principal">
            <div class="col-xs-12 col-md-8 col-lg-8 col-lg-offset-1 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                         <p class="text-center"> <img src="#" id="logo" style="width:150px;" /></p>
                        <div class="alert alert-success text-center">
                          <p>  You are OUT</p>
                        </div>

                        <a href="#" id="buttonContinue" class="btn btn-default btn-block">login</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
