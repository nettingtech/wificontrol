﻿<%@ Page Title="" Language="C#" MasterPageFile="~/resources/master/MerakiMaster.Master" AutoEventWireup="true" CodeBehind="successurl.aspx.cs" Inherits="Portal.meraki.successurl" %>

<asp:Content ContentPlaceHolderID="head" ID="Content1" runat="server">
    <script>

        function getParameters() {
            var searchString = window.location.search.substring(1)
                , params = searchString.split("&")
                , hash = {}
            ;

            for (var i = 0; i < params.length; i++) {
                var val = params[i].split("=");
                hash[unescape(val[0])] = unescape(val[1]);
            }
            return hash;
        }

        $(document).ready(function (e) {

            var parameters = getParameters();

            $("#logo").attr("src", "/meraki/resources/images/" + $("#idhotel").val() + "/" + $("#idlocation").val() + "-logo.png");

            var cookie = getCookie("merakiuser");
            if (cookie != "") {
                var username = "";
                values = cookie.split('&');
                for (var i = 0; i < values.length; i++) {
                    switch (values[i].split("=")[0]) {
                        case 'continue_url': $("#buttonContinue").attr("href", values[i].split("=")[1]); break;
                        case 'username': $("#labelusername").html("UserName: <span style='color:#004085;'>" + values[i].split("=")[1] + "</span>"); username = values[i].split("=")[1]; break;
                        case 'password': $("#labelpassword").html("Password: <span style='color:#004085;'> " + values[i].split("=")[1]) + "</span>";  break;
                        case 'validtill': $("#labelvalidtill").html("ValidTill: <span style='color:#004085;'> " + values[i].split("=")[1] + "</span>"); break;
                        }
                }

                var continue_url = window.location.origin + "/meraki/logout.aspx";

                var data = {};
                data["action"] = 'SAVELOGOUTURL';
                data["mac"] = $("#mac").val();
                data["user"] = username;
                data["idhotel"] = $("#idhotel").val();
                data["idlocation"] = $("#idlocation").val();
                data["url"] = parameters.logout_url + "&continue_url=" + continue_url;



                $.ajax({
                    url: "../resources/handlers/MerakiHandler.ashx",
                    type: "POST",
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    beforeSend: function () {
                        $("#myModalWait").modal('show');
                    },
                    complete: function () {
                        $("#myModalWait").modal('hide');
                    },
                    success: function (msg) {
                        var lReturn = JSON.parse(msg);
                        
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#message").removeClass().addClass("alert alert-danger");
                        $("#message").html(xhr.statusText);
                        $("#myModalMessage").modal('show');

                    }
                });
            }

            $("#buttonLogout").click(function (e) {
                var param = getParameters();
                var continue_url = window.location.origin + "/meraki/logout.aspx";
                window.location.href = param.logout_url + "&continue_url=" + continue_url;
            });
        });

        
    </script>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1">
    <div class="container">
        <div class="row" id="principal">
            <div class="col-xs-12 col-md-8 col-lg-8 col-lg-offset-1 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                         <p class="text-center"> <img src="#" id="logo" style="width:150px;" /></p>
                        <div class="alert alert-success text-center">
                            
                            <label id="labelusername"></label><br />
                            <label id="labelpassword"></label><br />
                            <label id="labelvalidtill"></label><br />
                        </div>
                        <a href="#" id="buttonLogout" class="btn btn-warning btn-block">Logout</a>

                        <a href="#" id="buttonContinue" target="_blank" class="btn btn-default btn-block">Continuar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>