﻿<%@ Page Title="" Language="C#" MasterPageFile="~/resources/master/MerakiMaster.Master" AutoEventWireup="true" CodeBehind="paypalsuccess.aspx.cs" Inherits="Portal.meraki.paypalsuccess" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function (e) {

            var continue_url = window.location.origin + "/meraki/successurl.aspx";
            var val = "username=" + $("#usernamepay").val() + "&password=" + $("#passwordpay").val() + "&validtill=" + $("#validtillpay").val() + "&continue_url=" + $("#continue_urlpay").val();
            createCookie("merakiuser", val, 1);

            var action = $("#login_urlpay").val();

            var newForm = jQuery('<form>', {
                'action': action,
                'method': 'POST',
            }).append(jQuery('<input>', {
                'name': 'email',
                'value': $("#usernamepay").val(),
                'type': 'hidden'
            })).append(jQuery('<input>', {
                'name': 'password',
                'value': $("#passwordpay").val(),
                'type': 'hidden'
            })).append(jQuery('<input>', {
                'name': 'success_url',
                'value': continue_url,
                'type': 'hidden'
            })).append(jQuery('<input>', {
                'name': 'continue_url',
                'value': $("#continue_urlpay").val(),
                'type': 'hidden'

            }));
            $(document.body).append(newForm);
            newForm.submit();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row" id="principal" style="display:none">
            <div class="col-xs-12 col-sm-12 col-md-12">
            </div>
            </div>
        </div>
        <div class="modal fade" id="myModalFinish" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header text-center">
            <h4 class="modal-title"><asp:Label runat="server" ID="finishlabel"></asp:Label></h4>
          </div>
          <div class="modal-body">
                <div id="finishInformation" class="text-center"><asp:Literal ID="literalText" runat="server"></asp:Literal></div>
              <br />
                <asp:HyperLink runat="server" ID="finish" CssClass="btn btn-success btn-block" Text="Continue browsing"></asp:HyperLink>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>
        <input type="hidden" id="usernamepay" value="<%Response.Write(username);%>" />
        <input type="hidden" id="passwordpay" value="<%Response.Write(password);%>" />
        <input type="hidden" id="validtillpay" value="<%Response.Write(validtill);%>" />
        <input type="hidden" id="continue_urlpay" value="<%Response.Write(continue_url);%>" />
        <input type="hidden" id="login_urlpay" value="<%Response.Write(login_url);%>" />
</asp:Content>