﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.resources.response
{
    public class UserResponse
    {
        public int IdUser { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ValidTill { get; set; }
        public string Url { get; set; }
        public int allow { get; set; }
        
    }
}