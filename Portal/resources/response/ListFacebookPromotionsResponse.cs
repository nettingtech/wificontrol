﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.resources.response
{
    public class ListFacebookPromotionsResponse
    {
        public List<FacebookPromotionResponse> list { get; set; }
    }
}