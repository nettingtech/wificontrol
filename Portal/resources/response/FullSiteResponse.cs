﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.resources.response
{
    public class FullSiteResponse
    {
        public int IdHotel { get; set; }
        public string Name { get; set; }
        public string Disclaimer { get; set; }
        public string LocationText { get; set; }
        public string LoginText { get; set; }
        public string VoucherText { get; set; }
        public string FreeText { get; set; }
        public string PremiumText { get; set; }
        public string SocialText { get; set; }
        public string CustomText { get; set; }
        public bool LoginModule { get; set; }
        public bool VoucherModule { get; set; }
        public bool FreeAccessModule { get; set; }
        public bool PayAccessModule { get; set; }
        public bool SocialNetworksModule { get; set; }
        public bool CustomAccessModule { get; set; }
        public bool MandatorySurvey { get; set; }
        public int IdLanguageDefault { get; set; }
        public int DefaultModule { get; set; }
        public List<BillingTypeResponse> listPremium { get; set; }
        public BillingTypeResponse freeAccess { get; set; }
        public BillingTypeResponse socialAccess { get; set; }
        public List<SurveyResponse> survey { get; set; }
        public FacebookPromotionResponse facebookPromotion { get; set; }
        public bool DisclaimerMandatory { get; set; }

        public List<ResponseSocialNetwork> redes { get; set; }
    }
}