﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.resources.response
{
    public class DisclaimerResult
    {
        public string Text { get; set; }
    }
}