﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.resources.response
{
    public class FacebookPromotionResponse
    {
        public int IdPromotion { get; set; }
        public int IdHotel { get; set; }
        public int IdLocation { get; set; }
        public int IdBillingType { get; set; }
        public string Page { get; set; }
        public string PageID { get; set; }
        public string PageName { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool Visible { get; set; }
        public int ValidDays { get; set; }
        public int Order { get; set; }
    }
}