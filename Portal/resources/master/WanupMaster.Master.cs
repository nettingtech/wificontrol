﻿using System;
using System.Configuration;
using System.Web;
using System.Web.UI;
using Wifi360.Data.Class;
using Wifi360.Data.Controllers;
using Wifi360.Data.Model;

namespace Portal.resources.master
{
    public partial class WanupMaster : System.Web.UI.MasterPage
    {
        protected string language = string.Empty;
        protected string idhotel = string.Empty;
        protected string idlocation = string.Empty;
        protected string mac = string.Empty;
        protected string origin = string.Empty;
        protected string error = string.Empty;
        protected string urlHotel = string.Empty;
        protected string urlDest = string.Empty;
        private Hotels hotel = null;
        protected Guid idproceso = Guid.Empty;

        private static LogDaily log = null;
        private bool isLog = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    isLog = Boolean.Parse(ConfigurationManager.AppSettings["isLog"].ToString());
                    if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Inicio del proceso LOAD", idproceso));

                    if (hotel.MacAuthenticate)
                    {
                        if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|INICIO DEL PROCESO MAC AUTHENTICATE", idproceso));
                        RememberMeTable remember = BillingController.GetRememberMeTableUser(mac, hotel.IdHotel);

                        if (!remember.IdRememberMe.Equals(0))
                        {
                            Users user = BillingController.GetRadiusUser(remember.UserName, hotel.IdHotel);
                            if (user.Enabled)
                            {
                                if (isLog)
                                {
                                    log.FileWriteEntry(ELogType.Inform, string.Format("{0}|USUARIO VALIDO PARA MAC {1} --> USUARIO:  {2}", idproceso, mac, user.Name));
                                    log.FileWriteEntry(ELogType.Inform, string.Format("{0}|SE LANZA PROCESO DE LOGIN", idproceso, mac, user.Name));
                                }
                                string urlDestino = string.Format("{0}?username={1}&password={2}&dst={3}", origin, user.Name, user.Password, urlDest);
                                Response.Redirect(urlDestino, false);
                            }
                            else
                            {
                                if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|SIN USUARIO VALIDO PARA MAC {1}", idproceso, mac));
                            }
                        }
                        else
                        {
                            if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|SIN ENTRADA EN REMEMBERMETBL MAC {1}", idproceso, mac));
                        }

                        if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|FIN DEL PROCESO MAC AUTHENTICATE", idproceso));
                    }
                    else
                    {
                        if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|PROCESO MAC AUTENTICATE DESHABILITADO", idproceso));
                    }

                }
                catch (Exception ex)
                {
                    if (isLog) log.FileWriteEntry(ELogType.Error, string.Format("{0}|{1}", idproceso, ex.Message));
                    Response.End();
                }
                finally
                {
                    if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Fin del Proceso LOAD", idproceso));
                }
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                try
                {
                    isLog = Boolean.Parse(ConfigurationManager.AppSettings["isLog"].ToString());
                    idproceso = Guid.NewGuid();

                    if (isLog)
                    {
                        log = new LogDaily("Mikrotik-Captive", HttpContext.Current.Server.MapPath("/log"));
                        log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Inicio del proceso", idproceso));
                    }

                    #region OBTENER PARAMETRO DE ENTRADA (HOTEL, LOCATION, MAC, ORIGEN, ETC)
                    //OBTENEMOS LOS PARAMETRO POR EL METODO POST
                    string hotelid = Request.Form["hotelid"];
                    string locationid = Request.Form["locationid"];
                    string macs = Request.Form["MAC"];
                    origin = Request.Form["link-login-only"];
                    error = Request.Form["error"];
                    urlDest = Request.Form["link-orig"];

                    if (Session["hotelid"] == null)
                        Session["hotelid"] = hotelid;
                    if (Session["locationid"] == null)
                        Session["locationid"] = locationid;
                    if (Session["mac"] == null)
                        Session["mac"] = macs;
                    if (Session["origin"] == null)
                        Session["origin"] = origin;
                    if (Session["error"] == null)
                        Session["error"] = error;

                    if (isLog)
                    {
                        log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Lectura parámetros método POST.", idproceso));
                        log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Parámetros: hotelid --> {1} ; locationid = {2} ; macs = {3} ; origin = {4} ; error = {5} ; urlDest = {6}.", idproceso, hotelid, locationid, macs, origin, error, urlDest));
                    }

                    if (string.IsNullOrEmpty(hotelid)) //EN CASO DE NO TENER POST PROBAMOS POR GET O COOKIE
                    {
                        if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Lectura parámetros método POST sin resultados.", idproceso));
                        if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Lectura parámetros método GET.", idproceso));

                        if (HttpContext.Current.Request["idhotel"] != null)
                        {
                            hotelid = HttpContext.Current.Request["idhotel"].ToString();
                            locationid = HttpContext.Current.Request["idlocation"].ToString();
                            macs = HttpContext.Current.Request["mac"].ToString();
                            origin = HttpContext.Current.Request["origin"].ToString();
                            error = HttpContext.Current.Request["error"].ToString();

                            if (isLog)
                            {
                                log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Parámetros: hotelid --> {1} ; locationid = {2} ; macs = {3} ; origin = {4} ; error = {5}.", idproceso, hotelid, locationid, macs, origin, error));
                                if (!string.IsNullOrEmpty(hotelid))
                                    log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Lectura parámetros método GET COMPLETADA.", idproceso));
                                else
                                    log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Lectura parámetros método GET COMPLETADA sin resultados.", idproceso));
                            }
                        }
                        else if (Session["hotelid"] != null)
                        {
                            if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Lectura parámetros método GET sin resultados.", idproceso));
                            if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Lectura parámetros método VARIABLE DE SESSION.", idproceso));

                            hotelid = Session["hotelid"].ToString();
                            locationid = Session["locationid"].ToString();
                            macs = Session["mac"].ToString();
                            origin = Session["origin"].ToString();
                            error = Session["error"].ToString();

                            if (isLog)
                            {
                                if (!string.IsNullOrEmpty(hotelid))
                                    log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Lectura parámetros método VARIABLE DE SESSION COMPLETADA.", idproceso));
                                else
                                    log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Lectura parámetros método VARIABLE DE SESSION sin resultados.", idproceso));
                            }
                        }
                        else
                        {
                            if (isLog)
                            {
                                log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Lectura parámetros VARIABLE DE SESSION sin resultados.", idproceso));
                                log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Lectura parámetros de COOKIE.", idproceso));
                            }
                            if (HttpContext.Current.Request.Cookies["mikrotik"] != null)
                            {
                                HttpCookie cookie = HttpContext.Current.Request.Cookies["mikrotik"];
                                hotelid = cookie.Values["idhotel"].ToString();
                                locationid = cookie.Values["idlocation"].ToString();
                                macs = cookie.Values["mac"].ToString();
                                origin = cookie.Values["origin"].ToString();

                                if (isLog)
                                {

                                    log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Parámetros: hotelid --> {1} ; locationid = {2} ; macs = {3} ; origin = {4} ; error = {5}.", idproceso, hotelid, locationid, macs, origin, error));
                                    if (!string.IsNullOrEmpty(hotelid))
                                        log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Lectura parámetros de COOKIE COMPLETADA.", idproceso));
                                }
                            }
                            else
                            {
                                if (isLog) log.FileWriteEntry(ELogType.Warning, string.Format("{0}|Fin del proceso: NO HAY PARÁMETROS QUE LEER", idproceso));
                                //Response.End();
                            }
                        }
                    }
                    else
                    {
                        if (isLog)
                        {
                            log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Lectura parámetros método POST COMPLETADA.", idproceso));
                        }
                    }
                    #endregion

                    string userAgent = Request.UserAgent;

                    if (!string.IsNullOrEmpty(macs))
                    {
                        string[] macpre = macs.Split(',');

                        if (macpre.Length >= 1)
                        {
                            mac = macpre[0].Replace(':', '-');
                        }
                        else
                            mac = macs;
                    }

                    if (!string.IsNullOrEmpty(hotelid)) //COMPROBAMOS QUE AUNQUE VENGA HOTEL NO VENGA VACIO
                    {
                        #region COMPROBAMOS USERAGENT Y MAC

                        if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|PROCESANDO MAC --> {1}", idproceso, mac));

                        //COMPROBAMOS SI EL USERAGENT ESTÁ EN LA LISTA NEGRA
                        BlackListUserAgents blackUA = BillingController.GetBlackUserAgent(userAgent);

                        if (blackUA.IdBlackUserAgent.Equals(0)) //SI NO ESTÁ EN LA LISTA NEGRA, VAMOS A COMPROBAR LA LISTA NEGRA DE LAS MACS
                        {
                            if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|USERAGENT PERMITIDO --> {1}", idproceso, userAgent));
                            hotel = BillingController.GetHotel(Int32.Parse(hotelid));

                            if (hotel.MACBlocking) //SI TIENE ACTIVADO EL BLOQUEO POR MACS
                            {
                                if (!string.IsNullOrEmpty(mac))
                                {
                                    #region MACBLOCKING
                                    //COMPROBAMOS SI LA MAC ESTÁ BLOQUEADA
                                    BlackListMACs blackMAC = BillingController.GetBlackListMac(mac, DateTime.Now);

                                    if (blackMAC.IdMACBlackList.Equals(0)) //SI LA MAC NO ESTÁ BLOQUEADA
                                    {
                                        if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|MAC PERMITIDA --> {1}", idproceso, mac));
                                        //COMPROBAMOS SI HAY QUE METER LA MAC EN LA LISTA NEGRA
                                        if (hotel.MaxMACAttemps <= BillingController.GetUserAgentsTemps(mac, hotel.MACAttempsInterval)) //SI HAY QUE METERLA LA AGREGAMOS, Y MATAMOS LA CONEXION
                                        {
                                            BlackListMACs newBLM = new BlackListMACs();
                                            newBLM.IdHotel = hotel.IdHotel;
                                            newBLM.MAC = mac;
                                            newBLM.BanningStart = DateTime.Now;
                                            newBLM.BanningEnd = DateTime.Now.AddSeconds(hotel.MACBannedInterval);

                                            BillingController.SaveBlackListMac(newBLM);

                                            if (isLog) log.FileWriteEntry(ELogType.Warning, string.Format("{0}|Fin del proceso: NUEVA MAC BLOQUEADA --> {1}", idproceso, mac));

                                            Response.End();
                                        }
                                    }
                                    else //SI ESTÁ EN LA LISTA NEGRA DE MAC, MATAMOS EL PROCESO
                                    {
                                        if (isLog) log.FileWriteEntry(ELogType.Warning, string.Format("{0}|Fin del proceso: MAC BLOQUEADA --> {1}", idproceso, mac));
                                        Response.End();
                                    }

                                    #endregion
                                }
                            }
                            else
                            {
                                if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|BLOQUEO POR MAC NO HABILITADO", idproceso));
                            }

                            UserAgents uA = BillingController.SalvarTransaccionado(userAgent);

                            if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|UserAgent actualizado en base de datos: {1}", idproceso, userAgent));

                            //DAMOS ENTRADA AL NUEVO USERAGENT Y LO GUARDAMOS EN LA BASE DE DATOS TEMPORAL PARA VER SI ES VÁLIDO.
                            if (!uA.IdUserAgent.Equals(0))
                            {
                                UserAgentsTemp temp = new UserAgentsTemp();
                                temp.IdUserAgent = uA.IdUserAgent;
                                temp.MAC = mac;
                                temp.AttemptTime = DateTime.Now;

                                BillingController.SaveUserAgentTemp(temp);

                                if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|UserAgentTemp insertado en base de datos: ID--> {1}  IDUSERAGENT --> {2}", idproceso, temp.IdTemp, temp.IdUserAgent));
                            }
                            else
                                if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Intento Insersión UserAgentTemp con IdUserAgent = 0: MAC--> {1}  IDUSERAGENT --> {2}", idproceso, mac, uA.String));

                        }
                        else //SI ESTÁ EN LA LISTA NEGRA DE USERAGENT, MATAMOS EL PROCESO
                        {
                            if (isLog) log.FileWriteEntry(ELogType.Warning, string.Format("{0}|Fin del proceso: USERAGENT BLOQUEADO --> {1}", idproceso, userAgent));
                            Response.End();
                        }

                        #endregion

                        #region OBTENER DE LA BASE DE DATOS, LOS PARAMETROS DEL HOTEL PARA TRABAJAR LUEGO CON ELLOS CON EL JAVASCRIPT

                        urlHotel = hotel.UrlHotel;
                        Locations2 location = null;
                        if (!string.IsNullOrEmpty(locationid))
                            location = BillingController.ObtainLocation(Int32.Parse(locationid));

                        int idlanguage = hotel.IdLanguage;

                        Languages languageDefault = BillingController.GetLanguage(idlanguage);

                        language = languageDefault.Code;
                        idhotel = hotelid;
                        idlocation = locationid;

                        #endregion

                        if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Obtener datos site: IDHOTEL--> {1}  IDLOCATION --> {2}", idproceso, idhotel, idlocation));
                    }
                    else
                    {
                        #region SALVAMOS EL USERAGENT PARA PREVENIR ATAQUES
                        //SALVAMOS EL USERAGENT PARA PREVENIR ATAQUES
                        UserAgents uA = BillingController.SalvarTransaccionado(userAgent);

                        if (isLog) log.FileWriteEntry(ELogType.Warning, string.Format("{0}|Fin del proceso: USERAGENT ACTUALIZADO --> {1}", idproceso, userAgent));

                        Response.End();

                        #endregion
                    }

                    #region APPLEURL
                    //GUARDAMOS LA APPLE URL
                    if (!string.IsNullOrEmpty(urlDest))
                    {
                        AppleUrls appleUrl = new AppleUrls();
                        appleUrl.useragent = userAgent;
                        appleUrl.url = urlDest;
                        appleUrl.DateTime = DateTime.Now;
                        appleUrl.IdHotel = Int32.Parse(hotelid);
                        appleUrl.IdLocation = Int32.Parse(locationid);
                        appleUrl.CallerID = mac;
                        BillingController.SaveAppleUrl(appleUrl);

                        if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Guardamos AppleURL.", idproceso));
                    }

                    #endregion
                }
                catch (Exception ex)
                {
                    if (isLog) log.FileWriteEntry(ELogType.Error, string.Format("{0}|{1}", idproceso, ex.Message));

                    Response.End();
                }
                finally
                {
                    if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Fin del Proceso PREINIT", idproceso));
                }
            }
        }
    }
}