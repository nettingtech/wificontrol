﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Wifi360.Data.Class;
using Wifi360.Data.Controllers;
using Wifi360.Data.Model;

namespace Portal.resources.master
{
    public partial class NomadixMaster : System.Web.UI.MasterPage
    {
        protected string language = string.Empty;
        protected string idhotel = string.Empty;
        protected string idlocation = string.Empty;
        protected string mac = string.Empty;
        protected string urlHotel = string.Empty;
        private Hotels hotel = null;
        protected Guid idproceso = Guid.Empty;

        private static LogDaily log = null;
        private bool isLog = false;

        protected void Page_Init(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                try
                {
                    isLog = Boolean.Parse(ConfigurationManager.AppSettings["isLog"].ToString());
                    idproceso = Guid.NewGuid();

                    if (isLog)
                    {
                        log = new LogDaily("Nomadix-Captive", HttpContext.Current.Server.MapPath("/log"));
                        log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Inicio del proceso", idproceso));
                    }

                    string ui = string.Empty;
                    string rn = string.Empty;

                    #region OBTENER PARAMETROS DE ENTRADA

                    if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Lectura parámetros método GET.", idproceso));
                    if (HttpContext.Current.Request["UI"] != null)
                    {
                        ui = HttpContext.Current.Request["UI"].ToString();
                        rn = HttpContext.Current.Request["RN"].ToString();
                        mac = HttpContext.Current.Request["MA"].ToString();

                        if (isLog)
                        {
                            log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Parámetros: ui --> {1} ; rn = {2} ; mac = {3}.", idproceso, ui, rn, mac));
                            log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Lectura parámetros método GET COMPLETADA.", idproceso));
                        }
                    }
                    else if (HttpContext.Current.Request.Cookies["nomadix"] != null)
                    {
                        HttpCookie cookie = HttpContext.Current.Request.Cookies["nomadix"];
                        idhotel = cookie.Values["idhotel"].ToString();
                        idlocation = cookie.Values["idlocation"].ToString();
                        mac = cookie.Values["mac"].ToString();

                        if (isLog)
                        {

                            log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Parámetros: hotelid --> {1} ; locationid = {2} ; macs = {3}.", idproceso, idhotel, idlocation, mac));
                            if (!string.IsNullOrEmpty(idhotel))
                                log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Lectura parámetros de COOKIE COMPLETADA.", idproceso));
                        }
                    }

                    #endregion

                    if (HttpContext.Current.Request["MA"] != null)
                    {
                        string macformated = string.Empty;
                        if (!mac.Contains('-'))
                        {
                            for (int i = 1; i <= mac.Length; i++) //COGEMOS LA MAC POR PARAMETROS Y LA FORMATEAMOS PARA COMPARAR
                            {
                                macformated += mac[i - 1];
                                if (i % 2 == 0)
                                    macformated += "-";
                            }

                            mac = macformated.Substring(0, macformated.Length - 1);
                        }
                    }

                    if ((!string.IsNullOrEmpty(ui)) || (!string.IsNullOrEmpty(idhotel)))
                    {
                        #region USERAGENT Y MAC

                        string userAgent = Request.UserAgent;

                        if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|PROCESANDO MAC --> {1}", idproceso, mac));

                        //COMPROBAMOS SI EL USERAGENT ESTÁ EN LA LISTA NEGRA
                        BlackListUserAgents blackUA = BillingController.GetBlackUserAgent(userAgent);

                        if (blackUA.IdBlackUserAgent.Equals(0)) //SI NO ESTÁ EN LA LISTA NEGRA, VAMOS A COMPROBAR LA LISTA NEGRA DE LAS MACS
                        {
                            if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|USERAGENT PERMITIDO --> {1}", idproceso, userAgent));
                            if (!string.IsNullOrEmpty(ui))
                                hotel = BillingController.GetHotel(ui);
                            else
                                hotel = BillingController.GetHotel(Int32.Parse(idhotel));

                            if (!hotel.IdHotel.Equals(0))
                            {
                                if (hotel.MACBlocking)
                                {
                                    if (!string.IsNullOrEmpty(mac))
                                    {
                                        #region MACKBLOCKING

                                        //COMPROBAMOS SI LA MAC ESTÁ BLOQUEADA
                                        BlackListMACs blackMAC = BillingController.GetBlackListMac(mac, DateTime.Now);

                                        if (blackMAC.IdMACBlackList.Equals(0)) //SI LA MAC NO ESTÁ BLOQUEADA
                                        {
                                            if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|MAC PERMITIDA --> {1}", idproceso, mac));

                                            //COMPROBAMOS SI HAY QUE METER LA MAC EN LA LISTA NEGRA
                                            if (hotel.MaxMACAttemps <= BillingController.GetUserAgentsTemps(mac, hotel.MACAttempsInterval)) //SI HAY QUE METERLA LA AGREGAMOS, Y MATAMOS LA CONEXION
                                            {
                                                BlackListMACs newBLM = new BlackListMACs();
                                                newBLM.IdHotel = hotel.IdHotel;
                                                newBLM.MAC = mac;
                                                newBLM.BanningStart = DateTime.Now;
                                                newBLM.BanningEnd = DateTime.Now.AddSeconds(hotel.MACBannedInterval);

                                                BillingController.SaveBlackListMac(newBLM);

                                                if (isLog) log.FileWriteEntry(ELogType.Warning, string.Format("{0}|Fin del proceso: NUEVA MAC BLOQUEADA --> {1}", idproceso, mac));

                                                Response.End();
                                            }
                                        }
                                        else //SI ESTÁ EN LA LISTA NEGRA DE MAC, MATAMOS EL PROCESO
                                        {
                                            if (isLog) log.FileWriteEntry(ELogType.Warning, string.Format("{0}|Fin del proceso: MAC BLOQUEADA --> {1}", idproceso, mac));
                                            Response.End();
                                        }

                                        #endregion
                                    }
                                }
                                else
                                {
                                    if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|BLOQUEO POR MAC NO HABILITADO", idproceso));
                                }

                                UserAgents uA = BillingController.SalvarTransaccionado(userAgent);

                                if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|UserAgent actualizado en base de datos: {1}", idproceso, userAgent));

                                //DAMOS ENTRADA AL NUEVO USERAGENT Y LO GUARDAMOS EN LA BASE DE DATOS TEMPORAL PARA VER SI ES VÁLIDO.
                                if (!uA.IdUserAgent.Equals(0))
                                {
                                    UserAgentsTemp temp = new UserAgentsTemp();
                                    temp.IdUserAgent = uA.IdUserAgent;
                                    temp.MAC = mac;
                                    temp.AttemptTime = DateTime.Now;

                                    BillingController.SaveUserAgentTemp(temp);

                                    if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|UserAgentTemp insertado en base de datos: ID--> {1}  IDUSERAGENT --> {2}", idproceso, temp.IdTemp, temp.IdUserAgent));
                                }
                                else
                                    if (isLog) log.FileWriteEntry(ELogType.Warning, string.Format("{0}|Intento Insersión UserAgentTemp con IdUserAgent = 0: MAC--> {1}  IDUSERAGENT --> {2}", idproceso, mac, uA.String));
                            }
                            else
                            {
                                #region salvamos el UserAgent para evitar ataques

                                UserAgents uA = BillingController.SalvarTransaccionado(userAgent);
                                #endregion

                                if (isLog) log.FileWriteEntry(ELogType.Warning, string.Format("{0}|Fin del proceso: HOTEL UI NO VALIDO --> {1}", idproceso, ui));
                                if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|UserAgent actualizado en base de datos: {1}", idproceso, userAgent));
                                Response.End();
                            }

                        }
                        else //SI ESTÁ EN LA LISTA NEGRA DE USERAGENT, MATAMOS EL PROCESO
                        {
                            if (isLog) log.FileWriteEntry(ELogType.Warning, string.Format("{0}|Fin del proceso: USERAGENT BLOQUEADO --> {1}", idproceso, userAgent));
                            Response.End();
                        }

                        #endregion

                        #region Obtenermos los datos del hotel para luego trabajar con ellos en el javascript

                        urlHotel = hotel.UrlHotel;

                        if ((string.IsNullOrEmpty(rn)) && (string.IsNullOrEmpty(idlocation)))
                            rn = "direct_connection";

                        Locations2 location = null;
                        if (string.IsNullOrEmpty(idlocation))
                            location = BillingController.ObtainLocation(hotel.IdHotel, rn);
                        else
                            location = BillingController.ObtainLocation(Int32.Parse(idlocation));

                        int idlanguage = hotel.IdLanguage;

                        Languages languageDefault = BillingController.GetLanguage(idlanguage);

                        language = languageDefault.Code;
                        idhotel = hotel.IdHotel.ToString();
                        idlocation = location.IdLocation.ToString();

                        #endregion

                        if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Obtener datos site: IDHOTEL--> {1}  IDLOCATION --> {2}", idproceso, idhotel, idlocation));

                        #region AppleURL

                        if (Request.QueryString["OS"] != null)
                        {

                            string userAgent2 = Request.UserAgent;

                            AppleUrls appleUrl = new AppleUrls();
                            appleUrl.useragent = userAgent2;
                            appleUrl.url = Request.QueryString["OS"].ToString();
                            appleUrl.DateTime = DateTime.Now;
                            appleUrl.IdHotel = Int32.Parse(idhotel);
                            appleUrl.IdLocation = Int32.Parse(idlocation);
                            appleUrl.CallerID = mac;
                            BillingController.SaveAppleUrl(appleUrl);

                            if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Guardamos AppleURL.", idproceso));
                        }

                        #endregion
                    }
                    else
                    {
                        if (isLog) log.FileWriteEntry(ELogType.Warning, string.Format("{0}|Fin del proceso: NO HAY PARÁMETROS QUE LEER", idproceso));

                        #region SALVAMOS USERAGENT PARA EVITAR ATAQUES

                        string userAgent = Request.UserAgent;

                        UserAgents uA = BillingController.SalvarTransaccionado(userAgent);
                        if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Fin del proceso: USERAGENT ACTUALIZADO --> {1}", idproceso, userAgent));

                        #endregion

                        Response.End();
                    }
                }
                catch (Exception ex)
                {
                    if (isLog) log.FileWriteEntry(ELogType.Error, string.Format("{0}|{1}", idproceso, ex.Message));

                    Response.End();
                }
                finally
                {
                    if (isLog) log.FileWriteEntry(ELogType.Inform, string.Format("{0}|Fin del Proceso PREINIT", idproceso));
                }
            }
        }
    }
}