﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text.RegularExpressions;
using System.Web;
using Wifi360.Data.Controllers;
using Wifi360.Data.Model;

namespace Portal.resources.utils
{
    public class util
    {
        public static string CreateRandomPassword(int PasswordLength)
        {
            string _allowedChars = "23456789";
            Random randNum = new Random(Guid.NewGuid().GetHashCode());
            char[] chars = new char[PasswordLength];
            int allowedCharCount = _allowedChars.Length;

            for (int i = 0; i < PasswordLength; i++)
            {
                chars[i] = _allowedChars[(int)((_allowedChars.Length) * randNum.NextDouble())];
            }

            return new string(chars);
        }

        public static string createRandomUserNumber(int Length)
        {
            string _allowedChars = "ABCDEFGHJKLMNPQRSTUVWXYZ23456789";
            Random randNum = new Random(Guid.NewGuid().GetHashCode());
            char[] chars = new char[Length];
            int allowedCharCount = _allowedChars.Length;

            for (int i = 0; i < Length; i++)
            {
                chars[i] = _allowedChars[(int)((_allowedChars.Length) * randNum.NextDouble())];
            }

            return new string(chars);
        }

        public static void sendMail(int idhotel, int idBillingType, string to, string user, string password, string lang)
        {
            string template = "";
            if (lang.Equals("es"))
            {
                template = HttpContext.Current.Server.MapPath("/resources/HTMLTemplates/PlantillaCorreo_es.html");
            }
            else if (lang.Equals("en"))
            {
                template = HttpContext.Current.Server.MapPath("/resources/HTMLTemplates/PlantillaCorreo_en.html");
            }
            else
            {
                template = HttpContext.Current.Server.MapPath("/resources/HTMLTemplates/PlantillaCorreo_en.html");
            }
            System.IO.StreamReader StreamReader = new System.IO.StreamReader(template);
            string body1 = StreamReader.ReadToEnd();
            StreamReader.Close();


            BillingTypes bt = BillingController.ObtainBillingType(idBillingType);

            string filePath = HttpContext.Current.Server.MapPath(string.Format("/resources/images/{0}/{1}-logo.png", idhotel, bt.IdLocation));

            Hotels h = BillingController.GetHotel(idhotel);

            body1 = body1.Replace("{idhotel}", idhotel.ToString());
            body1 = body1.Replace("{hotelname}", h.Name);
            body1 = body1.Replace("{username}", user);
            body1 = body1.Replace("{password}", password);
            string timetext = string.Empty;
            TimeSpan t = TimeSpan.FromSeconds(bt.TimeCredit / bt.MaxDevices);
            if (bt.TimeCredit >= 86400)
                timetext += string.Format("{0}d ", t.Days);
            if (t.Hours > 0)
                timetext += string.Format("{0:D2}h ", t.Hours);
            if (t.Minutes > 0)
                timetext += string.Format("{0:D2}m ", t.Minutes);
            if (t.Seconds > 0)
                timetext += string.Format("{0:D2}s ", t.Seconds);

            body1 = body1.Replace("{TimeCredit}", timetext);
            body1 = body1.Replace("{ValidSince}", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            body1 = body1.Replace("{ValidTill}", DateTime.Now.AddHours(bt.ValidTill).ToString("dd/MM/yyyy HH:mm:ss"));
            body1 = body1.Replace("{Devices}", bt.MaxDevices.ToString());
            body1 = body1.Replace("{BWDown}", bt.BWDown.ToString());
            body1 = body1.Replace("{BWUp}", bt.BWUp.ToString());
            body1 = body1.Replace("{VolumeUp}", bt.VolumeUp.ToString());
            body1 = body1.Replace("{VolumeDown}", bt.VolumeDown.ToString());
            body1 = body1.Replace("{Description}", bt.PayPalDescription);

            Send(idhotel, to, ConfigurationManager.AppSettings["Subject"].ToString(), body1, filePath);
        }

        public static void sendMailRoot(int idhotel, int idBillingType, string to, string user, string password, string lang)
        {
            try
            {
                string template = "";
                if (lang.Equals("es"))
                {
                    template = "/resources/HTMLTemplates/PlantillaCorreo_es.html";
                }
                else if (lang.Equals("en"))
                {
                    template = "/resources/HTMLTemplates/PlantillaCorreo_en.html";
                }
                System.IO.StreamReader StreamReader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath(template));
                string body1 = StreamReader.ReadToEnd();
                StreamReader.Close();

                BillingTypes bt = BillingController.ObtainBillingType(idBillingType);
                string filePath = HttpContext.Current.Server.MapPath(string.Format("/resources/images/{0}/{1}-logo.png", idhotel, bt.IdLocation));

                Hotels h = BillingController.GetHotel(idhotel);

                body1 = body1.Replace("{idhotel}", idhotel.ToString());
                body1 = body1.Replace("{hotelname}", h.Name);
                body1 = body1.Replace("{username}", user);
                body1 = body1.Replace("{password}", password);
                string timetext = string.Empty;
                TimeSpan t = TimeSpan.FromSeconds(bt.TimeCredit / bt.MaxDevices);
                if (bt.TimeCredit >= 86400)
                    timetext += string.Format("{0}d ", t.Days);
                if (t.Hours > 0)
                    timetext += string.Format("{0:D2}h ", t.Hours);
                if (t.Minutes > 0)
                    timetext += string.Format("{0:D2}m ", t.Minutes);
                if (t.Seconds > 0)
                    timetext += string.Format("{0:D2}s ", t.Seconds);

                body1 = body1.Replace("{TimeCredit}", timetext);
                body1 = body1.Replace("{ValidSince}", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                body1 = body1.Replace("{ValidTill}", DateTime.Now.AddHours(bt.ValidTill).ToString("dd/MM/yyyy HH:mm:ss"));
                body1 = body1.Replace("{Devices}", bt.MaxDevices.ToString());
                body1 = body1.Replace("{BWDown}", bt.BWDown.ToString());
                body1 = body1.Replace("{BWUp}", bt.BWUp.ToString());
                body1 = body1.Replace("{VolumeUp}", bt.VolumeUp.ToString());
                body1 = body1.Replace("{VolumeDown}", bt.VolumeDown.ToString());
                body1 = body1.Replace("{Description}", bt.PayPalDescription);



                Send(idhotel, to, ConfigurationManager.AppSettings["Subject"].ToString(), body1, filePath);

            }
            catch (Exception ex)
            {
                CaptivePortalLogInternal internalLog = new CaptivePortalLogInternal();
                internalLog.IdHotel = idhotel;
                internalLog.IdLocation = 0;
                internalLog.NSEId = "0";
                internalLog.UserName = "";
                internalLog.Password = "";
                internalLog.CallerID = "";
                internalLog.Date = DateTime.Now;
                internalLog.Page = "Sending mail";

                internalLog.NomadixResponse = ex.Message;
                internalLog.RadiusResponse = "NA";

                BillingController.SaveCaptivePortalLogInternal(internalLog);
            }
        }

        public static void sendMailRoot(int idhotel, int idBillingType, string to, string user, string password, string lang, string url)
        {
            try
            {
                string template = "";
                if (lang.Equals("es"))
                {
                    template = "/resources/HTMLTemplates/PlantillaCorreo_es.html";
                }
                else if (lang.Equals("en"))
                {
                    template = "/resources/HTMLTemplates/PlantillaCorreo_en.html";
                }
                System.IO.StreamReader StreamReader = new System.IO.StreamReader(HttpContext.Current.Server.MapPath(template));
                string body1 = StreamReader.ReadToEnd();
                StreamReader.Close();

                BillingTypes bt = BillingController.ObtainBillingType(idBillingType);
                Hotels h = BillingController.GetHotel(idhotel);

                body1 = body1.Replace("{idhotel}", idhotel.ToString());
                body1 = body1.Replace("{hotelname}", h.Name);
                body1 = body1.Replace("{username}", user);
                body1 = body1.Replace("{password}", password);
                body1 = body1.Replace("{urlcaptive}", url);
                string timetext = string.Empty;
                TimeSpan t = TimeSpan.FromSeconds(bt.TimeCredit / bt.MaxDevices);
                if (bt.TimeCredit >= 86400)
                    timetext += string.Format("{0}d ", t.Days);
                if (t.Hours > 0)
                    timetext += string.Format("{0:D2}h ", t.Hours);
                if (t.Minutes > 0)
                    timetext += string.Format("{0:D2}m ", t.Minutes);
                if (t.Seconds > 0)
                    timetext += string.Format("{0:D2}s ", t.Seconds);

                body1 = body1.Replace("{TimeCredit}", timetext);
                body1 = body1.Replace("{ValidSince}", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                body1 = body1.Replace("{ValidTill}", DateTime.Now.AddHours(bt.ValidTill).ToString("dd/MM/yyyy HH:mm:ss"));
                body1 = body1.Replace("{Devices}", bt.MaxDevices.ToString());
                body1 = body1.Replace("{BWDown}", bt.BWDown.ToString());
                body1 = body1.Replace("{BWUp}", bt.BWUp.ToString());
                body1 = body1.Replace("{VolumeUp}", bt.VolumeUp.ToString());
                body1 = body1.Replace("{VolumeDown}", bt.VolumeDown.ToString());
                body1 = body1.Replace("{Description}", bt.PayPalDescription);

                Send(idhotel, to, ConfigurationManager.AppSettings["Subject"].ToString(), body1);
            }
            catch (Exception ex)
            {
                CaptivePortalLogInternal internalLog = new CaptivePortalLogInternal();
                internalLog.IdHotel = idhotel;
                internalLog.IdLocation = 0;
                internalLog.NSEId = "0";
                internalLog.UserName = "";
                internalLog.Password = "";
                internalLog.CallerID = "";
                internalLog.Date = DateTime.Now;
                internalLog.Page = "Sending mail";

                internalLog.NomadixResponse = ex.Message;
                internalLog.RadiusResponse = "NA";

                BillingController.SaveCaptivePortalLogInternal(internalLog);
            }
        }

        private static void Send(int idHotel, string to, string subject, string body)
        {
            Hotels h = BillingController.GetHotel(idHotel);

            string _smtpClient = h.SMTP; ;
            int _smtpPort = Int32.Parse(h.SmtpPort.ToString());
            string _userName = h.SmtpUser;
            string _password = h.SmtpPassword;
            string _from = h.MailFrom;
            string _to = to;
            bool _isBodyHtml = true;
            bool _sendAsync = false;
            bool _ssl = h.SmtpSSL;

            try
            {
                SmtpClient smtpClient = new SmtpClient(_smtpClient, _smtpPort);
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.EnableSsl = _ssl;

                if ((_userName != string.Empty) && (_password != string.Empty))
                {
                    NetworkCredential networkCredential = new NetworkCredential(_userName, _password);
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = networkCredential;
                }

                MailMessage mailMessage = new MailMessage(_from, to, subject, body);
                mailMessage.IsBodyHtml = _isBodyHtml;

                if (_sendAsync)
                    smtpClient.SendAsync(mailMessage, null);
                else
                    smtpClient.Send(mailMessage);
            }
            catch
            {
                throw;
            }
        }

        private static void Send(int idHotel, string to, string subject, string body, string filePath)
        {
            Hotels h = BillingController.GetHotel(idHotel);

            string _smtpClient = h.SMTP; ;
            int _smtpPort = Int32.Parse(h.SmtpPort.ToString());
            string _userName = h.SmtpUser;
            string _password = h.SmtpPassword;
            string _from = h.MailFrom;
            string _to = to;
            bool _isBodyHtml = true;
            bool _sendAsync = false;
            bool _ssl = h.SmtpSSL;

            try
            {
                SmtpClient smtpClient = new SmtpClient(_smtpClient, _smtpPort);
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.EnableSsl = _ssl;

                if ((_userName != string.Empty) && (_password != string.Empty))
                {
                    NetworkCredential networkCredential = new NetworkCredential(_userName, _password);
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = networkCredential;
                }

                LinkedResource inline = new LinkedResource(filePath, MediaTypeNames.Image.Jpeg);
                inline.ContentId = Guid.NewGuid().ToString();

                body = body.Replace("{filename}", String.Format(@"cid:{0}", inline.ContentId));
                AlternateView avHtml = AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Html);
                avHtml.LinkedResources.Add(inline);

                Attachment att = new Attachment(filePath);
                att.ContentDisposition.Inline = true;

                MailMessage mailMessage = new MailMessage(_from, to, subject, body);
                mailMessage.IsBodyHtml = _isBodyHtml;
                mailMessage.AlternateViews.Add(avHtml);
                mailMessage.Attachments.Add(att);

                if (_sendAsync)
                    smtpClient.SendAsync(mailMessage, null);
                else
                    smtpClient.Send(mailMessage);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static bool ValidateEmail(string mail)
        {
            string email = mail;
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(email);
            if (match.Success)
                return true;
            else
                return false;
        }

        private static DateTime DayLightSavingStart(string year)
        {
            try
            {
                DateTime endWeek = DateTime.Parse("01/03/" + year);
                switch (endWeek.DayOfWeek)
                {
                    case DayOfWeek.Sunday: endWeek.AddHours(673); break;
                    case DayOfWeek.Monday: endWeek.AddHours(649); break;
                    case DayOfWeek.Tuesday: endWeek.AddHours(625); break;
                    case DayOfWeek.Wednesday: endWeek.AddHours(601); break;
                    case DayOfWeek.Thursday: endWeek.AddHours(577); break;
                    case DayOfWeek.Friday: endWeek.AddHours(553); break;
                    case DayOfWeek.Saturday: endWeek.AddHours(529); break;
                }

                return endWeek;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private static DateTime DayLightSavingEnd(string year)
        {
            try
            {
                DateTime endWeek = DateTime.Parse("01/10/" + year);
                switch (endWeek.DayOfWeek)
                {
                    case DayOfWeek.Sunday: endWeek.AddHours(673); break;
                    case DayOfWeek.Monday: endWeek.AddHours(649); break;
                    case DayOfWeek.Tuesday: endWeek.AddHours(625); break;
                    case DayOfWeek.Wednesday: endWeek.AddHours(601); break;
                    case DayOfWeek.Thursday: endWeek.AddHours(577); break;
                    case DayOfWeek.Friday: endWeek.AddHours(553); break;
                    case DayOfWeek.Saturday: endWeek.AddHours(529); break;
                }

                return endWeek;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static bool isDayLightSaving(DateTime date)
        {
            try
            {
                if ((DateTime.Now >= DayLightSavingStart(DateTime.Now.Year.ToString())) && (DateTime.Now <= DayLightSavingEnd(DateTime.Now.Year.ToString())))
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}