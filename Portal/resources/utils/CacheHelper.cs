﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Portal.resources.utils
{
    public enum CachePriority
    {
        Low = 0,
        Medium = 1,
        High = 2
    }

    public class CacheHelper
    {
        #region Singleton

        private static volatile CacheHelper instance;
        private static object sync = new Object();

        private CacheHelper() { }

        public static CacheHelper Current
        {
            get
            {
                if (instance == null)
                {
                    lock (sync)
                    {
                        if (instance == null)
                            instance = new CacheHelper();
                    }
                }
                return instance;
            }
        }

        #endregion

        private List<string> keys = new List<string>();

        private bool IsEnabled
        {
            get
            {
                bool isEnabled = false;
                if (ConfigurationManager.AppSettings["CacheEnabled"] == null)
                    return isEnabled;
                else
                {
                    if (Boolean.TryParse(ConfigurationManager.AppSettings["CacheEnabled"].ToString(), out isEnabled))
                        return isEnabled;
                    return isEnabled;
                }
            }
        }

        private int CacheLowPriority
        {
            get
            {
                int cacheLowPriority = 60;
                if (ConfigurationManager.AppSettings["CacheLowPriority"] == null)
                    return cacheLowPriority;
                else
                {
                    if (Int32.TryParse(ConfigurationManager.AppSettings["CacheLowPriority"].ToString(), out cacheLowPriority))
                        return cacheLowPriority;
                    return cacheLowPriority;
                }
            }
        }

        private int CacheMediumPriority
        {
            get
            {
                int cacheMediumPriority = 300;
                if (ConfigurationManager.AppSettings["CacheMediumPriority"] == null)
                    return cacheMediumPriority;
                else
                {
                    if (Int32.TryParse(ConfigurationManager.AppSettings["CacheMediumPriority"].ToString(), out cacheMediumPriority))
                        return cacheMediumPriority;
                    return cacheMediumPriority;
                }
            }
        }

        private int CacheHighPriority
        {
            get
            {
                int cacheHighPriority = 1800;
                if (ConfigurationManager.AppSettings["CacheHighPriority"] == null)
                    return cacheHighPriority;
                else
                {
                    if (Int32.TryParse(ConfigurationManager.AppSettings["CacheHighPriority"].ToString(), out cacheHighPriority))
                        return cacheHighPriority;
                    return cacheHighPriority;
                }
            }
        }

        public void AddItem(string key, object value)
        {
            AddItem(key, value, CachePriority.Low);
        }

        public void AddItem(string key, object value, CachePriority cachePriority)
        {
            if (IsEnabled)
            {
                int cacheDuration = 0;
                switch (cachePriority)
                {
                    case CachePriority.Low: cacheDuration = CacheLowPriority; break;
                    case CachePriority.Medium: cacheDuration = CacheMediumPriority; break;
                    case CachePriority.High: cacheDuration = CacheHighPriority; break;
                    default: cacheDuration = 0; break;
                }

                if (HttpContext.Current.Cache[key] == null)
                {
                    if (!keys.Contains(key))
                        keys.Add(key);
                    HttpContext.Current.Cache.Insert(key, value, null, DateTime.Now.AddSeconds(cacheDuration), TimeSpan.Zero);
                }
            }
        }

        public object GetItem(string key)
        {
            if (!IsEnabled)
                return null;
            else
                return HttpContext.Current.Cache[key];
        }

        public void RemoveItem(string key)
        {
            if (keys.Contains(key))
                keys.Remove(key);
            HttpContext.Current.Cache.Remove(key);
        }


        public void Clear()
        {
            foreach (string key in keys)
            {
                HttpContext.Current.Cache.Remove(key);
            }
        }

        public void Clear(string providerName)
        {
            foreach (string key in keys)
                if (key.StartsWith(key))
                    HttpContext.Current.Cache.Remove(key);
        }


    }
}