﻿using Portal.resources.response;
using Portal.resources.utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;
using Wifi360.Data.Controllers;
using Wifi360.Data.Model;

namespace Portal.resources.handlers
{
    /// <summary>
    /// Descripción breve de NomadixHandler
    /// </summary>
    public class NomadixHandler : IHttpHandler
    {


        public void ProcessRequest(HttpContext context)
        {
            if (string.IsNullOrEmpty(context.Request.Params["action"])) ProcessEmtpy(context);

            switch (context.Request.Params["action"].ToUpper())
            {
                case "ALLOW": allowfree(context); break;
                case "BILLINGTYPES": billingTypes(context); break;
                case "FREEACCESS": freeaccess(context); break;
                case "FREEACCESSNOSURVEY": freeaccessnosurvey(context); break;
                case "FULLSITE": fullsite(context); break;
                case "GETFACEBOOKPROMOTIONS": getFacebookPromotions(context); break;
                case "GETFACEBOOKPROMOTION": getFacebookPromotion(context); break;
                case "GETFREEINFORMATION": getfreeInformation(context); break;
                case "GETSITE": getsite(context); break;
                case "GETSURVEY": getsurvey(context); break;
                case "GETSOCIALNETWORKINFORMATION": getSocialNetworkInformation(context); break;
                case "LOGIN": login(context); break;
                case "PAYPALREQUEST": paypalrequest(context); break;
                case "PAYPALREQUESTNOSURVEY": paypalrequestnosurvey(context); break;
                case "SAVESURVEY": saveSurvey(context); break;
                case "SITELOCATIONMODULE": sitelocationmodule(context); break;
                case "FACEBOOKACCESS": facebookAccess(context); break;
                case "USERNAME": username(context); break;



                default: ProcessEmtpy(context); break;
            }
        }

        private UserResponse allowfree(HttpContext context)
        {
            CaptivePortalLog log = new CaptivePortalLog();
            UserResponse response = new UserResponse();
            try
            {
                string idhotel = (context.Request["idhotel"] == null ? string.Empty : context.Request["idhotel"].ToString());
                string mac = (context.Request["mac"] == null ? string.Empty : context.Request["mac"].ToString());
                string idlocation = (context.Request["idlocation"] == null ? string.Empty : context.Request["idlocation"].ToString());
                string mailing = (context.Request["mailing"] == null ? string.Empty : context.Request["mailing"].ToString());
                string lang = (context.Request["lang"] == null ? string.Empty : context.Request["lang"].ToString());

                string sIP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
                string uri = "http://" + sIP + ":1111/usg/command.xml";

                //string uri = "http://192.168.10.16:1111/usg/command.xml";

                Hotels hotel = BillingController.GetHotel(Int32.Parse(idhotel));
                Locations2 location = BillingController.ObtainLocation(Int32.Parse(idlocation));

                log.IdHotel = hotel.IdHotel;
                log.CallerID = mac;
                log.Date = DateTime.Now.ToUniversalTime();
                log.UserName = "";
                log.Password = "";
                log.Page = "freeaccess reconnect";
                log.IdLocation = Int32.Parse(idlocation);
                log.IdHotel = hotel.IdHotel;
                log.NSEId = "Nomadix - " + hotel.IdHotel;
                log.RadiusResponse = string.Empty;
                log.NomadixResponse = string.Empty;

                Users lastUser = BillingController.GetRadiusUsermac(mac, hotel.IdHotel, 1);
                if (!lastUser.IdUser.Equals(0))
                {
                    BillingTypes bt = BillingController.ObtainBillingType(lastUser.IdBillingType);
                    log.IdLocation = bt.IdLocation;
                    if (bt.IdBillingModule.Equals(1))
                    {
                        if ((lastUser.Enabled))
                        {
                            XmlDocument xml = CrearXML(lastUser.Name.ToUpper(), lastUser.Password.ToUpper(), mac);
                            response.allow = 1; //LO PASA DIRECTAMENTE AL LOGIN
                            response.UserName = lastUser.Name;
                            response.Password = lastUser.Password;
                            response.ValidTill = lastUser.ValidTill.AddHours(hotel.GMT).ToString("dd/MM/yyyy HH:mm:ss");
                            if (string.IsNullOrEmpty(bt.UrlLanding))
                            {
                                if (string.IsNullOrEmpty(location.UrlLanding))
                                    response.Url = hotel.UrlHotel;
                                else
                                    response.Url = location.UrlLanding;
                            }
                            else
                                response.Url = bt.UrlLanding;

                            DateTime timeXMLResponse = DateTime.Now.ToUniversalTime();
                            XmlDocument xmlResponse = PostXMLTransaction(uri, xml);

                            Thread.Sleep(1000);

                            foreach (XmlAttribute att in xmlResponse.DocumentElement.Attributes)
                            {
                                if (att.Name.Equals("RESULT"))
                                {
                                    if (att.Value.Equals("OK")) //EL PAQUETE ENVIADO AL NOMADIX TIENE EL FORMATO CORRECTO DE USER, PASS Y MAC
                                    {
                                        log.NomadixResponse = "OK";

                                        DateTime time = DateTime.Now.ToUniversalTime();
                                        DateTime aux = time.AddSeconds(30);
                                        do
                                        {
                                            //COMPROBAMOS QUE HAYA UNA SESION ACTIVA EN LOS ULTIMOS 30 SEGUNDOS CON LA MAC , SI ES CORRECTA, DEVOLVEMOS UN OK
                                            ActiveSessions session = BillingController.GetActiveSession(mac, lastUser.Name.ToUpper());
                                            if (!string.IsNullOrEmpty(session.AcctSessionID))
                                            {
                                                log.UserName = lastUser.Name.ToUpper();
                                                log.Password = lastUser.Password.ToUpper();
                                                log.RadiusResponse = "OK";

                                                response.allow = 1; //PASA DIRECTAMENTE
                                                response.IdUser = 1;
                                                response.UserName = lastUser.Name;
                                                response.Password = lastUser.Password;
                                                response.ValidTill = lastUser.ValidTill.AddHours(hotel.GMT).ToString("dd/MM/yyyy HH:mm:ss");

                                                if (string.IsNullOrEmpty(bt.UrlLanding))
                                                {
                                                    if (string.IsNullOrEmpty(location.UrlLanding))
                                                        response.Url = hotel.UrlHotel;
                                                    else
                                                        response.Url = location.UrlLanding;
                                                }
                                                else
                                                    response.Url = bt.UrlLanding;
                                                break;
                                            }
                                            else // SI NO HAY SESSION ACTIVA ES PORQUE HA FALLADO EL LOGIN, VAMOS A LA TABLA DE FAILED REQUEST Y MOSTRAMOS EL ERROR INDICADO POR EL RADIUS
                                            {
                                                FailedRequests failed = BillingController.GetFailedRequest(mac, lastUser.Name.ToUpper(), timeXMLResponse);
                                                if (!failed.ID.Equals(0))
                                                {
                                                    //json = "{\"code\":\"ERROR\",\"message\":\"" + failed.ReplyMessage + "\"}";
                                                    log.RadiusResponse = failed.ReplyMessage;
                                                    response.IdUser = -1;
                                                    response.UserName = failed.ReplyMessage;
                                                    response.allow = 2;
                                                    break;
                                                }
                                                else
                                                { // SI TAMPOCO HAY ERROR, ES PORQUE EL RADIUS HA RECHAZADO EL PAQUETE Y MOSTRAMOS UN ERROR DESCONOCIDO.

                                                    //json = "{\"code\":\"ERROR\",\"message\":\"Unknown Error\"}";
                                                    log.RadiusResponse += "ERROR - USER IS NOT IN RADIUS TABLES - TRAZA " + time.ToString() + Environment.NewLine;
                                                }
                                            }
                                            time = time.AddSeconds(1);
                                            Thread.Sleep(1000);
                                        } while (time != aux);

                                        //VALIDAMOS EL USERAGENT
                                        UserAgentsTemp temp = BillingController.GetUserAgentTemp(mac);
                                        if (temp.IdTemp != 0)
                                        {
                                            UserAgents userAgent = BillingController.GetUserAgent(temp.IdUserAgent);
                                            List<UserAgents> listUA = BillingController.GetUserAgents(userAgent.String);

                                            userAgent.Valid = userAgent.Valid + 1; ;
                                            BillingController.SaveUserAgent(userAgent);
                                            foreach (UserAgents x in listUA)
                                            {
                                                x.Valid = x.Valid + 1; ;
                                                BillingController.SaveUserAgent(x);
                                            }

                                            //BillingController.DeleteUserAgentTemp(temp);
                                        }

                                        //METEMOS LOS USERAGENTS EN LA LISTA NEGRA

                                        List<UserAgents> uAtoBlackList = BillingController.GetUserAgentsToBlackList(Int32.Parse(ConfigurationManager.AppSettings["MaxAttemps"].ToString()));
                                        foreach (UserAgents uA in uAtoBlackList)
                                        {
                                            if (uA.Valid.Equals(0))
                                            {
                                                BlackListUserAgents bUA = BillingController.GetBlackUserAgent(uA.String);
                                                if (bUA.IdBlackUserAgent.Equals(0))
                                                {
                                                    BlackListUserAgents obj = new BlackListUserAgents();
                                                    obj.String = uA.String;
                                                    BillingController.SaveBlackListUserAgent(obj);
                                                }
                                            }
                                        }
                                    }
                                    else if (att.Value.Equals("ERROR"))
                                    {
                                        if (xmlResponse.InnerText.Contains("209"))
                                        {
                                            //json = "{\"code\":\"OK\",\"message\":\"" + hotel.UrlHotel + "\"}";
                                            log.NomadixResponse = "OK - ALREADY CONNECTED, REDIRECTING " + xmlResponse.InnerText;
                                            log.RadiusResponse = "NOT APPLICABLE";
                                        }
                                        else
                                        {
                                            //json = "{\"code\":\"ERROR\",\"message\":\"" + xmlResponse.InnerText + "\"}";
                                            log.NomadixResponse = "ERROR" + xmlResponse.InnerText;
                                            log.RadiusResponse = "NOT APPLICABLE";
                                        }
                                    }
                                }
                            }

                            BillingController.SaveCaptivePortalLog(log);

                        }
                        else if (lastUser.ValidSince.AddHours(hotel.FreeAccessRepeatTime) > DateTime.Now.ToUniversalTime())
                        {
                            response.allow = 2; //SI RESPONSE ALLOW = 2, NO PUEDE PASAR

                            log.Page = "No more free access allowed";
                            log.RadiusResponse = "No more free access allowed";
                            log.NomadixResponse = "NOT APPLICABLE";
                            log.UserName = lastUser.Name;
                            log.Password = lastUser.Password;
                            log.NSEId = "Nomadix " + hotel.IdHotel;
                            log.CallerID = mac;
                            log.Date = DateTime.Now.ToUniversalTime();
                            response.UserName = "No more free access allowed";

                            BillingController.SaveCaptivePortalLog(log);
                        }
                        else
                        {
                            response.allow = 0; //SI RESPONSE ALLOW = 0, HAY USUARIO ANTERIOR PERO ESTA FUERA DEL PLAZO O NO ES GRATUITO
                        }
                    }
                    else
                    {
                        response.allow = 0; //SI RESPONSE ALLOW = 0, HAY USUARIO ANTERIOR PERO ESTA FUERA DEL PLAZO O NO ES GRATUITO
                    }
                }
                else
                {
                    response.allow = 0; //ALLOW = 0, NO HAY UN USUARIO ANTERIOR
                }
            }
            catch
            {

            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private ListBillingTypesResponse billingTypes(HttpContext context)
        {
            string idhotel = (context.Request["idhotel"] == null ? string.Empty : context.Request["idhotel"].ToString());
            string idlocation = (context.Request["idlocation"] == null ? string.Empty : context.Request["idlocation"].ToString());

            ListBillingTypesResponse response = new ListBillingTypesResponse();

            try
            {
                Hotels hotel = BillingController.GetHotel(Int32.Parse(idhotel));
                Locations2 location = BillingController.ObtainLocation(Int32.Parse(idlocation));

                if (location != null)
                {
                    response.hotel = location.IdHotel;
                    List<BillingTypes> list = BillingController.ObtainBillingTypes(location.IdLocation, 2);

                    if (list.Count.Equals(0))
                        list = BillingController.ObtainBillingTypes(BillingController.ObtainLocation(hotel.IdHotel, "All_Zones").IdLocation, 2);

                    response.list = new List<BillingTypeResponse>();

                    foreach (BillingTypes b in list.OrderBy(a => a.Order))
                    {
                        if ((!b.FreeAccess) && (b.Enabled))
                        {
                            BillingTypeResponse br = new BillingTypeResponse();
                            br.IdBillingType = b.IdBillingType;
                            br.Description = b.Description;
                            br.Price = b.Price;
                            br.ValidTill = b.ValidTill;
                            br.BWDown = b.BWDown;
                            br.BWUp = b.BWUp;
                            br.VolumeDown = b.VolumeDown;
                            br.VolumeUp = b.VolumeUp;
                            br.MaxDevices = b.MaxDevices;
                            TimeSpan t = TimeSpan.FromSeconds(b.TimeCredit / b.MaxDevices);
                            if (b.TimeCredit >= 86400)
                                br.TimeCredit += string.Format("{0}d ", t.Days);
                            if (t.Hours > 0)
                                br.TimeCredit += string.Format("{0:D2}h ", t.Hours);
                            if (t.Minutes > 0)
                                br.TimeCredit += string.Format("{0:D2}m ", t.Minutes);
                            if (t.Seconds > 0)
                                br.TimeCredit += string.Format("{0:D2}s ", t.Seconds);

                            if (hotel.CurrencyCode.Equals("EUR"))
                                br.Currency = string.Format("{0} €", b.Price);
                            else if (hotel.CurrencyCode.Equals("GBP"))
                                br.Currency = string.Format("£ {0}", b.Price);

                            if (b.VolumeUp.Equals(0))
                                br.LimitUp = "--";
                            else
                                br.LimitUp = string.Format("{0} Mb", b.VolumeUp);

                            if (b.VolumeDown.Equals(0))
                                br.LimitDown = "--";
                            else
                                br.LimitDown = string.Format("{0} Mb", b.VolumeDown);

                            response.list.Add(br);
                        }
                    }
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private UserResponse freeaccess(HttpContext context)
        {
            CaptivePortalLog log = new CaptivePortalLog();
            UserResponse response = new UserResponse();
            try
            {
                string name = (context.Request["name"] == null ? string.Empty : context.Request["name"].ToString());
                string surname = (context.Request["surname"] == null ? string.Empty : context.Request["surname"].ToString());
                string mail = (context.Request["email"] == null ? string.Empty : context.Request["email"].ToString());
                string username = (context.Request["username"] == null ? string.Empty : context.Request["username"].ToString());
                string password = (context.Request["password"] == null ? string.Empty : context.Request["password"].ToString());
                string survey = (context.Request["survey"] == null ? string.Empty : context.Request["survey"].ToString());

                string idhotel = (context.Request["idhotel"] == null ? string.Empty : context.Request["idhotel"].ToString());
                string mac = (context.Request["mac"] == null ? string.Empty : context.Request["mac"].ToString());
                string idlocation = (context.Request["idlocation"] == null ? string.Empty : context.Request["idlocation"].ToString());
                string mailing = (context.Request["mailing"] == null ? string.Empty : context.Request["mailing"].ToString());
                string lang = (context.Request["lang"] == null ? string.Empty : context.Request["lang"].ToString());

                string sIP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
                string uri = "http://" + sIP + ":1111/usg/command.xml";

                //string uri = "http://192.168.10.16:1111/usg/command.xml";

                Hotels hotel = BillingController.GetHotel(Int32.Parse(idhotel));
                Locations2 location = BillingController.ObtainLocation(Int32.Parse(idlocation));

                log.NSEId = string.Format("NOMADIX - Idhotel:{0}", idhotel);
                log.CallerID = mac;
                log.Date = DateTime.Now.ToUniversalTime();
                log.UserName = "";
                log.Password = "";
                log.Page = "freeaccess";
                log.IdLocation = Int32.Parse(idlocation);
                log.IdHotel = hotel.IdHotel;
                log.RadiusResponse = string.Empty;
                log.NomadixResponse = string.Empty;

                List<BillingTypes> free = BillingController.ObtainBillingTypes(Int32.Parse(idlocation), 1);
                BillingTypes billingType = null;
                if (free.Count > 0)
                {
                    billingType = free[0];
                }
                else
                {
                    free = BillingController.ObtainBillingTypes(BillingController.ObtainLocation(hotel.IdHotel, "All_Zones").IdLocation, 1);
                    if (free.Count > 0)
                        billingType = free[0];
                }

                if (util.ValidateEmail(mail))
                {
                    Rooms room = BillingController.GetRoom(Int32.Parse(idhotel), "DEFAULT");
                    List<ParametrosConfiguracion> parametros = BillingController.GetParametrosConfiguracion(hotel.IdHotel);
                    int longUserName = 3;
                    int longPassword = 3;
                    string prefixUserName = string.Empty;

                    foreach (ParametrosConfiguracion obj in parametros)
                    {
                        switch (obj.Key.ToUpper())
                        {
                            case "LONGUSERNAME": longUserName = Int32.Parse(obj.value); break;
                            case "LONGPASSWORD": longPassword = Int32.Parse(obj.value); break;
                            case "USERNAMEPREFIX": prefixUserName = obj.value; break;
                        }
                    }

                    if (longPassword < 3)
                        longPassword = 3;
                    if (longUserName < 3)
                        longUserName = 3;

                    Users existuser = null;
                    username = string.Empty;
                    do
                    {
                        username = string.Format("{0}{1}", prefixUserName, util.createRandomUserNumber(longUserName)).ToUpper();
                        existuser = BillingController.GetRadiusUser(username.ToUpper(), hotel.IdHotel);

                    } while (existuser.Enabled);

                    password = util.CreateRandomPassword(longPassword).ToUpper();

                    log.UserName = username.ToUpper();
                    log.Password = password.ToUpper();


                    Users user = new Users();
                    //user.IdHotel = location.IdHotel;
                    user.IdHotel = Int32.Parse(idhotel);
                    user.IdRoom = room.IdRoom;
                    user.IdBillingType = billingType.IdBillingType;
                    user.BWDown = billingType.BWDown;
                    user.BWUp = billingType.BWUp;
                    user.ValidSince = DateTime.Now.ToUniversalTime(); // bill.BillingDate;
                    user.ValidTill = DateTime.Now.ToUniversalTime().AddHours(billingType.ValidTill); // bill.BillingDate.AddHours(billingType.ValidTill);
                    user.TimeCredit = billingType.TimeCredit;
                    user.MaxDevices = billingType.MaxDevices;
                    user.Priority = billingType.IdPriority;
                    user.VolumeDown = billingType.VolumeDown;
                    user.VolumeUp = billingType.VolumeUp;
                    user.Enabled = true;
                    user.Name = username.ToUpper();
                    user.Password = password.ToUpper();
                    user.CHKO = true;
                    user.BuyingFrom = 1;
                    user.BuyingCallerID = mac;

                    Loyalties2 loyalty = new Loyalties2();
                    loyalty.IdHotel = user.IdHotel;
                    loyalty.Name = name.ToUpper();
                    loyalty.Surname = surname.ToUpper();
                    loyalty.Email = mail;
                    loyalty.Date = DateTime.Now.ToUniversalTime();
                    loyalty.Survey = survey;

                    BillingController.SaveLoyalty(loyalty);

                    BillingController.SaveUser(user);

                    XmlDocument xml = CrearXML(username.ToUpper(), password.ToUpper(), mac);

                    DateTime timeXMLResponse = DateTime.Now.ToUniversalTime();
                    Thread.Sleep(1000);
                    XmlDocument xmlResponse = PostXMLTransaction(uri, xml);

                    foreach (XmlAttribute att in xmlResponse.DocumentElement.Attributes)
                    {
                        if (att.Name.Equals("RESULT"))
                        {
                            if (att.Value.Equals("OK"))
                            {
                                log.NomadixResponse = "OK";

                                DateTime time = DateTime.Now.ToUniversalTime();
                                DateTime aux = time.AddSeconds(30);

                                do
                                {
                                    //COMPROBAMOS QUE HAYA UNA SESION ACTIVA CON LA MAC , SI ES CORRECTA, DEVOLVEMOS UN OK
                                    ActiveSessions session = BillingController.GetActiveSession(mac, username.ToUpper());
                                    if (!string.IsNullOrEmpty(session.AcctSessionID))
                                    {
                                        response.IdUser = 1;
                                        response.UserName = user.Name;
                                        response.Password = user.Password;
                                        response.ValidTill = user.ValidTill.AddHours(hotel.GMT).ToString("dd/MM/yyyy HH:mm:ss");
                                        if (string.IsNullOrEmpty(billingType.UrlLanding))
                                        {
                                            if (string.IsNullOrEmpty(location.UrlLanding))
                                                response.Url = hotel.UrlHotel;
                                            else
                                                response.Url = location.UrlLanding;
                                        }
                                        else
                                            response.Url = billingType.UrlLanding;

                                        log.RadiusResponse = "OK";

                                        util.sendMailRoot(user.IdHotel, billingType.IdBillingType, mail, user.Name, user.Password, lang.Substring(0, 2));

                                        break;
                                    }
                                    else // SI NO HAY SESSION ACTIVA ES PORQUE HA FALLADO EL LOGIN, VAMOS A LA TABLA DE FAILED REQUEST Y MOSTRAMOS EL ERROR INDICADO POR EL RADIUS
                                    {
                                        FailedRequests failed = BillingController.GetFailedRequest(mac, username.ToUpper(), timeXMLResponse);
                                        if (!failed.ID.Equals(0))
                                        {
                                            response.IdUser = 0;
                                            log.RadiusResponse = failed.ReplyMessage;
                                            response.UserName = failed.ReplyMessage;
                                            break;
                                        }
                                        else
                                        { // SI TAMPOCO HAY ERROR, ES PORQUE EL RADIUS HA RECHAZADO EL PAQUETE Y MOSTRAMOS UN ERROR DESCONOCIDO.
                                            response.IdUser = -3;
                                            log.RadiusResponse += "ERROR - USER IS NOT IN RADIUS TABLES - TRAZA " + time.ToString() + Environment.NewLine;
                                        }
                                    }
                                    Thread.Sleep(1000);
                                    time = time.AddSeconds(1);
                                } while (time < aux);

                                //VALIDAMOS EL USERAGENT
                                UserAgentsTemp temp = BillingController.GetUserAgentTemp(mac);
                                if (temp.IdTemp != 0)
                                {
                                    UserAgents userAgent = BillingController.GetUserAgent(temp.IdUserAgent);
                                    List<UserAgents> listUA = BillingController.GetUserAgents(userAgent.String);

                                    userAgent.Valid = userAgent.Valid + 1; ;
                                    BillingController.SaveUserAgent(userAgent);
                                    foreach (UserAgents x in listUA)
                                    {
                                        x.Valid = x.Valid + 1; ;
                                        BillingController.SaveUserAgent(x);
                                    }

                                    //BillingController.DeleteUserAgentTemp(temp);
                                }

                                //METEMOS LOS USERAGENTS EN LA LISTA NEGRA

                                List<UserAgents> uAtoBlackList = BillingController.GetUserAgentsToBlackList(Int32.Parse(ConfigurationManager.AppSettings["MaxAttemps"].ToString()));
                                foreach (UserAgents uA in uAtoBlackList)
                                {
                                    if (uA.Valid.Equals(0))
                                    {
                                        BlackListUserAgents bUA = BillingController.GetBlackUserAgent(uA.String);
                                        if (bUA.IdBlackUserAgent.Equals(0))
                                        {
                                            BlackListUserAgents obj = new BlackListUserAgents();
                                            obj.String = uA.String;
                                            BillingController.SaveBlackListUserAgent(obj);
                                        }
                                    }
                                }
                            }
                            else if (att.Value.Equals("ERROR"))
                            {
                                response.IdUser = 0;
                                log.NomadixResponse = "ERROR EN PAQUETE DE NOMADIX - " + xmlResponse.InnerText;
                                log.RadiusResponse = "NOT APPLICABLE";
                                response.UserName = xmlResponse.InnerText;
                            }

                        }

                    }
                }
                else
                {
                    response.IdUser = -2;
                    log.NomadixResponse = "Email's format is not correct";
                    log.RadiusResponse = "Email's format is not correct";
                    response.UserName = "Email's format is not correct";

                }
            }
            catch (Exception ex)
            {
                if (ex.GetType().Namespace.Contains("IO"))
                {
                    log.NomadixResponse = "OK";
                    log.RadiusResponse = "OK";

                    CaptivePortalLogInternal internalLog = new CaptivePortalLogInternal();
                    internalLog.NSEId = log.NSEId;
                    internalLog.CallerID = log.CallerID;
                    internalLog.Date = DateTime.Now.ToUniversalTime();
                    internalLog.UserName = "";
                    internalLog.Password = "";
                    internalLog.Page = "freeaccess";
                    internalLog.IdLocation = 0;

                    internalLog.NomadixResponse = "ERROR - CAN`T LOAD EMAIL TEMPLATE";
                    internalLog.RadiusResponse = "OK";

                    BillingController.SaveCaptivePortalLogInternal(internalLog);

                }
                else if (ex.GetType().Namespace.ToUpper().Contains("MAIL"))
                {
                    CaptivePortalLogInternal internalLog = new CaptivePortalLogInternal();
                    internalLog.NSEId = log.NSEId;
                    internalLog.CallerID = log.CallerID;
                    internalLog.Date = DateTime.Now.ToUniversalTime();
                    internalLog.UserName = "";
                    internalLog.Password = "";
                    internalLog.Page = "freeaccess";
                    internalLog.IdLocation = 0;
                    internalLog.NomadixResponse = "ERROR - CAN`T SEND EMAIL TO CLIENT";
                    internalLog.RadiusResponse = "OK";

                    BillingController.SaveCaptivePortalLogInternal(internalLog);

                    log.NomadixResponse = "OK ";
                    log.RadiusResponse = "OK";

                }
                else
                {
                    log.NomadixResponse = "OK";
                    log.RadiusResponse = "OK";

                    CaptivePortalLogInternal internalLog = new CaptivePortalLogInternal();
                    internalLog.NSEId = log.NSEId;
                    internalLog.CallerID = log.CallerID;
                    internalLog.Date = DateTime.Now.ToUniversalTime();
                    internalLog.UserName = "";
                    internalLog.Password = "";
                    internalLog.Page = "freeaccess";
                    internalLog.IdLocation = 0;
                    internalLog.NomadixResponse = "ERROR NO CONTEMPLADO - " + ex.Message;
                    internalLog.RadiusResponse = "OK";

                    BillingController.SaveCaptivePortalLogInternal(internalLog);
                }

            }

            BillingController.SaveCaptivePortalLog(log);

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private UserResponse freeaccessnosurvey(HttpContext context)
        {
            CaptivePortalLog log = new CaptivePortalLog();
            UserResponse response = new UserResponse();
            try
            {
                string name = (context.Request["name"] == null ? string.Empty : context.Request["name"].ToString());
                string surname = (context.Request["surname"] == null ? string.Empty : context.Request["surname"].ToString());

                string idhotel = (context.Request["idhotel"] == null ? string.Empty : context.Request["idhotel"].ToString());
                string mac = (context.Request["mac"] == null ? string.Empty : context.Request["mac"].ToString());
                string idlocation = (context.Request["idlocation"] == null ? string.Empty : context.Request["idlocation"].ToString());
                string mailing = (context.Request["mailing"] == null ? string.Empty : context.Request["mailing"].ToString());

                string sIP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
                string uri = "http://" + sIP + ":1111/usg/command.xml";

                //string uri = "http://192.168.10.16:1111/usg/command.xml";


                Hotels hotel = BillingController.GetHotel(Int32.Parse(idhotel));
                Locations2 location = BillingController.ObtainLocation(Int32.Parse(idlocation));

                log.NSEId = string.Format("Nomadix - Idhotel:{0}", idhotel);
                log.CallerID = mac;
                log.Date = DateTime.Now.ToUniversalTime();
                log.UserName = "";
                log.Password = "";
                log.Page = "freeaccess no survey";
                log.IdLocation = Int32.Parse(idlocation);
                log.RadiusResponse = string.Empty;
                log.NomadixResponse = string.Empty;

                log.IdHotel = hotel.IdHotel;
                List<BillingTypes> free = BillingController.ObtainBillingTypes(Int32.Parse(idlocation), 1);
                BillingTypes billingType = null;
                if (free.Count > 0)
                {
                    billingType = free[0];
                }
                else
                {
                    free = BillingController.ObtainBillingTypes(BillingController.ObtainLocation(hotel.IdHotel, "All_Zones").IdLocation, 1);
                    if (free.Count > 0)
                        billingType = free[0];
                }
                Rooms room = BillingController.GetRoom(Int32.Parse(idhotel), "DEFAULT");
                List<ParametrosConfiguracion> parametros = BillingController.GetParametrosConfiguracion(hotel.IdHotel);
                int longUserName = 3;
                int longPassword = 3;
                string prefixUserName = string.Empty;

                foreach (ParametrosConfiguracion obj in parametros)
                {
                    switch (obj.Key.ToUpper())
                    {
                        case "LONGUSERNAME": longUserName = Int32.Parse(obj.value); break;
                        case "LONGPASSWORD": longPassword = Int32.Parse(obj.value); break;
                        case "USERNAMEPREFIX": prefixUserName = obj.value; break;
                    }
                }

                if (longPassword < 3)
                    longPassword = 3;
                if (longUserName < 3)
                    longUserName = 3;

                Users existuser = null;
                string username = string.Empty;
                do
                {
                    username = string.Format("{0}{1}", prefixUserName, util.createRandomUserNumber(longUserName)).ToUpper();
                    existuser = BillingController.GetRadiusUser(username.ToUpper(), hotel.IdHotel);

                } while (existuser.Enabled);

                string password = util.CreateRandomPassword(longPassword).ToUpper();

                log.UserName = username.ToUpper();
                log.Password = password.ToUpper();

                Users user = new Users();
                user.IdHotel = Int32.Parse(idhotel);
                user.IdRoom = room.IdRoom;
                user.IdBillingType = billingType.IdBillingType;
                user.BWDown = billingType.BWDown;
                user.BWUp = billingType.BWUp;
                user.ValidSince = DateTime.Now.ToUniversalTime(); // bill.BillingDate;
                user.ValidTill = DateTime.Now.ToUniversalTime().AddHours(billingType.ValidTill); // bill.BillingDate.AddHours(billingType.ValidTill);
                user.TimeCredit = billingType.TimeCredit;
                user.MaxDevices = billingType.MaxDevices;
                user.Priority = billingType.IdPriority;
                user.VolumeDown = billingType.VolumeDown;
                user.VolumeUp = billingType.VolumeUp;
                user.Enabled = true;
                user.Name = username.ToUpper();
                user.Password = password.ToUpper();
                user.CHKO = true;
                user.BuyingFrom = 1;
                user.BuyingCallerID = mac;

                BillingController.SaveUser(user);

                XmlDocument xml = CrearXML(username.ToUpper(), password.ToUpper(), mac);

                DateTime timeXMLResponse = DateTime.Now.ToUniversalTime();
                Thread.Sleep(1000);
                XmlDocument xmlResponse = PostXMLTransaction(uri, xml);

                foreach (XmlAttribute att in xmlResponse.DocumentElement.Attributes)
                {
                    if (att.Name.Equals("RESULT"))
                    {
                        if (att.Value.Equals("OK"))
                        {
                            log.NomadixResponse = "OK";

                            DateTime time = DateTime.Now.ToUniversalTime();
                            DateTime aux = time.AddSeconds(30);

                            do
                            {
                                //COMPROBAMOS QUE HAYA UNA SESION ACTIVA CON LA MAC , SI ES CORRECTA, DEVOLVEMOS UN OK
                                ActiveSessions session = BillingController.GetActiveSession(mac, username.ToUpper());
                                if (!string.IsNullOrEmpty(session.AcctSessionID))
                                {
                                    response.IdUser = 1;
                                    response.UserName = user.Name;
                                    response.Password = user.Password;
                                    response.ValidTill = user.ValidTill.AddHours(hotel.GMT).ToString("dd/MM/yyyy HH:mm:ss");
                                    if (string.IsNullOrEmpty(billingType.UrlLanding))
                                    {
                                        if (string.IsNullOrEmpty(location.UrlLanding))
                                            response.Url = hotel.UrlHotel;
                                        else
                                            response.Url = location.UrlLanding;
                                    }
                                    else
                                        response.Url = billingType.UrlLanding;

                                    log.RadiusResponse = "OK";

                                    break;
                                }
                                else // SI NO HAY SESSION ACTIVA ES PORQUE HA FALLADO EL LOGIN, VAMOS A LA TABLA DE FAILED REQUEST Y MOSTRAMOS EL ERROR INDICADO POR EL RADIUS
                                {
                                    FailedRequests failed = BillingController.GetFailedRequest(mac, username.ToUpper(), timeXMLResponse);
                                    if (!failed.ID.Equals(0))
                                    {
                                        response.IdUser = 0;
                                        log.RadiusResponse = failed.ReplyMessage;
                                        response.UserName = failed.ReplyMessage;
                                        break;
                                    }
                                    else
                                    { // SI TAMPOCO HAY ERROR, ES PORQUE EL RADIUS HA RECHAZADO EL PAQUETE Y MOSTRAMOS UN ERROR DESCONOCIDO.
                                        response.IdUser = -3;
                                        log.RadiusResponse += "ERROR - USER IS NOT IN RADIUS TABLES - TRAZA " + time.ToString() + Environment.NewLine;
                                    }
                                }
                                Thread.Sleep(1000);
                                time = time.AddSeconds(1);
                            } while (time < aux);

                            //VALIDAMOS EL USERAGENT
                            UserAgentsTemp temp = BillingController.GetUserAgentTemp(mac);
                            if (temp.IdTemp != 0)
                            {
                                UserAgents userAgent = BillingController.GetUserAgent(temp.IdUserAgent);
                                List<UserAgents> listUA = BillingController.GetUserAgents(userAgent.String);

                                userAgent.Valid = userAgent.Valid + 1; ;
                                BillingController.SaveUserAgent(userAgent);
                                foreach (UserAgents x in listUA)
                                {
                                    x.Valid = x.Valid + 1; ;
                                    BillingController.SaveUserAgent(x);
                                }

                                //BillingController.DeleteUserAgentTemp(temp);
                            }

                            //METEMOS LOS USERAGENTS EN LA LISTA NEGRA

                            List<UserAgents> uAtoBlackList = BillingController.GetUserAgentsToBlackList(Int32.Parse(ConfigurationManager.AppSettings["MaxAttemps"].ToString()));
                            foreach (UserAgents uA in uAtoBlackList)
                            {
                                if (uA.Valid.Equals(0))
                                {
                                    BlackListUserAgents bUA = BillingController.GetBlackUserAgent(uA.String);
                                    if (bUA.IdBlackUserAgent.Equals(0))
                                    {
                                        BlackListUserAgents obj = new BlackListUserAgents();
                                        obj.String = uA.String;
                                        BillingController.SaveBlackListUserAgent(obj);
                                    }
                                }
                            }
                        }
                        else if (att.Value.Equals("ERROR"))
                        {
                            response.IdUser = 0;
                            log.NomadixResponse = "ERROR EN PAQUETE DE NOMADIX - " + xmlResponse.InnerText;
                            log.RadiusResponse = "NOT APPLICABLE";
                        }

                    }

                }
                //}
                //else
                //{
                //    response.IdUser = -1;
                //    log.NomadixResponse = "Only one free access in 24 hours";
                //    log.RadiusResponse = "Only one free access in 24 hours";
                //}
            }
            catch (Exception ex)
            {
                log.NomadixResponse = "OK";
                log.RadiusResponse = "OK";

                CaptivePortalLogInternal internalLog = new CaptivePortalLogInternal();
                internalLog.NSEId = log.NSEId;
                internalLog.CallerID = log.CallerID;
                internalLog.Date = DateTime.Now.ToUniversalTime();
                internalLog.UserName = "";
                internalLog.Password = "";
                internalLog.Page = "freeaccess";
                internalLog.IdLocation = 0;
                internalLog.NomadixResponse = "ERROR NO CONTEMPLADO - " + ex.Message;
                internalLog.RadiusResponse = "OK";

                BillingController.SaveCaptivePortalLogInternal(internalLog);

            }

            BillingController.SaveCaptivePortalLog(log);

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private FullSiteResponse fullsite(HttpContext context)
        {
            FullSiteResponse response = new FullSiteResponse();
            try
            {

                string idhotel = (context.Request["idhotel"] == null ? string.Empty : context.Request["idhotel"].ToString());
                string idlocation = (context.Request["idlocation"] == null ? string.Empty : context.Request["idlocation"].ToString());
                string lang = (context.Request["lang"] == null ? string.Empty : context.Request["lang"].ToString());

                Hotels hotel = BillingController.GetHotel(Int32.Parse(idhotel));
                response.IdHotel = hotel.IdHotel;

                Languages language = BillingController.GetLanguage(lang);
                if (language.IdLanguage.Equals(0))
                    language.IdLanguage = hotel.IdLanguage;

                response.IdLanguageDefault = hotel.IdLanguage;

                Disclaimers dis = BillingController.GetDisclaimer(response.IdHotel, Int32.Parse(idlocation), language.IdLanguage);
                response.Disclaimer = dis.Text;

                response.MandatorySurvey = hotel.Survey;

                response.LoginModule = hotel.LoginModule;

                if (hotel.FreeAccessModule.Equals(false))
                    response.FreeAccessModule = false;
                else
                    response.FreeAccessModule = true;

                if (hotel.PayAccessModule.Equals(false))
                    response.PayAccessModule = false;
                else
                    response.PayAccessModule = true;

                if (hotel.SocialNetworksModule.Equals(false))
                    response.SocialNetworksModule = false;
                else
                    response.SocialNetworksModule = true;

                if (hotel.CustomAccessModule.Equals(false))
                    response.CustomAccessModule = false;
                else
                    response.CustomAccessModule = true;

                Locations2 location = location = BillingController.ObtainLocation(Int32.Parse(idlocation));

                response.DefaultModule = location.DefaultModule;
                response.DisclaimerMandatory = location.Disclaimer;


                List<LocationModuleText> locationTexts = BillingController.GetLocationModuleTexts(location.IdLocation, language.IdLanguage);
                foreach (LocationModuleText obj in locationTexts)
                {
                    switch (obj.IdBillingModule)
                    {
                        case 1: response.FreeText = obj.Text; break;
                        case 2: response.PremiumText = obj.Text; break;
                        case 3: response.SocialText = obj.Text; break;
                        case 5: response.LoginText = obj.Text; break;
                        case 4: break;

                    }
                }

                Locations2 locationAllZones = BillingController.ObtainLocation(hotel.IdHotel, "All_Zones");
                if (hotel.FreeAccessModule)
                {
                    List<BillingTypes> list = BillingController.ObtainBillingTypes(location.IdLocation, 1);
                    if (list.Count.Equals(0))
                    {
                        List<BillingTypes> listAllZones = BillingController.ObtainBillingTypes(locationAllZones.IdLocation, 1);
                        if (listAllZones.Count.Equals(0))
                        {
                            response.FreeAccessModule = false;
                        }
                        else
                        {
                            BillingTypeResponse btfree = new BillingTypeResponse();
                            btfree.IdBillingType = listAllZones[0].IdBillingType;
                            btfree.Description = listAllZones[0].Description;
                            btfree.Price = listAllZones[0].Price;
                            btfree.ValidTill = listAllZones[0].ValidTill;
                            btfree.BWDown = listAllZones[0].BWDown;
                            btfree.BWUp = listAllZones[0].BWUp;
                            btfree.VolumeDown = listAllZones[0].VolumeDown;
                            btfree.VolumeUp = listAllZones[0].VolumeUp;
                            btfree.MaxDevices = listAllZones[0].MaxDevices;
                            TimeSpan t = TimeSpan.FromSeconds(list[0].TimeCredit / list[0].MaxDevices);
                            if (listAllZones[0].TimeCredit >= 86400)
                                btfree.TimeCredit += string.Format("{0}d ", t.Days);
                            if (t.Hours > 0)
                                btfree.TimeCredit += string.Format("{0:D2}h ", t.Hours);
                            if (t.Minutes > 0)
                                btfree.TimeCredit += string.Format("{0:D2}m ", t.Minutes);
                            if (t.Seconds > 0)
                                btfree.TimeCredit += string.Format("{0:D2}s ", t.Seconds);

                            if (listAllZones[0].VolumeUp.Equals(0))
                                btfree.LimitUp = "--";
                            else
                                btfree.LimitUp = string.Format("{0} Mb", (listAllZones[0].VolumeUp / (1024 * 1024)));

                            if (listAllZones[0].VolumeDown.Equals(0))
                                btfree.LimitDown = "--";
                            else
                                btfree.LimitDown = string.Format("{0} Mb", (listAllZones[0].VolumeDown / (1024 * 1024)));

                            response.freeAccess = btfree;
                        }
                    }
                    else
                    {
                        BillingTypeResponse btfree = new BillingTypeResponse();
                        btfree.IdBillingType = list[0].IdBillingType;
                        btfree.Description = list[0].Description;
                        btfree.Price = list[0].Price;
                        btfree.ValidTill = list[0].ValidTill;
                        btfree.BWDown = list[0].BWDown;
                        btfree.BWUp = list[0].BWUp;
                        btfree.VolumeDown = list[0].VolumeDown;
                        btfree.VolumeUp = list[0].VolumeUp;
                        btfree.MaxDevices = list[0].MaxDevices;
                        TimeSpan t = TimeSpan.FromSeconds(list[0].TimeCredit / list[0].MaxDevices);
                        if (list[0].TimeCredit >= 86400)
                            btfree.TimeCredit += string.Format("{0}d ", t.Days);
                        if (t.Hours > 0)
                            btfree.TimeCredit += string.Format("{0:D2}h ", t.Hours);
                        if (t.Minutes > 0)
                            btfree.TimeCredit += string.Format("{0:D2}m ", t.Minutes);
                        if (t.Seconds > 0)
                            btfree.TimeCredit += string.Format("{0:D2}s ", t.Seconds);

                        if (list[0].VolumeUp.Equals(0))
                            btfree.LimitUp = "--";
                        else
                            btfree.LimitUp = string.Format("{0} Mb", (list[0].VolumeUp / (1024 * 1024)));

                        if (list[0].VolumeDown.Equals(0))
                            btfree.LimitDown = "--";
                        else
                            btfree.LimitDown = string.Format("{0} Mb", (list[0].VolumeDown / (1024 * 1024)));

                        response.freeAccess = btfree;
                    }
                }

                //CARGAMOS LOS TIPOS DE ACCESO DE PAGO
                if (hotel.PayAccessModule)
                {
                    List<BillingTypes> list = BillingController.ObtainBillingTypes(location.IdLocation, 2);
                    if (list.Count.Equals(0))
                    {
                        list = BillingController.ObtainBillingTypes(locationAllZones.IdLocation, 2);
                        if (list.Count.Equals(0))
                            response.PayAccessModule = false;
                        else
                        {
                            response.listPremium = new List<BillingTypeResponse>();

                            foreach (BillingTypes b in list.OrderBy(a => a.Order))
                            {
                                if ((!b.FreeAccess) && (b.Enabled))
                                {
                                    BillingTypeResponse br = new BillingTypeResponse();
                                    br.IdBillingType = b.IdBillingType;
                                    br.Description = b.Description;
                                    br.Price = b.Price;
                                    br.ValidTill = b.ValidTill;
                                    br.BWDown = b.BWDown;
                                    br.BWUp = b.BWUp;
                                    br.VolumeDown = b.VolumeDown;
                                    br.VolumeUp = b.VolumeUp;
                                    br.MaxDevices = b.MaxDevices;
                                    TimeSpan t = TimeSpan.FromSeconds(b.TimeCredit / b.MaxDevices);
                                    if (b.TimeCredit >= 86400)
                                        br.TimeCredit += string.Format("{0}d ", t.Days);
                                    if (t.Hours > 0)
                                        br.TimeCredit += string.Format("{0:D2}h ", t.Hours);
                                    if (t.Minutes > 0)
                                        br.TimeCredit += string.Format("{0:D2}m ", t.Minutes);
                                    if (t.Seconds > 0)
                                        br.TimeCredit += string.Format("{0:D2}s ", t.Seconds);

                                    if (hotel.CurrencyCode.Equals("EUR"))
                                        br.Currency = string.Format("{0} €", b.Price);
                                    else if (hotel.CurrencyCode.Equals("GBP"))
                                        br.Currency = string.Format("£ {0}", b.Price);

                                    if (b.VolumeUp.Equals(0))
                                        br.LimitUp = "--";
                                    else
                                        br.LimitUp = string.Format("{0} Mb", (b.VolumeUp / (1024 * 1024)));

                                    if (b.VolumeDown.Equals(0))
                                        br.LimitDown = "--";
                                    else
                                        br.LimitDown = string.Format("{0} Mb", (b.VolumeDown / (1024 * 1024)));

                                    response.listPremium.Add(br);
                                }
                            }
                        }
                    }
                    else
                    {
                        response.listPremium = new List<BillingTypeResponse>();

                        foreach (BillingTypes b in list.OrderBy(a => a.Order))
                        {
                            if ((!b.FreeAccess) && (b.Enabled))
                            {
                                BillingTypeResponse br = new BillingTypeResponse();
                                br.IdBillingType = b.IdBillingType;
                                br.Description = b.Description;
                                br.Price = b.Price;
                                br.ValidTill = b.ValidTill;
                                br.BWDown = b.BWDown;
                                br.BWUp = b.BWUp;
                                br.VolumeDown = b.VolumeDown;
                                br.VolumeUp = b.VolumeUp;
                                br.MaxDevices = b.MaxDevices;
                                TimeSpan t = TimeSpan.FromSeconds(b.TimeCredit / b.MaxDevices);
                                if (b.TimeCredit >= 86400)
                                    br.TimeCredit += string.Format("{0}d ", t.Days);
                                if (t.Hours > 0)
                                    br.TimeCredit += string.Format("{0:D2}h ", t.Hours);
                                if (t.Minutes > 0)
                                    br.TimeCredit += string.Format("{0:D2}m ", t.Minutes);
                                if (t.Seconds > 0)
                                    br.TimeCredit += string.Format("{0:D2}s ", t.Seconds);

                                if (hotel.CurrencyCode.Equals("EUR"))
                                    br.Currency = string.Format("{0} €", b.Price);
                                else if (hotel.CurrencyCode.Equals("GBP"))
                                    br.Currency = string.Format("£ {0}", b.Price);

                                if (b.VolumeUp.Equals(0))
                                    br.LimitUp = "--";
                                else
                                    br.LimitUp = string.Format("{0} Mb", (b.VolumeUp / (1024 * 1024)));

                                if (b.VolumeDown.Equals(0))
                                    br.LimitDown = "--";
                                else
                                    br.LimitDown = string.Format("{0} Mb", (b.VolumeDown / (1024 * 1024)));

                                response.listPremium.Add(br);
                            }
                        }
                    }
                }

                //ACCESO SOCIAL
                if (hotel.SocialNetworksModule)
                {
                    List<FacebookPromotions> fbpromotions = BillingController.GetFacebookPromotions(hotel.IdHotel, location.IdLocation, true);
                    if (fbpromotions.Count.Equals(0))
                    {
                        response.SocialNetworksModule = false;
                    }
                    else
                    {
                        FacebookPromotionResponse fbpromotionresponse = new FacebookPromotionResponse();
                        fbpromotionresponse.PageName = fbpromotions[0].PageName;
                        fbpromotionresponse.Page = fbpromotions[0].Page;
                        fbpromotionresponse.PageID = fbpromotions[0].PageID;
                        fbpromotionresponse.IdPromotion = fbpromotions[0].IdFacebookPromotion;
                        response.facebookPromotion = fbpromotionresponse;


                        BillingTypeResponse btsocial = new BillingTypeResponse();
                        BillingTypes b = BillingController.ObtainBillingType(fbpromotions[0].IdBillingType);

                        btsocial.IdBillingType = b.IdBillingType;
                        btsocial.Description = b.Description;
                        btsocial.Price = b.Price;
                        btsocial.ValidTill = b.ValidTill;
                        btsocial.BWDown = b.BWDown;
                        btsocial.BWUp = b.BWUp;
                        btsocial.VolumeDown = b.VolumeDown;
                        btsocial.VolumeUp = b.VolumeUp;
                        btsocial.MaxDevices = b.MaxDevices;
                        TimeSpan t = TimeSpan.FromSeconds(b.TimeCredit / b.MaxDevices);
                        if (b.TimeCredit >= 86400)
                            btsocial.TimeCredit += string.Format("{0}d ", t.Days);
                        if (t.Hours > 0)
                            btsocial.TimeCredit += string.Format("{0:D2}h ", t.Hours);
                        if (t.Minutes > 0)
                            btsocial.TimeCredit += string.Format("{0:D2}m ", t.Minutes);
                        if (t.Seconds > 0)
                            btsocial.TimeCredit += string.Format("{0:D2}s ", t.Seconds);

                        if (b.VolumeUp.Equals(0))
                            btsocial.LimitUp = "--";
                        else
                            btsocial.LimitUp = string.Format("{0} Mb", (b.VolumeUp / (1024 * 1024)));

                        if (b.VolumeDown.Equals(0))
                            btsocial.LimitDown = "--";
                        else
                            btsocial.LimitDown = string.Format("{0} Mb", (b.VolumeDown / (1024 * 1024)));

                        response.socialAccess = btsocial;

                    }
                }
                if (hotel.CustomAccessModule)
                {
                    List<BillingTypes> list = BillingController.ObtainBillingTypes(location.IdLocation, 4);
                    if (list.Count.Equals(0))
                    {
                        List<BillingTypes> listAllZones = BillingController.ObtainBillingTypes(locationAllZones.IdLocation, 4);
                        if (listAllZones.Count.Equals(0))
                            response.CustomAccessModule = false;
                    }
                }

                List<Surveys2> listsurvey = BillingController.GetSurveys(hotel.IdHotel, language.IdLanguage);

                response.survey = new List<SurveyResponse>();

                foreach (Surveys2 b in listsurvey.OrderBy(a => a.Order))
                {
                    SurveyResponse s = new SurveyResponse();
                    s.IdSurvey = b.IdSurvey;
                    s.IdHotel = b.IdHotel;
                    s.Question = b.Question;
                    string[] types = b.Type.Split('-');
                    s.Type = types[0].Trim();
                    if (types.Length > 1)
                    {
                        s.Values = types[1];
                    }
                    s.Order = b.Order;

                    response.survey.Add(s);
                }
            }
            catch
            {
                ;
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private FacebookPromotionResponse getFacebookPromotion(HttpContext context)
        {
            FacebookPromotionResponse response = new FacebookPromotionResponse();
            try
            {
                string idhotel = (context.Request["idhotel"] == null ? string.Empty : context.Request["idhotel"].ToString());
                string idlocation = (context.Request["idlocation"] == null ? string.Empty : context.Request["idlocation"].ToString());
                //string idpromotion = (context.Request["idpromotion"] == null ? string.Empty : context.Request["idpromotion"].ToString());

                Hotels hotel = BillingController.GetHotel(Int32.Parse(idhotel));
                Locations2 location = BillingController.ObtainLocation(Int32.Parse(idlocation));
                List<FacebookPromotions> list = BillingController.GetFacebookPromotions(hotel.IdHotel, location.IdLocation, true);

                FacebookPromotions obj = null;
                if (list.Count() > 0)
                    obj = list[0];

                response = new FacebookPromotionResponse();
                if (obj != null)
                {
                    response.IdPromotion = obj.IdFacebookPromotion;
                    response.IdHotel = obj.IdHotel;
                    response.IdLocation = obj.IdLocation;
                    response.IdBillingType = obj.IdBillingType;
                    response.Page = obj.Page;
                    response.PageID = obj.PageID;
                    response.CreationDate = obj.CreationDate;
                    response.Visible = obj.Visible;
                    response.PageName = obj.PageName;
                    response.ValidDays = obj.ValidDays;
                }

            }
            catch
            {

            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private ListFacebookPromotionsResponse getFacebookPromotions(HttpContext context)
        {
            ListFacebookPromotionsResponse response = new ListFacebookPromotionsResponse();
            try
            {
                string idhotel = (context.Request["idhotel"] == null ? string.Empty : context.Request["idhotel"].ToString());
                string idlocation = (context.Request["idlocation"] == null ? string.Empty : context.Request["idlocation"].ToString());

                List<FacebookPromotions> promotions = BillingController.GetFacebookPromotions(Int32.Parse(idhotel), Int32.Parse(idlocation), true);

                Locations2 allZones = BillingController.ObtainLocation(Int32.Parse(idhotel), "All_Zones");
                List<FacebookPromotions> promotionsAllZones = BillingController.GetFacebookPromotions(Int32.Parse(idhotel), allZones.IdLocation, true);

                response.list = new List<FacebookPromotionResponse>();

                foreach (FacebookPromotions obj in promotions)
                {
                    FacebookPromotionResponse r = new FacebookPromotionResponse();
                    r.IdPromotion = obj.IdFacebookPromotion;
                    r.IdHotel = obj.IdHotel;
                    r.IdLocation = obj.IdLocation;
                    r.IdBillingType = obj.IdBillingType;
                    r.Page = obj.Page;
                    r.PageID = obj.PageID;
                    r.CreationDate = obj.CreationDate;
                    r.Visible = obj.Visible;
                    r.PageName = obj.PageName;
                    r.ValidDays = obj.ValidDays;

                    response.list.Add(r);
                }

                foreach (FacebookPromotions obj in promotionsAllZones)
                {
                    FacebookPromotionResponse r = new FacebookPromotionResponse();
                    r.IdPromotion = obj.IdFacebookPromotion;
                    r.IdHotel = obj.IdHotel;
                    r.IdLocation = obj.IdLocation;
                    r.IdBillingType = obj.IdBillingType;
                    r.Page = obj.Page;
                    r.PageID = obj.PageID;
                    r.CreationDate = obj.CreationDate;
                    r.Visible = obj.Visible;
                    r.PageName = obj.PageName;
                    r.ValidDays = obj.ValidDays;

                    response.list.Add(r);
                }
            }
            catch
            {

            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private BillingTypeResponse getfreeInformation(HttpContext context)
        {
            BillingTypeResponse response = new BillingTypeResponse();
            try
            {
                string idhotel = (context.Request["idhotel"] == null ? string.Empty : context.Request["idhotel"].ToString());
                string idlocation = (context.Request["idlocation"] == null ? string.Empty : context.Request["idlocation"].ToString());

                //               HttpCookie cookie = context.Request.Cookies["HotelCookie"];

                Hotels hotel = BillingController.GetHotel(Int32.Parse(idhotel));
                Locations2 location = BillingController.ObtainLocation(Int32.Parse(idlocation));

                List<BillingTypes> free = BillingController.ObtainBillingTypes(location.IdLocation, 1);
                BillingTypes billingType = null;
                if (free.Count > 0)
                {
                    billingType = free[0];
                }
                else
                {
                    free = BillingController.ObtainBillingTypes(BillingController.ObtainLocation(hotel.IdHotel, "All_Zones").IdLocation, 1);
                    if (free.Count > 0)
                        billingType = free[0];
                }

                //                cookie["idbillingtype"] = billingType.IdBillingType.ToString();
                //                context.Response.Cookies.Add(cookie);

                response.IdBillingType = billingType.IdBillingType;
                response.Description = billingType.Description;
                response.Price = billingType.Price;
                response.ValidTill = billingType.ValidTill;
                response.BWDown = billingType.BWDown;
                response.BWUp = billingType.BWUp;
                response.VolumeDown = billingType.VolumeDown;
                response.VolumeUp = billingType.VolumeUp;
                response.MaxDevices = billingType.MaxDevices;
                TimeSpan t = TimeSpan.FromSeconds(billingType.TimeCredit / billingType.MaxDevices);
                if (billingType.TimeCredit >= 86400)
                    response.TimeCredit += string.Format("{0}d ", t.Days);
                if (t.Hours > 0)
                    response.TimeCredit += string.Format("{0:D2}h ", t.Hours);
                if (t.Minutes > 0)
                    response.TimeCredit += string.Format("{0:D2}m ", t.Minutes);
                if (t.Seconds > 0)
                    response.TimeCredit += string.Format("{0:D2}s ", t.Seconds);

                if (billingType.VolumeUp.Equals(0))
                    response.LimitUp = "--";
                else
                    response.LimitUp = string.Format("{0} Mb", billingType.VolumeUp);

                if (billingType.VolumeDown.Equals(0))
                    response.LimitDown = "--";
                else
                    response.LimitDown = string.Format("{0} Mb", billingType.VolumeDown);
            }
            catch
            {

            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private HotelResponse getsite(HttpContext context)
        {
            string idhotel = (context.Request["idhotel"] == null ? string.Empty : context.Request["idhotel"].ToString());
            string idlocation = (context.Request["idlocation"] == null ? string.Empty : context.Request["idlocation"].ToString());
            string lang = (context.Request["lang"] == null ? string.Empty : context.Request["lang"].ToString());
            string port = (context.Request["PORT"] == null ? string.Empty : context.Request["PORT"].ToString());

            HotelResponse response = new HotelResponse();

            try
            {
                Hotels hotel = BillingController.GetHotel(Int32.Parse(idhotel));

                response.IdHotel = hotel.IdHotel;
                response.Name = hotel.Name;

                Languages language = BillingController.GetLanguage(lang);
                if (language.IdLanguage.Equals(0))
                    language.IdLanguage = hotel.IdLanguage;

                Disclaimers dis = BillingController.GetDisclaimer(response.IdHotel, language.IdLanguage);
                response.Disclaimer = dis.Text;
                if (hotel.FreeAccessModule.Equals(false))
                    response.FreeAccessModule = false;
                else
                    response.FreeAccessModule = true;

                if (hotel.PayAccessModule.Equals(false))
                    response.PayAccessModule = false;
                else
                    response.PayAccessModule = true;

                if (hotel.SocialNetworksModule.Equals(false))
                    response.SocialNetworksModule = false;
                else
                    response.SocialNetworksModule = true;

                if (hotel.CustomAccessModule.Equals(false))
                    response.CustomAccessModule = false;
                else
                    response.CustomAccessModule = true;

                Locations2 location = location = BillingController.ObtainLocation(Int32.Parse(idlocation));

                LocationsTexts locationText = BillingController.GetLocationText(location.IdLocation, language.IdLanguage);
                response.LocationText = locationText.Text;

                Locations2 locationAllZones = BillingController.ObtainLocation(hotel.IdHotel, "All_Zones");
                if (hotel.FreeAccessModule)
                {
                    List<BillingTypes> list = BillingController.ObtainBillingTypes(location.IdLocation, 1);
                    if (list.Count.Equals(0))
                    {
                        List<BillingTypes> listAllZones = BillingController.ObtainBillingTypes(locationAllZones.IdLocation, 1);
                        if (listAllZones.Count.Equals(0))
                            response.FreeAccessModule = false;
                    }
                }
                if (hotel.PayAccessModule)
                {
                    List<BillingTypes> list = BillingController.ObtainBillingTypes(location.IdLocation, 2);
                    if (list.Count.Equals(0))
                    {
                        List<BillingTypes> listAllZones = BillingController.ObtainBillingTypes(locationAllZones.IdLocation, 2);
                        if (listAllZones.Count.Equals(0))
                            response.PayAccessModule = false;
                    }
                }
                if (hotel.SocialNetworksModule)
                {
                    List<BillingTypes> list = BillingController.ObtainBillingTypes(location.IdLocation, 3);
                    if (list.Count.Equals(0))
                    {
                        List<BillingTypes> listAllZones = BillingController.ObtainBillingTypes(locationAllZones.IdLocation, 3);
                        if (listAllZones.Count.Equals(0))
                            response.SocialNetworksModule = false;
                    }
                }
                if (hotel.CustomAccessModule)
                {
                    List<BillingTypes> list = BillingController.ObtainBillingTypes(location.IdLocation, 4);
                    if (list.Count.Equals(0))
                    {
                        List<BillingTypes> listAllZones = BillingController.ObtainBillingTypes(locationAllZones.IdLocation, 4);
                        if (listAllZones.Count.Equals(0))
                            response.CustomAccessModule = false;
                    }
                }

            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private BillingTypeResponse getSocialNetworkInformation(HttpContext context)
        {
            BillingTypeResponse response = new BillingTypeResponse();
            try
            {
                string idhotel = (context.Request["idhotel"] == null ? string.Empty : context.Request["idhotel"].ToString());
                string idlocation = (context.Request["idlocation"] == null ? string.Empty : context.Request["idlocation"].ToString());
                string idpromotion = (context.Request["idpromotion"] == null ? string.Empty : context.Request["idpromotion"].ToString());


                Hotels hotel = BillingController.GetHotel(Int32.Parse(idhotel));
                Locations2 location = BillingController.ObtainLocation(Int32.Parse(idlocation));

                FacebookPromotions obj = null;
                if (!string.IsNullOrEmpty(idpromotion))
                    obj = BillingController.GetFacebookPromotion(Int32.Parse(idpromotion));
                else
                {
                    List<FacebookPromotions> fbpromotions = BillingController.GetFacebookPromotions(hotel.IdHotel, location.IdLocation, true);
                    if (fbpromotions.Count() > 0)
                        obj = fbpromotions[0];
                }

                BillingTypes billingType = BillingController.ObtainBillingType(obj.IdBillingType);

                response.IdBillingType = billingType.IdBillingType;
                response.Description = billingType.Description;
                response.Price = billingType.Price;
                response.ValidTill = billingType.ValidTill;
                response.BWDown = billingType.BWDown;
                response.BWUp = billingType.BWUp;
                response.VolumeDown = billingType.VolumeDown;
                response.VolumeUp = billingType.VolumeUp;
                response.MaxDevices = billingType.MaxDevices;
                TimeSpan t = TimeSpan.FromSeconds(billingType.TimeCredit / billingType.MaxDevices);
                if (billingType.TimeCredit >= 86400)
                    response.TimeCredit += string.Format("{0}d ", t.Days);
                if (t.Hours > 0)
                    response.TimeCredit += string.Format("{0:D2}h ", t.Hours);
                if (t.Minutes > 0)
                    response.TimeCredit += string.Format("{0:D2}m ", t.Minutes);
                if (t.Seconds > 0)
                    response.TimeCredit += string.Format("{0:D2}s ", t.Seconds);

                if (billingType.VolumeUp.Equals(0))
                    response.LimitUp = "--";
                else
                    response.LimitUp = string.Format("{0} Mb", billingType.VolumeUp);

                if (billingType.VolumeDown.Equals(0))
                    response.LimitDown = "--";
                else
                    response.LimitDown = string.Format("{0} Mb", billingType.VolumeDown);
            }
            catch
            {

            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private ListSurveyResponse getsurvey(HttpContext context)
        {
            string idhotel = (context.Request["idhotel"] == null ? string.Empty : context.Request["idhotel"].ToString());
            string lang = (context.Request["lang"] == null ? string.Empty : context.Request["lang"].ToString());

            ListSurveyResponse response = new ListSurveyResponse();

            try
            {
                Hotels hotel = BillingController.GetHotel(Int32.Parse(idhotel));

                Languages language = BillingController.GetLanguage(lang);
                if (language.IdLanguage.Equals(0))
                    language.IdLanguage = hotel.IdLanguage;
                response.hotel = hotel.IdHotel;
                List<Surveys2> list = BillingController.GetSurveys(hotel.IdHotel, language.IdLanguage);

                response.list = new List<SurveyResponse>();

                foreach (Surveys2 b in list.OrderBy(a => a.Order))
                {
                    SurveyResponse s = new SurveyResponse();
                    s.IdSurvey = b.IdSurvey;
                    s.IdHotel = b.IdHotel;
                    s.Question = b.Question;
                    string[] types = b.Type.Split('-');
                    s.Type = types[0].Trim();
                    if (types.Length > 1)
                    {
                        s.Values = types[1];
                    }
                    s.Order = b.Order;

                    response.list.Add(s);
                }
            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private void login(HttpContext context)
        {
            string json = string.Empty;
            CaptivePortalLog log = new CaptivePortalLog();
            try
            {
                string username = (context.Request["username"] == null ? string.Empty : context.Request["username"].ToString());
                string password = (context.Request["password"] == null ? string.Empty : context.Request["password"].ToString());
                string idhotel = (context.Request["idhotel"] == null ? string.Empty : context.Request["idhotel"].ToString());
                string idlocation = (context.Request["idlocation"] == null ? string.Empty : context.Request["idlocation"].ToString());
                string mac = (context.Request["mac"] == null ? string.Empty : context.Request["mac"].ToString());

                string sIP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
                string uri = "http://" + sIP + ":1111/usg/command.xml";

                //string uri = "http://192.168.10.16:1111/usg/command.xml";

                log.NSEId = string.Format("Nomadix - Idhotel:{0}", idhotel);
                log.CallerID = mac;
                log.Date = DateTime.Now.ToUniversalTime();
                log.UserName = username.ToUpper();
                log.Password = password.ToUpper();
                log.Page = "login";
                log.IdLocation = Int32.Parse(idlocation);
                log.IdHotel = Int32.Parse(idhotel);
                log.RadiusResponse = string.Empty;
                log.NomadixResponse = string.Empty;

                Hotels hotel = BillingController.GetHotel(Int32.Parse(idhotel));
                Locations2 location = BillingController.ObtainLocation(Int32.Parse(idlocation));
                Users user = BillingController.GetRadiusUser(username, hotel.IdHotel);
                BillingTypes billingType = BillingController.ObtainBillingType(user.IdBillingType);
                string allow = "allow";

                if (hotel.CustomAccessModule)
                {
                    CustomAccess ca = BillingController.GetCustomAccess(hotel.IdHotel);
                    switch (ca.Nombre.ToUpper())
                    {
                        case "CUNEF": allow = Cunef(hotel.IdHotel, username, password, mac); break;
                    }
                }

                if (allow.ToUpper().Equals("ALLOW"))
                {
                    XmlDocument xml = CrearXML(username.ToUpper(), password.ToUpper(), mac);

                    DateTime timeXMLResponse = DateTime.Now.ToUniversalTime();
                    XmlDocument xmlResponse = PostXMLTransaction(uri, xml);

                    Thread.Sleep(1000);

                    foreach (XmlAttribute att in xmlResponse.DocumentElement.Attributes)
                    {
                        if (att.Name.Equals("RESULT"))
                        {
                            if (att.Value.Equals("OK")) //EL PAQUETE ENVIADO AL NOMADIX TIENE EL FORMATO CORRECTO DE USER, PASS Y MAC
                            {
                                log.NomadixResponse = "OK";

                                DateTime time = DateTime.Now.ToUniversalTime();
                                DateTime aux = time.AddSeconds(30);
                                do
                                {
                                    //COMPROBAMOS QUE HAYA UNA SESION ACTIVA EN LOS ULTIMOS 30 SEGUNDOS CON LA MAC , SI ES CORRECTA, DEVOLVEMOS UN OK
                                    ActiveSessions session = BillingController.GetActiveSession(mac, username.ToUpper());
                                    if (!string.IsNullOrEmpty(session.AcctSessionID))
                                    {
                                        log.RadiusResponse = "OK";
                                        var url = string.Empty;
                                        if (string.IsNullOrEmpty(billingType.UrlLanding))
                                        {
                                            if (!String.IsNullOrEmpty(location.UrlLanding))
                                                url = location.UrlLanding;
                                            else
                                                url = hotel.UrlHotel;
                                        }
                                        else
                                            url = billingType.UrlLanding;

                                        json = "{\"code\":\"OK\",\"message\":\"" + url + "\"}";
                                        break;
                                    }
                                    else // SI NO HAY SESSION ACTIVA ES PORQUE HA FALLADO EL LOGIN, VAMOS A LA TABLA DE FAILED REQUEST Y MOSTRAMOS EL ERROR INDICADO POR EL RADIUS
                                    {
                                        FailedRequests failed = BillingController.GetFailedRequest(mac, username.ToUpper(), timeXMLResponse);
                                        if (!failed.ID.Equals(0))
                                        {
                                            json = "{\"code\":\"ERROR\",\"message\":\"" + failed.ReplyMessage + "\"}";
                                            log.RadiusResponse = failed.ReplyMessage;
                                            break;
                                        }
                                        else
                                        { // SI TAMPOCO HAY ERROR, ES PORQUE EL RADIUS HA RECHAZADO EL PAQUETE Y MOSTRAMOS UN ERROR DESCONOCIDO.

                                            json = "{\"code\":\"ERROR\",\"message\":\"Unknown Error\"}";
                                            log.RadiusResponse += "ERROR - USER IS NOT IN RADIUS TABLES - TRAZA " + time.ToString() + Environment.NewLine;
                                        }
                                    }
                                    time = time.AddSeconds(1);
                                    Thread.Sleep(1000);
                                } while (time != aux);

                                //VALIDAMOS EL USERAGENT
                                UserAgentsTemp temp = BillingController.GetUserAgentTemp(mac);
                                if (temp.IdTemp != 0)
                                {
                                    UserAgents userAgent = BillingController.GetUserAgent(temp.IdUserAgent);
                                    List<UserAgents> listUA = BillingController.GetUserAgents(userAgent.String);

                                    userAgent.Valid = userAgent.Valid + 1; ;
                                    BillingController.SaveUserAgent(userAgent);
                                    foreach (UserAgents x in listUA)
                                    {
                                        x.Valid = x.Valid + 1; ;
                                        BillingController.SaveUserAgent(x);
                                    }

                                    // BillingController.DeleteUserAgentTemp(temp);
                                }

                                //METEMOS LOS USERAGENTS EN LA LISTA NEGRA
                                List<UserAgents> uAtoBlackList = BillingController.GetUserAgentsToBlackList(Int32.Parse(ConfigurationManager.AppSettings["MaxAttemps"].ToString()));
                                foreach (UserAgents uA in uAtoBlackList)
                                {
                                    if (uA.Valid.Equals(0))
                                    {
                                        BlackListUserAgents bUA = BillingController.GetBlackUserAgent(uA.String);
                                        if (bUA.IdBlackUserAgent.Equals(0))
                                        {
                                            BlackListUserAgents obj = new BlackListUserAgents();
                                            obj.String = uA.String;
                                            BillingController.SaveBlackListUserAgent(obj);
                                        }
                                    }
                                }
                            }
                            else if (att.Value.Equals("ERROR"))
                            {
                                if (xmlResponse.InnerText.Contains("209"))
                                {
                                    json = "{\"code\":\"OK\",\"message\":\"" + hotel.UrlHotel + "\"}";
                                    log.NomadixResponse = "OK - ALREADY CONNECTED, REDIRECTING " + xmlResponse.InnerText;
                                    log.RadiusResponse = "NOT APPLICABLE";
                                }
                                else
                                {
                                    json = "{\"code\":\"ERROR\",\"message\":\"" + xmlResponse.InnerText + "\"}";
                                    log.NomadixResponse = "ERROR" + xmlResponse.InnerText;
                                    log.RadiusResponse = "NOT APPLICABLE";
                                }
                            }
                        }
                    }
                }
                else
                {
                    log.NomadixResponse = "WS ERROR RESPONSE - " + allow;
                    log.RadiusResponse = "NOT APPLICABLE";
                    json = "{\"code\":\"ERROR\",\"message\":\"" + allow + "\"}";
                }
            }
            catch (Exception ex)
            {
                json = "{\"code\":\"ERROR\",\"message\":\" Sorry, please try again!.\"}";
                log.NomadixResponse = "ERROR - NOMADIX NO COMUNICATIONS";
                log.RadiusResponse = "NOT APPLICABLE";
            }

            BillingController.SaveCaptivePortalLog(log);

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();
        }

        private PayPalResponse paypalrequest(HttpContext context)
        {
            PayPalResponse response = new PayPalResponse();
            try
            {
                string name = (context.Request["name"] == null ? string.Empty : context.Request["name"].ToString());
                string surname = (context.Request["surname"] == null ? string.Empty : context.Request["surname"].ToString());
                string mail = (context.Request["email"] == null ? string.Empty : context.Request["email"].ToString());
                string username = (context.Request["username"] == null ? string.Empty : context.Request["username"].ToString());
                string password = (context.Request["password"] == null ? string.Empty : context.Request["password"].ToString());

                string idhotel = (context.Request["idhotel"] == null ? string.Empty : context.Request["idhotel"].ToString());
                string mac = (context.Request["mac"] == null ? string.Empty : context.Request["mac"].ToString());
                string idlocation = (context.Request["idlocation"] == null ? string.Empty : context.Request["idlocation"].ToString());
                string idBillingType = (context.Request["IdBillingType"] == null ? string.Empty : context.Request["IdBillingType"].ToString());
                string survey = (context.Request["survey"] == null ? string.Empty : context.Request["survey"].ToString());
                string mailing = (context.Request["mailing"] == null ? string.Empty : context.Request["mailing"].ToString());


                string sIP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
                string uri = "http://" + sIP + ":1111/usg/command.xml";

                //string uri = "http://192.168.10.16:1111/usg/command.xml";

                if (util.ValidateEmail(mail))
                {
                    Hotels hotel = BillingController.GetHotel(Int32.Parse(idhotel));
                    Locations2 location = BillingController.ObtainLocation(Int32.Parse(idlocation));

                    if (location != null)
                    {
                        BillingTypes bt = BillingController.ObtainBillingType(Int32.Parse(idBillingType));

                        PayPalTransactions transaction = new PayPalTransactions();
                        transaction.IdHotel = location.IdHotel;
                        transaction.IdBillingType = bt.IdBillingType;
                        transaction.Price = bt.Price;
                        transaction.CreateDate = DateTime.Now.ToUniversalTime();
                        transaction.UpdateDate = DateTime.Now.ToUniversalTime();
                        transaction.Status = 0;
                        transaction.NameClient = name + " " + surname;
                        transaction.Email = mail;
                        transaction.Survey = survey;
                        transaction.CallerIP = sIP; // origin;
                        transaction.IdLocation = location.IdLocation;

                        List<ParametrosConfiguracion> parametros = BillingController.GetParametrosConfiguracion(hotel.IdHotel);
                        int longUserName = 3;
                        int longPassword = 3;
                        string prefixUserName = string.Empty;

                        foreach (ParametrosConfiguracion obj in parametros)
                        {
                            switch (obj.Key.ToUpper())
                            {
                                case "LONGUSERNAME": longUserName = Int32.Parse(obj.value); break;
                                case "LONGPASSWORD": longPassword = Int32.Parse(obj.value); break;
                                case "USERNAMEPREFIX": prefixUserName = obj.value; break;
                            }
                        }

                        if (longPassword < 3)
                            longPassword = 3;
                        if (longUserName < 3)
                            longUserName = 3;

                        Users existuser = null;
                        username = string.Empty;
                        do
                        {
                            username = string.Format("{0}{1}", prefixUserName, util.createRandomUserNumber(longUserName)).ToUpper();
                            existuser = BillingController.GetRadiusUser(username.ToUpper(), hotel.IdHotel);

                        } while (existuser.Enabled);

                        password = util.CreateRandomPassword(longPassword).ToUpper();


                        transaction.UserName = username.ToUpper();
                        transaction.Password = password.ToUpper();
                        transaction.MAC = mac;
                        transaction.InvoiceNo = "";
                        transaction.UI = idhotel;
                        transaction.OS = "";

                        if (Boolean.Parse(mailing))
                        {
                            Loyalties2 loyalty = new Loyalties2();
                            loyalty.IdHotel = transaction.IdHotel;
                            loyalty.Name = name.ToUpper();
                            loyalty.Surname = surname.ToUpper();
                            loyalty.Email = mail;
                            loyalty.Date = DateTime.Now.ToUniversalTime();
                            loyalty.Survey = survey;

                            BillingController.SaveLoyalty(loyalty);
                        }

                        BillingController.SavePayPalTransaction(transaction);

                        response.IdTransaction = transaction.IdTransaction;
                        response.Description = bt.PayPalDescription;
                        response.Price = bt.Price.ToString();
                        response.Price = response.Price.Replace(',', '.');
                    }
                }
                else
                    response.IdTransaction = 0;
            }
            catch
            {

            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private PayPalResponse paypalrequestnosurvey(HttpContext context)
        {
            PayPalResponse response = new PayPalResponse();
            try
            {
                string idhotel = (context.Request["idhotel"] == null ? string.Empty : context.Request["idhotel"].ToString());
                string mac = (context.Request["mac"] == null ? string.Empty : context.Request["mac"].ToString());
                string idlocation = (context.Request["idlocation"] == null ? string.Empty : context.Request["idlocation"].ToString());
                string idBillingType = (context.Request["IdBillingType"] == null ? string.Empty : context.Request["IdBillingType"].ToString());
                string email = (context.Request["email"] == null ? string.Empty : context.Request["email"].ToString());

                string sIP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
                string uri = "http://" + sIP + ":1111/usg/command.xml";

                //string uri = "http://192.168.10.16:1111/usg/command.xml";

                Hotels hotel = BillingController.GetHotel(Int32.Parse(idhotel));
                Locations2 location = BillingController.ObtainLocation(Int32.Parse(idlocation));

                if (location != null)
                {
                    BillingTypes bt = BillingController.ObtainBillingType(Int32.Parse(idBillingType));

                    PayPalTransactions transaction = new PayPalTransactions();
                    transaction.IdHotel = location.IdHotel;
                    transaction.IdBillingType = bt.IdBillingType;
                    transaction.Price = bt.Price;
                    transaction.CreateDate = DateTime.Now.ToUniversalTime();
                    transaction.UpdateDate = DateTime.Now.ToUniversalTime();
                    transaction.Status = 0;
                    transaction.NameClient = "NO SURVEY";
                    transaction.Email = email;
                    transaction.Survey = "NO MANDATORY";
                    transaction.CallerIP = sIP; // origin;
                    transaction.IdLocation = location.IdLocation;

                    List<ParametrosConfiguracion> parametros = BillingController.GetParametrosConfiguracion(hotel.IdHotel);
                    int longUserName = 3;
                    int longPassword = 3;
                    string prefixUserName = string.Empty;

                    foreach (ParametrosConfiguracion obj in parametros)
                    {
                        switch (obj.Key.ToUpper())
                        {
                            case "LONGUSERNAME": longUserName = Int32.Parse(obj.value); break;
                            case "LONGPASSWORD": longPassword = Int32.Parse(obj.value); break;
                            case "USERNAMEPREFIX": prefixUserName = obj.value; break;
                        }
                    }

                    if (longPassword < 3)
                        longPassword = 3;
                    if (longUserName < 3)
                        longUserName = 3;

                    Users existuser = null;
                    string username = string.Empty;
                    do
                    {
                        username = string.Format("{0}{1}", prefixUserName, util.createRandomUserNumber(longUserName)).ToUpper();
                        existuser = BillingController.GetRadiusUser(username.ToUpper(), hotel.IdHotel);

                    } while (existuser.Enabled);

                    string password = util.CreateRandomPassword(longPassword).ToUpper();

                    transaction.UserName = username.ToUpper();
                    transaction.Password = password.ToUpper();
                    transaction.MAC = mac;
                    transaction.InvoiceNo = "";
                    transaction.UI = idhotel;
                    transaction.OS = "";

                    BillingController.SavePayPalTransaction(transaction);

                    response.IdTransaction = transaction.IdTransaction;
                    response.Description = bt.PayPalDescription;
                    response.Price = bt.Price.ToString();
                    response.Price = response.Price.Replace(',', '.');
                }
            }
            catch
            {

            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private void saveSurvey(HttpContext context)
        {
            string json = string.Empty;
            try
            {
                string survey = (context.Request["survey"] == null ? string.Empty : context.Request["survey"].ToString());
                string idhotel = (context.Request["idhotel"] == null ? string.Empty : context.Request["idhotel"].ToString());
                string mail = (context.Request["email"] == null ? string.Empty : context.Request["email"].ToString());

                Hotels hotel = BillingController.GetHotel(Int32.Parse(idhotel));
                Loyalties2 loyalty = new Loyalties2();

                loyalty.IdHotel = hotel.IdHotel;
                loyalty.Name = "";
                loyalty.Surname = "";
                loyalty.Email = mail;
                loyalty.Date = DateTime.Now.ToUniversalTime();
                loyalty.Survey = survey;

                BillingController.SaveLoyalty(loyalty);

                json = "{\"code\":\"OK\",\"message\":\"..\"}";
            }
            catch (Exception ex)
            {
                json = "{\"code\":\"ERROR\",\"message\":\" " + ex.Message + ".\"}";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();
        }

        private HotelResponse sitelocationmodule(HttpContext context)
        {
            string idhotel = (context.Request["idhotel"] == null ? string.Empty : context.Request["idhotel"].ToString());
            string idlocation = (context.Request["idlocation"] == null ? string.Empty : context.Request["idlocation"].ToString());
            string lang = (context.Request["lang"] == null ? string.Empty : context.Request["lang"].ToString());
            string billingmodule = (context.Request["billingmodule"] == null ? string.Empty : context.Request["billingmodule"].ToString());

            HotelResponse response = new HotelResponse();

            try
            {
                Hotels hotel = BillingController.GetHotel(Int32.Parse(idhotel));

                response.IdHotel = hotel.IdHotel;
                response.Name = hotel.Name;

                Languages language = BillingController.GetLanguage(lang);
                if (language.IdLanguage.Equals(0))
                    language.IdLanguage = hotel.IdLanguage;

                Disclaimers dis = BillingController.GetDisclaimer(response.IdHotel, language.IdLanguage);
                response.Disclaimer = dis.Text;

                Locations2 location = BillingController.ObtainLocation(Int32.Parse(idlocation));
                LocationModuleText locationText = BillingController.GetLocationModuleText(location.IdLocation, Int32.Parse(billingmodule), language.IdLanguage);
                response.LocationText = locationText.Text;

            }
            catch
            {
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private UserResponse socialnetwork(HttpContext context)
        {
            CaptivePortalLog log = new CaptivePortalLog();
            UserResponse response = new UserResponse();
            try
            {
                string name = (context.Request["name"] == null ? string.Empty : context.Request["name"].ToString());
                string surname = (context.Request["surname"] == null ? string.Empty : context.Request["surname"].ToString());
                string mail = (context.Request["email"] == null ? string.Empty : context.Request["email"].ToString());
                string survey = (context.Request["survey"] == null ? string.Empty : context.Request["survey"].ToString());
                string mac = (context.Request["mac"] == null ? string.Empty : context.Request["mac"].ToString());

                string idhotel = (context.Request["idhotel"] == null ? string.Empty : context.Request["idhotel"].ToString());
                string macformated = (context.Request["mac"] == null ? string.Empty : context.Request["mac"].ToString());
                string idlocation = (context.Request["idlocation"] == null ? string.Empty : context.Request["idlocation"].ToString());
                string lang = (context.Request["lang"] == null ? string.Empty : context.Request["lang"].ToString());
                string idpromotion = (context.Request["idpromotion"] == null ? string.Empty : context.Request["idpromotion"].ToString());

                string sIP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
                string uri = "http://" + sIP + ":1111/usg/command.xml";

                //string uri = "http://192.168.10.16:1111/usg/command.xml";

                log.NSEId = string.Format("Nomadix - {0}", idhotel);
                log.CallerID = mac;//origin;
                log.Date = DateTime.Now.ToUniversalTime();
                log.UserName = mail.ToUpper();
                log.Password = mail.ToUpper();
                log.Page = "socialnetwork";
                log.IdLocation = Int32.Parse(idlocation);
                log.IdHotel = Int32.Parse(idhotel);
                log.RadiusResponse = string.Empty;
                log.NomadixResponse = string.Empty;

                Hotels hotel = BillingController.GetHotel(Int32.Parse(idhotel));
                Locations2 location = BillingController.ObtainLocation(Int32.Parse(idlocation));

                FacebookPromotions promotion = null;
                if (!string.IsNullOrEmpty(idpromotion))
                    promotion = BillingController.GetFacebookPromotion(Int32.Parse(idpromotion));
                else
                {
                    List<FacebookPromotions> fbpromotions = BillingController.GetFacebookPromotions(hotel.IdHotel, location.IdLocation, true);
                    if (fbpromotions.Count() > 0)
                        promotion = fbpromotions[0];
                }

                BillingTypes billingType = BillingController.ObtainBillingType(promotion.IdBillingType);

                if (location != null)
                {
                    log.IdHotel = location.IdHotel;
                    log.IdLocation = location.IdLocation;


                    Rooms room = BillingController.GetRoom(hotel.IdHotel, "DEFAULT");

                    log.UserName = mail.ToUpper();
                    log.Password = mail.ToUpper();

                    Users user = null;

                    bool existe = false;

                    if (mail.ToUpper().Equals("UNDEFINED") || string.IsNullOrEmpty(mail))  //EL USUARIO NO HA CONCEDIDO SU CORREO ELECTRÓNICO
                    {
                        mail = string.Format("{0}{1}", "SN", util.createRandomUserNumber(8)).ToUpper(); //GENERAMOS UN USUARIO NUEVO
                        existe = true;

                        do
                        {
                            user = BillingController.GetRadiusUser(mail.ToUpper(), hotel.IdHotel);  //COMPROBAMOS QUE NO EXISTA EN LA BASE DE DATOS
                            if (user.IdUser.Equals(0))
                                existe = false; // SI NO EXISTE, CONTINUAMOS 
                            else
                            {
                                if (user.Enabled) // SI EXISTE Y ESTÁ ACTIVO, GENERAMOS UNO NUEVO Y EMPEZAMOS EL PROCESO DE NUEVO
                                {
                                    mail = string.Format("{0}{1}", "SN", util.createRandomUserNumber(8)).ToUpper();
                                }
                                else
                                {//SI NO ESTÁ ACTIVO, CONTINUAMOS CON USUARIO GENERADO Y CREAMOS UN USUARIO NUEVO
                                    existe = false;
                                    user = new Users();
                                }
                            }

                        } while (existe);
                    }
                    else
                        user = BillingController.GetRadiusUser(mail.ToUpper(), hotel.IdHotel);


                    if (user.IdUser.Equals(0))
                        user = new Users();
                    user.IdHotel = location.IdHotel;
                    user.IdRoom = room.IdRoom;
                    user.IdBillingType = billingType.IdBillingType;
                    user.BWDown = billingType.BWDown;
                    user.BWUp = billingType.BWUp;
                    user.ValidSince = DateTime.Now.ToUniversalTime(); // bill.BillingDate;
                    user.ValidTill = DateTime.Now.ToUniversalTime().AddHours(billingType.ValidTill); // bill.BillingDate.AddHours(billingType.ValidTill);
                    user.TimeCredit = billingType.TimeCredit;
                    user.MaxDevices = billingType.MaxDevices;
                    user.Priority = billingType.IdPriority;
                    user.VolumeDown = billingType.VolumeDown;
                    user.VolumeUp = billingType.VolumeUp;
                    user.Enabled = true;
                    user.Name = mail.ToUpper();
                    user.Password = mail.ToUpper();
                    user.CHKO = true;
                    user.BuyingFrom = 1;
                    user.BuyingCallerID = macformated;

                    Loyalties2 loyalty = new Loyalties2();
                    loyalty.IdHotel = user.IdHotel;
                    loyalty.Name = name.ToUpper();
                    loyalty.Surname = surname.ToUpper();
                    loyalty.Email = mail;
                    loyalty.Date = DateTime.Now.ToUniversalTime();
                    loyalty.Survey = survey;

                    BillingController.SaveLoyalty(loyalty);

                    BillingController.SaveUser(user);

                    XmlDocument xml = CrearXML(user.Name.ToUpper(), user.Password.ToUpper(), mac);

                    DateTime timeXMLResponse = DateTime.Now.ToUniversalTime();
                    Thread.Sleep(1000);
                    XmlDocument xmlResponse = PostXMLTransaction(uri, xml);

                    foreach (XmlAttribute att in xmlResponse.DocumentElement.Attributes)
                    {
                        if (att.Name.Equals("RESULT"))
                        {
                            if (att.Value.Equals("OK"))
                            {
                                log.NomadixResponse = "OK";

                                DateTime time = DateTime.Now.ToUniversalTime();
                                DateTime aux = time.AddSeconds(30);

                                do
                                {
                                    //COMPROBAMOS QUE HAYA UNA SESION ACTIVA CON LA MAC , SI ES CORRECTA, DEVOLVEMOS UN OK
                                    ActiveSessions session = BillingController.GetActiveSession(mac, user.Name.ToUpper());
                                    if (!string.IsNullOrEmpty(session.AcctSessionID))
                                    {
                                        response.IdUser = 1;
                                        response.UserName = user.Name;
                                        response.Password = user.Password;
                                        response.ValidTill = user.ValidTill.AddHours(hotel.GMT).ToString("dd/MM/yyyy HH:mm:ss");

                                        if (string.IsNullOrEmpty(billingType.UrlLanding))
                                        {
                                            if (string.IsNullOrEmpty(location.UrlLanding))
                                                response.Url = hotel.UrlHotel;
                                            else
                                                response.Url = location.UrlLanding;
                                        }
                                        else
                                        {
                                            response.Url = billingType.UrlLanding;
                                        }

                                        log.RadiusResponse = "OK";

                                        //VALIDAMOS EL USERAGENT
                                        UserAgentsTemp temp = BillingController.GetUserAgentTemp(mac);
                                        if (temp.IdTemp != 0)
                                        {
                                            UserAgents userAgent = BillingController.GetUserAgent(temp.IdUserAgent);
                                            List<UserAgents> listUA = BillingController.GetUserAgents(userAgent.String);

                                            userAgent.Valid = userAgent.Valid + 1; ;
                                            BillingController.SaveUserAgent(userAgent);
                                            foreach (UserAgents x in listUA)
                                            {
                                                x.Valid = x.Valid + 1; ;
                                                BillingController.SaveUserAgent(x);
                                            }

                                            //BillingController.DeleteUserAgentTemp(temp);
                                        }

                                        //METEMOS LOS USERAGENTS EN LA LISTA NEGRA

                                        List<UserAgents> uAtoBlackList = BillingController.GetUserAgentsToBlackList(Int32.Parse(ConfigurationManager.AppSettings["MaxAttemps"].ToString()));
                                        foreach (UserAgents uA in uAtoBlackList)
                                        {
                                            if (uA.Valid.Equals(0))
                                            {
                                                BlackListUserAgents bUA = BillingController.GetBlackUserAgent(uA.String);
                                                if (bUA.IdBlackUserAgent.Equals(0))
                                                {
                                                    BlackListUserAgents obj = new BlackListUserAgents();
                                                    obj.String = uA.String;
                                                    BillingController.SaveBlackListUserAgent(obj);
                                                }
                                            }
                                        }

                                        util.sendMailRoot(user.IdHotel, billingType.IdBillingType, mail, user.Name, user.Password, lang.Substring(0, 2));

                                        break;
                                    }
                                    else // SI NO HAY SESSION ACTIVA ES PORQUE HA FALLADO EL LOGIN, VAMOS A LA TABLA DE FAILED REQUEST Y MOSTRAMOS EL ERROR INDICADO POR EL RADIUS
                                    {
                                        FailedRequests failed = BillingController.GetFailedRequest(mac, user.Name.ToUpper(), timeXMLResponse);
                                        if (!failed.ID.Equals(0))
                                        {
                                            response.IdUser = 0;
                                            response.UserName = failed.ReplyMessage;
                                            log.RadiusResponse = failed.ReplyMessage;
                                            break;
                                        }
                                        else
                                        { // SI TAMPOCO HAY ERROR, ES PORQUE EL RADIUS HA RECHAZADO EL PAQUETE Y MOSTRAMOS UN ERROR DESCONOCIDO.
                                            response.IdUser = -3;
                                            log.RadiusResponse += "ERROR - USER IS NOT IN RADIUS TABLES - TRAZA " + time.ToString() + Environment.NewLine;
                                        }
                                    }
                                    Thread.Sleep(1000);
                                    time = time.AddSeconds(1);
                                } while (time < aux);


                            }
                            else if (att.Value.Equals("ERROR"))
                            {
                                response.IdUser = 0;
                                log.NomadixResponse = "ERROR EN PAQUETE DE NOMADIX - " + xmlResponse.InnerText;
                                log.RadiusResponse = "NOT APPLICABLE";
                            }

                        }

                    }
                }
                else
                {
                    response.IdUser = -3;
                    log.NomadixResponse = "No Location";
                    log.RadiusResponse = "No Location";

                }
            }
            catch (Exception ex)
            {
                if (ex.GetType().Namespace.Contains("IO"))
                {
                    log.NomadixResponse = "OK";
                    log.RadiusResponse = "OK";

                    CaptivePortalLogInternal internalLog = new CaptivePortalLogInternal();
                    internalLog.NSEId = log.NSEId;
                    internalLog.CallerID = log.CallerID;
                    internalLog.Date = DateTime.Now.ToUniversalTime();
                    internalLog.UserName = "";
                    internalLog.Password = "";
                    internalLog.Page = "socialnetwork";
                    internalLog.IdLocation = 0;

                    internalLog.NomadixResponse = "ERROR - CAN`T LOAD EMAIL TEMPLATE";
                    internalLog.RadiusResponse = "OK";

                    BillingController.SaveCaptivePortalLogInternal(internalLog);

                }
                else if (ex.GetType().Namespace.ToUpper().Contains("MAIL"))
                {
                    CaptivePortalLogInternal internalLog = new CaptivePortalLogInternal();
                    internalLog.NSEId = log.NSEId;
                    internalLog.CallerID = log.CallerID;
                    internalLog.Date = DateTime.Now.ToUniversalTime();
                    internalLog.UserName = "";
                    internalLog.Password = "";
                    internalLog.Page = "socialnetwork";
                    internalLog.IdLocation = 0;
                    internalLog.NomadixResponse = "ERROR - CAN`T SEND EMAIL TO CLIENT";
                    internalLog.RadiusResponse = "OK";

                    BillingController.SaveCaptivePortalLogInternal(internalLog);

                    log.NomadixResponse = "OK ";
                    log.RadiusResponse = "OK";

                }
                else
                {
                    log.NomadixResponse = "NOT APPLICABLE";
                    log.RadiusResponse = "NOT APPLICABLE";

                    CaptivePortalLogInternal internalLog = new CaptivePortalLogInternal();
                    internalLog.NSEId = log.NSEId;
                    internalLog.CallerID = log.CallerID;
                    internalLog.Date = DateTime.Now.ToUniversalTime();
                    internalLog.UserName = "";
                    internalLog.Password = "";
                    internalLog.Page = "socialnetwork";
                    internalLog.IdLocation = 0;
                    internalLog.NomadixResponse = "ERROR NO CONTEMPLADO - " + ex.Message;
                    internalLog.RadiusResponse = "OK";

                    BillingController.SaveCaptivePortalLogInternal(internalLog);
                }

            }

            BillingController.SaveCaptivePortalLog(log);

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private UserResponse facebookAccess(HttpContext context)
        {
            CaptivePortalLog log = new CaptivePortalLog();
            UserResponse response = new UserResponse();
            try
            {
                string mail = (context.Request["email"] == null ? string.Empty : context.Request["email"].ToString());

                string idhotel = (context.Request["idhotel"] == null ? string.Empty : context.Request["idhotel"].ToString());
                string mac = (context.Request["mac"] == null ? string.Empty : context.Request["mac"].ToString());
                string idlocation = (context.Request["idlocation"] == null ? string.Empty : context.Request["idlocation"].ToString());

                string lang = (context.Request["lang"] == null ? string.Empty : context.Request["lang"].ToString());
                string idpromotion = (context.Request["idpromotion"] == null ? string.Empty : context.Request["idpromotion"].ToString());
                string survey = (context.Request["survey"] == null ? string.Empty : context.Request["survey"].ToString());

                string sIP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
                string uri = "http://" + sIP + ":1111/usg/command.xml";

                //string uri = "http://192.168.10.16:1111/usg/command.xml";

                log.NSEId = string.Format("Nomadix - Idhotel:{0}", idhotel);
                log.CallerID = mac;
                log.Date = DateTime.Now.ToUniversalTime();
                log.UserName = mail.ToUpper();
                log.Password = mail.ToUpper();
                log.Page = "socialnetworkmegusta";
                log.IdLocation = Int32.Parse(idlocation);
                log.IdHotel = Int32.Parse(idhotel);
                log.RadiusResponse = string.Empty;
                log.NomadixResponse = string.Empty;

                //Hotels hotel = BillingController.GetHotel(nseid);
                //Locations location = BillingController.ObtainLocation(hotel.IdHotel, rn);
                FacebookPromotions promotion = null;
                if (!string.IsNullOrEmpty(idpromotion))
                    promotion = BillingController.GetFacebookPromotion(Int32.Parse(idpromotion));
                else
                {
                    List<FacebookPromotions> fbpromotions = BillingController.GetFacebookPromotions(Int32.Parse(idhotel), Int32.Parse(idlocation), true);
                    if (fbpromotions.Count() > 0)
                        promotion = fbpromotions[0];
                }

                FacebookPromotionsLog promotionLog = BillingController.GetFacebookPromotionLog(mail, promotion.IdFacebookPromotion);
                BillingTypes billingType = BillingController.ObtainBillingType(promotion.IdBillingType);
                Hotels hotel = BillingController.GetHotel(Int32.Parse(idhotel));
                Locations2 location = BillingController.ObtainLocation(Int32.Parse(idlocation));

                if (promotionLog.IdPromotionLog.Equals(0))
                {
                    //YA HABIA HECHO ME GUSTA PREVIAMENTE EN LA WEB
                    promotionLog.IdPromocion = promotion.IdFacebookPromotion;
                    promotionLog.Email = mail.ToUpper();
                    promotionLog.CreationDate = DateTime.Now.ToUniversalTime();
                    promotionLog.ValidTill = DateTime.Now.ToUniversalTime().AddDays(promotion.ValidDays);

                    BillingController.SavePromotionLog(promotionLog);

                }

                Rooms room = BillingController.GetRoom(Int32.Parse(idhotel), "DEFAULT");

                Users user = null;

                bool existe = false;

                if (mail.ToUpper().Equals("UNDEFINED") || string.IsNullOrEmpty(mail))  //EL USUARIO NO HA CONCEDIDO SU CORREO ELECTRÓNICO
                {
                    mail = string.Format("{0}{1}", "SN", util.createRandomUserNumber(8)).ToUpper(); //GENERAMOS UN USUARIO NUEVO
                    existe = true;

                    do
                    {
                        user = BillingController.GetRadiusUser(mail.ToUpper(), hotel.IdHotel);  //COMPROBAMOS QUE NO EXISTA EN LA BASE DE DATOS
                        if (user.IdUser.Equals(0))
                            existe = false; // SI NO EXISTE, CONTINUAMOS 
                        else
                        {
                            if (user.Enabled) // SI EXISTE Y ESTÁ ACTIVO, GENERAMOS UNO NUEVO Y EMPEZAMOS EL PROCESO DE NUEVO
                            {
                                mail = string.Format("{0}{1}", "SN", util.createRandomUserNumber(8)).ToUpper();
                            }
                            else
                            {//SI NO ESTÁ ACTIVO, CONTINUAMOS CON USUARIO GENERADO Y CREAMOS UN USUARIO NUEVO
                                existe = false;
                                user = new Users();
                            }
                        }

                    } while (existe);
                }
                else
                    user = BillingController.GetRadiusUser(mail.ToUpper(), hotel.IdHotel);

                if (user.IdUser.Equals(0))
                    user = new Users();

                user.IdHotel = Int32.Parse(idhotel);
                user.IdRoom = room.IdRoom;
                user.IdBillingType = billingType.IdBillingType;
                user.BWDown = billingType.BWDown;
                user.BWUp = billingType.BWUp;
                user.ValidSince = DateTime.Now.ToUniversalTime(); // bill.BillingDate;
                user.ValidTill = DateTime.Now.ToUniversalTime().AddHours(billingType.ValidTill); // bill.BillingDate.AddHours(billingType.ValidTill);
                user.TimeCredit = billingType.TimeCredit;
                user.MaxDevices = billingType.MaxDevices;
                user.Priority = billingType.IdPriority;
                user.VolumeDown = billingType.VolumeDown;
                user.VolumeUp = billingType.VolumeUp;
                user.Enabled = true;
                user.Name = mail.ToUpper();
                user.Password = mail.ToUpper();
                user.CHKO = true;
                user.BuyingFrom = 1;
                user.BuyingCallerID = mac;

                Loyalties2 loyalty = new Loyalties2();
                loyalty.IdHotel = user.IdHotel;
                loyalty.Name = string.Empty;
                loyalty.Surname = string.Empty;
                loyalty.Email = mail;
                loyalty.Date = DateTime.Now.ToUniversalTime();
                loyalty.Survey = survey;

                BillingController.SaveLoyalty(loyalty);

                BillingController.SaveUser(user);

                if (promotionLog.ValidTill > DateTime.Now.ToUniversalTime())
                {
                    XmlDocument xml = CrearXML(user.Name.ToUpper(), user.Password.ToUpper(), mac);

                    DateTime timeXMLResponse = DateTime.Now.ToUniversalTime();
                    Thread.Sleep(1000);
                    XmlDocument xmlResponse = PostXMLTransaction(uri, xml);

                    foreach (XmlAttribute att in xmlResponse.DocumentElement.Attributes)
                    {
                        if (att.Name.Equals("RESULT"))
                        {
                            if (att.Value.Equals("OK"))
                            {
                                log.NomadixResponse = "OK";

                                DateTime time = DateTime.Now.ToUniversalTime();
                                DateTime aux = time.AddSeconds(30);

                                do
                                {
                                    //COMPROBAMOS QUE HAYA UNA SESION ACTIVA CON LA MAC , SI ES CORRECTA, DEVOLVEMOS UN OK
                                    ActiveSessions session = BillingController.GetActiveSession(mac, user.Name.ToUpper());
                                    if (!string.IsNullOrEmpty(session.AcctSessionID))
                                    {
                                        response.IdUser = 1;
                                        response.UserName = user.Name;
                                        response.Password = user.Password;
                                        response.ValidTill = user.ValidTill.AddHours(hotel.GMT).ToString("dd/MM/yyyy HH:mm:ss");

                                        if (string.IsNullOrEmpty(billingType.UrlLanding))
                                        {
                                            if (string.IsNullOrEmpty(location.UrlLanding))
                                                response.Url = hotel.UrlHotel;
                                            else
                                                response.Url = location.UrlLanding;
                                        }
                                        else
                                        {
                                            response.Url = billingType.UrlLanding;
                                        }

                                        log.RadiusResponse = "OK";

                                        util.sendMailRoot(user.IdHotel, billingType.IdBillingType, mail, user.Name, user.Password, lang.Substring(0, 2));

                                        break;
                                    }
                                    else // SI NO HAY SESSION ACTIVA ES PORQUE HA FALLADO EL LOGIN, VAMOS A LA TABLA DE FAILED REQUEST Y MOSTRAMOS EL ERROR INDICADO POR EL RADIUS
                                    {
                                        FailedRequests failed = BillingController.GetFailedRequest(mac, user.Name.ToUpper(), timeXMLResponse);
                                        if (!failed.ID.Equals(0))
                                        {
                                            response.IdUser = 0;
                                            response.UserName = failed.ReplyMessage;
                                            log.RadiusResponse = failed.ReplyMessage;
                                            break;
                                        }
                                        else
                                        { // SI TAMPOCO HAY ERROR, ES PORQUE EL RADIUS HA RECHAZADO EL PAQUETE Y MOSTRAMOS UN ERROR DESCONOCIDO.
                                            response.IdUser = -3;
                                            log.RadiusResponse += "ERROR - USER IS NOT IN RADIUS TABLES - TRAZA " + time.ToString() + Environment.NewLine;
                                        }
                                    }
                                    Thread.Sleep(1000);
                                    time = time.AddSeconds(1);
                                } while (time < aux);

                                //VALIDAMOS EL USERAGENT
                                UserAgentsTemp temp = BillingController.GetUserAgentTemp(mac);
                                if (temp.IdTemp != 0)
                                {
                                    UserAgents userAgent = BillingController.GetUserAgent(temp.IdUserAgent);
                                    List<UserAgents> listUA = BillingController.GetUserAgents(userAgent.String);

                                    userAgent.Valid = userAgent.Valid + 1; ;
                                    BillingController.SaveUserAgent(userAgent);
                                    foreach (UserAgents x in listUA)
                                    {
                                        x.Valid = x.Valid + 1; ;
                                        BillingController.SaveUserAgent(x);
                                    }

                                    //BillingController.DeleteUserAgentTemp(temp);
                                }

                                //METEMOS LOS USERAGENTS EN LA LISTA NEGRA

                                List<UserAgents> uAtoBlackList = BillingController.GetUserAgentsToBlackList(Int32.Parse(ConfigurationManager.AppSettings["MaxAttemps"].ToString()));
                                foreach (UserAgents uA in uAtoBlackList)
                                {
                                    if (uA.Valid.Equals(0))
                                    {
                                        BlackListUserAgents bUA = BillingController.GetBlackUserAgent(uA.String);
                                        if (bUA.IdBlackUserAgent.Equals(0))
                                        {
                                            BlackListUserAgents obj = new BlackListUserAgents();
                                            obj.String = uA.String;
                                            BillingController.SaveBlackListUserAgent(obj);
                                        }
                                    }
                                }
                            }
                            else if (att.Value.Equals("ERROR"))
                            {
                                response.IdUser = 0;
                                log.NomadixResponse = "ERROR EN PAQUETE DE NOMADIX - " + xmlResponse.InnerText;
                                log.RadiusResponse = "NOT APPLICABLE";
                            }

                        }

                    }
                }
                else
                {
                    response.IdUser = -1;
                    log.RadiusResponse = "USER NOR VALID, PROMOTION IS EXPIRED";
                }
            }
            catch (Exception ex)
            {
                if (ex.GetType().Name.ToUpper().Contains("URI"))
                {
                    log.NomadixResponse = "ERROR - NOMADIX NO COMUNICATIONS";
                    log.RadiusResponse = "NOT APPLICABLE";
                }
                else
                {
                    log.NomadixResponse = "OK";
                    log.RadiusResponse = "OK";

                    CaptivePortalLogInternal internalLog = new CaptivePortalLogInternal();
                    internalLog.NSEId = log.NSEId;
                    internalLog.CallerID = log.CallerID;
                    internalLog.Date = DateTime.Now.ToUniversalTime();
                    internalLog.UserName = "";
                    internalLog.Password = "";
                    internalLog.Page = "socialnetwork";
                    internalLog.IdLocation = 0;
                    internalLog.NomadixResponse = "ERROR NO CONTEMPLADO - " + ex.Message;
                    internalLog.RadiusResponse = "OK";

                    BillingController.SaveCaptivePortalLogInternal(internalLog);
                }

            }

            BillingController.SaveCaptivePortalLog(log);

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private void username(HttpContext context)
        {
            string json = string.Empty;
            try
            {
                string username = (context.Request["u"] == null ? string.Empty : context.Request["u"].ToString());
                string idhotel = (context.Request["idhotel"] == null ? string.Empty : context.Request["idhotel"].ToString());

                Hotels hotel = BillingController.GetHotel(Int32.Parse(idhotel));
                Users user = BillingController.GetRadiusUser(username, hotel.IdHotel);

                if (user.Enabled)
                    json = "{\"code\":\"ERROR\",\"message\":\"UserName not avalaible\"}";
                else
                    json = "{\"code\":\"OK\",\"message\":\"UserName it is free\"}";
            }
            catch (Exception ex)
            {
                json = "{\"code\":\"ERROR\",\"message\":\" " + ex.Message + ".\"}";
            }

            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            context.Response.End();
        }

        private static XmlDocument CrearXML(string username, string password, string mac)
        {
            try
            {
                XmlDocument documento = new XmlDocument();
                //CREAMOS LA DECLARACION DEL DOCUMENTO
                //XmlDeclaration xmldecl;
                //xmldecl = documento.CreateXmlDeclaration("1.0", null, null);
                //xmldecl.Encoding = "UTF-8";
                //xmldecl.Standalone = "yes";
                //documento.AppendChild(xmldecl);

                //CREAMOS EL NODO RAIZ
                XmlElement root = documento.CreateElement("USG");
                root.SetAttribute("COMMAND", "RADIUS_LOGIN");
                documento.AppendChild(root);


                //CREAMOS LOS ELEMENTOS DEL NODO
                XmlElement xmlusername = documento.CreateElement("SUB_USER_NAME");
                xmlusername.InnerText = username;
                root.AppendChild(xmlusername);

                XmlElement xmlpass = documento.CreateElement("SUB_PASSWORD");
                xmlpass.SetAttribute("ENCRYPT", "FALSE");
                xmlpass.InnerText = password;
                root.AppendChild(xmlpass);

                XmlElement xmlmac = documento.CreateElement("SUB_MAC_ADDR");
                xmlmac.InnerText = mac;
                root.AppendChild(xmlmac);

                return documento;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static XmlDocument PostXMLTransaction(string v_strURL, XmlDocument v_objXMLDoc)
        {
            //Declare XMLResponse document
            XmlDocument XMLResponse = null;

            //Declare an HTTP-specific implementation of the WebRequest class.
            HttpWebRequest objHttpWebRequest;

            //Declare an HTTP-specific implementation of the WebResponse class
            HttpWebResponse objHttpWebResponse = null;

            //Declare a generic view of a sequence of bytes
            Stream objRequestStream = null;
            Stream objResponseStream = null;

            //Declare XMLReader
            XmlTextReader objXMLReader;

            //Creates an HttpWebRequest for the specified URL.
            objHttpWebRequest = (HttpWebRequest)WebRequest.Create(v_strURL);

            try
            {
                //---------- Start HttpRequest 

                //Set HttpWebRequest properties
                byte[] bytes;
                bytes = System.Text.Encoding.ASCII.GetBytes(v_objXMLDoc.InnerXml);
                objHttpWebRequest.Method = "POST";
                objHttpWebRequest.ContentLength = bytes.Length;
                objHttpWebRequest.ContentType = "text/xml";

                //Get Stream object 
                objRequestStream = objHttpWebRequest.GetRequestStream();

                //Writes a sequence of bytes to the current stream 
                objRequestStream.Write(bytes, 0, bytes.Length);

                //Close stream
                objRequestStream.Close();

                //---------- End HttpRequest

                //Sends the HttpWebRequest, and waits for a response.
                objHttpWebResponse = (HttpWebResponse)objHttpWebRequest.GetResponse();

                //---------- Start HttpResponse
                if (objHttpWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    //Get response stream 
                    objResponseStream = objHttpWebResponse.GetResponseStream();

                    //Load response stream into XMLReader
                    objXMLReader = new XmlTextReader(objResponseStream);

                    //Declare XMLDocument
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.Load(objXMLReader);

                    //Set XMLResponse object returned from XMLReader
                    XMLResponse = xmldoc;

                    //Close XMLReader
                    objXMLReader.Close();
                }

                //Close HttpWebResponse
                objHttpWebResponse.Close();
            }
            catch (WebException we)
            {
                //TODO: Add custom exception handling
                throw new Exception(we.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                //Close connections
                objRequestStream.Close();
                objResponseStream.Close();
                objHttpWebResponse.Close();

                //Release objects
                objXMLReader = null;
                objRequestStream = null;
                objResponseStream = null;
                objHttpWebResponse = null;
                objHttpWebRequest = null;
            }

            //Return
            return XMLResponse;
        }

        private string Cunef(int idhotel, string username, string password, string mac)
        {
            CaptivePortalLog log = new CaptivePortalLog();
            try
            {
                log.UserName = username;
                log.Password = password;
                log.NSEId = "CUNEF";
                log.CallerID = "CUNEF";
                log.Date = DateTime.Now.ToUniversalTime();
                log.Page = "Login - CustomCunef";
                log.IdHotel = idhotel;

                EncryptDLL.Encrypt encryter = new EncryptDLL.Encrypt();
                CWS.GEAPortalServiceClient service = new CWS.GEAPortalServiceClient();
                CWS.LogeoCunefRespuesta response = service.LogadoCunef(encryter.Encriptar(username, password));

                if (response.Acceso.Equals(1))
                {
                    Users userExits = BillingController.GetRadiusUser(username, idhotel);
                    if (userExits.IdUser.Equals(0) || !userExits.Enabled)
                    {
                        string bt = string.Empty;
                        switch (response.Rol)
                        {
                            case 1: bt = "ALUMNO"; break;
                            case 2: bt = "PROFESOR"; break;
                            case 3: bt = "PAS"; break;
                            case 4: bt = "INVITADO"; break;
                            case 5: bt = "VIP"; break;
                        }

                        List<BillingTypes> billingTypes = BillingController.ObtainBillingTypesModules(idhotel, 4);
                        BillingTypes type = null;

                        foreach (BillingTypes obj in billingTypes)
                        {
                            if (obj.Description.ToUpper().Equals(bt))
                            {
                                type = obj;
                                break;
                            }
                        }

                        if (type != null)
                        {

                            Rooms room = BillingController.GetRoom(idhotel, "DEFAULT");

                            Users user = new Users();
                            user.IdHotel = idhotel;
                            user.IdRoom = room.IdRoom;
                            user.IdBillingType = type.IdBillingType;
                            user.BWDown = type.BWDown;
                            user.BWUp = type.BWUp;
                            user.ValidSince = DateTime.Now.ToUniversalTime(); // bill.BillingDate;
                            user.ValidTill = DateTime.Now.ToUniversalTime().AddHours(type.ValidTill); // bill.BillingDate.AddHours(billingType.ValidTill);
                            user.TimeCredit = type.TimeCredit;
                            user.MaxDevices = type.MaxDevices;
                            user.Priority = type.IdPriority;
                            user.VolumeDown = type.VolumeDown;
                            user.VolumeUp = type.VolumeUp;
                            user.Enabled = true;
                            user.Name = username.ToUpper();
                            user.Password = password.ToUpper();
                            user.CHKO = true;
                            user.BuyingFrom = 1;
                            user.BuyingCallerID = mac;

                            BillingController.SaveUser(user);

                            return "allow";

                        }
                        else
                        {
                            log.NomadixResponse = "No hay tipo de acceso asociado a este rol";
                            log.RadiusResponse = "No hay tipo de acceso asociado a este rol";
                            BillingController.SaveCaptivePortalLog(log);

                            return log.NomadixResponse;

                        }
                    }
                    else
                    {
                        userExits.Password = password.ToUpper();
                        BillingController.SaveUser(userExits);
                        log.NomadixResponse = "El usuario existe y está válido, Se actualiza en la base de datos";
                        log.RadiusResponse = "El usuario existe y está válido, Se actualiza en la base de datos";
                        BillingController.SaveCaptivePortalLog(log);



                        return "allow";
                    }
                }
                else
                {
                    log.NomadixResponse = response.Mensaje;
                    log.RadiusResponse = response.Mensaje;
                    BillingController.SaveCaptivePortalLog(log);
                    return response.Mensaje;
                }
            }

            catch (Exception ex)
            {

                log.NomadixResponse = ex.Message;
                log.RadiusResponse = ex.Message;
                BillingController.SaveCaptivePortalLog(log);

                return ex.Message;
            }
        }

        public void ProcessEmtpy(HttpContext context)
        {
            // Set the content type and encoding for JSON
            context.Response.ContentType = "application/json";
            context.Response.ContentEncoding = Encoding.UTF8;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}