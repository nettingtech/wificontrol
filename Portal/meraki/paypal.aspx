﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="paypal.aspx.cs" Inherits="Portal.meraki.paypal" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
 <title></title>
    <link href="../resources/css/bootstrap-theme.css" rel="stylesheet" />
    <link href="../resources/css/bootstrap.css" rel="stylesheet" />
    <script src="../resources/scripts/jquery.js"></script>
    <script src="../resources/scripts/bootstrap.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <script type="text/javascript">
        function getParameters() {
            var searchString = window.location.search.substring(1)
                , params = searchString.split("&")
                , hash = {}
            ;

            for (var i = 0; i < params.length; i++) {
                var val = params[i].split("=");
                hash[unescape(val[0])] = unescape(val[1]);
            }
            return hash;
        }

        $(document).ready(function () {
            var parameters = getParameters();

            if (parameters.lang == 'es') {
                $("#submit").attr('value', 'Aceptar y continuar');
                $("#alert").html('<p style="text-align:center; font-size:40px">ATENCIÓN</p><p>Su conexión va a ser ahora transferida a la página de PayPal para procesar el pago. Por favor, siga las instrucciones y <b>NO CIERRE EL NAVEGADOR (o la pestaña)</b> hasta que el pago se haya completado y se devuelva la conexión a este asistente.</p>');
            }

            $("#submit").click(function () {
                document.forms["payForm"].submit();
                var parameters = getParameters();

                if (parameters.lang == 'es') {
                    $("#submit").attr('value', 'Espere, conectando con Paypal');
                }
                else {
                    $("#submit").attr('value', 'Wait, connecting to PayPal');
                }
            });
        });
    </script>
</head>
<body>
    <form method="post" id="payForm" action= "https://www.paypal.com/cgi-bin/webscr">
        <input type="hidden" name="cmd" value="_xclick">
        <input type="hidden" name="business" value="<%Response.Write (PayPalUser);%>">
        <input type="hidden" name="item_name" value="<%Response.Write (item_name);%>">
        <input type="hidden" name="item_number" value="<%Response.Write(idtransaction);%>">
        <input type="hidden" name="amount" value="<%Response.Write (amount);%>">
        <input type="hidden" name="no_shipping" value="1">
        <input type="hidden" name="rm" value="2">
        <input type="hidden" name="notify_url" value="<%Response.Write(notify_url);%>">
        <input type="hidden" name="return" id="return" value="<%Response.Write(return_url);%>">
        <input type="hidden" name="cancel_return" value="<%Response.Write(error_url);%>">
        <input type="hidden" name="currency_code" value="<%Response.Write(currency_code);%>">
        <input type="hidden" name="custom" value="<%Response.Write(idtransaction);%>">
    </form>

    <div class="container">
        <div class="row" id="principal">
            <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="panel panel-warning">
                <div id="alert" class="panel panel-body text-center" >
                    <p style="font-size:40px">WARNING</p><p> Your connection will now be transferred to the PayPal website for payment processing. Please follow the instructions and <b>DO NOT CLOSE YOUR BROWSER (or browser tab)</b> until the payment is complete and you are redirected back to this wizard.</p>
                </div>
            </div>

            <input type="button" id="submit" value="Accept & Continue" class="btn btn-primary btn-block" />
        </div>

    </div>
    </div>
</body>
</html>
