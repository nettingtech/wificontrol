﻿<%@ Page Title="" Language="C#" MasterPageFile="~/resources/master/PortalMaster.Master" AutoEventWireup="true" CodeBehind="daylypass.aspx.cs" Inherits="Portal.daylypass" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script>

    $(document).ready(function (e) {

        $("#logo").attr("src", "/resources/images/" + $("#idhotel").val() + "/" + $("#idlocation").val() + "-logo.png");

        var lang = navigator.browserLanguage ? navigator.browserLanguage : navigator.language;
        var langcode = lang.substring(0, 2);

        if ((langcode != 'es') && (langcode != 'en'))
            langcode = $("#lang").val();

        if (langcode != 'es') {
            $("#sitepasstext").html("This network is only for our customers. Please enter the site's password below:");
        }
        else {
            $("#sitepasstext").html("Esta red es para uso exclusivo de nuestros clientes. Por favor introduzca la contraseña del sitio:");
            $("#sitepasslabel").html("Contraseña del sitio");
            $("#sitepassvalue").attr("placeholder", "Contraseña del sitio");
            $("#Continuebutton").html("Continuar");
        }

        $("#Continuebutton").click(function (e) {
            var dataStringm = 'action=CHECKDAYPASS&daylypass=' + $("#sitepassvalue").val() + '&mac=' + $("#mac").val() + '&idhotel=' + $("#idhotel").val() + '&idlocation=' + $("#idlocation").val();
            $.ajax({
                url: "resources/handlers/PortalHandler.ashx",
                data: dataStringm,
                contentType: "application/json; charset=utf-8",
                dataType: "text",
                beforeSend: function () {
                    $("#MyModalWait").modal('show');
                },
                complete: function () {
                    $("#MyModalWait").modal('hide');
                },
                success: function (pResult) {
                    var lResult = JSON.parse(pResult);
                    if (lResult.code == 'OK') {
                        var url = "default.aspx";
                        window.location.href = url;
                    }
                    else if (lResult.code == 'KO') {
                        if (langcode != 'es') {
                            $("#message").html("Incorrect password.");
                        }
                        else {
                            $("#message").html("Contraseña incorrecta.");
                        }
                        $("#myModalError").modal('show');
                    }

                    else {
                        if (langcode != 'es') {
                            $("#message").html("We are currently experiencing a problem, please try again later.");
                        }
                        else {
                            $("#message").html("Tenemos un problema temporal, intentelo de nuevo más tarde.");
                        }
                        $("#myModalError").modal('show');

                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#message2").html("We have a problem, please try again later.");
                    alert('error: ' + xhr.statusText);
                }
            });
        });

    });
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row" id="principal">
            <div class="col-xs-12 col-md-8 col-lg-8 col-lg-offset-2 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                         <p class="text-center"> <img src="#" id="logo" style="width:150px;" /></p>
                         <p class="text-center" id="sitepasstext"></p>
                            <div role="form">
                                <div class="form-group text-center">
                                <label for="username" id="sitepasslabel">Site Password</label>
                                <input type="text" class="form-control" id="sitepassvalue" placeholder="Site Password"/>
                                </div>
                               <button type="button" class="btn btn-primary btn-lg btn-block" id="Continuebutton">Continue</button>
                            </div>    
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModalError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title text-center" id="H2">Error</h4>
          </div>
          <div class="modal-body">
            <div id="message" class="alert alert-danger text-center"></div> 
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" id="errorbutton" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
</asp:Content>
