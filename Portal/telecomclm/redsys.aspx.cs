﻿using RedsysAPIPrj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Wifi360.Data.Controllers;
using Wifi360.Data.Model;

namespace Portal.telecomclm
{
    public partial class redsys : System.Web.UI.Page
    {
        public string Action = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            string amount = string.Empty;
            string id = string.Empty;
            if (Request.Params["amount"] != null)
                amount = Request.Params["amount"];

            if (Request.Params["IdTransaction"] != null)
                id = Request.Params["IdTransaction"];

            string version = "HMAC_SHA256_V1";

            // New instance of RedysAPI
            RedsysAPI r = new RedsysAPI();

            var trans = "0";
            string fuc = string.Empty;
            string tipoMoneda = string.Empty;
            string idTerminal = string.Empty;
            string url = string.Empty;
            string urlOK = string.Empty;
            string urlKO = string.Empty;

            string key = string.Empty;

            string idhotel = (Request["idhotel"] == null ? string.Empty : Request["idhotel"].ToString());
            HttpCookie cookie = new HttpCookie("mikorik-cmt");
            cookie.Value = idhotel;
            cookie.Expires = DateTime.Now.AddHours(1);

            Response.Cookies.Add(cookie);

            Hotels hotel = BillingController.GetHotel(Int32.Parse(idhotel));
            List<ParametrosConfiguracion> parametros = BillingController.GetParametrosConfiguracion(hotel.IdHotel);
            foreach (ParametrosConfiguracion config in parametros)
            {
                switch (config.Key.ToUpper())
                {
                    case "FUC": fuc = config.value; break;
                    case "TIPOMONEDA": tipoMoneda = config.value; break;
                    case "IDTERMINAL": idTerminal = config.value; break;
                    case "URL": url = config.value; break;
                    case "URLOK": urlOK = config.value; break;
                    case "URLKO": urlKO = config.value; break;
                    case "ACTION": Action = config.value; break;
                    case "KEY": key = config.value; break;
                }
            }

            string[] values = amount.Split('.');

            // Fill Ds_MerchantParameters parameters
            r.SetParameter("DS_MERCHANT_AMOUNT", string.Format("{0}{1}", values[0], values[1]));
            r.SetParameter("DS_MERCHANT_ORDER", id);
            r.SetParameter("DS_MERCHANT_MERCHANTCODE", fuc);
            r.SetParameter("DS_MERCHANT_CURRENCY", tipoMoneda);
            r.SetParameter("DS_MERCHANT_TRANSACTIONTYPE", trans);
            r.SetParameter("DS_MERCHANT_TERMINAL", idTerminal);
            r.SetParameter("DS_MERCHANT_MERCHANTURL", url);
            r.SetParameter("DS_MERCHANT_URLOK", urlOK);
            r.SetParameter("DS_MERCHANT_URLKO", urlKO);

            // Calculate Ds_SignatureVersion
            Ds_SignatureVersion.Value = version;
            // Calculate Ds_MerchantParameters
            string parms = r.createMerchantParameters();
            Ds_MerchantParameters.Value = parms;

            // Calculate Ds_Signature
            string sig = r.createMerchantSignature(key);
            Ds_Signature.Value = sig;

            payForm.Action = Action;

        }
    }
}