﻿<%@ Page Title="" Language="C#" MasterPageFile="~/resources/master/WanupMaster.Master" AutoEventWireup="true" CodeBehind="facebook.aspx.cs" Inherits="Portal.wanup.facebook" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
        <script type="text/javascript">

            $(document).ready(function () {
                var cookie = getCookie("mikrotik");

                values = cookie.split('&');

                $("#idhotel").val(values[0].split('=')[1]);
                $("#idlocation").val(values[1].split('=')[1]);
                $("#mac").val(values[2].split('=')[1]);
                $("#origin").val(values[3].split('=')[1]);

                var lang = navigator.browserLanguage ? navigator.browserLanguage : navigator.language;
                var langcode = lang.substring(0, 2);
                if ((langcode != 'es') && (langcode != 'en'))
                    langcode = $("#languagecode").val();

                var parameters = getParameters();
                var dataString = 'action=fullsite&idhotel=' + $("#idhotel").val() + "&idlocation=" + $("#idlocation").val() + "&lang=" + langcode;
                $.ajax({
                    url: "../resources/handlers/WanupHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    beforeSend: function () {
                        $("#MyModalWait").modal('show');
                    },
                    complete: function () {
                        $("#MyModalWait").modal('hide');
                    },
                    success: function (pReturn) {
                        var lReturn = JSON.parse(pReturn);

                        window.fbAsyncInit = function () {
                            FB.init({
                                appId: '233231503535149',
                                status: true,
                                cookie: true,
                                xfbml: true,
                                oauth: true,
                                version: 'v2.11'

                            });

                            FB.getLoginStatus(function (response) {
                                if (response.status === 'connected')
                                    FB.api('/me', { fields: 'first_name, last_name,email, gender, birthday' }, function (response) {
                                        $("#email").val(response.email);
                                        var survey = 'facebook: nombre=' + response.first_name + '|' + 'apellidos=' + response.last_name + '|id=' + response.id + "|";
                                        if (response.gender != undefined) {
                                            survey += 'sexo=' + response.gender + '|';
                                        }
                                        if (response.birthday != undefined) {
                                            survey += 'nacimiento=' + response.birthday + '|';
                                        }

                                        survey += 'language= ' + navigator.language + '|';

                                        survey += "likes:";
                                        if (lang.substring(0, 2) == 'es') {
                                            $("#userInformation").html("");
                                            $("#userInformation").append("<h3>Información del Usuario</h3>");
                                            $("#userInformation").append("Email: <b>" + response.email + "</b><br />");
                                            $("#userInformation").append("Nombre: <b>" + response.first_name + "</b><br />");
                                            $("#userInformation").append("Apellidos: <b>" + response.last_name + "</b><br />");
                                            $("#finishbutton").val("Continuar navegando");
                                        }
                                        else {
                                            $("#userInformation").html("");
                                            $("#userInformation").append("<h3>User Information </h3>");
                                            $("#userInformation").append("Email: <b>" + response.email + "</b><br />");
                                            $("#userInformation").append("First Name: <b>" + response.first_name + "</b><br />");
                                            $("#userInformation").append("Last Name: <b>" + response.last_name + "</b><br />");
                                            $("#finishbutton").val("Continue browsing");

                                        }

                                        var dataStringm = 'action=facebookaccess&email=' + response.email + '&mac=' + $("#mac").val() + '&idhotel=' + $("#idhotel").val() + '&idlocation=' + $("#idlocation").val() + '&origin=' + $("#origin").val() + '&survey=' + survey + "&lang=" + langcode;
                                        $.ajax({
                                            url: "../resources/handlers/WanupHandler.ashx",
                                            data: dataStringm,
                                            contentType: "application/json; charset=utf-8",
                                            dataType: "text",
                                            beforeSend: function () {
                                                $("#MyModalWait").modal('show');
                                            },
                                            complete: function () {
                                                $("#MyModalWait").modal('hide');
                                            },
                                            success: function (pResult) {
                                                var lResult = JSON.parse(pResult);
                                                if (lResult.IdUser == -1) {
                                                    if (langcode != 'es') {
                                                        $("#message").html("This offer is no longer available.");
                                                    }
                                                    else {
                                                        $("#message").html("Esta promoción ya no está disponible.");
                                                    }
                                                    $("#myModalError").modal('show');
                                                }
                                                else if (lResult.IdUser == -3) {
                                                    if (langcode != 'es') {
                                                        $("#message").html("We are currently experiencing a problem, please try again later.");
                                                    }
                                                    else {
                                                        $("#message").html("Tenemos un problema temporal, intentelo de nuevo más tarde.");
                                                    }
                                                    $("#myModalError").modal('show');
                                                }
                                                else if (lResult.IdUser == 0) {
                                                    if (langcode != 'es') {
                                                        $("#message").html(lResult.UserName);
                                                    }
                                                    else {
                                                        $("#message").html(lResult.UserName);
                                                    }
                                                    $("#myModalError").modal('show');
                                                }
                                                else {
                                                    $("#finishbutton").attr("href", lResult.Url);
                                                    $("#finishInformation").append($("#socialaccessinformation").html());

                                                    if (langcode == 'es') {
                                                        $("#finishlabel").html("Bienvenido de nuevo " + response.first_name);
                                                        $("#finishInformation").append('<b>Nombre de usuario: </b>' + lResult.UserName + '<br />');
                                                        $("#finishInformation").append('<b>Contraseña: </b>' + lResult.Password + '<br />');
                                                        $("#finishInformation").append('<b>Fecha de expiración: </b>' + lResult.ValidTill + '<br />');
                                                        $("#finishbutton").val("Continuar navegando");
                                                    }
                                                    else {
                                                        $("#finishlabel").html("Welcome again " + response.first_name);
                                                        $("#finishInformation").append('<b>UserName: </b>' + lResult.UserName + '<br />');
                                                        $("#finishInformation").append('<b>Password: </b>' + lResult.Password + '<br />');
                                                        $("#finishInformation").append('<b>Valid Till: </b>' + lResult.ValidTill + '<br />');
                                                        $("#finishbutton").val("Continue browsing");
                                                    }

                                                    $('#myModalFinish').modal('show');
                                                }

                                            },
                                            error: function (xhr, ajaxOptions, thrownError) {
                                                $("#message2").html("We have a problem, please try again later.");
                                                alert('error: ' + xhr.statusText);
                                            }
                                        });
                                    });
                            }, { scope: 'email,public_profile,user_birthday,user_likes' });

                        };
                        (function (d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) { return; }
                            js = d.createElement(s); js.id = id;
                            js.src = "//connect.facebook.net/es_ES/sdk.js";
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#loading").html("We are currently experiencing a problem, please try again later.");
                        alert('error: ' + xhr.statusText);
                    }
                });

                $("#finishbutton").click(function (e) {
                    e.preventDefault();
                    var url = $("#finishbutton").attr("href");
                    window.location.href = url;

                });
            });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-wrapper">
        <input type="hidden" name="code" id="idhotel" value="" />
        <input type="hidden" name="code" id="idlocation" value="" />
        <input type="hidden" name="code" id="mac" value="" />
        <input type="hidden" name="code" id="origin" value="" />
        <div id="fb-root"></div>
    </div>
    <div class="modal fade" id="myModalError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title text-center" id="H2">Error</h4>
          </div>
          <div class="modal-body">
            <div id="message" class="alert alert-danger text-center"></div> 
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="errorbutton">Close</button>
          </div>
        </div>
      </div>
    </div>

        <div class="modal fade" id="myModalFinish" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title text-center" id="finishlabel">Congratulations</h4>
          </div>
          <div class="modal-body">
                <div style="text-align:center">
                        <svg class="logo-wanup" aria-hidden="true" width="113" height="33" alt="Wanup Logo">
                        <use id="logo-wanup" xlink:href="#icon-logo-wanup-color"></use>
                    </svg>
                </div>
                <div id="finishInformation" style="text-align:center" ><br /><br /></div>
                            <br />
                <input type="button" id="finishbutton" class="btn btn-success btn-block" value="Continue browsing" /> 
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="MyModalWait" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center" id="waitheader">Please, wait a moment</h4>
                </div>
                <div class="modal-body">
                     <div style="text-align:center; margin-bottom: 25px;">
                          <svg class="logo-wanup" aria-hidden="true" width="113" height="33" alt="Wanup Logo">
                            <use id="Use1" xlink:href="#icon-logo-wanup-color"></use>
                        </svg>
                    </div>

                    <div class="progress">
                            <div class="progress-bar progress-bar-danger progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                <span class="sr-only">Loading - Waiting please</span>
                            </div>
                        </div>

                </div>
            </div>
        </div>
    </div>

        <div id="socialaccessinformation" style="display:none"></div>
        <input type="text" hidden="hidden" id="idsocialbillintype" />
        <input type="text" hidden="hidden" id="facebookPageID" />
        <input type="text" hidden="hidden" id="idpromotion" />
        <input type="text" hidden="hidden" id="surveytext" />
        <input type="text" hidden="hidden" id="email" />


</asp:Content>
