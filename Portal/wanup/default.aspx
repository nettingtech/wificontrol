﻿<%@ Page Title="" Language="C#" MasterPageFile="~/resources/master/WanupMaster.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Portal.wanup._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
        <script type="text/javascript">

            $(document).ready(function (e) {

                $("#modalWait").modal("show");

                var parameters = getParameters();
                var val = "idhotel=" + $("#idhotel").val() + "&idlocation=" + $("#idlocation").val() + "&mac=" + $("#mac").val() + "&origin=" + $("#origin").val();
                createCookie("mikrotik", val, 1);

                var legal = 'I accept the <a href="https://www.wanup.com/en/terms-and-conditions" target="_blank">Terms and Conditions</a> of Wanup’s membership, its Terms of Use and <a href="https://www.wanup.com/en/legal-notice" target="_blank">Privacy Policy</a>, its <a href="https://www.wanup.com/en/cookies" target="_blank">Cookies Policy</a> and the assignment of my personal data to the Hotel.';
                $("#conditionsterms").html(legal);

                var lang = navigator.browserLanguage ? navigator.browserLanguage : navigator.language;
                var langcode = lang.substring(0, 2);

                if ((langcode != 'es') && (langcode != 'en'))
                    langcode = $("#lang").val();

                if (langcode == 'es') {
                    //MODALES
                    $("#errorbutton").html("Cerrar");
                    $("#errorbutton2").html("Cerrar");
                    $("#languagelabel").html("Idioma");


                    //TEXTOS GENERALES
                    $("#sorrytext").html("Lo sentimos");
                    $("#waitheader").html("Espere un momento por favor");

                    $("#name").attr("placeholder", "Nombre*")
                    $("#surname").attr("placeholder", "Apellidos*")
                    $('#pais')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="" selected="selected">País de procedencia*</option>')
                        .append('		<option label="Afganistán" value="AF">Afganistán</option>')
                        .append('		<option label="Albania" value="AL">Albania</option>')
                        .append('		<option label="Alemania" value="DE">Alemania</option>')
                        .append('		<option label="Andorra" value="AD">Andorra</option>')
                        .append('		<option label="Angola" value="AO">Angola</option>')
                        .append('		<option label="Anguila" value="AI">Anguila</option>')
                        .append('		<option label="Antártida" value="AQ">Antártida</option>')
                        .append('		<option label="Antigua y Barbuda" value="AG">Antigua y Barbuda</option>')
                        .append('		<option label="Arabia Saudí" value="SA">Arabia Saudí</option>')
                        .append('		<option label="Argelia" value="DZ">Argelia</option>')
                        .append('		<option label="Argentina" value="AR">Argentina</option>')
                        .append('		<option label="Armenia" value="AM">Armenia</option>')
                        .append('		<option label="Aruba" value="AW">Aruba</option>')
                        .append('		<option label="Australia" value="AU">Australia</option>')
                        .append('		<option label="Austria" value="AT">Austria</option>')
                        .append('		<option label="Azerbaiyán" value="AZ">Azerbaiyán</option>')
                        .append('		<option label="Bahamas" value="BS">Bahamas</option>')
                        .append('		<option label="Bangladés" value="BD">Bangladés</option>')
                        .append('		<option label="Barbados" value="BB">Barbados</option>')
                        .append('		<option label="Baréin" value="BH">Baréin</option>')
                        .append('		<option label="Bélgica" value="BE">Bélgica</option>')
                        .append('		<option label="Belice" value="BZ">Belice</option>')
                        .append('		<option label="Benín" value="BJ">Benín</option>')
                        .append('		<option label="Bermudas" value="BM">Bermudas</option>')
                        .append('		<option label="Bielorrusia" value="BY">Bielorrusia</option>')
                        .append('		<option label="Bolivia" value="BO">Bolivia</option>')
                        .append('		<option label="Bosnia-Herzegovina" value="BA">Bosnia-Herzegovina</option>')
                        .append('		<option label="Botsuana" value="BW">Botsuana</option>')
                        .append('		<option label="Brasil" value="BR">Brasil</option>')
                        .append('		<option label="Brunéi" value="BN">Brunéi</option>')
                        .append('		<option label="Bulgaria" value="BG">Bulgaria</option>')
                        .append('		<option label="Burkina Faso" value="BF">Burkina Faso</option>')
                        .append('		<option label="Burundi" value="BI">Burundi</option>')
                        .append('		<option label="Bután" value="BT">Bután</option>')
                        .append('		<option label="Cabo Verde" value="CV">Cabo Verde</option>')
                        .append('		<option label="Camboya" value="KH">Camboya</option>')
                        .append('		<option label="Camerún" value="CM">Camerún</option>')
                        .append('		<option label="Canadá" value="CA">Canadá</option>')
                        .append('		<option label="Canarias" value="IC">Canarias</option>')
                        .append('		<option label="Caribe neerlandés" value="BQ">Caribe neerlandés</option>')
                        .append('		<option label="Catar" value="QA">Catar</option>')
                        .append('		<option label="Ceuta y Melilla" value="EA">Ceuta y Melilla</option>')
                        .append('		<option label="Chad" value="TD">Chad</option>')
                        .append('		<option label="Chile" value="CL">Chile</option>')
                        .append('		<option label="China" value="CN">China</option>')
                        .append('		<option label="Chipre" value="CY">Chipre</option>')
                        .append('		<option label="Ciudad del Vaticano" value="VA">Ciudad del Vaticano</option>')
                        .append('		<option label="Colombia" value="CO">Colombia</option>')
                        .append('		<option label="Comoras" value="KM">Comoras</option>')
                        .append('		<option label="Corea del Norte" value="KP">Corea del Norte</option>')
                        .append('		<option label="Corea del Sur" value="KR">Corea del Sur</option>')
                        .append('		<option label="Costa de Marfil" value="CI">Costa de Marfil</option>')
                        .append('		<option label="Costa Rica" value="CR">Costa Rica</option>')
                        .append('		<option label="Croacia" value="HR">Croacia</option>')
                        .append('		<option label="Cuba" value="CU">Cuba</option>')
                        .append('		<option label="Curazao" value="CW">Curazao</option>')
                        .append('		<option label="Diego García" value="DG">Diego García</option>')
                        .append('		<option label="Dinamarca" value="DK">Dinamarca</option>')
                        .append('		<option label="Dominica" value="DM">Dominica</option>')
                        .append('		<option label="Ecuador" value="EC">Ecuador</option>')
                        .append('		<option label="Egipto" value="EG">Egipto</option>')
                        .append('		<option label="El Salvador" value="SV">El Salvador</option>')
                        .append('		<option label="Emiratos Árabes Unidos" value="AE">Emiratos Árabes Unidos</option>')
                        .append('		<option label="Eritrea" value="ER">Eritrea</option>')
                        .append('		<option label="Eslovaquia" value="SK">Eslovaquia</option>')
                        .append('		<option label="Eslovenia" value="SI">Eslovenia</option>')
                        .append('		<option label="España" value="ES">España</option>')
                        .append('		<option label="Estados Unidos" value="US">Estados Unidos</option>')
                        .append('		<option label="Estonia" value="EE">Estonia</option>')
                        .append('		<option label="Etiopía" value="ET">Etiopía</option>')
                        .append('		<option label="Filipinas" value="PH">Filipinas</option>')
                        .append('		<option label="Finlandia" value="FI">Finlandia</option>')
                        .append('		<option label="Fiyi" value="FJ">Fiyi</option>')
                        .append('		<option label="Francia" value="FR">Francia</option>')
                        .append('		<option label="Gabón" value="GA">Gabón</option>')
                        .append('		<option label="Gambia" value="GM">Gambia</option>')
                        .append('		<option label="Georgia" value="GE">Georgia</option>')
                        .append('		<option label="Ghana" value="GH">Ghana</option>')
                        .append('		<option label="Gibraltar" value="GI">Gibraltar</option>')
                        .append('		<option label="Granada" value="GD">Granada</option>')
                        .append('		<option label="Grecia" value="GR">Grecia</option>')
                        .append('		<option label="Groenlandia" value="GL">Groenlandia</option>')
                        .append('		<option label="Guadalupe" value="GP">Guadalupe</option>')
                        .append('		<option label="Guam" value="GU">Guam</option>')
                        .append('		<option label="Guatemala" value="GT">Guatemala</option>')
                        .append('		<option label="Guayana Francesa" value="GF">Guayana Francesa</option>')
                        .append('		<option label="Guernesey" value="GG">Guernesey</option>')
                        .append('		<option label="Guinea" value="GN">Guinea</option>')
                        .append('		<option label="Guinea Ecuatorial" value="GQ">Guinea Ecuatorial</option>')
                        .append('		<option label="Guinea-Bisáu" value="GW">Guinea-Bisáu</option>')
                        .append('		<option label="Guyana" value="GY">Guyana</option>')
                        .append('		<option label="Haití" value="HT">Haití</option>')
                        .append('		<option label="Honduras" value="HN">Honduras</option>')
                        .append('		<option label="Hungría" value="HU">Hungría</option>')
                        .append('		<option label="India" value="IN">India</option>')
                        .append('		<option label="Indonesia" value="ID">Indonesia</option>')
                        .append('		<option label="Irak" value="IQ">Irak</option>')
                        .append('		<option label="Irán" value="IR">Irán</option>')
                        .append('		<option label="Irlanda" value="IE">Irlanda</option>')
                        .append('		<option label="Isla de la Ascensión" value="AC">Isla de la Ascensión</option>')
                        .append('		<option label="Isla de Man" value="IM">Isla de Man</option>')
                        .append('		<option label="Isla de Navidad" value="CX">Isla de Navidad</option>')
                        .append('		<option label="Isla Norfolk" value="NF">Isla Norfolk</option>')
                        .append('		<option label="Islandia" value="IS">Islandia</option>')
                        .append('		<option label="Islas Åland" value="AX">Islas Åland</option>')
                        .append('		<option label="Islas Caimán" value="KY">Islas Caimán</option>')
                        .append('		<option label="Islas Cocos" value="CC">Islas Cocos</option>')
                        .append('		<option label="Islas Cook" value="CK">Islas Cook</option>')
                        .append('		<option label="Islas Feroe" value="FO">Islas Feroe</option>')
                        .append('		<option label="Islas Georgia del Sur y Sandwich del Sur" value="GS">Islas Georgia del Sur y Sandwich del Sur</option>')
                        .append('		<option label="Islas Malvinas" value="FK">Islas Malvinas</option>')
                        .append('		<option label="Islas Marianas del Norte" value="MP">Islas Marianas del Norte</option>')
                        .append('		<option label="Islas Marshall" value="MH">Islas Marshall</option>')
                        .append('		<option label="Islas menores alejadas de EE. UU." value="UM">Islas menores alejadas de EE. UU.</option>')
                        .append('		<option label="Islas Pitcairn" value="PN">Islas Pitcairn</option>')
                        .append('		<option label="Islas Salomón" value="SB">Islas Salomón</option>')
                        .append('		<option label="Islas Turcas y Caicos" value="TC">Islas Turcas y Caicos</option>')
                        .append('		<option label="Islas Vírgenes Británicas" value="VG">Islas Vírgenes Británicas</option>')
                        .append('		<option label="Islas Vírgenes de EE. UU." value="VI">Islas Vírgenes de EE. UU.</option>')
                        .append('		<option label="Israel" value="IL">Israel</option>')
                        .append('		<option label="Italia" value="IT">Italia</option>')
                        .append('		<option label="Jamaica" value="JM">Jamaica</option>')
                        .append('		<option label="Japón" value="JP">Japón</option>')
                        .append('		<option label="Jersey" value="JE">Jersey</option>')
                        .append('		<option label="Jordania" value="JO">Jordania</option>')
                        .append('		<option label="Kazajistán" value="KZ">Kazajistán</option>')
                        .append('		<option label="Kenia" value="KE">Kenia</option>')
                        .append('		<option label="Kirguistán" value="KG">Kirguistán</option>')
                        .append('		<option label="Kiribati" value="KI">Kiribati</option>')
                        .append('		<option label="Kosovo" value="XK">Kosovo</option>')
                        .append('		<option label="Kuwait" value="KW">Kuwait</option>')
                        .append('		<option label="Laos" value="LA">Laos</option>')
                        .append('		<option label="Lesoto" value="LS">Lesoto</option>')
                        .append('		<option label="Letonia" value="LV">Letonia</option>')
                        .append('		<option label="Líbano" value="LB">Líbano</option>')
                        .append('		<option label="Liberia" value="LR">Liberia</option>')
                        .append('		<option label="Libia" value="LY">Libia</option>')
                        .append('		<option label="Liechtenstein" value="LI">Liechtenstein</option>')
                        .append('		<option label="Lituania" value="LT">Lituania</option>')
                        .append('		<option label="Luxemburgo" value="LU">Luxemburgo</option>')
                        .append('		<option label="Macedonia" value="MK">Macedonia</option>')
                        .append('		<option label="Madagascar" value="MG">Madagascar</option>')
                        .append('		<option label="Malasia" value="MY">Malasia</option>')
                        .append('		<option label="Malaui" value="MW">Malaui</option>')
                        .append('		<option label="Maldivas" value="MV">Maldivas</option>')
                        .append('		<option label="Mali" value="ML">Mali</option>')
                        .append('		<option label="Malta" value="MT">Malta</option>')
                        .append('		<option label="Marruecos" value="MA">Marruecos</option>')
                        .append('		<option label="Martinica" value="MQ">Martinica</option>')
                        .append('		<option label="Mauricio" value="MU">Mauricio</option>')
                        .append('		<option label="Mauritania" value="MR">Mauritania</option>')
                        .append('		<option label="Mayotte" value="YT">Mayotte</option>')
                        .append('		<option label="México" value="MX">México</option>')
                        .append('		<option label="Micronesia" value="FM">Micronesia</option>')
                        .append('		<option label="Moldavia" value="MD">Moldavia</option>')
                        .append('		<option label="Mónaco" value="MC">Mónaco</option>')
                        .append('		<option label="Mongolia" value="MN">Mongolia</option>')
                        .append('		<option label="Montenegro" value="ME">Montenegro</option>')
                        .append('		<option label="Montserrat" value="MS">Montserrat</option>')
                        .append('		<option label="Mozambique" value="MZ">Mozambique</option>')
                        .append('		<option label="Myanmar (Birmania)" value="MM">Myanmar (Birmania)</option>')
                        .append('		<option label="Namibia" value="NA">Namibia</option>')
                        .append('		<option label="Nauru" value="NR">Nauru</option>')
                        .append('		<option label="Nepal" value="NP">Nepal</option>')
                        .append('		<option label="Nicaragua" value="NI">Nicaragua</option>')
                        .append('		<option label="Níger" value="NE">Níger</option>')
                        .append('		<option label="Nigeria" value="NG">Nigeria</option>')
                        .append('		<option label="Niue" value="NU">Niue</option>')
                        .append('		<option label="Noruega" value="NO">Noruega</option>')
                        .append('		<option label="Nueva Caledonia" value="NC">Nueva Caledonia</option>')
                        .append('		<option label="Nueva Zelanda" value="NZ">Nueva Zelanda</option>')
                        .append('		<option label="Omán" value="OM">Omán</option>')
                        .append('		<option label="Países Bajos" value="NL">Países Bajos</option>')
                        .append('		<option label="Pakistán" value="PK">Pakistán</option>')
                        .append('		<option label="Palaos" value="PW">Palaos</option>')
                        .append('		<option label="Panamá" value="PA">Panamá</option>')
                        .append('		<option label="Papúa Nueva Guinea" value="PG">Papúa Nueva Guinea</option>')
                        .append('		<option label="Paraguay" value="PY">Paraguay</option>')
                        .append('		<option label="Perú" value="PE">Perú</option>')
                        .append('		<option label="Polinesia Francesa" value="PF">Polinesia Francesa</option>')
                        .append('		<option label="Polonia" value="PL">Polonia</option>')
                        .append('		<option label="Portugal" value="PT">Portugal</option>')
                        .append('		<option label="Puerto Rico" value="PR">Puerto Rico</option>')
                        .append('		<option label="RAE de Hong Kong (China)" value="HK">RAE de Hong Kong (China)</option>')
                        .append('		<option label="RAE de Macao (China)" value="MO">RAE de Macao (China)</option>')
                        .append('		<option label="Reino Unido" value="GB">Reino Unido</option>')
                        .append('		<option label="República Centroafricana" value="CF">República Centroafricana</option>')
                        .append('		<option label="República Checa" value="CZ">República Checa</option>')
                        .append('		<option label="República del Congo" value="CG">República del Congo</option>')
                        .append('		<option label="República Democrática del Congo" value="CD">República Democrática del Congo</option>')
                        .append('		<option label="República Dominicana" value="DO">República Dominicana</option>')
                        .append('		<option label="Reunión" value="RE">Reunión</option>')
                        .append('		<option label="Ruanda" value="RW">Ruanda</option>')
                        .append('		<option label="Rumanía" value="RO">Rumanía</option>')
                        .append('		<option label="Rusia" value="RU">Rusia</option>')
                        .append('		<option label="Sáhara Occidental" value="EH">Sáhara Occidental</option>')
                        .append('		<option label="Samoa" value="WS">Samoa</option>')
                        .append('		<option label="Samoa Americana" value="AS">Samoa Americana</option>')
                        .append('		<option label="San Bartolomé" value="BL">San Bartolomé</option>')
                        .append('		<option label="San Cristóbal y Nieves" value="KN">San Cristóbal y Nieves</option>')
                        .append('		<option label="San Marino" value="SM">San Marino</option>')
                        .append('		<option label="San Martín" value="MF">San Martín</option>')
                        .append('		<option label="San Pedro y Miquelón" value="PM">San Pedro y Miquelón</option>')
                        .append('		<option label="San Vicente y las Granadinas" value="VC">San Vicente y las Granadinas</option>')
                        .append('		<option label="Santa Elena" value="SH">Santa Elena</option>')
                        .append('		<option label="Santa Lucía" value="LC">Santa Lucía</option>')
                        .append('		<option label="Santo Tomé y Príncipe" value="ST">Santo Tomé y Príncipe</option>')
                        .append('		<option label="Senegal" value="SN">Senegal</option>')
                        .append('		<option label="Serbia" value="RS">Serbia</option>')
                        .append('		<option label="Seychelles" value="SC">Seychelles</option>')
                        .append('		<option label="Sierra Leona" value="SL">Sierra Leona</option>')
                        .append('		<option label="Singapur" value="SG">Singapur</option>')
                        .append('		<option label="Sint Maarten" value="SX">Sint Maarten</option>')
                        .append('		<option label="Siria" value="SY">Siria</option>')
                        .append('		<option label="Somalia" value="SO">Somalia</option>')
                        .append('		<option label="Sri Lanka" value="LK">Sri Lanka</option>')
                        .append('		<option label="Suazilandia" value="SZ">Suazilandia</option>')
                        .append('		<option label="Sudáfrica" value="ZA">Sudáfrica</option>')
                        .append('		<option label="Sudán" value="SD">Sudán</option>')
                        .append('		<option label="Sudán del Sur" value="SS">Sudán del Sur</option>')
                        .append('		<option label="Suecia" value="SE">Suecia</option>')
                        .append('		<option label="Suiza" value="CH">Suiza</option>')
                        .append('		<option label="Surinam" value="SR">Surinam</option>')
                        .append('		<option label="Svalbard y Jan Mayen" value="SJ">Svalbard y Jan Mayen</option>')
                        .append('		<option label="Tailandia" value="TH">Tailandia</option>')
                        .append('		<option label="Taiwán" value="TW">Taiwán</option>')
                        .append('		<option label="Tanzania" value="TZ">Tanzania</option>')
                        .append('		<option label="Tayikistán" value="TJ">Tayikistán</option>')
                        .append('		<option label="Territorio Británico del Océano Índico" value="IO">Territorio Británico del Océano Índico</option>')
                        .append('		<option label="Territorios Australes Franceses" value="TF">Territorios Australes Franceses</option>')
                        .append('		<option label="Territorios Palestinos" value="PS">Territorios Palestinos</option>')
                        .append('		<option label="Timor Oriental" value="TL">Timor Oriental</option>')
                        .append('		<option label="Togo" value="TG">Togo</option>')
                        .append('		<option label="Tokelau" value="TK">Tokelau</option>')
                        .append('		<option label="Tonga" value="TO">Tonga</option>')
                        .append('		<option label="Trinidad y Tobago" value="TT">Trinidad y Tobago</option>')
                        .append('		<option label="Tristán da Cunha" value="TA">Tristán da Cunha</option>')
                        .append('		<option label="Túnez" value="TN">Túnez</option>')
                        .append('		<option label="Turkmenistán" value="TM">Turkmenistán</option>')
                        .append('		<option label="Turquía" value="TR">Turquía</option>')
                        .append('		<option label="Tuvalu" value="TV">Tuvalu</option>')
                        .append('		<option label="Ucrania" value="UA">Ucrania</option>')
                        .append('		<option label="Uganda" value="UG">Uganda</option>')
                        .append('		<option label="Uruguay" value="UY">Uruguay</option>')
                        .append('		<option label="Uzbekistán" value="UZ">Uzbekistán</option>')
                        .append('		<option label="Vanuatu" value="VU">Vanuatu</option>')
                        .append('		<option label="Venezuela" value="VE">Venezuela</option>')
                        .append('		<option label="Vietnam" value="VN">Vietnam</option>')
                        .append('		<option label="Wallis y Futuna" value="WF">Wallis y Futuna</option>')
                        .append('		<option label="Yemen" value="YE">Yemen</option>')
                        .append('		<option label="Yibuti" value="DJ">Yibuti</option>')
                        .append('		<option label="Zambia" value="ZM">Zambia</option>')
                        .append('		<option label="Zimbabue" value="ZW">Zimbabue</option><option value="" selected="selected">País de procedencia*</option>')
                        .append('		<option label="Afganistán" value="AF">Afganistán</option>')
                        .append('		<option label="Albania" value="AL">Albania</option>')
                        .append('		<option label="Alemania" value="DE">Alemania</option>')
                        .append('		<option label="Andorra" value="AD">Andorra</option>')
                        .append('		<option label="Angola" value="AO">Angola</option>')
                        .append('		<option label="Anguila" value="AI">Anguila</option>')
                        .append('		<option label="Antártida" value="AQ">Antártida</option>')
                        .append('		<option label="Antigua y Barbuda" value="AG">Antigua y Barbuda</option>')
                        .append('		<option label="Arabia Saudí" value="SA">Arabia Saudí</option>')
                        .append('		<option label="Argelia" value="DZ">Argelia</option>')
                        .append('		<option label="Argentina" value="AR">Argentina</option>')
                        .append('		<option label="Armenia" value="AM">Armenia</option>')
                        .append('		<option label="Aruba" value="AW">Aruba</option>')
                        .append('		<option label="Australia" value="AU">Australia</option>')
                        .append('		<option label="Austria" value="AT">Austria</option>')
                        .append('		<option label="Azerbaiyán" value="AZ">Azerbaiyán</option>')
                        .append('		<option label="Bahamas" value="BS">Bahamas</option>')
                        .append('		<option label="Bangladés" value="BD">Bangladés</option>')
                        .append('		<option label="Barbados" value="BB">Barbados</option>')
                        .append('		<option label="Baréin" value="BH">Baréin</option>')
                        .append('		<option label="Bélgica" value="BE">Bélgica</option>')
                        .append('		<option label="Belice" value="BZ">Belice</option>')
                        .append('		<option label="Benín" value="BJ">Benín</option>')
                        .append('		<option label="Bermudas" value="BM">Bermudas</option>')
                        .append('		<option label="Bielorrusia" value="BY">Bielorrusia</option>')
                        .append('		<option label="Bolivia" value="BO">Bolivia</option>')
                        .append('		<option label="Bosnia-Herzegovina" value="BA">Bosnia-Herzegovina</option>')
                        .append('		<option label="Botsuana" value="BW">Botsuana</option>')
                        .append('		<option label="Brasil" value="BR">Brasil</option>')
                        .append('		<option label="Brunéi" value="BN">Brunéi</option>')
                        .append('		<option label="Bulgaria" value="BG">Bulgaria</option>')
                        .append('		<option label="Burkina Faso" value="BF">Burkina Faso</option>')
                        .append('		<option label="Burundi" value="BI">Burundi</option>')
                        .append('		<option label="Bután" value="BT">Bután</option>')
                        .append('		<option label="Cabo Verde" value="CV">Cabo Verde</option>')
                        .append('		<option label="Camboya" value="KH">Camboya</option>')
                        .append('		<option label="Camerún" value="CM">Camerún</option>')
                        .append('		<option label="Canadá" value="CA">Canadá</option>')
                        .append('		<option label="Canarias" value="IC">Canarias</option>')
                        .append('		<option label="Caribe neerlandés" value="BQ">Caribe neerlandés</option>')
                        .append('		<option label="Catar" value="QA">Catar</option>')
                        .append('		<option label="Ceuta y Melilla" value="EA">Ceuta y Melilla</option>')
                        .append('		<option label="Chad" value="TD">Chad</option>')
                        .append('		<option label="Chile" value="CL">Chile</option>')
                        .append('		<option label="China" value="CN">China</option>')
                        .append('		<option label="Chipre" value="CY">Chipre</option>')
                        .append('		<option label="Ciudad del Vaticano" value="VA">Ciudad del Vaticano</option>')
                        .append('		<option label="Colombia" value="CO">Colombia</option>')
                        .append('		<option label="Comoras" value="KM">Comoras</option>')
                        .append('		<option label="Corea del Norte" value="KP">Corea del Norte</option>')
                        .append('		<option label="Corea del Sur" value="KR">Corea del Sur</option>')
                        .append('		<option label="Costa de Marfil" value="CI">Costa de Marfil</option>')
                        .append('		<option label="Costa Rica" value="CR">Costa Rica</option>')
                        .append('		<option label="Croacia" value="HR">Croacia</option>')
                        .append('		<option label="Cuba" value="CU">Cuba</option>')
                        .append('		<option label="Curazao" value="CW">Curazao</option>')
                        .append('		<option label="Diego García" value="DG">Diego García</option>')
                        .append('		<option label="Dinamarca" value="DK">Dinamarca</option>')
                        .append('		<option label="Dominica" value="DM">Dominica</option>')
                        .append('		<option label="Ecuador" value="EC">Ecuador</option>')
                        .append('		<option label="Egipto" value="EG">Egipto</option>')
                        .append('		<option label="El Salvador" value="SV">El Salvador</option>')
                        .append('		<option label="Emiratos Árabes Unidos" value="AE">Emiratos Árabes Unidos</option>')
                        .append('		<option label="Eritrea" value="ER">Eritrea</option>')
                        .append('		<option label="Eslovaquia" value="SK">Eslovaquia</option>')
                        .append('		<option label="Eslovenia" value="SI">Eslovenia</option>')
                        .append('		<option label="España" value="ES">España</option>')
                        .append('		<option label="Estados Unidos" value="US">Estados Unidos</option>')
                        .append('		<option label="Estonia" value="EE">Estonia</option>')
                        .append('		<option label="Etiopía" value="ET">Etiopía</option>')
                        .append('		<option label="Filipinas" value="PH">Filipinas</option>')
                        .append('		<option label="Finlandia" value="FI">Finlandia</option>')
                        .append('		<option label="Fiyi" value="FJ">Fiyi</option>')
                        .append('		<option label="Francia" value="FR">Francia</option>')
                        .append('		<option label="Gabón" value="GA">Gabón</option>')
                        .append('		<option label="Gambia" value="GM">Gambia</option>')
                        .append('		<option label="Georgia" value="GE">Georgia</option>')
                        .append('		<option label="Ghana" value="GH">Ghana</option>')
                        .append('		<option label="Gibraltar" value="GI">Gibraltar</option>')
                        .append('		<option label="Granada" value="GD">Granada</option>')
                        .append('		<option label="Grecia" value="GR">Grecia</option>')
                        .append('		<option label="Groenlandia" value="GL">Groenlandia</option>')
                        .append('		<option label="Guadalupe" value="GP">Guadalupe</option>')
                        .append('		<option label="Guam" value="GU">Guam</option>')
                        .append('		<option label="Guatemala" value="GT">Guatemala</option>')
                        .append('		<option label="Guayana Francesa" value="GF">Guayana Francesa</option>')
                        .append('		<option label="Guernesey" value="GG">Guernesey</option>')
                        .append('		<option label="Guinea" value="GN">Guinea</option>')
                        .append('		<option label="Guinea Ecuatorial" value="GQ">Guinea Ecuatorial</option>')
                        .append('		<option label="Guinea-Bisáu" value="GW">Guinea-Bisáu</option>')
                        .append('		<option label="Guyana" value="GY">Guyana</option>')
                        .append('		<option label="Haití" value="HT">Haití</option>')
                        .append('		<option label="Honduras" value="HN">Honduras</option>')
                        .append('		<option label="Hungría" value="HU">Hungría</option>')
                        .append('		<option label="India" value="IN">India</option>')
                        .append('		<option label="Indonesia" value="ID">Indonesia</option>')
                        .append('		<option label="Irak" value="IQ">Irak</option>')
                        .append('		<option label="Irán" value="IR">Irán</option>')
                        .append('		<option label="Irlanda" value="IE">Irlanda</option>')
                        .append('		<option label="Isla de la Ascensión" value="AC">Isla de la Ascensión</option>')
                        .append('		<option label="Isla de Man" value="IM">Isla de Man</option>')
                        .append('		<option label="Isla de Navidad" value="CX">Isla de Navidad</option>')
                        .append('		<option label="Isla Norfolk" value="NF">Isla Norfolk</option>')
                        .append('		<option label="Islandia" value="IS">Islandia</option>')
                        .append('		<option label="Islas Åland" value="AX">Islas Åland</option>')
                        .append('		<option label="Islas Caimán" value="KY">Islas Caimán</option>')
                        .append('		<option label="Islas Cocos" value="CC">Islas Cocos</option>')
                        .append('		<option label="Islas Cook" value="CK">Islas Cook</option>')
                        .append('		<option label="Islas Feroe" value="FO">Islas Feroe</option>')
                        .append('		<option label="Islas Georgia del Sur y Sandwich del Sur" value="GS">Islas Georgia del Sur y Sandwich del Sur</option>')
                        .append('		<option label="Islas Malvinas" value="FK">Islas Malvinas</option>')
                        .append('		<option label="Islas Marianas del Norte" value="MP">Islas Marianas del Norte</option>')
                        .append('		<option label="Islas Marshall" value="MH">Islas Marshall</option>')
                        .append('		<option label="Islas menores alejadas de EE. UU." value="UM">Islas menores alejadas de EE. UU.</option>')
                        .append('		<option label="Islas Pitcairn" value="PN">Islas Pitcairn</option>')
                        .append('		<option label="Islas Salomón" value="SB">Islas Salomón</option>')
                        .append('		<option label="Islas Turcas y Caicos" value="TC">Islas Turcas y Caicos</option>')
                        .append('		<option label="Islas Vírgenes Británicas" value="VG">Islas Vírgenes Británicas</option>')
                        .append('		<option label="Islas Vírgenes de EE. UU." value="VI">Islas Vírgenes de EE. UU.</option>')
                        .append('		<option label="Israel" value="IL">Israel</option>')
                        .append('		<option label="Italia" value="IT">Italia</option>')
                        .append('		<option label="Jamaica" value="JM">Jamaica</option>')
                        .append('		<option label="Japón" value="JP">Japón</option>')
                        .append('		<option label="Jersey" value="JE">Jersey</option>')
                        .append('		<option label="Jordania" value="JO">Jordania</option>')
                        .append('		<option label="Kazajistán" value="KZ">Kazajistán</option>')
                        .append('		<option label="Kenia" value="KE">Kenia</option>')
                        .append('		<option label="Kirguistán" value="KG">Kirguistán</option>')
                        .append('		<option label="Kiribati" value="KI">Kiribati</option>')
                        .append('		<option label="Kosovo" value="XK">Kosovo</option>')
                        .append('		<option label="Kuwait" value="KW">Kuwait</option>')
                        .append('		<option label="Laos" value="LA">Laos</option>')
                        .append('		<option label="Lesoto" value="LS">Lesoto</option>')
                        .append('		<option label="Letonia" value="LV">Letonia</option>')
                        .append('		<option label="Líbano" value="LB">Líbano</option>')
                        .append('		<option label="Liberia" value="LR">Liberia</option>')
                        .append('		<option label="Libia" value="LY">Libia</option>')
                        .append('		<option label="Liechtenstein" value="LI">Liechtenstein</option>')
                        .append('		<option label="Lituania" value="LT">Lituania</option>')
                        .append('		<option label="Luxemburgo" value="LU">Luxemburgo</option>')
                        .append('		<option label="Macedonia" value="MK">Macedonia</option>')
                        .append('		<option label="Madagascar" value="MG">Madagascar</option>')
                        .append('		<option label="Malasia" value="MY">Malasia</option>')
                        .append('		<option label="Malaui" value="MW">Malaui</option>')
                        .append('		<option label="Maldivas" value="MV">Maldivas</option>')
                        .append('		<option label="Mali" value="ML">Mali</option>')
                        .append('		<option label="Malta" value="MT">Malta</option>')
                        .append('		<option label="Marruecos" value="MA">Marruecos</option>')
                        .append('		<option label="Martinica" value="MQ">Martinica</option>')
                        .append('		<option label="Mauricio" value="MU">Mauricio</option>')
                        .append('		<option label="Mauritania" value="MR">Mauritania</option>')
                        .append('		<option label="Mayotte" value="YT">Mayotte</option>')
                        .append('		<option label="México" value="MX">México</option>')
                        .append('		<option label="Micronesia" value="FM">Micronesia</option>')
                        .append('		<option label="Moldavia" value="MD">Moldavia</option>')
                        .append('		<option label="Mónaco" value="MC">Mónaco</option>')
                        .append('		<option label="Mongolia" value="MN">Mongolia</option>')
                        .append('		<option label="Montenegro" value="ME">Montenegro</option>')
                        .append('		<option label="Montserrat" value="MS">Montserrat</option>')
                        .append('		<option label="Mozambique" value="MZ">Mozambique</option>')
                        .append('		<option label="Myanmar (Birmania)" value="MM">Myanmar (Birmania)</option>')
                        .append('		<option label="Namibia" value="NA">Namibia</option>')
                        .append('		<option label="Nauru" value="NR">Nauru</option>')
                        .append('		<option label="Nepal" value="NP">Nepal</option>')
                        .append('		<option label="Nicaragua" value="NI">Nicaragua</option>')
                        .append('		<option label="Níger" value="NE">Níger</option>')
                        .append('		<option label="Nigeria" value="NG">Nigeria</option>')
                        .append('		<option label="Niue" value="NU">Niue</option>')
                        .append('		<option label="Noruega" value="NO">Noruega</option>')
                        .append('		<option label="Nueva Caledonia" value="NC">Nueva Caledonia</option>')
                        .append('		<option label="Nueva Zelanda" value="NZ">Nueva Zelanda</option>')
                        .append('		<option label="Omán" value="OM">Omán</option>')
                        .append('		<option label="Países Bajos" value="NL">Países Bajos</option>')
                        .append('		<option label="Pakistán" value="PK">Pakistán</option>')
                        .append('		<option label="Palaos" value="PW">Palaos</option>')
                        .append('		<option label="Panamá" value="PA">Panamá</option>')
                        .append('		<option label="Papúa Nueva Guinea" value="PG">Papúa Nueva Guinea</option>')
                        .append('		<option label="Paraguay" value="PY">Paraguay</option>')
                        .append('		<option label="Perú" value="PE">Perú</option>')
                        .append('		<option label="Polinesia Francesa" value="PF">Polinesia Francesa</option>')
                        .append('		<option label="Polonia" value="PL">Polonia</option>')
                        .append('		<option label="Portugal" value="PT">Portugal</option>')
                        .append('		<option label="Puerto Rico" value="PR">Puerto Rico</option>')
                        .append('		<option label="RAE de Hong Kong (China)" value="HK">RAE de Hong Kong (China)</option>')
                        .append('		<option label="RAE de Macao (China)" value="MO">RAE de Macao (China)</option>')
                        .append('		<option label="Reino Unido" value="GB">Reino Unido</option>')
                        .append('		<option label="República Centroafricana" value="CF">República Centroafricana</option>')
                        .append('		<option label="República Checa" value="CZ">República Checa</option>')
                        .append('		<option label="República del Congo" value="CG">República del Congo</option>')
                        .append('		<option label="República Democrática del Congo" value="CD">República Democrática del Congo</option>')
                        .append('		<option label="República Dominicana" value="DO">República Dominicana</option>')
                        .append('		<option label="Reunión" value="RE">Reunión</option>')
                        .append('		<option label="Ruanda" value="RW">Ruanda</option>')
                        .append('		<option label="Rumanía" value="RO">Rumanía</option>')
                        .append('		<option label="Rusia" value="RU">Rusia</option>')
                        .append('		<option label="Sáhara Occidental" value="EH">Sáhara Occidental</option>')
                        .append('		<option label="Samoa" value="WS">Samoa</option>')
                        .append('		<option label="Samoa Americana" value="AS">Samoa Americana</option>')
                        .append('		<option label="San Bartolomé" value="BL">San Bartolomé</option>')
                        .append('		<option label="San Cristóbal y Nieves" value="KN">San Cristóbal y Nieves</option>')
                        .append('		<option label="San Marino" value="SM">San Marino</option>')
                        .append('		<option label="San Martín" value="MF">San Martín</option>')
                        .append('		<option label="San Pedro y Miquelón" value="PM">San Pedro y Miquelón</option>')
                        .append('		<option label="San Vicente y las Granadinas" value="VC">San Vicente y las Granadinas</option>')
                        .append('		<option label="Santa Elena" value="SH">Santa Elena</option>')
                        .append('		<option label="Santa Lucía" value="LC">Santa Lucía</option>')
                        .append('		<option label="Santo Tomé y Príncipe" value="ST">Santo Tomé y Príncipe</option>')
                        .append('		<option label="Senegal" value="SN">Senegal</option>')
                        .append('		<option label="Serbia" value="RS">Serbia</option>')
                        .append('		<option label="Seychelles" value="SC">Seychelles</option>')
                        .append('		<option label="Sierra Leona" value="SL">Sierra Leona</option>')
                        .append('		<option label="Singapur" value="SG">Singapur</option>')
                        .append('		<option label="Sint Maarten" value="SX">Sint Maarten</option>')
                        .append('		<option label="Siria" value="SY">Siria</option>')
                        .append('		<option label="Somalia" value="SO">Somalia</option>')
                        .append('		<option label="Sri Lanka" value="LK">Sri Lanka</option>')
                        .append('		<option label="Suazilandia" value="SZ">Suazilandia</option>')
                        .append('		<option label="Sudáfrica" value="ZA">Sudáfrica</option>')
                        .append('		<option label="Sudán" value="SD">Sudán</option>')
                        .append('		<option label="Sudán del Sur" value="SS">Sudán del Sur</option>')
                        .append('		<option label="Suecia" value="SE">Suecia</option>')
                        .append('		<option label="Suiza" value="CH">Suiza</option>')
                        .append('		<option label="Surinam" value="SR">Surinam</option>')
                        .append('		<option label="Svalbard y Jan Mayen" value="SJ">Svalbard y Jan Mayen</option>')
                        .append('		<option label="Tailandia" value="TH">Tailandia</option>')
                        .append('		<option label="Taiwán" value="TW">Taiwán</option>')
                        .append('		<option label="Tanzania" value="TZ">Tanzania</option>')
                        .append('		<option label="Tayikistán" value="TJ">Tayikistán</option>')
                        .append('		<option label="Territorio Británico del Océano Índico" value="IO">Territorio Británico del Océano Índico</option>')
                        .append('		<option label="Territorios Australes Franceses" value="TF">Territorios Australes Franceses</option>')
                        .append('		<option label="Territorios Palestinos" value="PS">Territorios Palestinos</option>')
                        .append('		<option label="Timor Oriental" value="TL">Timor Oriental</option>')
                        .append('		<option label="Togo" value="TG">Togo</option>')
                        .append('		<option label="Tokelau" value="TK">Tokelau</option>')
                        .append('		<option label="Tonga" value="TO">Tonga</option>')
                        .append('		<option label="Trinidad y Tobago" value="TT">Trinidad y Tobago</option>')
                        .append('		<option label="Tristán da Cunha" value="TA">Tristán da Cunha</option>')
                        .append('		<option label="Túnez" value="TN">Túnez</option>')
                        .append('		<option label="Turkmenistán" value="TM">Turkmenistán</option>')
                        .append('		<option label="Turquía" value="TR">Turquía</option>')
                        .append('		<option label="Tuvalu" value="TV">Tuvalu</option>')
                        .append('		<option label="Ucrania" value="UA">Ucrania</option>')
                        .append('		<option label="Uganda" value="UG">Uganda</option>')
                        .append('		<option label="Uruguay" value="UY">Uruguay</option>')
                        .append('		<option label="Uzbekistán" value="UZ">Uzbekistán</option>')
                        .append('		<option label="Vanuatu" value="VU">Vanuatu</option>')
                        .append('		<option label="Venezuela" value="VE">Venezuela</option>')
                        .append('		<option label="Vietnam" value="VN">Vietnam</option>')
                        .append('		<option label="Wallis y Futuna" value="WF">Wallis y Futuna</option>')
                        .append('		<option label="Yemen" value="YE">Yemen</option>')
                        .append('		<option label="Yibuti" value="DJ">Yibuti</option>')
                        .append('		<option label="Zambia" value="ZM">Zambia</option>')
                        .append('		<option label="Zimbabue" value="ZW">Zimbabue</option><option value="" selected="selected">País de procedencia*</option>')
                        .append('		<option label="Afganistán" value="AF">Afganistán</option>')
                        .append('		<option label="Albania" value="AL">Albania</option>')
                        .append('		<option label="Alemania" value="DE">Alemania</option>')
                        .append('		<option label="Andorra" value="AD">Andorra</option>')
                        .append('		<option label="Angola" value="AO">Angola</option>')
                        .append('		<option label="Anguila" value="AI">Anguila</option>')
                        .append('		<option label="Antártida" value="AQ">Antártida</option>')
                        .append('		<option label="Antigua y Barbuda" value="AG">Antigua y Barbuda</option>')
                        .append('		<option label="Arabia Saudí" value="SA">Arabia Saudí</option>')
                        .append('		<option label="Argelia" value="DZ">Argelia</option>')
                        .append('		<option label="Argentina" value="AR">Argentina</option>')
                        .append('		<option label="Armenia" value="AM">Armenia</option>')
                        .append('		<option label="Aruba" value="AW">Aruba</option>')
                        .append('		<option label="Australia" value="AU">Australia</option>')
                        .append('		<option label="Austria" value="AT">Austria</option>')
                        .append('		<option label="Azerbaiyán" value="AZ">Azerbaiyán</option>')
                        .append('		<option label="Bahamas" value="BS">Bahamas</option>')
                        .append('		<option label="Bangladés" value="BD">Bangladés</option>')
                        .append('		<option label="Barbados" value="BB">Barbados</option>')
                        .append('		<option label="Baréin" value="BH">Baréin</option>')
                        .append('		<option label="Bélgica" value="BE">Bélgica</option>')
                        .append('		<option label="Belice" value="BZ">Belice</option>')
                        .append('		<option label="Benín" value="BJ">Benín</option>')
                        .append('		<option label="Bermudas" value="BM">Bermudas</option>')
                        .append('		<option label="Bielorrusia" value="BY">Bielorrusia</option>')
                        .append('		<option label="Bolivia" value="BO">Bolivia</option>')
                        .append('		<option label="Bosnia-Herzegovina" value="BA">Bosnia-Herzegovina</option>')
                        .append('		<option label="Botsuana" value="BW">Botsuana</option>')
                        .append('		<option label="Brasil" value="BR">Brasil</option>')
                        .append('		<option label="Brunéi" value="BN">Brunéi</option>')
                        .append('		<option label="Bulgaria" value="BG">Bulgaria</option>')
                        .append('		<option label="Burkina Faso" value="BF">Burkina Faso</option>')
                        .append('		<option label="Burundi" value="BI">Burundi</option>')
                        .append('		<option label="Bután" value="BT">Bután</option>')
                        .append('		<option label="Cabo Verde" value="CV">Cabo Verde</option>')
                        .append('		<option label="Camboya" value="KH">Camboya</option>')
                        .append('		<option label="Camerún" value="CM">Camerún</option>')
                        .append('		<option label="Canadá" value="CA">Canadá</option>')
                        .append('		<option label="Canarias" value="IC">Canarias</option>')
                        .append('		<option label="Caribe neerlandés" value="BQ">Caribe neerlandés</option>')
                        .append('		<option label="Catar" value="QA">Catar</option>')
                        .append('		<option label="Ceuta y Melilla" value="EA">Ceuta y Melilla</option>')
                        .append('		<option label="Chad" value="TD">Chad</option>')
                        .append('		<option label="Chile" value="CL">Chile</option>')
                        .append('		<option label="China" value="CN">China</option>')
                        .append('		<option label="Chipre" value="CY">Chipre</option>')
                        .append('		<option label="Ciudad del Vaticano" value="VA">Ciudad del Vaticano</option>')
                        .append('		<option label="Colombia" value="CO">Colombia</option>')
                        .append('		<option label="Comoras" value="KM">Comoras</option>')
                        .append('		<option label="Corea del Norte" value="KP">Corea del Norte</option>')
                        .append('		<option label="Corea del Sur" value="KR">Corea del Sur</option>')
                        .append('		<option label="Costa de Marfil" value="CI">Costa de Marfil</option>')
                        .append('		<option label="Costa Rica" value="CR">Costa Rica</option>')
                        .append('		<option label="Croacia" value="HR">Croacia</option>')
                        .append('		<option label="Cuba" value="CU">Cuba</option>')
                        .append('		<option label="Curazao" value="CW">Curazao</option>')
                        .append('		<option label="Diego García" value="DG">Diego García</option>')
                        .append('		<option label="Dinamarca" value="DK">Dinamarca</option>')
                        .append('		<option label="Dominica" value="DM">Dominica</option>')
                        .append('		<option label="Ecuador" value="EC">Ecuador</option>')
                        .append('		<option label="Egipto" value="EG">Egipto</option>')
                        .append('		<option label="El Salvador" value="SV">El Salvador</option>')
                        .append('		<option label="Emiratos Árabes Unidos" value="AE">Emiratos Árabes Unidos</option>')
                        .append('		<option label="Eritrea" value="ER">Eritrea</option>')
                        .append('		<option label="Eslovaquia" value="SK">Eslovaquia</option>')
                        .append('		<option label="Eslovenia" value="SI">Eslovenia</option>')
                        .append('		<option label="España" value="ES">España</option>')
                        .append('		<option label="Estados Unidos" value="US">Estados Unidos</option>')
                        .append('		<option label="Estonia" value="EE">Estonia</option>')
                        .append('		<option label="Etiopía" value="ET">Etiopía</option>')
                        .append('		<option label="Filipinas" value="PH">Filipinas</option>')
                        .append('		<option label="Finlandia" value="FI">Finlandia</option>')
                        .append('		<option label="Fiyi" value="FJ">Fiyi</option>')
                        .append('		<option label="Francia" value="FR">Francia</option>')
                        .append('		<option label="Gabón" value="GA">Gabón</option>')
                        .append('		<option label="Gambia" value="GM">Gambia</option>')
                        .append('		<option label="Georgia" value="GE">Georgia</option>')
                        .append('		<option label="Ghana" value="GH">Ghana</option>')
                        .append('		<option label="Gibraltar" value="GI">Gibraltar</option>')
                        .append('		<option label="Granada" value="GD">Granada</option>')
                        .append('		<option label="Grecia" value="GR">Grecia</option>')
                        .append('		<option label="Groenlandia" value="GL">Groenlandia</option>')
                        .append('		<option label="Guadalupe" value="GP">Guadalupe</option>')
                        .append('		<option label="Guam" value="GU">Guam</option>')
                        .append('		<option label="Guatemala" value="GT">Guatemala</option>')
                        .append('		<option label="Guayana Francesa" value="GF">Guayana Francesa</option>')
                        .append('		<option label="Guernesey" value="GG">Guernesey</option>')
                        .append('		<option label="Guinea" value="GN">Guinea</option>')
                        .append('		<option label="Guinea Ecuatorial" value="GQ">Guinea Ecuatorial</option>')
                        .append('		<option label="Guinea-Bisáu" value="GW">Guinea-Bisáu</option>')
                        .append('		<option label="Guyana" value="GY">Guyana</option>')
                        .append('		<option label="Haití" value="HT">Haití</option>')
                        .append('		<option label="Honduras" value="HN">Honduras</option>')
                        .append('		<option label="Hungría" value="HU">Hungría</option>')
                        .append('		<option label="India" value="IN">India</option>')
                        .append('		<option label="Indonesia" value="ID">Indonesia</option>')
                        .append('		<option label="Irak" value="IQ">Irak</option>')
                        .append('		<option label="Irán" value="IR">Irán</option>')
                        .append('		<option label="Irlanda" value="IE">Irlanda</option>')
                        .append('		<option label="Isla de la Ascensión" value="AC">Isla de la Ascensión</option>')
                        .append('		<option label="Isla de Man" value="IM">Isla de Man</option>')
                        .append('		<option label="Isla de Navidad" value="CX">Isla de Navidad</option>')
                        .append('		<option label="Isla Norfolk" value="NF">Isla Norfolk</option>')
                        .append('		<option label="Islandia" value="IS">Islandia</option>')
                        .append('		<option label="Islas Åland" value="AX">Islas Åland</option>')
                        .append('		<option label="Islas Caimán" value="KY">Islas Caimán</option>')
                        .append('		<option label="Islas Cocos" value="CC">Islas Cocos</option>')
                        .append('		<option label="Islas Cook" value="CK">Islas Cook</option>')
                        .append('		<option label="Islas Feroe" value="FO">Islas Feroe</option>')
                        .append('		<option label="Islas Georgia del Sur y Sandwich del Sur" value="GS">Islas Georgia del Sur y Sandwich del Sur</option>')
                        .append('		<option label="Islas Malvinas" value="FK">Islas Malvinas</option>')
                        .append('		<option label="Islas Marianas del Norte" value="MP">Islas Marianas del Norte</option>')
                        .append('		<option label="Islas Marshall" value="MH">Islas Marshall</option>')
                        .append('		<option label="Islas menores alejadas de EE. UU." value="UM">Islas menores alejadas de EE. UU.</option>')
                        .append('		<option label="Islas Pitcairn" value="PN">Islas Pitcairn</option>')
                        .append('		<option label="Islas Salomón" value="SB">Islas Salomón</option>')
                        .append('		<option label="Islas Turcas y Caicos" value="TC">Islas Turcas y Caicos</option>')
                        .append('		<option label="Islas Vírgenes Británicas" value="VG">Islas Vírgenes Británicas</option>')
                        .append('		<option label="Islas Vírgenes de EE. UU." value="VI">Islas Vírgenes de EE. UU.</option>')
                        .append('		<option label="Israel" value="IL">Israel</option>')
                        .append('		<option label="Italia" value="IT">Italia</option>')
                        .append('		<option label="Jamaica" value="JM">Jamaica</option>')
                        .append('		<option label="Japón" value="JP">Japón</option>')
                        .append('		<option label="Jersey" value="JE">Jersey</option>')
                        .append('		<option label="Jordania" value="JO">Jordania</option>')
                        .append('		<option label="Kazajistán" value="KZ">Kazajistán</option>')
                        .append('		<option label="Kenia" value="KE">Kenia</option>')
                        .append('		<option label="Kirguistán" value="KG">Kirguistán</option>')
                        .append('		<option label="Kiribati" value="KI">Kiribati</option>')
                        .append('		<option label="Kosovo" value="XK">Kosovo</option>')
                        .append('		<option label="Kuwait" value="KW">Kuwait</option>')
                        .append('		<option label="Laos" value="LA">Laos</option>')
                        .append('		<option label="Lesoto" value="LS">Lesoto</option>')
                        .append('		<option label="Letonia" value="LV">Letonia</option>')
                        .append('		<option label="Líbano" value="LB">Líbano</option>')
                        .append('		<option label="Liberia" value="LR">Liberia</option>')
                        .append('		<option label="Libia" value="LY">Libia</option>')
                        .append('		<option label="Liechtenstein" value="LI">Liechtenstein</option>')
                        .append('		<option label="Lituania" value="LT">Lituania</option>')
                        .append('		<option label="Luxemburgo" value="LU">Luxemburgo</option>')
                        .append('		<option label="Macedonia" value="MK">Macedonia</option>')
                        .append('		<option label="Madagascar" value="MG">Madagascar</option>')
                        .append('		<option label="Malasia" value="MY">Malasia</option>')
                        .append('		<option label="Malaui" value="MW">Malaui</option>')
                        .append('		<option label="Maldivas" value="MV">Maldivas</option>')
                        .append('		<option label="Mali" value="ML">Mali</option>')
                        .append('		<option label="Malta" value="MT">Malta</option>')
                        .append('		<option label="Marruecos" value="MA">Marruecos</option>')
                        .append('		<option label="Martinica" value="MQ">Martinica</option>')
                        .append('		<option label="Mauricio" value="MU">Mauricio</option>')
                        .append('		<option label="Mauritania" value="MR">Mauritania</option>')
                        .append('		<option label="Mayotte" value="YT">Mayotte</option>')
                        .append('		<option label="México" value="MX">México</option>')
                        .append('		<option label="Micronesia" value="FM">Micronesia</option>')
                        .append('		<option label="Moldavia" value="MD">Moldavia</option>')
                        .append('		<option label="Mónaco" value="MC">Mónaco</option>')
                        .append('		<option label="Mongolia" value="MN">Mongolia</option>')
                        .append('		<option label="Montenegro" value="ME">Montenegro</option>')
                        .append('		<option label="Montserrat" value="MS">Montserrat</option>')
                        .append('		<option label="Mozambique" value="MZ">Mozambique</option>')
                        .append('		<option label="Myanmar (Birmania)" value="MM">Myanmar (Birmania)</option>')
                        .append('		<option label="Namibia" value="NA">Namibia</option>')
                        .append('		<option label="Nauru" value="NR">Nauru</option>')
                        .append('		<option label="Nepal" value="NP">Nepal</option>')
                        .append('		<option label="Nicaragua" value="NI">Nicaragua</option>')
                        .append('		<option label="Níger" value="NE">Níger</option>')
                        .append('		<option label="Nigeria" value="NG">Nigeria</option>')
                        .append('		<option label="Niue" value="NU">Niue</option>')
                        .append('		<option label="Noruega" value="NO">Noruega</option>')
                        .append('		<option label="Nueva Caledonia" value="NC">Nueva Caledonia</option>')
                        .append('		<option label="Nueva Zelanda" value="NZ">Nueva Zelanda</option>')
                        .append('		<option label="Omán" value="OM">Omán</option>')
                        .append('		<option label="Países Bajos" value="NL">Países Bajos</option>')
                        .append('		<option label="Pakistán" value="PK">Pakistán</option>')
                        .append('		<option label="Palaos" value="PW">Palaos</option>')
                        .append('		<option label="Panamá" value="PA">Panamá</option>')
                        .append('		<option label="Papúa Nueva Guinea" value="PG">Papúa Nueva Guinea</option>')
                        .append('		<option label="Paraguay" value="PY">Paraguay</option>')
                        .append('		<option label="Perú" value="PE">Perú</option>')
                        .append('		<option label="Polinesia Francesa" value="PF">Polinesia Francesa</option>')
                        .append('		<option label="Polonia" value="PL">Polonia</option>')
                        .append('		<option label="Portugal" value="PT">Portugal</option>')
                        .append('		<option label="Puerto Rico" value="PR">Puerto Rico</option>')
                        .append('		<option label="RAE de Hong Kong (China)" value="HK">RAE de Hong Kong (China)</option>')
                        .append('		<option label="RAE de Macao (China)" value="MO">RAE de Macao (China)</option>')
                        .append('		<option label="Reino Unido" value="GB">Reino Unido</option>')
                        .append('		<option label="República Centroafricana" value="CF">República Centroafricana</option>')
                        .append('		<option label="República Checa" value="CZ">República Checa</option>')
                        .append('		<option label="República del Congo" value="CG">República del Congo</option>')
                        .append('		<option label="República Democrática del Congo" value="CD">República Democrática del Congo</option>')
                        .append('		<option label="República Dominicana" value="DO">República Dominicana</option>')
                        .append('		<option label="Reunión" value="RE">Reunión</option>')
                        .append('		<option label="Ruanda" value="RW">Ruanda</option>')
                        .append('		<option label="Rumanía" value="RO">Rumanía</option>')
                        .append('		<option label="Rusia" value="RU">Rusia</option>')
                        .append('		<option label="Sáhara Occidental" value="EH">Sáhara Occidental</option>')
                        .append('		<option label="Samoa" value="WS">Samoa</option>')
                        .append('		<option label="Samoa Americana" value="AS">Samoa Americana</option>')
                        .append('		<option label="San Bartolomé" value="BL">San Bartolomé</option>')
                        .append('		<option label="San Cristóbal y Nieves" value="KN">San Cristóbal y Nieves</option>')
                        .append('		<option label="San Marino" value="SM">San Marino</option>')
                        .append('		<option label="San Martín" value="MF">San Martín</option>')
                        .append('		<option label="San Pedro y Miquelón" value="PM">San Pedro y Miquelón</option>')
                        .append('		<option label="San Vicente y las Granadinas" value="VC">San Vicente y las Granadinas</option>')
                        .append('		<option label="Santa Elena" value="SH">Santa Elena</option>')
                        .append('		<option label="Santa Lucía" value="LC">Santa Lucía</option>')
                        .append('		<option label="Santo Tomé y Príncipe" value="ST">Santo Tomé y Príncipe</option>')
                        .append('		<option label="Senegal" value="SN">Senegal</option>')
                        .append('		<option label="Serbia" value="RS">Serbia</option>')
                        .append('		<option label="Seychelles" value="SC">Seychelles</option>')
                        .append('		<option label="Sierra Leona" value="SL">Sierra Leona</option>')
                        .append('		<option label="Singapur" value="SG">Singapur</option>')
                        .append('		<option label="Sint Maarten" value="SX">Sint Maarten</option>')
                        .append('		<option label="Siria" value="SY">Siria</option>')
                        .append('		<option label="Somalia" value="SO">Somalia</option>')
                        .append('		<option label="Sri Lanka" value="LK">Sri Lanka</option>')
                        .append('		<option label="Suazilandia" value="SZ">Suazilandia</option>')
                        .append('		<option label="Sudáfrica" value="ZA">Sudáfrica</option>')
                        .append('		<option label="Sudán" value="SD">Sudán</option>')
                        .append('		<option label="Sudán del Sur" value="SS">Sudán del Sur</option>')
                        .append('		<option label="Suecia" value="SE">Suecia</option>')
                        .append('		<option label="Suiza" value="CH">Suiza</option>')
                        .append('		<option label="Surinam" value="SR">Surinam</option>')
                        .append('		<option label="Svalbard y Jan Mayen" value="SJ">Svalbard y Jan Mayen</option>')
                        .append('		<option label="Tailandia" value="TH">Tailandia</option>')
                        .append('		<option label="Taiwán" value="TW">Taiwán</option>')
                        .append('		<option label="Tanzania" value="TZ">Tanzania</option>')
                        .append('		<option label="Tayikistán" value="TJ">Tayikistán</option>')
                        .append('		<option label="Territorio Británico del Océano Índico" value="IO">Territorio Británico del Océano Índico</option>')
                        .append('		<option label="Territorios Australes Franceses" value="TF">Territorios Australes Franceses</option>')
                        .append('		<option label="Territorios Palestinos" value="PS">Territorios Palestinos</option>')
                        .append('		<option label="Timor Oriental" value="TL">Timor Oriental</option>')
                        .append('		<option label="Togo" value="TG">Togo</option>')
                        .append('		<option label="Tokelau" value="TK">Tokelau</option>')
                        .append('		<option label="Tonga" value="TO">Tonga</option>')
                        .append('		<option label="Trinidad y Tobago" value="TT">Trinidad y Tobago</option>')
                        .append('		<option label="Tristán da Cunha" value="TA">Tristán da Cunha</option>')
                        .append('		<option label="Túnez" value="TN">Túnez</option>')
                        .append('		<option label="Turkmenistán" value="TM">Turkmenistán</option>')
                        .append('		<option label="Turquía" value="TR">Turquía</option>')
                        .append('		<option label="Tuvalu" value="TV">Tuvalu</option>')
                        .append('		<option label="Ucrania" value="UA">Ucrania</option>')
                        .append('		<option label="Uganda" value="UG">Uganda</option>')
                        .append('		<option label="Uruguay" value="UY">Uruguay</option>')
                        .append('		<option label="Uzbekistán" value="UZ">Uzbekistán</option>')
                        .append('		<option label="Vanuatu" value="VU">Vanuatu</option>')
                        .append('		<option label="Venezuela" value="VE">Venezuela</option>')
                        .append('		<option label="Vietnam" value="VN">Vietnam</option>')
                        .append('		<option label="Wallis y Futuna" value="WF">Wallis y Futuna</option>')
                        .append('		<option label="Yemen" value="YE">Yemen</option>')
                        .append('		<option label="Yibuti" value="DJ">Yibuti</option>')
                        .append('		<option label="Zambia" value="ZM">Zambia</option>')
                        .append('		<option label="Zimbabue" value="ZW">Zimbabue</option><option value="" selected="selected">País de procedencia*</option>')
                        .append('		<option label="Afganistán" value="AF">Afganistán</option>')
                        .append('		<option label="Albania" value="AL">Albania</option>')
                        .append('		<option label="Alemania" value="DE">Alemania</option>')
                        .append('		<option label="Andorra" value="AD">Andorra</option>')
                        .append('		<option label="Angola" value="AO">Angola</option>')
                        .append('		<option label="Anguila" value="AI">Anguila</option>')
                        .append('		<option label="Antártida" value="AQ">Antártida</option>')
                        .append('		<option label="Antigua y Barbuda" value="AG">Antigua y Barbuda</option>')
                        .append('		<option label="Arabia Saudí" value="SA">Arabia Saudí</option>')
                        .append('		<option label="Argelia" value="DZ">Argelia</option>')
                        .append('		<option label="Argentina" value="AR">Argentina</option>')
                        .append('		<option label="Armenia" value="AM">Armenia</option>')
                        .append('		<option label="Aruba" value="AW">Aruba</option>')
                        .append('		<option label="Australia" value="AU">Australia</option>')
                        .append('		<option label="Austria" value="AT">Austria</option>')
                        .append('		<option label="Azerbaiyán" value="AZ">Azerbaiyán</option>')
                        .append('		<option label="Bahamas" value="BS">Bahamas</option>')
                        .append('		<option label="Bangladés" value="BD">Bangladés</option>')
                        .append('		<option label="Barbados" value="BB">Barbados</option>')
                        .append('		<option label="Baréin" value="BH">Baréin</option>')
                        .append('		<option label="Bélgica" value="BE">Bélgica</option>')
                        .append('		<option label="Belice" value="BZ">Belice</option>')
                        .append('		<option label="Benín" value="BJ">Benín</option>')
                        .append('		<option label="Bermudas" value="BM">Bermudas</option>')
                        .append('		<option label="Bielorrusia" value="BY">Bielorrusia</option>')
                        .append('		<option label="Bolivia" value="BO">Bolivia</option>')
                        .append('		<option label="Bosnia-Herzegovina" value="BA">Bosnia-Herzegovina</option>')
                        .append('		<option label="Botsuana" value="BW">Botsuana</option>')
                        .append('		<option label="Brasil" value="BR">Brasil</option>')
                        .append('		<option label="Brunéi" value="BN">Brunéi</option>')
                        .append('		<option label="Bulgaria" value="BG">Bulgaria</option>')
                        .append('		<option label="Burkina Faso" value="BF">Burkina Faso</option>')
                        .append('		<option label="Burundi" value="BI">Burundi</option>')
                        .append('		<option label="Bután" value="BT">Bután</option>')
                        .append('		<option label="Cabo Verde" value="CV">Cabo Verde</option>')
                        .append('		<option label="Camboya" value="KH">Camboya</option>')
                        .append('		<option label="Camerún" value="CM">Camerún</option>')
                        .append('		<option label="Canadá" value="CA">Canadá</option>')
                        .append('		<option label="Canarias" value="IC">Canarias</option>')
                        .append('		<option label="Caribe neerlandés" value="BQ">Caribe neerlandés</option>')
                        .append('		<option label="Catar" value="QA">Catar</option>')
                        .append('		<option label="Ceuta y Melilla" value="EA">Ceuta y Melilla</option>')
                        .append('		<option label="Chad" value="TD">Chad</option>')
                        .append('		<option label="Chile" value="CL">Chile</option>')
                        .append('		<option label="China" value="CN">China</option>')
                        .append('		<option label="Chipre" value="CY">Chipre</option>')
                        .append('		<option label="Ciudad del Vaticano" value="VA">Ciudad del Vaticano</option>')
                        .append('		<option label="Colombia" value="CO">Colombia</option>')
                        .append('		<option label="Comoras" value="KM">Comoras</option>')
                        .append('		<option label="Corea del Norte" value="KP">Corea del Norte</option>')
                        .append('		<option label="Corea del Sur" value="KR">Corea del Sur</option>')
                        .append('		<option label="Costa de Marfil" value="CI">Costa de Marfil</option>')
                        .append('		<option label="Costa Rica" value="CR">Costa Rica</option>')
                        .append('		<option label="Croacia" value="HR">Croacia</option>')
                        .append('		<option label="Cuba" value="CU">Cuba</option>')
                        .append('		<option label="Curazao" value="CW">Curazao</option>')
                        .append('		<option label="Diego García" value="DG">Diego García</option>')
                        .append('		<option label="Dinamarca" value="DK">Dinamarca</option>')
                        .append('		<option label="Dominica" value="DM">Dominica</option>')
                        .append('		<option label="Ecuador" value="EC">Ecuador</option>')
                        .append('		<option label="Egipto" value="EG">Egipto</option>')
                        .append('		<option label="El Salvador" value="SV">El Salvador</option>')
                        .append('		<option label="Emiratos Árabes Unidos" value="AE">Emiratos Árabes Unidos</option>')
                        .append('		<option label="Eritrea" value="ER">Eritrea</option>')
                        .append('		<option label="Eslovaquia" value="SK">Eslovaquia</option>')
                        .append('		<option label="Eslovenia" value="SI">Eslovenia</option>')
                        .append('		<option label="España" value="ES">España</option>')
                        .append('		<option label="Estados Unidos" value="US">Estados Unidos</option>')
                        .append('		<option label="Estonia" value="EE">Estonia</option>')
                        .append('		<option label="Etiopía" value="ET">Etiopía</option>')
                        .append('		<option label="Filipinas" value="PH">Filipinas</option>')
                        .append('		<option label="Finlandia" value="FI">Finlandia</option>')
                        .append('		<option label="Fiyi" value="FJ">Fiyi</option>')
                        .append('		<option label="Francia" value="FR">Francia</option>')
                        .append('		<option label="Gabón" value="GA">Gabón</option>')
                        .append('		<option label="Gambia" value="GM">Gambia</option>')
                        .append('		<option label="Georgia" value="GE">Georgia</option>')
                        .append('		<option label="Ghana" value="GH">Ghana</option>')
                        .append('		<option label="Gibraltar" value="GI">Gibraltar</option>')
                        .append('		<option label="Granada" value="GD">Granada</option>')
                        .append('		<option label="Grecia" value="GR">Grecia</option>')
                        .append('		<option label="Groenlandia" value="GL">Groenlandia</option>')
                        .append('		<option label="Guadalupe" value="GP">Guadalupe</option>')
                        .append('		<option label="Guam" value="GU">Guam</option>')
                        .append('		<option label="Guatemala" value="GT">Guatemala</option>')
                        .append('		<option label="Guayana Francesa" value="GF">Guayana Francesa</option>')
                        .append('		<option label="Guernesey" value="GG">Guernesey</option>')
                        .append('		<option label="Guinea" value="GN">Guinea</option>')
                        .append('		<option label="Guinea Ecuatorial" value="GQ">Guinea Ecuatorial</option>')
                        .append('		<option label="Guinea-Bisáu" value="GW">Guinea-Bisáu</option>')
                        .append('		<option label="Guyana" value="GY">Guyana</option>')
                        .append('		<option label="Haití" value="HT">Haití</option>')
                        .append('		<option label="Honduras" value="HN">Honduras</option>')
                        .append('		<option label="Hungría" value="HU">Hungría</option>')
                        .append('		<option label="India" value="IN">India</option>')
                        .append('		<option label="Indonesia" value="ID">Indonesia</option>')
                        .append('		<option label="Irak" value="IQ">Irak</option>')
                        .append('		<option label="Irán" value="IR">Irán</option>')
                        .append('		<option label="Irlanda" value="IE">Irlanda</option>')
                        .append('		<option label="Isla de la Ascensión" value="AC">Isla de la Ascensión</option>')
                        .append('		<option label="Isla de Man" value="IM">Isla de Man</option>')
                        .append('		<option label="Isla de Navidad" value="CX">Isla de Navidad</option>')
                        .append('		<option label="Isla Norfolk" value="NF">Isla Norfolk</option>')
                        .append('		<option label="Islandia" value="IS">Islandia</option>')
                        .append('		<option label="Islas Åland" value="AX">Islas Åland</option>')
                        .append('		<option label="Islas Caimán" value="KY">Islas Caimán</option>')
                        .append('		<option label="Islas Cocos" value="CC">Islas Cocos</option>')
                        .append('		<option label="Islas Cook" value="CK">Islas Cook</option>')
                        .append('		<option label="Islas Feroe" value="FO">Islas Feroe</option>')
                        .append('		<option label="Islas Georgia del Sur y Sandwich del Sur" value="GS">Islas Georgia del Sur y Sandwich del Sur</option>')
                        .append('		<option label="Islas Malvinas" value="FK">Islas Malvinas</option>')
                        .append('		<option label="Islas Marianas del Norte" value="MP">Islas Marianas del Norte</option>')
                        .append('		<option label="Islas Marshall" value="MH">Islas Marshall</option>')
                        .append('		<option label="Islas menores alejadas de EE. UU." value="UM">Islas menores alejadas de EE. UU.</option>')
                        .append('		<option label="Islas Pitcairn" value="PN">Islas Pitcairn</option>')
                        .append('		<option label="Islas Salomón" value="SB">Islas Salomón</option>')
                        .append('		<option label="Islas Turcas y Caicos" value="TC">Islas Turcas y Caicos</option>')
                        .append('		<option label="Islas Vírgenes Británicas" value="VG">Islas Vírgenes Británicas</option>')
                        .append('		<option label="Islas Vírgenes de EE. UU." value="VI">Islas Vírgenes de EE. UU.</option>')
                        .append('		<option label="Israel" value="IL">Israel</option>')
                        .append('		<option label="Italia" value="IT">Italia</option>')
                        .append('		<option label="Jamaica" value="JM">Jamaica</option>')
                        .append('		<option label="Japón" value="JP">Japón</option>')
                        .append('		<option label="Jersey" value="JE">Jersey</option>')
                        .append('		<option label="Jordania" value="JO">Jordania</option>')
                        .append('		<option label="Kazajistán" value="KZ">Kazajistán</option>')
                        .append('		<option label="Kenia" value="KE">Kenia</option>')
                        .append('		<option label="Kirguistán" value="KG">Kirguistán</option>')
                        .append('		<option label="Kiribati" value="KI">Kiribati</option>')
                        .append('		<option label="Kosovo" value="XK">Kosovo</option>')
                        .append('		<option label="Kuwait" value="KW">Kuwait</option>')
                        .append('		<option label="Laos" value="LA">Laos</option>')
                        .append('		<option label="Lesoto" value="LS">Lesoto</option>')
                        .append('		<option label="Letonia" value="LV">Letonia</option>')
                        .append('		<option label="Líbano" value="LB">Líbano</option>')
                        .append('		<option label="Liberia" value="LR">Liberia</option>')
                        .append('		<option label="Libia" value="LY">Libia</option>')
                        .append('		<option label="Liechtenstein" value="LI">Liechtenstein</option>')
                        .append('		<option label="Lituania" value="LT">Lituania</option>')
                        .append('		<option label="Luxemburgo" value="LU">Luxemburgo</option>')
                        .append('		<option label="Macedonia" value="MK">Macedonia</option>')
                        .append('		<option label="Madagascar" value="MG">Madagascar</option>')
                        .append('		<option label="Malasia" value="MY">Malasia</option>')
                        .append('		<option label="Malaui" value="MW">Malaui</option>')
                        .append('		<option label="Maldivas" value="MV">Maldivas</option>')
                        .append('		<option label="Mali" value="ML">Mali</option>')
                        .append('		<option label="Malta" value="MT">Malta</option>')
                        .append('		<option label="Marruecos" value="MA">Marruecos</option>')
                        .append('		<option label="Martinica" value="MQ">Martinica</option>')
                        .append('		<option label="Mauricio" value="MU">Mauricio</option>')
                        .append('		<option label="Mauritania" value="MR">Mauritania</option>')
                        .append('		<option label="Mayotte" value="YT">Mayotte</option>')
                        .append('		<option label="México" value="MX">México</option>')
                        .append('		<option label="Micronesia" value="FM">Micronesia</option>')
                        .append('		<option label="Moldavia" value="MD">Moldavia</option>')
                        .append('		<option label="Mónaco" value="MC">Mónaco</option>')
                        .append('		<option label="Mongolia" value="MN">Mongolia</option>')
                        .append('		<option label="Montenegro" value="ME">Montenegro</option>')
                        .append('		<option label="Montserrat" value="MS">Montserrat</option>')
                        .append('		<option label="Mozambique" value="MZ">Mozambique</option>')
                        .append('		<option label="Myanmar (Birmania)" value="MM">Myanmar (Birmania)</option>')
                        .append('		<option label="Namibia" value="NA">Namibia</option>')
                        .append('		<option label="Nauru" value="NR">Nauru</option>')
                        .append('		<option label="Nepal" value="NP">Nepal</option>')
                        .append('		<option label="Nicaragua" value="NI">Nicaragua</option>')
                        .append('		<option label="Níger" value="NE">Níger</option>')
                        .append('		<option label="Nigeria" value="NG">Nigeria</option>')
                        .append('		<option label="Niue" value="NU">Niue</option>')
                        .append('		<option label="Noruega" value="NO">Noruega</option>')
                        .append('		<option label="Nueva Caledonia" value="NC">Nueva Caledonia</option>')
                        .append('		<option label="Nueva Zelanda" value="NZ">Nueva Zelanda</option>')
                        .append('		<option label="Omán" value="OM">Omán</option>')
                        .append('		<option label="Países Bajos" value="NL">Países Bajos</option>')
                        .append('		<option label="Pakistán" value="PK">Pakistán</option>')
                        .append('		<option label="Palaos" value="PW">Palaos</option>')
                        .append('		<option label="Panamá" value="PA">Panamá</option>')
                        .append('		<option label="Papúa Nueva Guinea" value="PG">Papúa Nueva Guinea</option>')
                        .append('		<option label="Paraguay" value="PY">Paraguay</option>')
                        .append('		<option label="Perú" value="PE">Perú</option>')
                        .append('		<option label="Polinesia Francesa" value="PF">Polinesia Francesa</option>')
                        .append('		<option label="Polonia" value="PL">Polonia</option>')
                        .append('		<option label="Portugal" value="PT">Portugal</option>')
                        .append('		<option label="Puerto Rico" value="PR">Puerto Rico</option>')
                        .append('		<option label="RAE de Hong Kong (China)" value="HK">RAE de Hong Kong (China)</option>')
                        .append('		<option label="RAE de Macao (China)" value="MO">RAE de Macao (China)</option>')
                        .append('		<option label="Reino Unido" value="GB">Reino Unido</option>')
                        .append('		<option label="República Centroafricana" value="CF">República Centroafricana</option>')
                        .append('		<option label="República Checa" value="CZ">República Checa</option>')
                        .append('		<option label="República del Congo" value="CG">República del Congo</option>')
                        .append('		<option label="República Democrática del Congo" value="CD">República Democrática del Congo</option>')
                        .append('		<option label="República Dominicana" value="DO">República Dominicana</option>')
                        .append('		<option label="Reunión" value="RE">Reunión</option>')
                        .append('		<option label="Ruanda" value="RW">Ruanda</option>')
                        .append('		<option label="Rumanía" value="RO">Rumanía</option>')
                        .append('		<option label="Rusia" value="RU">Rusia</option>')
                        .append('		<option label="Sáhara Occidental" value="EH">Sáhara Occidental</option>')
                        .append('		<option label="Samoa" value="WS">Samoa</option>')
                        .append('		<option label="Samoa Americana" value="AS">Samoa Americana</option>')
                        .append('		<option label="San Bartolomé" value="BL">San Bartolomé</option>')
                        .append('		<option label="San Cristóbal y Nieves" value="KN">San Cristóbal y Nieves</option>')
                        .append('		<option label="San Marino" value="SM">San Marino</option>')
                        .append('		<option label="San Martín" value="MF">San Martín</option>')
                        .append('		<option label="San Pedro y Miquelón" value="PM">San Pedro y Miquelón</option>')
                        .append('		<option label="San Vicente y las Granadinas" value="VC">San Vicente y las Granadinas</option>')
                        .append('		<option label="Santa Elena" value="SH">Santa Elena</option>')
                        .append('		<option label="Santa Lucía" value="LC">Santa Lucía</option>')
                        .append('		<option label="Santo Tomé y Príncipe" value="ST">Santo Tomé y Príncipe</option>')
                        .append('		<option label="Senegal" value="SN">Senegal</option>')
                        .append('		<option label="Serbia" value="RS">Serbia</option>')
                        .append('		<option label="Seychelles" value="SC">Seychelles</option>')
                        .append('		<option label="Sierra Leona" value="SL">Sierra Leona</option>')
                        .append('		<option label="Singapur" value="SG">Singapur</option>')
                        .append('		<option label="Sint Maarten" value="SX">Sint Maarten</option>')
                        .append('		<option label="Siria" value="SY">Siria</option>')
                        .append('		<option label="Somalia" value="SO">Somalia</option>')
                        .append('		<option label="Sri Lanka" value="LK">Sri Lanka</option>')
                        .append('		<option label="Suazilandia" value="SZ">Suazilandia</option>')
                        .append('		<option label="Sudáfrica" value="ZA">Sudáfrica</option>')
                        .append('		<option label="Sudán" value="SD">Sudán</option>')
                        .append('		<option label="Sudán del Sur" value="SS">Sudán del Sur</option>')
                        .append('		<option label="Suecia" value="SE">Suecia</option>')
                        .append('		<option label="Suiza" value="CH">Suiza</option>')
                        .append('		<option label="Surinam" value="SR">Surinam</option>')
                        .append('		<option label="Svalbard y Jan Mayen" value="SJ">Svalbard y Jan Mayen</option>')
                        .append('		<option label="Tailandia" value="TH">Tailandia</option>')
                        .append('		<option label="Taiwán" value="TW">Taiwán</option>')
                        .append('		<option label="Tanzania" value="TZ">Tanzania</option>')
                        .append('		<option label="Tayikistán" value="TJ">Tayikistán</option>')
                        .append('		<option label="Territorio Británico del Océano Índico" value="IO">Territorio Británico del Océano Índico</option>')
                        .append('		<option label="Territorios Australes Franceses" value="TF">Territorios Australes Franceses</option>')
                        .append('		<option label="Territorios Palestinos" value="PS">Territorios Palestinos</option>')
                        .append('		<option label="Timor Oriental" value="TL">Timor Oriental</option>')
                        .append('		<option label="Togo" value="TG">Togo</option>')
                        .append('		<option label="Tokelau" value="TK">Tokelau</option>')
                        .append('		<option label="Tonga" value="TO">Tonga</option>')
                        .append('		<option label="Trinidad y Tobago" value="TT">Trinidad y Tobago</option>')
                        .append('		<option label="Tristán da Cunha" value="TA">Tristán da Cunha</option>')
                        .append('		<option label="Túnez" value="TN">Túnez</option>')
                        .append('		<option label="Turkmenistán" value="TM">Turkmenistán</option>')
                        .append('		<option label="Turquía" value="TR">Turquía</option>')
                        .append('		<option label="Tuvalu" value="TV">Tuvalu</option>')
                        .append('		<option label="Ucrania" value="UA">Ucrania</option>')
                        .append('		<option label="Uganda" value="UG">Uganda</option>')
                        .append('		<option label="Uruguay" value="UY">Uruguay</option>')
                        .append('		<option label="Uzbekistán" value="UZ">Uzbekistán</option>')
                        .append('		<option label="Vanuatu" value="VU">Vanuatu</option>')
                        .append('		<option label="Venezuela" value="VE">Venezuela</option>')
                        .append('		<option label="Vietnam" value="VN">Vietnam</option>')
                        .append('		<option label="Wallis y Futuna" value="WF">Wallis y Futuna</option>')
                        .append('		<option label="Yemen" value="YE">Yemen</option>')
                        .append('		<option label="Yibuti" value="DJ">Yibuti</option>')
                        .append('		<option label="Zambia" value="ZM">Zambia</option>')
                        .append('		<option label="Zimbabue" value="ZW">Zimbabue</option>');


                    $("#conditionsterms").html('Acepto las <a href="https://www.wanup.com/es/terminos-y-condiciones" target="_blank">Condiciones de Participación</a> en Wanup, sus <a href="https://www.wanup.com/es/aviso-legal" target="_blank">Condiciones de Uso y Política de Privacidad</a>, su <a href="https://www.wanup.com/es/cookies" target="_blank">Política de Cookies</a> y la cesión de mis datos personales al Hotel.');
                }

                var dataString = 'action=fullsite&idhotel=' + $("#idhotel").val() + "&idlocation=" + $("#idlocation").val() + "&lang=" + langcode;
                $.ajax({
                    url: "../resources/handlers/WanupHandler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    success: function (pReturn) {
                        var lReturn = JSON.parse(pReturn);
                        $("#freetext").html(lReturn.LoginText);
                        $("#logo-location").attr("src", "../resources/images/" + $("#idhotel").val() + "/" + $("#idlocation").val() + "-logo.png");

                        if ($("#error").val() != "") {
                            if (langcode == 'es') {
                                switch ($("#error").val().toUpperCase()) {
                                    case 'UNKNOWN USER': $("#message").html('Usuario Desconocido'); break;
                                    case 'NO TIME CREDIT': $("#message").html('Tiempo de conexión agotado'); break;
                                    case 'TICKET EXPIRED': $("#message").html('Ticket Caducado'); break;
                                    case 'TOO MANY DEVICES': $("#message").html('Demasiados dispositivos'); break;
                                    case 'SORRY, NO MORE USERS ALLOWED': $("#message").html('Lo sentimos, no se permiten más usuarios'); break;
                                    case 'WRONG PASSWORD': $("#message").html('Contraseña incorrecta'); break;
                                    case 'UPLOAD QUOTA EXCEEDED': $("#message").html('Cuota de datos de subida agotada'); break;
                                    case 'DOWNLOAD QUOTA EXCEEDED': $("#message").html('Cuota de datos de bajada agotada'); break;

                                    default: $("#message").html($("#error").val());
                                }
                            }
                            else
                                $("#message").html($("#error").val());

                            $("#myModalError").modal('show');
                            $("#myModal").modal('hide');
                            $("#modalWait").modal('hide');
                            $("#principal").fadeIn("slow");

                        }
                        else {
                            $("#myModal").modal('hide');
                            $("#modalWait").modal('hide');
                            $("#principal").delay(800).fadeIn("slow");
                        }

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#loading").html("Su ha producido un error durante el envio. Por favor intentelo más tarde.");
                        alert('error: ' + xhr.statusText);
                    }

                });

                $('#freebutton').click(function (e) {

                    if ($("#accept-conditions").is(":checked")) {
                        var dataString = 'action=allow&idhotel=' + $("#idhotel").val() + '&idlocation=' + $("#idlocation").val() + '&mac=' + $("#mac").val() + '&origin=' + $("#origin").val();
                        $.ajax({
                            url: "../resources/handlers/WanupHandler.ashx",
                            data: dataString,
                            contentType: "application/json; charset=utf-8",
                            dataType: "text",
                            beforeSend: function () {
                                $("#MyModalWait").modal('show');
                            },
                            complete: function () {
                                $("#MyModalWait").modal('hide');
                            },
                            success: function (pResult) {
                                var lResult = JSON.parse(pResult);

                                if (lResult.allow == 1) {
                                    $("#finishbutton").attr("href", lResult.Url);
                                    $("#finishInformation").append($("#freeaccessinformation").html());

                                    if (langcode == 'es') {
                                        $("#finishlabel").html("Enhorabuena");
                                        $("#finishInformation").append('<b>Nombre de usuario: </b>' + lResult.UserName + '<br />');
                                        $("#finishInformation").append('<b>Contraseña: </b>' + lResult.Password + '<br />');
                                        $("#finishInformation").append('<b>Fecha de expiración: </b>' + lResult.ValidTill + '<br />');
                                        $("#finishbutton").val("Continuar navegando");
                                    }
                                    else {
                                        $("#finishInformation").append('<b>UserName: </b>' + lResult.UserName + '<br />');
                                        $("#finishInformation").append('<b>Password: </b>' + lResult.Password + '<br />');
                                        $("#finishInformation").append('<b>Valid Till: </b>' + lResult.ValidTill + '<br />');
                                        $("#finishbutton").val("Continue browsing");
                                    }

                                    $('#myModalFinish').modal('show');
                                }
                                else if (lResult.allow == 2) {
                                    if (langcode != 'es') {
                                        $("#message").html("No more free access allowed now.");
                                    }
                                    else {
                                        $("#message").html("No se permiten más accesos gratuitos en este periodo.");
                                    }
                                    $("#myModalError").modal('show');
                                }
                                else if (lResult.allow == 0) {
                                    var email = $("#email").val();
                                    var name = $("#name").val();
                                    var surname = $("#surname").val();
                                    var pais = $("#pais").val();

                                    var parameters = getParameters();

                                    var valid = true;
                                    if (email == '')
                                        valid = false;
                                    if (name == '')
                                        valid = false;
                                    if (surname == '')
                                        valid = false;
                                    if (pais == '')
                                        valid = false;


                                    if (valid == false) {
                                        if (langcode == 'es') {
                                            $("#message2").html("Todos los campos son obligatorios.").removeClass().addClass('alert alert-danger').show();
                                        }
                                        else {
                                            $("#message2").html("All fields are mandatory.").removeClass().addClass('alert alert-danger').show();
                                        }
                                        $("#myModalMandatory").modal('show');
                                    }
                                    else if (isValidEmailAddress(email) != true) {
                                        if (langcode == 'es') {
                                            $("#message2").html("El formato del correo electrónico no es correcto").removeClass().addClass('alert alert-danger').show();
                                        }
                                        else {
                                            $("#message2").html("Email format is not correct").removeClass().addClass('alert alert-danger').show();
                                        }
                                        $("#myModalMandatory").modal('show');
                                    }

                                    else {

                                        var dataString = 'action=freeaccess&idhotel=' + $("#idhotel").val() + '&name=' + name + '&surname=' + surname + '&pais=' + pais + '&email=' + email + '&idlocation=' + $("#idlocation").val() + '&mac=' + $("#mac").val() + '&origin=' + $("#origin").val() + '&language=' + navigator.language;
                                        $.ajax({
                                            url: "../resources/handlers/WanupHandler.ashx",
                                            data: dataString,
                                            contentType: "application/json; charset=utf-8",
                                            dataType: "text",
                                            beforeSend: function () {
                                                $("#MyModalWait").modal('show');
                                            },
                                            complete: function () {
                                                $("#MyModalWait").modal('hide');
                                            },
                                            success: function (pResult) {
                                                var lResult = JSON.parse(pResult);

                                                if (lResult.IdUser == -1) {
                                                    if (langcode != 'es') {
                                                        $("#message").html("No more free access are allowed now.");
                                                    }
                                                    else {
                                                        $("#message").html("No se permiten más accesos gratuitos en este periodo.");
                                                    }
                                                    $("#myModalError").modal('show');
                                                }
                                                else if (lResult.IdUser == -3) {
                                                    if (langcode != 'es') {
                                                        $("#message").html("We are currently experiencing a problem, please try again later.");
                                                    }
                                                    else {
                                                        $("#message").html("Tenemos un problema temporal, intentelo de nuevo más tarde.");
                                                    }
                                                    $("#myModalError").modal('show');
                                                }
                                                else if (lResult.IdUser == 0) {
                                                    if (langcode != 'es') {
                                                        $("#message").html("We are currently experiencing a problem, please try again later.");
                                                    }
                                                    else {
                                                        $("#message").html("Tenemos un problema temporal, intentelo de nuevo más tarde.");
                                                    }
                                                    $("#myModalError").modal('show');
                                                }
                                                else {
                                                    $("#finishbutton").attr("href", lResult.Url);
                                                    $("#finishInformation").append($("#freeaccessinformation").html());

                                                    if (langcode == 'es') {
                                                        $("#finishlabel").html("Enhorabuena");
                                                        $("#finishInformation").append('<b>Nombre de usuario: </b>' + lResult.UserName + '<br />');
                                                        $("#finishInformation").append('<b>Contraseña: </b>' + lResult.Password + '<br />');
                                                        $("#finishInformation").append('<b>Fecha de expiración: </b>' + lResult.ValidTill + '<br />');
                                                        $("#finishbutton").val("Continuar navegando");
                                                    }
                                                    else {
                                                        $("#finishInformation").append('<b>UserName: </b>' + lResult.UserName + '<br />');
                                                        $("#finishInformation").append('<b>Password: </b>' + lResult.Password + '<br />');
                                                        $("#finishInformation").append('<b>Valid Till: </b>' + lResult.ValidTill + '<br />');
                                                        $("#finishbutton").val("Continue browsing");
                                                    }

                                                    $('#myModalFinish').modal('show');
                                                }

                                            },
                                            error: function (xhr, ajaxOptions, thrownError) {
                                                $("#loading").html("Su ha producido un error durante el envio. Por favor intentelo más tarde.");
                                                alert('error: ' + xhr.statusText);
                                                $("#nextstep3").show();
                                                $("#backstep3").show();

                                            }
                                        });
                                    }

                                }
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                $("#loading").html("Su ha producido un error durante el envio. Por favor intentelo más tarde.");
                                alert('error: ' + xhr.statusText);
                                $("#nextstep3").show();
                                $("#backstep3").show();

                            }
                        });
                    }
                    else {
                        if (langcode == 'es') {
                            $("#message2").html("Para continuar debe aceptar las condiciones").removeClass().addClass('alert alert-danger text-center').show();
                        }
                        else {
                            $("#message2").html("In order to continue you must accept Terms and Conditions.").removeClass().addClass('alert alert-danger text-center').show();
                        }
                        $("#myModalMandatory").modal('show');
                    }

                });

                $("#finishbutton").click(function (e) {
                    e.preventDefault();
                    var url = $("#finishbutton").attr("href");
                    window.location.href = url;

                });

                $("#instagrambutton").click(function (e) {
                    e.preventDefault();

                    if ($("#accept-conditions").is(":checked")) {
                        var uri = window.location.origin;
                        var url = "https://www.instagram.com/oauth/authorize/?client_id=a3d2212db0a1455388b370858fe80e64&redirect_uri=" + uri + "/wanup/instagram.aspx&response_type=token";
                        window.location.href = url;
                    }
                    else {
                        if (langcode == 'es') {
                            $("#message2").html("Para continuar debe aceptar las condiciones").removeClass().addClass('alert alert-danger text-center').show();
                        }
                        else {
                            $("#message2").html("In order to continue you must accept Terms and Conditions.").removeClass().addClass('alert alert-danger text-center').show();
                        }
                        $("#myModalMandatory").modal('show');
                    }
                });

                $("#socialbutton").click(function (e) {
                    e.preventDefault();

                    if ($("#accept-conditions").is(":checked")) {
                        var parameters = getParameters();

                        window.fbAsyncInit = function () {
                            FB.init({
                                appId: '233231503535149',
                                status: true,
                                cookie: true,
                                version: 'v2.11' // 
                            });

                            var uri = window.location.origin + "/wanup/facebook.aspx";
                            FB.getLoginStatus(function (response) {
                                if (response.status === 'connected') {
                                    window.location.href = uri;
                                } else {
                                    window.location = encodeURI("https://www.facebook.com/dialog/oauth?client_id=233231503535149&redirect_uri=" + uri + "&response_type=token&scope=email,public_profile,user_birthday,user_likes");
                                }
                            }, { scope: 'email,public_profile,user_birthday,user_likes' });


                        };
                        (function (d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) { return; }
                            js = d.createElement(s); js.id = id;
                            js.src = "//connect.facebook.net/es_ES/sdk.js";
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));
                    }
                    else {
                        if (langcode == 'es') {
                            $("#message2").html("Para continuar debe aceptar las condiciones").removeClass().addClass('alert alert-danger text-center').show();
                        }
                        else {
                            $("#message2").html("In order to continue you must accept Terms and Conditions.").removeClass().addClass('alert alert-danger text-center').show();
                        }
                        $("#myModalMandatory").modal('show');
                    }

                });

                $("#language").change(function (e) {

                    if ($("#language").val() == 'es') {
                        $("#errorbutton").html("Cerrar");
                        $("#errorbutton2").html("Cerrar");
                        $("#languagelabel").html("Idioma");


                        //TEXTOS GENERALES
                        $("#sorrytext").html("Lo sentimos");
                        $("#waitheader").html("Espere un momento por favor");

                        $("#name").attr("placeholder", "Nombre*")
                        $("#surname").attr("placeholder", "Apellidos*")
                        $('#pais')
                                           .find('option')
                                           .remove()
                                           .end()
                                           .append('<option value="" selected="selected">País de procedencia*</option>')
                                           .append('		<option label="Afganistán" value="AF">Afganistán</option>')
                                           .append('		<option label="Albania" value="AL">Albania</option>')
                                           .append('		<option label="Alemania" value="DE">Alemania</option>')
                                           .append('		<option label="Andorra" value="AD">Andorra</option>')
                                           .append('		<option label="Angola" value="AO">Angola</option>')
                                           .append('		<option label="Anguila" value="AI">Anguila</option>')
                                           .append('		<option label="Antártida" value="AQ">Antártida</option>')
                                           .append('		<option label="Antigua y Barbuda" value="AG">Antigua y Barbuda</option>')
                                           .append('		<option label="Arabia Saudí" value="SA">Arabia Saudí</option>')
                                           .append('		<option label="Argelia" value="DZ">Argelia</option>')
                                           .append('		<option label="Argentina" value="AR">Argentina</option>')
                                           .append('		<option label="Armenia" value="AM">Armenia</option>')
                                           .append('		<option label="Aruba" value="AW">Aruba</option>')
                                           .append('		<option label="Australia" value="AU">Australia</option>')
                                           .append('		<option label="Austria" value="AT">Austria</option>')
                                           .append('		<option label="Azerbaiyán" value="AZ">Azerbaiyán</option>')
                                           .append('		<option label="Bahamas" value="BS">Bahamas</option>')
                                           .append('		<option label="Bangladés" value="BD">Bangladés</option>')
                                           .append('		<option label="Barbados" value="BB">Barbados</option>')
                                           .append('		<option label="Baréin" value="BH">Baréin</option>')
                                           .append('		<option label="Bélgica" value="BE">Bélgica</option>')
                                           .append('		<option label="Belice" value="BZ">Belice</option>')
                                           .append('		<option label="Benín" value="BJ">Benín</option>')
                                           .append('		<option label="Bermudas" value="BM">Bermudas</option>')
                                           .append('		<option label="Bielorrusia" value="BY">Bielorrusia</option>')
                                           .append('		<option label="Bolivia" value="BO">Bolivia</option>')
                                           .append('		<option label="Bosnia-Herzegovina" value="BA">Bosnia-Herzegovina</option>')
                                           .append('		<option label="Botsuana" value="BW">Botsuana</option>')
                                           .append('		<option label="Brasil" value="BR">Brasil</option>')
                                           .append('		<option label="Brunéi" value="BN">Brunéi</option>')
                                           .append('		<option label="Bulgaria" value="BG">Bulgaria</option>')
                                           .append('		<option label="Burkina Faso" value="BF">Burkina Faso</option>')
                                           .append('		<option label="Burundi" value="BI">Burundi</option>')
                                           .append('		<option label="Bután" value="BT">Bután</option>')
                                           .append('		<option label="Cabo Verde" value="CV">Cabo Verde</option>')
                                           .append('		<option label="Camboya" value="KH">Camboya</option>')
                                           .append('		<option label="Camerún" value="CM">Camerún</option>')
                                           .append('		<option label="Canadá" value="CA">Canadá</option>')
                                           .append('		<option label="Canarias" value="IC">Canarias</option>')
                                           .append('		<option label="Caribe neerlandés" value="BQ">Caribe neerlandés</option>')
                                           .append('		<option label="Catar" value="QA">Catar</option>')
                                           .append('		<option label="Ceuta y Melilla" value="EA">Ceuta y Melilla</option>')
                                           .append('		<option label="Chad" value="TD">Chad</option>')
                                           .append('		<option label="Chile" value="CL">Chile</option>')
                                           .append('		<option label="China" value="CN">China</option>')
                                           .append('		<option label="Chipre" value="CY">Chipre</option>')
                                           .append('		<option label="Ciudad del Vaticano" value="VA">Ciudad del Vaticano</option>')
                                           .append('		<option label="Colombia" value="CO">Colombia</option>')
                                           .append('		<option label="Comoras" value="KM">Comoras</option>')
                                           .append('		<option label="Corea del Norte" value="KP">Corea del Norte</option>')
                                           .append('		<option label="Corea del Sur" value="KR">Corea del Sur</option>')
                                           .append('		<option label="Costa de Marfil" value="CI">Costa de Marfil</option>')
                                           .append('		<option label="Costa Rica" value="CR">Costa Rica</option>')
                                           .append('		<option label="Croacia" value="HR">Croacia</option>')
                                           .append('		<option label="Cuba" value="CU">Cuba</option>')
                                           .append('		<option label="Curazao" value="CW">Curazao</option>')
                                           .append('		<option label="Diego García" value="DG">Diego García</option>')
                                           .append('		<option label="Dinamarca" value="DK">Dinamarca</option>')
                                           .append('		<option label="Dominica" value="DM">Dominica</option>')
                                           .append('		<option label="Ecuador" value="EC">Ecuador</option>')
                                           .append('		<option label="Egipto" value="EG">Egipto</option>')
                                           .append('		<option label="El Salvador" value="SV">El Salvador</option>')
                                           .append('		<option label="Emiratos Árabes Unidos" value="AE">Emiratos Árabes Unidos</option>')
                                           .append('		<option label="Eritrea" value="ER">Eritrea</option>')
                                           .append('		<option label="Eslovaquia" value="SK">Eslovaquia</option>')
                                           .append('		<option label="Eslovenia" value="SI">Eslovenia</option>')
                                           .append('		<option label="España" value="ES">España</option>')
                                           .append('		<option label="Estados Unidos" value="US">Estados Unidos</option>')
                                           .append('		<option label="Estonia" value="EE">Estonia</option>')
                                           .append('		<option label="Etiopía" value="ET">Etiopía</option>')
                                           .append('		<option label="Filipinas" value="PH">Filipinas</option>')
                                           .append('		<option label="Finlandia" value="FI">Finlandia</option>')
                                           .append('		<option label="Fiyi" value="FJ">Fiyi</option>')
                                           .append('		<option label="Francia" value="FR">Francia</option>')
                                           .append('		<option label="Gabón" value="GA">Gabón</option>')
                                           .append('		<option label="Gambia" value="GM">Gambia</option>')
                                           .append('		<option label="Georgia" value="GE">Georgia</option>')
                                           .append('		<option label="Ghana" value="GH">Ghana</option>')
                                           .append('		<option label="Gibraltar" value="GI">Gibraltar</option>')
                                           .append('		<option label="Granada" value="GD">Granada</option>')
                                           .append('		<option label="Grecia" value="GR">Grecia</option>')
                                           .append('		<option label="Groenlandia" value="GL">Groenlandia</option>')
                                           .append('		<option label="Guadalupe" value="GP">Guadalupe</option>')
                                           .append('		<option label="Guam" value="GU">Guam</option>')
                                           .append('		<option label="Guatemala" value="GT">Guatemala</option>')
                                           .append('		<option label="Guayana Francesa" value="GF">Guayana Francesa</option>')
                                           .append('		<option label="Guernesey" value="GG">Guernesey</option>')
                                           .append('		<option label="Guinea" value="GN">Guinea</option>')
                                           .append('		<option label="Guinea Ecuatorial" value="GQ">Guinea Ecuatorial</option>')
                                           .append('		<option label="Guinea-Bisáu" value="GW">Guinea-Bisáu</option>')
                                           .append('		<option label="Guyana" value="GY">Guyana</option>')
                                           .append('		<option label="Haití" value="HT">Haití</option>')
                                           .append('		<option label="Honduras" value="HN">Honduras</option>')
                                           .append('		<option label="Hungría" value="HU">Hungría</option>')
                                           .append('		<option label="India" value="IN">India</option>')
                                           .append('		<option label="Indonesia" value="ID">Indonesia</option>')
                                           .append('		<option label="Irak" value="IQ">Irak</option>')
                                           .append('		<option label="Irán" value="IR">Irán</option>')
                                           .append('		<option label="Irlanda" value="IE">Irlanda</option>')
                                           .append('		<option label="Isla de la Ascensión" value="AC">Isla de la Ascensión</option>')
                                           .append('		<option label="Isla de Man" value="IM">Isla de Man</option>')
                                           .append('		<option label="Isla de Navidad" value="CX">Isla de Navidad</option>')
                                           .append('		<option label="Isla Norfolk" value="NF">Isla Norfolk</option>')
                                           .append('		<option label="Islandia" value="IS">Islandia</option>')
                                           .append('		<option label="Islas Åland" value="AX">Islas Åland</option>')
                                           .append('		<option label="Islas Caimán" value="KY">Islas Caimán</option>')
                                           .append('		<option label="Islas Cocos" value="CC">Islas Cocos</option>')
                                           .append('		<option label="Islas Cook" value="CK">Islas Cook</option>')
                                           .append('		<option label="Islas Feroe" value="FO">Islas Feroe</option>')
                                           .append('		<option label="Islas Georgia del Sur y Sandwich del Sur" value="GS">Islas Georgia del Sur y Sandwich del Sur</option>')
                                           .append('		<option label="Islas Malvinas" value="FK">Islas Malvinas</option>')
                                           .append('		<option label="Islas Marianas del Norte" value="MP">Islas Marianas del Norte</option>')
                                           .append('		<option label="Islas Marshall" value="MH">Islas Marshall</option>')
                                           .append('		<option label="Islas menores alejadas de EE. UU." value="UM">Islas menores alejadas de EE. UU.</option>')
                                           .append('		<option label="Islas Pitcairn" value="PN">Islas Pitcairn</option>')
                                           .append('		<option label="Islas Salomón" value="SB">Islas Salomón</option>')
                                           .append('		<option label="Islas Turcas y Caicos" value="TC">Islas Turcas y Caicos</option>')
                                           .append('		<option label="Islas Vírgenes Británicas" value="VG">Islas Vírgenes Británicas</option>')
                                           .append('		<option label="Islas Vírgenes de EE. UU." value="VI">Islas Vírgenes de EE. UU.</option>')
                                           .append('		<option label="Israel" value="IL">Israel</option>')
                                           .append('		<option label="Italia" value="IT">Italia</option>')
                                           .append('		<option label="Jamaica" value="JM">Jamaica</option>')
                                           .append('		<option label="Japón" value="JP">Japón</option>')
                                           .append('		<option label="Jersey" value="JE">Jersey</option>')
                                           .append('		<option label="Jordania" value="JO">Jordania</option>')
                                           .append('		<option label="Kazajistán" value="KZ">Kazajistán</option>')
                                           .append('		<option label="Kenia" value="KE">Kenia</option>')
                                           .append('		<option label="Kirguistán" value="KG">Kirguistán</option>')
                                           .append('		<option label="Kiribati" value="KI">Kiribati</option>')
                                           .append('		<option label="Kosovo" value="XK">Kosovo</option>')
                                           .append('		<option label="Kuwait" value="KW">Kuwait</option>')
                                           .append('		<option label="Laos" value="LA">Laos</option>')
                                           .append('		<option label="Lesoto" value="LS">Lesoto</option>')
                                           .append('		<option label="Letonia" value="LV">Letonia</option>')
                                           .append('		<option label="Líbano" value="LB">Líbano</option>')
                                           .append('		<option label="Liberia" value="LR">Liberia</option>')
                                           .append('		<option label="Libia" value="LY">Libia</option>')
                                           .append('		<option label="Liechtenstein" value="LI">Liechtenstein</option>')
                                           .append('		<option label="Lituania" value="LT">Lituania</option>')
                                           .append('		<option label="Luxemburgo" value="LU">Luxemburgo</option>')
                                           .append('		<option label="Macedonia" value="MK">Macedonia</option>')
                                           .append('		<option label="Madagascar" value="MG">Madagascar</option>')
                                           .append('		<option label="Malasia" value="MY">Malasia</option>')
                                           .append('		<option label="Malaui" value="MW">Malaui</option>')
                                           .append('		<option label="Maldivas" value="MV">Maldivas</option>')
                                           .append('		<option label="Mali" value="ML">Mali</option>')
                                           .append('		<option label="Malta" value="MT">Malta</option>')
                                           .append('		<option label="Marruecos" value="MA">Marruecos</option>')
                                           .append('		<option label="Martinica" value="MQ">Martinica</option>')
                                           .append('		<option label="Mauricio" value="MU">Mauricio</option>')
                                           .append('		<option label="Mauritania" value="MR">Mauritania</option>')
                                           .append('		<option label="Mayotte" value="YT">Mayotte</option>')
                                           .append('		<option label="México" value="MX">México</option>')
                                           .append('		<option label="Micronesia" value="FM">Micronesia</option>')
                                           .append('		<option label="Moldavia" value="MD">Moldavia</option>')
                                           .append('		<option label="Mónaco" value="MC">Mónaco</option>')
                                           .append('		<option label="Mongolia" value="MN">Mongolia</option>')
                                           .append('		<option label="Montenegro" value="ME">Montenegro</option>')
                                           .append('		<option label="Montserrat" value="MS">Montserrat</option>')
                                           .append('		<option label="Mozambique" value="MZ">Mozambique</option>')
                                           .append('		<option label="Myanmar (Birmania)" value="MM">Myanmar (Birmania)</option>')
                                           .append('		<option label="Namibia" value="NA">Namibia</option>')
                                           .append('		<option label="Nauru" value="NR">Nauru</option>')
                                           .append('		<option label="Nepal" value="NP">Nepal</option>')
                                           .append('		<option label="Nicaragua" value="NI">Nicaragua</option>')
                                           .append('		<option label="Níger" value="NE">Níger</option>')
                                           .append('		<option label="Nigeria" value="NG">Nigeria</option>')
                                           .append('		<option label="Niue" value="NU">Niue</option>')
                                           .append('		<option label="Noruega" value="NO">Noruega</option>')
                                           .append('		<option label="Nueva Caledonia" value="NC">Nueva Caledonia</option>')
                                           .append('		<option label="Nueva Zelanda" value="NZ">Nueva Zelanda</option>')
                                           .append('		<option label="Omán" value="OM">Omán</option>')
                                           .append('		<option label="Países Bajos" value="NL">Países Bajos</option>')
                                           .append('		<option label="Pakistán" value="PK">Pakistán</option>')
                                           .append('		<option label="Palaos" value="PW">Palaos</option>')
                                           .append('		<option label="Panamá" value="PA">Panamá</option>')
                                           .append('		<option label="Papúa Nueva Guinea" value="PG">Papúa Nueva Guinea</option>')
                                           .append('		<option label="Paraguay" value="PY">Paraguay</option>')
                                           .append('		<option label="Perú" value="PE">Perú</option>')
                                           .append('		<option label="Polinesia Francesa" value="PF">Polinesia Francesa</option>')
                                           .append('		<option label="Polonia" value="PL">Polonia</option>')
                                           .append('		<option label="Portugal" value="PT">Portugal</option>')
                                           .append('		<option label="Puerto Rico" value="PR">Puerto Rico</option>')
                                           .append('		<option label="RAE de Hong Kong (China)" value="HK">RAE de Hong Kong (China)</option>')
                                           .append('		<option label="RAE de Macao (China)" value="MO">RAE de Macao (China)</option>')
                                           .append('		<option label="Reino Unido" value="GB">Reino Unido</option>')
                                           .append('		<option label="República Centroafricana" value="CF">República Centroafricana</option>')
                                           .append('		<option label="República Checa" value="CZ">República Checa</option>')
                                           .append('		<option label="República del Congo" value="CG">República del Congo</option>')
                                           .append('		<option label="República Democrática del Congo" value="CD">República Democrática del Congo</option>')
                                           .append('		<option label="República Dominicana" value="DO">República Dominicana</option>')
                                           .append('		<option label="Reunión" value="RE">Reunión</option>')
                                           .append('		<option label="Ruanda" value="RW">Ruanda</option>')
                                           .append('		<option label="Rumanía" value="RO">Rumanía</option>')
                                           .append('		<option label="Rusia" value="RU">Rusia</option>')
                                           .append('		<option label="Sáhara Occidental" value="EH">Sáhara Occidental</option>')
                                           .append('		<option label="Samoa" value="WS">Samoa</option>')
                                           .append('		<option label="Samoa Americana" value="AS">Samoa Americana</option>')
                                           .append('		<option label="San Bartolomé" value="BL">San Bartolomé</option>')
                                           .append('		<option label="San Cristóbal y Nieves" value="KN">San Cristóbal y Nieves</option>')
                                           .append('		<option label="San Marino" value="SM">San Marino</option>')
                                           .append('		<option label="San Martín" value="MF">San Martín</option>')
                                           .append('		<option label="San Pedro y Miquelón" value="PM">San Pedro y Miquelón</option>')
                                           .append('		<option label="San Vicente y las Granadinas" value="VC">San Vicente y las Granadinas</option>')
                                           .append('		<option label="Santa Elena" value="SH">Santa Elena</option>')
                                           .append('		<option label="Santa Lucía" value="LC">Santa Lucía</option>')
                                           .append('		<option label="Santo Tomé y Príncipe" value="ST">Santo Tomé y Príncipe</option>')
                                           .append('		<option label="Senegal" value="SN">Senegal</option>')
                                           .append('		<option label="Serbia" value="RS">Serbia</option>')
                                           .append('		<option label="Seychelles" value="SC">Seychelles</option>')
                                           .append('		<option label="Sierra Leona" value="SL">Sierra Leona</option>')
                                           .append('		<option label="Singapur" value="SG">Singapur</option>')
                                           .append('		<option label="Sint Maarten" value="SX">Sint Maarten</option>')
                                           .append('		<option label="Siria" value="SY">Siria</option>')
                                           .append('		<option label="Somalia" value="SO">Somalia</option>')
                                           .append('		<option label="Sri Lanka" value="LK">Sri Lanka</option>')
                                           .append('		<option label="Suazilandia" value="SZ">Suazilandia</option>')
                                           .append('		<option label="Sudáfrica" value="ZA">Sudáfrica</option>')
                                           .append('		<option label="Sudán" value="SD">Sudán</option>')
                                           .append('		<option label="Sudán del Sur" value="SS">Sudán del Sur</option>')
                                           .append('		<option label="Suecia" value="SE">Suecia</option>')
                                           .append('		<option label="Suiza" value="CH">Suiza</option>')
                                           .append('		<option label="Surinam" value="SR">Surinam</option>')
                                           .append('		<option label="Svalbard y Jan Mayen" value="SJ">Svalbard y Jan Mayen</option>')
                                           .append('		<option label="Tailandia" value="TH">Tailandia</option>')
                                           .append('		<option label="Taiwán" value="TW">Taiwán</option>')
                                           .append('		<option label="Tanzania" value="TZ">Tanzania</option>')
                                           .append('		<option label="Tayikistán" value="TJ">Tayikistán</option>')
                                           .append('		<option label="Territorio Británico del Océano Índico" value="IO">Territorio Británico del Océano Índico</option>')
                                           .append('		<option label="Territorios Australes Franceses" value="TF">Territorios Australes Franceses</option>')
                                           .append('		<option label="Territorios Palestinos" value="PS">Territorios Palestinos</option>')
                                           .append('		<option label="Timor Oriental" value="TL">Timor Oriental</option>')
                                           .append('		<option label="Togo" value="TG">Togo</option>')
                                           .append('		<option label="Tokelau" value="TK">Tokelau</option>')
                                           .append('		<option label="Tonga" value="TO">Tonga</option>')
                                           .append('		<option label="Trinidad y Tobago" value="TT">Trinidad y Tobago</option>')
                                           .append('		<option label="Tristán da Cunha" value="TA">Tristán da Cunha</option>')
                                           .append('		<option label="Túnez" value="TN">Túnez</option>')
                                           .append('		<option label="Turkmenistán" value="TM">Turkmenistán</option>')
                                           .append('		<option label="Turquía" value="TR">Turquía</option>')
                                           .append('		<option label="Tuvalu" value="TV">Tuvalu</option>')
                                           .append('		<option label="Ucrania" value="UA">Ucrania</option>')
                                           .append('		<option label="Uganda" value="UG">Uganda</option>')
                                           .append('		<option label="Uruguay" value="UY">Uruguay</option>')
                                           .append('		<option label="Uzbekistán" value="UZ">Uzbekistán</option>')
                                           .append('		<option label="Vanuatu" value="VU">Vanuatu</option>')
                                           .append('		<option label="Venezuela" value="VE">Venezuela</option>')
                                           .append('		<option label="Vietnam" value="VN">Vietnam</option>')
                                           .append('		<option label="Wallis y Futuna" value="WF">Wallis y Futuna</option>')
                                           .append('		<option label="Yemen" value="YE">Yemen</option>')
                                           .append('		<option label="Yibuti" value="DJ">Yibuti</option>')
                                           .append('		<option label="Zambia" value="ZM">Zambia</option>')
                                           .append('		<option label="Zimbabue" value="ZW">Zimbabue</option><option value="" selected="selected">País de procedencia*</option>')
                                           .append('		<option label="Afganistán" value="AF">Afganistán</option>')
                                           .append('		<option label="Albania" value="AL">Albania</option>')
                                           .append('		<option label="Alemania" value="DE">Alemania</option>')
                                           .append('		<option label="Andorra" value="AD">Andorra</option>')
                                           .append('		<option label="Angola" value="AO">Angola</option>')
                                           .append('		<option label="Anguila" value="AI">Anguila</option>')
                                           .append('		<option label="Antártida" value="AQ">Antártida</option>')
                                           .append('		<option label="Antigua y Barbuda" value="AG">Antigua y Barbuda</option>')
                                           .append('		<option label="Arabia Saudí" value="SA">Arabia Saudí</option>')
                                           .append('		<option label="Argelia" value="DZ">Argelia</option>')
                                           .append('		<option label="Argentina" value="AR">Argentina</option>')
                                           .append('		<option label="Armenia" value="AM">Armenia</option>')
                                           .append('		<option label="Aruba" value="AW">Aruba</option>')
                                           .append('		<option label="Australia" value="AU">Australia</option>')
                                           .append('		<option label="Austria" value="AT">Austria</option>')
                                           .append('		<option label="Azerbaiyán" value="AZ">Azerbaiyán</option>')
                                           .append('		<option label="Bahamas" value="BS">Bahamas</option>')
                                           .append('		<option label="Bangladés" value="BD">Bangladés</option>')
                                           .append('		<option label="Barbados" value="BB">Barbados</option>')
                                           .append('		<option label="Baréin" value="BH">Baréin</option>')
                                           .append('		<option label="Bélgica" value="BE">Bélgica</option>')
                                           .append('		<option label="Belice" value="BZ">Belice</option>')
                                           .append('		<option label="Benín" value="BJ">Benín</option>')
                                           .append('		<option label="Bermudas" value="BM">Bermudas</option>')
                                           .append('		<option label="Bielorrusia" value="BY">Bielorrusia</option>')
                                           .append('		<option label="Bolivia" value="BO">Bolivia</option>')
                                           .append('		<option label="Bosnia-Herzegovina" value="BA">Bosnia-Herzegovina</option>')
                                           .append('		<option label="Botsuana" value="BW">Botsuana</option>')
                                           .append('		<option label="Brasil" value="BR">Brasil</option>')
                                           .append('		<option label="Brunéi" value="BN">Brunéi</option>')
                                           .append('		<option label="Bulgaria" value="BG">Bulgaria</option>')
                                           .append('		<option label="Burkina Faso" value="BF">Burkina Faso</option>')
                                           .append('		<option label="Burundi" value="BI">Burundi</option>')
                                           .append('		<option label="Bután" value="BT">Bután</option>')
                                           .append('		<option label="Cabo Verde" value="CV">Cabo Verde</option>')
                                           .append('		<option label="Camboya" value="KH">Camboya</option>')
                                           .append('		<option label="Camerún" value="CM">Camerún</option>')
                                           .append('		<option label="Canadá" value="CA">Canadá</option>')
                                           .append('		<option label="Canarias" value="IC">Canarias</option>')
                                           .append('		<option label="Caribe neerlandés" value="BQ">Caribe neerlandés</option>')
                                           .append('		<option label="Catar" value="QA">Catar</option>')
                                           .append('		<option label="Ceuta y Melilla" value="EA">Ceuta y Melilla</option>')
                                           .append('		<option label="Chad" value="TD">Chad</option>')
                                           .append('		<option label="Chile" value="CL">Chile</option>')
                                           .append('		<option label="China" value="CN">China</option>')
                                           .append('		<option label="Chipre" value="CY">Chipre</option>')
                                           .append('		<option label="Ciudad del Vaticano" value="VA">Ciudad del Vaticano</option>')
                                           .append('		<option label="Colombia" value="CO">Colombia</option>')
                                           .append('		<option label="Comoras" value="KM">Comoras</option>')
                                           .append('		<option label="Corea del Norte" value="KP">Corea del Norte</option>')
                                           .append('		<option label="Corea del Sur" value="KR">Corea del Sur</option>')
                                           .append('		<option label="Costa de Marfil" value="CI">Costa de Marfil</option>')
                                           .append('		<option label="Costa Rica" value="CR">Costa Rica</option>')
                                           .append('		<option label="Croacia" value="HR">Croacia</option>')
                                           .append('		<option label="Cuba" value="CU">Cuba</option>')
                                           .append('		<option label="Curazao" value="CW">Curazao</option>')
                                           .append('		<option label="Diego García" value="DG">Diego García</option>')
                                           .append('		<option label="Dinamarca" value="DK">Dinamarca</option>')
                                           .append('		<option label="Dominica" value="DM">Dominica</option>')
                                           .append('		<option label="Ecuador" value="EC">Ecuador</option>')
                                           .append('		<option label="Egipto" value="EG">Egipto</option>')
                                           .append('		<option label="El Salvador" value="SV">El Salvador</option>')
                                           .append('		<option label="Emiratos Árabes Unidos" value="AE">Emiratos Árabes Unidos</option>')
                                           .append('		<option label="Eritrea" value="ER">Eritrea</option>')
                                           .append('		<option label="Eslovaquia" value="SK">Eslovaquia</option>')
                                           .append('		<option label="Eslovenia" value="SI">Eslovenia</option>')
                                           .append('		<option label="España" value="ES">España</option>')
                                           .append('		<option label="Estados Unidos" value="US">Estados Unidos</option>')
                                           .append('		<option label="Estonia" value="EE">Estonia</option>')
                                           .append('		<option label="Etiopía" value="ET">Etiopía</option>')
                                           .append('		<option label="Filipinas" value="PH">Filipinas</option>')
                                           .append('		<option label="Finlandia" value="FI">Finlandia</option>')
                                           .append('		<option label="Fiyi" value="FJ">Fiyi</option>')
                                           .append('		<option label="Francia" value="FR">Francia</option>')
                                           .append('		<option label="Gabón" value="GA">Gabón</option>')
                                           .append('		<option label="Gambia" value="GM">Gambia</option>')
                                           .append('		<option label="Georgia" value="GE">Georgia</option>')
                                           .append('		<option label="Ghana" value="GH">Ghana</option>')
                                           .append('		<option label="Gibraltar" value="GI">Gibraltar</option>')
                                           .append('		<option label="Granada" value="GD">Granada</option>')
                                           .append('		<option label="Grecia" value="GR">Grecia</option>')
                                           .append('		<option label="Groenlandia" value="GL">Groenlandia</option>')
                                           .append('		<option label="Guadalupe" value="GP">Guadalupe</option>')
                                           .append('		<option label="Guam" value="GU">Guam</option>')
                                           .append('		<option label="Guatemala" value="GT">Guatemala</option>')
                                           .append('		<option label="Guayana Francesa" value="GF">Guayana Francesa</option>')
                                           .append('		<option label="Guernesey" value="GG">Guernesey</option>')
                                           .append('		<option label="Guinea" value="GN">Guinea</option>')
                                           .append('		<option label="Guinea Ecuatorial" value="GQ">Guinea Ecuatorial</option>')
                                           .append('		<option label="Guinea-Bisáu" value="GW">Guinea-Bisáu</option>')
                                           .append('		<option label="Guyana" value="GY">Guyana</option>')
                                           .append('		<option label="Haití" value="HT">Haití</option>')
                                           .append('		<option label="Honduras" value="HN">Honduras</option>')
                                           .append('		<option label="Hungría" value="HU">Hungría</option>')
                                           .append('		<option label="India" value="IN">India</option>')
                                           .append('		<option label="Indonesia" value="ID">Indonesia</option>')
                                           .append('		<option label="Irak" value="IQ">Irak</option>')
                                           .append('		<option label="Irán" value="IR">Irán</option>')
                                           .append('		<option label="Irlanda" value="IE">Irlanda</option>')
                                           .append('		<option label="Isla de la Ascensión" value="AC">Isla de la Ascensión</option>')
                                           .append('		<option label="Isla de Man" value="IM">Isla de Man</option>')
                                           .append('		<option label="Isla de Navidad" value="CX">Isla de Navidad</option>')
                                           .append('		<option label="Isla Norfolk" value="NF">Isla Norfolk</option>')
                                           .append('		<option label="Islandia" value="IS">Islandia</option>')
                                           .append('		<option label="Islas Åland" value="AX">Islas Åland</option>')
                                           .append('		<option label="Islas Caimán" value="KY">Islas Caimán</option>')
                                           .append('		<option label="Islas Cocos" value="CC">Islas Cocos</option>')
                                           .append('		<option label="Islas Cook" value="CK">Islas Cook</option>')
                                           .append('		<option label="Islas Feroe" value="FO">Islas Feroe</option>')
                                           .append('		<option label="Islas Georgia del Sur y Sandwich del Sur" value="GS">Islas Georgia del Sur y Sandwich del Sur</option>')
                                           .append('		<option label="Islas Malvinas" value="FK">Islas Malvinas</option>')
                                           .append('		<option label="Islas Marianas del Norte" value="MP">Islas Marianas del Norte</option>')
                                           .append('		<option label="Islas Marshall" value="MH">Islas Marshall</option>')
                                           .append('		<option label="Islas menores alejadas de EE. UU." value="UM">Islas menores alejadas de EE. UU.</option>')
                                           .append('		<option label="Islas Pitcairn" value="PN">Islas Pitcairn</option>')
                                           .append('		<option label="Islas Salomón" value="SB">Islas Salomón</option>')
                                           .append('		<option label="Islas Turcas y Caicos" value="TC">Islas Turcas y Caicos</option>')
                                           .append('		<option label="Islas Vírgenes Británicas" value="VG">Islas Vírgenes Británicas</option>')
                                           .append('		<option label="Islas Vírgenes de EE. UU." value="VI">Islas Vírgenes de EE. UU.</option>')
                                           .append('		<option label="Israel" value="IL">Israel</option>')
                                           .append('		<option label="Italia" value="IT">Italia</option>')
                                           .append('		<option label="Jamaica" value="JM">Jamaica</option>')
                                           .append('		<option label="Japón" value="JP">Japón</option>')
                                           .append('		<option label="Jersey" value="JE">Jersey</option>')
                                           .append('		<option label="Jordania" value="JO">Jordania</option>')
                                           .append('		<option label="Kazajistán" value="KZ">Kazajistán</option>')
                                           .append('		<option label="Kenia" value="KE">Kenia</option>')
                                           .append('		<option label="Kirguistán" value="KG">Kirguistán</option>')
                                           .append('		<option label="Kiribati" value="KI">Kiribati</option>')
                                           .append('		<option label="Kosovo" value="XK">Kosovo</option>')
                                           .append('		<option label="Kuwait" value="KW">Kuwait</option>')
                                           .append('		<option label="Laos" value="LA">Laos</option>')
                                           .append('		<option label="Lesoto" value="LS">Lesoto</option>')
                                           .append('		<option label="Letonia" value="LV">Letonia</option>')
                                           .append('		<option label="Líbano" value="LB">Líbano</option>')
                                           .append('		<option label="Liberia" value="LR">Liberia</option>')
                                           .append('		<option label="Libia" value="LY">Libia</option>')
                                           .append('		<option label="Liechtenstein" value="LI">Liechtenstein</option>')
                                           .append('		<option label="Lituania" value="LT">Lituania</option>')
                                           .append('		<option label="Luxemburgo" value="LU">Luxemburgo</option>')
                                           .append('		<option label="Macedonia" value="MK">Macedonia</option>')
                                           .append('		<option label="Madagascar" value="MG">Madagascar</option>')
                                           .append('		<option label="Malasia" value="MY">Malasia</option>')
                                           .append('		<option label="Malaui" value="MW">Malaui</option>')
                                           .append('		<option label="Maldivas" value="MV">Maldivas</option>')
                                           .append('		<option label="Mali" value="ML">Mali</option>')
                                           .append('		<option label="Malta" value="MT">Malta</option>')
                                           .append('		<option label="Marruecos" value="MA">Marruecos</option>')
                                           .append('		<option label="Martinica" value="MQ">Martinica</option>')
                                           .append('		<option label="Mauricio" value="MU">Mauricio</option>')
                                           .append('		<option label="Mauritania" value="MR">Mauritania</option>')
                                           .append('		<option label="Mayotte" value="YT">Mayotte</option>')
                                           .append('		<option label="México" value="MX">México</option>')
                                           .append('		<option label="Micronesia" value="FM">Micronesia</option>')
                                           .append('		<option label="Moldavia" value="MD">Moldavia</option>')
                                           .append('		<option label="Mónaco" value="MC">Mónaco</option>')
                                           .append('		<option label="Mongolia" value="MN">Mongolia</option>')
                                           .append('		<option label="Montenegro" value="ME">Montenegro</option>')
                                           .append('		<option label="Montserrat" value="MS">Montserrat</option>')
                                           .append('		<option label="Mozambique" value="MZ">Mozambique</option>')
                                           .append('		<option label="Myanmar (Birmania)" value="MM">Myanmar (Birmania)</option>')
                                           .append('		<option label="Namibia" value="NA">Namibia</option>')
                                           .append('		<option label="Nauru" value="NR">Nauru</option>')
                                           .append('		<option label="Nepal" value="NP">Nepal</option>')
                                           .append('		<option label="Nicaragua" value="NI">Nicaragua</option>')
                                           .append('		<option label="Níger" value="NE">Níger</option>')
                                           .append('		<option label="Nigeria" value="NG">Nigeria</option>')
                                           .append('		<option label="Niue" value="NU">Niue</option>')
                                           .append('		<option label="Noruega" value="NO">Noruega</option>')
                                           .append('		<option label="Nueva Caledonia" value="NC">Nueva Caledonia</option>')
                                           .append('		<option label="Nueva Zelanda" value="NZ">Nueva Zelanda</option>')
                                           .append('		<option label="Omán" value="OM">Omán</option>')
                                           .append('		<option label="Países Bajos" value="NL">Países Bajos</option>')
                                           .append('		<option label="Pakistán" value="PK">Pakistán</option>')
                                           .append('		<option label="Palaos" value="PW">Palaos</option>')
                                           .append('		<option label="Panamá" value="PA">Panamá</option>')
                                           .append('		<option label="Papúa Nueva Guinea" value="PG">Papúa Nueva Guinea</option>')
                                           .append('		<option label="Paraguay" value="PY">Paraguay</option>')
                                           .append('		<option label="Perú" value="PE">Perú</option>')
                                           .append('		<option label="Polinesia Francesa" value="PF">Polinesia Francesa</option>')
                                           .append('		<option label="Polonia" value="PL">Polonia</option>')
                                           .append('		<option label="Portugal" value="PT">Portugal</option>')
                                           .append('		<option label="Puerto Rico" value="PR">Puerto Rico</option>')
                                           .append('		<option label="RAE de Hong Kong (China)" value="HK">RAE de Hong Kong (China)</option>')
                                           .append('		<option label="RAE de Macao (China)" value="MO">RAE de Macao (China)</option>')
                                           .append('		<option label="Reino Unido" value="GB">Reino Unido</option>')
                                           .append('		<option label="República Centroafricana" value="CF">República Centroafricana</option>')
                                           .append('		<option label="República Checa" value="CZ">República Checa</option>')
                                           .append('		<option label="República del Congo" value="CG">República del Congo</option>')
                                           .append('		<option label="República Democrática del Congo" value="CD">República Democrática del Congo</option>')
                                           .append('		<option label="República Dominicana" value="DO">República Dominicana</option>')
                                           .append('		<option label="Reunión" value="RE">Reunión</option>')
                                           .append('		<option label="Ruanda" value="RW">Ruanda</option>')
                                           .append('		<option label="Rumanía" value="RO">Rumanía</option>')
                                           .append('		<option label="Rusia" value="RU">Rusia</option>')
                                           .append('		<option label="Sáhara Occidental" value="EH">Sáhara Occidental</option>')
                                           .append('		<option label="Samoa" value="WS">Samoa</option>')
                                           .append('		<option label="Samoa Americana" value="AS">Samoa Americana</option>')
                                           .append('		<option label="San Bartolomé" value="BL">San Bartolomé</option>')
                                           .append('		<option label="San Cristóbal y Nieves" value="KN">San Cristóbal y Nieves</option>')
                                           .append('		<option label="San Marino" value="SM">San Marino</option>')
                                           .append('		<option label="San Martín" value="MF">San Martín</option>')
                                           .append('		<option label="San Pedro y Miquelón" value="PM">San Pedro y Miquelón</option>')
                                           .append('		<option label="San Vicente y las Granadinas" value="VC">San Vicente y las Granadinas</option>')
                                           .append('		<option label="Santa Elena" value="SH">Santa Elena</option>')
                                           .append('		<option label="Santa Lucía" value="LC">Santa Lucía</option>')
                                           .append('		<option label="Santo Tomé y Príncipe" value="ST">Santo Tomé y Príncipe</option>')
                                           .append('		<option label="Senegal" value="SN">Senegal</option>')
                                           .append('		<option label="Serbia" value="RS">Serbia</option>')
                                           .append('		<option label="Seychelles" value="SC">Seychelles</option>')
                                           .append('		<option label="Sierra Leona" value="SL">Sierra Leona</option>')
                                           .append('		<option label="Singapur" value="SG">Singapur</option>')
                                           .append('		<option label="Sint Maarten" value="SX">Sint Maarten</option>')
                                           .append('		<option label="Siria" value="SY">Siria</option>')
                                           .append('		<option label="Somalia" value="SO">Somalia</option>')
                                           .append('		<option label="Sri Lanka" value="LK">Sri Lanka</option>')
                                           .append('		<option label="Suazilandia" value="SZ">Suazilandia</option>')
                                           .append('		<option label="Sudáfrica" value="ZA">Sudáfrica</option>')
                                           .append('		<option label="Sudán" value="SD">Sudán</option>')
                                           .append('		<option label="Sudán del Sur" value="SS">Sudán del Sur</option>')
                                           .append('		<option label="Suecia" value="SE">Suecia</option>')
                                           .append('		<option label="Suiza" value="CH">Suiza</option>')
                                           .append('		<option label="Surinam" value="SR">Surinam</option>')
                                           .append('		<option label="Svalbard y Jan Mayen" value="SJ">Svalbard y Jan Mayen</option>')
                                           .append('		<option label="Tailandia" value="TH">Tailandia</option>')
                                           .append('		<option label="Taiwán" value="TW">Taiwán</option>')
                                           .append('		<option label="Tanzania" value="TZ">Tanzania</option>')
                                           .append('		<option label="Tayikistán" value="TJ">Tayikistán</option>')
                                           .append('		<option label="Territorio Británico del Océano Índico" value="IO">Territorio Británico del Océano Índico</option>')
                                           .append('		<option label="Territorios Australes Franceses" value="TF">Territorios Australes Franceses</option>')
                                           .append('		<option label="Territorios Palestinos" value="PS">Territorios Palestinos</option>')
                                           .append('		<option label="Timor Oriental" value="TL">Timor Oriental</option>')
                                           .append('		<option label="Togo" value="TG">Togo</option>')
                                           .append('		<option label="Tokelau" value="TK">Tokelau</option>')
                                           .append('		<option label="Tonga" value="TO">Tonga</option>')
                                           .append('		<option label="Trinidad y Tobago" value="TT">Trinidad y Tobago</option>')
                                           .append('		<option label="Tristán da Cunha" value="TA">Tristán da Cunha</option>')
                                           .append('		<option label="Túnez" value="TN">Túnez</option>')
                                           .append('		<option label="Turkmenistán" value="TM">Turkmenistán</option>')
                                           .append('		<option label="Turquía" value="TR">Turquía</option>')
                                           .append('		<option label="Tuvalu" value="TV">Tuvalu</option>')
                                           .append('		<option label="Ucrania" value="UA">Ucrania</option>')
                                           .append('		<option label="Uganda" value="UG">Uganda</option>')
                                           .append('		<option label="Uruguay" value="UY">Uruguay</option>')
                                           .append('		<option label="Uzbekistán" value="UZ">Uzbekistán</option>')
                                           .append('		<option label="Vanuatu" value="VU">Vanuatu</option>')
                                           .append('		<option label="Venezuela" value="VE">Venezuela</option>')
                                           .append('		<option label="Vietnam" value="VN">Vietnam</option>')
                                           .append('		<option label="Wallis y Futuna" value="WF">Wallis y Futuna</option>')
                                           .append('		<option label="Yemen" value="YE">Yemen</option>')
                                           .append('		<option label="Yibuti" value="DJ">Yibuti</option>')
                                           .append('		<option label="Zambia" value="ZM">Zambia</option>')
                                           .append('		<option label="Zimbabue" value="ZW">Zimbabue</option><option value="" selected="selected">País de procedencia*</option>')
                                           .append('		<option label="Afganistán" value="AF">Afganistán</option>')
                                           .append('		<option label="Albania" value="AL">Albania</option>')
                                           .append('		<option label="Alemania" value="DE">Alemania</option>')
                                           .append('		<option label="Andorra" value="AD">Andorra</option>')
                                           .append('		<option label="Angola" value="AO">Angola</option>')
                                           .append('		<option label="Anguila" value="AI">Anguila</option>')
                                           .append('		<option label="Antártida" value="AQ">Antártida</option>')
                                           .append('		<option label="Antigua y Barbuda" value="AG">Antigua y Barbuda</option>')
                                           .append('		<option label="Arabia Saudí" value="SA">Arabia Saudí</option>')
                                           .append('		<option label="Argelia" value="DZ">Argelia</option>')
                                           .append('		<option label="Argentina" value="AR">Argentina</option>')
                                           .append('		<option label="Armenia" value="AM">Armenia</option>')
                                           .append('		<option label="Aruba" value="AW">Aruba</option>')
                                           .append('		<option label="Australia" value="AU">Australia</option>')
                                           .append('		<option label="Austria" value="AT">Austria</option>')
                                           .append('		<option label="Azerbaiyán" value="AZ">Azerbaiyán</option>')
                                           .append('		<option label="Bahamas" value="BS">Bahamas</option>')
                                           .append('		<option label="Bangladés" value="BD">Bangladés</option>')
                                           .append('		<option label="Barbados" value="BB">Barbados</option>')
                                           .append('		<option label="Baréin" value="BH">Baréin</option>')
                                           .append('		<option label="Bélgica" value="BE">Bélgica</option>')
                                           .append('		<option label="Belice" value="BZ">Belice</option>')
                                           .append('		<option label="Benín" value="BJ">Benín</option>')
                                           .append('		<option label="Bermudas" value="BM">Bermudas</option>')
                                           .append('		<option label="Bielorrusia" value="BY">Bielorrusia</option>')
                                           .append('		<option label="Bolivia" value="BO">Bolivia</option>')
                                           .append('		<option label="Bosnia-Herzegovina" value="BA">Bosnia-Herzegovina</option>')
                                           .append('		<option label="Botsuana" value="BW">Botsuana</option>')
                                           .append('		<option label="Brasil" value="BR">Brasil</option>')
                                           .append('		<option label="Brunéi" value="BN">Brunéi</option>')
                                           .append('		<option label="Bulgaria" value="BG">Bulgaria</option>')
                                           .append('		<option label="Burkina Faso" value="BF">Burkina Faso</option>')
                                           .append('		<option label="Burundi" value="BI">Burundi</option>')
                                           .append('		<option label="Bután" value="BT">Bután</option>')
                                           .append('		<option label="Cabo Verde" value="CV">Cabo Verde</option>')
                                           .append('		<option label="Camboya" value="KH">Camboya</option>')
                                           .append('		<option label="Camerún" value="CM">Camerún</option>')
                                           .append('		<option label="Canadá" value="CA">Canadá</option>')
                                           .append('		<option label="Canarias" value="IC">Canarias</option>')
                                           .append('		<option label="Caribe neerlandés" value="BQ">Caribe neerlandés</option>')
                                           .append('		<option label="Catar" value="QA">Catar</option>')
                                           .append('		<option label="Ceuta y Melilla" value="EA">Ceuta y Melilla</option>')
                                           .append('		<option label="Chad" value="TD">Chad</option>')
                                           .append('		<option label="Chile" value="CL">Chile</option>')
                                           .append('		<option label="China" value="CN">China</option>')
                                           .append('		<option label="Chipre" value="CY">Chipre</option>')
                                           .append('		<option label="Ciudad del Vaticano" value="VA">Ciudad del Vaticano</option>')
                                           .append('		<option label="Colombia" value="CO">Colombia</option>')
                                           .append('		<option label="Comoras" value="KM">Comoras</option>')
                                           .append('		<option label="Corea del Norte" value="KP">Corea del Norte</option>')
                                           .append('		<option label="Corea del Sur" value="KR">Corea del Sur</option>')
                                           .append('		<option label="Costa de Marfil" value="CI">Costa de Marfil</option>')
                                           .append('		<option label="Costa Rica" value="CR">Costa Rica</option>')
                                           .append('		<option label="Croacia" value="HR">Croacia</option>')
                                           .append('		<option label="Cuba" value="CU">Cuba</option>')
                                           .append('		<option label="Curazao" value="CW">Curazao</option>')
                                           .append('		<option label="Diego García" value="DG">Diego García</option>')
                                           .append('		<option label="Dinamarca" value="DK">Dinamarca</option>')
                                           .append('		<option label="Dominica" value="DM">Dominica</option>')
                                           .append('		<option label="Ecuador" value="EC">Ecuador</option>')
                                           .append('		<option label="Egipto" value="EG">Egipto</option>')
                                           .append('		<option label="El Salvador" value="SV">El Salvador</option>')
                                           .append('		<option label="Emiratos Árabes Unidos" value="AE">Emiratos Árabes Unidos</option>')
                                           .append('		<option label="Eritrea" value="ER">Eritrea</option>')
                                           .append('		<option label="Eslovaquia" value="SK">Eslovaquia</option>')
                                           .append('		<option label="Eslovenia" value="SI">Eslovenia</option>')
                                           .append('		<option label="España" value="ES">España</option>')
                                           .append('		<option label="Estados Unidos" value="US">Estados Unidos</option>')
                                           .append('		<option label="Estonia" value="EE">Estonia</option>')
                                           .append('		<option label="Etiopía" value="ET">Etiopía</option>')
                                           .append('		<option label="Filipinas" value="PH">Filipinas</option>')
                                           .append('		<option label="Finlandia" value="FI">Finlandia</option>')
                                           .append('		<option label="Fiyi" value="FJ">Fiyi</option>')
                                           .append('		<option label="Francia" value="FR">Francia</option>')
                                           .append('		<option label="Gabón" value="GA">Gabón</option>')
                                           .append('		<option label="Gambia" value="GM">Gambia</option>')
                                           .append('		<option label="Georgia" value="GE">Georgia</option>')
                                           .append('		<option label="Ghana" value="GH">Ghana</option>')
                                           .append('		<option label="Gibraltar" value="GI">Gibraltar</option>')
                                           .append('		<option label="Granada" value="GD">Granada</option>')
                                           .append('		<option label="Grecia" value="GR">Grecia</option>')
                                           .append('		<option label="Groenlandia" value="GL">Groenlandia</option>')
                                           .append('		<option label="Guadalupe" value="GP">Guadalupe</option>')
                                           .append('		<option label="Guam" value="GU">Guam</option>')
                                           .append('		<option label="Guatemala" value="GT">Guatemala</option>')
                                           .append('		<option label="Guayana Francesa" value="GF">Guayana Francesa</option>')
                                           .append('		<option label="Guernesey" value="GG">Guernesey</option>')
                                           .append('		<option label="Guinea" value="GN">Guinea</option>')
                                           .append('		<option label="Guinea Ecuatorial" value="GQ">Guinea Ecuatorial</option>')
                                           .append('		<option label="Guinea-Bisáu" value="GW">Guinea-Bisáu</option>')
                                           .append('		<option label="Guyana" value="GY">Guyana</option>')
                                           .append('		<option label="Haití" value="HT">Haití</option>')
                                           .append('		<option label="Honduras" value="HN">Honduras</option>')
                                           .append('		<option label="Hungría" value="HU">Hungría</option>')
                                           .append('		<option label="India" value="IN">India</option>')
                                           .append('		<option label="Indonesia" value="ID">Indonesia</option>')
                                           .append('		<option label="Irak" value="IQ">Irak</option>')
                                           .append('		<option label="Irán" value="IR">Irán</option>')
                                           .append('		<option label="Irlanda" value="IE">Irlanda</option>')
                                           .append('		<option label="Isla de la Ascensión" value="AC">Isla de la Ascensión</option>')
                                           .append('		<option label="Isla de Man" value="IM">Isla de Man</option>')
                                           .append('		<option label="Isla de Navidad" value="CX">Isla de Navidad</option>')
                                           .append('		<option label="Isla Norfolk" value="NF">Isla Norfolk</option>')
                                           .append('		<option label="Islandia" value="IS">Islandia</option>')
                                           .append('		<option label="Islas Åland" value="AX">Islas Åland</option>')
                                           .append('		<option label="Islas Caimán" value="KY">Islas Caimán</option>')
                                           .append('		<option label="Islas Cocos" value="CC">Islas Cocos</option>')
                                           .append('		<option label="Islas Cook" value="CK">Islas Cook</option>')
                                           .append('		<option label="Islas Feroe" value="FO">Islas Feroe</option>')
                                           .append('		<option label="Islas Georgia del Sur y Sandwich del Sur" value="GS">Islas Georgia del Sur y Sandwich del Sur</option>')
                                           .append('		<option label="Islas Malvinas" value="FK">Islas Malvinas</option>')
                                           .append('		<option label="Islas Marianas del Norte" value="MP">Islas Marianas del Norte</option>')
                                           .append('		<option label="Islas Marshall" value="MH">Islas Marshall</option>')
                                           .append('		<option label="Islas menores alejadas de EE. UU." value="UM">Islas menores alejadas de EE. UU.</option>')
                                           .append('		<option label="Islas Pitcairn" value="PN">Islas Pitcairn</option>')
                                           .append('		<option label="Islas Salomón" value="SB">Islas Salomón</option>')
                                           .append('		<option label="Islas Turcas y Caicos" value="TC">Islas Turcas y Caicos</option>')
                                           .append('		<option label="Islas Vírgenes Británicas" value="VG">Islas Vírgenes Británicas</option>')
                                           .append('		<option label="Islas Vírgenes de EE. UU." value="VI">Islas Vírgenes de EE. UU.</option>')
                                           .append('		<option label="Israel" value="IL">Israel</option>')
                                           .append('		<option label="Italia" value="IT">Italia</option>')
                                           .append('		<option label="Jamaica" value="JM">Jamaica</option>')
                                           .append('		<option label="Japón" value="JP">Japón</option>')
                                           .append('		<option label="Jersey" value="JE">Jersey</option>')
                                           .append('		<option label="Jordania" value="JO">Jordania</option>')
                                           .append('		<option label="Kazajistán" value="KZ">Kazajistán</option>')
                                           .append('		<option label="Kenia" value="KE">Kenia</option>')
                                           .append('		<option label="Kirguistán" value="KG">Kirguistán</option>')
                                           .append('		<option label="Kiribati" value="KI">Kiribati</option>')
                                           .append('		<option label="Kosovo" value="XK">Kosovo</option>')
                                           .append('		<option label="Kuwait" value="KW">Kuwait</option>')
                                           .append('		<option label="Laos" value="LA">Laos</option>')
                                           .append('		<option label="Lesoto" value="LS">Lesoto</option>')
                                           .append('		<option label="Letonia" value="LV">Letonia</option>')
                                           .append('		<option label="Líbano" value="LB">Líbano</option>')
                                           .append('		<option label="Liberia" value="LR">Liberia</option>')
                                           .append('		<option label="Libia" value="LY">Libia</option>')
                                           .append('		<option label="Liechtenstein" value="LI">Liechtenstein</option>')
                                           .append('		<option label="Lituania" value="LT">Lituania</option>')
                                           .append('		<option label="Luxemburgo" value="LU">Luxemburgo</option>')
                                           .append('		<option label="Macedonia" value="MK">Macedonia</option>')
                                           .append('		<option label="Madagascar" value="MG">Madagascar</option>')
                                           .append('		<option label="Malasia" value="MY">Malasia</option>')
                                           .append('		<option label="Malaui" value="MW">Malaui</option>')
                                           .append('		<option label="Maldivas" value="MV">Maldivas</option>')
                                           .append('		<option label="Mali" value="ML">Mali</option>')
                                           .append('		<option label="Malta" value="MT">Malta</option>')
                                           .append('		<option label="Marruecos" value="MA">Marruecos</option>')
                                           .append('		<option label="Martinica" value="MQ">Martinica</option>')
                                           .append('		<option label="Mauricio" value="MU">Mauricio</option>')
                                           .append('		<option label="Mauritania" value="MR">Mauritania</option>')
                                           .append('		<option label="Mayotte" value="YT">Mayotte</option>')
                                           .append('		<option label="México" value="MX">México</option>')
                                           .append('		<option label="Micronesia" value="FM">Micronesia</option>')
                                           .append('		<option label="Moldavia" value="MD">Moldavia</option>')
                                           .append('		<option label="Mónaco" value="MC">Mónaco</option>')
                                           .append('		<option label="Mongolia" value="MN">Mongolia</option>')
                                           .append('		<option label="Montenegro" value="ME">Montenegro</option>')
                                           .append('		<option label="Montserrat" value="MS">Montserrat</option>')
                                           .append('		<option label="Mozambique" value="MZ">Mozambique</option>')
                                           .append('		<option label="Myanmar (Birmania)" value="MM">Myanmar (Birmania)</option>')
                                           .append('		<option label="Namibia" value="NA">Namibia</option>')
                                           .append('		<option label="Nauru" value="NR">Nauru</option>')
                                           .append('		<option label="Nepal" value="NP">Nepal</option>')
                                           .append('		<option label="Nicaragua" value="NI">Nicaragua</option>')
                                           .append('		<option label="Níger" value="NE">Níger</option>')
                                           .append('		<option label="Nigeria" value="NG">Nigeria</option>')
                                           .append('		<option label="Niue" value="NU">Niue</option>')
                                           .append('		<option label="Noruega" value="NO">Noruega</option>')
                                           .append('		<option label="Nueva Caledonia" value="NC">Nueva Caledonia</option>')
                                           .append('		<option label="Nueva Zelanda" value="NZ">Nueva Zelanda</option>')
                                           .append('		<option label="Omán" value="OM">Omán</option>')
                                           .append('		<option label="Países Bajos" value="NL">Países Bajos</option>')
                                           .append('		<option label="Pakistán" value="PK">Pakistán</option>')
                                           .append('		<option label="Palaos" value="PW">Palaos</option>')
                                           .append('		<option label="Panamá" value="PA">Panamá</option>')
                                           .append('		<option label="Papúa Nueva Guinea" value="PG">Papúa Nueva Guinea</option>')
                                           .append('		<option label="Paraguay" value="PY">Paraguay</option>')
                                           .append('		<option label="Perú" value="PE">Perú</option>')
                                           .append('		<option label="Polinesia Francesa" value="PF">Polinesia Francesa</option>')
                                           .append('		<option label="Polonia" value="PL">Polonia</option>')
                                           .append('		<option label="Portugal" value="PT">Portugal</option>')
                                           .append('		<option label="Puerto Rico" value="PR">Puerto Rico</option>')
                                           .append('		<option label="RAE de Hong Kong (China)" value="HK">RAE de Hong Kong (China)</option>')
                                           .append('		<option label="RAE de Macao (China)" value="MO">RAE de Macao (China)</option>')
                                           .append('		<option label="Reino Unido" value="GB">Reino Unido</option>')
                                           .append('		<option label="República Centroafricana" value="CF">República Centroafricana</option>')
                                           .append('		<option label="República Checa" value="CZ">República Checa</option>')
                                           .append('		<option label="República del Congo" value="CG">República del Congo</option>')
                                           .append('		<option label="República Democrática del Congo" value="CD">República Democrática del Congo</option>')
                                           .append('		<option label="República Dominicana" value="DO">República Dominicana</option>')
                                           .append('		<option label="Reunión" value="RE">Reunión</option>')
                                           .append('		<option label="Ruanda" value="RW">Ruanda</option>')
                                           .append('		<option label="Rumanía" value="RO">Rumanía</option>')
                                           .append('		<option label="Rusia" value="RU">Rusia</option>')
                                           .append('		<option label="Sáhara Occidental" value="EH">Sáhara Occidental</option>')
                                           .append('		<option label="Samoa" value="WS">Samoa</option>')
                                           .append('		<option label="Samoa Americana" value="AS">Samoa Americana</option>')
                                           .append('		<option label="San Bartolomé" value="BL">San Bartolomé</option>')
                                           .append('		<option label="San Cristóbal y Nieves" value="KN">San Cristóbal y Nieves</option>')
                                           .append('		<option label="San Marino" value="SM">San Marino</option>')
                                           .append('		<option label="San Martín" value="MF">San Martín</option>')
                                           .append('		<option label="San Pedro y Miquelón" value="PM">San Pedro y Miquelón</option>')
                                           .append('		<option label="San Vicente y las Granadinas" value="VC">San Vicente y las Granadinas</option>')
                                           .append('		<option label="Santa Elena" value="SH">Santa Elena</option>')
                                           .append('		<option label="Santa Lucía" value="LC">Santa Lucía</option>')
                                           .append('		<option label="Santo Tomé y Príncipe" value="ST">Santo Tomé y Príncipe</option>')
                                           .append('		<option label="Senegal" value="SN">Senegal</option>')
                                           .append('		<option label="Serbia" value="RS">Serbia</option>')
                                           .append('		<option label="Seychelles" value="SC">Seychelles</option>')
                                           .append('		<option label="Sierra Leona" value="SL">Sierra Leona</option>')
                                           .append('		<option label="Singapur" value="SG">Singapur</option>')
                                           .append('		<option label="Sint Maarten" value="SX">Sint Maarten</option>')
                                           .append('		<option label="Siria" value="SY">Siria</option>')
                                           .append('		<option label="Somalia" value="SO">Somalia</option>')
                                           .append('		<option label="Sri Lanka" value="LK">Sri Lanka</option>')
                                           .append('		<option label="Suazilandia" value="SZ">Suazilandia</option>')
                                           .append('		<option label="Sudáfrica" value="ZA">Sudáfrica</option>')
                                           .append('		<option label="Sudán" value="SD">Sudán</option>')
                                           .append('		<option label="Sudán del Sur" value="SS">Sudán del Sur</option>')
                                           .append('		<option label="Suecia" value="SE">Suecia</option>')
                                           .append('		<option label="Suiza" value="CH">Suiza</option>')
                                           .append('		<option label="Surinam" value="SR">Surinam</option>')
                                           .append('		<option label="Svalbard y Jan Mayen" value="SJ">Svalbard y Jan Mayen</option>')
                                           .append('		<option label="Tailandia" value="TH">Tailandia</option>')
                                           .append('		<option label="Taiwán" value="TW">Taiwán</option>')
                                           .append('		<option label="Tanzania" value="TZ">Tanzania</option>')
                                           .append('		<option label="Tayikistán" value="TJ">Tayikistán</option>')
                                           .append('		<option label="Territorio Británico del Océano Índico" value="IO">Territorio Británico del Océano Índico</option>')
                                           .append('		<option label="Territorios Australes Franceses" value="TF">Territorios Australes Franceses</option>')
                                           .append('		<option label="Territorios Palestinos" value="PS">Territorios Palestinos</option>')
                                           .append('		<option label="Timor Oriental" value="TL">Timor Oriental</option>')
                                           .append('		<option label="Togo" value="TG">Togo</option>')
                                           .append('		<option label="Tokelau" value="TK">Tokelau</option>')
                                           .append('		<option label="Tonga" value="TO">Tonga</option>')
                                           .append('		<option label="Trinidad y Tobago" value="TT">Trinidad y Tobago</option>')
                                           .append('		<option label="Tristán da Cunha" value="TA">Tristán da Cunha</option>')
                                           .append('		<option label="Túnez" value="TN">Túnez</option>')
                                           .append('		<option label="Turkmenistán" value="TM">Turkmenistán</option>')
                                           .append('		<option label="Turquía" value="TR">Turquía</option>')
                                           .append('		<option label="Tuvalu" value="TV">Tuvalu</option>')
                                           .append('		<option label="Ucrania" value="UA">Ucrania</option>')
                                           .append('		<option label="Uganda" value="UG">Uganda</option>')
                                           .append('		<option label="Uruguay" value="UY">Uruguay</option>')
                                           .append('		<option label="Uzbekistán" value="UZ">Uzbekistán</option>')
                                           .append('		<option label="Vanuatu" value="VU">Vanuatu</option>')
                                           .append('		<option label="Venezuela" value="VE">Venezuela</option>')
                                           .append('		<option label="Vietnam" value="VN">Vietnam</option>')
                                           .append('		<option label="Wallis y Futuna" value="WF">Wallis y Futuna</option>')
                                           .append('		<option label="Yemen" value="YE">Yemen</option>')
                                           .append('		<option label="Yibuti" value="DJ">Yibuti</option>')
                                           .append('		<option label="Zambia" value="ZM">Zambia</option>')
                                           .append('		<option label="Zimbabue" value="ZW">Zimbabue</option><option value="" selected="selected">País de procedencia*</option>')
                                           .append('		<option label="Afganistán" value="AF">Afganistán</option>')
                                           .append('		<option label="Albania" value="AL">Albania</option>')
                                           .append('		<option label="Alemania" value="DE">Alemania</option>')
                                           .append('		<option label="Andorra" value="AD">Andorra</option>')
                                           .append('		<option label="Angola" value="AO">Angola</option>')
                                           .append('		<option label="Anguila" value="AI">Anguila</option>')
                                           .append('		<option label="Antártida" value="AQ">Antártida</option>')
                                           .append('		<option label="Antigua y Barbuda" value="AG">Antigua y Barbuda</option>')
                                           .append('		<option label="Arabia Saudí" value="SA">Arabia Saudí</option>')
                                           .append('		<option label="Argelia" value="DZ">Argelia</option>')
                                           .append('		<option label="Argentina" value="AR">Argentina</option>')
                                           .append('		<option label="Armenia" value="AM">Armenia</option>')
                                           .append('		<option label="Aruba" value="AW">Aruba</option>')
                                           .append('		<option label="Australia" value="AU">Australia</option>')
                                           .append('		<option label="Austria" value="AT">Austria</option>')
                                           .append('		<option label="Azerbaiyán" value="AZ">Azerbaiyán</option>')
                                           .append('		<option label="Bahamas" value="BS">Bahamas</option>')
                                           .append('		<option label="Bangladés" value="BD">Bangladés</option>')
                                           .append('		<option label="Barbados" value="BB">Barbados</option>')
                                           .append('		<option label="Baréin" value="BH">Baréin</option>')
                                           .append('		<option label="Bélgica" value="BE">Bélgica</option>')
                                           .append('		<option label="Belice" value="BZ">Belice</option>')
                                           .append('		<option label="Benín" value="BJ">Benín</option>')
                                           .append('		<option label="Bermudas" value="BM">Bermudas</option>')
                                           .append('		<option label="Bielorrusia" value="BY">Bielorrusia</option>')
                                           .append('		<option label="Bolivia" value="BO">Bolivia</option>')
                                           .append('		<option label="Bosnia-Herzegovina" value="BA">Bosnia-Herzegovina</option>')
                                           .append('		<option label="Botsuana" value="BW">Botsuana</option>')
                                           .append('		<option label="Brasil" value="BR">Brasil</option>')
                                           .append('		<option label="Brunéi" value="BN">Brunéi</option>')
                                           .append('		<option label="Bulgaria" value="BG">Bulgaria</option>')
                                           .append('		<option label="Burkina Faso" value="BF">Burkina Faso</option>')
                                           .append('		<option label="Burundi" value="BI">Burundi</option>')
                                           .append('		<option label="Bután" value="BT">Bután</option>')
                                           .append('		<option label="Cabo Verde" value="CV">Cabo Verde</option>')
                                           .append('		<option label="Camboya" value="KH">Camboya</option>')
                                           .append('		<option label="Camerún" value="CM">Camerún</option>')
                                           .append('		<option label="Canadá" value="CA">Canadá</option>')
                                           .append('		<option label="Canarias" value="IC">Canarias</option>')
                                           .append('		<option label="Caribe neerlandés" value="BQ">Caribe neerlandés</option>')
                                           .append('		<option label="Catar" value="QA">Catar</option>')
                                           .append('		<option label="Ceuta y Melilla" value="EA">Ceuta y Melilla</option>')
                                           .append('		<option label="Chad" value="TD">Chad</option>')
                                           .append('		<option label="Chile" value="CL">Chile</option>')
                                           .append('		<option label="China" value="CN">China</option>')
                                           .append('		<option label="Chipre" value="CY">Chipre</option>')
                                           .append('		<option label="Ciudad del Vaticano" value="VA">Ciudad del Vaticano</option>')
                                           .append('		<option label="Colombia" value="CO">Colombia</option>')
                                           .append('		<option label="Comoras" value="KM">Comoras</option>')
                                           .append('		<option label="Corea del Norte" value="KP">Corea del Norte</option>')
                                           .append('		<option label="Corea del Sur" value="KR">Corea del Sur</option>')
                                           .append('		<option label="Costa de Marfil" value="CI">Costa de Marfil</option>')
                                           .append('		<option label="Costa Rica" value="CR">Costa Rica</option>')
                                           .append('		<option label="Croacia" value="HR">Croacia</option>')
                                           .append('		<option label="Cuba" value="CU">Cuba</option>')
                                           .append('		<option label="Curazao" value="CW">Curazao</option>')
                                           .append('		<option label="Diego García" value="DG">Diego García</option>')
                                           .append('		<option label="Dinamarca" value="DK">Dinamarca</option>')
                                           .append('		<option label="Dominica" value="DM">Dominica</option>')
                                           .append('		<option label="Ecuador" value="EC">Ecuador</option>')
                                           .append('		<option label="Egipto" value="EG">Egipto</option>')
                                           .append('		<option label="El Salvador" value="SV">El Salvador</option>')
                                           .append('		<option label="Emiratos Árabes Unidos" value="AE">Emiratos Árabes Unidos</option>')
                                           .append('		<option label="Eritrea" value="ER">Eritrea</option>')
                                           .append('		<option label="Eslovaquia" value="SK">Eslovaquia</option>')
                                           .append('		<option label="Eslovenia" value="SI">Eslovenia</option>')
                                           .append('		<option label="España" value="ES">España</option>')
                                           .append('		<option label="Estados Unidos" value="US">Estados Unidos</option>')
                                           .append('		<option label="Estonia" value="EE">Estonia</option>')
                                           .append('		<option label="Etiopía" value="ET">Etiopía</option>')
                                           .append('		<option label="Filipinas" value="PH">Filipinas</option>')
                                           .append('		<option label="Finlandia" value="FI">Finlandia</option>')
                                           .append('		<option label="Fiyi" value="FJ">Fiyi</option>')
                                           .append('		<option label="Francia" value="FR">Francia</option>')
                                           .append('		<option label="Gabón" value="GA">Gabón</option>')
                                           .append('		<option label="Gambia" value="GM">Gambia</option>')
                                           .append('		<option label="Georgia" value="GE">Georgia</option>')
                                           .append('		<option label="Ghana" value="GH">Ghana</option>')
                                           .append('		<option label="Gibraltar" value="GI">Gibraltar</option>')
                                           .append('		<option label="Granada" value="GD">Granada</option>')
                                           .append('		<option label="Grecia" value="GR">Grecia</option>')
                                           .append('		<option label="Groenlandia" value="GL">Groenlandia</option>')
                                           .append('		<option label="Guadalupe" value="GP">Guadalupe</option>')
                                           .append('		<option label="Guam" value="GU">Guam</option>')
                                           .append('		<option label="Guatemala" value="GT">Guatemala</option>')
                                           .append('		<option label="Guayana Francesa" value="GF">Guayana Francesa</option>')
                                           .append('		<option label="Guernesey" value="GG">Guernesey</option>')
                                           .append('		<option label="Guinea" value="GN">Guinea</option>')
                                           .append('		<option label="Guinea Ecuatorial" value="GQ">Guinea Ecuatorial</option>')
                                           .append('		<option label="Guinea-Bisáu" value="GW">Guinea-Bisáu</option>')
                                           .append('		<option label="Guyana" value="GY">Guyana</option>')
                                           .append('		<option label="Haití" value="HT">Haití</option>')
                                           .append('		<option label="Honduras" value="HN">Honduras</option>')
                                           .append('		<option label="Hungría" value="HU">Hungría</option>')
                                           .append('		<option label="India" value="IN">India</option>')
                                           .append('		<option label="Indonesia" value="ID">Indonesia</option>')
                                           .append('		<option label="Irak" value="IQ">Irak</option>')
                                           .append('		<option label="Irán" value="IR">Irán</option>')
                                           .append('		<option label="Irlanda" value="IE">Irlanda</option>')
                                           .append('		<option label="Isla de la Ascensión" value="AC">Isla de la Ascensión</option>')
                                           .append('		<option label="Isla de Man" value="IM">Isla de Man</option>')
                                           .append('		<option label="Isla de Navidad" value="CX">Isla de Navidad</option>')
                                           .append('		<option label="Isla Norfolk" value="NF">Isla Norfolk</option>')
                                           .append('		<option label="Islandia" value="IS">Islandia</option>')
                                           .append('		<option label="Islas Åland" value="AX">Islas Åland</option>')
                                           .append('		<option label="Islas Caimán" value="KY">Islas Caimán</option>')
                                           .append('		<option label="Islas Cocos" value="CC">Islas Cocos</option>')
                                           .append('		<option label="Islas Cook" value="CK">Islas Cook</option>')
                                           .append('		<option label="Islas Feroe" value="FO">Islas Feroe</option>')
                                           .append('		<option label="Islas Georgia del Sur y Sandwich del Sur" value="GS">Islas Georgia del Sur y Sandwich del Sur</option>')
                                           .append('		<option label="Islas Malvinas" value="FK">Islas Malvinas</option>')
                                           .append('		<option label="Islas Marianas del Norte" value="MP">Islas Marianas del Norte</option>')
                                           .append('		<option label="Islas Marshall" value="MH">Islas Marshall</option>')
                                           .append('		<option label="Islas menores alejadas de EE. UU." value="UM">Islas menores alejadas de EE. UU.</option>')
                                           .append('		<option label="Islas Pitcairn" value="PN">Islas Pitcairn</option>')
                                           .append('		<option label="Islas Salomón" value="SB">Islas Salomón</option>')
                                           .append('		<option label="Islas Turcas y Caicos" value="TC">Islas Turcas y Caicos</option>')
                                           .append('		<option label="Islas Vírgenes Británicas" value="VG">Islas Vírgenes Británicas</option>')
                                           .append('		<option label="Islas Vírgenes de EE. UU." value="VI">Islas Vírgenes de EE. UU.</option>')
                                           .append('		<option label="Israel" value="IL">Israel</option>')
                                           .append('		<option label="Italia" value="IT">Italia</option>')
                                           .append('		<option label="Jamaica" value="JM">Jamaica</option>')
                                           .append('		<option label="Japón" value="JP">Japón</option>')
                                           .append('		<option label="Jersey" value="JE">Jersey</option>')
                                           .append('		<option label="Jordania" value="JO">Jordania</option>')
                                           .append('		<option label="Kazajistán" value="KZ">Kazajistán</option>')
                                           .append('		<option label="Kenia" value="KE">Kenia</option>')
                                           .append('		<option label="Kirguistán" value="KG">Kirguistán</option>')
                                           .append('		<option label="Kiribati" value="KI">Kiribati</option>')
                                           .append('		<option label="Kosovo" value="XK">Kosovo</option>')
                                           .append('		<option label="Kuwait" value="KW">Kuwait</option>')
                                           .append('		<option label="Laos" value="LA">Laos</option>')
                                           .append('		<option label="Lesoto" value="LS">Lesoto</option>')
                                           .append('		<option label="Letonia" value="LV">Letonia</option>')
                                           .append('		<option label="Líbano" value="LB">Líbano</option>')
                                           .append('		<option label="Liberia" value="LR">Liberia</option>')
                                           .append('		<option label="Libia" value="LY">Libia</option>')
                                           .append('		<option label="Liechtenstein" value="LI">Liechtenstein</option>')
                                           .append('		<option label="Lituania" value="LT">Lituania</option>')
                                           .append('		<option label="Luxemburgo" value="LU">Luxemburgo</option>')
                                           .append('		<option label="Macedonia" value="MK">Macedonia</option>')
                                           .append('		<option label="Madagascar" value="MG">Madagascar</option>')
                                           .append('		<option label="Malasia" value="MY">Malasia</option>')
                                           .append('		<option label="Malaui" value="MW">Malaui</option>')
                                           .append('		<option label="Maldivas" value="MV">Maldivas</option>')
                                           .append('		<option label="Mali" value="ML">Mali</option>')
                                           .append('		<option label="Malta" value="MT">Malta</option>')
                                           .append('		<option label="Marruecos" value="MA">Marruecos</option>')
                                           .append('		<option label="Martinica" value="MQ">Martinica</option>')
                                           .append('		<option label="Mauricio" value="MU">Mauricio</option>')
                                           .append('		<option label="Mauritania" value="MR">Mauritania</option>')
                                           .append('		<option label="Mayotte" value="YT">Mayotte</option>')
                                           .append('		<option label="México" value="MX">México</option>')
                                           .append('		<option label="Micronesia" value="FM">Micronesia</option>')
                                           .append('		<option label="Moldavia" value="MD">Moldavia</option>')
                                           .append('		<option label="Mónaco" value="MC">Mónaco</option>')
                                           .append('		<option label="Mongolia" value="MN">Mongolia</option>')
                                           .append('		<option label="Montenegro" value="ME">Montenegro</option>')
                                           .append('		<option label="Montserrat" value="MS">Montserrat</option>')
                                           .append('		<option label="Mozambique" value="MZ">Mozambique</option>')
                                           .append('		<option label="Myanmar (Birmania)" value="MM">Myanmar (Birmania)</option>')
                                           .append('		<option label="Namibia" value="NA">Namibia</option>')
                                           .append('		<option label="Nauru" value="NR">Nauru</option>')
                                           .append('		<option label="Nepal" value="NP">Nepal</option>')
                                           .append('		<option label="Nicaragua" value="NI">Nicaragua</option>')
                                           .append('		<option label="Níger" value="NE">Níger</option>')
                                           .append('		<option label="Nigeria" value="NG">Nigeria</option>')
                                           .append('		<option label="Niue" value="NU">Niue</option>')
                                           .append('		<option label="Noruega" value="NO">Noruega</option>')
                                           .append('		<option label="Nueva Caledonia" value="NC">Nueva Caledonia</option>')
                                           .append('		<option label="Nueva Zelanda" value="NZ">Nueva Zelanda</option>')
                                           .append('		<option label="Omán" value="OM">Omán</option>')
                                           .append('		<option label="Países Bajos" value="NL">Países Bajos</option>')
                                           .append('		<option label="Pakistán" value="PK">Pakistán</option>')
                                           .append('		<option label="Palaos" value="PW">Palaos</option>')
                                           .append('		<option label="Panamá" value="PA">Panamá</option>')
                                           .append('		<option label="Papúa Nueva Guinea" value="PG">Papúa Nueva Guinea</option>')
                                           .append('		<option label="Paraguay" value="PY">Paraguay</option>')
                                           .append('		<option label="Perú" value="PE">Perú</option>')
                                           .append('		<option label="Polinesia Francesa" value="PF">Polinesia Francesa</option>')
                                           .append('		<option label="Polonia" value="PL">Polonia</option>')
                                           .append('		<option label="Portugal" value="PT">Portugal</option>')
                                           .append('		<option label="Puerto Rico" value="PR">Puerto Rico</option>')
                                           .append('		<option label="RAE de Hong Kong (China)" value="HK">RAE de Hong Kong (China)</option>')
                                           .append('		<option label="RAE de Macao (China)" value="MO">RAE de Macao (China)</option>')
                                           .append('		<option label="Reino Unido" value="GB">Reino Unido</option>')
                                           .append('		<option label="República Centroafricana" value="CF">República Centroafricana</option>')
                                           .append('		<option label="República Checa" value="CZ">República Checa</option>')
                                           .append('		<option label="República del Congo" value="CG">República del Congo</option>')
                                           .append('		<option label="República Democrática del Congo" value="CD">República Democrática del Congo</option>')
                                           .append('		<option label="República Dominicana" value="DO">República Dominicana</option>')
                                           .append('		<option label="Reunión" value="RE">Reunión</option>')
                                           .append('		<option label="Ruanda" value="RW">Ruanda</option>')
                                           .append('		<option label="Rumanía" value="RO">Rumanía</option>')
                                           .append('		<option label="Rusia" value="RU">Rusia</option>')
                                           .append('		<option label="Sáhara Occidental" value="EH">Sáhara Occidental</option>')
                                           .append('		<option label="Samoa" value="WS">Samoa</option>')
                                           .append('		<option label="Samoa Americana" value="AS">Samoa Americana</option>')
                                           .append('		<option label="San Bartolomé" value="BL">San Bartolomé</option>')
                                           .append('		<option label="San Cristóbal y Nieves" value="KN">San Cristóbal y Nieves</option>')
                                           .append('		<option label="San Marino" value="SM">San Marino</option>')
                                           .append('		<option label="San Martín" value="MF">San Martín</option>')
                                           .append('		<option label="San Pedro y Miquelón" value="PM">San Pedro y Miquelón</option>')
                                           .append('		<option label="San Vicente y las Granadinas" value="VC">San Vicente y las Granadinas</option>')
                                           .append('		<option label="Santa Elena" value="SH">Santa Elena</option>')
                                           .append('		<option label="Santa Lucía" value="LC">Santa Lucía</option>')
                                           .append('		<option label="Santo Tomé y Príncipe" value="ST">Santo Tomé y Príncipe</option>')
                                           .append('		<option label="Senegal" value="SN">Senegal</option>')
                                           .append('		<option label="Serbia" value="RS">Serbia</option>')
                                           .append('		<option label="Seychelles" value="SC">Seychelles</option>')
                                           .append('		<option label="Sierra Leona" value="SL">Sierra Leona</option>')
                                           .append('		<option label="Singapur" value="SG">Singapur</option>')
                                           .append('		<option label="Sint Maarten" value="SX">Sint Maarten</option>')
                                           .append('		<option label="Siria" value="SY">Siria</option>')
                                           .append('		<option label="Somalia" value="SO">Somalia</option>')
                                           .append('		<option label="Sri Lanka" value="LK">Sri Lanka</option>')
                                           .append('		<option label="Suazilandia" value="SZ">Suazilandia</option>')
                                           .append('		<option label="Sudáfrica" value="ZA">Sudáfrica</option>')
                                           .append('		<option label="Sudán" value="SD">Sudán</option>')
                                           .append('		<option label="Sudán del Sur" value="SS">Sudán del Sur</option>')
                                           .append('		<option label="Suecia" value="SE">Suecia</option>')
                                           .append('		<option label="Suiza" value="CH">Suiza</option>')
                                           .append('		<option label="Surinam" value="SR">Surinam</option>')
                                           .append('		<option label="Svalbard y Jan Mayen" value="SJ">Svalbard y Jan Mayen</option>')
                                           .append('		<option label="Tailandia" value="TH">Tailandia</option>')
                                           .append('		<option label="Taiwán" value="TW">Taiwán</option>')
                                           .append('		<option label="Tanzania" value="TZ">Tanzania</option>')
                                           .append('		<option label="Tayikistán" value="TJ">Tayikistán</option>')
                                           .append('		<option label="Territorio Británico del Océano Índico" value="IO">Territorio Británico del Océano Índico</option>')
                                           .append('		<option label="Territorios Australes Franceses" value="TF">Territorios Australes Franceses</option>')
                                           .append('		<option label="Territorios Palestinos" value="PS">Territorios Palestinos</option>')
                                           .append('		<option label="Timor Oriental" value="TL">Timor Oriental</option>')
                                           .append('		<option label="Togo" value="TG">Togo</option>')
                                           .append('		<option label="Tokelau" value="TK">Tokelau</option>')
                                           .append('		<option label="Tonga" value="TO">Tonga</option>')
                                           .append('		<option label="Trinidad y Tobago" value="TT">Trinidad y Tobago</option>')
                                           .append('		<option label="Tristán da Cunha" value="TA">Tristán da Cunha</option>')
                                           .append('		<option label="Túnez" value="TN">Túnez</option>')
                                           .append('		<option label="Turkmenistán" value="TM">Turkmenistán</option>')
                                           .append('		<option label="Turquía" value="TR">Turquía</option>')
                                           .append('		<option label="Tuvalu" value="TV">Tuvalu</option>')
                                           .append('		<option label="Ucrania" value="UA">Ucrania</option>')
                                           .append('		<option label="Uganda" value="UG">Uganda</option>')
                                           .append('		<option label="Uruguay" value="UY">Uruguay</option>')
                                           .append('		<option label="Uzbekistán" value="UZ">Uzbekistán</option>')
                                           .append('		<option label="Vanuatu" value="VU">Vanuatu</option>')
                                           .append('		<option label="Venezuela" value="VE">Venezuela</option>')
                                           .append('		<option label="Vietnam" value="VN">Vietnam</option>')
                                           .append('		<option label="Wallis y Futuna" value="WF">Wallis y Futuna</option>')
                                           .append('		<option label="Yemen" value="YE">Yemen</option>')
                                           .append('		<option label="Yibuti" value="DJ">Yibuti</option>')
                                           .append('		<option label="Zambia" value="ZM">Zambia</option>')
                                           .append('		<option label="Zimbabue" value="ZW">Zimbabue</option>');
                        $("#conditionsterms").html('Acepto las <a href="https://www.wanup.com/es/terminos-y-condiciones" target="_blank">Condiciones de Participación</a> en Wanup, sus <a href="https://www.wanup.com/es/aviso-legal" target="_blank">Condiciones de Uso y Política de Privacidad</a>, su <a href="https://www.wanup.com/es/cookies" target="_blank">Política de Cookies</a> y la cesión de mis datos personales al Hotel.');
                    }
                    else {
                        $("#errorbutton").html("Close");
                        $("#errorbutton2").html("Close");
                        $("#languagelabel").html("Language");


                        //TEXTOS GENERALES
                        $("#sorrytext").html("We apologize");
                        $("#waitheader").html("Wait a moment please");

                        $("#name").attr("placeholder", "Name*")
                        $("#surname").attr("placeholder", "Surname*")
                        $('#pais')
                           .find('option')
                           .remove()
                           .end()
                        .append('<option value="" class="" selected="selected">Country of residence*</option>')
                            .append('		<option label="Afghanistan" value="AF">Afghanistan</option>')
                            .append('		<option label="Åland Islands" value="AX">Åland Islands</option>')
                            .append('		<option label="Albania" value="AL">Albania</option>')
                            .append('		<option label="Algeria" value="DZ">Algeria</option>')
                            .append('		<option label="American Samoa" value="AS">American Samoa</option>')
                            .append('		<option label="Andorra" value="AD">Andorra</option>')
                            .append('		<option label="Angola" value="AO">Angola</option>')
                            .append('		<option label="Anguilla" value="AI">Anguilla</option>')
                            .append('		<option label="Antarctica" value="AQ">Antarctica</option>')
                            .append('		<option label="Antigua &amp; Barbuda" value="AG">Antigua &amp; Barbuda</option>')
                            .append('		<option label="Argentina" value="AR">Argentina</option>')
                            .append('		<option label="Armenia" value="AM">Armenia</option>')
                            .append('		<option label="Aruba" value="AW">Aruba</option>')
                            .append('		<option label="Ascension Island" value="AC">Ascension Island</option>')
                            .append('		<option label="Australia" value="AU">Australia</option>')
                            .append('		<option label="Austria" value="AT">Austria</option>')
                            .append('		<option label="Azerbaijan" value="AZ">Azerbaijan</option>')
                            .append('		<option label="Bahamas" value="BS">Bahamas</option>')
                            .append('		<option label="Bahrain" value="BH">Bahrain</option>')
                            .append('		<option label="Bangladesh" value="BD">Bangladesh</option>')
                            .append('		<option label="Barbados" value="BB">Barbados</option>')
                            .append('		<option label="Belarus" value="BY">Belarus</option>')
                            .append('		<option label="Belgium" value="BE">Belgium</option>')
                            .append('		<option label="Belize" value="BZ">Belize</option>')
                            .append('		<option label="Benin" value="BJ">Benin</option>')
                            .append('		<option label="Bermuda" value="BM">Bermuda</option>')
                            .append('		<option label="Bhutan" value="BT">Bhutan</option>')
                            .append('		<option label="Bolivia" value="BO">Bolivia</option>')
                            .append('		<option label="Bosnia &amp; Herzegovina" value="BA">Bosnia &amp; Herzegovina</option>')
                            .append('		<option label="Botswana" value="BW">Botswana</option>')
                            .append('		<option label="Brazil" value="BR">Brazil</option>')
                            .append('		<option label="British Indian Ocean Territory" value="IO">British Indian Ocean Territory</option>')
                            .append('		<option label="British Virgin Islands" value="VG">British Virgin Islands</option>')
                            .append('		<option label="Brunei" value="BN">Brunei</option>')
                            .append('		<option label="Bulgaria" value="BG">Bulgaria</option>')
                            .append('		<option label="Burkina Faso" value="BF">Burkina Faso</option>')
                            .append('		<option label="Burundi" value="BI">Burundi</option>')
                            .append('		<option label="Cambodia" value="KH">Cambodia</option>')
                            .append('		<option label="Cameroon" value="CM">Cameroon</option>')
                            .append('		<option label="Canada" value="CA">Canada</option>')
                            .append('		<option label="Canary Islands" value="IC">Canary Islands</option>')
                            .append('		<option label="Cape Verde" value="CV">Cape Verde</option>')
                            .append('		<option label="Caribbean Netherlands" value="BQ">Caribbean Netherlands</option>')
                            .append('		<option label="Cayman Islands" value="KY">Cayman Islands</option>')
                            .append('		<option label="Central African Republic" value="CF">Central African Republic</option>')
                            .append('		<option label="Ceuta &amp; Melilla" value="EA">Ceuta &amp; Melilla</option>')
                            .append('		<option label="Chad" value="TD">Chad</option>')
                            .append('		<option label="Chile" value="CL">Chile</option>')
                            .append('		<option label="China" value="CN">China</option>')
                            .append('		<option label="Christmas Island" value="CX">Christmas Island</option>')
                            .append('		<option label="Cocos (Keeling) Islands" value="CC">Cocos (Keeling) Islands</option>')
                            .append('		<option label="Colombia" value="CO">Colombia</option>')
                            .append('		<option label="Comoros" value="KM">Comoros</option>')
                            .append('		<option label="Congo - Brazzaville" value="CG">Congo - Brazzaville</option>')
                            .append('		<option label="Congo - Kinshasa" value="CD">Congo - Kinshasa</option>')
                            .append('		<option label="Cook Islands" value="CK">Cook Islands</option>')
                            .append('		<option label="Costa Rica" value="CR">Costa Rica</option>')
                            .append('		<option label="Côte d’Ivoire" value="CI">Côte d’Ivoire</option>')
                            .append('		<option label="Croatia" value="HR">Croatia</option>')
                            .append('		<option label="Cuba" value="CU">Cuba</option>')
                            .append('		<option label="Curaçao" value="CW">Curaçao</option>')
                            .append('		<option label="Cyprus" value="CY">Cyprus</option>')
                            .append('		<option label="Czech Republic" value="CZ">Czech Republic</option>')
                            .append('		<option label="Denmark" value="DK">Denmark</option>')
                            .append('		<option label="Diego Garcia" value="DG">Diego Garcia</option>')
                            .append('		<option label="Djibouti" value="DJ">Djibouti</option>')
                            .append('		<option label="Dominica" value="DM">Dominica</option>')
                            .append('		<option label="Dominican Republic" value="DO">Dominican Republic</option>')
                            .append('		<option label="Ecuador" value="EC">Ecuador</option>')
                            .append('		<option label="Egypt" value="EG">Egypt</option>')
                            .append('		<option label="El Salvador" value="SV">El Salvador</option>')
                            .append('		<option label="Equatorial Guinea" value="GQ">Equatorial Guinea</option>')
                            .append('		<option label="Eritrea" value="ER">Eritrea</option>')
                            .append('		<option label="Estonia" value="EE">Estonia</option>')
                            .append('		<option label="Ethiopia" value="ET">Ethiopia</option>')
                            .append('		<option label="Falkland Islands" value="FK">Falkland Islands</option>')
                            .append('		<option label="Faroe Islands" value="FO">Faroe Islands</option>')
                            .append('		<option label="Fiji" value="FJ">Fiji</option>')
                            .append('		<option label="Finland" value="FI">Finland</option>')
                            .append('		<option label="France" value="FR">France</option>')
                            .append('		<option label="French Guiana" value="GF">French Guiana</option>')
                            .append('		<option label="French Polynesia" value="PF">French Polynesia</option>')
                            .append('		<option label="French Southern Territories" value="TF">French Southern Territories</option>')
                            .append('		<option label="Gabon" value="GA">Gabon</option>')
                            .append('		<option label="Gambia" value="GM">Gambia</option>')
                            .append('		<option label="Georgia" value="GE">Georgia</option>')
                            .append('		<option label="Germany" value="DE">Germany</option>')
                            .append('		<option label="Ghana" value="GH">Ghana</option>')
                            .append('		<option label="Gibraltar" value="GI">Gibraltar</option>')
                            .append('		<option label="Greece" value="GR">Greece</option>')
                            .append('		<option label="Greenland" value="GL">Greenland</option>')
                            .append('		<option label="Grenada" value="GD">Grenada</option>')
                            .append('		<option label="Guadeloupe" value="GP">Guadeloupe</option>')
                            .append('		<option label="Guam" value="GU">Guam</option>')
                            .append('		<option label="Guatemala" value="GT">Guatemala</option>')
                            .append('		<option label="Guernsey" value="GG">Guernsey</option>')
                            .append('		<option label="Guinea" value="GN">Guinea</option>')
                            .append('		<option label="Guinea-Bissau" value="GW">Guinea-Bissau</option>')
                            .append('		<option label="Guyana" value="GY">Guyana</option>')
                            .append('		<option label="Haiti" value="HT">Haiti</option>')
                            .append('		<option label="Honduras" value="HN">Honduras</option>')
                            .append('		<option label="Hong Kong SAR China" value="HK">Hong Kong SAR China</option>')
                            .append('		<option label="Hungary" value="HU">Hungary</option>')
                            .append('		<option label="Iceland" value="IS">Iceland</option>')
                            .append('		<option label="India" value="IN">India</option>')
                            .append('		<option label="Indonesia" value="ID">Indonesia</option>')
                            .append('		<option label="Iran" value="IR">Iran</option>')
                            .append('		<option label="Iraq" value="IQ">Iraq</option>')
                            .append('		<option label="Ireland" value="IE">Ireland</option>')
                            .append('		<option label="Isle of Man" value="IM">Isle of Man</option>')
                            .append('		<option label="Israel" value="IL">Israel</option>')
                            .append('		<option label="Italy" value="IT">Italy</option>')
                            .append('		<option label="Jamaica" value="JM">Jamaica</option>')
                            .append('		<option label="Japan" value="JP">Japan</option>')
                            .append('		<option label="Jersey" value="JE">Jersey</option>')
                            .append('		<option label="Jordan" value="JO">Jordan</option>')
                            .append('		<option label="Kazakhstan" value="KZ">Kazakhstan</option>')
                            .append('		<option label="Kenya" value="KE">Kenya</option>')
                            .append('		<option label="Kiribati" value="KI">Kiribati</option>')
                            .append('		<option label="Kosovo" value="XK">Kosovo</option>')
                            .append('		<option label="Kuwait" value="KW">Kuwait</option>')
                            .append('		<option label="Kyrgyzstan" value="KG">Kyrgyzstan</option>')
                            .append('		<option label="Laos" value="LA">Laos</option>')
                            .append('		<option label="Latvia" value="LV">Latvia</option>')
                            .append('		<option label="Lebanon" value="LB">Lebanon</option>')
                            .append('		<option label="Lesotho" value="LS">Lesotho</option>')
                            .append('		<option label="Liberia" value="LR">Liberia</option>')
                            .append('		<option label="Libya" value="LY">Libya</option>')
                            .append('		<option label="Liechtenstein" value="LI">Liechtenstein</option>')
                            .append('		<option label="Lithuania" value="LT">Lithuania</option>')
                            .append('		<option label="Luxembourg" value="LU">Luxembourg</option>')
                            .append('		<option label="Macau SAR China" value="MO">Macau SAR China</option>')
                            .append('		<option label="Macedonia" value="MK">Macedonia</option>')
                            .append('		<option label="Madagascar" value="MG">Madagascar</option>')
                            .append('		<option label="Malawi" value="MW">Malawi</option>')
                            .append('		<option label="Malaysia" value="MY">Malaysia</option>')
                            .append('		<option label="Maldives" value="MV">Maldives</option>')
                            .append('		<option label="Mali" value="ML">Mali</option>')
                            .append('		<option label="Malta" value="MT">Malta</option>')
                            .append('		<option label="Marshall Islands" value="MH">Marshall Islands</option>')
                            .append('		<option label="Martinique" value="MQ">Martinique</option>')
                            .append('		<option label="Mauritania" value="MR">Mauritania</option>')
                            .append('		<option label="Mauritius" value="MU">Mauritius</option>')
                            .append('		<option label="Mayotte" value="YT">Mayotte</option>')
                            .append('		<option label="Mexico" value="MX">Mexico</option>')
                            .append('		<option label="Micronesia" value="FM">Micronesia</option>')
                            .append('		<option label="Moldova" value="MD">Moldova</option>')
                            .append('		<option label="Monaco" value="MC">Monaco</option>')
                            .append('		<option label="Mongolia" value="MN">Mongolia</option>')
                            .append('		<option label="Montenegro" value="ME">Montenegro</option>')
                            .append('		<option label="Montserrat" value="MS">Montserrat</option>')
                            .append('		<option label="Morocco" value="MA">Morocco</option>')
                            .append('		<option label="Mozambique" value="MZ">Mozambique</option>')
                            .append('		<option label="Myanmar (Burma)" value="MM">Myanmar (Burma)</option>')
                            .append('		<option label="Namibia" value="NA">Namibia</option>')
                            .append('		<option label="Nauru" value="NR">Nauru</option>')
                            .append('		<option label="Nepal" value="NP">Nepal</option>')
                            .append('		<option label="Netherlands" value="NL">Netherlands</option>')
                            .append('		<option label="New Caledonia" value="NC">New Caledonia</option>')
                            .append('		<option label="New Zealand" value="NZ">New Zealand</option>')
                            .append('		<option label="Nicaragua" value="NI">Nicaragua</option>')
                            .append('		<option label="Niger" value="NE">Niger</option>')
                            .append('		<option label="Nigeria" value="NG">Nigeria</option>')
                            .append('		<option label="Niue" value="NU">Niue</option>')
                            .append('		<option label="Norfolk Island" value="NF">Norfolk Island</option>')
                            .append('		<option label="North Korea" value="KP">North Korea</option>')
                            .append('		<option label="Northern Mariana Islands" value="MP">Northern Mariana Islands</option>')
                            .append('		<option label="Norway" value="NO">Norway</option>')
                            .append('		<option label="Oman" value="OM">Oman</option>')
                            .append('		<option label="Pakistan" value="PK">Pakistan</option>')
                            .append('		<option label="Palau" value="PW">Palau</option>')
                            .append('		<option label="Palestinian Territories" value="PS">Palestinian Territories</option>')
                            .append('		<option label="Panama" value="PA">Panama</option>')
                            .append('		<option label="Papua New Guinea" value="PG">Papua New Guinea</option>')
                            .append('		<option label="Paraguay" value="PY">Paraguay</option>')
                            .append('		<option label="Peru" value="PE">Peru</option>')
                            .append('		<option label="Philippines" value="PH">Philippines</option>')
                            .append('		<option label="Pitcairn Islands" value="PN">Pitcairn Islands</option>')
                            .append('		<option label="Poland" value="PL">Poland</option>')
                            .append('		<option label="Portugal" value="PT">Portugal</option>')
                            .append('		<option label="Puerto Rico" value="PR">Puerto Rico</option>')
                            .append('		<option label="Qatar" value="QA">Qatar</option>')
                            .append('		<option label="Réunion" value="RE">Réunion</option>')
                            .append('		<option label="Romania" value="RO">Romania</option>')
                            .append('		<option label="Russia" value="RU">Russia</option>')
                            .append('		<option label="Rwanda" value="RW">Rwanda</option>')
                            .append('		<option label="Samoa" value="WS">Samoa</option>')
                            .append('		<option label="San Marino" value="SM">San Marino</option>')
                            .append('		<option label="São Tomé &amp; Príncipe" value="ST">São Tomé &amp; Príncipe</option>')
                            .append('		<option label="Saudi Arabia" value="SA">Saudi Arabia</option>')
                            .append('		<option label="Senegal" value="SN">Senegal</option>')
                            .append('		<option label="Serbia" value="RS">Serbia</option>')
                            .append('		<option label="Seychelles" value="SC">Seychelles</option>')
                            .append('		<option label="Sierra Leone" value="SL">Sierra Leone</option>')
                            .append('		<option label="Singapore" value="SG">Singapore</option>')
                            .append('		<option label="Sint Maarten" value="SX">Sint Maarten</option>')
                            .append('		<option label="Slovakia" value="SK">Slovakia</option>')
                            .append('		<option label="Slovenia" value="SI">Slovenia</option>')
                            .append('		<option label="Solomon Islands" value="SB">Solomon Islands</option>')
                            .append('		<option label="Somalia" value="SO">Somalia</option>')
                            .append('		<option label="South Africa" value="ZA">South Africa</option>')
                            .append('		<option label="South Georgia &amp; South Sandwich Islands" value="GS">South Georgia &amp; South Sandwich Islands</option>')
                            .append('		<option label="South Korea" value="KR">South Korea</option>')
                            .append('		<option label="South Sudan" value="SS">South Sudan</option>')
                            .append('		<option label="Spain" value="ES">Spain</option>')
                            .append('		<option label="Sri Lanka" value="LK">Sri Lanka</option>')
                            .append('		<option label="St. Barthélemy" value="BL">St. Barthélemy</option>')
                            .append('		<option label="St. Helena" value="SH">St. Helena</option>')
                            .append('		<option label="St. Kitts &amp; Nevis" value="KN">St. Kitts &amp; Nevis</option>')
                            .append('		<option label="St. Lucia" value="LC">St. Lucia</option>')
                            .append('		<option label="St. Martin" value="MF">St. Martin</option>')
                            .append('		<option label="St. Pierre &amp; Miquelon" value="PM">St. Pierre &amp; Miquelon</option>')
                            .append('		<option label="St. Vincent &amp; Grenadines" value="VC">St. Vincent &amp; Grenadines</option>')
                            .append('		<option label="Sudan" value="SD">Sudan</option>')
                            .append('		<option label="Suriname" value="SR">Suriname</option>')
                            .append('		<option label="Svalbard &amp; Jan Mayen" value="SJ">Svalbard &amp; Jan Mayen</option>')
                            .append('		<option label="Swaziland" value="SZ">Swaziland</option>')
                            .append('		<option label="Sweden" value="SE">Sweden</option>')
                            .append('		<option label="Switzerland" value="CH">Switzerland</option>')
                            .append('		<option label="Syria" value="SY">Syria</option>')
                            .append('		<option label="Taiwan" value="TW">Taiwan</option>')
                            .append('		<option label="Tajikistan" value="TJ">Tajikistan</option>')
                            .append('		<option label="Tanzania" value="TZ">Tanzania</option>')
                            .append('		<option label="Thailand" value="TH">Thailand</option>')
                            .append('		<option label="Timor-Leste" value="TL">Timor-Leste</option>')
                            .append('		<option label="Togo" value="TG">Togo</option>')
                            .append('		<option label="Tokelau" value="TK">Tokelau</option>')
                            .append('		<option label="Tonga" value="TO">Tonga</option>')
                            .append('		<option label="Trinidad &amp; Tobago" value="TT">Trinidad &amp; Tobago</option>')
                            .append('		<option label="Tristan da Cunha" value="TA">Tristan da Cunha</option>')
                            .append('		<option label="Tunisia" value="TN">Tunisia</option>')
                            .append('		<option label="Turkey" value="TR">Turkey</option>')
                            .append('		<option label="Turkmenistan" value="TM">Turkmenistan</option>')
                            .append('		<option label="Turks &amp; Caicos Islands" value="TC">Turks &amp; Caicos Islands</option>')
                            .append('		<option label="Tuvalu" value="TV">Tuvalu</option>')
                            .append('		<option label="U.S. Outlying Islands" value="UM">U.S. Outlying Islands</option>')
                            .append('		<option label="U.S. Virgin Islands" value="VI">U.S. Virgin Islands</option>')
                            .append('		<option label="Uganda" value="UG">Uganda</option>')
                            .append('		<option label="Ukraine" value="UA">Ukraine</option>')
                            .append('		<option label="United Arab Emirates" value="AE">United Arab Emirates</option>')
                            .append('		<option label="United Kingdom" value="GB">United Kingdom</option>')
                            .append('		<option label="United States" value="US">United States</option>')
                            .append('		<option label="Uruguay" value="UY">Uruguay</option>')
                            .append('		<option label="Uzbekistan" value="UZ">Uzbekistan</option>')
                            .append('		<option label="Vanuatu" value="VU">Vanuatu</option>')
                            .append('		<option label="Vatican City" value="VA">Vatican City</option>')
                            .append('		<option label="Venezuela" value="VE">Venezuela</option>')
                            .append('		<option label="Vietnam" value="VN">Vietnam</option>')
                            .append('		<option label="Wallis &amp; Futuna" value="WF">Wallis &amp; Futuna</option>')
                            .append('		<option label="Western Sahara" value="EH">Western Sahara</option>')
                            .append('		<option label="Yemen" value="YE">Yemen</option>')
                            .append('		<option label="Zambia" value="ZM">Zambia</option>')
                            .append('		<option label="Zimbabwe" value="ZW">Zimbabwe</option>');

                        $("#conditionsterms").html('I accept the <a href="https://www.wanup.com/en/terms-and-conditions" target="_blank">Terms and Conditions</a> of Wanup’s membership, its Terms of Use and <a href="https://www.wanup.com/en/legal-notice" target="_blank">Privacy Policy</a>, its <a href="https://www.wanup.com/en/cookies" target="_blank">Cookies Policy</a> and the assignment of my personal data to the Hotel.');

                    }
                    var dataString = 'action=fullsite&idhotel=' + $("#idhotel").val() + "&idlocation=" + $("#idlocation").val() + "&lang=" + $("#language").val();
                    $.ajax({
                        url: "../resources/handlers/WanupHandler.ashx",
                        data: dataString,
                        contentType: "application/json; charset=utf-8",
                        dataType: "text",
                        beforeSend: function () {
                            $("#modalWait").modal('show');
                        },
                        complete: function () {
                            $("#modalWait").modal('hide');
                        },
                        success: function (pReturn) {
                            var lReturn = JSON.parse(pReturn);
                            $("#freetext").html(lReturn.LoginText);

                            if ($("#error").val() != "") {
                                if (langcode == 'es') {
                                    switch ($("#error").val().toUpperCase()) {
                                        case 'UNKNOWN USER': $("#message").html('Usuario Desconocido'); break;
                                        case 'NO TIME CREDIT': $("#message").html('Tiempo de conexión agotado'); break;
                                        case 'TICKET EXPIRED': $("#message").html('Ticket Caducado'); break;
                                        case 'TOO MANY DEVICES': $("#message").html('Demasiados dispositivos'); break;
                                        case 'SORRY, NO MORE USERS ALLOWED': $("#message").html('Lo sentimos, no se permiten más usuarios'); break;
                                        case 'WRONG PASSWORD': $("#message").html('Contraseña incorrecta'); break;
                                        case 'UPLOAD QUOTA EXCEEDED': $("#message").html('Cuota de datos de subida agotada'); break;
                                        case 'DOWNLOAD QUOTA EXCEEDED': $("#message").html('Cuota de datos de bajada agotada'); break;

                                        default: $("#message").html($("#error").val());
                                    }
                                }
                                else
                                    $("#message").html($("#error").val());

                                $("#myModalError").modal('show');
                                $("#myModal").modal('hide');
                                $("#modalWait").modal('hide');
                                $("#principal").fadeIn("slow");

                            }
                            else {
                                $("#myModal").modal('hide');
                                $("#modalWait").modal('hide');
                                $("#principal").delay(800).fadeIn("slow");
                            }

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#loading").html("Su ha producido un error durante el envio. Por favor intentelo más tarde.");
                            alert('error: ' + xhr.statusText);
                        }

                    });

                });

            });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="fb-root"></div>
    <div class="page-wrapper">

      <div class="row" id="principal" style="display:none;">
        <div class="col col-l col-50 col-50-md">
            <div class="claim-text" id="freetext">
                <div class="section-title">
                    <span>¡Hola!</span>
                </div>
                <div class="section-text">
                    <p>Somos Wanup y tampoco podemos vivir sin Wifi. <strong>¡Únete a nuestro programa de fidelidad y disfrútalo gratis!</strong></p> 
                </div>
            </div>
        </div>
        <div class="col col-r col-50 col-50-md">
            <div class="content-login">
                <div class="logo-wanup__wrapper">
                    <svg class="logo-wanup" aria-hidden="true" width="113" height="33" alt="Wanup Logo">
                       <use id="Use2" xlink:href="#icon-logo-wanup-color"></use>
                    </svg>

                </div>
                <div class="social-login">
                    <a href="#" id="socialbutton" class="social-login-button facebook" title="Facebook">
                        <svg class="social-login-icon" width="30px" height="30px" viewBox="0 0 512 512" xml:space="preserve">
                            <use id="logo-facebook" xlink:href="#icon-logo-facebook"></use>
                        </svg>
                        <span>Facebook Login</span>
                    </a>
<%--                    <br />
                    <a href="#" id="instagrambutton" class="social-login-button instagram" >
                        <svg class="social-login-icon" width="30px" height="30px" viewBox="0 0 512 512" xml:space="preserve">
                            <use id="logo-instagram" xlink:href="#icon-logo-instagram"></use>
                        </svg>
                        <span>Instagram Login</span>
                    </a>--%>
                </div>
                <div class="row">
                    <div class="col-100">
                        <p class="form-separator">o</p>
                    </div>
                </div>
                <div class="form">
                    <div class="row">
                        <div class="col col-100">
                        <div class="col-l col-50 col-50-xs">
                            <div class="form-field">
                            <label for="name" class="hidden">Nombre<span class="required">*</span></label>
                            <input type="text" id="name" name="name" placeholder="Name*" />
                            </div>
                        </div>
                        <div class="col-r col-50 col-50-xs">
                            <div class="form-field">
                            <label for="surname" class="hidden">E-mail<span class="required">*</span></label>
                            <input type="text" id="surname" name="surname" placeholder="Surname*" />
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-100">
                        <div class="form-field">
                            <label for="email" class="hidden">E-mail<span class="required">*</span></label>
                            <input type="email" id="email" name="email" placeholder="E-mail*" />
                        </div>
                        </div>
                        <div class="col col-100">
                        <div class="form-field">
                            <label for="surname" class="hidden">País de Residencia<span class="required">*</span></label>
                            <select class="form-control" id="pais">
<option value="" class="" selected="selected">Country of residence*</option>
		<option label="Afghanistan" value="AF">Afghanistan</option>
		<option label="Åland Islands" value="AX">Åland Islands</option>
		<option label="Albania" value="AL">Albania</option>
		<option label="Algeria" value="DZ">Algeria</option>
		<option label="American Samoa" value="AS">American Samoa</option>
		<option label="Andorra" value="AD">Andorra</option>
		<option label="Angola" value="AO">Angola</option>
		<option label="Anguilla" value="AI">Anguilla</option>
		<option label="Antarctica" value="AQ">Antarctica</option>
		<option label="Antigua &amp; Barbuda" value="AG">Antigua &amp; Barbuda</option>
		<option label="Argentina" value="AR">Argentina</option>
		<option label="Armenia" value="AM">Armenia</option>
		<option label="Aruba" value="AW">Aruba</option>
		<option label="Ascension Island" value="AC">Ascension Island</option>
		<option label="Australia" value="AU">Australia</option>
		<option label="Austria" value="AT">Austria</option>
		<option label="Azerbaijan" value="AZ">Azerbaijan</option>
		<option label="Bahamas" value="BS">Bahamas</option>
		<option label="Bahrain" value="BH">Bahrain</option>
		<option label="Bangladesh" value="BD">Bangladesh</option>
		<option label="Barbados" value="BB">Barbados</option>
		<option label="Belarus" value="BY">Belarus</option>
		<option label="Belgium" value="BE">Belgium</option>
		<option label="Belize" value="BZ">Belize</option>
		<option label="Benin" value="BJ">Benin</option>
		<option label="Bermuda" value="BM">Bermuda</option>
		<option label="Bhutan" value="BT">Bhutan</option>
		<option label="Bolivia" value="BO">Bolivia</option>
		<option label="Bosnia &amp; Herzegovina" value="BA">Bosnia &amp; Herzegovina</option>
		<option label="Botswana" value="BW">Botswana</option>
		<option label="Brazil" value="BR">Brazil</option>
		<option label="British Indian Ocean Territory" value="IO">British Indian Ocean Territory</option>
		<option label="British Virgin Islands" value="VG">British Virgin Islands</option>
		<option label="Brunei" value="BN">Brunei</option>
		<option label="Bulgaria" value="BG">Bulgaria</option>
		<option label="Burkina Faso" value="BF">Burkina Faso</option>
		<option label="Burundi" value="BI">Burundi</option>
		<option label="Cambodia" value="KH">Cambodia</option>
		<option label="Cameroon" value="CM">Cameroon</option>
		<option label="Canada" value="CA">Canada</option>
		<option label="Canary Islands" value="IC">Canary Islands</option>
		<option label="Cape Verde" value="CV">Cape Verde</option>
		<option label="Caribbean Netherlands" value="BQ">Caribbean Netherlands</option>
		<option label="Cayman Islands" value="KY">Cayman Islands</option>
		<option label="Central African Republic" value="CF">Central African Republic</option>
		<option label="Ceuta &amp; Melilla" value="EA">Ceuta &amp; Melilla</option>
		<option label="Chad" value="TD">Chad</option>
		<option label="Chile" value="CL">Chile</option>
		<option label="China" value="CN">China</option>
		<option label="Christmas Island" value="CX">Christmas Island</option>
		<option label="Cocos (Keeling) Islands" value="CC">Cocos (Keeling) Islands</option>
		<option label="Colombia" value="CO">Colombia</option>
		<option label="Comoros" value="KM">Comoros</option>
		<option label="Congo - Brazzaville" value="CG">Congo - Brazzaville</option>
		<option label="Congo - Kinshasa" value="CD">Congo - Kinshasa</option>
		<option label="Cook Islands" value="CK">Cook Islands</option>
		<option label="Costa Rica" value="CR">Costa Rica</option>
		<option label="Côte d’Ivoire" value="CI">Côte d’Ivoire</option>
		<option label="Croatia" value="HR">Croatia</option>
		<option label="Cuba" value="CU">Cuba</option>
		<option label="Curaçao" value="CW">Curaçao</option>
		<option label="Cyprus" value="CY">Cyprus</option>
		<option label="Czech Republic" value="CZ">Czech Republic</option>
		<option label="Denmark" value="DK">Denmark</option>
		<option label="Diego Garcia" value="DG">Diego Garcia</option>
		<option label="Djibouti" value="DJ">Djibouti</option>
		<option label="Dominica" value="DM">Dominica</option>
		<option label="Dominican Republic" value="DO">Dominican Republic</option>
		<option label="Ecuador" value="EC">Ecuador</option>
		<option label="Egypt" value="EG">Egypt</option>
		<option label="El Salvador" value="SV">El Salvador</option>
		<option label="Equatorial Guinea" value="GQ">Equatorial Guinea</option>
		<option label="Eritrea" value="ER">Eritrea</option>
		<option label="Estonia" value="EE">Estonia</option>
		<option label="Ethiopia" value="ET">Ethiopia</option>
		<option label="Falkland Islands" value="FK">Falkland Islands</option>
		<option label="Faroe Islands" value="FO">Faroe Islands</option>
		<option label="Fiji" value="FJ">Fiji</option>
		<option label="Finland" value="FI">Finland</option>
		<option label="France" value="FR">France</option>
		<option label="French Guiana" value="GF">French Guiana</option>
		<option label="French Polynesia" value="PF">French Polynesia</option>
		<option label="French Southern Territories" value="TF">French Southern Territories</option>
		<option label="Gabon" value="GA">Gabon</option>
		<option label="Gambia" value="GM">Gambia</option>
		<option label="Georgia" value="GE">Georgia</option>
		<option label="Germany" value="DE">Germany</option>
		<option label="Ghana" value="GH">Ghana</option>
		<option label="Gibraltar" value="GI">Gibraltar</option>
		<option label="Greece" value="GR">Greece</option>
		<option label="Greenland" value="GL">Greenland</option>
		<option label="Grenada" value="GD">Grenada</option>
		<option label="Guadeloupe" value="GP">Guadeloupe</option>
		<option label="Guam" value="GU">Guam</option>
		<option label="Guatemala" value="GT">Guatemala</option>
		<option label="Guernsey" value="GG">Guernsey</option>
		<option label="Guinea" value="GN">Guinea</option>
		<option label="Guinea-Bissau" value="GW">Guinea-Bissau</option>
		<option label="Guyana" value="GY">Guyana</option>
		<option label="Haiti" value="HT">Haiti</option>
		<option label="Honduras" value="HN">Honduras</option>
		<option label="Hong Kong SAR China" value="HK">Hong Kong SAR China</option>
		<option label="Hungary" value="HU">Hungary</option>
		<option label="Iceland" value="IS">Iceland</option>
		<option label="India" value="IN">India</option>
		<option label="Indonesia" value="ID">Indonesia</option>
		<option label="Iran" value="IR">Iran</option>
		<option label="Iraq" value="IQ">Iraq</option>
		<option label="Ireland" value="IE">Ireland</option>
		<option label="Isle of Man" value="IM">Isle of Man</option>
		<option label="Israel" value="IL">Israel</option>
		<option label="Italy" value="IT">Italy</option>
		<option label="Jamaica" value="JM">Jamaica</option>
		<option label="Japan" value="JP">Japan</option>
		<option label="Jersey" value="JE">Jersey</option>
		<option label="Jordan" value="JO">Jordan</option>
		<option label="Kazakhstan" value="KZ">Kazakhstan</option>
		<option label="Kenya" value="KE">Kenya</option>
		<option label="Kiribati" value="KI">Kiribati</option>
		<option label="Kosovo" value="XK">Kosovo</option>
		<option label="Kuwait" value="KW">Kuwait</option>
		<option label="Kyrgyzstan" value="KG">Kyrgyzstan</option>
		<option label="Laos" value="LA">Laos</option>
		<option label="Latvia" value="LV">Latvia</option>
		<option label="Lebanon" value="LB">Lebanon</option>
		<option label="Lesotho" value="LS">Lesotho</option>
		<option label="Liberia" value="LR">Liberia</option>
		<option label="Libya" value="LY">Libya</option>
		<option label="Liechtenstein" value="LI">Liechtenstein</option>
		<option label="Lithuania" value="LT">Lithuania</option>
		<option label="Luxembourg" value="LU">Luxembourg</option>
		<option label="Macau SAR China" value="MO">Macau SAR China</option>
		<option label="Macedonia" value="MK">Macedonia</option>
		<option label="Madagascar" value="MG">Madagascar</option>
		<option label="Malawi" value="MW">Malawi</option>
		<option label="Malaysia" value="MY">Malaysia</option>
		<option label="Maldives" value="MV">Maldives</option>
		<option label="Mali" value="ML">Mali</option>
		<option label="Malta" value="MT">Malta</option>
		<option label="Marshall Islands" value="MH">Marshall Islands</option>
		<option label="Martinique" value="MQ">Martinique</option>
		<option label="Mauritania" value="MR">Mauritania</option>
		<option label="Mauritius" value="MU">Mauritius</option>
		<option label="Mayotte" value="YT">Mayotte</option>
		<option label="Mexico" value="MX">Mexico</option>
		<option label="Micronesia" value="FM">Micronesia</option>
		<option label="Moldova" value="MD">Moldova</option>
		<option label="Monaco" value="MC">Monaco</option>
		<option label="Mongolia" value="MN">Mongolia</option>
		<option label="Montenegro" value="ME">Montenegro</option>
		<option label="Montserrat" value="MS">Montserrat</option>
		<option label="Morocco" value="MA">Morocco</option>
		<option label="Mozambique" value="MZ">Mozambique</option>
		<option label="Myanmar (Burma)" value="MM">Myanmar (Burma)</option>
		<option label="Namibia" value="NA">Namibia</option>
		<option label="Nauru" value="NR">Nauru</option>
		<option label="Nepal" value="NP">Nepal</option>
		<option label="Netherlands" value="NL">Netherlands</option>
		<option label="New Caledonia" value="NC">New Caledonia</option>
		<option label="New Zealand" value="NZ">New Zealand</option>
		<option label="Nicaragua" value="NI">Nicaragua</option>
		<option label="Niger" value="NE">Niger</option>
		<option label="Nigeria" value="NG">Nigeria</option>
		<option label="Niue" value="NU">Niue</option>
		<option label="Norfolk Island" value="NF">Norfolk Island</option>
		<option label="North Korea" value="KP">North Korea</option>
		<option label="Northern Mariana Islands" value="MP">Northern Mariana Islands</option>
		<option label="Norway" value="NO">Norway</option>
		<option label="Oman" value="OM">Oman</option>
		<option label="Pakistan" value="PK">Pakistan</option>
		<option label="Palau" value="PW">Palau</option>
		<option label="Palestinian Territories" value="PS">Palestinian Territories</option>
		<option label="Panama" value="PA">Panama</option>
		<option label="Papua New Guinea" value="PG">Papua New Guinea</option>
		<option label="Paraguay" value="PY">Paraguay</option>
		<option label="Peru" value="PE">Peru</option>
		<option label="Philippines" value="PH">Philippines</option>
		<option label="Pitcairn Islands" value="PN">Pitcairn Islands</option>
		<option label="Poland" value="PL">Poland</option>
		<option label="Portugal" value="PT">Portugal</option>
		<option label="Puerto Rico" value="PR">Puerto Rico</option>
		<option label="Qatar" value="QA">Qatar</option>
		<option label="Réunion" value="RE">Réunion</option>
		<option label="Romania" value="RO">Romania</option>
		<option label="Russia" value="RU">Russia</option>
		<option label="Rwanda" value="RW">Rwanda</option>
		<option label="Samoa" value="WS">Samoa</option>
		<option label="San Marino" value="SM">San Marino</option>
		<option label="São Tomé &amp; Príncipe" value="ST">São Tomé &amp; Príncipe</option>
		<option label="Saudi Arabia" value="SA">Saudi Arabia</option>
		<option label="Senegal" value="SN">Senegal</option>
		<option label="Serbia" value="RS">Serbia</option>
		<option label="Seychelles" value="SC">Seychelles</option>
		<option label="Sierra Leone" value="SL">Sierra Leone</option>
		<option label="Singapore" value="SG">Singapore</option>
		<option label="Sint Maarten" value="SX">Sint Maarten</option>
		<option label="Slovakia" value="SK">Slovakia</option>
		<option label="Slovenia" value="SI">Slovenia</option>
		<option label="Solomon Islands" value="SB">Solomon Islands</option>
		<option label="Somalia" value="SO">Somalia</option>
		<option label="South Africa" value="ZA">South Africa</option>
		<option label="South Georgia &amp; South Sandwich Islands" value="GS">South Georgia &amp; South Sandwich Islands</option>
		<option label="South Korea" value="KR">South Korea</option>
		<option label="South Sudan" value="SS">South Sudan</option>
		<option label="Spain" value="ES">Spain</option>
		<option label="Sri Lanka" value="LK">Sri Lanka</option>
		<option label="St. Barthélemy" value="BL">St. Barthélemy</option>
		<option label="St. Helena" value="SH">St. Helena</option>
		<option label="St. Kitts &amp; Nevis" value="KN">St. Kitts &amp; Nevis</option>
		<option label="St. Lucia" value="LC">St. Lucia</option>
		<option label="St. Martin" value="MF">St. Martin</option>
		<option label="St. Pierre &amp; Miquelon" value="PM">St. Pierre &amp; Miquelon</option>
		<option label="St. Vincent &amp; Grenadines" value="VC">St. Vincent &amp; Grenadines</option>
		<option label="Sudan" value="SD">Sudan</option>
		<option label="Suriname" value="SR">Suriname</option>
		<option label="Svalbard &amp; Jan Mayen" value="SJ">Svalbard &amp; Jan Mayen</option>
		<option label="Swaziland" value="SZ">Swaziland</option>
		<option label="Sweden" value="SE">Sweden</option>
		<option label="Switzerland" value="CH">Switzerland</option>
		<option label="Syria" value="SY">Syria</option>
		<option label="Taiwan" value="TW">Taiwan</option>
		<option label="Tajikistan" value="TJ">Tajikistan</option>
		<option label="Tanzania" value="TZ">Tanzania</option>
		<option label="Thailand" value="TH">Thailand</option>
		<option label="Timor-Leste" value="TL">Timor-Leste</option>
		<option label="Togo" value="TG">Togo</option>
		<option label="Tokelau" value="TK">Tokelau</option>
		<option label="Tonga" value="TO">Tonga</option>
		<option label="Trinidad &amp; Tobago" value="TT">Trinidad &amp; Tobago</option>
		<option label="Tristan da Cunha" value="TA">Tristan da Cunha</option>
		<option label="Tunisia" value="TN">Tunisia</option>
		<option label="Turkey" value="TR">Turkey</option>
		<option label="Turkmenistan" value="TM">Turkmenistan</option>
		<option label="Turks &amp; Caicos Islands" value="TC">Turks &amp; Caicos Islands</option>
		<option label="Tuvalu" value="TV">Tuvalu</option>
		<option label="U.S. Outlying Islands" value="UM">U.S. Outlying Islands</option>
		<option label="U.S. Virgin Islands" value="VI">U.S. Virgin Islands</option>
		<option label="Uganda" value="UG">Uganda</option>
		<option label="Ukraine" value="UA">Ukraine</option>
		<option label="United Arab Emirates" value="AE">United Arab Emirates</option>
		<option label="United Kingdom" value="GB">United Kingdom</option>
		<option label="United States" value="US">United States</option>
		<option label="Uruguay" value="UY">Uruguay</option>
		<option label="Uzbekistan" value="UZ">Uzbekistan</option>
		<option label="Vanuatu" value="VU">Vanuatu</option>
		<option label="Vatican City" value="VA">Vatican City</option>
		<option label="Venezuela" value="VE">Venezuela</option>
		<option label="Vietnam" value="VN">Vietnam</option>
		<option label="Wallis &amp; Futuna" value="WF">Wallis &amp; Futuna</option>
		<option label="Western Sahara" value="EH">Western Sahara</option>
		<option label="Yemen" value="YE">Yemen</option>
		<option label="Zambia" value="ZM">Zambia</option>
		<option label="Zimbabwe" value="ZW">Zimbabwe</option>                            </select>
                        </div>
                        </div>
                        <div class="col col-100">
                        <div class="form-field form-check">
                            <label for="accept-conditions">
                            <input id="accept-conditions" type="checkbox" name="accept-conditions" value="1" />
                            <svg class="icon icon-check" aria-hidden="true"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-check"></use></svg>
                            <span id="conditionsterms"></span>
                            </label>
                        </div>
                        </div>
                        <div class="col col-100">
                        <div class="form-field">
                            <a href="#" class="btn" id="freebutton">Log in!</a>
                        </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
        </div>
        <!-- Modal -->

        <div class="modal fade" id="myModalError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title text-center" id="H2">Error</h4>
              </div>
              <div class="modal-body">
                <div id="message" class="alert alert-danger text-center"></div> 
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="errorbutton">Close</button>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="myModalMandatory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title text-center" id="H1">Error</h4>
              </div>
              <div class="modal-body">
                <div id="message2" class="alert alert-danger text-center"></div> 
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="errorbutton2">Close</button>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="modalWait" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body">
                    <div style="text-align:center; margin-bottom: 25px;">
                          <svg class="logo-wanup" aria-hidden="true" width="113" height="33" alt="Wanup Logo">
                            <use id="Use1" xlink:href="#icon-logo-wanup-color"></use>
                        </svg>
                    </div>

                    <div class="progress">
                            <div class="progress-bar progress-bar-danger progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                <span class="sr-only">Loading - Waiting please</span>
                            </div>
                        </div>

              </div>
              <div class="modal-footer">
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="myModalFinish" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title text-center" id="finishlabel">Congratulations</h4>
              </div>
              <div class="modal-body">
                    <div style="text-align:center">
                          <svg class="logo-wanup" aria-hidden="true" width="113" height="33" alt="Wanup Logo">
                            <use id="logo-wanup" xlink:href="#icon-logo-wanup-color"></use>
                        </svg>
                    </div>
                    <div id="finishInformation" style="text-align:center" ><br /><br /></div>
                  <br />
                    <input type="button" id="finishbutton" class="btn btn-success btn-block" value="Continue browsing" /> 
              </div>
              <div class="modal-footer">
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="MyModalWait" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title text-center" id="waitheader">Please, wait a moment</h4>
                    </div>
                    <div class="modal-body">
                        <div class="progress">
                            <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                <span class="sr-only">Waiting please</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div id="accesstypes" style="display:none"></div>
        <div id="freeaccessinformation" style="display:none"></div>
        <div id="socialaccessinformation" style="display:none"></div>
        <input type="text" hidden="hidden" id="idsocialbillintype" />
        <input type="text" hidden="hidden" id="mandatorysurvey" />

</asp:Content>
