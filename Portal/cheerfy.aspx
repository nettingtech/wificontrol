﻿<%@ Page Title="" Language="C#" MasterPageFile="~/resources/master/PortalMaster.Master" AutoEventWireup="true" CodeBehind="cheerfy.aspx.cs" Inherits="Portal.cheerfy" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        $(document).ready(function (e) {
            var parameters = getParameters();
            var lang = navigator.browserLanguage ? navigator.browserLanguage : navigator.language;
            var langcode = lang.substring(0, 2);
            if ((langcode != 'es') && (langcode != 'en'))
                langcode = $("#languagecode").val();

            var dataStringm = 'action=cheerfy&username=' + parameters.username + '&mac=' + $("#mac").val() + '&idhotel=' + $("#idhotel").val() + '&idlocation=' + $("#idlocation").val() + '&origin=' + $("#origin").val() + "&lang=" + langcode;
            $.ajax({
                url: "resources/handlers/PortalHandler.ashx",
                data: dataStringm,
                contentType: "application/json; charset=utf-8",
                dataType: "text",
                beforeSend: function () {
                    $("#MyModalWait").modal('show');
                },
                complete: function () {
                    $("#MyModalWait").modal('hide');
                },
                success: function (pResult) {
                    var lResult = JSON.parse(pResult);
                    if (lResult.IdUser == -3) {
                        if (langcode != 'es') {
                            $("#message").html("We are currently experiencing a problem, please try again later.");
                        }
                        else {
                            $("#message").html("Tenemos un problema temporal, intentelo de nuevo más tarde.");
                        }
                        $("#myModalError").modal('show');
                    }
                    else if (lResult.IdUser == 0) {
                        if (langcode != 'es') {
                            $("#message").html(lResult.UserName);
                        }
                        else {
                            $("#message").html(lResult.UserName);
                        }
                        $("#myModalError").modal('show');
                    }
                    else {
                        window.location.href = lResult.Url;
                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#message2").html("We have a problem, please try again later.");
                }
            });

            $("#finishbutton").click(function (e) {
                e.preventDefault();
                var url = $("#finishbutton").attr("href");
                window.location.href = url;

            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input type="hidden" name="code" id="idhotel" value="" />
        <input type="hidden" name="code" id="idlocation" value="" />
        <input type="hidden" name="code" id="mac" value="" />
        <input type="hidden" name="code" id="origin" value="" />
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">

            </div>
        </div>
    </div>

    <div class="modal fade" id="myModalError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title text-center" id="H2">Error</h4>
          </div>
          <div class="modal-body">
            <div id="message" class="alert alert-danger text-center"></div> 
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="errorbutton">Close</button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="myModalFinish" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title text-center" id="finishlabel">Congratulations</h4>
          </div>
          <div class="modal-body">
                <div id="finishInformation" style="text-align:center" ><img src="#" alt="logo hotel" id="logofinish" style="width:150px"  /><br /><br /></div>
                            <br />
                <input type="button" id="finishbutton" class="btn btn-success btn-block" value="Continue browsing" /> 
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="MyModalWait" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center" id="waitheader">Please, wait a moment</h4>
                </div>
                <div class="modal-body">
                    <div class="progress">
                        <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                            <span class="sr-only">Waiting please</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
