﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Wifi360.MerakiDaemonController {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "14.0.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("check.install.bat")]
        public string ScriptInstallExecutable {
            get {
                return ((string)(this["ScriptInstallExecutable"]));
            }
            set {
                this["ScriptInstallExecutable"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("check.uninstall.bat")]
        public string ScriptUninstallExecutable {
            get {
                return ((string)(this["ScriptUninstallExecutable"]));
            }
            set {
                this["ScriptUninstallExecutable"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Wifi360.MerakiDaemon")]
        public string ServiceName {
            get {
                return ((string)(this["ServiceName"]));
            }
            set {
                this["ServiceName"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Wifi360.MerakiDaemon.exe")]
        public string ServiceExecutable {
            get {
                return ((string)(this["ServiceExecutable"]));
            }
            set {
                this["ServiceExecutable"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("D:\\Dropbox\\WBB\\InternetSolution\\Wifi360.MerakiDaemonController\\bin\\Debug")]
        public string ServicePath {
            get {
                return ((string)(this["ServicePath"]));
            }
            set {
                this["ServicePath"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("D:\\Dropbox\\WBB\\InternetSolution\\Wifi360.MerakiDaemonController\\bin\\Debug")]
        public string ScriptPath {
            get {
                return ((string)(this["ScriptPath"]));
            }
            set {
                this["ScriptPath"] = value;
            }
        }
    }
}
