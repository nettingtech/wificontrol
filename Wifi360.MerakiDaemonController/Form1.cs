﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wifi360.MerakiDaemon.Common;

namespace Wifi360.MerakiDaemonController
{
    public partial class Form1 : Form
    {

        #region variables y constantes

        private bool closing = false;
        private ServiceController _serviceController = null;
        private const string windowTitle = "Wifi360 Meraki API Daemon Controller";

        #endregion


        #region Metodos para controlar el servicio

        private void FindService()
        {
            this.Enabled = false;

            _serviceController = null;

            foreach (ServiceController service in ServiceController.GetServices())
            {
                if (service.ServiceName == Settings.Default.ServiceName)
                {
                    _serviceController = service;
                    return;
                }
            }
        }

        private void StartService()
        {
            _serviceController.Refresh();
            if (_serviceController.Status == ServiceControllerStatus.Stopped)
            {
                _serviceController.Start();
            }
        }

        private void StopService()
        {
            _serviceController.Refresh();
            if (_serviceController.Status == ServiceControllerStatus.Running)
            {
                _serviceController.Stop();
            }
        }

        private void GetServiceConfig(string dirPath, bool show)
        {
            string configPath = string.Format(@"{0}\{1}.config", dirPath, Wifi360.MerakiDaemonController.Settings.Default.ServiceExecutable);
            if (File.Exists(configPath))
            {
                buttonApplyConfig.Enabled = true;
                buttonRestoreConfig.Enabled = true;
                textMonitorInterval.Enabled = true;
                textMonitorInterval.Text = ConfigLowLevel.GetAppSetting(configPath, "TimerInterval");
                textMonitorApiUrl.Text = ConfigLowLevel.GetAppSetting(configPath, "ApiUrl");
                textMonitorApiKey.Text = ConfigLowLevel.GetAppSetting(configPath, "ApiKey");
                textMonitorIntervalSplashLoginAttemps.Text = ConfigLowLevel.GetAppSetting(configPath, "TimerIntervalSplashLoginAttemps");
                textMonitorIntervalAccounting.Text = ConfigLowLevel.GetAppSetting(configPath, "TimerIntervalAccounting");
                textMonitorAccountingTime.Text = ConfigLowLevel.GetAppSetting(configPath, "AccountingInterval");
                checkBoxIsLoging.Checked = Boolean.Parse(ConfigLowLevel.GetAppSetting(configPath, "IsLogin"));
                textMonitorConnectionString.Enabled = true;
                textMonitorConnectionString.Text = ConfigLowLevel.GetConnectionString(configPath, "W360MerakiDaemon");
            }
            else
            {
                if (show)
                    ShowForm();

                buttonApplyConfig.Enabled = false;
                buttonRestoreConfig.Enabled = false;
                textMonitorInterval.Enabled = false;
                textMonitorConnectionString.Enabled = false;

                MessageBox.Show(this, "No se encontró el archivo de configuración del servicio." + Environment.NewLine
                    + "El servicio podría funcionar erráticamente.", "'" + Settings.Default.ServiceExecutable
                    + ".config' no encontrado", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void SetServiceConfig(string dirpath)
        {
            string configPath = string.Format(@"{0}\{1}.config", dirpath, Settings.Default.ServiceExecutable);
            if (File.Exists(configPath))
            {
                ConfigLowLevel.SetAppSetting(configPath, "TimerInterval", textMonitorInterval.Text);
                ConfigLowLevel.SetAppSetting(configPath, "ApiUrl", textMonitorApiUrl.Text);
                ConfigLowLevel.SetAppSetting(configPath, "ApiKey", textMonitorApiKey.Text);
                ConfigLowLevel.SetAppSetting(configPath, "TimerIntervalSplashLoginAttemps", textMonitorIntervalSplashLoginAttemps.Text);
                ConfigLowLevel.SetAppSetting(configPath, "TimerIntervalAccounting", textMonitorIntervalAccounting.Text);
                ConfigLowLevel.SetAppSetting(configPath, "AccountingInterval", textMonitorAccountingTime.Text);
                ConfigLowLevel.SetConnectionString(configPath, "W360MerakiDaemon", textMonitorConnectionString.Text);

            }
            else
            {
                MessageBox.Show(this, "No se encontró el archivo de configuración del servicio." + Environment.NewLine
                    + "El servicio podría funcionar erráticamente.", "'" + Settings.Default.ServiceExecutable
                    + ".config' no encontrado", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        #endregion

        #region Métodos de apoyo a la configuración de la ventana y la aplicación

        private void BindConfig()
        {
            try
            {
                if (File.Exists(@".\" + Wifi360.MerakiDaemonController.Settings.Default.ServiceExecutable))
                {
                    string dirPath = Path.GetDirectoryName(Path.GetFullPath(@".\"));
                    Settings.Default.ServicePath = dirPath;
                    Settings.Default.ScriptPath = dirPath;

                    GetServiceConfig(dirPath, true);
                }

                textServiceName.Text = Wifi360.MerakiDaemonController.Settings.Default.ServiceName;
                textServicePath.Text = Wifi360.MerakiDaemonController.Settings.Default.ServicePath;
                textServiceExecutable.Text = Wifi360.MerakiDaemonController.Settings.Default.ServiceExecutable;
                textScriptPath.Text = Wifi360.MerakiDaemonController.Settings.Default.ScriptPath;
                textScriptInstallExecutable.Text = Wifi360.MerakiDaemonController.Settings.Default.ScriptInstallExecutable;
                textScriptUninstallExecutable.Text = Wifi360.MerakiDaemonController.Settings.Default.ScriptUninstallExecutable;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        private void ConfigureView()
        {
            try
            {
                if (!this.Enabled) this.Enabled = true;

                if (_serviceController == null)
                {
                    this.Text = windowTitle + " -- Servicio no encontrado.";
                    labelEstado.Text = "No encontrado.";
                    buttonStart.Enabled = false;
                    buttonStop.Enabled = false;
                    notifyIcon.Text = windowTitle + " -- Servicio no encontrado.";
                    contextMenu.Items[0].Enabled = false;
                    contextMenu.Items[1].Enabled = false;
                    return;
                }

                _serviceController.Refresh();
                switch (_serviceController.Status)
                {
                    case ServiceControllerStatus.Running:
                    case ServiceControllerStatus.StartPending:
                        this.Text = windowTitle + " -- Servicio ejecutandose.";
                        labelEstado.Text = "Ejecutandose.";
                        buttonStart.Enabled = false;
                        buttonStop.Enabled = true;
                        notifyIcon.Text = windowTitle + " -- Servicio ejecutandose.";
                        contextMenu.Items[0].Enabled = false;
                        contextMenu.Items[1].Enabled = true;
                        break;

                    case ServiceControllerStatus.Stopped:
                    case ServiceControllerStatus.StopPending:
                        this.Text = windowTitle + " -- Servicio parado.";
                        labelEstado.Text = "Parado.";
                        buttonStart.Enabled = true;
                        buttonStop.Enabled = false;
                        notifyIcon.Text = windowTitle + " -- Servicio parado.";
                        contextMenu.Items[0].Enabled = true;
                        contextMenu.Items[1].Enabled = false;
                        break;

                    default:
                        this.Text = windowTitle + " -- Servicio desconocido.";
                        labelEstado.Text = "Desconocido.";
                        buttonStart.Enabled = false;
                        buttonStop.Enabled = false;
                        notifyIcon.Text = windowTitle + " -- Servicio desconocido.";
                        contextMenu.Items[0].Enabled = false;
                        contextMenu.Items[1].Enabled = false;
                        break;
                }
            }
            catch
            {
                throw;
            }
        }

        private void ShowForm()
        {
            if (WindowState == FormWindowState.Minimized)
                WindowState = FormWindowState.Normal;
            ShowInTaskbar = true;
            Show();
        }

        #endregion

        public Form1()
        {
            InitializeComponent();

            WindowState = FormWindowState.Normal;

            BindConfig();
            FindService();

            ConfigureView();

            timer1.Start();
        }


        #region Eventos Formulario

        private void ControllerServiceForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (closing)
            {
                if (MessageBox.Show("¿Esta seguro que desea cerrar el controlador?", string.Empty, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    notifyIcon.Dispose();
                else
                {
                    closing = false;
                    e.Cancel = true;
                    ShowInTaskbar = false;
                    Hide();
                }
            }
            else
            {
                closing = false;
                e.Cancel = true;
                ShowInTaskbar = false;
                Hide();
            }
        }

        private void maximizarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowForm();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            closing = true;
            this.Close();
        }

        private void buttonShowDialog_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.ShowDialog();

            if (!string.IsNullOrEmpty(folderBrowserDialog1.SelectedPath))
            {
                Settings.Default.ServicePath = folderBrowserDialog1.SelectedPath;
                Settings.Default.ScriptPath = folderBrowserDialog1.SelectedPath;

                textServicePath.Text = Settings.Default.ServicePath;
                textScriptPath.Text = Settings.Default.ScriptPath;

                GetServiceConfig(folderBrowserDialog1.SelectedPath, false);
            }
        }

        private void buttonInstall_Click(object sender, EventArgs e)
        {
            try
            {
                string path = string.Format(@"{0}\{1}", Settings.Default.ScriptPath, Settings.Default.ScriptInstallExecutable);
                string ppp = Application.StartupPath;
                string aa = System.IO.Path.Combine(ppp, Settings.Default.ServiceExecutable);


                if (System.IO.File.Exists(path))
                {
                    System.Diagnostics.ProcessStartInfo p = new System.Diagnostics.ProcessStartInfo(path);
                    p.WorkingDirectory = Settings.Default.ScriptPath;

                    System.Diagnostics.Process proc = new System.Diagnostics.Process();
                    proc.StartInfo = p;

                    proc.Start();
                    proc.WaitForExit();

                    FindService();
                    ConfigureView();
                }
                else
                    MessageBox.Show("El instalador no existe." + System.Environment.NewLine + path);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonUninstall_Click(object sender, EventArgs e)
        {
            try
            {
                string path = string.Format(@"{0}\{1}", Settings.Default.ScriptPath, Settings.Default.ScriptUninstallExecutable);
                string ppp = Application.StartupPath;
                string aa = System.IO.Path.Combine(ppp, Settings.Default.ServiceExecutable);


                if (System.IO.File.Exists(path))
                {
                    System.Diagnostics.ProcessStartInfo p = new System.Diagnostics.ProcessStartInfo(path);
                    p.WorkingDirectory = Settings.Default.ScriptPath;

                    System.Diagnostics.Process proc = new System.Diagnostics.Process();
                    proc.StartInfo = p;

                    proc.Start();
                    proc.WaitForExit();

                    FindService();
                    ConfigureView();
                }
                else
                    MessageBox.Show("El desinstalador no existe." + System.Environment.NewLine + path);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            StartService();
            ConfigureView();
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            StopService();
            ConfigureView();
        }

        private void iniciarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StartService();
            ConfigureView();
        }

        private void detenerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StopService();
            ConfigureView();
        }

        private void buttonApplyConfig_Click(object sender, EventArgs e)
        {
            SetServiceConfig(Settings.Default.ServicePath);
        }

        private void buttonRestoreConfig_Click(object sender, EventArgs e)
        {
            GetServiceConfig(Settings.Default.ServicePath, false);
        }

        private void notifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ShowForm();
        }

        #endregion

        #region Timer

        private void timer1_Tick(object sender, EventArgs e)
        {
            ConfigureView();
        }

        #endregion Timer

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (closing)
            {
                if (MessageBox.Show("¿Esta seguro que desea cerrar el controlador?", string.Empty, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    notifyIcon.Dispose();
                else
                {
                    closing = false;
                    e.Cancel = true;
                    ShowInTaskbar = false;
                    Hide();
                }
            }
            else
            {
                closing = false;
                e.Cancel = true;
                ShowInTaskbar = false;
                Hide();
            }
        }
    }
}
