﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wifi360.DaemonController.Common;

namespace Wifi360.DaemonController
{
    public partial class Form1 : Form
    {
        #region variables y constantes

        private bool closing = false;
        private ServiceController _serviceController = null;
        private const string windowTitle = "Wifi360 Daemon Controller";

        #endregion

        public Form1()
        {
            InitializeComponent();

            WindowState = FormWindowState.Normal;

            BindConfig();
            FindService();

            ConfigureView();

            timer1.Start();
        }

        #region Metodos para controlar el servicio

        private void FindService()
        {
            this.Enabled = false;

            _serviceController = null;

            foreach (ServiceController service in ServiceController.GetServices())
            {
                if (service.ServiceName == Properties.Settings.Default.ServiceName)
                {
                    _serviceController = service;
                    return;
                }
            }
        }

        private void StartService()
        {
            _serviceController.Refresh();
            if (_serviceController.Status == ServiceControllerStatus.Stopped)
            {
                _serviceController.Start();
            }
        }

        private void StopService()
        {
            _serviceController.Refresh();
            if (_serviceController.Status == ServiceControllerStatus.Running)
            {
                _serviceController.Stop();
            }
        }

        private void GetServiceConfig(string dirPath, bool show)
        {
            string configPath = string.Format(@"{0}\{1}.config", dirPath, Wifi360.DaemonController.Properties.Settings.Default.ServiceExecutable);
            if (File.Exists(configPath))
            {
                buttonApplyConfig.Enabled = true;
                buttonRestoreConfig.Enabled = true;
                textMonitorInterval.Enabled = true;
                textMonitorInterval.Text = ConfigLowLevel.GetAppSetting(configPath, "TimerInterval");
                textMonitorRadiusIP.Text = ConfigLowLevel.GetAppSetting(configPath, "RadiusIP");
                textMonitorRadiusPort.Text = ConfigLowLevel.GetAppSetting(configPath, "RadiusPort");
                textMonitorIntentos.Text = ConfigLowLevel.GetAppSetting(configPath, "Attemps");
                checkBoxIsLoging.Checked = Boolean.Parse(ConfigLowLevel.GetAppSetting(configPath, "IsLogin"));
                checkBoxTimeStamp.Checked = Boolean.Parse(ConfigLowLevel.GetAppSetting(configPath, "TimeStamp"));
                textMonitorConnectionString.Enabled = true;
                textMonitorConnectionString.Text = ConfigLowLevel.GetConnectionString(configPath, "W360Daemon");
            }
            else
            {
                if (show)
                    ShowForm();

                buttonApplyConfig.Enabled = false;
                buttonRestoreConfig.Enabled = false;
                textMonitorInterval.Enabled = false;
                textMonitorConnectionString.Enabled = false;

                MessageBox.Show(this, "No se encontró el archivo de configuración del servicio." + Environment.NewLine
                    + "El servicio podría funcionar erráticamente.", "'" + Properties.Settings.Default.ServiceExecutable
                    + ".config' no encontrado", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void SetServiceConfig(string dirpath)
        {
            string configPath = string.Format(@"{0}\{1}.config", dirpath, Properties.Settings.Default.ServiceExecutable);
            if (File.Exists(configPath))
            {
                ConfigLowLevel.SetAppSetting(configPath, "TimerInterval", textMonitorInterval.Text);
                ConfigLowLevel.SetAppSetting(configPath, "RadiusIP", textMonitorRadiusIP.Text);
                ConfigLowLevel.SetAppSetting(configPath, "RadiusPort", textMonitorRadiusPort.Text);
                ConfigLowLevel.SetAppSetting(configPath, "Attemps", textMonitorIntentos.Text);
                ConfigLowLevel.SetAppSetting(configPath, "IsLogin", checkBoxIsLoging.Checked.ToString());
                ConfigLowLevel.SetAppSetting(configPath, "TimeStamp", checkBoxTimeStamp.Checked.ToString());
                ConfigLowLevel.SetConnectionString(configPath, "W360Daemon", textMonitorConnectionString.Text);

            }
            else
            {
                MessageBox.Show(this, "No se encontró el archivo de configuración del servicio." + Environment.NewLine
                    + "El servicio podría funcionar erráticamente.", "'" + Properties.Settings.Default.ServiceExecutable
                    + ".config' no encontrado", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        #endregion

        #region Métodos de apoyo a la configuración de la ventana y la aplicación

        private void BindConfig()
        {
            try
            {
                if (File.Exists(@".\" + Wifi360.DaemonController.Properties.Settings.Default.ServiceExecutable))
                {
                    string dirPath = Path.GetDirectoryName(Path.GetFullPath(@".\"));
                    Properties.Settings.Default.ServicePath = dirPath;
                    Properties.Settings.Default.ScriptPath = dirPath;

                    GetServiceConfig(dirPath, true);
                }

                textServiceName.Text = Wifi360.DaemonController.Properties.Settings.Default.ServiceName;
                textServicePath.Text = Wifi360.DaemonController.Properties.Settings.Default.ServicePath;
                textServiceExecutable.Text = Wifi360.DaemonController.Properties.Settings.Default.ServiceExecutable;
                textScriptPath.Text = Wifi360.DaemonController.Properties.Settings.Default.ScriptPath;
                textScriptInstallExecutable.Text = Wifi360.DaemonController.Properties.Settings.Default.ScriptInstallExecutable;
                textScriptUninstallExecutable.Text = Wifi360.DaemonController.Properties.Settings.Default.ScriptUninstallExecutable;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        private void ConfigureView()
        {
            try
            {
                if (!this.Enabled) this.Enabled = true;

                if (_serviceController == null)
                {
                    this.Text = windowTitle + " -- Servicio no encontrado.";
                    labelEstado.Text = "No encontrado.";
                    buttonStart.Enabled = false;
                    buttonStop.Enabled = false;
                    notifyIcon.Text = windowTitle + " -- Servicio no encontrado.";
                    contextMenu.Items[0].Enabled = false;
                    contextMenu.Items[1].Enabled = false;
                    return;
                }

                _serviceController.Refresh();
                switch (_serviceController.Status)
                {
                    case ServiceControllerStatus.Running:
                    case ServiceControllerStatus.StartPending:
                        this.Text = windowTitle + " -- Servicio ejecutandose.";
                        labelEstado.Text = "Ejecutandose.";
                        buttonStart.Enabled = false;
                        buttonStop.Enabled = true;
                        notifyIcon.Text = windowTitle + " -- Servicio ejecutandose.";
                        contextMenu.Items[0].Enabled = false;
                        contextMenu.Items[1].Enabled = true;
                        break;

                    case ServiceControllerStatus.Stopped:
                    case ServiceControllerStatus.StopPending:
                        this.Text = windowTitle + " -- Servicio parado.";
                        labelEstado.Text = "Parado.";
                        buttonStart.Enabled = true;
                        buttonStop.Enabled = false;
                        notifyIcon.Text = windowTitle + " -- Servicio parado.";
                        contextMenu.Items[0].Enabled = true;
                        contextMenu.Items[1].Enabled = false;
                        break;

                    default:
                        this.Text = windowTitle + " -- Servicio desconocido.";
                        labelEstado.Text = "Desconocido.";
                        buttonStart.Enabled = false;
                        buttonStop.Enabled = false;
                        notifyIcon.Text = windowTitle + " -- Servicio desconocido.";
                        contextMenu.Items[0].Enabled = false;
                        contextMenu.Items[1].Enabled = false;
                        break;
                }
            }
            catch
            {
                throw;
            }
        }

        private void ShowForm()
        {
            if (WindowState == FormWindowState.Minimized)
                WindowState = FormWindowState.Normal;
            ShowInTaskbar = true;
            Show();
        }

        #endregion

        #region Eventos Formulario

        private void ControllerServiceForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (closing)
            {
                if (MessageBox.Show("¿Esta seguro que desea cerrar el controlador?", string.Empty, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    notifyIcon.Dispose();
                else
                {
                    closing = false;
                    e.Cancel = true;
                    ShowInTaskbar = false;
                    Hide();
                }
            }
            else
            {
                closing = false;
                e.Cancel = true;
                ShowInTaskbar = false;
                Hide();
            }
        }

        private void maximizarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowForm();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            closing = true;
            this.Close();
        }

        private void buttonShowDialog_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.ShowDialog();

            if (!string.IsNullOrEmpty(folderBrowserDialog1.SelectedPath))
            {
                Properties.Settings.Default.ServicePath = folderBrowserDialog1.SelectedPath;
                Properties.Settings.Default.ScriptPath = folderBrowserDialog1.SelectedPath;

                textServicePath.Text = Properties.Settings.Default.ServicePath;
                textScriptPath.Text = Properties.Settings.Default.ScriptPath;

                GetServiceConfig(folderBrowserDialog1.SelectedPath, false);
            }
        }

        private void buttonInstall_Click(object sender, EventArgs e)
        {
            try
            {
                string path = string.Format(@"{0}\{1}", Properties.Settings.Default.ScriptPath, Properties.Settings.Default.ScriptInstallExecutable);
                string ppp = Application.StartupPath;
                string aa = System.IO.Path.Combine(ppp, Properties.Settings.Default.ServiceExecutable);


                if (System.IO.File.Exists(path))
                {
                    System.Diagnostics.ProcessStartInfo p = new System.Diagnostics.ProcessStartInfo(path);
                    p.WorkingDirectory = Properties.Settings.Default.ScriptPath;

                    System.Diagnostics.Process proc = new System.Diagnostics.Process();
                    proc.StartInfo = p;

                    proc.Start();
                    proc.WaitForExit();

                    FindService();
                    ConfigureView();
                }
                else
                    MessageBox.Show("El instalador no existe." + System.Environment.NewLine + path);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonUninstall_Click(object sender, EventArgs e)
        {
            try
            {
                string path = string.Format(@"{0}\{1}", Properties.Settings.Default.ScriptPath, Properties.Settings.Default.ScriptUninstallExecutable);
                string ppp = Application.StartupPath;
                string aa = System.IO.Path.Combine(ppp, Properties.Settings.Default.ServiceExecutable);


                if (System.IO.File.Exists(path))
                {
                    System.Diagnostics.ProcessStartInfo p = new System.Diagnostics.ProcessStartInfo(path);
                    p.WorkingDirectory = Properties.Settings.Default.ScriptPath;

                    System.Diagnostics.Process proc = new System.Diagnostics.Process();
                    proc.StartInfo = p;

                    proc.Start();
                    proc.WaitForExit();

                    FindService();
                    ConfigureView();
                }
                else
                    MessageBox.Show("El desinstalador no existe." + System.Environment.NewLine + path);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            StartService();
            ConfigureView();
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            StopService();
            ConfigureView();
        }

        private void iniciarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StartService();
            ConfigureView();
        }

        private void detenerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StopService();
            ConfigureView();
        }

        private void buttonApplyConfig_Click(object sender, EventArgs e)
        {
            SetServiceConfig(Properties.Settings.Default.ServicePath);
        }

        private void buttonRestoreConfig_Click(object sender, EventArgs e)
        {
            GetServiceConfig(Properties.Settings.Default.ServicePath, false);
        }

        private void notifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ShowForm();
        }

        #endregion

        #region Timer

        private void timer1_Tick(object sender, EventArgs e)
        {
            ConfigureView();
        }

        #endregion Timer

    }
}
