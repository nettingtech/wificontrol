﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wifi360.DaemonController.Common
{
    [Serializable()]
    public class CollectionBase<T> : List<T> where T : BusinessBase
    {
        #region Lock

        public void InsertFirstWithLock(T obj)
        {
            lock (this)
            {
                Insert(0, obj);
            }
        }

        public void InsertWithLock(T obj)
        {
            lock (this)
            {
                Add(obj);
            }
        }

        public BusinessBase RemoveFirstWithLock()
        {
            BusinessBase obj = default(BusinessBase);
            lock (this)
            {
                if (Count > 0)
                {
                    obj = this[0];
                    RemoveAt(0);
                    return obj;
                }
            }
            return obj;
        }

        public void RemoveWithLock(T obj)
        {
            lock (this)
            {
                if (Count > 0)
                {
                    if (this.Contains(obj))
                        this.Remove(obj);
                }
            }
        }

        #endregion

        #region IsDirty, IsValid

        public bool IsDirty
        {
            get
            {
                //if (deletedList.Count > 0)
                //    return true;

                //foreach (BusinessBase child in this)
                //    if (child.IsDirty)
                return true;
            }
        }

        virtual public bool IsValid
        {
            get
            {
                //foreach (BusinessBase child in this)
                //    if (!child.IsValid)
                //        return false;
                return true;
            }
        }

        #endregion

        #region Delete and Undelete child

        private void Delete(T obj)
        {
            obj.Delete();
            deletedList.Add(obj);
        }

        private void UnDelete(T obj)
        {
            this.Add(obj);
            deletedList.Remove(obj);
        }

        #endregion

        #region DeletedCollection

        [Serializable()]
        protected class DeletedCollection : CollectionBase
        {
            public void Add(BusinessBase child)
            {
                List.Add(child);
            }

            public void Remove(BusinessBase child)
            {
                List.Remove(child);
            }

            public BusinessBase this[int index]
            {
                get
                {
                    return (BusinessBase)List[index];
                }
            }
        }

        protected DeletedCollection deletedList = new DeletedCollection();

        public IEnumerable GetDeletedItems()
        {
            return deletedList;
        }

        #endregion

        #region Insert, Remove, Clear

        public new void Remove(T obj)
        {
            Delete(obj);
            base.Remove(obj);
        }

        #endregion

        #region Data Access

        virtual public void Save()
        {
            //if (this.IsChild)
            //    throw new NotSupportedException("Can not directly save a child object");

            if (!IsValid)
                throw new Exception("Object is not valid and can not be saved");

            if (IsDirty)
                this.Business_Update();
        }

        virtual protected void Business_Create(object criteria)
        {
            throw new NotSupportedException("Invalid operation - create not allowed");
        }

        virtual protected void Business_Fetch(object criteria)
        {
            throw new NotSupportedException("Invalid operation - fetch not allowed");
        }

        virtual protected void Business_Update()
        {
            throw new NotSupportedException("Invalid operation - update not allowed");
        }

        virtual protected void Business_Delete(object criteria)
        {
            throw new NotSupportedException("Invalid operation - delete not allowed");
        }

        #endregion
    }
}
