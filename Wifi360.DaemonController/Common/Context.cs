﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wifi360.DaemonController.Common
{
    public class Context
    {
        #region Variables

        private static Log _log = null;
        private static string _appName = string.Empty;

        #endregion

        #region Properties

        public static Log Log
        {
            set { _log = value; }
            get { return _log; }
        }

        public static string AppName
        {
            set { _appName = value; }
            get
            {
                _appName = ConfigurationManager.AppSettings["AppName"].ToString();
                return _appName;
            }
        }

        #endregion
    }
}
