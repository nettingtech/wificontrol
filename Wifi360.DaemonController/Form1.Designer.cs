﻿namespace Wifi360.DaemonController
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.checkBoxIsLoging = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.textMonitorIntentos = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textMonitorRadiusPort = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textMonitorRadiusIP = new System.Windows.Forms.TextBox();
            this.buttonRestoreConfig = new System.Windows.Forms.Button();
            this.buttonApplyConfig = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.textMonitorConnectionString = new System.Windows.Forms.TextBox();
            this.textMonitorInterval = new System.Windows.Forms.TextBox();
            this.labelEstado = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.buttonStop = new System.Windows.Forms.Button();
            this.buttonStart = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textScriptPath = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonServicePath = new System.Windows.Forms.Button();
            this.textScriptUninstallExecutable = new System.Windows.Forms.TextBox();
            this.textScriptInstallExecutable = new System.Windows.Forms.TextBox();
            this.textServicePath = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textServiceExecutable = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonUninstall = new System.Windows.Forms.Button();
            this.buttonInstall = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textServiceName = new System.Windows.Forms.TextBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.iniciarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.detenerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.maximizarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.checkBoxTimeStamp = new System.Windows.Forms.CheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.contextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Controls.Add(this.tabPage2);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.HotTrack = true;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Multiline = true;
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(586, 341);
            this.tabControl.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl.TabIndex = 5;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox21);
            this.tabPage1.Controls.Add(this.labelEstado);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.buttonStop);
            this.tabPage1.Controls.Add(this.buttonStart);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(578, 315);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Servicio";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this.checkBoxTimeStamp);
            this.groupBox21.Controls.Add(this.label13);
            this.groupBox21.Controls.Add(this.checkBoxIsLoging);
            this.groupBox21.Controls.Add(this.label12);
            this.groupBox21.Controls.Add(this.label11);
            this.groupBox21.Controls.Add(this.textMonitorIntentos);
            this.groupBox21.Controls.Add(this.label10);
            this.groupBox21.Controls.Add(this.textMonitorRadiusPort);
            this.groupBox21.Controls.Add(this.label9);
            this.groupBox21.Controls.Add(this.textMonitorRadiusIP);
            this.groupBox21.Controls.Add(this.buttonRestoreConfig);
            this.groupBox21.Controls.Add(this.buttonApplyConfig);
            this.groupBox21.Controls.Add(this.label14);
            this.groupBox21.Controls.Add(this.label8);
            this.groupBox21.Controls.Add(this.textMonitorConnectionString);
            this.groupBox21.Controls.Add(this.textMonitorInterval);
            this.groupBox21.Location = new System.Drawing.Point(7, 50);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(562, 257);
            this.groupBox21.TabIndex = 19;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "Parámetros del Servicio";
            // 
            // checkBoxIsLoging
            // 
            this.checkBoxIsLoging.AutoSize = true;
            this.checkBoxIsLoging.Location = new System.Drawing.Point(122, 152);
            this.checkBoxIsLoging.Name = "checkBoxIsLoging";
            this.checkBoxIsLoging.Size = new System.Drawing.Size(15, 14);
            this.checkBoxIsLoging.TabIndex = 37;
            this.checkBoxIsLoging.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(5, 152);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 13);
            this.label12.TabIndex = 36;
            this.label12.Text = "Guardar log:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(5, 126);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(48, 13);
            this.label11.TabIndex = 34;
            this.label11.Text = "Intentos:";
            // 
            // textMonitorIntentos
            // 
            this.textMonitorIntentos.Location = new System.Drawing.Point(122, 123);
            this.textMonitorIntentos.Name = "textMonitorIntentos";
            this.textMonitorIntentos.Size = new System.Drawing.Size(62, 20);
            this.textMonitorIntentos.TabIndex = 33;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(5, 100);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 13);
            this.label10.TabIndex = 32;
            this.label10.Text = "Radius Port:";
            // 
            // textMonitorRadiusPort
            // 
            this.textMonitorRadiusPort.Location = new System.Drawing.Point(122, 97);
            this.textMonitorRadiusPort.Name = "textMonitorRadiusPort";
            this.textMonitorRadiusPort.Size = new System.Drawing.Size(62, 20);
            this.textMonitorRadiusPort.TabIndex = 31;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(5, 74);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 13);
            this.label9.TabIndex = 30;
            this.label9.Text = "Radius IP:";
            // 
            // textMonitorRadiusIP
            // 
            this.textMonitorRadiusIP.Location = new System.Drawing.Point(122, 71);
            this.textMonitorRadiusIP.Name = "textMonitorRadiusIP";
            this.textMonitorRadiusIP.Size = new System.Drawing.Size(434, 20);
            this.textMonitorRadiusIP.TabIndex = 29;
            // 
            // buttonRestoreConfig
            // 
            this.buttonRestoreConfig.Location = new System.Drawing.Point(379, 228);
            this.buttonRestoreConfig.Name = "buttonRestoreConfig";
            this.buttonRestoreConfig.Size = new System.Drawing.Size(75, 23);
            this.buttonRestoreConfig.TabIndex = 28;
            this.buttonRestoreConfig.Text = "Restaurar";
            this.buttonRestoreConfig.UseVisualStyleBackColor = true;
            this.buttonRestoreConfig.Click += new System.EventHandler(this.buttonRestoreConfig_Click);
            // 
            // buttonApplyConfig
            // 
            this.buttonApplyConfig.Location = new System.Drawing.Point(456, 228);
            this.buttonApplyConfig.Name = "buttonApplyConfig";
            this.buttonApplyConfig.Size = new System.Drawing.Size(100, 23);
            this.buttonApplyConfig.TabIndex = 19;
            this.buttonApplyConfig.Text = "Guardar / Aplicar";
            this.buttonApplyConfig.UseVisualStyleBackColor = true;
            this.buttonApplyConfig.Click += new System.EventHandler(this.buttonApplyConfig_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(5, 48);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(109, 13);
            this.label14.TabIndex = 18;
            this.label14.Text = "Cadena de Conexión:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(5, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "Intervalo (seg):";
            // 
            // textMonitorConnectionString
            // 
            this.textMonitorConnectionString.Location = new System.Drawing.Point(122, 45);
            this.textMonitorConnectionString.Name = "textMonitorConnectionString";
            this.textMonitorConnectionString.Size = new System.Drawing.Size(434, 20);
            this.textMonitorConnectionString.TabIndex = 17;
            // 
            // textMonitorInterval
            // 
            this.textMonitorInterval.Location = new System.Drawing.Point(122, 19);
            this.textMonitorInterval.Name = "textMonitorInterval";
            this.textMonitorInterval.Size = new System.Drawing.Size(62, 20);
            this.textMonitorInterval.TabIndex = 6;
            // 
            // labelEstado
            // 
            this.labelEstado.AutoSize = true;
            this.labelEstado.Location = new System.Drawing.Point(232, 12);
            this.labelEstado.Name = "labelEstado";
            this.labelEstado.Size = new System.Drawing.Size(0, 13);
            this.labelEstado.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(182, 12);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Servicio :";
            // 
            // buttonStop
            // 
            this.buttonStop.Location = new System.Drawing.Point(89, 6);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(75, 23);
            this.buttonStop.TabIndex = 1;
            this.buttonStop.Text = "Parar";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.buttonStop_Click);
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(8, 6);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(75, 23);
            this.buttonStart.TabIndex = 0;
            this.buttonStart.Text = "Iniciar";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(578, 267);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Instalación";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.textScriptPath);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.buttonServicePath);
            this.groupBox2.Controls.Add(this.textScriptUninstallExecutable);
            this.groupBox2.Controls.Add(this.textScriptInstallExecutable);
            this.groupBox2.Controls.Add(this.textServicePath);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.textServiceExecutable);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.buttonUninstall);
            this.groupBox2.Controls.Add(this.buttonInstall);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.textServiceName);
            this.groupBox2.Location = new System.Drawing.Point(7, 9);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(564, 236);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = " Datos de la instalación del servicio ";
            // 
            // textScriptPath
            // 
            this.textScriptPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textScriptPath.Location = new System.Drawing.Point(155, 100);
            this.textScriptPath.Name = "textScriptPath";
            this.textScriptPath.Size = new System.Drawing.Size(402, 20);
            this.textScriptPath.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 103);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(142, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Ruta de los ficheros por lote:";
            // 
            // buttonServicePath
            // 
            this.buttonServicePath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonServicePath.Location = new System.Drawing.Point(528, 46);
            this.buttonServicePath.Name = "buttonServicePath";
            this.buttonServicePath.Size = new System.Drawing.Size(29, 23);
            this.buttonServicePath.TabIndex = 3;
            this.buttonServicePath.Text = "...";
            this.buttonServicePath.UseVisualStyleBackColor = true;
            this.buttonServicePath.Click += new System.EventHandler(this.buttonShowDialog_Click);
            // 
            // textScriptUninstallExecutable
            // 
            this.textScriptUninstallExecutable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textScriptUninstallExecutable.Enabled = false;
            this.textScriptUninstallExecutable.Location = new System.Drawing.Point(155, 158);
            this.textScriptUninstallExecutable.Name = "textScriptUninstallExecutable";
            this.textScriptUninstallExecutable.Size = new System.Drawing.Size(402, 20);
            this.textScriptUninstallExecutable.TabIndex = 7;
            // 
            // textScriptInstallExecutable
            // 
            this.textScriptInstallExecutable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textScriptInstallExecutable.Enabled = false;
            this.textScriptInstallExecutable.Location = new System.Drawing.Point(155, 131);
            this.textScriptInstallExecutable.Name = "textScriptInstallExecutable";
            this.textScriptInstallExecutable.Size = new System.Drawing.Size(402, 20);
            this.textScriptInstallExecutable.TabIndex = 6;
            // 
            // textServicePath
            // 
            this.textServicePath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textServicePath.Location = new System.Drawing.Point(155, 47);
            this.textServicePath.Name = "textServicePath";
            this.textServicePath.Size = new System.Drawing.Size(373, 20);
            this.textServicePath.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(17, 161);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(129, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Nombre del desinstalador:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(36, 134);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Nombre de instalador:";
            // 
            // textServiceExecutable
            // 
            this.textServiceExecutable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textServiceExecutable.Enabled = false;
            this.textServiceExecutable.Location = new System.Drawing.Point(155, 74);
            this.textServiceExecutable.Name = "textServiceExecutable";
            this.textServiceExecutable.Size = new System.Drawing.Size(402, 20);
            this.textServiceExecutable.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(31, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(116, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Nombre del ejecutable:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(57, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Ruta del servicio:";
            // 
            // buttonUninstall
            // 
            this.buttonUninstall.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonUninstall.Location = new System.Drawing.Point(482, 184);
            this.buttonUninstall.Name = "buttonUninstall";
            this.buttonUninstall.Size = new System.Drawing.Size(75, 23);
            this.buttonUninstall.TabIndex = 9;
            this.buttonUninstall.Text = "Desintalar";
            this.buttonUninstall.UseVisualStyleBackColor = true;
            this.buttonUninstall.Click += new System.EventHandler(this.buttonUninstall_Click);
            // 
            // buttonInstall
            // 
            this.buttonInstall.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonInstall.Location = new System.Drawing.Point(401, 184);
            this.buttonInstall.Name = "buttonInstall";
            this.buttonInstall.Size = new System.Drawing.Size(75, 23);
            this.buttonInstall.TabIndex = 8;
            this.buttonInstall.Text = "Instalar";
            this.buttonInstall.UseVisualStyleBackColor = true;
            this.buttonInstall.Click += new System.EventHandler(this.buttonInstall_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(45, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nombre del servicio:";
            // 
            // textServiceName
            // 
            this.textServiceName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textServiceName.Enabled = false;
            this.textServiceName.Location = new System.Drawing.Point(155, 20);
            this.textServiceName.Name = "textServiceName";
            this.textServiceName.Size = new System.Drawing.Size(402, 20);
            this.textServiceName.TabIndex = 1;
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.iniciarToolStripMenuItem,
            this.detenerToolStripMenuItem,
            this.toolStripSeparator1,
            this.maximizarToolStripMenuItem,
            this.salirToolStripMenuItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(129, 98);
            // 
            // iniciarToolStripMenuItem
            // 
            this.iniciarToolStripMenuItem.Name = "iniciarToolStripMenuItem";
            this.iniciarToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.iniciarToolStripMenuItem.Text = "Iniciar";
            this.iniciarToolStripMenuItem.Click += new System.EventHandler(this.iniciarToolStripMenuItem_Click);
            // 
            // detenerToolStripMenuItem
            // 
            this.detenerToolStripMenuItem.Name = "detenerToolStripMenuItem";
            this.detenerToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.detenerToolStripMenuItem.Text = "Detener";
            this.detenerToolStripMenuItem.Click += new System.EventHandler(this.detenerToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(125, 6);
            // 
            // maximizarToolStripMenuItem
            // 
            this.maximizarToolStripMenuItem.Name = "maximizarToolStripMenuItem";
            this.maximizarToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.maximizarToolStripMenuItem.Text = "Maximizar";
            this.maximizarToolStripMenuItem.Click += new System.EventHandler(this.maximizarToolStripMenuItem_Click);
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.salirToolStripMenuItem.Text = "Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // notifyIcon
            // 
            this.notifyIcon.ContextMenuStrip = this.contextMenu;
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "Wifi360 Daemon Controller";
            this.notifyIcon.Visible = true;
            // 
            // checkBoxTimeStamp
            // 
            this.checkBoxTimeStamp.AutoSize = true;
            this.checkBoxTimeStamp.Location = new System.Drawing.Point(122, 174);
            this.checkBoxTimeStamp.Name = "checkBoxTimeStamp";
            this.checkBoxTimeStamp.Size = new System.Drawing.Size(15, 14);
            this.checkBoxTimeStamp.TabIndex = 39;
            this.checkBoxTimeStamp.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(5, 174);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(96, 13);
            this.label13.TabIndex = 38;
            this.label13.Text = "Enviar TimeStamp:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(586, 341);
            this.Controls.Add(this.tabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Wifi360 Daemon Controller";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ControllerServiceForm_FormClosing);
            this.tabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.contextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.Button buttonRestoreConfig;
        private System.Windows.Forms.Button buttonApplyConfig;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textMonitorConnectionString;
        private System.Windows.Forms.TextBox textMonitorInterval;
        private System.Windows.Forms.Label labelEstado;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buttonServicePath;
        private System.Windows.Forms.TextBox textScriptUninstallExecutable;
        private System.Windows.Forms.TextBox textScriptInstallExecutable;
        private System.Windows.Forms.TextBox textServicePath;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textServiceExecutable;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonUninstall;
        private System.Windows.Forms.Button buttonInstall;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textServiceName;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.ToolStripMenuItem iniciarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem detenerToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem maximizarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.TextBox textScriptPath;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textMonitorRadiusPort;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textMonitorRadiusIP;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textMonitorIntentos;
        private System.Windows.Forms.CheckBox checkBoxIsLoging;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox checkBoxTimeStamp;
        private System.Windows.Forms.Label label13;
    }
}

