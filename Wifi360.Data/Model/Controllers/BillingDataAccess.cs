﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Wifi360.Data.Model;
using Wifi360.Data.Common;
using Wifi360.Data.Class;
using System.Transactions;
using System.Data.SqlClient;

namespace Wifi360.Data.Controllers
{
    public class BillingDataAccess
    {
        #region Hoteles
        
        /// <summary>
        /// Seleccionar un hotel 
        /// </summary>
        /// <param name="obj">Objeto del tipo hotel donde se devolveran los datos del hotels</param>
        /// <param name="idHotel">Identificador del hotel a seleccionar</param>
        public void Select(Hotels obj, int idHotel)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.Hotels
                               where h.IdHotel.Equals(idHotel)
                               select h).SingleOrDefault();



                if (element != null)
                {
                    Util.Map(element, obj);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="nseid"></param>
        public void Select(Hotels obj, string nseid)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
               var element = (from h in context.Hotels
                               where h.NSE.Contains(nseid)
                               select h).SingleOrDefault();

                if (element != null)
                {
                    Util.Map(element, obj);
                }
            }
        }

        /// <summary>
        /// Insertamos un nuevo hotel en la base de datos
        /// </summary>
        /// <param name="obj">Elemento del tipo Hotel a insertar</param>
        public void Insert(Hotels obj)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                context.Hotels.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        /// <summary>
        /// Actualizamos los datos de un hotel existente
        /// </summary>
        /// <param name="obj">Elemento del tipo Hotel con los datos a actualizar</param>
        public void Update(Hotels obj)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.Hotels
                               where h.IdHotel.Equals(obj.IdHotel)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(obj, element);

                context.SubmitChanges();
            }
        }

        /// <summary>
        /// Seleccionar el listado de todos los hotels
        /// </summary>
        /// <returns></returns>
        public List<Hotels> SelectHoteles()
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.Hotels
                                select h);

                List<Hotels> listado = new List<Hotels>();

                foreach (var element in elements)
                {
                    Hotels obj = new Hotels();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }
        
        /// <summary>
        /// Seleccionar un listado de todos lo hoteles de una cadena
        /// </summary>
        /// <param name="idChain">Identificador de la cadena de hoteles</param>
        /// <returns></returns>
        public List<Hotels> SelectHoteles(string nseid)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.Hotels
                                where h.NSE.Contains(nseid)
                                select h);

                List<Hotels> listado = new List<Hotels>();

                foreach (var element in elements)
                {
                    Hotels obj = new Hotels();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<Hotels> SelectSitesByParent(int idParent)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from t in context.Hotels
                                where t.IdParent.Equals(idParent)
                                select t);

                List<Hotels> listado = new List<Hotels>();

                foreach (var element in elements)
                {
                    Hotels obj = new Hotels();
                    Util.Map(elements, obj);

                
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public void Update(FDSGroupsMembers obj)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.FDSGroupsMembers
                               where h.Id.Equals(obj.IdGroup)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(obj, element);

                context.SubmitChanges();
            }
        }

        public void Update(FDSGroups obj)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.FDSGroups
                               where h.IdGroup.Equals(obj.IdGroup)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(obj, element);

                context.SubmitChanges();
            }
        }

        public List<Hotels> SelectHotelesPorGrupo(int IdGroup)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from t in context.Hotels
                                join d in context.FDSGroupsMembers on t.IdHotel equals d.IdMember
                                where d.IdMemberType.Equals(2) && d.IdGroup.Equals(IdGroup)
                                select t).ToList();

                List<Hotels> listado = new List<Hotels>();

                foreach (var element in elements)
                {
                    Hotels obj = new Hotels();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<Hotels> SelectHoteles(string name, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.Hotels
                                where h.Name.Contains(name)
                                select h);


                List<Hotels> listado = new List<Hotels>();
                int count = elements.Count();
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                elements = elements.Skip((pageIndex) * pageSize).Take(pageSize);

                foreach (var element in elements)
                {
                    Hotels obj = new Hotels();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        #endregion

        #region FDSGroups

        public void Select(FDSGroups obj, int idGroup)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.FDSGroups
                               where h.IdGroup.Equals(idGroup)
                               select h).SingleOrDefault();

                if (element != null)
                {
                    Util.Map(element, obj);
                    //obj.IdGroup = element.IdGroup;
                    //obj.Name = element.Name;
                }
            }
        }

        public void SelectRealmShareGroup(FDSGroups obj, int idSite)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from t in context.FDSGroups
                               join d in context.FDSGroupsMembers on t.IdGroup equals d.IdGroup
                               where d.IdMember.Equals(idSite) && t.RealmShare.Equals(true)
                               select t).FirstOrDefault();

                if (element != null)
                {
                    Util.Map(element, obj);
                    //obj.IdGroup = element.IdGroup;
                    //obj.Name = element.Name;
                }
            }
        }

        public List<Hotels> SelectSitesGroup(int idGroup)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (context.SP_360_SELECTSITESBYGROUP(idGroup));

                List<Hotels> listado = new List<Hotels>();

                foreach (var element in elements)
                {
                    Hotels obj = new Hotels();
                    obj.IdHotel = element.IdHotel;
                    obj.Name = element.Name;
                    obj.NSE = element.NSE;
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<FDSGroups> SelectGroupsByParent(int idParent)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from t in context.FDSGroups
                                where t.IdGroup.Equals(idParent)
                                select t);

                List<FDSGroups> listado = new List<FDSGroups>();

                foreach (var element in elements)
                {
                    FDSGroups obj = new FDSGroups();
                    obj.IdGroup = element.IdGroup;
                    obj.Name = element.Name;

                    listado.Add(obj);
                }

                return listado;
            }
        }

        public void Insert(FDSGroups obj)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                context.FDSGroups.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        public void Insert(FDSGroupsMembers obj)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                context.FDSGroupsMembers.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        public List<FDSGroups> SelectGroups()
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from t in context.FDSGroups
                                orderby t.Name
                                select t);

                List<FDSGroups> listado = new List<FDSGroups>();

                foreach (var element in elements)
                {
                    FDSGroups obj = new FDSGroups();
                    Util.Map(element, obj);

                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<FDSGroups> SelectGroups(int IdGroup)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from t in context.FDSGroups
                                join d in context.FDSGroupsMembers on t.IdGroup equals d.IdMember
                                where d.IdGroup.Equals(IdGroup)
                                select t).ToList();

                List<FDSGroups> listado = new List<FDSGroups>();

                foreach (var element in elements)
                {
                    FDSGroups obj = new FDSGroups();
                    Util.Map(element, obj);

                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<FDSGroups> SelectGroups(string name, int pageIndex, int pageSize, ref int pageNumber)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {

                var elements = (from h in context.FDSGroups
                                where h.Name.Contains(name)
                                select h);


                List<FDSGroups> listado = new List<FDSGroups>();
                int count = elements.Count();
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                elements = elements.Skip((pageIndex) * pageSize).Take(pageSize);

                foreach (var element in elements)
                {
                    FDSGroups obj = new FDSGroups();
                    Util.Map(element, obj);

                    listado.Add(obj);
                }

                return listado;

            }
        }

        #endregion

        #region FDSGroupMembers

        public void SelectFDSGroupMemberByIdMember(FDSGroupsMembers obj, int IdMember)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from t in context.FDSGroupsMembers
                               where t.IdMember.Equals(IdMember)
                               select t).FirstOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        public List<FDSGroupsMembers> SelectFDSGroupMembers(int idGroup)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from t in context.FDSGroupsMembers
                                where t.IdGroup.Equals(idGroup)
                                select t);

                List<FDSGroupsMembers> listado = new List<FDSGroupsMembers>();

                foreach (var element in elements)
                {
                    FDSGroupsMembers obj = new FDSGroupsMembers();
                    Util.Map(element, obj);
                    //obj.Id = element.Id;
                    //obj.IdGroup = element.IdGroup;
                    //obj.IdMember = element.IdMember;
                    //obj.IdMemberType = element.IdMemberType;
                    //obj.IsParent = element.IsParent;

                    listado.Add(obj);
                }

                return listado;
            }
        }

        public void DeleteGroupMember(int id)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.FDSGroupsMembers
                                where h.Id.Equals(id)
                                select h);

                if (elements != null)
                    foreach (var element in elements)
                    {
                        context.FDSGroupsMembers.DeleteOnSubmit(element);
                    }
                context.SubmitChanges();
            }
        }


        #endregion

        #region BillingModule

        /// <summary>
        /// Seleccionar un BillingModule 
        /// </summary>
        /// <param name="obj">Objeto del tipo BillingModule donde se devolveran los datos del BillingModule</param>
        /// <param name="idHotel">Identificador del BillingModule a seleccionar</param>
        public void Select(BillingModules obj, int idBillingModule)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.BillingModules
                               where h.IdBillingModule.Equals(idBillingModule)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        /// <summary>
        /// Insertamos un nuevo BillingModule en la base de datos
        /// </summary>
        /// <param name="obj">Elemento del tipo BillingModule a insertar</param>
        public void Insert(BillingModules obj)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                context.BillingModules.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        /// <summary>
        /// Actualizamos los datos de un BillingModule existente
        /// </summary>
        /// <param name="obj">Elemento del tipo BillingModule con los datos a actualizar</param>
        public void Update(BillingModules obj)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.BillingModules
                               where h.IdBillingModule.Equals(obj.IdBillingModule)
                               select h).SingleOrDefault();

                if (element != null)
                {
                    element.IdBillingModule = obj.IdBillingModule;
                    element.Name = obj.Name;

                }

                context.SubmitChanges();
            }
        }

        /// <summary>
        /// Seleccionar el listado de todos los BillingModule
        /// </summary>
        /// <returns></returns>
        public List<BillingModules> SelectBillingModule()
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.BillingModules
                                select h);

                List<BillingModules> listado = new List<BillingModules>();

                foreach (var element in elements)
                {
                    BillingModules obj = new BillingModules();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        #endregion
        
        #region BillingType

        /// <summary>
        /// Seleccionar un BillingType 
        /// </summary>
        /// <param name="obj">Objeto del tipo BillingType donde se devolveran los datos del BillingType</param>
        /// <param name="idHotel">Identificador del BillingType a seleccionar</param>
        public void Select(BillingTypes obj, int idBillingType)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.BillingTypes
                               where h.IdBillingType.Equals(idBillingType)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        public void Select(BillingTypes2 obj, int idBillingType)
        {

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var element = (from h in context.BillingTypes2
                               where h.IdBillingType.Equals(idBillingType)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        /// Seleccionar un BillingType 
        /// </summary>
        /// <param name="obj">Objeto del tipo BillingType donde se devolveran los datos del BillingType</param>
        /// <param name="idhotel">Identificador del Si es  a seleccionar</param>
        /// <param name="defualtFast">Identificador del Si es  a seleccionar</param>
        public void Select(BillingTypes obj, int idhotel, bool defaultFast)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.BillingTypes
                               where h.IdHotel.Equals(idhotel) && h.DefaultFastTicket.Equals(defaultFast)
                               select h).FirstOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        /// <summary>
        /// Insertamos un nuevo BillingType en la base de datos
        /// </summary>
        /// <param name="obj">Elemento del tipo BillingType a insertar</param>
        public void Insert(BillingTypes obj)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                context.BillingTypes.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        public void Insert(BillingTypes2 obj)
        {
            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                context.BillingTypes2.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        /// <summary>
        /// Actualizamos los datos de un BillingType existente
        /// </summary>
        /// <param name="obj">Elemento del tipo BillingType con los datos a actualizar</param>
        public void Update(BillingTypes obj)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.BillingTypes
                               where h.IdBillingType.Equals(obj.IdBillingType)
                               select h).SingleOrDefault();

                if (element != null)
                {
                    element.IdHotel = obj.IdHotel;
                    element.Description = obj.Description;
                    element.Price = obj.Price;
                    element.TimeCredit = obj.TimeCredit;
                    element.ValidTill = obj.ValidTill;
                    element.MaxDevices = obj.MaxDevices;
                    element.BWDown = obj.BWDown;
                    element.BWUp = obj.BWUp;
                    element.IdPriority = obj.IdPriority;
                    element.FreeAccess = obj.FreeAccess;
                    element.Visible = obj.Visible;
                    element.Order = obj.Order;
                    element.Enabled = obj.Enabled;
                    element.VolumeUp = obj.VolumeUp;
                    element.VolumeDown = obj.VolumeDown;
                    element.IdLocation = obj.IdLocation;
                    element.PayPalDescription = obj.PayPalDescription;
                    element.IdBillingModule = obj.IdBillingModule;
                    element.DefaultFastTicket = obj.DefaultFastTicket;
                    element.UrlLanding = obj.UrlLanding;
                    element.Filter_Id = obj.Filter_Id;
                    element.IoT = obj.IoT;
                    element.NextBillingType = obj.NextBillingType;

                }

                context.SubmitChanges();
            }
        }

        public void Update(BillingTypes2 obj)
        {
            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var element = (from h in context.BillingTypes2
                               where h.IdBillingType.Equals(obj.IdBillingType)
                               select h).SingleOrDefault();

                Util.Map(obj, element);

                context.SubmitChanges();
            }
        }

        /// <summary>
        /// Seleccionar el listado de todos los BillingType
        /// </summary>
        /// <returns></returns>
        public List<BillingTypes> SelectBillingType()
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.BillingTypes
                                select h);

                List<BillingTypes> listado = new List<BillingTypes>();

                foreach (var element in elements)
                {
                    BillingTypes obj = new BillingTypes();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public BillingTypes SelectBillingTypeFree(int idHotel)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.BillingTypes
                               where (h.IdHotel.Equals(idHotel) && (h.FreeAccess) && (h.Enabled))
                                select h).FirstOrDefault();

                
                BillingTypes obj = new BillingTypes();
                if (element != null)    
                    Util.Map(element, obj);

                return obj;
            }
        }

        public BillingTypes SelectBillingTypeFreeLocation(int idLocation)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.BillingTypes
                               where (h.IdLocation.Equals(idLocation) && (h.FreeAccess) && (h.Enabled))
                                select h).FirstOrDefault();

                
                BillingTypes obj = new BillingTypes();
                if (element != null)    
                    Util.Map(element, obj);

                return obj;
            }
        }

        public List<BillingTypes> SelectBillingTypesOnlyAllZones(int idAllzones)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.BillingTypes
                                where (h.IdLocation.Equals(idAllzones) && (!h.FreeAccess) && (h.Enabled))
                                select h);

                List<BillingTypes> list = new List<BillingTypes>();

                foreach (var element in elements)
                {
                    BillingTypes obj = new BillingTypes();
                    if (element != null)
                    {
                        Util.Map(element, obj);
                        list.Add(obj);
                    }


                }

                return list;
            }
        }
        
        public List<BillingTypes> SelectBillingTypesAndAllZones(int idLocation, int idAllzones)
        {
            
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.BillingTypes
                               where ((h.IdLocation.Equals(idLocation) || (h.IdLocation.Equals(idAllzones))) && (!h.FreeAccess) && (h.Enabled))
                               select h);

                List<BillingTypes> list = new List<BillingTypes>();

                foreach (var element in elements)
                {
                    BillingTypes obj = new BillingTypes();
                    if (element != null)
                    {
                        Util.Map(element, obj);
                        list.Add(obj);
                    }

                    
                }

                return list;
            }
        }

        public List<BillingTypes> SelectBillingTypesAllAndAllZones(int idLocation, int idAllzones)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.BillingTypes
                                where ((h.IdLocation.Equals(idLocation) || (h.IdLocation.Equals(idAllzones))) && (h.Enabled))
                                select h);

                List<BillingTypes> list = new List<BillingTypes>();

                foreach (var element in elements)
                {
                    BillingTypes obj = new BillingTypes();
                    if (element != null)
                    {
                        Util.Map(element, obj);
                        list.Add(obj);
                    }


                }

                return list;
            }
        }

        /// <summary>
        /// Seleccionar un listado de todos lo BillingType de un hotel
        /// </summary>
        /// <param name="idHotel">Identificador del hotel</param>
        /// <returns></returns>
        public List<BillingTypes> SelectBillingType(int idHotel)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                List<BillingTypes> elements = (from h in context.BillingTypes
                                where h.IdHotel.Equals(idHotel) && h.Enabled
                                orderby h.Order ascending
                                select h).ToList();

                return elements;
            }
        }

        public List<BillingTypes2> SelectBillingType2All(int idHotel)
        {

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                List<BillingTypes2> elements = (from h in context.BillingTypes2
                                               where h.IdHotel.Equals(idHotel)
                                               orderby h.Order ascending
                                               select h).ToList();

                return elements;
            }
        }

        public List<BillingTypes> SelectBillingTypeAll(int idHotel)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                List<BillingTypes> elements = (from h in context.BillingTypes
                                               where h.IdHotel.Equals(idHotel)
                                               orderby h.Order ascending
                                               select h).ToList();

                return elements;
            }
        }

        public List<BillingTypes> SelectBillingType(int idHotel, int pageIndex, int pageSize, ref int pageNumber)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.BillingTypes
                                where h.IdHotel.Equals(idHotel) && h.Enabled.Equals(true)
                                select h);

                int count = elements.Count();
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                elements = elements.OrderBy(a => a.Order).Skip((pageIndex) * pageSize).Take(pageSize);

                List<BillingTypes> listado = new List<BillingTypes>();

                foreach (var element in elements)
                {
                    BillingTypes obj = new BillingTypes();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<BillingTypes> SelectBillingType(int idHotel, string filter, int idbillingmodule, bool status, int pageIndex, int pageSize, ref int pageNumber)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.BillingTypes
                                where h.IdHotel.Equals(idHotel) && h.Enabled.Equals(status) && h.Description.Contains(filter)
                                && (idbillingmodule.Equals(0) || h.IdBillingModule.Equals(idbillingmodule))
                                select h);

                int count = elements.Count();
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                elements = elements.OrderBy(a => a.Order).Skip((pageIndex) * pageSize).Take(pageSize);

                List<BillingTypes> listado = new List<BillingTypes>();

                foreach (var element in elements)
                {
                    BillingTypes obj = new BillingTypes();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }
        
        public List<BillingTypes> SelectBillingType(int idHotel, string filter, int idbillingmodule, bool status, bool iot, int pageIndex, int pageSize, ref int pageNumber)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.BillingTypes
                                where h.IdHotel.Equals(idHotel) && h.Enabled.Equals(status) && h.IoT.Equals(iot) && h.Description.Contains(filter)
                                && (idbillingmodule.Equals(0) || h.IdBillingModule.Equals(idbillingmodule))
                                select h);

                int count = elements.Count();
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                elements = elements.OrderBy(a => a.Order).Skip((pageIndex) * pageSize).Take(pageSize);

                List<BillingTypes> listado = new List<BillingTypes>();

                foreach (var element in elements)
                {
                    BillingTypes obj = new BillingTypes();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }


        public List<BillingTypes> SelectBillingTypeLocation(int idLocation)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.BillingTypes
                                where h.IdLocation.Equals(idLocation) && h.Enabled.Equals(true)
                                select h);

                List<BillingTypes> listado = new List<BillingTypes>();

                foreach (var element in elements)
                {
                    BillingTypes obj = new BillingTypes();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<BillingTypes2> SelectAllBillingTypeLocation(int idLocation)
        {

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var elements = (from h in context.BillingTypes2
                                where h.IdLocation.Equals(idLocation)
                                select h);

                List<BillingTypes2> listado = new List<BillingTypes2>();

                foreach (var element in elements)
                {
                    BillingTypes2 obj = new BillingTypes2();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }



        public List<BillingTypes> SelectBillingTypeLocation(int idLocation, int pageIndex, int pageSize, ref int pageNumber)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.BillingTypes
                                where h.IdLocation.Equals(idLocation) && h.Enabled.Equals(true)
                                select h);

                int count = elements.Count();
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                elements = elements.OrderBy(a => a.Order).Skip((pageIndex) * pageSize).Take(pageSize);

                List<BillingTypes> listado = new List<BillingTypes>();

                foreach (var element in elements)
                {
                    BillingTypes obj = new BillingTypes();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<BillingTypes> SelectBillingTypeLocation(int idLocation, string filter, int idbillingmodule, bool status, int pageIndex, int pageSize, ref int pageNumber)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.BillingTypes
                                where h.IdLocation.Equals(idLocation) && h.Enabled.Equals(status) && h.Description.Contains(filter)
                                && (idbillingmodule.Equals(0) || h.IdBillingModule.Equals(idbillingmodule))
                                select h);

                int count = elements.Count();
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                elements = elements.OrderBy(a => a.Order).Skip((pageIndex) * pageSize).Take(pageSize);

                List<BillingTypes> listado = new List<BillingTypes>();

                foreach (var element in elements)
                {
                    BillingTypes obj = new BillingTypes();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }
        
        public List<BillingTypes> SelectBillingTypeLocation(int idLocation, string filter, int idbillingmodule, bool status, bool iot, int pageIndex, int pageSize, ref int pageNumber)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.BillingTypes
                                where h.IdLocation.Equals(idLocation) && h.Enabled.Equals(status) && h.IoT.Equals(iot) && h.Description.Contains(filter)
                                && (idbillingmodule.Equals(0) || h.IdBillingModule.Equals(idbillingmodule))
                                select h);

                int count = elements.Count();
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                elements = elements.OrderBy(a => a.Order).Skip((pageIndex) * pageSize).Take(pageSize);

                List<BillingTypes> listado = new List<BillingTypes>();

                foreach (var element in elements)
                {
                    BillingTypes obj = new BillingTypes();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<BillingTypes> SelectBillingType(int idLocation, int idBillingModule)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.BillingTypes
                                where (h.IdLocation.Equals(idLocation) && h.IdBillingModule.Equals(idBillingModule) && (h.Enabled))
                                select h);

                List<BillingTypes> list = (from h in context.BillingTypes
                                           where (h.IdLocation.Equals(idLocation) && h.IdBillingModule.Equals(idBillingModule) && (h.Enabled))
                                           select h).ToList();

                return list;
            }
        }

        public List<BillingTypes> SelectBillingTypeModule(int idHotel, int idBillingModule)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.BillingTypes
                                where h.IdHotel.Equals(idHotel) && h.Enabled && h.IdBillingModule.Equals(idBillingModule)
                                orderby h.Order ascending
                                select h);

                List<BillingTypes> listado = new List<BillingTypes>();

                foreach (var element in elements)
                {
                    BillingTypes obj = new BillingTypes();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        #endregion

        #region Billing

        /// <summary>
        /// Seleccionar un Billing
        /// </summary>
        /// <param name="obj">Objeto del tipo Billing donde se devolveran los datos del Billing</param>
        /// <param name="idHotel">Identificador del Billing a seleccionar</param>
        public void Select(Billings obj, int idBilling)
        {
            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var element = (from h in context.Billings
                               where h.IdBilling.Equals(idBilling)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        /// <summary>
        /// Insertamos un nuevo Billing en la base de datos
        /// </summary>
        /// <param name="obj">Elemento del tipo Billing a insertar</param>
        public void Insert(Billings obj)
        {

            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                context.Billings.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        /// <summary>
        /// Actualizamos los datos de un Billing existente
        /// </summary>
        /// <param name="obj">Elemento del tipo Billing con los datos a actualizar</param>
        public void Update(Billings obj)
        {
            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var element = (from h in context.Billings
                               where h.IdBilling.Equals(obj.IdBilling)
                               select h).SingleOrDefault();

                if (element != null)
                {
                    element.IdBillingType = obj.IdBillingType;
                    element.IdHotel = obj.IdHotel;
                    element.IdRoom = obj.IdRoom;
                    element.Amount = obj.Amount;
                    element.BillingCharge = obj.BillingCharge;
                    element.BillingDate = obj.BillingDate;
                    element.Flag = obj.Flag;
                    element.IdUser = obj.IdUser;
                    element.Billable = obj.Billable;
                }

                Util.Map(obj, element);

                context.SubmitChanges();
            }
        }

        public void DeleteBilling(int id)
        {
            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var elements = (from h in context.Billings
                                where h.IdBilling.Equals(id)
                                select h);

                if (elements != null)
                    foreach (var element in elements)
                    {
                        context.Billings.DeleteOnSubmit(element);
                    }
                context.SubmitChanges();
            }
        }

        /// <summary>
        /// Seleccionar el listado de todos los Billings
        /// </summary>
        /// <returns></returns>
        public List<Billings> SelectBillings()
        {

            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var elements = (from h in context.Billings
                                select h);

                List<Billings> listado = new List<Billings>();

                foreach (var element in elements)
                {
                    Billings obj = new Billings();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        /// <summary>
        /// Seleccionar un listado de todos lo Billings de un hotel
        /// </summary>
        /// <param name="idHotel">Identificador del hotel</param>
        /// <returns></returns>
        public List<Billings> SelectBillingsSiteLocation(int idHotel, int idLocation)
        {

            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var elements = (from h in context.Billings
                                where h.IdHotel.Equals(idHotel)
                                where h.IdLocation.Equals(idLocation)
                                select h);

                List<Billings> listado = new List<Billings>();

                foreach (var element in elements)
                {
                    Billings obj = new Billings();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        /// <summary>
        /// Seleccionar un listado de todos lo Billings de un hotel
        /// </summary>
        /// <param name="idHotel">Identificador del hotel</param>
        /// <returns></returns>
        public List<Billings> SelectBillings(int idHotel)
        {

            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var elements = (from h in context.Billings
                                where h.IdHotel.Equals(idHotel)
                                select h);

                List<Billings> listado = new List<Billings>();

                foreach (var element in elements)
                {
                    Billings obj = new Billings();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        /// <summary>
        /// Recupera todos los cargos de un hotel en un periodo de tiempo
        /// </summary>
        /// <param name="idHotel">Identificador del hotel</param>
        /// <param name="start">Fecha de inicio, incluida en la busqueda</param>
        /// <param name="finish">Fecha de finalizacion, no incluida en la busqueda</param>
        /// <returns></returns>
        public List<BillingInfo> SelectBillings(int idHotel, DateTime start, DateTime finish)
        {

            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var elements = context.SP_FDS_OBTAINBILLINGBETWEENDATE(idHotel, start, finish);

                List<BillingInfo> listado = new List<BillingInfo>();

                foreach (var element in elements)
                {
                    BillingInfo obj = new BillingInfo();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<BillingInfo> SelectBillings(string listadohoteles, DateTime start, DateTime finish)
        {

            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                List<BillingInfo> listado = new List<BillingInfo>();
                string[] ids = listadohoteles.Split(',');
                foreach (string id in ids)
                {
                    int idHotel = Int32.Parse(id);
                    var elements = context.SP_FDS_OBTAINBILLINGBETWEENDATE(idHotel, start, finish);

                    foreach (var element in elements)
                    {
                        BillingInfo obj = new BillingInfo();
                        Util.Map(element, obj);
                        listado.Add(obj);
                    }
                }

                return listado;
            }
        }

        public List<BillingInfo> SelectBillings(string listadohoteles, DateTime start, DateTime finish, int idroom, int idfdsuser)
        {

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                List<BillingInfo> listado = new List<BillingInfo>();
                string[] ids = listadohoteles.Split(',');
                foreach (string id in ids)
                {
                    int idHotel = Int32.Parse(id);
                    var count = context.SP_FDS_OBTAINBILLINGBETWEENDATEALLFILTERS(idHotel, start, finish, idroom, idfdsuser).Count();
                    var elements = context.SP_FDS_OBTAINBILLINGBETWEENDATEALLFILTERS(idHotel, start, finish, idroom, idfdsuser);


                    foreach (var element in elements)
                    {
                        BillingInfo obj = new BillingInfo();
                        Util.Map(element, obj);
                        listado.Add(obj);
                    }
                }

                return listado;
            }
        }

        public List<BillingInfo> SelectBillings(int idHotel, DateTime start, DateTime finish, int pageIndex, int pageSize, ref int pageNumber, ref double suma)
        {

            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var count = context.SP_FDS_OBTAINBILLINGBETWEENDATE(idHotel, start, finish).Count();
                var total = context.SP_FDS_OBTAINBILLINGBETWEENDATE(idHotel, start, finish).ToList();
                suma = (from t in total select t.Amount).Sum();
                var elements = context.SP_FDS_OBTAINBILLINGBETWEENDATE(idHotel, start, finish).Skip((pageIndex) * pageSize).Take(pageSize);

                //int count = elements.Count();
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                //elements = elements.Skip((pageIndex) * pageSize).Take(pageSize);

                List<BillingInfo> listado = new List<BillingInfo>();

                foreach (var element in elements)
                {
                    BillingInfo obj = new BillingInfo();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<BillingInfo> SelectBillings(int idHotel, DateTime start, DateTime finish, int idroom, int idfdsuser, int pageIndex, int pageSize, ref int pageNumber, ref double suma)
        {

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var count = context.SP_FDS_OBTAINBILLINGBETWEENDATEALLFILTERS(idHotel, start, finish, idroom, idfdsuser).Count();
                var total = context.SP_FDS_OBTAINBILLINGBETWEENDATEALLFILTERS(idHotel, start, finish, idroom, idfdsuser).ToList();
                suma = (from t in total select t.Amount).Sum();
                var elements = context.SP_FDS_OBTAINBILLINGBETWEENDATEALLFILTERS(idHotel, start, finish, idroom, idfdsuser).Skip((pageIndex) * pageSize).Take(pageSize);

                //int count = elements.Count();
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                //elements = elements.Skip((pageIndex) * pageSize).Take(pageSize);

                List<BillingInfo> listado = new List<BillingInfo>();

                foreach (var element in elements)
                {
                    BillingInfo obj = new BillingInfo();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<BillingInfo> SelectBillings(int idHotel, List<FDSUsers> users, DateTime start, DateTime finish, int idroom, int idfdsuser, int pageIndex, int pageSize, ref int pageNumber, ref double suma)
        {

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var elements = context.SP_FDS_OBTAINBILLINGBETWEENDATEALLFILTERS(idHotel, start, finish, idroom, idfdsuser);

                List<BillingInfo> listado = new List<BillingInfo>();

                foreach (var element in elements)
                {
                    BillingInfo obj = new BillingInfo();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                int count = listado.Count();
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                return listado.Skip((pageIndex) * pageSize).Take(pageSize).ToList();
            }
        }

        public List<BillingInfo> SelectBillings(string listHoteles, List<FDSUsers> users, DateTime start, DateTime finish, int idroom, int idfdsuser, int pageIndex, int pageSize, ref int pageNumber, ref double suma)
        {

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                List<BillingInfo> listado = new List<BillingInfo>();
                string[] hoteles = listHoteles.Split(',');
                foreach (string idhotel in hoteles)
                {
                    var elements = context.SP_FDS_OBTAINBILLINGBETWEENDATEALLFILTERS(int.Parse(idhotel), start, finish, idroom, idfdsuser);

                    foreach (var element in elements)
                    {
                        BillingInfo obj = new BillingInfo();
                        Util.Map(element, obj);
                        listado.Add(obj);
                    }
                }

                int count = listado.Count();
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                return listado.Skip((pageIndex) * pageSize).Take(pageSize).ToList();
            }
        }

        public List<BillingInfo> SelectBillings(int idHotel, DateTime start, DateTime finish, int idroom, int idfdsuser)
        {

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var count = context.SP_FDS_OBTAINBILLINGBETWEENDATEALLFILTERS(idHotel, start, finish, idroom, idfdsuser).Count();
                var elements = context.SP_FDS_OBTAINBILLINGBETWEENDATEALLFILTERS(idHotel, start, finish, idroom, idfdsuser);

                List<BillingInfo> listado = new List<BillingInfo>();

                foreach (var element in elements)
                {
                    BillingInfo obj = new BillingInfo();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<BillingInfo> SelectBillings(int idHotel, DateTime start, DateTime finish, int idfdsuser, int pageIndex, int pageSize, ref int pageNumber, ref double suma)
        {

            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var count = context.SP_FDS_OBTAINBILLINGBETWEENDATE(idHotel, start, finish).Count();
                var total = context.SP_FDS_OBTAINBILLINGBETWEENDATE(idHotel, start, finish).ToList();
                suma = (from t in total select t.Amount).Sum();
                var elements = context.SP_FDS_OBTAINBILLINGBETWEENDATE(idHotel, start, finish).Skip((pageIndex) * pageSize).Take(pageSize);

                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                List<BillingInfo> listado = new List<BillingInfo>();

                foreach (var element in elements)
                {
                    BillingInfo obj = new BillingInfo();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<Exchanges> SelectTiposDeMoneda(string sites)
        {
            List<Exchanges> result = new List<Exchanges>();
            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {               
                string[] ids = sites.Split(',');
                int count = 0;
                if (ids[0] != null)
                {
                    string consulta = "select distinct Currency,IdLocation from Locations where IdHotel =" + ids[0];                   
                    foreach (string id in ids)
                    {
                        consulta += " or IdHotel = "+ id;                                                
                    }
                    IEnumerable<Exchanges> result_query = context.ExecuteQuery<Exchanges>(consulta);
                    count++;

                    result = result_query.ToList();
                }                   
            }
            return result;
        }


        public List<BillingInfo> SelectBillings(string hotels, DateTime start, DateTime finish, int pageIndex, int pageSize, ref int pageNumber, ref double suma)
        {
            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                List<BillingInfo> listado = new List<BillingInfo>();
                string[] ids = hotels.Split(',');
                int count = 0;
                foreach (string id in ids)
                {
                    int idHotel = Int32.Parse(id);
                    count += context.SP_FDS_OBTAINBILLINGBETWEENDATE(idHotel, start, finish).Count();
                    var total = context.SP_FDS_OBTAINBILLINGBETWEENDATE(idHotel, start, finish).ToList();
                    suma += (from t in total select t.Amount).Sum();
                    var elements = context.SP_FDS_OBTAINBILLINGBETWEENDATE(idHotel, start, finish);

                    foreach (var element in elements)
                    {
                        BillingInfo obj = new BillingInfo();
                        Util.Map(element, obj);
                        listado.Add(obj);
                    }
                }

                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                return listado.Skip((pageIndex) * pageSize).Take(pageSize).ToList();
            }
        }

        public List<BillingInfo> SelectBillings(string hotels, DateTime start, DateTime finish, int idfdsuserfilter, int pageIndex, int pageSize, ref int pageNumber, ref double suma)
        {
            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                List<BillingInfo> listado = new List<BillingInfo>();
                string[] ids = hotels.Split(',');
                int count = 0;
                foreach (string id in ids)
                {
                    int idHotel = Int32.Parse(id);
                    count += context.SP_FDS_OBTAINBILLINGBETWEENDATE(idHotel, start, finish).Count();
                    var total = context.SP_FDS_OBTAINBILLINGBETWEENDATE(idHotel, start, finish).ToList();
                    suma += (from t in total select t.Amount).Sum();
                    var elements = context.SP_FDS_OBTAINBILLINGBETWEENDATE(idHotel, start, finish);

                    foreach (var element in elements)
                    {
                        BillingInfo obj = new BillingInfo();
                        Util.Map(element, obj);
                        listado.Add(obj);
                    }
                }

                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                return listado.Skip((pageIndex) * pageSize).Take(pageSize).ToList();
            }
        }

        public List<BillingInfo> SelectBillingsLocation(int idlocation, DateTime start, DateTime finish, int pageIndex, int pageSize, ref int pageNumber, ref double suma)
        {
            
            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var count = context.SP_FDS_OBTAINBILLINGBETWEENDATELOCATION(idlocation, start, finish).Count();
                var total = context.SP_FDS_OBTAINBILLINGBETWEENDATELOCATION(idlocation, start, finish).ToList();
                suma = (from t in total select t.Amount).Sum();
                var elements = context.SP_FDS_OBTAINBILLINGBETWEENDATELOCATION(idlocation, start, finish).Skip((pageIndex) * pageSize).Take(pageSize);

                //int count = elements.Count();
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                //elements = elements.Skip((pageIndex) * pageSize).Take(pageSize);

                List<BillingInfo> listado = new List<BillingInfo>();

                foreach (var element in elements)
                {
                    BillingInfo obj = new BillingInfo();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<BillingInfo> SelectBillingsLocation(int idlocation, DateTime start, DateTime finish)
        {
            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var elements = context.SP_FDS_OBTAINBILLINGBETWEENDATELOCATION(idlocation, start, finish);
                //elements = elements.Skip((pageIndex) * pageSize).Take(pageSize);

                List<BillingInfo> listado = new List<BillingInfo>();

                foreach (var element in elements)
                {
                    BillingInfo obj = new BillingInfo();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<BillingInfo> SelectBillingsLocation(int idlocation, List<FDSUsers> users, DateTime start, DateTime finish)
        {
            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var elements = context.SP_FDS_OBTAINBILLINGBETWEENDATELOCATION(idlocation, start, finish);
                //elements = elements.Skip((pageIndex) * pageSize).Take(pageSize);

                List<BillingInfo> listado = new List<BillingInfo>();

                foreach (var element in elements)
                {
                    FDSUsers u = (from t in users where t.IdUser.Equals(element.IdFDSUser) select t).FirstOrDefault();
                    if (u != null)
                    {
                        BillingInfo obj = new BillingInfo();
                        Util.Map(element, obj);
                        listado.Add(obj);
                    }
                }

                return listado;
            }
        }

        public List<BillingInfo> SelectBillingsLocation(int idlocation, DateTime start, DateTime finish, int idfdsuser, int pageIndex, int pageSize, ref int pageNumber, ref double suma)
        {

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var count = context.SP_FDS_OBTAINBILLINGBETWEENDATELOCATIONIDUSER(idlocation, start, finish, idfdsuser, 0).Count();
                var total = context.SP_FDS_OBTAINBILLINGBETWEENDATELOCATIONIDUSER(idlocation, start, finish, idfdsuser, 0).ToList();
                suma = (from t in total select t.Amount).Sum();
                var elements = context.SP_FDS_OBTAINBILLINGBETWEENDATELOCATIONIDUSER(idlocation, start, finish, idfdsuser, 0).Skip((pageIndex) * pageSize).Take(pageSize);

                //int count = elements.Count();
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                //elements = elements.Skip((pageIndex) * pageSize).Take(pageSize);

                List<BillingInfo> listado = new List<BillingInfo>();

                foreach (var element in elements)
                {
                    BillingInfo obj = new BillingInfo();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<BillingInfo> SelectBillingsLocation(int idlocation, List<FDSUsers> users, DateTime start, DateTime finish, int idfdsuser, int pageIndex, int pageSize, ref int pageNumber, ref double suma)
        {

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                //var count = context.SP_FDS_OBTAINBILLINGBETWEENDATELOCATIONIDUSER(idlocation, start, finish, idfdsuser, 0).Count();
                //var total = context.SP_FDS_OBTAINBILLINGBETWEENDATELOCATIONIDUSER(idlocation, start, finish, idfdsuser, 0).ToList();
            //    suma = (from t in total select t.Amount).Sum();
//                var elements = context.SP_FDS_OBTAINBILLINGBETWEENDATELOCATIONIDUSER(idlocation, start, finish, idfdsuser, 0).Skip((pageIndex) * pageSize).Take(pageSize);

                var elements = context.SP_FDS_OBTAINBILLINGBETWEENDATELOCATIONIDUSER(idlocation, start, finish, idfdsuser, 0);

                List<BillingInfo> listado = new List<BillingInfo>();

                foreach (var element in elements)
                {
                    FDSUsers u = (from t in users where t.IdUser.Equals(element.IdFDSUser) select t).FirstOrDefault();
                    if (u != null)
                    {
                        BillingInfo obj = new BillingInfo();
                        Util.Map(element, obj);
                        listado.Add(obj);
                        suma += obj.Amount;
                    }
                }
                int count = listado.Count();
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                return  listado.Skip((pageIndex) * pageSize).Take(pageSize).ToList();
            }
        }

        public List<BillingInfo> SelectBillingsLocation(int idlocation, DateTime start, DateTime finish, int idfdsuser)
        {
            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var elements = context.SP_FDS_OBTAINBILLINGBETWEENDATELOCATIONIDUSER(idlocation, start, finish, idfdsuser, 0);
                //elements = elements.Skip((pageIndex) * pageSize).Take(pageSize);

                List<BillingInfo> listado = new List<BillingInfo>();

                foreach (var element in elements)
                {
                    BillingInfo obj = new BillingInfo();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<BillingInfo> SelectBillingsLocation(int idlocation, DateTime start, DateTime finish, int idfdsuser, int idroom)
        {
            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var elements = context.SP_FDS_OBTAINBILLINGBETWEENDATELOCATIONIDUSER(idlocation, start, finish, idfdsuser, idroom);
                //elements = elements.Skip((pageIndex) * pageSize).Take(pageSize);

                List<BillingInfo> listado = new List<BillingInfo>();

                foreach (var element in elements)
                {
                    BillingInfo obj = new BillingInfo();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        /// <summary>
        /// Resultado de la produccion de una habitación
        /// </summary>
        /// <param name="idHotel"></param>
        /// <param name="idRoom"></param>
        /// <returns></returns>
        public List<Billings> SelectBillings(int idHotel, int idRoom)
        {

            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var elements = (from h in context.Billings
                                where h.IdHotel.Equals(idHotel) && h.IdRoom.Equals(idRoom)
                                select h);

                List<Billings> listado = new List<Billings>();

                foreach (var element in elements)
                {
                    Billings obj = new Billings();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        //public List<Billings> SelectBillings(int idHotel, int idRoom, int pageIndex, int pageSize, ref int pageNumber)
        //{

        //    using (DataModel context = new DataModel(Util.ConnectionString()))
        //    {
        //        var elements = (from h in context.Billings
        //                        where h.IdHotel.Equals(idHotel) && h.IdRoom.Equals(idRoom) && (h.Flag.Equals(3) || h.Flag.Equals(4) || h.Flag.Equals(5) || h.Flag.Equals(6))
        //                        select h);

        //        int count = elements.Count();
        //        pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
        //        elements = elements.Skip((pageIndex) * pageSize).Take(pageSize);

        //        List<Billings> listado = new List<Billings>();

        //        foreach (var element in elements)
        //        {
        //            Billings obj = new Billings();
        //            Util.Map(element, obj);
        //            listado.Add(obj);
        //        }

        //        return listado;
        //    }
        //}

        public List<BillingInfo> SelectBillings(int idHotel, int idRoom, int pageIndex, int pageSize, ref int pageNumber, ref double suma)
        {

            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                int count = context.SP_FDS_OBTAINBILLINGROOM(idHotel, idRoom).Count();
                var total = context.SP_FDS_OBTAINBILLINGROOM(idHotel, idRoom).ToList();
                suma = (from t in total select t.Amount).Sum();
                
                var elements = context.SP_FDS_OBTAINBILLINGROOM(idHotel, idRoom).Skip((pageIndex) * pageSize).Take(pageSize);

                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                List<BillingInfo> listado = new List<BillingInfo>();

                foreach (var element in elements)
                {
                    BillingInfo obj = new BillingInfo();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<BillingInfo> SelectBillings(string listadohoteles, int idRoom, int pageIndex, int pageSize, ref int pageNumber, ref double suma)
        {
            string[] ids = listadohoteles.Split(',');
            int count = 0;
            List<BillingInfo> listado = new List<BillingInfo>();
            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                foreach (string id in ids)
                {
                    int idHotel = int.Parse(id);
                    count += context.SP_FDS_OBTAINBILLINGROOM(idHotel, idRoom).Count();
                    var total = context.SP_FDS_OBTAINBILLINGROOM(idHotel, idRoom).ToList();
                    suma += (from t in total select t.Amount).Sum();

                    var elements = context.SP_FDS_OBTAINBILLINGROOM(idHotel, idRoom).Skip((pageIndex) * pageSize).Take(pageSize);

                    foreach (var element in elements)
                    {
                        BillingInfo obj = new BillingInfo();
                        Util.Map(element, obj);
                        listado.Add(obj);
                    }
                }
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                return listado;
            }
        }

        public List<BillingInfo> SelectBillingsLocation(int idLocation, int idRoom, int pageIndex, int pageSize, ref int pageNumber, ref double suma)
        {

            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                int count = context.SP_FDS_OBTAINBILLINGROOMLOCATION(idLocation, idRoom).Count();
                var total = context.SP_FDS_OBTAINBILLINGROOMLOCATION(idLocation, idRoom).ToList();
                suma = (from t in total select t.Amount).Sum();

                var elements = context.SP_FDS_OBTAINBILLINGROOMLOCATION(idLocation, idRoom).Skip((pageIndex) * pageSize).Take(pageSize);

                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                List<BillingInfo> listado = new List<BillingInfo>();

                foreach (var element in elements)
                {
                    BillingInfo obj = new BillingInfo();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<BillingInfo> SelectBillingsLocation(int idLocation, List<FDSUsers> users, int idRoom, int pageIndex, int pageSize, ref int pageNumber, ref double suma)
        {

            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var elements = context.SP_FDS_OBTAINBILLINGROOMLOCATION(idLocation, idRoom);

                List<BillingInfo> listado = new List<BillingInfo>();

                foreach (var element in elements)
                {
                    FDSUsers u = (from t in users where t.IdUser.Equals(element.IdFDSUser) select t).FirstOrDefault();
                    if (u != null)
                    {
                        BillingInfo obj = new BillingInfo();
                        Util.Map(element, obj);
                        listado.Add(obj);
                    }
                }

                int count = listado.Count();
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                return listado.Skip((pageIndex) * pageSize).Take(pageSize).ToList();
            }
        }

        public List<BillingInfo> SelectBillingsLocation(int idLocation, int idRoom)
        {

            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var elements = context.SP_FDS_OBTAINBILLINGROOMLOCATION(idLocation, idRoom).ToList();

                List<BillingInfo> listado = new List<BillingInfo>();

                foreach (var element in elements)
                {
                    BillingInfo obj = new BillingInfo();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<Billings> SelectBillings(int idHotel, int idRoom, DateTime start, DateTime finish)
        {

            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var elements = (from h in context.Billings
                                where h.IdHotel.Equals(idHotel) && h.IdRoom.Equals(idRoom)
                                && h.BillingDate >= start && h.BillingDate < finish
                                select h);

                List<Billings> listado = new List<Billings>();

                foreach (var element in elements)
                {
                    Billings obj = new Billings();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<Billings> SelectBillingsLocation(int idHotel, int idRoom, DateTime start, DateTime finish)
        {

            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var elements = (from h in context.Billings
                                where h.IdLocation.Equals(idHotel) && h.IdRoom.Equals(idRoom)
                                && h.BillingDate >= start && h.BillingDate < finish
                                select h);

                List<Billings> listado = new List<Billings>();

                foreach (var element in elements)
                {
                    Billings obj = new Billings();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<BillingInfo> SelectBillings(int idHotel, int idRoom, DateTime start, DateTime finish, int pageIndex, int pageSize, ref int pageNumber, ref double suma)
        {
            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                int count = context.SP_FDS_OBTAINBILLINGBETWEENDATEANDROOM(idHotel, idRoom, start, finish).Count();
                var total = context.SP_FDS_OBTAINBILLINGBETWEENDATEANDROOM(idHotel, idRoom, start, finish).ToList();
                suma = (from t in total select t.Amount).Sum();
                var elements = context.SP_FDS_OBTAINBILLINGBETWEENDATEANDROOM(idHotel, idRoom, start, finish).Skip((pageIndex) * pageSize).Take(pageSize);

                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                List<BillingInfo> listado = new List<BillingInfo>();

                foreach (var element in elements)
                {
                    BillingInfo obj = new BillingInfo();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<BillingInfo> SelectBillings(string listadohoteles, int idRoom, DateTime start, DateTime finish, int pageIndex, int pageSize, ref int pageNumber, ref double suma)
        {
            string[] ids = listadohoteles.Split(',');
            int count = 0;
            List<BillingInfo> listado = new List<BillingInfo>();
            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                foreach (string id in ids)
                {
                    int idHotel = int.Parse(id);
                    count += context.SP_FDS_OBTAINBILLINGBETWEENDATEANDROOM(idHotel, idRoom, start, finish).Count();
                    var total = context.SP_FDS_OBTAINBILLINGBETWEENDATEANDROOM(idHotel, idRoom, start, finish).ToList();
                    suma += (from t in total select t.Amount).Sum();
                    var elements = context.SP_FDS_OBTAINBILLINGBETWEENDATEANDROOM(idHotel, idRoom, start, finish).Skip((pageIndex) * pageSize).Take(pageSize);

                    foreach (var element in elements)
                    {
                        BillingInfo obj = new BillingInfo();
                        Util.Map(element, obj);
                        listado.Add(obj);
                    }
                }
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                return listado;
            }
        }

        public List<BillingInfo> SelectBillingsLocation(int idLocation, int idRoom, DateTime start, DateTime finish, int pageIndex, int pageSize, ref int pageNumber, ref double suma)
        {
            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                int count = context.SP_FDS_OBTAINBILLINGBETWEENDATEANDROOMLOCATION(idLocation, idRoom, start, finish).Count();
                var total = context.SP_FDS_OBTAINBILLINGBETWEENDATEANDROOMLOCATION(idLocation, idRoom, start, finish).ToList();
                suma = (from t in total select t.Amount).Sum();
                var elements = context.SP_FDS_OBTAINBILLINGBETWEENDATEANDROOMLOCATION(idLocation, idRoom, start, finish).Skip((pageIndex) * pageSize).Take(pageSize);

                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                List<BillingInfo> listado = new List<BillingInfo>();

                foreach (var element in elements)
                {
                    BillingInfo obj = new BillingInfo();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<BillingInfo> SelectBillingsLocation(int idLocation, int idRoom, DateTime start, DateTime finish, int idfdsuser, int pageIndex, int pageSize, ref int pageNumber, ref double suma)
        {
            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                int count = context.SP_FDS_OBTAINBILLINGBETWEENDATELOCATIONIDUSER(idLocation, start, finish, idfdsuser, idRoom).Count();
                var total = context.SP_FDS_OBTAINBILLINGBETWEENDATELOCATIONIDUSER(idLocation, start, finish, idfdsuser, idRoom).ToList();
                suma = (from t in total select t.Amount).Sum();
                var elements = context.SP_FDS_OBTAINBILLINGBETWEENDATELOCATIONIDUSER(idLocation, start, finish, idfdsuser, idRoom).Skip((pageIndex) * pageSize).Take(pageSize);

                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                List<BillingInfo> listado = new List<BillingInfo>();

                foreach (var element in elements)
                {
                    BillingInfo obj = new BillingInfo();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<BillingInfo> SelectBillingsLocation(int idLocation, List<FDSUsers> users, int idRoom, DateTime start, DateTime finish, int idfdsuser, int pageIndex, int pageSize, ref int pageNumber, ref double suma)
        {
            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var elements = context.SP_FDS_OBTAINBILLINGBETWEENDATELOCATIONIDUSER(idLocation, start, finish, idfdsuser, idRoom);

                List<BillingInfo> listado = new List<BillingInfo>();

                foreach (var element in elements)
                {
                    if (!idfdsuser.Equals(0))
                    {
                        BillingInfo obj = new BillingInfo();
                        Util.Map(element, obj);
                        listado.Add(obj);
                    }
                    else
                    {
                        FDSUsers u = (from t in users where t.IdUser.Equals(element.IdFDSUser) select t).FirstOrDefault();
                        if (u != null)
                        {
                            BillingInfo obj = new BillingInfo();
                            Util.Map(element, obj);
                            listado.Add(obj);
                        }
                    }
                }

                int count = listado.Count();
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                return listado.Skip((pageIndex) * pageSize).Take(pageSize).ToList();
            }
        }

        public List<BillingInfo> SelectBillingsInfoLocation(int idLocation, int idRoom, DateTime start, DateTime finish)
        {
            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var elements = context.SP_FDS_OBTAINBILLINGBETWEENDATEANDROOMLOCATION(idLocation, idRoom, start, finish);

                List<BillingInfo> listado = new List<BillingInfo>();

                foreach (var element in elements)
                {
                    BillingInfo obj = new BillingInfo();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<Billings> SelectNewBillings()
        {

            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var elements = (from h in context.Billings
                                where h.Flag.Equals(0) || h.Flag.Equals(1)
                                select h);

                List<Billings> listado = new List<Billings>();

                foreach (var element in elements)
                {
                    Billings obj = new Billings();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<Billings> SelectReadBillings()
        {

            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var elements = (from h in context.Billings
                                where h.Flag.Equals(2)
                                select h);

                List<Billings> listado = new List<Billings>();

                foreach (var element in elements)
                {
                    Billings obj = new Billings();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        
        #endregion

        #region RadiusUsers

        /// <summary>
        /// Seleccionar un usuario del user 
        /// </summary>
        /// <param name="obj">Objeto del tipo RadiusUser donde se devolveran los datos del usuario</param>
        /// <param name="idHotel">Identificador del hotel a seleccionar</param>
        public void Select(Users obj, int idUser)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.Users
                               where h.IdUser.Equals(idUser)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        static public void Select(Users obj, string username, string password)
        {
            try
            {
                using (DataModel context = new DataModel(Util.ConnectionString()))
                {
                    var element = (from h in context.Users
                                   where h.Name.ToUpper().Equals(username.ToUpper()) && h.Password.ToUpper().Equals(password.ToUpper())
                                   select h).SingleOrDefault();

                    if (element != null)
                        Util.Map(element, obj);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void Select(Users obj, string username)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.Users
                               where h.Name.ToUpper().Equals(username.ToUpper())
                               orderby h.IdUser descending
                               select h).FirstOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        public void Select(Users obj, string username, int idHotel)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.Users
                               where h.Enabled.Equals(true) && h.IdHotel.Equals(idHotel) && 
                               h.Name.ToUpper().Equals(username.ToUpper()) 
                               orderby h.IdUser descending
                               select h).FirstOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        public void SelectMac(Users obj, string mac)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.Users
                               where h.BuyingCallerID.ToUpper().Equals(mac.ToUpper())
                               orderby h.IdUser descending
                               select h).FirstOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        public void SelectMac(Users obj, string mac, int idhotel)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.Users
                               where h.IdHotel.Equals(idhotel) && h.BuyingCallerID.ToUpper().Equals(mac.ToUpper())
                               orderby h.IdUser descending
                               select h).FirstOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        public void SelectMac(Users obj, string mac, int idhotel, int idbillingMoodule)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements2 = (from h in context.BillingTypes
                                 where h.IdHotel.Equals(idhotel) && h.IdBillingModule.Equals(idbillingMoodule)
                                 select h);

                List<Users> listado = new List<Users>();

                foreach (var b in elements2)
                {
                    var element = (from h in context.Users
                                   where h.IdHotel.Equals(idhotel) && h.BuyingCallerID.ToUpper().Equals(mac.ToUpper()) && h.IdBillingType.Equals(b.IdBillingType)
                                   orderby h.IdUser descending
                                   select h).FirstOrDefault();

                    if (element != null)
                    {
                        Users u = new Users();
                        Util.Map(element, u);
                        listado.Add(u);
                    }
                }

                if (listado.Count > 0)
                {
                    foreach (Users i in listado.OrderByDescending(a => a.ValidSince))
                    {
                        Util.Map(i, obj);
                        break;
                    }
                }

            }
        }

        public void Select(UserAll obj, int idUser)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = context.SP_360_SELECTUSERALL(idUser).FirstOrDefault();

                if (element != null)
                {
                    obj.BillingType = element.BillingType;
                    obj.BuyingCallerID = element.BuyingCallerID;
                    obj.BuyingFrom = element.BuyingFrom;
                    obj.BWDown = element.BWDown;
                    obj.BWUp = element.BWUp;
                    obj.CHKO = element.CHKO;
                    obj.Enabled = element.Enabled;
                    obj.FilterId = element.Filter_Id;
                    obj.IdBillingType = element.IdBillingType;
                    obj.IdHotel = element.IdHotel;
                    obj.IdLocation = element.IdLocation;
                    obj.IdRoom = element.IdRoom;
                    obj.IdUser = element.IdUser;
                    obj.MaxDevices = element.MaxDevices;
                    obj.Name = element.Name;
                    obj.Password = element.Password;
                    obj.Priority = element.Priority;
                    obj.PriorityName = element.PriorityName;
                    obj.RoomName = element.RoomName;
                    obj.TimeCredit = element.TimeCredit;
                    obj.ValidSince = element.ValidSince;
                    obj.ValidTill = element.ValidTill;
                    obj.VolumeDown = element.VolumeDown;
                    obj.VolumeUp = element.VolumeUp;
                    obj.Comment = element.Comment;

                }
            }
        }

        /// <summary>
        /// Insertamos un nuevo usuario en el radius
        /// </summary>
        /// <param name="obj">Elemento del tipo RadiusUser a insertar</param>
        public void Insert(Users obj)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                context.Users.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        /// <summary>
        /// Actualizamos los datos de un hotel existente
        /// </summary>
        /// <param name="obj">Elemento del tipo Hotel con los datos a actualizar</param>
        public void Update(Users obj)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.Users
                               where h.IdUser.Equals(obj.IdUser)
                               select h).SingleOrDefault();

                if (element != null)
                {
                    Util.Map(obj, element);
                    element.BWDown = obj.BWDown;
                    element.BWUp = obj.BWUp;
                    element.CHKO = obj.CHKO;
                    element.Enabled = obj.Enabled;
                    element.IdBillingType = obj.IdBillingType;
                    element.IdHotel = obj.IdHotel;
                    element.MaxDevices = obj.MaxDevices;
                    element.Name = obj.Name;
                    element.Password = obj.Password;
                    element.Priority = obj.Priority;
                    element.TimeCredit = obj.TimeCredit;
                    element.ValidSince = obj.ValidSince;
                    element.ValidTill = obj.ValidTill;
                    element.VolumeDown = obj.VolumeDown;
                    element.VolumeUp = obj.VolumeUp;
                    element.Comment = obj.Comment;
                    element.Filter_Id = obj.Filter_Id;
                }

                context.SubmitChanges();
            }
        }

        /// <summary>
        /// Seleccionar el listado de todos los hotels
        /// </summary>
        /// <returns></returns>
        public List<Users> SelectRadiusUsers()
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.Users
                                select h);

                List<Users> listado = new List<Users>();

                foreach (var element in elements)
                {
                    Users obj = new Users();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        /// <summary>
        /// Seleccionar un listado de todos lo hoteles de una cadena
        /// </summary>
        /// <param name="idChain">Identificador de la cadena de hoteles</param>
        /// <returns></returns>
        public List<Users> SelectRadiusUsers(int idHotel)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.Users
                                where h.IdHotel.Equals(idHotel)
                                select h);

                List<Users> listado = new List<Users>();

                foreach (var element in elements)
                {
                    Users obj = new Users();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        /// <summary>
        /// Procedimiento que obtiene los usuarios de un site que tienen el mismo nombre y han sido creado a la vez, sirve para localizar los 
        /// usuarios que se han creado concatenados por el nextbillingtype al agotarse el usuario actual
        /// </summary>
        /// <param name="idHotel"></param>
        /// <param name="username"></param>
        /// <param name="validSince"></param>
        /// <returns></returns>
        public List<UserAll> SelectRadiusUsers(int idHotel, string username, DateTime validSince)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.Users
                                where h.IdHotel.Equals(idHotel) && h.Name.Equals(username)
                                && h.ValidSince.Equals(validSince)
                                orderby h.ValidTill ascending
                                select h);

                List<UserAll> listado = new List<UserAll>();

                foreach (var element in elements)
                {
                    UserAll obj = new UserAll();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }
        

        #endregion

        #region RegisterUsers

        /// <summary>
        /// Insertamos un nuevo usuario en el radius
        /// </summary>
        /// <param name="obj">Elemento del tipo RadiusUser a insertar</param>
        public void Insert(RegisterUsers obj)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                context.RegisterUsers.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        /// <summary>
        /// Actualizamos los datos de un hotel existente
        /// </summary>
        /// <param name="obj">Elemento del tipo Hotel con los datos a actualizar</param>
        public void Update(RegisterUsers obj)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.RegisterUsers
                               where h.IdRegister.Equals(obj.IdRegister)
                               select h).SingleOrDefault();

                if (element != null)
                {
                    Util.Map(obj, element);
                }

                context.SubmitChanges();
            }
        }

        public void Select(RegisterUsers obj, int idRegister)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.RegisterUsers
                               where h.IdRegister.Equals(idRegister)
                               select h).FirstOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        public void Select(RegisterUsers obj, string username)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.RegisterUsers
                               where h.User.ToUpper().Equals(username.ToUpper())
                               select h).FirstOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        #endregion

        #region FDSUsers

        /// <summary>
        /// Seleccionar un usuario del user 
        /// </summary>
        /// <param name="obj">Objeto del tipo RadiusUser donde se devolveran los datos del usuario</param>
        /// <param name="idHotel">Identificador del hotel a seleccionar</param>
        /// 
        public void Update(FDSUsers obj)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.FDSUsers
                               where h.IdUser.Equals(obj.IdUser)
                               select h).SingleOrDefault();

                if (element != null)
                {
                    Util.Map(obj, element);
                
                }

                context.SubmitChanges();
            }
        }

        public void Insert(FDSUsers obj)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                context.FDSUsers.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        static public void Select(FDSUsers obj, int idUser)
        {
            try
            {
                using (DataModel context = new DataModel(Util.ConnectionString()))
                {
                    var element = (from h in context.FDSUsers
                                   where h.IdUser.Equals(idUser)
                                   select h).SingleOrDefault();

                    if (element != null)
                        Util.Map(element, obj);
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("SQL ERROR", ex);
            }
            catch(Exception ex)
            {
                throw new Exception("NO CONTEMPLATE ERROR", ex);
            }
        }
        
        static public void Select(FDSUsers obj, string username, string password)
        {
            try
            {
                using (DataModel context = new DataModel(Util.ConnectionString()))
                {
                    var element = (from h in context.FDSUsers
                                   where h.Username.Equals(username) && h.Password.Equals(password)
                                   select h).SingleOrDefault();

                    if (element != null)
                        Util.Map(element, obj);
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("SQL ERROR", ex);
            }
            catch (Exception ex)
            {
                throw new Exception("NO CONTEMPLATE ERROR", ex);
            }
        }

        static public void Select(FDSUsersType obj, int idType)
        {
            try
            {
                using (DataModel context = new DataModel(Util.ConnectionString()))
                {
                    var element = (from h in context.FDSUsersType
                                   where h.Id.Equals(idType)
                                   select h).SingleOrDefault();

                    if (element != null)
                    {
                        obj.Id = element.Id;
                        obj.Name = element.Name;
                        obj.Descpription = element.Descpription;
                    }
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("SQL ERROR", ex);
            }
            catch (Exception ex)
            {
                throw new Exception("NO CONTEMPLATE ERROR", ex);
            }
        }

        static public void Select(FDSLocationsIdentifier obj, string identifier)
        {
            try
            {
                using (DataModel context = new DataModel(Util.ConnectionString()))
                {
                    var element = (from h in context.FDSLocationsIdentifier
                                   where h.Identifier.Equals(identifier)
                                   select h).SingleOrDefault();

                    if (element != null)
                    {
                        obj.Id = element.Id;
                        obj.IdSite = element.IdSite;
                        obj.IdLocation = element.IdLocation;
                        obj.Identifier = element.Identifier;
                    }
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("SQL ERROR", ex);
            }
            catch (Exception ex)
            {
                throw new Exception("NO CONTEMPLATE ERROR", ex);
            }

        }

        static public void Select(FDSLocationsIdentifier obj, int idlocation)
        {
            try
            {
                using (DataModel context = new DataModel(Util.ConnectionString()))
                {
                    var element = (from h in context.FDSLocationsIdentifier
                                   where h.IdLocation.Equals(idlocation)
                                   select h).SingleOrDefault();

                    if (element != null)
                    {
                        obj.Id = element.Id;
                        obj.IdSite = element.IdSite;
                        obj.IdLocation = element.IdLocation;
                        obj.Identifier = element.Identifier;
                    }
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("SQL ERROR", ex);
            }
            catch (Exception ex)
            {
                throw new Exception("NO CONTEMPLATE ERROR", ex);
            }
        }

        static public void Select(FDSUsersAccess obj, int idUser)
        {
            try
            {
                using (DataModel context = new DataModel(Util.ConnectionString()))
                {
                    var element = (from h in context.FDSUsersAccess
                                   where h.IdUser.Equals(idUser)
                                   select h).FirstOrDefault();

                    if (element != null)
                        Util.Map(element, obj);

                }
            }
            catch (SqlException ex)
            {
                throw new Exception("SQL ERROR", ex);
            }
            catch (Exception ex)
            {
                throw new Exception("NO CONTEMPLATE ERROR", ex);
            }
        }

        static public List<FDSUsersAccess> SelectFDSUserAccesses()
        {
                using (DataModel context = new DataModel(Util.ConnectionString()))
                {
                    var elements = (from h in context.FDSUsersAccess
                                    select h);

                    List<FDSUsersAccess> list = new List<FDSUsersAccess>();
                    foreach (var element in elements)
                    {
                        FDSUsersAccess obj = new FDSUsersAccess();
                        Util.Map(element, obj);

                        list.Add(obj);
                    }

                    return list;
                }
        }

        static public List<FDSUsersAccess> SelectFDSUserAccessesBySite(int idSite)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.FDSUsersAccess
                                where h.IdSite.Equals(idSite)
                                select h);

                List<FDSUsersAccess> list = new List<FDSUsersAccess>();
                foreach (var element in elements)
                {
                    FDSUsersAccess obj = new FDSUsersAccess();
                    Util.Map(element, obj);

                    list.Add(obj);
                }

                return list;
            }
        }

        static public List<FDSUsersAccess> SelectFDSUserAccesses(int idUser)
        {
            try
            {
                using (DataModel context = new DataModel(Util.ConnectionString()))
                {
                    var elements = (from h in context.FDSUsersAccess
                                    where h.IdUser.Equals(idUser)
                                    select h);

                    List<FDSUsersAccess> list = new List<FDSUsersAccess>();
                    foreach (var element in elements)
                    {

                        FDSUsersAccess obj = new FDSUsersAccess();
                        obj.Id = element.Id;
                        obj.IdSite = element.IdSite;
                        obj.IdGroup = element.IdGroup;
                        obj.IdLocation = element.IdLocation;

                        list.Add(obj);
                    }

                    return list;
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("SQL ERROR", ex);
            }
            catch (Exception ex)
            {
                throw new Exception("NO CONTEMPLATE ERROR", ex);
            }
        }

        static public List<FDSSitesGroups> SelectFDSSitesGroup(int idGroup)
        {
            try
            {
                using (DataModel context = new DataModel(Util.ConnectionString()))
                {
                    var elements = (from h in context.FDSSitesGroups
                                    where h.IdGroup.Equals(idGroup)
                                    select h);

                    List<FDSSitesGroups> list = new List<FDSSitesGroups>();
                    foreach (var element in elements)
                    {

                        FDSSitesGroups obj = new FDSSitesGroups();
                        obj.Id = element.Id;
                        obj.IdSite = element.IdSite;
                        obj.IdGroup = element.IdGroup;

                        list.Add(obj);
                    }

                    return list;
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("SQL ERROR", ex);
            }
            catch (Exception ex)
            {
                throw new Exception("NO CONTEMPLATE ERROR", ex);
            }
        }

        static public List<Hotels> SelectSitesUser(int idUser)
        {
            try
            {
                using (DataModel context = new DataModel(Util.ConnectionString()))
                {
                    var elements = context.SP_360_SELECTSITESUSER(idUser);

                    List<Hotels> list = new List<Hotels>();
                    foreach (var element in elements)
                    {
                        Hotels obj = new Hotels();
                        Util.Map(element, obj);

                        list.Add(obj);
                    }

                    return list;
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("SQL ERROR", ex);
            }
            catch (Exception ex)
            {
                throw new Exception("NO CONTEMPLATE ERROR", ex);
            }
        }

        static public List<FDSLocationsIdentifier> SelectFDSLocationsIdentifiers(int idSite)
        {
            try
            {
                using (DataModel context = new DataModel(Util.ConnectionString()))
                {
                    var elements = (from h in context.FDSLocationsIdentifier
                                    where h.IdSite.Equals(idSite)
                                    select h);

                    List<FDSLocationsIdentifier> list = new List<FDSLocationsIdentifier>();
                    foreach (var element in elements)
                    {

                        FDSLocationsIdentifier obj = new FDSLocationsIdentifier();
                        obj.Id = element.Id;
                        obj.IdSite = element.IdSite;
                        obj.IdLocation = element.IdLocation;
                        obj.Identifier = element.Identifier;

                        list.Add(obj);
                    }

                    return list;
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("SQL ERROR", ex);
            }
            catch (Exception ex)
            {
                throw new Exception("NO CONTEMPLATE ERROR", ex);
            }
        }

        static public List<MerakiLocationsGPS> SelectAllMerakiLocationsGPSSite(int idLoc, int idSite)
        {
            try
            {
                using (DataModel2 context = new DataModel2(Util.ConnectionString()))
                {
                    var elements = (from h in context.MerakiLocationsGPS
                                    where (h.IdSite.Equals(idSite) && h.IdLocation.Equals(idLoc))
                                    select h);
                    List<MerakiLocationsGPS> list = new List<MerakiLocationsGPS>();
                    foreach (var element in elements)
                    {
                        MerakiLocationsGPS obj = new MerakiLocationsGPS();
                        Util.Map(element, obj);
                        list.Add(obj);
                    }

                    return list;
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("SQL ERROR", ex);
            }
            catch (Exception ex)
            {
                throw new Exception("NO CONTEMPLATE ERROR", ex);
            }
        }


        static public List<FDSLocationsIdentifier> SelectFDSLocationsIdentifiers(int idLoc, int idSite)
        {
            try
            {
                using (DataModel context = new DataModel(Util.ConnectionString()))
                {
                    var elements = (from h in context.FDSLocationsIdentifier
                                    where (h.IdSite.Equals(idSite) && h.IdLocation.Equals(idLoc))
                                    select h);

                    List<FDSLocationsIdentifier> list = new List<FDSLocationsIdentifier>();
                    foreach (var element in elements)
                    {

                        FDSLocationsIdentifier obj = new FDSLocationsIdentifier();
                        obj.Id = element.Id;
                        obj.IdSite = element.IdSite;
                        obj.IdLocation = element.IdLocation;
                        obj.Identifier = element.Identifier;

                        list.Add(obj);
                    }

                    return list;
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("SQL ERROR", ex);
            }
            catch (Exception ex)
            {
                throw new Exception("NO CONTEMPLATE ERROR", ex);
            }
        }

        static public List<FDSUsers> SelectFDSUsers()
        {
            try
            {
                using (DataModel context = new DataModel(Util.ConnectionString()))
                {
                    var elements = (from h in context.FDSUsers
                                    select h);

                    List<FDSUsers> list = new List<FDSUsers>();

                    foreach (var element in elements)
                    {

                        FDSUsers obj = new FDSUsers();
                        Util.Map(element, obj);

                        list.Add(obj);
                    }

                    return list;
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("SQL ERROR", ex);
            }
            catch (Exception ex)
            {
                throw new Exception("NO CONTEMPLATE ERROR", ex);
            }
        }

        static public List<FDSUsers> SelectFDSUsers(string name, int pageIndex, int pageSize, ref int pageNumber)
        {
            try
            {
                using (DataModel context = new DataModel(Util.ConnectionString()))
                {
                    var elements = (from h in context.FDSUsers
                                    where h.Username.Contains(name)
                                    select h);

                    List<FDSUsers> list = new List<FDSUsers>();

                    int count = elements.Count();
                    pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                    elements = elements.Skip((pageIndex) * pageSize).Take(pageSize);

                    foreach (var element in elements)
                    {

                        FDSUsers obj = new FDSUsers();
                        Util.Map(element, obj);

                        list.Add(obj);
                    }

                    return list;
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("SQL ERROR", ex);
            }
            catch (Exception ex)
            {
                throw new Exception("NO CONTEMPLATE ERROR", ex);
            }
        }

        static public List<FDSUsers> SelectFDSUsersLocation(int idlocation)
        {
            try
            {
                using (DataModel context = new DataModel(Util.ConnectionString()))
                {
                    var elements = (from h in context.FDSUsers
                                    join t in context.FDSUsersAccess on h.IdUser equals t.IdUser
                                    where t.IdLocation.Equals(idlocation)
                                    select h);

                    List<FDSUsers> list = new List<FDSUsers>();
                    foreach (var element in elements)
                    {

                        FDSUsers obj = new FDSUsers();
                        Util.Map(element, obj);

                        list.Add(obj);
                    }

                    return list;
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("SQL ERROR", ex);
            }
            catch (Exception ex)
            {
                throw new Exception("NO CONTEMPLATE ERROR", ex);
            }
        }

        static public List<FDSUsers> SelectFDSUsersSite(int idSite)
        {
            try
            {
                using (DataModel context = new DataModel(Util.ConnectionString()))
                {
                    var elements = (from h in context.FDSUsers
                                    join t in context.FDSUsersAccess on h.IdUser equals t.IdUser
                                    where t.IdSite.Equals(idSite)
                                    select h);

                    List<FDSUsers> list = new List<FDSUsers>();
                    foreach (var element in elements)
                    {

                        FDSUsers obj = new FDSUsers();
                        Util.Map(element, obj);

                        list.Add(obj);
                    }

                    return list;
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("SQL ERROR", ex);
            }
            catch (Exception ex)
            {
                throw new Exception("NO CONTEMPLATE ERROR", ex);
            }
        }

        static public List<FDSUsers> SelectFDSUsersInBillings(string listHoteles, DateTime start, DateTime end)
        {
            try
            {
                List<FDSUsers> list = new List<FDSUsers>();
                using (DataModel context = new DataModel(Util.ConnectionString()))
                {
                    string[] ids = listHoteles.Split(',');
                    int count = 0;
                    if (ids[0] != null)
                    {
                        string consulta = "select * from FDSUsers where IdUser in (select distinct IdFDSUser from Billings where (IdHotel =" + ids[0];
                        foreach (string id in ids)
                        {
                            consulta += " or IdHotel = " + id;
                        }
                        consulta += ") and billable = 1 ";
                        consulta += " AND BillingCharge >= '" + start.ToString("yyyy/MM/dd HH:mm:ss") + "' AND BillingCharge <= '" + end.ToString("yyyy/MM/dd HH:mm:ss") + "')";
                        IEnumerable<FDSUsers> result_query = context.ExecuteQuery<FDSUsers>(consulta);
                        count++;

                        var result = result_query.ToList();
                       
                        foreach (var element in result)
                        {
                            FDSUsers obj = new FDSUsers();
                            Util.Map(element, obj);

                            list.Add(obj);
                        }
                    }
                }
                return list;
            }
            catch (SqlException ex)
            {
                throw new Exception("SQL ERROR", ex);
            }
            catch (Exception ex)
            {
                throw new Exception("NO CONTEMPLATE ERROR", ex);
            }
        }

        public void Insert(FDSUsersAccess obj)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                context.FDSUsersAccess.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }


        public void Update(FDSUsersAccess obj)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.FDSUsersAccess
                               where h.Id.Equals(obj.Id)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(obj, element);

                context.SubmitChanges();
            }
        }


        static public List<FDSUsers> SelectFDSUsersGroup(int idGroup)
        {
            try
            {
                using (DataModel context = new DataModel(Util.ConnectionString()))
                {
                    var elements = (from h in context.FDSUsers
                                    join t in context.FDSUsersAccess on h.IdUser equals t.IdUser
                                    where t.IdGroup.Equals(idGroup)
                                    select h);

                    List<FDSUsers> list = new List<FDSUsers>();
                    foreach (var element in elements)
                    {

                        FDSUsers obj = new FDSUsers();
                        Util.Map(element, obj);

                        list.Add(obj);
                    }

                    return list;
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("SQL ERROR", ex);
            }
            catch (Exception ex)
            {
                throw new Exception("NO CONTEMPLATE ERROR", ex);
            }
        }

        #endregion

        #region Rols

        static public void Select(Rols obj, int idRol)
        {
            try
            {
                using (DataModel context = new DataModel(Util.ConnectionString()))
                {
                    var element = (from h in context.Rols
                                   where h.IdRol.Equals(idRol)
                                   select h).SingleOrDefault();

                    if (element != null)
                        Util.Map(element, obj);
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("SQL ERROR", ex);
            }
            catch (Exception ex)
            {
                throw new Exception("NO CONTEMPLATE ERROR", ex);
            }
        }

        
        static public List<Rols> SelectFDSRols()
        {
            try
            {
                using (DataModel context = new DataModel(Util.ConnectionString()))
                {
                    var elements = (from h in context.Rols
                                    select h);

                    List<Rols> list = new List<Rols>();

                    foreach (var element in elements)
                    {
                        Rols obj = new Rols();
                        Util.Map(element, obj);

                        list.Add(obj);
                    }

                    return list;
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("SQL ERROR", ex);
            }
            catch (Exception ex)
            {
                throw new Exception("NO CONTEMPLATE ERROR", ex);
            }
        }

        static public List<FDSUsersType> SelectFDSUserTypes()
        {
            try
            {
                using (DataModel context = new DataModel(Util.ConnectionString()))
                {
                    var elements = (from h in context.FDSUsersType
                                    select h);

                    List<FDSUsersType> list = new List<FDSUsersType>();

                    foreach (var element in elements)
                    {
                        FDSUsersType obj = new FDSUsersType();
                        Util.Map(element, obj);

                        list.Add(obj);
                    }

                    return list;
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("SQL ERROR", ex);
            }
            catch (Exception ex)
            {
                throw new Exception("NO CONTEMPLATE ERROR", ex);
            }
        }

        #endregion

        #region Rooms

        /// <summary>
        /// Seleccionar una habitación del hotel
        /// </summary>
        /// <param name="obj">Objeto del tipo habitacion</param>
        /// <param name="idHotel">Identificador de la habitacion a seleccionar</param>
        public void Select(Rooms obj, int idRoom)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.Rooms
                               where h.IdRoom.Equals(idRoom)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        public void Select(Rooms obj,  int idHotel, string name)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.Rooms
                               where (h.IdHotel.Equals(idHotel) && h.Name.ToUpper().Equals(name.ToUpper()))
                               select h).SingleOrDefault();

                if (element != null)
                {
                    obj.IdHotel = element.IdHotel;
                    obj.IdRoom = element.IdRoom;
                    obj.Name = element.Name;
                    obj.CheckIn = element.CheckIn;
                    obj.CheckInDate = element.CheckInDate;
                    obj.CheckOutDate = element.CheckOutDate;
                    obj.Blocked = element.Blocked;
                    obj.STB_Mac = element.STB_Mac;
                    obj.STB_IP = element.STB_IP;

                }
            }
        }

        /// <summary>
        /// Insertamos una nueva habitacion
        /// </summary>
        /// <param name="obj">Elemento del tipo habitacion a insertar</param>
        public void Insert(Rooms obj)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                context.Rooms.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        /// <summary>
        /// Actualizamos los datos de una habitacion existente
        /// </summary>
        /// <param name="obj">Elemento del tipo habitacion con los datos a actualizar</param>
        public void Update(Rooms obj)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.Rooms
                               where h.IdRoom.Equals(obj.IdRoom)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(obj, element);

                context.SubmitChanges();
            }
        }

        /// <summary>
        /// Seleccionar el listado de todas las habitaciones
        /// </summary>
        /// <returns></returns>
        public List<Rooms> SelectRooms()
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.Rooms
                                select h);

                List<Rooms> listado = new List<Rooms>();

                foreach (var element in elements)
                {
                    Rooms obj = new Rooms();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        /// <summary>
        /// Seleccionar de todas las habitaciones de un hotel
        /// </summary>
        /// <param name="idChain">Identificador de la cadena de hoteles</param>
        /// <returns></returns>
        public List<Rooms> SelectRooms(int idHotel)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                //var elements = (from h in context.Rooms
                //                where h.IdHotel.Equals(idHotel)
                //                select h);

                var elements = context.SP_FDS_OBTAINROOMS(idHotel);

                List<Rooms> listado = new List<Rooms>();

                foreach (var element in elements)
                {
                    Rooms obj = new Rooms();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        #endregion

        #region accounting

        public List<Accounting> OnlineUsers(int idHotel)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = from accounting in context.Accounting
                            join activesessions in context.ActiveSessions on new { AcctSessionId = accounting.AcctSessionId } equals new { AcctSessionId = activesessions.AcctSessionID }
                            where
                              activesessions.IdHotel.Equals(idHotel)
                            group accounting by new
                            {
                                accounting.AcctSessionId
                            } into g
                            select new
                            {
                                g.Key.AcctSessionId,
                                SessionTime = (System.Int32?)g.Max(p => p.SessionTime),
                                BytesIn = (System.Int32?)g.Max(p => p.BytesIn),
                                BytesOut = (System.Int32?)g.Max(p => p.BytesOut)//,
                                //User = g.Select(p => p.User).FirstOrDefault()
                            };

                List<Accounting> list = new List<Accounting>();

                foreach (var q in elements)
                {
                    Accounting obj = new Accounting();
                    Util.Map(q, obj);
                    list.Add(obj);
                }

                return list;
            }
        }

        public List<UsersOnlineInfo> OnlineUsers(int idHotel, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var count = context.SP_FDS_OBTAINUSERSONLINE(idHotel).Count();
                var elements = context.SP_FDS_OBTAINUSERSONLINE(idHotel).Skip((pageIndex) * pageSize).Take(pageSize);
                
                List<UsersOnlineInfo> list = new List<UsersOnlineInfo>();

                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                foreach (var q in elements)
                {
                    UsersOnlineInfo obj = new UsersOnlineInfo();
                    Util.Map(q, obj);
                    obj.SessionStarted = q.SessionStarted.Value;
                    obj.BytesIn = q.BytesIn.Value;
                    obj.BytesOut = q.BytesOut.Value;
                    obj.Comment = q.Comment;
                    
                    list.Add(obj);
                }

                return list;
            }
        }

        public List<UsersOnlineInfo> OnlineUsersFilter(int idHotel, string username, bool? iot, int? idbillingtype, int? idpriority, int? idfilter, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                List<UsersOnlineInfo> list = new List<UsersOnlineInfo>();

                var count = context.SP_FDS_OBTAINUSERSONLINEFILTER(idHotel, iot, '%' + username + '%', idbillingtype, idpriority, idfilter).Count();
                var elements = context.SP_FDS_OBTAINUSERSONLINEFILTER(idHotel, iot, '%' + username + '%', idbillingtype, idpriority, idfilter).Skip((pageIndex) * pageSize).Take(pageSize);
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                List<FilteringProfiles> filters = SelectFilteringProfiles(idHotel);
                List<Priorities> priorities = SelectPriorities(idHotel);

                foreach (var q in elements)
                {
                    UsersOnlineInfo obj = new UsersOnlineInfo();
                    Util.Map(q, obj);
                    obj.SessionStarted = q.SessionStarted.Value;
                    obj.BytesIn = q.BytesIn.Value;
                    obj.BytesOut = q.BytesOut.Value;
                    obj.Comment = q.Comment;
                    obj.IdUser = q.IdUser.Value;
                    obj.AccessType = q.AccessType;
                    obj.AcctSessionID = q.AcctSessionID;
                    obj.Room = q.Room;
                    obj.IdUser = q.IdUser.Value;
                    FilteringProfiles filter = (from t in filters where t.IdFilter.Equals(q.Filter_Id) select t).FirstOrDefault();
                    if (filter != null)
                        obj.Filter = filter.Description;
                    else
                        obj.Filter = string.Empty;
                    Priorities priority = (from t in priorities where t.IdPriority.Equals(q.IdPriority) select t).FirstOrDefault();
                    if (priority != null)
                        obj.Priority = priority.Description;
                    else
                        obj.Priority = string.Empty;
                    

                    list.Add(obj);
                }

                return list;
            }
        }

        public List<UsersOnlineInfo> OnlineUsersFilter(int idHotel, string username, bool? iot, int? idbillingtype, int? idpriority, int? idfilter, int idfdsuser, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                List<UsersOnlineInfo> list = new List<UsersOnlineInfo>();

                var count = context.SP_FDS_OBTAINUSERSONLINEFILTERALL(idHotel, iot, '%' + username + '%', idbillingtype, idpriority, idfilter, idfdsuser).Count();
                var elements = context.SP_FDS_OBTAINUSERSONLINEFILTERALL(idHotel, iot, '%' + username + '%', idbillingtype, idpriority, idfilter, idfdsuser).Skip((pageIndex) * pageSize).Take(pageSize);
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                List<FilteringProfiles> filters = SelectFilteringProfiles(idHotel);
                List<Priorities> priorities = SelectPriorities(idHotel);

                foreach (var q in elements)
                {
                    UsersOnlineInfo obj = new UsersOnlineInfo();
                    Util.Map(q, obj);
                    obj.SessionStarted = q.SessionStarted.Value;
                    obj.BytesIn = q.BytesIn.Value;
                    obj.BytesOut = q.BytesOut.Value;
                    obj.Comment = q.Comment;
                    obj.IdUser = q.IdUser.Value;
                    obj.AccessType = q.AccessType;
                    obj.AcctSessionID = q.AcctSessionID;
                    obj.Room = q.Room;
                    obj.IdUser = q.IdUser.Value;
                    FilteringProfiles filter = (from t in filters where t.IdFilter.Equals(q.Filter_Id) select t).FirstOrDefault();
                    if (filter != null)
                        obj.Filter = filter.Description;
                    else
                        obj.Filter = string.Empty;
                    Priorities priority = (from t in priorities where t.IdPriority.Equals(q.IdPriority) select t).FirstOrDefault();
                    if (priority != null)
                        obj.Priority = priority.Description;
                    else
                        obj.Priority = string.Empty;

                    obj.IdFDSUser = q.IdFDSUser.Value;
                    list.Add(obj);
                }

                return list;
            }
        }


        public List<UsersOnlineInfo> OnlineUsersFilter(int idHotel, List<FDSUsers> users, string username, bool? iot, int? idbillingtype, int? idpriority, int? idfilter, int idfdsuser, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                List<UsersOnlineInfo> list = new List<UsersOnlineInfo>();

                var elements = context.SP_FDS_OBTAINUSERSONLINEFILTERALL(idHotel, iot, '%' + username + '%', idbillingtype, idpriority, idfilter, idfdsuser);

                List<FilteringProfiles> filters = SelectFilteringProfiles(idHotel);
                List<Priorities> priorities = SelectPriorities(idHotel);

                foreach (var q in elements)
                {
                    FDSUsers u = (from t in users where t.IdUser.Equals(q.IdFDSUser) select t).FirstOrDefault();
                    if ((u != null) || q.IdFDSUser.Equals(0))
                    {
                        UsersOnlineInfo obj = new UsersOnlineInfo();
                        Util.Map(q, obj);
                        obj.SessionStarted = q.SessionStarted.Value;
                        obj.BytesIn = q.BytesIn.Value;
                        obj.BytesOut = q.BytesOut.Value;
                        obj.Comment = q.Comment;
                        obj.IdUser = q.IdUser.Value;
                        obj.AccessType = q.AccessType;
                        obj.AcctSessionID = q.AcctSessionID;
                        obj.Room = q.Room;
                        obj.IdUser = q.IdUser.Value;
                        FilteringProfiles filter = (from t in filters where t.IdFilter.Equals(q.Filter_Id) select t).FirstOrDefault();
                        if (filter != null)
                            obj.Filter = filter.Description;
                        else
                            obj.Filter = string.Empty;
                        Priorities priority = (from t in priorities where t.IdPriority.Equals(q.IdPriority) select t).FirstOrDefault();
                        if (priority != null)
                            obj.Priority = priority.Description;
                        else
                            obj.Priority = string.Empty;

                        obj.IdFDSUser = q.IdFDSUser.Value;
                        list.Add(obj);
                    }
                }

                int count = list.Count();
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                return list.Skip((pageIndex) * pageSize).Take(pageSize).ToList();
            }
        }

        public List<UsersOnlineInfo> OnlineUsersFilter(string listHoteles, string username, bool? iot, int? idbillingtype, int? idpriority, int? idfilter, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                List<UsersOnlineInfo> list = new List<UsersOnlineInfo>();
                List<FilteringProfiles> filters = new List<FilteringProfiles>(); ;
                List<Priorities> priorities = new List<Priorities>();   
                int count = 0;
                string[] ids = listHoteles.Split(',');
                foreach (string id in ids)
                {
                    int idHotel = int.Parse(id);
                    count += context.SP_FDS_OBTAINUSERSONLINEFILTER(idHotel, iot, '%' + username + '%', idbillingtype, idpriority, idfilter).Count();
                    var elements = context.SP_FDS_OBTAINUSERSONLINEFILTER(idHotel, iot, '%' + username + '%', idbillingtype, idpriority, idfilter);//.Skip((pageIndex) * pageSize).Take(pageSize);
                    

                    filters = filters.Concat(SelectFilteringProfiles(idHotel)).ToList();
                    priorities = priorities.Concat(SelectPriorities(idHotel)).ToList();

                    foreach (var q in elements)
                    {
                        UsersOnlineInfo obj = new UsersOnlineInfo();
                        Util.Map(q, obj);
                        obj.SessionStarted = q.SessionStarted.Value;
                        obj.BytesIn = q.BytesIn.Value;
                        obj.BytesOut = q.BytesOut.Value;
                        obj.Comment = q.Comment;
                        obj.IdUser = q.IdUser.Value;
                        obj.AccessType = q.AccessType;
                        obj.AcctSessionID = q.AcctSessionID;
                        obj.Room = q.Room;
                        obj.IdUser = q.IdUser.Value;
                        FilteringProfiles filter = (from t in filters where t.IdFilter.Equals(q.Filter_Id) select t).FirstOrDefault();
                        if (filter != null)
                            obj.Filter = filter.Description;
                        else
                            obj.Filter = string.Empty;
                        Priorities priority = (from t in priorities where t.IdPriority.Equals(q.IdPriority) select t).FirstOrDefault();
                        if (priority != null)
                            obj.Priority = priority.Description;
                        else
                            obj.Priority = string.Empty;


                        list.Add(obj);
                    }
                }

                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                return list.Skip((pageIndex) * pageSize).Take(pageSize).ToList();
            }

        }

        public List<UsersOnlineInfo> OnlineUsers(string locationIdentifier, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var count = context.SP_FDS_OBTAINUSERSONLINELOCATION(locationIdentifier).Count();
                var elements = context.SP_FDS_OBTAINUSERSONLINELOCATION(locationIdentifier).Skip((pageIndex) * pageSize).Take(pageSize);

                List<UsersOnlineInfo> list = new List<UsersOnlineInfo>();

                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                foreach (var q in elements)
                {
                    UsersOnlineInfo obj = new UsersOnlineInfo();
                    Util.Map(q, obj);
                    obj.SessionStarted = q.SessionStarted.Value;
                    obj.BytesIn = q.BytesIn.Value;
                    obj.BytesOut = q.BytesOut.Value;
                    obj.Comment = q.Comment;
                    list.Add(obj);
                }

                return list;
            }
        }

        public List<UsersOnlineInfo> OnlineUsersFilter(string locationIdentifier, string username, bool? iot, int? idbillingtype, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var count = context.SP_FDS_OBTAINUSERSONLINELOCATIONFILTER(locationIdentifier, iot, '%' + username + '%', idbillingtype).Count();
                var elements = context.SP_FDS_OBTAINUSERSONLINELOCATIONFILTER(locationIdentifier, iot, '%' + username + '%', idbillingtype).Skip((pageIndex) * pageSize).Take(pageSize);

                List<UsersOnlineInfo> list = new List<UsersOnlineInfo>();

                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                FDSLocationsIdentifier identifier = BillingController.GetFDSLocationIdentifier(locationIdentifier);
                List<FilteringProfiles> filters = SelectFilteringProfiles(identifier.IdSite);
                List<Priorities> priorities = SelectPriorities(identifier.IdSite);

                foreach (var q in elements)
                {
                    UsersOnlineInfo obj = new UsersOnlineInfo();
                    Util.Map(q, obj);
                    obj.SessionStarted = q.SessionStarted.Value;
                    obj.BytesIn = q.BytesIn.Value;
                    obj.BytesOut = q.BytesOut.Value;
                    obj.Comment = q.Comment;
                    obj.IdUser = q.IdUser.Value;
                    FilteringProfiles filter = (from t in filters where t.IdFilter.Equals(q.Filter_Id) select t).FirstOrDefault();
                    if (filter != null)
                        obj.Filter = filter.Description;
                    else
                        obj.Filter = string.Empty;
                    Priorities priority = (from t in priorities where t.IdPriority.Equals(q.IdPriority) select t).FirstOrDefault();
                    if (priority != null)
                        obj.Priority = priority.Description;
                    else
                        obj.Priority = string.Empty;
                    list.Add(obj);
                }

                return list;
            }
        }

        public List<UsersOnlineInfo> OnlineUsersFilter(string locationIdentifier, string username, bool? iot, int? idbillingtype, int idfdsuser, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var count = context.SP_FDS_OBTAINUSERSONLINELOCATIONALLFILTER(locationIdentifier, iot, '%' + username + '%', idbillingtype, idfdsuser).Count();
                var elements = context.SP_FDS_OBTAINUSERSONLINELOCATIONALLFILTER(locationIdentifier, iot, '%' + username + '%', idbillingtype, idfdsuser).Skip((pageIndex) * pageSize).Take(pageSize);

                List<UsersOnlineInfo> list = new List<UsersOnlineInfo>();

                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                FDSLocationsIdentifier identifier = BillingController.GetFDSLocationIdentifier(locationIdentifier);
                List<FilteringProfiles> filters = SelectFilteringProfiles(identifier.IdSite);
                List<Priorities> priorities = SelectPriorities(identifier.IdSite);

                foreach (var q in elements)
                {
                    UsersOnlineInfo obj = new UsersOnlineInfo();
                    Util.Map(q, obj);
                    obj.SessionStarted = q.SessionStarted.Value;
                    obj.BytesIn = q.BytesIn.Value;
                    obj.BytesOut = q.BytesOut.Value;
                    obj.Comment = q.Comment;
                    obj.IdUser = q.IdUser.Value;
                    FilteringProfiles filter = (from t in filters where t.IdFilter.Equals(q.Filter_Id) select t).FirstOrDefault();
                    if (filter != null)
                        obj.Filter = filter.Description;
                    else
                        obj.Filter = string.Empty;
                    Priorities priority = (from t in priorities where t.IdPriority.Equals(q.IdPriority) select t).FirstOrDefault();
                    if (priority != null)
                        obj.Priority = priority.Description;
                    else
                        obj.Priority = string.Empty;

                    obj.IdFDSUser = q.IdFDSUser.Value;
                    list.Add(obj);
                }

                return list;
            }
        }

        public List<UsersOnlineInfo> OnlineUsersFilter(string locationIdentifier, List<FDSUsers> users, string username, bool? iot, int? idbillingtype, int idfdsuser, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var elements = context.SP_FDS_OBTAINUSERSONLINELOCATIONALLFILTER(locationIdentifier, iot, '%' + username + '%', idbillingtype, idfdsuser);

                List<UsersOnlineInfo> list = new List<UsersOnlineInfo>();

                FDSLocationsIdentifier identifier = BillingController.GetFDSLocationIdentifier(locationIdentifier);
                List<FilteringProfiles> filters = SelectFilteringProfiles(identifier.IdSite);
                List<Priorities> priorities = SelectPriorities(identifier.IdSite);

                foreach (var q in elements)
                {
                    if (!idfdsuser.Equals(0))
                    {
                        UsersOnlineInfo obj = new UsersOnlineInfo();
                        Util.Map(q, obj);
                        obj.SessionStarted = q.SessionStarted.Value;
                        obj.BytesIn = q.BytesIn.Value;
                        obj.BytesOut = q.BytesOut.Value;
                        obj.Comment = q.Comment;
                        obj.IdUser = q.IdUser.Value;
                        FilteringProfiles filter = (from t in filters where t.IdFilter.Equals(q.Filter_Id) select t).FirstOrDefault();
                        if (filter != null)
                            obj.Filter = filter.Description;
                        else
                            obj.Filter = string.Empty;
                        Priorities priority = (from t in priorities where t.IdPriority.Equals(q.IdPriority) select t).FirstOrDefault();
                        if (priority != null)
                            obj.Priority = priority.Description;
                        else
                            obj.Priority = string.Empty;

                        obj.IdFDSUser = q.IdFDSUser.Value;
                        list.Add(obj);
                    }
                    else
                    {
                        FDSUsers u = (from t in users where t.IdUser.Equals(q.IdFDSUser) select t).FirstOrDefault();
                        if ((u != null) || q.IdFDSUser.Equals(0))
                        {
                            UsersOnlineInfo obj = new UsersOnlineInfo();
                            Util.Map(q, obj);
                            obj.SessionStarted = q.SessionStarted.Value;
                            obj.BytesIn = q.BytesIn.Value;
                            obj.BytesOut = q.BytesOut.Value;
                            obj.Comment = q.Comment;
                            obj.IdUser = q.IdUser.Value;
                            FilteringProfiles filter = (from t in filters where t.IdFilter.Equals(q.Filter_Id) select t).FirstOrDefault();
                            if (filter != null)
                                obj.Filter = filter.Description;
                            else
                                obj.Filter = string.Empty;
                            Priorities priority = (from t in priorities where t.IdPriority.Equals(q.IdPriority) select t).FirstOrDefault();
                            if (priority != null)
                                obj.Priority = priority.Description;
                            else
                                obj.Priority = string.Empty;

                            obj.IdFDSUser = q.IdFDSUser.Value;
                            list.Add(obj);

                        }
                    }
                }

                int count = list.Count();
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                return list.Skip((pageIndex) * pageSize).Take(pageSize).ToList();
            }
        }

        public List<UsersOnlineInfo> OnlineUsersIoT(int idHotel, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var count = context.SP_FDS_OBTAINUSERSONLINEIOT(idHotel).Count();
                var elements = context.SP_FDS_OBTAINUSERSONLINEIOT(idHotel).Skip((pageIndex) * pageSize).Take(pageSize);

                List<UsersOnlineInfo> list = new List<UsersOnlineInfo>();

                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                foreach (var q in elements)
                {
                    UsersOnlineInfo obj = new UsersOnlineInfo();
                    Util.Map(q, obj);
                    obj.SessionStarted = q.SessionStarted.Value;
                    obj.BytesIn = q.BytesIn.Value;
                    obj.BytesOut = q.BytesOut.Value;
                    obj.Comment = q.Comment;
                    list.Add(obj);
                }

                return list;
            }
        }

        public List<UsersOnlineInfo> OnlineUsersIoT(string locationIdentifier, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var count = context.SP_FDS_OBTAINUSERSONLINELOCATIONIOT(locationIdentifier).Count();
                var elements = context.SP_FDS_OBTAINUSERSONLINELOCATIONIOT(locationIdentifier).Skip((pageIndex) * pageSize).Take(pageSize);

                List<UsersOnlineInfo> list = new List<UsersOnlineInfo>();

                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                foreach (var q in elements)
                {
                    UsersOnlineInfo obj = new UsersOnlineInfo();
                    Util.Map(q, obj);
                    obj.SessionStarted = q.SessionStarted.Value;
                    obj.BytesIn = q.BytesIn.Value;
                    obj.BytesOut = q.BytesOut.Value;
                    obj.Comment = q.Comment;
                    list.Add(obj);
                }

                return list;
            }
        }

        public List<Accounting> GetAccounting(string user, DateTime start)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from a in context.Accounting
                               where a.User.Equals(user) && a.DateInserted >= start
                               select a);
                               

                List<Accounting> list = new List<Accounting>();

                foreach (var q in elements)
                {
                    Accounting obj = new Accounting();
                    Util.Map(q, obj);
                    list.Add(obj);
                }

                return list;
            }
        }

        public List<Accounting> GetAccounting(string user, DateTime start, string mac)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from a in context.Accounting
                                where a.User.Equals(user) && a.DateInserted >= start && a.CallerID.Equals(mac)
                                select a);


                List<Accounting> list = new List<Accounting>();

                foreach (var q in elements)
                {
                    Accounting obj = new Accounting();
                    Util.Map(q, obj);
                    list.Add(obj);
                }

                return list;
            }
        }

        public List<Accounting> GetAccountingStart(int idHotel, DateTime start)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from a in context.Accounting
                                where a.SystemNo.Equals(idHotel) && a.SessionTime.Equals(0) && a.DateInserted >= start
                                select a);


                List<Accounting> list = new List<Accounting>();

                foreach (var q in elements)
                {
                    Accounting obj = new Accounting();
                    Util.Map(q, obj);
                    list.Add(obj);
                }

                return list;
            }
        }

        public void Select(string AcctSessionID, Accounting obj)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.Accounting
                               where h.AcctSessionId.Equals(AcctSessionID)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(element, obj);

            }
        }

        public void Select(int idActiveSession, ActiveSessions obj)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.ActiveSessions
                               where h.IdActiveSession.Equals(idActiveSession)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(element, obj);

            }
        }

        public void Select(string AcctSessionID, ActiveSessions obj)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.ActiveSessions
                               where h.AcctSessionID.Equals(AcctSessionID)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(element, obj);

            }
        }

        public void Select(string mac, int idhotel, ActiveSessions obj)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.ActiveSessions
                               where h.CallerID.Equals(mac) && h.IdHotel.Equals(idhotel)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(element, obj);

            }
        }

        public List<ClosedSessions> SessionUser(string username)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.ClosedSessions
                               where h.UserName.Equals(username)
                               select h);

                List<ClosedSessions> list = new List<ClosedSessions>();

                foreach (var element in elements)
                {
                    ClosedSessions obj = new ClosedSessions();
                    if (element != null)
                        Util.Map(element, obj);

                    list.Add(obj);
                }

                return list;


            }
        }

        public List<Users> UsuariosPeriodoHabitacion(int idHotel, int idRoom, string username, DateTime start, DateTime end)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from a in context.Users
                                where (a.IdHotel.Equals(idHotel) && (string.IsNullOrEmpty(username) || a.Name.Equals(username)) && ((start <= a.ValidSince && a.ValidSince <= end) || (start <= a.ValidTill && end <= a.ValidTill)))
                                select a).ToList();

                
                List<Users> list = new List<Users>();
                foreach (var element in elements)
                {
                    Users obj = new Users();
                    Util.Map(element, obj);
                    list.Add(obj);
                }
                return list;
            }
        }

        public List<Users> UsuariosPeriodo(int idHotel, DateTime start, DateTime end)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from a in context.Users
                                where (a.IdHotel.Equals(idHotel) && ((start <= a.ValidSince && a.ValidSince <= end) || ( start <= a.ValidTill && end <= a.ValidTill)))
                                orderby a.ValidSince ascending
                                select a).ToList();


                List<Users> list = new List<Users>();
                foreach (var element in elements)
                {
                    Users obj = new Users();
                    Util.Map(element, obj);
                    list.Add(obj);
                }
                return list;
            }
        }

        public List<Users> UsuariosPeriodo(int idHotel, string username, DateTime start, DateTime end)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from a in context.Users
                                where (a.IdHotel.Equals(idHotel) && ((string.IsNullOrEmpty(username)) || (a.Name.Equals(username)))  && ((start <= a.ValidSince && a.ValidSince <= end) || (start <= a.ValidTill && end <= a.ValidTill)))
                                orderby a.ValidSince ascending
                                select a).ToList();


                List<Users> list = new List<Users>();
                foreach (var element in elements)
                {
                    Users obj = new Users();
                    Util.Map(element, obj);
                    list.Add(obj);
                }
                return list;
            }
        }

        public List<Users> GetUsuariosPeriodo(int idHotel, DateTime start, DateTime end)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from a in context.Users
                                where (a.IdHotel.Equals(idHotel) && ((start <= a.ValidSince && a.ValidSince <= end) 
                                || (start <= a.ValidTill && a.ValidTill <= end)))
                                select a).ToList();


                List<Users> list = new List<Users>();
                foreach (var element in elements)
                {
                    Users obj = new Users();
                    Util.Map(element, obj);
                    list.Add(obj);
                }
                return list;
            }
        }

        public List<Users> GetUsuariosPeriodo(int idHotel, DateTime start, DateTime end, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from a in context.Users
                                where (a.IdHotel.Equals(idHotel) && ((start <= a.ValidSince && a.ValidSince <= end)
                                || (start <= a.ValidTill && a.ValidTill <= end)))
                                select a);

                int count = elements.Count();
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                elements = elements.Skip((pageIndex) * pageSize).Take(pageSize);

                List<Users> list = new List<Users>();
                foreach (var element in elements)
                {
                    Users obj = new Users();
                    Util.Map(element, obj);
                    list.Add(obj);
                }
                return list;
            }
        }

        public List<Users> GetUsuariosPeriodo(int idHotel, DateTime start, DateTime end, string username, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var count = context.SP_360_OBTAININTERNETUSAGE(idHotel, start, end, "%" + username +"%").Count();
                var elements = context.SP_360_OBTAININTERNETUSAGE(idHotel, start, end, "%" + username + "%").Skip((pageIndex) * pageSize).Take(pageSize);

                //var elements = (from a in context.Users
                //               where (a.IdHotel.Equals(idHotel) && a.Name.Contains(username) && ((start <= a.ValidSince && a.ValidSince <= end)
                //                || (start <= a.ValidTill && a.ValidTill <= end)))
                //                select a);

                //   int count = elements.Count();
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                //elements = elements.Skip((pageIndex) * pageSize).Take(pageSize);

                List<Users> list = new List<Users>();
                foreach (var element in elements)
                {
                    Users obj = new Users();
                    Util.Map(element, obj);
                    list.Add(obj);
                }
                return list;
            }
        }

        public List<Users> GetUsuariosPeriodo(string listadhohoteles, DateTime start, DateTime end, string username, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                string[] ids = listadhohoteles.Split(',');
                int count = 0;
                List<Users> list = new List<Users>();
                foreach (string id in ids)
                {
                    int idHotel = int.Parse(id);
                    count += context.SP_360_OBTAININTERNETUSAGE(idHotel, start, end, "%" + username + "%").Count();
                    var elements = context.SP_360_OBTAININTERNETUSAGE(idHotel, start, end, "%" + username + "%");

                    foreach (var element in elements)
                    {
                        Users obj = new Users();
                        Util.Map(element, obj);
                        list.Add(obj);
                    }
                }
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                return list.OrderBy(a=>a.ValidSince).Skip((pageIndex) * pageSize).Take(pageSize).ToList();
            }
        }

        public List<Users> GetUsuariosPeriodoLocation(int idLocation, DateTime start, DateTime end, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var billings = (from a in context.BillingTypes
                                where a.IdLocation.Equals(idLocation)
                                select a);


                List<Users> list = new List<Users>();

                foreach (var billing in billings)
                {

                    var elements = (from a in context.Users
                                    where (a.IdBillingType.Equals(billing.IdBillingType) && ((start <= a.ValidSince && a.ValidSince <= end)
                                    || (start <= a.ValidTill && a.ValidTill <= end)))
                                    select a);

                    foreach (var element in elements)
                    {
                        Users obj = new Users();
                        Util.Map(element, obj);
                        list.Add(obj);
                    }
                }

                int count = list.Count();
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                list = list.Skip((pageIndex) * pageSize).Take(pageSize).ToList();

                //List<Users> list = new List<Users>();
               
                return list;
            }
        }

        public List<Users> GetUsuariosPeriodoLocation(int idLocation, int idhotel, DateTime start, DateTime end, string username, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var count = context.SP_360_OBTAININTERNETUSAGELOCATION(idLocation, idhotel, start, end, "%" + username + "%").Count();
                var elements = context.SP_360_OBTAININTERNETUSAGELOCATION(idLocation, idhotel, start, end, "%" + username + "%").Skip((pageIndex) * pageSize).Take(pageSize);

                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                List<Users> list = new List<Users>();
                foreach (var element in elements)
                {
                    Users obj = new Users();
                    Util.Map(element, obj);
                    list.Add(obj);
                }
                return list;

            }
        }


        public List<Users> GetActiveUsers(int idHotel)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                //List<Users> elements = (from a in context.Users
                //                where (a.IdHotel.Equals(idHotel) && a.Enabled.Equals(true)) //&& DateTime.Now >= a.ValidSince && DateTime.Now <= a.ValidTill && a.TimeCredit > 0)
                //                select a).ToList();

                var list = context.SP_360_OBTAINACTIVEUSERS(idHotel);
                List<Users> users = new List<Users>();

                foreach (var element in list)
                {
                    Users o = new Users();
                    Util.Map(element, o);

                    users.Add(o);
                }

                return users;
            }
        }

        public List<Users> GetActiveUsersLocation(int idLocation)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                List<Users> elements = (from a in context.Users
                                        where (a.IdLocation.Equals(idLocation) && a.Enabled.Equals(true) && DateTime.Now >= a.ValidSince && DateTime.Now <= a.ValidTill && a.TimeCredit > 0)
                                        select a).ToList();

                return elements;
            }
        }


        public List<UserInfo> GetActiveUsers(int idHotel, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                int count = context.SP_FDS_OBTAINACTIVEUSERS(idHotel).Count();
                var elementos = context.SP_FDS_OBTAINACTIVEUSERS(idHotel).Skip((pageIndex) * pageSize).Take(pageSize);
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                List<UserInfo> list = new List<UserInfo>();
                foreach (var element in elementos)
                {
                    UserInfo obj = new UserInfo();
                    Util.Map(element, obj);
                    list.Add(obj);
                }
                return list;
            }
        }

        public List<UserInfo> GetActiveUsers(int idHotel, string username, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                int count = context.SP_FDS_OBTAINACTIVEUSERSNAME(idHotel, "%" + username + "%").Count();
                var elementos = context.SP_FDS_OBTAINACTIVEUSERSNAME(idHotel, "%" + username + "%").Skip((pageIndex) * pageSize).Take(pageSize);
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                List<UserInfo> list = new List<UserInfo>();
                foreach (var element in elementos)
                {
                    UserInfo obj = new UserInfo();
                    Util.Map(element, obj);
                    list.Add(obj);
                }
                return list;
            }
        }

        public List<UserInfo> GetActiveUsers(int idHotel, string username, DateTime start, DateTime end, int billing, int room, int priority, int filterprofile, int? iot, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                if (start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                {
                    if (iot.HasValue)
                    {

                        int count = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, null, null, iot.Value).Count();
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, null, null, iot.Value).Skip((pageIndex) * pageSize).Take(pageSize);
                        pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                        List<UserInfo> list = new List<UserInfo>();
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                        return list;
                    }
                    else
                    {
                        int count = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, null, null, null).Count();
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, null, null, null).Skip((pageIndex) * pageSize).Take(pageSize);
                        pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                        List<UserInfo> list = new List<UserInfo>();
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                        return list;
                    }
                }
                else if (!start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                {
                    if (iot.HasValue)
                    {
                        int count = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, start, null, iot.Value).Count();
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, start, null, iot.Value).Skip((pageIndex) * pageSize).Take(pageSize);
                        pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                        List<UserInfo> list = new List<UserInfo>();
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                        return list;
                    }
                    else
                    {
                        int count = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, start, null, null).Count();
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, start, null, null).Skip((pageIndex) * pageSize).Take(pageSize);
                        pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                        List<UserInfo> list = new List<UserInfo>();
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                        return list;
                    }
                }
                else if (start.Equals(DateTime.MaxValue) && !end.Equals(DateTime.MaxValue))
                {
                    if (iot.HasValue)
                    {
                        int count = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, null, end, iot.Value).Count();
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, null, end, iot.Value).Skip((pageIndex) * pageSize).Take(pageSize);
                        pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                        List<UserInfo> list = new List<UserInfo>();
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                        return list;
                    }
                }
                else
                {
                    int count = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, start, end, null).Count();
                    var elementos = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, start, end, null).Skip((pageIndex) * pageSize).Take(pageSize);
                    pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                    List<UserInfo> list = new List<UserInfo>();
                    foreach (var element in elementos)
                    {
                        UserInfo obj = new UserInfo();
                        Util.Map(element, obj);
                        list.Add(obj);
                    }
                    return list;
                }

                return null;
            }
        }

        public List<UserInfo> GetActiveUsers(int idHotel, string username, DateTime start, DateTime end, int billing, int room, int priority, int filterprofile, int? iot, int idfdsuser, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                if (start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                {
                    if (iot.HasValue)
                    {

                        int count = context.SP_FDS_OBTAINACTIVEUSERSFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, null, null, iot.Value, idfdsuser).Count();
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, null, null, iot.Value, idfdsuser).Skip((pageIndex) * pageSize).Take(pageSize);
                        pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                        List<UserInfo> list = new List<UserInfo>();
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                        return list;
                    }
                    else
                    {
                        int count = context.SP_FDS_OBTAINACTIVEUSERSFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, null, null, null, idfdsuser).Count();
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, null, null, null, idfdsuser).Skip((pageIndex) * pageSize).Take(pageSize);
                        pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                        List<UserInfo> list = new List<UserInfo>();
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                        return list;
                    }
                }
                else if (!start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                {
                    if (iot.HasValue)
                    {
                        int count = context.SP_FDS_OBTAINACTIVEUSERSFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, start, null, iot.Value, idfdsuser).Count();
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, start, null, iot.Value, idfdsuser).Skip((pageIndex) * pageSize).Take(pageSize);
                        pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                        List<UserInfo> list = new List<UserInfo>();
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                        return list;
                    }
                    else
                    {
                        int count = context.SP_FDS_OBTAINACTIVEUSERSFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, start, null, null, idfdsuser).Count();
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, start, null, null, idfdsuser).Skip((pageIndex) * pageSize).Take(pageSize);
                        pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                        List<UserInfo> list = new List<UserInfo>();
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                        return list;
                    }
                }
                else if (start.Equals(DateTime.MaxValue) && !end.Equals(DateTime.MaxValue))
                {
                    if (iot.HasValue)
                    {
                        int count = context.SP_FDS_OBTAINACTIVEUSERSFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, null, end, iot.Value, idfdsuser).Count();
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, null, end, iot.Value, idfdsuser).Skip((pageIndex) * pageSize).Take(pageSize);
                        pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                        List<UserInfo> list = new List<UserInfo>();
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                        return list;
                    }
                }
                else
                {
                    int count = context.SP_FDS_OBTAINACTIVEUSERSFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, start, end, null, idfdsuser).Count();
                    var elementos = context.SP_FDS_OBTAINACTIVEUSERSFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, start, end, null, idfdsuser).Skip((pageIndex) * pageSize).Take(pageSize);
                    pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                    List<UserInfo> list = new List<UserInfo>();
                    foreach (var element in elementos)
                    {
                        UserInfo obj = new UserInfo();
                        Util.Map(element, obj);
                        list.Add(obj);
                    }
                    return list;
                }

                return null;
            }
        }

        public List<UserInfo> GetActiveUsers(int idHotel, List<FDSUsers> users, string username, DateTime start, DateTime end, int billing, int room, int priority, int filterprofile, int? iot, int idfdsuser, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                List<UserInfo> list = new List<UserInfo>();

                if (start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                {
                    if (iot.HasValue)
                    {
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, null, null, iot.Value, idfdsuser);
                        foreach (var element in elementos)
                        {
                            if (!idfdsuser.Equals(0))
                            {
                                UserInfo obj = new UserInfo();
                                Util.Map(element, obj);
                                list.Add(obj);
                            }
                            else
                            {
                                FDSUsers u = (from t in users where t.IdUser.Equals(element.IdFDSUser) select t).FirstOrDefault();
                                if ((u != null) || element.IdFDSUser.Equals(0))
                                {
                                    UserInfo obj = new UserInfo();
                                    Util.Map(element, obj);
                                    list.Add(obj);
                                }
                            }
                        }
                    }
                    else
                    {
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, null, null, null, idfdsuser);
                        foreach (var element in elementos)
                        {
                            if (!idfdsuser.Equals(0))
                            {
                                UserInfo obj = new UserInfo();
                                Util.Map(element, obj);
                                list.Add(obj);
                            }
                            else
                            {
                                FDSUsers u = (from t in users where t.IdUser.Equals(element.IdFDSUser) select t).FirstOrDefault();
                                if ((u != null) || element.IdFDSUser.Equals(0))
                                {
                                    UserInfo obj = new UserInfo();
                                    Util.Map(element, obj);
                                    list.Add(obj);
                                }
                            }
                        }
                    }
                }
                else if (!start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                {
                    if (iot.HasValue)
                    {
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, start, null, iot.Value, idfdsuser);
                        foreach (var element in elementos)
                        {
                            if (!idfdsuser.Equals(0))
                            {
                                UserInfo obj = new UserInfo();
                                Util.Map(element, obj);
                                list.Add(obj);
                            }
                            else
                            {
                                FDSUsers u = (from t in users where t.IdUser.Equals(element.IdFDSUser) select t).FirstOrDefault();
                                if ((u != null) || element.IdFDSUser.Equals(0))
                                {
                                    UserInfo obj = new UserInfo();
                                    Util.Map(element, obj);
                                    list.Add(obj);
                                }
                            }
                        }
                    }
                    else
                    {
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, start, null, null, idfdsuser);
                        foreach (var element in elementos)
                        {
                            if (!idfdsuser.Equals(0))
                            {
                                UserInfo obj = new UserInfo();
                                Util.Map(element, obj);
                                list.Add(obj);
                            }
                            else
                            {
                                FDSUsers u = (from t in users where t.IdUser.Equals(element.IdFDSUser) select t).FirstOrDefault();
                                if ((u != null) || element.IdFDSUser.Equals(0))
                                {
                                    UserInfo obj = new UserInfo();
                                    Util.Map(element, obj);
                                    list.Add(obj);
                                }
                            }
                        }
                    }
                }
                else if (start.Equals(DateTime.MaxValue) && !end.Equals(DateTime.MaxValue))
                {
                    if (iot.HasValue)
                    {
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, null, end, iot.Value, idfdsuser);
                        foreach (var element in elementos)
                        {
                            if (!idfdsuser.Equals(0))
                            {
                                UserInfo obj = new UserInfo();
                                Util.Map(element, obj);
                                list.Add(obj);
                            }
                            else
                            {
                                FDSUsers u = (from t in users where t.IdUser.Equals(element.IdFDSUser) select t).FirstOrDefault();
                                if ((u != null) || element.IdFDSUser.Equals(0))
                                {
                                    UserInfo obj = new UserInfo();
                                    Util.Map(element, obj);
                                    list.Add(obj);
                                }
                            }
                        }
                    }
                }
                else
                {
                    var elementos = context.SP_FDS_OBTAINACTIVEUSERSFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, start, end, null, idfdsuser);
                    foreach (var element in elementos)
                    {
                        if (!idfdsuser.Equals(0))
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                        else
                        {
                            FDSUsers u = (from t in users where t.IdUser.Equals(element.IdFDSUser) select t).FirstOrDefault();
                            if ((u != null) || element.IdFDSUser.Equals(0))
                            {
                                UserInfo obj = new UserInfo();
                                Util.Map(element, obj);
                                list.Add(obj);
                            }
                        }
                    }
                }

                int count = list.Count();
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                return list.Skip((pageIndex) * pageSize).Take(pageSize).ToList();
            }
        }


        public List<UserInfo> GetActiveUsers(string listadohoteles, string username, DateTime start, DateTime end, int billing, int room, int priority, int filterprofile, int? iot, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                List<UserInfo> list = new List<UserInfo>();
                string[] ids = listadohoteles.Split(',');
                int count = 0;
                foreach (string id in ids)
                {
                    int idHotel = Int32.Parse(id);
                    if (start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                    {
                        if (iot.HasValue)
                        {
                            count += context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, null, null, iot.Value).Count();
                            var elementos = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, null, null, iot.Value);//.Skip((pageIndex) * pageSize).Take(pageSize);

                            foreach (var element in elementos)
                            {
                                UserInfo obj = new UserInfo();
                                Util.Map(element, obj);
                                list.Add(obj);
                            }
                        }
                        else
                        {
                            count += context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, null, null, null).Count();
                            var elementos = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, null, null, null);//.Skip((pageIndex) * pageSize).Take(pageSize);

                            foreach (var element in elementos)
                            {
                                UserInfo obj = new UserInfo();
                                Util.Map(element, obj);
                                list.Add(obj);
                            }
                        }
                    }
                    else if (!start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                    {
                        if (iot.HasValue)
                        {
                            count += context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, start, null, iot.Value).Count();
                            var elementos = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, start, null, iot.Value);//.Skip((pageIndex) * pageSize).Take(pageSize);

                            foreach (var element in elementos)
                            {
                                UserInfo obj = new UserInfo();
                                Util.Map(element, obj);
                                list.Add(obj);
                            }
                        }
                        else
                        {
                            count += context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, start, null, null).Count();
                            var elementos = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, start, null, null);//.Skip((pageIndex) * pageSize).Take(pageSize);

                            foreach (var element in elementos)
                            {
                                UserInfo obj = new UserInfo();
                                Util.Map(element, obj);
                                list.Add(obj);
                            }
                        }
                    }
                    else if (start.Equals(DateTime.MaxValue) && !end.Equals(DateTime.MaxValue))
                    {
                        if (iot.HasValue)
                        {
                            count += context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, null, end, iot.Value).Count();
                            var elementos = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, null, end, iot.Value);//.Skip((pageIndex) * pageSize).Take(pageSize);

                            foreach (var element in elementos)
                            {
                                UserInfo obj = new UserInfo();
                                Util.Map(element, obj);
                                list.Add(obj);
                            }
                        }
                    }
                    else
                    {
                        count += context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, start, end, null).Count();
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERS(idHotel, "%" + username + "%", room, billing, priority, filterprofile, start, end, null);//.Skip((pageIndex) * pageSize).Take(pageSize);

                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                    }
                }

                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                list = list.OrderBy(o => o.ValidSince).ToList();
                list = list.Skip((pageIndex) * pageSize).Take(pageSize).ToList();

                return list;
            }
        }

        public List<UserInfo> GetVouchersUsers(int idHotel, string username, DateTime start, DateTime end, int billing, bool? enabled, bool? billable, int idfdsuser, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                List<UserInfo> list = new List<UserInfo>();
                if (enabled.HasValue && billable.HasValue)
                {
                    var elementos = context.SP_FDS_OBTAINACTIVEVOUCHERSUSERSNAMEALLFILTERS(idHotel, "%" + username + "%", billing, start, end, billable.Value, enabled.Value, idfdsuser);
                    foreach (var element in elementos)
                    {
                        UserInfo obj = new UserInfo();
                        Util.Map(element, obj);
                        list.Add(obj);
                    }
                }
                else if (!enabled.HasValue && billable.HasValue) 
                {
                    var elementos = context.SP_FDS_OBTAINACTIVEVOUCHERSUSERSNAMEALLFILTERS(idHotel, "%" + username + "%", billing, start, end, billable.Value, null, idfdsuser);
                    foreach (var element in elementos)
                    {
                        UserInfo obj = new UserInfo();
                        Util.Map(element, obj);
                        list.Add(obj);
                    }

                }
                else if (enabled.HasValue && !billable.HasValue)
                {
                    var elementos = context.SP_FDS_OBTAINACTIVEVOUCHERSUSERSNAMEALLFILTERS(idHotel, "%" + username + "%", billing, start, end, null, enabled.Value, idfdsuser);
                    foreach (var element in elementos)
                    {
                        UserInfo obj = new UserInfo();
                        Util.Map(element, obj);
                        list.Add(obj);
                    }

                }
                else
                {
                    var elementos = context.SP_FDS_OBTAINACTIVEVOUCHERSUSERSNAMEALLFILTERS(idHotel, "%" + username + "%", billing, start, end, null, null, idfdsuser);
                    foreach (var element in elementos)
                    {
                        UserInfo obj = new UserInfo();
                        Util.Map(element, obj);
                        list.Add(obj);
                    }
                }

                pageNumber = ((list.Count() % pageSize) == 0 ? list.Count() / pageSize : (list.Count() / pageSize) + 1);
                return list.Skip((pageIndex) * pageSize).Take(pageSize).ToList();
            }
        }

        public List<UserInfo> GetVouchersUsers(string listadoHoteles, string username, DateTime start, DateTime end, int billing, bool? enabled, bool? billable, int idfdsuser, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                List<UserInfo> list = new List<UserInfo>();
                string[] ids = listadoHoteles.Split(',');
                foreach (string id in ids)
                {
                    int idHotel = int.Parse(id);
                    if (enabled.HasValue && billable.HasValue)
                    {
                        var elementos = context.SP_FDS_OBTAINACTIVEVOUCHERSUSERSNAMEALLFILTERS(idHotel, "%" + username + "%", billing, start, end, billable.Value, enabled.Value, idfdsuser);
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                    }
                    else if (!enabled.HasValue && billable.HasValue)
                    {
                        var elementos = context.SP_FDS_OBTAINACTIVEVOUCHERSUSERSNAMEALLFILTERS(idHotel, "%" + username + "%", billing, start, end, billable.Value, null, idfdsuser);
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }

                    }
                    else if (enabled.HasValue && !billable.HasValue)
                    {
                        var elementos = context.SP_FDS_OBTAINACTIVEVOUCHERSUSERSNAMEALLFILTERS(idHotel, "%" + username + "%", billing, start, end, null, enabled.Value, idfdsuser);
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }

                    }
                    else
                    {
                        var elementos = context.SP_FDS_OBTAINACTIVEVOUCHERSUSERSNAMEALLFILTERS(idHotel, "%" + username + "%", billing, start, end, null, null, idfdsuser);
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                    }

                }
                pageNumber = ((list.Count() % pageSize) == 0 ? list.Count() / pageSize : (list.Count() / pageSize) + 1);
                return list.Skip((pageIndex) * pageSize).Take(pageSize).ToList();
            }
        }

        public List<UserInfo> GetVouchersUsersLocation(int idlocation, string username, DateTime start, DateTime end, int billing, bool? enabled, bool? billable, int idfdsuser, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                List<UserInfo> list = new List<UserInfo>();
                if (enabled.HasValue && billable.HasValue)
                {
                    var elementos = context.SP_FDS_OBTAINACTIVEVOUCHERSLOCATIONUSERSNAMEALLFILTERS(idlocation, "%" + username + "%", billing, start, end, billable.Value, enabled.Value, idfdsuser);
                    foreach (var element in elementos)
                    {
                        UserInfo obj = new UserInfo();
                        Util.Map(element, obj);
                        list.Add(obj);
                    }
                }
                else if (!enabled.HasValue && billable.HasValue)
                {
                    var elementos = context.SP_FDS_OBTAINACTIVEVOUCHERSLOCATIONUSERSNAMEALLFILTERS(idlocation, "%" + username + "%", billing, start, end, billable.Value, null, idfdsuser);
                    foreach (var element in elementos)
                    {
                        UserInfo obj = new UserInfo();
                        Util.Map(element, obj);
                        list.Add(obj);
                    }

                }
                else if (enabled.HasValue && !billable.HasValue)
                {
                    var elementos = context.SP_FDS_OBTAINACTIVEVOUCHERSLOCATIONUSERSNAMEALLFILTERS(idlocation, "%" + username + "%", billing, start, end, null, enabled.Value, idfdsuser);
                    foreach (var element in elementos)
                    {
                        UserInfo obj = new UserInfo();
                        Util.Map(element, obj);
                        list.Add(obj);
                    }

                }
                else
                {
                    var elementos = context.SP_FDS_OBTAINACTIVEVOUCHERSLOCATIONUSERSNAMEALLFILTERS(idlocation, "%" + username + "%", billing, start, end, null, null, idfdsuser);
                    foreach (var element in elementos)
                    {
                        UserInfo obj = new UserInfo();
                        Util.Map(element, obj);
                        list.Add(obj);
                    }
                }

                pageNumber = ((list.Count() % pageSize) == 0 ? list.Count() / pageSize : (list.Count() / pageSize) + 1);
                return list.Skip((pageIndex) * pageSize).Take(pageSize).ToList();
            }
        }

        public List<UserInfo> GetActiveUsersIoT(int idHotel, string username, DateTime start, DateTime end, int billing, int room, int priority, int filterprofile, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                if (start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                {
                    int count = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERSIOT(idHotel, "%" + username + "%", room, billing, priority, filterprofile, null, null).Count();
                    var elementos = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERSIOT(idHotel, "%" + username + "%", room, billing, priority, filterprofile, null, null).Skip((pageIndex) * pageSize).Take(pageSize);
                    pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                    List<UserInfo> list = new List<UserInfo>();
                    foreach (var element in elementos)
                    {
                        UserInfo obj = new UserInfo();
                        Util.Map(element, obj);
                        list.Add(obj);
                    }
                    return list;
                }
                else if (!start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                {
                    int count = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERSIOT(idHotel, "%" + username + "%", room, billing, priority, filterprofile, start, null).Count();
                    var elementos = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERSIOT(idHotel, "%" + username + "%", room, billing, priority, filterprofile, start, null).Skip((pageIndex) * pageSize).Take(pageSize);
                    pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                    List<UserInfo> list = new List<UserInfo>();
                    foreach (var element in elementos)
                    {
                        UserInfo obj = new UserInfo();
                        Util.Map(element, obj);
                        list.Add(obj);
                    }
                    return list;
                }
                else if (start.Equals(DateTime.MaxValue) && !end.Equals(DateTime.MaxValue))
                {
                    int count = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERSIOT(idHotel, "%" + username + "%", room, billing, priority, filterprofile, null, end).Count();
                    var elementos = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERSIOT(idHotel, "%" + username + "%", room, billing, priority, filterprofile, null, end).Skip((pageIndex) * pageSize).Take(pageSize);
                    pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                    List<UserInfo> list = new List<UserInfo>();
                    foreach (var element in elementos)
                    {
                        UserInfo obj = new UserInfo();
                        Util.Map(element, obj);
                        list.Add(obj);
                    }
                    return list;
                }
                else
                {
                    int count = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERSIOT(idHotel, "%" + username + "%", room, billing, priority, filterprofile, start, end).Count();
                    var elementos = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERSIOT(idHotel, "%" + username + "%", room, billing, priority, filterprofile, start, end).Skip((pageIndex) * pageSize).Take(pageSize);
                    pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                    List<UserInfo> list = new List<UserInfo>();
                    foreach (var element in elementos)
                    {
                        UserInfo obj = new UserInfo();
                        Util.Map(element, obj);
                        list.Add(obj);
                    }
                    return list;
                }

                return null;
            }
        }

        public List<UserInfo> GetActiveUsersLocation(int idlocation, string username, DateTime start, DateTime end, int billing, int room, int priority, int filterprofile, int? iot, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                if (start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                {
                    if (iot.HasValue)
                    {
                        int count = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERSLOCATION(idlocation, "%" + username + "%", room, billing, priority, filterprofile, null, null, iot.Value).Count();
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERSLOCATION(idlocation, "%" + username + "%", room, billing, priority, filterprofile, null, null, iot.Value).Skip((pageIndex) * pageSize).Take(pageSize);
                        pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                        List<UserInfo> list = new List<UserInfo>();
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                        return list;
                    }
                    else
                    {
                        int count = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERSLOCATION(idlocation, "%" + username + "%", room, billing, priority, filterprofile, null, null, null).Count();
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERSLOCATION(idlocation, "%" + username + "%", room, billing, priority, filterprofile, null, null, null).Skip((pageIndex) * pageSize).Take(pageSize);
                        pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                        List<UserInfo> list = new List<UserInfo>();
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                        return list;

                    }
                }
                else if (!start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                {
                    if (iot.HasValue)
                    {
                        int count = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERSLOCATION(idlocation, "%" + username + "%", room, billing, priority, filterprofile, start, null, iot.Value).Count();
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERSLOCATION(idlocation, "%" + username + "%", room, billing, priority, filterprofile, start, null, iot.Value).Skip((pageIndex) * pageSize).Take(pageSize);
                        pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                        List<UserInfo> list = new List<UserInfo>();
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                        return list;
                    }
                    else
                    {
                        int count = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERSLOCATION(idlocation, "%" + username + "%", room, billing, priority, filterprofile, start, null, null).Count();
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERSLOCATION(idlocation, "%" + username + "%", room, billing, priority, filterprofile, start, null, null).Skip((pageIndex) * pageSize).Take(pageSize);
                        pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                        List<UserInfo> list = new List<UserInfo>();
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                        return list;

                    }
                }
                else if (start.Equals(DateTime.MaxValue) && !end.Equals(DateTime.MaxValue))
                {
                    if (iot.HasValue)
                    {
                        int count = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERSLOCATION(idlocation, "%" + username + "%", room, billing, priority, filterprofile, null, end, iot.Value).Count();
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERSLOCATION(idlocation, "%" + username + "%", room, billing, priority, filterprofile, null, end, iot.Value).Skip((pageIndex) * pageSize).Take(pageSize);
                        pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                        List<UserInfo> list = new List<UserInfo>();
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                        return list;
                    }
                    else
                    {
                        int count = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERSLOCATION(idlocation, "%" + username + "%", room, billing, priority, filterprofile, null, end, null).Count();
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERSLOCATION(idlocation, "%" + username + "%", room, billing, priority, filterprofile, null, end, null).Skip((pageIndex) * pageSize).Take(pageSize);
                        pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                        List<UserInfo> list = new List<UserInfo>();
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                        return list;
                    }
                }
                else
                {
                    if (iot.HasValue)
                    {
                        int count = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERSLOCATION(idlocation, "%" + username + "%", room, billing, priority, filterprofile, start, end, iot.Value).Count();
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERSLOCATION(idlocation, "%" + username + "%", room, billing, priority, filterprofile, start, end, iot.Value).Skip((pageIndex) * pageSize).Take(pageSize);
                        pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                        List<UserInfo> list = new List<UserInfo>();
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                        return list;
                    }
                    else
                    {
                        int count = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERSLOCATION(idlocation, "%" + username + "%", room, billing, priority, filterprofile, start, end, null).Count();
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERSLOCATION(idlocation, "%" + username + "%", room, billing, priority, filterprofile, start, end, null).Skip((pageIndex) * pageSize).Take(pageSize);
                        pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                        List<UserInfo> list = new List<UserInfo>();
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                        return list;

                    }
                }

                return null;
            }
        }

        public List<UserInfo> GetActiveUsersLocation(int idlocation, string username, DateTime start, DateTime end, int billing, int room, int priority, int filterprofile, int? iot, int idfdsuser, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                if (start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                {
                    if (iot.HasValue)
                    {
                        int count = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, null, null, iot.Value, idfdsuser).Count();
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, null, null, iot.Value, idfdsuser).Skip((pageIndex) * pageSize).Take(pageSize);
                        pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                        List<UserInfo> list = new List<UserInfo>();
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                        return list;
                    }
                    else
                    {
                        int count = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, null, null, null, idfdsuser).Count();
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, null, null, null, idfdsuser).Skip((pageIndex) * pageSize).Take(pageSize);
                        pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                        List<UserInfo> list = new List<UserInfo>();
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                        return list;

                    }
                }
                else if (!start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                {
                    if (iot.HasValue)
                    {
                        int count = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, start, null, iot.Value, idfdsuser).Count();
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, start, null, iot.Value, idfdsuser).Skip((pageIndex) * pageSize).Take(pageSize);
                        pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                        List<UserInfo> list = new List<UserInfo>();
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                        return list;
                    }
                    else
                    {
                        int count = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, start, null, null, idfdsuser).Count();
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, start, null, null, idfdsuser).Skip((pageIndex) * pageSize).Take(pageSize);
                        pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                        List<UserInfo> list = new List<UserInfo>();
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                        return list;

                    }
                }
                else if (start.Equals(DateTime.MaxValue) && !end.Equals(DateTime.MaxValue))
                {
                    if (iot.HasValue)
                    {
                        int count = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, null, end, iot.Value, idfdsuser).Count();
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, null, end, iot.Value, idfdsuser).Skip((pageIndex) * pageSize).Take(pageSize);
                        pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                        List<UserInfo> list = new List<UserInfo>();
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                        return list;
                    }
                    else
                    {
                        int count = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, null, end, null, idfdsuser).Count();
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, null, end, null, idfdsuser).Skip((pageIndex) * pageSize).Take(pageSize);
                        pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                        List<UserInfo> list = new List<UserInfo>();
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                        return list;
                    }
                }
                else
                {
                    if (iot.HasValue)
                    {
                        int count = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, start, end, iot.Value, idfdsuser).Count();
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, start, end, iot.Value, idfdsuser).Skip((pageIndex) * pageSize).Take(pageSize);
                        pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                        List<UserInfo> list = new List<UserInfo>();
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                        return list;
                    }
                    else
                    {
                        int count = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, start, end, null, idfdsuser).Count();
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, start, end, null, idfdsuser).Skip((pageIndex) * pageSize).Take(pageSize);
                        pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                        List<UserInfo> list = new List<UserInfo>();
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                        return list;

                    }
                }

                return null;
            }
        }

        public List<UserInfo> GetActiveUsersLocation(int idlocation, List<FDSUsers> users, string username, DateTime start, DateTime end, int billing, int room, int priority, int filterprofile, int? iot, int idfdsuser, int pageIndex, int pageSize, ref int pageNumber)
        {
            List<UserInfo> list = new List<UserInfo>();

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                if (start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                {
                    if (iot.HasValue)
                    {
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, null, null, iot.Value, idfdsuser);

                        foreach (var element in elementos)
                        {
                            if (!idfdsuser.Equals(0))
                            {
                                UserInfo obj = new UserInfo();
                                Util.Map(element, obj);
                                list.Add(obj);
                            }
                            else
                            {
                                FDSUsers u = (from t in users where t.IdUser.Equals(element.IdFDSUser) select t).FirstOrDefault();
                                if (u != null)
                                {
                                    UserInfo obj = new UserInfo();
                                    Util.Map(element, obj);
                                    list.Add(obj);

                                }
                            }
                        }
                    }
                    else
                    {
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, null, null, null, idfdsuser);

                        foreach (var element in elementos)
                        {
                            if (!idfdsuser.Equals(0))
                            {
                                UserInfo obj = new UserInfo();
                                Util.Map(element, obj);
                                list.Add(obj);
                            }
                            else
                            {
                                FDSUsers u = (from t in users where t.IdUser.Equals(element.IdFDSUser) select t).FirstOrDefault();
                                if (u != null)
                                {
                                    UserInfo obj = new UserInfo();
                                    Util.Map(element, obj);
                                    list.Add(obj);

                                }
                            }
                        }

                    }
                }
                else if (!start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                {
                    if (iot.HasValue)
                    {
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, start, null, iot.Value, idfdsuser);

                        foreach (var element in elementos)
                        {
                            if (!idfdsuser.Equals(0))
                            {
                                UserInfo obj = new UserInfo();
                                Util.Map(element, obj);
                                list.Add(obj);
                            }
                            else
                            {
                                FDSUsers u = (from t in users where t.IdUser.Equals(element.IdFDSUser) select t).FirstOrDefault();
                                if (u != null)
                                {
                                    UserInfo obj = new UserInfo();
                                    Util.Map(element, obj);
                                    list.Add(obj);

                                }
                            }
                        }
                    }
                    else
                    {
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, start, null, null, idfdsuser);

                        foreach (var element in elementos)
                        {
                            if (!idfdsuser.Equals(0))
                            {
                                UserInfo obj = new UserInfo();
                                Util.Map(element, obj);
                                list.Add(obj);
                            }
                            else
                            {
                                FDSUsers u = (from t in users where t.IdUser.Equals(element.IdFDSUser) select t).FirstOrDefault();
                                if (u != null)
                                {
                                    UserInfo obj = new UserInfo();
                                    Util.Map(element, obj);
                                    list.Add(obj);

                                }
                            }
                        }
                    }
                }
                else if (start.Equals(DateTime.MaxValue) && !end.Equals(DateTime.MaxValue))
                {
                    if (iot.HasValue)
                    {
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, null, end, iot.Value, idfdsuser);
                        foreach (var element in elementos)
                        {
                            if (!idfdsuser.Equals(0))
                            {
                                UserInfo obj = new UserInfo();
                                Util.Map(element, obj);
                                list.Add(obj);
                            }
                            else
                            {
                                FDSUsers u = (from t in users where t.IdUser.Equals(element.IdFDSUser) select t).FirstOrDefault();
                                if (u != null)
                                {
                                    UserInfo obj = new UserInfo();
                                    Util.Map(element, obj);
                                    list.Add(obj);

                                }
                            }
                        }
                    }
                    else
                    {
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, null, end, null, idfdsuser);
                        foreach (var element in elementos)
                        {
                            if (!idfdsuser.Equals(0))
                            {
                                UserInfo obj = new UserInfo();
                                Util.Map(element, obj);
                                list.Add(obj);
                            }
                            else
                            {
                                FDSUsers u = (from t in users where t.IdUser.Equals(element.IdFDSUser) select t).FirstOrDefault();
                                if (u != null)
                                {
                                    UserInfo obj = new UserInfo();
                                    Util.Map(element, obj);
                                    list.Add(obj);

                                }
                            }
                        }
                    }
                }
                else
                {
                    if (iot.HasValue)
                    {
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, start, end, iot.Value, idfdsuser);
                        foreach (var element in elementos)
                        {
                            if (!idfdsuser.Equals(0))
                            {
                                UserInfo obj = new UserInfo();
                                Util.Map(element, obj);
                                list.Add(obj);
                            }
                            else
                            {
                                FDSUsers u = (from t in users where t.IdUser.Equals(element.IdFDSUser) select t).FirstOrDefault();
                                if (u != null)
                                {
                                    UserInfo obj = new UserInfo();
                                    Util.Map(element, obj);
                                    list.Add(obj);

                                }
                            }
                        }
                    }
                    else
                    {
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, start, end, null, idfdsuser);
                        foreach (var element in elementos)
                        {
                            if (!idfdsuser.Equals(0))
                            {
                                UserInfo obj = new UserInfo();
                                Util.Map(element, obj);
                                list.Add(obj);
                            }
                            else
                            {
                                FDSUsers u = (from t in users where t.IdUser.Equals(element.IdFDSUser) select t).FirstOrDefault();
                                if (u != null)
                                {
                                    UserInfo obj = new UserInfo();
                                    Util.Map(element, obj);
                                    list.Add(obj);

                                }
                            }
                        }
                    }
                }
                int count = list.Count();
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                return list.Skip((pageIndex) * pageSize).Take(pageSize).ToList();
            }
        }

        public List<UserInfo> GetUsersLocation(int idlocation, bool enabled, string username, DateTime start, DateTime end, int billing, int room, int priority, int filterprofile, int? iot, int idfdsuser, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                if (start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                {
                    if (iot.HasValue)
                    {
                        int count = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, null, null, iot.Value, idfdsuser).Count();
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, null, null, iot.Value, idfdsuser).Skip((pageIndex) * pageSize).Take(pageSize);
                        pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                        List<UserInfo> list = new List<UserInfo>();
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                        return list;
                    }
                    else
                    {
                        int count = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, null, null, null, idfdsuser).Count();
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, null, null, null, idfdsuser).Skip((pageIndex) * pageSize).Take(pageSize);
                        pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                        List<UserInfo> list = new List<UserInfo>();
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                        return list;

                    }
                }
                else if (!start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                {
                    if (iot.HasValue)
                    {
                        int count = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, start, null, iot.Value, idfdsuser).Count();
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, start, null, iot.Value, idfdsuser).Skip((pageIndex) * pageSize).Take(pageSize);
                        pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                        List<UserInfo> list = new List<UserInfo>();
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                        return list;
                    }
                    else
                    {
                        int count = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, start, null, null, idfdsuser).Count();
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, start, null, null, idfdsuser).Skip((pageIndex) * pageSize).Take(pageSize);
                        pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                        List<UserInfo> list = new List<UserInfo>();
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                        return list;

                    }
                }
                else if (start.Equals(DateTime.MaxValue) && !end.Equals(DateTime.MaxValue))
                {
                    if (iot.HasValue)
                    {
                        int count = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, null, end, iot.Value, idfdsuser).Count();
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, null, end, iot.Value, idfdsuser).Skip((pageIndex) * pageSize).Take(pageSize);
                        pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                        List<UserInfo> list = new List<UserInfo>();
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                        return list;
                    }
                    else
                    {
                        int count = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, null, end, null, idfdsuser).Count();
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, null, end, null, idfdsuser).Skip((pageIndex) * pageSize).Take(pageSize);
                        pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                        List<UserInfo> list = new List<UserInfo>();
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                        return list;
                    }
                }
                else
                {
                    if (iot.HasValue)
                    {
                        int count = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, start, end, iot.Value, idfdsuser).Count();
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, start, end, iot.Value, idfdsuser).Skip((pageIndex) * pageSize).Take(pageSize);
                        pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                        List<UserInfo> list = new List<UserInfo>();
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                        return list;
                    }
                    else
                    {
                        int count = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, start, end, null, idfdsuser).Count();
                        var elementos = context.SP_FDS_OBTAINACTIVEUSERSLOCATIONFILTERS(idlocation, "%" + username + "%", room, billing, priority, filterprofile, start, end, null, idfdsuser).Skip((pageIndex) * pageSize).Take(pageSize);
                        pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                        List<UserInfo> list = new List<UserInfo>();
                        foreach (var element in elementos)
                        {
                            UserInfo obj = new UserInfo();
                            Util.Map(element, obj);
                            list.Add(obj);
                        }
                        return list;

                    }
                }

                return null;
            }
        }

        public List<UserInfo> GetActiveUsersLocationIoT(int idlocation, string username, DateTime start, DateTime end, int billing, int room, int prioritym, int filterprofile, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                if (start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                {
                    int count = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERSLOCATIONIOT(idlocation, "%" + username + "%", room, billing, prioritym, filterprofile, null, null).Count();
                    var elementos = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERSLOCATIONIOT(idlocation, "%" + username + "%", room, billing, prioritym, filterprofile, null, null).Skip((pageIndex) * pageSize).Take(pageSize);
                    pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                    List<UserInfo> list = new List<UserInfo>();
                    foreach (var element in elementos)
                    {
                        UserInfo obj = new UserInfo();
                        Util.Map(element, obj);
                        list.Add(obj);
                    }
                    return list;
                }
                else if (!start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                {
                    int count = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERSLOCATIONIOT(idlocation, "%" + username + "%", room, billing, prioritym, filterprofile, start, null).Count();
                    var elementos = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERSLOCATIONIOT(idlocation, "%" + username + "%", room, billing, prioritym, filterprofile, start, null).Skip((pageIndex) * pageSize).Take(pageSize);
                    pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                    List<UserInfo> list = new List<UserInfo>();
                    foreach (var element in elementos)
                    {
                        UserInfo obj = new UserInfo();
                        Util.Map(element, obj);
                        list.Add(obj);
                    }
                    return list;
                }
                else if (start.Equals(DateTime.MaxValue) && !end.Equals(DateTime.MaxValue))
                {
                    int count = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERSLOCATIONIOT(idlocation, "%" + username + "%", room, billing, prioritym, filterprofile, null, end).Count();
                    var elementos = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERSLOCATIONIOT(idlocation, "%" + username + "%", room, billing, prioritym, filterprofile, null, end).Skip((pageIndex) * pageSize).Take(pageSize);
                    pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                    List<UserInfo> list = new List<UserInfo>();
                    foreach (var element in elementos)
                    {
                        UserInfo obj = new UserInfo();
                        Util.Map(element, obj);
                        list.Add(obj);
                    }
                    return list;
                }
                else
                {
                    int count = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERSLOCATIONIOT(idlocation, "%" + username + "%", room, billing, prioritym, filterprofile, start, end).Count();
                    var elementos = context.SP_FDS_OBTAINACTIVEUSERSNAMEALLFILTERSLOCATIONIOT(idlocation, "%" + username + "%", room, billing, prioritym, filterprofile, start, end).Skip((pageIndex) * pageSize).Take(pageSize);
                    pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                    List<UserInfo> list = new List<UserInfo>();
                    foreach (var element in elementos)
                    {
                        UserInfo obj = new UserInfo();
                        Util.Map(element, obj);
                        list.Add(obj);
                    }
                    return list;
                }

                return null;
            }
        }
        
        public List<UserInfo> GetActiveUsers(int idHotel, int idBillingType, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                int count = context.SP_FDS_OBTAINACTIVEUSERSTYPE(idHotel, idBillingType).Count();
                var elementos = context.SP_FDS_OBTAINACTIVEUSERSTYPE(idHotel, idBillingType).Skip((pageIndex) * pageSize).Take(pageSize);
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                List<UserInfo> list = new List<UserInfo>();
                foreach (var element in elementos)
                {
                    UserInfo obj = new UserInfo();
                    Util.Map(element, obj);
                    list.Add(obj);
                }
                return list;
            }
        }

        public List<UserInfo> GetActiveUsers(int idHotel, string username, int idBillingType, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                int count = context.SP_FDS_OBTAINACTIVEUSERSNAMEANDTYPE(idHotel, "%" + username + "%", idBillingType).Count();
                var elementos = context.SP_FDS_OBTAINACTIVEUSERSNAMEANDTYPE(idHotel, "%"+ username + "%" , idBillingType).Skip((pageIndex) * pageSize).Take(pageSize);
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                List<UserInfo> list = new List<UserInfo>();
                foreach (var element in elementos)
                {
                    UserInfo obj = new UserInfo();
                    Util.Map(element, obj);
                    list.Add(obj);
                }
                return list;
            }
        }

        public List<Users> GetDisabledUsers(int idHotel, DateTime date, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                int count = context.SP_FDS_OBTAINDISABLEDUSERS(idHotel, date).Count();
                var elementos = context.SP_FDS_OBTAINDISABLEDUSERS(idHotel, date).Skip((pageIndex) * pageSize).Take(pageSize);
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                List<Users> list = new List<Users>();
                foreach (var element in elementos)
                {
                    Users obj = new Users();
                    Util.Map(element, obj);
                    list.Add(obj);
                }
                return list;
            }
        }

        public List<Users> GetDisabledUsers(int idHotel, string username, DateTime start, DateTime end, int billing, int room, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                if (start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                {
                    int count = context.SP_FDS_OBTAINDISABLEDUSERSALLFILTER(idHotel, "%" + username + "%", room, billing, null, null).Count();
                    var elementos = context.SP_FDS_OBTAINDISABLEDUSERSALLFILTER(idHotel, "%" + username + "%", room, billing, null, null).Skip((pageIndex) * pageSize).Take(pageSize);
                    pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                    List<Users> list = new List<Users>();
                    foreach (var element in elementos)
                    {
                        Users obj = new Users();
                        Util.Map(element, obj);
                        obj.Name = element.UserName;
                        list.Add(obj);
                    }
                    return list;
                }
                else if (!start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                {
                    int count = context.SP_FDS_OBTAINDISABLEDUSERSALLFILTER(idHotel, "%" + username + "%", room, billing, start, null).Count();
                    var elementos = context.SP_FDS_OBTAINDISABLEDUSERSALLFILTER(idHotel, "%" + username + "%", room, billing, start, null).Skip((pageIndex) * pageSize).Take(pageSize);
                    pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                    List<Users> list = new List<Users>();
                    foreach (var element in elementos)
                    {
                        Users obj = new Users();
                        Util.Map(element, obj);
                        obj.Name = element.UserName;
                        list.Add(obj);
                    }
                    return list;
                }
                else if (start.Equals(DateTime.MaxValue) && !end.Equals(DateTime.MaxValue))
                {
                    int count = context.SP_FDS_OBTAINDISABLEDUSERSALLFILTER(idHotel, "%" + username + "%", room, billing, null, end).Count();
                    var elementos = context.SP_FDS_OBTAINDISABLEDUSERSALLFILTER(idHotel, "%" + username + "%", room, billing, null, end).Skip((pageIndex) * pageSize).Take(pageSize);
                    pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                    List<Users> list = new List<Users>();
                    foreach (var element in elementos)
                    {
                        Users obj = new Users();
                        Util.Map(element, obj);
                        obj.Name = element.UserName;
                        list.Add(obj);
                    }
                    return list;
                }
                else
                {
                    int count = context.SP_FDS_OBTAINDISABLEDUSERSALLFILTER(idHotel, "%" + username + "%", room, billing, start, end).Count();
                    var elementos = context.SP_FDS_OBTAINDISABLEDUSERSALLFILTER(idHotel, "%" + username + "%", room, billing, start, end).Skip((pageIndex) * pageSize).Take(pageSize);
                    pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                    List<Users> list = new List<Users>();
                    foreach (var element in elementos)
                    {
                        Users obj = new Users();
                        Util.Map(element, obj);
                        obj.Name = element.UserName;
                        list.Add(obj);
                    }
                    return list;
                }

                return null;
            }
        }

        public List<Users> GetDisabledUsers(int idHotel, string username, DateTime start, DateTime end, int billing, int room, int idfdsuser, int iot, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                if (start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                {
                    int count = context.SP_FDS_OBTAINDISABLEDUSERSFILTER(idHotel, "%" + username + "%", room, billing, null, null, idfdsuser, iot).Count();
                    var elementos = context.SP_FDS_OBTAINDISABLEDUSERSFILTER(idHotel, "%" + username + "%", room, billing, null, null, idfdsuser, iot).Skip((pageIndex) * pageSize).Take(pageSize);
                    pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                    List<Users> list = new List<Users>();
                    foreach (var element in elementos)
                    {
                        Users obj = new Users();
                        Util.Map(element, obj);
                        obj.Name = element.UserName;
                        list.Add(obj);
                    }
                    return list;
                }
                else if (!start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                {
                    int count = context.SP_FDS_OBTAINDISABLEDUSERSFILTER(idHotel, "%" + username + "%", room, billing, start, null, idfdsuser, iot).Count();
                    var elementos = context.SP_FDS_OBTAINDISABLEDUSERSFILTER(idHotel, "%" + username + "%", room, billing, start, null, idfdsuser, iot).Skip((pageIndex) * pageSize).Take(pageSize);
                    pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                    List<Users> list = new List<Users>();
                    foreach (var element in elementos)
                    {
                        Users obj = new Users();
                        Util.Map(element, obj);
                        obj.Name = element.UserName;
                        list.Add(obj);
                    }
                    return list;
                }
                else if (start.Equals(DateTime.MaxValue) && !end.Equals(DateTime.MaxValue))
                {
                    int count = context.SP_FDS_OBTAINDISABLEDUSERSFILTER(idHotel, "%" + username + "%", room, billing, null, end, idfdsuser, iot).Count();
                    var elementos = context.SP_FDS_OBTAINDISABLEDUSERSFILTER(idHotel, "%" + username + "%", room, billing, null, end, idfdsuser, iot).Skip((pageIndex) * pageSize).Take(pageSize);
                    pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                    List<Users> list = new List<Users>();
                    foreach (var element in elementos)
                    {
                        Users obj = new Users();
                        Util.Map(element, obj);
                        obj.Name = element.UserName;
                        list.Add(obj);
                    }
                    return list;
                }
                else
                {
                    int count = context.SP_FDS_OBTAINDISABLEDUSERSFILTER(idHotel, "%" + username + "%", room, billing, start, end, idfdsuser, iot).Count();
                    var elementos = context.SP_FDS_OBTAINDISABLEDUSERSFILTER(idHotel, "%" + username + "%", room, billing, start, end, idfdsuser, iot).Skip((pageIndex) * pageSize).Take(pageSize);
                    pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                    List<Users> list = new List<Users>();
                    foreach (var element in elementos)
                    {
                        Users obj = new Users();
                        Util.Map(element, obj);
                        obj.Name = element.UserName;
                        list.Add(obj);
                    }
                    return list;
                }

            }
        }

        public List<Users> GetDisabledUsers(int idHotel, List<FDSUsers> users, string username, DateTime start, DateTime end, int billing, int room, int idfdsuser, int iot, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                List<Users> list = new List<Users>();

                if (start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                {
                    var elementos = context.SP_FDS_OBTAINDISABLEDUSERSFILTER(idHotel, "%" + username + "%", room, billing, null, null, idfdsuser, iot).Skip((pageIndex) * pageSize).Take(pageSize);

                    foreach (var element in elementos)
                    {
                        if (!idfdsuser.Equals(0))
                        {
                            Users obj = new Users();
                            Util.Map(element, obj);
                            obj.Name = element.UserName;
                            list.Add(obj);
                        }
                        else
                        {
                            FDSUsers u = (from t in users where t.IdUser.Equals(element.IdFDSUser) select t).FirstOrDefault();
                            if ((u != null) || (element.IdFDSUser.Equals(0)))
                            {
                                Users obj = new Users();
                                Util.Map(element, obj);
                                obj.Name = element.UserName;
                                list.Add(obj);

                            }
                        }
                    }
                }
                else if (!start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                {
                    var elementos = context.SP_FDS_OBTAINDISABLEDUSERSFILTER(idHotel, "%" + username + "%", room, billing, start, null, idfdsuser, iot);
                    foreach (var element in elementos)
                    {
                        if (!idfdsuser.Equals(0))
                        {
                            Users obj = new Users();
                            Util.Map(element, obj);
                            obj.Name = element.UserName;
                            list.Add(obj);
                        }
                        else
                        {
                            FDSUsers u = (from t in users where t.IdUser.Equals(element.IdFDSUser) select t).FirstOrDefault();
                            if ((u != null) || (element.IdFDSUser.Equals(0)))
                            {
                                Users obj = new Users();
                                Util.Map(element, obj);
                                obj.Name = element.UserName;
                                list.Add(obj);

                            }
                        }
                    }
                }
                else if (start.Equals(DateTime.MaxValue) && !end.Equals(DateTime.MaxValue))
                {
                    var elementos = context.SP_FDS_OBTAINDISABLEDUSERSFILTER(idHotel, "%" + username + "%", room, billing, null, end, idfdsuser, iot);
                    foreach (var element in elementos)
                    {
                        if (!idfdsuser.Equals(0))
                        {
                            Users obj = new Users();
                            Util.Map(element, obj);
                            obj.Name = element.UserName;
                            list.Add(obj);
                        }
                        else
                        {
                            FDSUsers u = (from t in users where t.IdUser.Equals(element.IdFDSUser) select t).FirstOrDefault();
                            if ((u != null) || (element.IdFDSUser.Equals(0)))
                            {
                                Users obj = new Users();
                                Util.Map(element, obj);
                                obj.Name = element.UserName;
                                list.Add(obj);

                            }
                        }
                    }
                }
                else
                {
                    var elementos = context.SP_FDS_OBTAINDISABLEDUSERSFILTER(idHotel, "%" + username + "%", room, billing, start, end, idfdsuser, iot);
                    foreach (var element in elementos)
                    {
                        if (!idfdsuser.Equals(0))
                        {
                            Users obj = new Users();
                            Util.Map(element, obj);
                            obj.Name = element.UserName;
                            list.Add(obj);
                        }
                        else
                        {
                            FDSUsers u = (from t in users where t.IdUser.Equals(element.IdFDSUser) select t).FirstOrDefault();
                            if ((u != null) || (element.IdFDSUser.Equals(0)))
                            {
                                Users obj = new Users();
                                Util.Map(element, obj);
                                obj.Name = element.UserName;
                                list.Add(obj);

                            }
                        }
                    }
                }

                int count = list.Count();
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                return list.OrderBy(a => a.ValidSince).Skip((pageIndex) * pageSize).Take(pageSize).ToList();

            }
        }

        public List<Users> GetDisabledUsers(string listadoHoteles, string username, DateTime start, DateTime end, int billing, int room, int iot, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                string[] ids = listadoHoteles.Split(',');
                int count = 0;
                List<Users> list = new List<Users>();
                foreach (string id in ids)
                {
                    int idHotel = int.Parse(id);
                    if (start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                    {
                        count += context.SP_FDS_OBTAINDISABLEDUSERSFILTER(idHotel, "%" + username + "%", room, billing, null, null, 0, iot).Count();
                        var elementos = context.SP_FDS_OBTAINDISABLEDUSERSFILTER(idHotel, "%" + username + "%", room, billing, null, null, 0, iot);//.Skip((pageIndex) * pageSize).Take(pageSize);

                        foreach (var element in elementos)
                        {
                            Users obj = new Users();
                            Util.Map(element, obj);
                            obj.Name = element.UserName;
                            list.Add(obj);
                        }
                    }
                    else if (!start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                    {
                        count += context.SP_FDS_OBTAINDISABLEDUSERSFILTER(idHotel, "%" + username + "%", room, billing, start, null, 0, iot).Count();
                        var elementos = context.SP_FDS_OBTAINDISABLEDUSERSFILTER(idHotel, "%" + username + "%", room, billing, start, null, 0, iot);//.Skip((pageIndex) * pageSize).Take(pageSize);

                        foreach (var element in elementos)
                        {
                            Users obj = new Users();
                            Util.Map(element, obj);
                            obj.Name = element.UserName;
                            list.Add(obj);
                        }
                    }
                    else if (start.Equals(DateTime.MaxValue) && !end.Equals(DateTime.MaxValue))
                    {
                        count += context.SP_FDS_OBTAINDISABLEDUSERSFILTER(idHotel, "%" + username + "%", room, billing, null, end, 0, iot).Count();
                        var elementos = context.SP_FDS_OBTAINDISABLEDUSERSFILTER(idHotel, "%" + username + "%", room, billing, null, end, 0, iot);//.Skip((pageIndex) * pageSize).Take(pageSize);

                        foreach (var element in elementos)
                        {
                            Users obj = new Users();
                            Util.Map(element, obj);
                            obj.Name = element.UserName;
                            list.Add(obj);
                        }
                    }
                    else
                    {
                        count += context.SP_FDS_OBTAINDISABLEDUSERSFILTER(idHotel, "%" + username + "%", room, billing, start, end, 0, iot).Count();
                        var elementos = context.SP_FDS_OBTAINDISABLEDUSERSFILTER(idHotel, "%" + username + "%", room, billing, start, end, 0, iot);//.Skip((pageIndex) * pageSize).Take(pageSize);

                        foreach (var element in elementos)
                        {
                            Users obj = new Users();
                            Util.Map(element, obj);
                            obj.Name = element.UserName;
                            list.Add(obj);
                        }
                 
                    }
                }
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                return list.OrderBy(a=> a.ValidSince).Skip((pageIndex) * pageSize).Take(pageSize).ToList();
            }
        }

        public List<Users> GetDisabledUsersIoT(int idHotel, string username, DateTime start, DateTime end, int billing, int room, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                if (start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                {
                    int count = context.SP_FDS_OBTAINDISABLEDUSERSALLFILTERIOT(idHotel, "%" + username + "%", room, billing, null, null).Count();
                    var elementos = context.SP_FDS_OBTAINDISABLEDUSERSALLFILTERIOT(idHotel, "%" + username + "%", room, billing, null, null).Skip((pageIndex) * pageSize).Take(pageSize);
                    pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                    List<Users> list = new List<Users>();
                    foreach (var element in elementos)
                    {
                        Users obj = new Users();
                        Util.Map(element, obj);
                        obj.Name = element.UserName;
                        list.Add(obj);
                    }
                    return list;
                }
                else if (!start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                {
                    int count = context.SP_FDS_OBTAINDISABLEDUSERSALLFILTERIOT(idHotel, "%" + username + "%", room, billing, start, null).Count();
                    var elementos = context.SP_FDS_OBTAINDISABLEDUSERSALLFILTERIOT(idHotel, "%" + username + "%", room, billing, start, null).Skip((pageIndex) * pageSize).Take(pageSize);
                    pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                    List<Users> list = new List<Users>();
                    foreach (var element in elementos)
                    {
                        Users obj = new Users();
                        Util.Map(element, obj);
                        obj.Name = element.UserName;
                        list.Add(obj);
                    }
                    return list;
                }
                else if (start.Equals(DateTime.MaxValue) && !end.Equals(DateTime.MaxValue))
                {
                    int count = context.SP_FDS_OBTAINDISABLEDUSERSALLFILTERIOT(idHotel, "%" + username + "%", room, billing, null, end).Count();
                    var elementos = context.SP_FDS_OBTAINDISABLEDUSERSALLFILTERIOT(idHotel, "%" + username + "%", room, billing, null, end).Skip((pageIndex) * pageSize).Take(pageSize);
                    pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                    List<Users> list = new List<Users>();
                    foreach (var element in elementos)
                    {
                        Users obj = new Users();
                        Util.Map(element, obj);
                        obj.Name = element.UserName;
                        list.Add(obj);
                    }
                    return list;
                }
                else
                {
                    int count = context.SP_FDS_OBTAINDISABLEDUSERSALLFILTERIOT(idHotel, "%" + username + "%", room, billing, start, end).Count();
                    var elementos = context.SP_FDS_OBTAINDISABLEDUSERSALLFILTERIOT(idHotel, "%" + username + "%", room, billing, start, end).Skip((pageIndex) * pageSize).Take(pageSize);
                    pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                    List<Users> list = new List<Users>();
                    foreach (var element in elementos)
                    {
                        Users obj = new Users();
                        Util.Map(element, obj);
                        obj.Name = element.UserName;
                        list.Add(obj);
                    }
                    return list;
                }

                return null;
            }
        }

        public List<Users> GetDisabledUsersIoT(string listadohoteles, string username, DateTime start, DateTime end, int billing, int room, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                List<Users> list = new List<Users>();
                int count = 0;
                string[] ids = listadohoteles.Split(',');
                foreach (string id in ids)
                {
                    int idHotel = int.Parse(id);
                    if (start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                    {
                        count += context.SP_FDS_OBTAINDISABLEDUSERSALLFILTERIOT(idHotel, "%" + username + "%", room, billing, null, null).Count();
                        var elementos = context.SP_FDS_OBTAINDISABLEDUSERSALLFILTERIOT(idHotel, "%" + username + "%", room, billing, null, null);

                        foreach (var element in elementos)
                        {
                            Users obj = new Users();
                            Util.Map(element, obj);
                            obj.Name = element.UserName;
                            list.Add(obj);
                        }
                    }
                    else if (!start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                    {
                        count += context.SP_FDS_OBTAINDISABLEDUSERSALLFILTERIOT(idHotel, "%" + username + "%", room, billing, start, null).Count();
                        var elementos = context.SP_FDS_OBTAINDISABLEDUSERSALLFILTERIOT(idHotel, "%" + username + "%", room, billing, start, null);
                        foreach (var element in elementos)
                        {
                            Users obj = new Users();
                            Util.Map(element, obj);
                            obj.Name = element.UserName;
                            list.Add(obj);
                        }
                    }
                    else if (start.Equals(DateTime.MaxValue) && !end.Equals(DateTime.MaxValue))
                    {
                        count += context.SP_FDS_OBTAINDISABLEDUSERSALLFILTERIOT(idHotel, "%" + username + "%", room, billing, null, end).Count();
                        var elementos = context.SP_FDS_OBTAINDISABLEDUSERSALLFILTERIOT(idHotel, "%" + username + "%", room, billing, null, end);
                        foreach (var element in elementos)
                        {
                            Users obj = new Users();
                            Util.Map(element, obj);
                            obj.Name = element.UserName;
                            list.Add(obj);
                        }
                    }
                    else
                    {
                        count += context.SP_FDS_OBTAINDISABLEDUSERSALLFILTERIOT(idHotel, "%" + username + "%", room, billing, start, end).Count();
                        var elementos = context.SP_FDS_OBTAINDISABLEDUSERSALLFILTERIOT(idHotel, "%" + username + "%", room, billing, start, end);//.Skip((pageIndex) * pageSize).Take(pageSize);

                        foreach (var element in elementos)
                        {
                            Users obj = new Users();
                            Util.Map(element, obj);
                            obj.Name = element.UserName;
                            list.Add(obj);
                        }
                    }
                }

                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                return list.OrderBy(a => a.ValidSince).Skip((pageIndex) * pageSize).Take(pageSize).ToList();
            }
        }

        public List<Users> GetDisabledUsersLocation(int idLocation, string username, DateTime start, DateTime end, int billing, int room, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                if (start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                {
                    int count = context.SP_FDS_OBTAINDISABLEDUSERSALLFILTERLOCATION(idLocation, "%" + username + "%", room, billing, null, null).Count();
                    var elementos = context.SP_FDS_OBTAINDISABLEDUSERSALLFILTERLOCATION(idLocation, "%" + username + "%", room, billing, null, null).Skip((pageIndex) * pageSize).Take(pageSize);
                    pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                    List<Users> list = new List<Users>();
                    foreach (var element in elementos)
                    {
                        Users obj = new Users();
                        Util.Map(element, obj);
                        obj.Name = element.UserName;
                        list.Add(obj);
                    }
                    return list;
                }
                else if (!start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                {
                    int count = context.SP_FDS_OBTAINDISABLEDUSERSALLFILTERLOCATION(idLocation, "%" + username + "%", room, billing, start, null).Count();
                    var elementos = context.SP_FDS_OBTAINDISABLEDUSERSALLFILTERLOCATION(idLocation, "%" + username + "%", room, billing, start, null).Skip((pageIndex) * pageSize).Take(pageSize);
                    pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                    List<Users> list = new List<Users>();
                    foreach (var element in elementos)
                    {
                        Users obj = new Users();
                        Util.Map(element, obj);
                        obj.Name = element.UserName;
                        list.Add(obj);
                    }
                    return list;
                }
                else if (start.Equals(DateTime.MaxValue) && !end.Equals(DateTime.MaxValue))
                {
                    int count = context.SP_FDS_OBTAINDISABLEDUSERSALLFILTERLOCATION(idLocation, "%" + username + "%", room, billing, null, end).Count();
                    var elementos = context.SP_FDS_OBTAINDISABLEDUSERSALLFILTERLOCATION(idLocation, "%" + username + "%", room, billing, null, end).Skip((pageIndex) * pageSize).Take(pageSize);
                    pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                    List<Users> list = new List<Users>();
                    foreach (var element in elementos)
                    {
                        Users obj = new Users();
                        Util.Map(element, obj);
                        obj.Name = element.UserName;
                        list.Add(obj);
                    }
                    return list;
                }
                else
                {
                    int count = context.SP_FDS_OBTAINDISABLEDUSERSALLFILTERLOCATION(idLocation, "%" + username + "%", room, billing, start, end).Count();
                    var elementos = context.SP_FDS_OBTAINDISABLEDUSERSALLFILTERLOCATION(idLocation, "%" + username + "%", room, billing, start, end).Skip((pageIndex) * pageSize).Take(pageSize);
                    pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                    List<Users> list = new List<Users>();
                    foreach (var element in elementos)
                    {
                        Users obj = new Users();
                        Util.Map(element, obj);
                        obj.Name = element.UserName;
                        list.Add(obj);
                    }
                    return list;
                }

                return null;
            }
        }

        public List<Users> GetDisabledUsersLocation(int idLocation, string username, DateTime start, DateTime end, int billing, int room, int idfdsuser, int iot, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                if (start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                {
                    int count = context.SP_FDS_OBTAINDISABLEDUSERSLOCATIONFILTER(idLocation, "%" + username + "%", room, billing, null, null, idfdsuser, iot).Count();
                    var elementos = context.SP_FDS_OBTAINDISABLEDUSERSLOCATIONFILTER(idLocation, "%" + username + "%", room, billing, null, null, idfdsuser, iot).Skip((pageIndex) * pageSize).Take(pageSize);
                    pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                    List<Users> list = new List<Users>();
                    foreach (var element in elementos)
                    {
                        Users obj = new Users();
                        Util.Map(element, obj);
                        obj.Name = element.UserName;
                        list.Add(obj);
                    }
                    return list;
                }
                else if (!start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                {
                    int count = context.SP_FDS_OBTAINDISABLEDUSERSLOCATIONFILTER(idLocation, "%" + username + "%", room, billing, start, null, idfdsuser, iot).Count();
                    var elementos = context.SP_FDS_OBTAINDISABLEDUSERSLOCATIONFILTER(idLocation, "%" + username + "%", room, billing, start, null, idfdsuser, iot).Skip((pageIndex) * pageSize).Take(pageSize);
                    pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                    List<Users> list = new List<Users>();
                    foreach (var element in elementos)
                    {
                        Users obj = new Users();
                        Util.Map(element, obj);
                        obj.Name = element.UserName;
                        list.Add(obj);
                    }
                    return list;
                }
                else if (start.Equals(DateTime.MaxValue) && !end.Equals(DateTime.MaxValue))
                {
                    int count = context.SP_FDS_OBTAINDISABLEDUSERSLOCATIONFILTER(idLocation, "%" + username + "%", room, billing, null, end, idfdsuser, iot).Count();
                    var elementos = context.SP_FDS_OBTAINDISABLEDUSERSLOCATIONFILTER(idLocation, "%" + username + "%", room, billing, null, end, idfdsuser, iot).Skip((pageIndex) * pageSize).Take(pageSize);
                    pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                    List<Users> list = new List<Users>();
                    foreach (var element in elementos)
                    {
                        Users obj = new Users();
                        Util.Map(element, obj);
                        obj.Name = element.UserName;
                        list.Add(obj);
                    }
                    return list;
                }
                else
                {
                    int count = context.SP_FDS_OBTAINDISABLEDUSERSLOCATIONFILTER(idLocation, "%" + username + "%", room, billing, start, end, idfdsuser, iot).Count();
                    var elementos = context.SP_FDS_OBTAINDISABLEDUSERSLOCATIONFILTER(idLocation, "%" + username + "%", room, billing, start, end, idfdsuser, iot).Skip((pageIndex) * pageSize).Take(pageSize);
                    pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                    List<Users> list = new List<Users>();
                    foreach (var element in elementos)
                    {
                        Users obj = new Users();
                        Util.Map(element, obj);
                        obj.Name = element.UserName;
                        list.Add(obj);
                    }
                    return list;
                }

                return null;
            }
        }

        public List<Users> GetDisabledUsersLocation(int idLocation, List<FDSUsers> users, string username, DateTime start, DateTime end, int billing, int room, int idfdsuser, int iot, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                List<Users> list = new List<Users>();
                if (start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                {
                    var elementos = context.SP_FDS_OBTAINDISABLEDUSERSLOCATIONFILTER(idLocation, "%" + username + "%", room, billing, null, null, idfdsuser, iot);
                    foreach (var element in elementos)
                    {
                        if (!idfdsuser.Equals(0))
                        {
                            Users obj = new Users();
                            Util.Map(element, obj);
                            obj.Name = element.UserName;
                            list.Add(obj);
                        }
                        else
                        {
                            FDSUsers u = (from t in users where t.IdUser.Equals(element.IdFDSUser) select t).FirstOrDefault();
                            if (u != null)
                            {
                                Users obj = new Users();
                                Util.Map(element, obj);
                                obj.Name = element.UserName;
                                list.Add(obj);

                            }
                        }
                    }
                }
                else if (!start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                {
                    var elementos = context.SP_FDS_OBTAINDISABLEDUSERSLOCATIONFILTER(idLocation, "%" + username + "%", room, billing, start, null, idfdsuser, iot);

                    foreach (var element in elementos)
                    {
                        if (!idfdsuser.Equals(0))
                        {
                            Users obj = new Users();
                            Util.Map(element, obj);
                            obj.Name = element.UserName;
                            list.Add(obj);
                        }
                        else
                        {
                            FDSUsers u = (from t in users where t.IdUser.Equals(element.IdFDSUser) select t).FirstOrDefault();
                            if (u != null)
                            {
                                Users obj = new Users();
                                Util.Map(element, obj);
                                obj.Name = element.UserName;
                                list.Add(obj);

                            }
                        }
                    }
                }
                else if (start.Equals(DateTime.MaxValue) && !end.Equals(DateTime.MaxValue))
                {
                    var elementos = context.SP_FDS_OBTAINDISABLEDUSERSLOCATIONFILTER(idLocation, "%" + username + "%", room, billing, null, end, idfdsuser, iot);

                    foreach (var element in elementos)
                    {
                        if (!idfdsuser.Equals(0))
                        {
                            Users obj = new Users();
                            Util.Map(element, obj);
                            obj.Name = element.UserName;
                            list.Add(obj);
                        }
                        else
                        {
                            FDSUsers u = (from t in users where t.IdUser.Equals(element.IdFDSUser) select t).FirstOrDefault();
                            if (u != null)
                            {
                                Users obj = new Users();
                                Util.Map(element, obj);
                                obj.Name = element.UserName;
                                list.Add(obj);

                            }
                        }
                    }
                }
                else
                {
                    var elementos = context.SP_FDS_OBTAINDISABLEDUSERSLOCATIONFILTER(idLocation, "%" + username + "%", room, billing, start, end, idfdsuser, iot);

                    foreach (var element in elementos)
                    {
                        if (!idfdsuser.Equals(0))
                        {
                            Users obj = new Users();
                            Util.Map(element, obj);
                            obj.Name = element.UserName;
                            list.Add(obj);
                        }
                        else
                        {
                            FDSUsers u = (from t in users where t.IdUser.Equals(element.IdFDSUser) select t).FirstOrDefault();
                            if (u != null)
                            {
                                Users obj = new Users();
                                Util.Map(element, obj);
                                obj.Name = element.UserName;
                                list.Add(obj);

                            }
                        }
                    }
                    return list;
                }

                int count = list.Count;
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                return list.Skip((pageIndex) * pageSize).Take(pageSize).ToList();
            }
        }

        public List<Users> GetDisabledUsersLocationIoT(int idLocation, string username, DateTime start, DateTime end, int billing, int room, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                if (start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                {
                    int count = context.SP_FDS_OBTAINDISABLEDUSERSALLFILTERLOCATIONIOT(idLocation, "%" + username + "%", room, billing, null, null).Count();
                    var elementos = context.SP_FDS_OBTAINDISABLEDUSERSALLFILTERLOCATIONIOT(idLocation, "%" + username + "%", room, billing, null, null).Skip((pageIndex) * pageSize).Take(pageSize);
                    pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                    List<Users> list = new List<Users>();
                    foreach (var element in elementos)
                    {
                        Users obj = new Users();
                        Util.Map(element, obj);
                        obj.Name = element.UserName;
                        list.Add(obj);
                    }
                    return list;
                }
                else if (!start.Equals(DateTime.MaxValue) && end.Equals(DateTime.MaxValue))
                {
                    int count = context.SP_FDS_OBTAINDISABLEDUSERSALLFILTERLOCATIONIOT(idLocation, "%" + username + "%", room, billing, start, null).Count();
                    var elementos = context.SP_FDS_OBTAINDISABLEDUSERSALLFILTERLOCATIONIOT(idLocation, "%" + username + "%", room, billing, start, null).Skip((pageIndex) * pageSize).Take(pageSize);
                    pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                    List<Users> list = new List<Users>();
                    foreach (var element in elementos)
                    {
                        Users obj = new Users();
                        Util.Map(element, obj);
                        obj.Name = element.UserName;
                        list.Add(obj);
                    }
                    return list;
                }
                else if (start.Equals(DateTime.MaxValue) && !end.Equals(DateTime.MaxValue))
                {
                    int count = context.SP_FDS_OBTAINDISABLEDUSERSALLFILTERLOCATIONIOT(idLocation, "%" + username + "%", room, billing, null, end).Count();
                    var elementos = context.SP_FDS_OBTAINDISABLEDUSERSALLFILTERLOCATIONIOT(idLocation, "%" + username + "%", room, billing, null, end).Skip((pageIndex) * pageSize).Take(pageSize);
                    pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                    List<Users> list = new List<Users>();
                    foreach (var element in elementos)
                    {
                        Users obj = new Users();
                        Util.Map(element, obj);
                        obj.Name = element.UserName;
                        list.Add(obj);
                    }
                    return list;
                }
                else
                {
                    int count = context.SP_FDS_OBTAINDISABLEDUSERSALLFILTERLOCATIONIOT(idLocation, "%" + username + "%", room, billing, start, end).Count();
                    var elementos = context.SP_FDS_OBTAINDISABLEDUSERSALLFILTERLOCATIONIOT(idLocation, "%" + username + "%", room, billing, start, end).Skip((pageIndex) * pageSize).Take(pageSize);
                    pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                    List<Users> list = new List<Users>();
                    foreach (var element in elementos)
                    {
                        Users obj = new Users();
                        Util.Map(element, obj);
                        obj.Name = element.UserName;
                        list.Add(obj);
                    }
                    return list;
                }

                return null;
            }
        }

        public List<Users> GetActiveUsers(int idHotel, DateTime date, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from a in context.Users
                                where (a.IdHotel.Equals(idHotel) && a.Enabled.Equals(true) && date >= a.ValidSince && date <= a.ValidTill && a.TimeCredit > 0)
                                select a);

                elements = elements.OrderBy(a => a.IdUser);
                int count = elements.Count();
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                elements = elements.Skip((pageIndex) * pageSize).Take(pageSize);

                List<Users> list = new List<Users>();
                foreach (var element in elements)
                {
                    Users obj = new Users();
                    Util.Map(element, obj);
                    list.Add(obj);
                }
                return list;
            }
        }

        public List<FailedRequests> GetFailedRequests(int idHotel, DateTime start, DateTime end)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from a in context.FailedRequests
                                where (a.IdHotel.Equals(idHotel) && start <= a.MessageDate && end >= a.MessageDate)
                                select a).ToList();


                List<FailedRequests> list = new List<FailedRequests>();
                foreach (var element in elements)
                {
                    FailedRequests obj = new FailedRequests();
                    Util.Map(element, obj);
                    list.Add(obj);
                }
                return list;
            }

        }

        public FailedRequests GetFailedRequests(int idHotel, string username, string mac)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from a in context.FailedRequests
                                where (a.IdHotel.Equals(idHotel) && a.UserName.Equals(username) && a.CallerID.Equals(mac))
                                orderby a.MessageDate descending
                                select a ).FirstOrDefault();

                FailedRequests obj = new FailedRequests();
                if (elements != null)
                {                    
                    Util.Map(elements, obj);                    
                }
                return obj;
            }
        }

        public List<FailedRequests> GetFailedRequests(int idHotel, string start, string end, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from a in context.FailedRequests
                                where (a.IdHotel.Equals(idHotel) && 
                                DateTime.Parse(start) <= a.MessageDate && 
                                DateTime.Parse(end) >= a.MessageDate)
                                select a);

                int count = elements.Count();
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                elements = elements.Skip((pageIndex) * pageSize).Take(pageSize);


                List<FailedRequests> list = new List<FailedRequests>();
                foreach (var element in elements)
                {
                    FailedRequests obj = new FailedRequests();
                    Util.Map(element, obj);
                    list.Add(obj);
                }
                return list;
            }

        }

        public List<FailedRequests> GetFailedRequestsHoteles(string listadoHoteles, string start, string end, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                List<FailedRequests> list = new List<FailedRequests>();
                int count = 0;
                string[] ids = listadoHoteles.Split(',');
                foreach (string id in ids)
                {
                    int idHotel = int.Parse(id);
                    var elements = (from a in context.FailedRequests
                                    where (a.IdHotel.Equals(idHotel) &&
                                    DateTime.Parse(start) <= a.MessageDate &&
                                    DateTime.Parse(end) >= a.MessageDate)
                                    select a);

                    count += elements.Count();
                    
                    foreach (var element in elements)
                    {
                        FailedRequests obj = new FailedRequests();
                        Util.Map(element, obj);
                        list.Add(obj);
                    }
                }
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                return list.OrderBy(a => a.MessageDate).Skip((pageIndex) * pageSize).Take(pageSize).ToList();
            }

        }

        public List<FailedRequests> GetFailedRequests(string locationIdentifier, string start, string end, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from a in context.FailedRequests
                                where (a.LocationIdentifier.Equals(locationIdentifier) &&
                                DateTime.Parse(start) <= a.MessageDate &&
                                DateTime.Parse(end) >= a.MessageDate)
                                select a);

                int count = elements.Count();
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                elements = elements.Skip((pageIndex) * pageSize).Take(pageSize);


                List<FailedRequests> list = new List<FailedRequests>();
                foreach (var element in elements)
                {
                    FailedRequests obj = new FailedRequests();
                    Util.Map(element, obj);
                    list.Add(obj);
                }
                return list;
            }

        }

        /// <summary>
        /// Insertamos un nuevo hotel en la base de datos
        /// </summary>
        /// <param name="obj">Elemento del tipo Hotel a insertar</param>
        public void Insert(SessionsLog obj)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                context.SessionsLog.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        /// <summary>
        /// Actualizamos los datos de un hotel existente
        /// </summary>
        /// <param name="obj">Elemento del tipo Hotel con los datos a actualizar</param>
        public void Update(SessionsLog obj)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.SessionsLog
                               where h.IdCount.Equals(obj.IdCount)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(obj, element);

                context.SubmitChanges();
            }
        }


        public List<SessionsLog> GetSessionLogs(int idHotel, DateTime start, DateTime end, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from a in context.SessionsLog
                                where (a.IdHotel.Equals(idHotel) && start <= a.Date && end >= a.Date)
                                select a);

                int count = elements.Count();
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                elements = elements.Skip((pageIndex) * pageSize).Take(pageSize);


                List<SessionsLog> list = new List<SessionsLog>();
                foreach (var element in elements)
                {
                    SessionsLog obj = new SessionsLog();
                    Util.Map(element, obj);
                    list.Add(obj);
                }
                return list;
            }



        }

        public List<SessionsLog> GetSessionLogs(int idHotel)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                List<SessionsLog> list = new List<SessionsLog>();

                list = (from a in context.SessionsLog
                        where (a.IdHotel.Equals(idHotel))
                        orderby a.Date ascending
                        select a).ToList();

                return list;
            }
        }

        public List<SessionsLog> GetSessionLogs(int idHotel, DateTime start, DateTime end)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                List<SessionsLog> list = new List<SessionsLog>();

                list = (from a in context.SessionsLog
                        where (a.IdHotel.Equals(idHotel) && start <= a.Date && end >= a.Date && a.Identifier.Equals(null))
                        orderby a.Date ascending
                        select a).ToList();
                return list;
            }
        }

        public List<SessionsLog> GetSessionLogsLocation(int idLocation, DateTime start, DateTime end)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                FDSLocationsIdentifier identifier = (from h in context.FDSLocationsIdentifier
                                                     where h.IdLocation.Equals(idLocation)
                                                     select h).FirstOrDefault();
                
                var elements = (from a in context.SessionsLog
                                where (a.Identifier.Equals(identifier.Identifier) && start <= a.Date && end >= a.Date)
                                orderby a.Date ascending
                                select a);

                List<SessionsLog> list = new List<SessionsLog>();
                foreach (var element in elements)
                {
                    SessionsLog obj = new SessionsLog();
                    Util.Map(element, obj);
                    list.Add(obj);
                }
                return list;
            }
        }

        public ActiveSessions GetActiveSession(string MAC, string username)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.ActiveSessions
                               where (h.CallerID.Equals(MAC) && h.UserName.Equals(username))
                               select h).SingleOrDefault();

                ActiveSessions obj = new ActiveSessions();
                if (element != null)
                {
                    Util.Map(element, obj);

                }

                return obj;
            }
        }

        public FailedRequests GetFailedRequest(string MAC, string username, DateTime time)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.FailedRequests
                               where (h.CallerID.Equals(MAC) && h.UserName.Equals(username) && time <= h.MessageDate)
                               orderby h.MessageDate descending
                               select h).FirstOrDefault();

                FailedRequests obj = new FailedRequests();
                if (element != null)
                {
                    Util.Map(element, obj);

                }

                return obj;
            }
        }

        public void Select(ClosedSessions2 obj, string mac, DateTime time)
        {

            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var element = (from h in context.ClosedSessions2
                               where (h.CallerID.Equals(mac) && h.SessionTerminated > time)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        public List<ClosedSessions2> SelectClossedSession(int idHotel, DateTime time)
        {
            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var elements = (from h in context.ClosedSessions2
                                where (h.IdHotel.Equals(idHotel) && h.SessionTerminated > time)
                                orderby h.SessionStarted descending
                                select h);

                List<ClosedSessions2> list = new List<ClosedSessions2>();
                foreach (var element in elements)
                {
                    ClosedSessions2 obj = new ClosedSessions2();
                    Util.Map(element, obj);
                    list.Add(obj);
                }
                return list;
            }
        }

        public List<ClosedSessions2> SelectClossedSessionLocation(int idLocation, DateTime time)
        {
            FDSLocationsIdentifier location = null;
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                location = (from h in context.FDSLocationsIdentifier
                            where h.IdLocation.Equals(idLocation)
                            select h).FirstOrDefault();

            }
            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var elements = (from h in context.ClosedSessions2
                                where (h.LocationIdentifier.Equals(location.Identifier) && h.SessionTerminated > time)
                                orderby h.SessionStarted descending
                                select h);

                List<ClosedSessions2> list = new List<ClosedSessions2>();
                foreach (var element in elements)
                {
                    ClosedSessions2 obj = new ClosedSessions2();
                    Util.Map(element, obj);
                    list.Add(obj);
                }
                return list;
            }
        }

        public List<ClosedSessions2> SelectAllClossedSessionSite(int idSite)
        {
            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var elements = (from h in context.ClosedSessions2
                                where h.IdHotel.Equals(idSite)
                                select h);

                List<ClosedSessions2> list = new List<ClosedSessions2>();
                foreach (var element in elements)
                {
                    ClosedSessions2 obj = new ClosedSessions2();
                    Util.Map(element, obj);
                    list.Add(obj);
                }
                return list;
            }
        }

        public List<ClosedSessions2> SelectClossedSession(string mac, DateTime time)
        {
            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var elements = (from h in context.ClosedSessions2
                                where (h.CallerID.Equals(mac) && h.SessionTerminated > time)
                                orderby h.SessionStarted descending
                                select h);

                List<ClosedSessions2> list = new List<ClosedSessions2>();
                foreach (var element in elements)
                {
                    ClosedSessions2 obj = new ClosedSessions2();
                    Util.Map(element, obj);
                    list.Add(obj);
                }
                return list;
            }
        }

        public List<ClosedSessions2> SelectClossedSession(int idHotel, string username, DateTime start, DateTime end, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var elements = (from h in context.ClosedSessions2
                                where h.IdHotel.Equals(idHotel) && h.SessionTerminated >= start && h.SessionTerminated <= end && h.UserName.Contains(username)
                                select h);

                int count = elements.Count();
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                elements = elements.OrderBy(a => a.SessionTerminated).Skip((pageIndex) * pageSize).Take(pageSize);

                List<ClosedSessions2> list = new List<ClosedSessions2>();
                foreach (var element in elements)
                {
                    ClosedSessions2 obj = new ClosedSessions2();
                    Util.Map(element, obj);
                    list.Add(obj);
                }
                return list;
            }
        }

        public List<ClosedSessions2> SelectClossedSessionListadoHoteles(string listadoHoteles, string username, DateTime start, DateTime end, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                string[] ids = listadoHoteles.Split(',');
                int count = 0;
                List<ClosedSessions2> list = new List<ClosedSessions2>();
                foreach (string id in ids)
                {
                    int idHotel = int.Parse(id);
                    var elements = (from h in context.ClosedSessions2
                                    where h.IdHotel.Equals(idHotel) && h.SessionTerminated >= start && h.SessionTerminated <= end && h.UserName.Contains(username)
                                    select h);

                    count += elements.Count();
                    foreach (var element in elements)
                    {
                        ClosedSessions2 obj = new ClosedSessions2();
                        Util.Map(element, obj);
                        list.Add(obj);
                    }
                }
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                return list.OrderBy(a => a.SessionTerminated).Skip((pageIndex) * pageSize).Take(pageSize).ToList();
            }
        }

        public List<ClosedSessions2> SelectClossedSessionListadoHoteles(string listadoHoteles, string username, DateTime start, DateTime end)
        {
            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                string[] ids = listadoHoteles.Split(',');
                int count = 0;
                List<ClosedSessions2> list = new List<ClosedSessions2>();
                foreach (string id in ids)
                {
                    int idHotel = int.Parse(id);
                    var elements = (from h in context.ClosedSessions2
                                    where h.IdHotel.Equals(idHotel) && h.SessionTerminated >= start && h.SessionTerminated <= end && h.UserName.Contains(username)
                                    select h);

                    count += elements.Count();
                    foreach (var element in elements)
                    {
                        ClosedSessions2 obj = new ClosedSessions2();
                        Util.Map(element, obj);
                        list.Add(obj);
                    }
                }
                return list.OrderBy(a => a.SessionTerminated).ToList();
            }
        }

        public List<ClosedSessions2> SelectClossedSession(string locationidentifier, int idHotel, string username, DateTime start, DateTime end, int pageIndex, int pageSize, ref int pageNumber)
        {
            try
            {
                using (DataModel2 context = new DataModel2(Util.ConnectionString()))
                {
                    var elements = (from h in context.ClosedSessions2
                                    where (h.IdHotel.Equals(idHotel) && h.LocationIdentifier.Equals(locationidentifier) && h.SessionTerminated >= start && h.SessionTerminated <= end) && (string.IsNullOrEmpty(username) || h.UserName.Equals(username))
                                    select h);

                    int count = elements.Count();
                    pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                    elements = elements.OrderBy(a => a.SessionTerminated).Skip((pageIndex) * pageSize).Take(pageSize);

                    List<ClosedSessions2> list = new List<ClosedSessions2>();
                    foreach (var element in elements)
                    {
                        ClosedSessions2 obj = new ClosedSessions2();
                        Util.Map(element, obj);
                        list.Add(obj);
                    }
                    return list;
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public List<ClosedSessions2> SelectClossedSession(string locationidentifier, int idHotel, string username, DateTime start, DateTime end)
        {
            try
            {
                using (DataModel2 context = new DataModel2(Util.ConnectionString()))
                {
                    var elements = (from h in context.ClosedSessions2
                                    where (h.IdHotel.Equals(idHotel) && h.LocationIdentifier.Equals(locationidentifier) && h.SessionTerminated >= start && h.SessionTerminated <= end) && (string.IsNullOrEmpty(username) || h.UserName.Equals(username))
                                    select h);


                    List<ClosedSessions2> list = new List<ClosedSessions2>();
                    foreach (var element in elements.OrderBy(a => a.SessionTerminated))
                    {
                        ClosedSessions2 obj = new ClosedSessions2();
                        Util.Map(element, obj);
                        list.Add(obj);
                    }
                    return list;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ClosedSessions2> SelectClossedSession(int idHotel, string username, DateTime start, DateTime end)
        {
            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var elements = (from h in context.ClosedSessions2
                                where (h.IdHotel.Equals(idHotel) && h.SessionTerminated >= start && h.SessionTerminated <= end && h.UserName.Contains(username))
                                orderby h.SessionTerminated ascending
                                select h);

                List<ClosedSessions2> list = new List<ClosedSessions2>();
                foreach (var element in elements)
                {
                    ClosedSessions2 obj = new ClosedSessions2();
                    Util.Map(element, obj);
                    list.Add(obj);
                }
                return list;
            }
        }

        public List<Users> SelectAllUsersLocation(int idLocation)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.Users
                                where h.IdLocation.Equals(idLocation)
                                select h);

                List<Users> list = new List<Users>();
                foreach (var element in elements)
                {
                    Users obj = new Users();
                    Util.Map(element, obj);
                    list.Add(obj);
                }
                return list;
            }

        }
        public List<ActiveSessions> SelectActiveSession(int idHotel, DateTime start)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.ActiveSessions
                                where (h.IdHotel.Equals(idHotel) && h.SessionStarted >= start)
                                select h);

                List<ActiveSessions> list = new List<ActiveSessions>();
                foreach (var element in elements)
                {
                    ActiveSessions obj = new ActiveSessions();
                    Util.Map(element, obj);
                    list.Add(obj);
                }
                return list;
            }

        }


        #endregion

        #region Locations

        public void Insert(MerakiLocationsGPS obj)
        {

            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                context.MerakiLocationsGPS.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        public void Update(MerakiLocationsGPS obj)
        {
            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var element = (from h in context.MerakiLocationsGPS
                               where h.Id.Equals(obj.Id)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(obj, element);

                context.SubmitChanges();
            }
        }


        public void Insert(FDSLocationsIdentifier obj)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                context.FDSLocationsIdentifier.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }


        public void Insert(Locations2 obj)
        {

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                context.Locations2.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        public void Select(Locations2 obj, int idLocation)
        {

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var element = (from h in context.Locations2
                               where h.IdLocation.Equals(idLocation)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        public void Select(Locations2 obj, int idHotel, string name)
        {

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var element = (from h in context.Locations2
                               where (h.Value.ToUpper().Equals(name.ToUpper()) && h.IdHotel.Equals(idHotel))
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        public void Select(Locations2 obj, int idsite, bool _default)
        {

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var element = (from h in context.Locations2
                               where h.IdHotel.Equals(idsite) && h.Default.Equals(_default)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        //public void Select(Locations obj, string deviceid)
        //{

        //    using (DataModel context = new DataModel(Util.ConnectionString()))
        //    {
        //        var element = (from h in context.Locations
        //                       where h.deviceId.Equals(deviceid)
        //                       select h).SingleOrDefault();

        //        if (element != null)
        //            Util.Map(element, obj);
        //    }
        //}

        public void Update(Locations2 obj)
        {
            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var element = (from h in context.Locations2
                               where h.IdLocation.Equals(obj.IdLocation)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(obj, element);

                context.SubmitChanges();
            }
        }

        public List<Locations2> SelectLocations()
        {

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var elements = (from h in context.Locations2
                                select h);

                List<Locations2> listado = new List<Locations2>();

                foreach (var element in elements)
                {
                    Locations2 obj = new Locations2();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        /// <summary>
        /// Seleccionar de todas las habitaciones de un hotel
        /// </summary>
        /// <param name="idChain">Identificador de la cadena de hoteles</param>
        /// <returns></returns>
        public List<Locations2> SelectLocations(int idHotel)
        {

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var elements = (from h in context.Locations2
                                where h.IdHotel.Equals(idHotel)
                                select h);

                List<Locations2> listado = new List<Locations2>();

                foreach (var element in elements)
                {
                    Locations2 obj = new Locations2();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        /// <summary>
        /// Seleccionar de todas las habitaciones de un hotel
        /// </summary>
        /// <param name="idChain">Identificador de la cadena de hoteles</param>
        /// <returns></returns>
        public Locations2 SelectLocation(int idLocation)
        {

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var element = (from h in context.Locations2
                                where h.IdLocation.Equals(idLocation)
                                select h).FirstOrDefault();

                Locations2 obj = new Locations2();
                if (element != null)
                    Util.Map(element, obj);

                return obj;
            }
        }

        public List<Locations2> SelectLocationsPorGrupos(int IdGroup)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from t in context.Locations
                                join d in context.FDSGroupsMembers on t.IdLocation equals d.IdMember
                                where d.IdMemberType.Equals(3) && d.IdGroup.Equals(IdGroup)
                                orderby t.IdHotel
                                select t).ToList();

                List<Locations2> listado = new List<Locations2>();

                foreach (var element in elements)
                {
                    Locations2 obj = new Locations2();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        #endregion

        #region Priorities

        public void Select(Priorities obj, int idPriority)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.Priorities
                               where h.IdPriority.Equals(idPriority)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        public void Insert(Priorities obj)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                context.Priorities.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        public void Update(Priorities obj)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.Priorities
                               where h.IdPriority.Equals(obj.IdPriority)
                               select h).SingleOrDefault();

                if (element != null)
                {
                    element.IdPriority = obj.IdPriority;
                    element.IdHotel = obj.IdHotel;
                    element.Description = obj.Description;
                }

                context.SubmitChanges();
            }
        }

        public void DeletePriority(int id)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.Priorities
                                where h.IdPriority.Equals(id)
                                select h);

                if (elements != null)
                    foreach (var element in elements)
                    {
                        context.Priorities.DeleteOnSubmit(element);
                    }
                context.SubmitChanges();
            }
        }

        public List<Priorities> SelectPriorities()
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.Priorities
                                select h);

                List<Priorities> listado = new List<Priorities>();

                foreach (var element in elements)
                {
                    Priorities obj = new Priorities();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        /// <summary>
        /// Seleccionar de todas las habitaciones de un hotel
        /// </summary>
        /// <param name="idChain">Identificador de la cadena de hoteles</param>
        /// <returns></returns>
        public List<Priorities> SelectPriorities(int idHotel)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.Priorities
                                where h.IdHotel.Equals(idHotel)
                                select h);

                List<Priorities> listado = new List<Priorities>();

                foreach (var element in elements)
                {
                    Priorities obj = new Priorities();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<Priorities> SelectPriorities(string listadoHoteles)
        {
            List<Priorities> listado = new List<Priorities>();
            string[] ids = listadoHoteles.Split(',');

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                foreach (string id in ids)
                {

                    var elements = (from h in context.Priorities
                                    where h.IdHotel.Equals(int.Parse(id))
                                    select h);

                    foreach (var element in elements)
                    {
                        Priorities obj = new Priorities();
                        Util.Map(element, obj);
                        listado.Add(obj);
                    }
                }
                return listado;
            }
        }

        #endregion

        #region PayPalTransactions 

        public void Select(PayPalTransactions obj, int idTransaction)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.PayPalTransactions
                               where h.IdTransaction.Equals(idTransaction)
                               select h).SingleOrDefault();

                if (element != null)
                {
                    //   Util.Map(element, obj);
                    obj.IdTransaction = element.IdTransaction;
                    obj.IdHotel = element.IdHotel;
                    obj.IdBillingType = element.IdBillingType;
                    obj.Price = element.Price;
                    obj.Status = element.Status;
                    obj.Email = element.Email;
                    obj.CreateDate = element.CreateDate;
                    obj.UpdateDate = element.UpdateDate;
                    obj.NameClient = element.NameClient;
                    obj.MAC = element.MAC;
                    obj.InvoiceNo = element.InvoiceNo;
                    obj.OS = element.OS;
                    obj.UserName = element.UserName;
                    obj.Password = element.Password;
                    obj.Survey = element.Survey;
                    obj.UI = element.UI;
                    obj.CallerIP = element.CallerIP;
                    obj.IdLocation = element.IdLocation;
                }
            }
        }

        public void SelectLast(PayPalTransactions obj)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.PayPalTransactions
                               orderby h.IdTransaction descending
                               select h).FirstOrDefault();

                if (element != null)
                {
                    //   Util.Map(element, obj);
                    obj.IdTransaction = element.IdTransaction;
                    obj.IdHotel = element.IdHotel;
                    obj.IdBillingType = element.IdBillingType;
                    obj.Price = element.Price;
                    obj.Status = element.Status;
                    obj.Email = element.Email;
                    obj.CreateDate = element.CreateDate;
                    obj.UpdateDate = element.UpdateDate;
                    obj.NameClient = element.NameClient;
                    obj.MAC = element.MAC;
                    obj.InvoiceNo = element.InvoiceNo;
                    obj.OS = element.OS;
                    obj.UserName = element.UserName;
                    obj.Password = element.Password;
                    obj.Survey = element.Survey;
                    obj.CallerIP = element.CallerIP;
                }
            }
        }

        public void Insert(PayPalTransactions obj)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                context.PayPalTransactions.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        public void Update(PayPalTransactions obj)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.PayPalTransactions
                               where h.IdTransaction.Equals(obj.IdTransaction)
                               select h).SingleOrDefault();

                if (element != null)
                {
                    //Util.Map(obj, element);
                    element.IdTransaction = obj.IdTransaction;
                    element.IdHotel = obj.IdHotel;
                    element.IdBillingType = obj.IdBillingType;
                    element.Price = obj.Price;
                    element.Status = obj.Status;
                    element.Email = obj.Email;
                    element.CreateDate = obj.CreateDate;
                    element.UpdateDate = obj.UpdateDate;
                    element.NameClient = obj.NameClient;
                    element.MAC = obj.MAC;
                    element.InvoiceNo = obj.InvoiceNo;
                    element.OS = obj.OS;
                    element.UserName = obj.UserName;
                    element.Password = obj.Password;
                    element.Survey = obj.Survey;
                    element.CallerIP = obj.CallerIP;
                }

                context.SubmitChanges();
            }
        }

        #endregion

        #region Loyalty

        public void Select(Loyalties2 obj, int idLoyalty)
        {
            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var element = (from h in context.Loyalties2
                               where h.IdLoyalty.Equals(idLoyalty)
                               select h).SingleOrDefault();

                if (element != null)
                {
                    //   Util.Map(element, obj);
                    obj.IdHotel = element.IdHotel;
                    obj.IdLoyalty = element.IdLoyalty;
                    obj.Name = element.Name;
                    obj.Surname = element.Surname;
                    obj.Email = element.Email;
                    obj.Date = element.Date;
                    obj.Survey = element.Survey;
                }
            }
        }

        public void Insert(Loyalties2 obj)
        {

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                context.Loyalties2.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        public void Update(Loyalties2 obj)
        {
            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var element = (from h in context.Loyalties2
                               where h.IdLoyalty.Equals(obj.IdLoyalty)
                               select h).SingleOrDefault();

                if (element != null)
                {
                    element.IdLoyalty = obj.IdLoyalty;
                    element.IdHotel = obj.IdHotel;
                    element.Name = obj.Name;
                    element.Surname = obj.Surname;
                    element.Email = obj.Email;
                    element.Date = obj.Date;
                    element.Survey = obj.Survey;

                }

                context.SubmitChanges();
            }
        }

        public List<Loyalties2> SelectLoyalties(int idHotel, DateTime start, DateTime finish, int pageIndex, int pageSize, ref int pageNumber)
        {

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var elements = (from h in context.Loyalties2
                                where (h.IdHotel.Equals(idHotel) && h.Date >= start && h.Date < finish)
                                select h);

                int count = elements.Count();
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                elements = elements.Skip((pageIndex) * pageSize).Take(pageSize);

                List<Loyalties2> listado = new List<Loyalties2>();

                foreach (var element in elements)
                {
                    Loyalties2 obj = new Loyalties2();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<Loyalties2> SelectLoyalties(string listadoHoteles, DateTime start, DateTime finish, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                string[] ids = listadoHoteles.Split(',');
                int count = 0;
                List<Loyalties2> listado = new List<Loyalties2>();
                foreach (string id in ids)
                {
                    int idHotel = int.Parse(id);
                    var elements = (from h in context.Loyalties2
                                    where (h.IdHotel.Equals(idHotel) && h.Date >= start && h.Date < finish)
                                    select h);

                    count += elements.Count();

                    foreach (var element in elements)
                    {
                        Loyalties2 obj = new Loyalties2();
                        Util.Map(element, obj);
                        listado.Add(obj);
                    }
                }

                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                return listado.OrderBy(a=>a.Date).Skip((pageIndex) * pageSize).Take(pageSize).ToList();
            }
        }

        public List<Loyalties2> SelectLoyaltiesLocation(int idLocation, DateTime start, DateTime finish, int pageIndex, int pageSize, ref int pageNumber)
        {

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var elements = (from h in context.Loyalties2
                                where (h.IdLocatin.Equals(idLocation) && h.Date >= start && h.Date < finish)
                                select h);

                int count = elements.Count();
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                elements = elements.Skip((pageIndex) * pageSize).Take(pageSize);

                List<Loyalties2> listado = new List<Loyalties2>();

                foreach (var element in elements)
                {
                    Loyalties2 obj = new Loyalties2();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<Loyalties2> SelectLoyalties(int idHotel, DateTime start, DateTime finish)
        {

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var elements = (from h in context.Loyalties2
                                where (h.IdHotel.Equals(idHotel) && h.Date >= start && h.Date < finish)
                                select h);

                List<Loyalties2> listado = new List<Loyalties2>();

                foreach (var element in elements)
                {
                    Loyalties2 obj = new Loyalties2();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<Loyalties2> SelectLoyalties(string listhoteles, DateTime start, DateTime finish)
        {

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                string[] ids = listhoteles.Split(',');
                List<Loyalties2> listado = new List<Loyalties2>();
                foreach (string id in ids)
                {
                    int idHotel = int.Parse(id);
                    var elements = (from h in context.Loyalties2
                                    where (h.IdHotel.Equals(idHotel) && h.Date >= start && h.Date < finish)
                                    select h);

                    foreach (var element in elements)
                    {
                        Loyalties2 obj = new Loyalties2();
                        Util.Map(element, obj);
                        listado.Add(obj);
                    }
                }

                return listado;
            }
        }

        public List<Loyalties2> SelectLoyaltiesLocation(int idLocation, DateTime start, DateTime finish)
        {

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var elements = (from h in context.Loyalties2
                                where (h.IdLocatin.Equals(idLocation) && h.Date >= start && h.Date < finish)
                                select h);

                List<Loyalties2> listado = new List<Loyalties2>();

                foreach (var element in elements)
                {
                    Loyalties2 obj = new Loyalties2();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<Loyalties2> SelectAllLoyaltiesLocation(int idLocation)
        {
            try { 
            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var elements = (from h in context.Loyalties2
                                where h.IdLocatin.Equals(idLocation)
                                select h);

                List<Loyalties2> listado = new List<Loyalties2>();

                foreach (var element in elements)
                {
                    Loyalties2 obj = new Loyalties2();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
            }
            catch(Exception ex)            
            {
                return null;
            }
        }

        public List<Loyalties2> SelectLoyalties(int idHotel, int pageIndex, int pageSize, ref int pageNumber)
        {

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var elements = (from h in context.Loyalties2
                                where h.IdHotel.Equals(idHotel)
                                select h);

                int count = elements.Count();
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                elements = elements.Skip((pageIndex) * pageSize).Take(pageSize);

                List<Loyalties2> listado = new List<Loyalties2>();

                foreach (var element in elements)
                {
                    Loyalties2 obj = new Loyalties2();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<Loyalties2> SelectLoyaltiesLocation(int idLocation, int pageIndex, int pageSize, ref int pageNumber)
        {

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var elements = (from h in context.Loyalties2
                                where h.IdLocatin.Equals(idLocation)
                                select h);

                int count = elements.Count();
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                elements = elements.Skip((pageIndex) * pageSize).Take(pageSize);

                List<Loyalties2> listado = new List<Loyalties2>();

                foreach (var element in elements)
                {
                    Loyalties2 obj = new Loyalties2();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<Loyalties2> SelectLoyaltiesMACLocation(string mac, int idLocation)
        {

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var elements = (from h in context.Loyalties2
                                where h.CallerId.Equals(mac) && h.IdLocatin.Equals(idLocation)
                                orderby h.Date descending
                                select h);

                List<Loyalties2> listado = new List<Loyalties2>();

                foreach (var element in elements)
                {
                    Loyalties2 obj = new Loyalties2();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }


        #endregion

        #region CaptivePortalLog && CaptivePortalLogInternal

        public void Insert(CaptivePortalLog obj)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                context.CaptivePortalLog.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        public List<CaptivePortalLogInfo> ObtainCaptivePortalLog(int idHotel, DateTime start, DateTime end, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var count = context.SP_FDS_OBTAINCAPTIVELOG(idHotel, start, end).Count();
                var elements = context.SP_FDS_OBTAINCAPTIVELOG(idHotel, start, end).Skip((pageIndex) * pageSize).Take(pageSize);

                List<CaptivePortalLogInfo> list = new List<CaptivePortalLogInfo>();

                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                foreach (var q in elements)
                {
                    CaptivePortalLogInfo obj = new CaptivePortalLogInfo();
                    Util.Map(q, obj);
                    list.Add(obj);
                }

                return list;
            }
        }

        public List<CaptivePortalLogInfo> ObtainCaptivePortalLog(string listhoteles, DateTime start, DateTime end, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                string[] ids = listhoteles.Split(',');
                List<CaptivePortalLogInfo> list = new List<CaptivePortalLogInfo>();
                int count = 0;
                foreach (string id in ids)
                {
                    count += context.SP_FDS_OBTAINCAPTIVELOG(int.Parse(id), start, end).Count();
                    var elements = context.SP_FDS_OBTAINCAPTIVELOG(int.Parse(id), start, end);

                    foreach (var q in elements)
                    {
                        CaptivePortalLogInfo obj = new CaptivePortalLogInfo();
                        Util.Map(q, obj);
                        list.Add(obj);
                    }
                }
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                return list.Skip((pageIndex) * pageSize).Take(pageSize).ToList();
            }
        }

        public List<CaptivePortalLogInfo> ObtainCaptivePortalLogLocation(int idLocation, DateTime start, DateTime end, int pageIndex, int pageSize, ref int pageNumber)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var count = context.SP_FDS_OBTAINCAPTIVELOGLOCATION(idLocation, start, end).Count();
                var elements = context.SP_FDS_OBTAINCAPTIVELOGLOCATION(idLocation, start, end).Skip((pageIndex) * pageSize).Take(pageSize);

                List<CaptivePortalLogInfo> list = new List<CaptivePortalLogInfo>();

                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);

                foreach (var q in elements)
                {
                    CaptivePortalLogInfo obj = new CaptivePortalLogInfo();
                    Util.Map(q, obj);
                    list.Add(obj);
                }

                return list;
            }
        }

        public void Insert(CaptivePortalLogInternal obj)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                context.CaptivePortalLogInternal.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        #endregion

        #region DashBoard

        public int BWUsed(int idhotel)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = context.SP_FDS_OBTAIN_BW_USED(idhotel);

                return (int)element;
            }
        }

        public int BWUsedLocation(int idLocation)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = context.SP_FDS_OBTAIN_BW_USEDLOCATION(idLocation);

                return (int)element;
            }
        }

        public int BWUsedDown(int idhotel)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = context.SP_FDS_OBTAIN_BW_USED_DOWNLOCATION(idhotel);

                return (int)element;
            }
        }

        public int BWUsedDownLocation(int idLocation)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = context.SP_FDS_OBTAIN_BW_USED_DOWN(idLocation);

                return (int)element;
            }
        }

        public double AveraUserUsage(int idHotel)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = context.SP_360_USERSUSAGEAVERAGE(idHotel);

                return Double.Parse(element.ToString());
            }
        }

        public double AveraUserUsageLocation(int idLocation)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = context.SP_360_USERSUSAGEAVERAGELOCATION(idLocation);

                return Double.Parse(element.ToString());
            }
        }

        #endregion

        #region Languages

        public void Select(Languages obj, string code)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.Languages
                                where h.Code.Equals(code)
                                select h).SingleOrDefault();

                if (element != null)
                {
                    obj.IdLanguage = element.IdLanguage;
                    obj.Code = element.Code;
                    obj.Name = element.Name;
                }

            }
        }

        public void Select(Languages obj, int idlanguaje)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.Languages
                               where h.IdLanguage.Equals(idlanguaje)
                               select h).SingleOrDefault();

                if (element != null)
                {
                    obj.IdLanguage = element.IdLanguage;
                    obj.Code = element.Code;
                    obj.Name = element.Name;
                }

            }
        }

        public List<Languages> SelectLanguages()
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.Languages
                                select h);

                List<Languages> listado = new List<Languages>();

                foreach (var element in elements)
                {
                    Languages obj = new Languages();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        #endregion

        #region Disclaimer 

        public void Select(Disclaimers obj, int idDisclaimer)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.Disclaimers
                               where h.IdDisclaimer.Equals(idDisclaimer)
                               select h).SingleOrDefault();

                if (element != null)
                {
                    obj.IdDisclaimer = element.IdDisclaimer;
                    obj.IdLanguage = element.IdLanguage;
                    obj.IdHotel = element.IdHotel;
                    obj.Text = element.Text;
                    obj.IdLocation = element.IdLocation;
                    obj.Date = element.Date;
                    obj.IdType = element.IdType;
                }

            }
        }

        public void Select(Disclaimers obj, int idhotel, int idlanguage)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.Disclaimers
                               where h.IdLanguage.Equals(idlanguage) && h.IdHotel.Equals(idhotel)
                               select h).SingleOrDefault();

                if (element != null)
                {
                    obj.IdDisclaimer = element.IdDisclaimer;
                    obj.IdLanguage = element.IdLanguage;
                    obj.IdHotel = element.IdHotel;
                    obj.Text = element.Text;
                    obj.IdLocation = element.IdLocation;
                    obj.IdType = element.IdType;
                }

            }
        }

        public void Select(Disclaimers obj, int idhotel, int idlocation, int idlanguage)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.Disclaimers
                               where h.IdLanguage.Equals(idlanguage) && h.IdHotel.Equals(idhotel) && h.IdLocation.Equals(idlocation)
                               orderby h.Date descending
                               select h).FirstOrDefault();

                if (element != null)
                {
                    obj.IdDisclaimer = element.IdDisclaimer;
                    obj.IdLanguage = element.IdLanguage;
                    obj.IdHotel = element.IdHotel;
                    obj.Text = element.Text;
                    obj.IdLocation = element.IdLocation;
                    obj.IdType = element.IdType;
                }

            }
        }

        public void Select(Disclaimers obj, int idhotel, int idlocation, int idlanguage, int idType)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.Disclaimers
                               where h.IdLanguage.Equals(idlanguage) && h.IdHotel.Equals(idhotel) && h.IdLocation.Equals(idlocation) && h.IdType.Equals(idType)
                               orderby h.Date descending
                               select h).FirstOrDefault();

                if (element != null)
                {
                    obj.IdDisclaimer = element.IdDisclaimer;
                    obj.IdLanguage = element.IdLanguage;
                    obj.IdHotel = element.IdHotel;
                    obj.Text = element.Text;
                    obj.IdLocation = element.IdLocation;
                    obj.IdType = element.IdType;
                }

            }
        }

        /// <summary>
        /// Insertamos un nuevo hotel en la base de datos
        /// </summary>
        /// <param name="obj">Elemento del tipo Hotel a insertar</param>
        public void Insert(Disclaimers obj)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                context.Disclaimers.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        /// <summary>
        /// Actualizamos los datos de un hotel existente
        /// </summary>
        /// <param name="obj">Elemento del tipo Hotel con los datos a actualizar</param>
        public void Update(Disclaimers obj)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.Disclaimers
                               where h.IdDisclaimer.Equals(obj.IdDisclaimer)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(obj, element);

                context.SubmitChanges();
            }
        }

        public List<Disclaimers> SelectDisclaimers(int idLocation)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.Disclaimers
                                         where h.IdLocation.Equals(idLocation)
                                         select h);

                List<Disclaimers> listado = new List<Disclaimers>();

                foreach (var element in elements)
                {
                    Disclaimers obj = new Disclaimers();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        #endregion

        #region fastticket

        /// <summary>
        /// Insertamos un nuevo FastTicket en la base de datos esperando a que el radius lo actualice
        /// </summary>
        /// <param name="obj">Elemento del tipo FastTocket a insertar</param>
        public void Insert(FastTicket obj)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                context.FastTicket.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        #endregion

        #region surveys

        /// <summary>
        /// Insertamos un nuevo Surveys en la base de datos
        /// </summary>
        /// <param name="obj">Elemento del tipo Surveys a insertar</param>
        public void Insert(Surveys2 obj)
        {

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                context.Surveys2.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        public void Update(Surveys2 obj)
        {
            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var element = (from h in context.Surveys2
                               where h.IdSurvey.Equals(obj.IdSurvey)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(obj, element);

                context.SubmitChanges();
            }
        }

        public void Select(Surveys2 obj, int idSurvey)
        {

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var element = (from h in context.Surveys2
                               where h.IdSurvey.Equals(idSurvey)
                               select h).SingleOrDefault();

                if (element != null)
                {
                    Util.Map(element, obj);
                }

            }
        }

        public void Delete(Surveys2 obj)
        {
            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var element = (from h in context.Surveys2
                                where h.IdSurvey.Equals(obj.IdSurvey)
                                select h).SingleOrDefault();

                if (element != null)
                    context.Surveys2.DeleteOnSubmit(element);
                context.SubmitChanges();
            }
        }

        public List<Surveys2> SelectSurvey(int idHotel)
        {
            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var elements = (from h in context.Surveys2
                                where h.IdHotel.Equals(idHotel) && h.IdLocation.Equals(0)
                                select h);

                List<Surveys2> listado = new List<Surveys2>();

                foreach (var element in elements)
                {
                    Surveys2 obj = new Surveys2();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<Surveys2> SelectSurveyLocation(int idLocation)
        {
            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var elements = (from h in context.Surveys2
                                where h.IdLocation.Equals(idLocation)
                                select h);

                List<Surveys2> listado = new List<Surveys2>();

                foreach (var element in elements)
                {
                    Surveys2 obj = new Surveys2();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<Surveys2> SelectSurvey(int idHotel, int idlanguage)
        {

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var elements = (from h in context.Surveys2
                                where h.IdHotel.Equals(idHotel) && h.IdLanguage.Equals(idlanguage) && h.IdLocation.Equals(0)
                                select h);

                List<Surveys2> listado = new List<Surveys2>();

                foreach (var element in elements)
                {
                    Surveys2 obj = new Surveys2();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<Surveys2> SelectSurveyLocation(int idLocation, int idlanguage)
        {

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var elements = (from h in context.Surveys2
                                where h.IdLocation.Equals(idLocation) && h.IdLanguage.Equals(idlanguage)
                                select h);

                List<Surveys2> listado = new List<Surveys2>();

                foreach (var element in elements)
                {
                    Surveys2 obj = new Surveys2();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        #endregion

        #region UserAgents

        public void Insert(UserAgents obj)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                context.UserAgents.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        public void Update(UserAgents obj)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.UserAgents
                               where h.IdUserAgent.Equals(obj.IdUserAgent)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(obj, element);

                context.SubmitChanges();
            }
        }

        public void Select(UserAgents obj, int idUserAgent)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.UserAgents
                               where h.IdUserAgent.Equals(idUserAgent)
                               select h).FirstOrDefault();

                if (element != null)
                {
                    //   Util.Map(element, obj);
                    obj.IdUserAgent= element.IdUserAgent;
                    obj.Attemps = element.Attemps;
                    obj.First = element.First;
                    obj.Last = element.Last;
                    obj.String = element.String;
                    obj.Valid = element.Valid;
                }
            }
        }

        public void ActualizarTransaccionado(UserAgents obj, string cadena)
        {
            //
            try
            {
                using (SqlConnection connection = new SqlConnection(Util.ConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "SP_CAPTIVE_UPDATEUSERAGENT_WITH_TABLOCKX";
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@string", cadena);
                        obj.IdUserAgent = (int)command.ExecuteScalar();
                    }

                    connection.Close();

                }
            }
            catch (Exception ex)
            {
                throw new System.ArgumentException(ex.Message);
            }
        }

        public void Select(UserAgents obj, string cadena)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.UserAgents
                               where h.String.Equals(cadena)
                               select h).FirstOrDefault();

                if (element != null)
                {
                    //   Util.Map(element, obj);
                    obj.IdUserAgent = element.IdUserAgent;
                    obj.Attemps = element.Attemps;
                    obj.First = element.First;
                    obj.Last = element.Last;
                    obj.String = element.String;
                    obj.Valid = element.Valid;
                }
            }
        }

        public void Insert(UserAgentsTemp obj)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                context.UserAgentsTemp.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        public void Select(UserAgentsTemp obj, string cadena)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.UserAgentsTemp
                               where h.MAC.Contains(cadena)
                               orderby h.IdTemp descending
                               select h).FirstOrDefault();

                if (element != null)
                {
                    //   Util.Map(element, obj);
                    obj.IdUserAgent = element.IdUserAgent;
                    obj.IdTemp = element.IdTemp;
                    obj.MAC = element.MAC;
                }
            }
        }

        public void Delete(UserAgentsTemp obj)
        {
            //using (DataModel context = new DataModel(Util.ConnectionString()))
            //{
            //    var elements = (from h in context.UserAgentsTemp
            //                   where h.MAC.Equals(obj.MAC)
            //                   select h);

            //    if (elements != null)
            //        foreach (var element in elements)
            //        {
            //            context.UserAgentsTemp.DeleteOnSubmit(element);
            //        }
            //    context.SubmitChanges();
            //}
            ;
        }

        public int SelectUserAgentTemps(string mac, int interval)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.UserAgentsTemp
                                where h.MAC.Equals(mac) && (h.AttemptTime > DateTime.Now.AddSeconds(-interval))
                                select h);

                return elements.Count();
            }
        }

        public List<UserAgents> SelectUserAgentsToBlackList(int max)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                List<UserAgents> list = (from h in context.UserAgents
                                         where h.Valid.Equals(0) && h.Attemps >= max
                                         select h).ToList();

                return list;
            }
        }

        public List<UserAgents> SelectUserAgents(string cadena)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                List<UserAgents> list = (from h in context.UserAgents
                                         where h.String.Equals(cadena)
                                         select h).ToList();

                return list;
            }
        }

        #endregion 

        #region BlackListUserAgents

        public void Insert(BlackListUserAgents obj)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                context.BlackListUserAgents.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        public void Select(BlackListUserAgents obj, string cadena)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.BlackListUserAgents
                               where h.String.Equals(cadena)
                               select h).FirstOrDefault();

                if (element != null)
                {
                    obj.IdBlackUserAgent = element.IdBlackUserAgent;
                    obj.String = element.String;
                }
            }
        }

        #endregion 

        #region BlackListMacs

        public void Insert(BlackListMACs obj)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                context.BlackListMACs.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        public void Select(BlackListMACs obj, string mac, DateTime now)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.BlackListMACs
                               where (h.MAC.Equals(mac) && h.BanningEnd > now)
                               select h).FirstOrDefault();

                if (element != null)
                {
                    obj.IdMACBlackList = element.IdMACBlackList;
                    obj.MAC = element.MAC;
                    obj.IdHotel = element.IdHotel;
                    obj.BanningStart = element.BanningStart;
                    obj.BanningEnd = element.BanningEnd;
                }
            }
        }

        public void DeleteMACBlackList(int idMAC)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.BlackListMACs
                                where h.IdMACBlackList.Equals(idMAC)
                                select h);

                if (elements != null)
                    foreach (var element in elements)
                    {
                        context.BlackListMACs.DeleteOnSubmit(element);
                    }
                context.SubmitChanges();
            }
        }

        public List<BlackListMACs> SelectBlackListMACs(int idhotel, DateTime start, DateTime end, int pageIndex, int pageSize, ref int pageNumber)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.BlackListMACs
                                where h.IdHotel.Equals(idhotel) && h.BanningStart >= start
                                select h);

                int count = elements.Count();
                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                elements = elements.OrderBy(a => a.BanningStart).Skip((pageIndex) * pageSize).Take(pageSize);
                List<BlackListMACs> listado = new List<BlackListMACs>();

                foreach (var element in elements)
                {
                    BlackListMACs obj = new BlackListMACs();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<BlackListMACs> SelectBlackListMACs(string listadoHoteles, DateTime start, DateTime end, int pageIndex, int pageSize, ref int pageNumber)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                int count = 0;
                string[] ids = listadoHoteles.Split(',');
                List<BlackListMACs> listado = new List<BlackListMACs>();
                foreach (string id in ids)
                {
                    int idhotel = int.Parse(id);
                    var elements = (from h in context.BlackListMACs
                                    where h.IdHotel.Equals(idhotel) && h.BanningStart >= start
                                    select h);

                    count += elements.Count();
                    foreach (var element in elements)
                    {
                        BlackListMACs obj = new BlackListMACs();
                        Util.Map(element, obj);
                        listado.Add(obj);
                    }
                }

                pageNumber = ((count % pageSize) == 0 ? count / pageSize : (count / pageSize) + 1);
                return listado.OrderBy(a => a.BanningStart).Skip((pageIndex) * pageSize).Take(pageSize).ToList();
            }
        }

        #endregion 

        #region LocationsTexts

        public void Select(LocationsTexts obj, int idLocationText)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.LocationsTexts
                               where h.IdLocationText.Equals(idLocationText)
                               select h).SingleOrDefault();

                if (element != null)
                {
                    obj.IdLocationText = element.IdLocationText;
                    obj.IdLanguage = element.IdLanguage;
                    obj.IdLocation = element.IdLocation;
                    obj.Text = element.Text;
                }

            }
        }
        
        public void Select(LocationsTexts obj, int idlocation, int idlanguage)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.LocationsTexts
                               where h.IdLocation.Equals(idlocation) && h.IdLanguage.Equals(idlanguage)
                               select h).SingleOrDefault();

                if (element != null)
                {
                    obj.IdLocationText = element.IdLocationText;
                    obj.IdLanguage = element.IdLanguage;
                    obj.IdLocation = element.IdLocation;
                    obj.Text = element.Text;
                }

            }
        }

        /// <summary>
        /// Insertamos un nuevo hotel en la base de datos
        /// </summary>
        /// <param name="obj">Elemento del tipo Hotel a insertar</param>
        public void Insert(LocationsTexts obj)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                context.LocationsTexts.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        /// <summary>
        /// Actualizamos los datos de un hotel existente
        /// </summary>
        /// <param name="obj">Elemento del tipo Hotel con los datos a actualizar</param>
        public void Update(LocationsTexts obj)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.LocationsTexts
                               where h.IdLocationText.Equals(obj.IdLocationText)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(obj, element);

                context.SubmitChanges();
            }
        }

        public List<LocationsTexts> SelectLocationsText(int idLocation)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.LocationsTexts
                                where h.IdLocation.Equals(idLocation)
                                select h);

                List<LocationsTexts> listado = new List<LocationsTexts>();

                foreach (var element in elements)
                {
                    LocationsTexts obj = new LocationsTexts();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        #endregion

        #region LocationModuleTexts

        public void Select(LocationModuleText obj, int idlocationModuleText)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.LocationModuleText
                               where h.IdLocationModuleText.Equals(idlocationModuleText)
                               select h).SingleOrDefault();

                if (element != null)
                {
                    Util.Map(element, obj);
                }

            }
        }
        
        public void Select(LocationModuleText obj, int idlocation, int idBillingModule, int idlanguage)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.LocationModuleText
                               where h.IdLocation.Equals(idlocation) && h.IdBillingModule.Equals(idBillingModule) && h.IdLanguage.Equals(idlanguage)
                               select h).SingleOrDefault();

                if (element != null)
                {
                    Util.Map(element, obj);
                }

            }
        }

        /// <summary>
        /// Insertamos un nuevo hotel en la base de datos
        /// </summary>
        /// <param name="obj">Elemento del tipo Hotel a insertar</param>
        public void Insert(LocationModuleText obj)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                context.LocationModuleText.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        /// <summary>
        /// Actualizamos los datos de un hotel existente
        /// </summary>
        /// <param name="obj">Elemento del tipo Hotel con los datos a actualizar</param>
        public void Update(LocationModuleText obj)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.LocationModuleText
                               where h.IdLocationModuleText.Equals(obj.IdLocationModuleText)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(obj, element);

                context.SubmitChanges();
            }
        }


        public List<LocationModuleText> SelectLocationModuleTexts(int idlocation)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.LocationModuleText
                                where h.IdLocation.Equals(idlocation)
                                select h);

                List<LocationModuleText> listado = new List<LocationModuleText>();

                foreach (var element in elements)
                {
                    LocationModuleText obj = new LocationModuleText();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<LocationModuleText> SelectLocationModuleTexts(int idlocation, int idlanguage)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.LocationModuleText
                                where h.IdLocation.Equals(idlocation) && h.IdLanguage.Equals(idlanguage)
                                select h);

                List<LocationModuleText> listado = new List<LocationModuleText>();

                foreach (var element in elements)
                {
                    LocationModuleText obj = new LocationModuleText();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }
        

        #endregion

        #region FacebookPromotions

        public void Select(FacebookPromotions obj, int idpromotion)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.FacebookPromotions
                               where h.IdFacebookPromotion.Equals(idpromotion)
                               select h).SingleOrDefault();

                if (element != null)
                {
                    Util.Map(element, obj);
                }

            }
        }

        /// <summary>
        /// Insertamos un nuevo hotel en la base de datos
        /// </summary>
        /// <param name="obj">Elemento del tipo Hotel a insertar</param>
        public void Insert(FacebookPromotions obj)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                context.FacebookPromotions.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        /// <summary>
        /// Actualizamos los datos de un hotel existente
        /// </summary>
        /// <param name="obj">Elemento del tipo Hotel con los datos a actualizar</param>
        public void Update(FacebookPromotions obj)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.FacebookPromotions
                               where h.IdFacebookPromotion.Equals(obj.IdFacebookPromotion)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(obj, element);

                context.SubmitChanges();
            }
        }

        public List<FacebookPromotions> SelectFacebookPromotions(int idhotel)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.FacebookPromotions
                                where h.IdHotel.Equals(idhotel) orderby h.Order, h.IdLocation
                                select h);

                List<FacebookPromotions> listado = new List<FacebookPromotions>();

                foreach (var element in elements)
                {
                    FacebookPromotions obj = new FacebookPromotions();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<FacebookPromotions> SelectFacebookPromotions(int idhotel, int idlocation)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.FacebookPromotions
                                where h.IdHotel.Equals(idhotel) && h.IdLocation.Equals(idlocation) orderby h.Order
                                select h);

                List<FacebookPromotions> listado = new List<FacebookPromotions>();

                foreach (var element in elements)
                {
                    FacebookPromotions obj = new FacebookPromotions();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<FacebookPromotions> SelectFacebookPromotions(int idhotel, int idlocation, bool visible)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.FacebookPromotions
                                where h.IdHotel.Equals(idhotel) && h.IdLocation.Equals(idlocation) && h.Visible.Equals(visible) && h.CreationDate < DateTime.Now && h.EndDate > DateTime.Now
                                orderby h.Order
                                select h);

                List<FacebookPromotions> listado = new List<FacebookPromotions>();

                foreach (var element in elements)
                {
                    FacebookPromotions obj = new FacebookPromotions();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        #endregion

        #region FacebookPromotionsLog

        public void Select(FacebookPromotionsLog obj, int idpromotionlog)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.FacebookPromotionsLog
                               where h.IdPromotionLog.Equals(idpromotionlog)
                               select h).SingleOrDefault();

                if (element != null)
                {
                    Util.Map(element, obj);
                }

            }
        }

        public void Select(FacebookPromotionsLog obj, int idpromotion, string mail)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.FacebookPromotionsLog
                               where h.IdPromocion.Equals(idpromotion) && h.Email.Equals(mail)
                               select h).SingleOrDefault();

                if (element != null)
                {
                    Util.Map(element, obj);
                }

            }
        }

        /// <summary>
        /// Insertamos un nuevo hotel en la base de datos
        /// </summary>
        /// <param name="obj">Elemento del tipo Hotel a insertar</param>
        public void Insert(FacebookPromotionsLog obj)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                context.FacebookPromotionsLog.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        /// <summary>
        /// Actualizamos los datos de un hotel existente
        /// </summary>
        /// <param name="obj">Elemento del tipo Hotel con los datos a actualizar</param>
        public void Update(FacebookPromotionsLog obj)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.FacebookPromotionsLog
                               where h.IdPromotionLog.Equals(obj.IdPromotionLog)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(obj, element);

                context.SubmitChanges();
            }
        }

        public List<FacebookPromotionsLog> SelectFacebookPromotionsLog(int idpromotion)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.FacebookPromotionsLog
                                where h.IdPromocion.Equals(idpromotion)
                                select h);

                List<FacebookPromotionsLog> listado = new List<FacebookPromotionsLog>();

                foreach (var element in elements)
                {
                    FacebookPromotionsLog obj = new FacebookPromotionsLog();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<FacebookPromotionsLog> SelectFacebookPromotionsLog(string mail)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.FacebookPromotionsLog
                                where h.Email.Equals(mail)
                                select h);

                List<FacebookPromotionsLog> listado = new List<FacebookPromotionsLog>();

                foreach (var element in elements)
                {
                    FacebookPromotionsLog obj = new FacebookPromotionsLog();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        #endregion

        #region CustomAccess

        public void Select(CustomAccess obj, int idHotel)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.CustomAccess
                               where h.IdHotel.Equals(idHotel)
                               select h).SingleOrDefault();

                if (element != null)
                {
                    Util.Map(element, obj);
                }

            }
        }

        #endregion

        #region AppleUrls

        public void Insert(AppleUrls obj)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                context.AppleUrls.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        #endregion 

        #region RememberMeTable

        public void Select(RememberMeTable obj, string mac, int idhotel)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.RememberMeTable
                               where h.IdHotel.Equals(idhotel) && h.CallerID.ToUpper().Equals(mac.ToUpper())
                               orderby h.IdRememberMe descending
                               select h).FirstOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        public void DeleteRememberMeTable(int id)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.RememberMeTable
                                where h.IdRememberMe.Equals(id)
                                select h);

                if (elements != null)
                    foreach (var element in elements)
                    {
                        context.RememberMeTable.DeleteOnSubmit(element);
                    }
                context.SubmitChanges();
            }
        }

        public void DeleteRememberMeTable(string mac, int idhotel)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.RememberMeTable
                                where h.IdHotel.Equals(idhotel) && h.CallerID.Equals(mac)
                                select h);

                if (elements != null)
                    foreach (var element in elements)
                    {
                        context.RememberMeTable.DeleteOnSubmit(element);
                    }
                context.SubmitChanges();
            }
        }

        #endregion

        #region ParametrosConfiguración

        public void Insert(ParametrosConfiguracion obj)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                context.ParametrosConfiguracion.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        public void Update(ParametrosConfiguracion obj)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.ParametrosConfiguracion
                               where h.IdParameter.Equals(obj.IdParameter)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(obj, element);

                context.SubmitChanges();
            }
        }

        public void Select(ParametrosConfiguracion obj, int idParameter)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.ParametrosConfiguracion
                               where h.IdParameter.Equals(idParameter)
                               select h).FirstOrDefault();

                if (element != null)
                {
                    //   Util.Map(element, obj);
                    obj.IdParameter = element.IdParameter;
                    obj.IdSite = element.IdSite;
                    obj.Key = element.Key;
                    obj.Description = element.Description;
                    obj.Type = element.Type;
                    obj.value = element.value;
                }
            }
        }

        public void Select(ParametrosConfiguracion obj, string value)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.ParametrosConfiguracion
                               where h.value.Equals(value)
                               select h).FirstOrDefault();

                if (element != null)
                {
                    //   Util.Map(element, obj);
                    obj.IdParameter = element.IdParameter;
                    obj.IdSite = element.IdSite;
                    obj.Key = element.Key;
                    obj.Description = element.Description;
                    obj.Type = element.Type;
                    obj.value = element.value;
                }
            }
        }

        public void Delete(ParametrosConfiguracion obj)
        {
            //using (DataModel context = new DataModel(Util.ConnectionString()))
            //{
            //    var elements = (from h in context.UserAgentsTemp
            //                   where h.MAC.Equals(obj.MAC)
            //                   select h);

            //    if (elements != null)
            //        foreach (var element in elements)
            //        {
            //            context.UserAgentsTemp.DeleteOnSubmit(element);
            //        }
            //    context.SubmitChanges();
            //}
            ;
        }

        public List<ParametrosConfiguracion> SelectParametrosConfiguracion(int idSite)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                List<ParametrosConfiguracion> list = (from h in context.ParametrosConfiguracion
                                         where h.IdSite.Equals(idSite)
                                         select h).ToList();

                return list;
            }
        }

        #endregion

        #region FilteringProfiles

        /// <summary>
        /// Insertamos un nuevo Filter en la base de datos
        /// </summary>
        /// <param name="obj">Elemento del tipo Filter a insertar</param>
        public void Insert(FilteringProfiles obj)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                context.FilteringProfiles.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        /// <summary>
        /// Actualizamos los datos de un Filter existente
        /// </summary>
        /// <param name="obj">Elemento del tipo Filter con los datos a actualizar</param>
        public void Update(FilteringProfiles obj)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.FilteringProfiles
                               where h.IdFilter.Equals(obj.IdFilter)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(obj, element);

                context.SubmitChanges();
            }
        }

        public void DeleteFilterProfile(int id)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.FilteringProfiles
                                where h.IdFilter.Equals(id)
                                select h);

                if (elements != null)
                foreach (var element in elements)
                {
                    context.FilteringProfiles.DeleteOnSubmit(element);
                }
                context.SubmitChanges();
            }
        }

        public void Select(FilteringProfiles obj, int idFilter)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.FilteringProfiles
                               where h.IdFilter.Equals(idFilter)
                               select h).FirstOrDefault();

                if (element != null)
                {
                    //   Util.Map(element, obj);
                    obj.IdFilter = element.IdFilter;
                    obj.IdSite = element.IdSite;
                    obj.Description = element.Description;
                }
            }
        }

        public List<FilteringProfiles> SelectFilteringProfiles(int idSite)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                List<FilteringProfiles> list = (from h in context.FilteringProfiles
                                                      where h.IdSite.Equals(idSite)
                                                      select h).ToList();

                return list;
            }
        }

        public List<FilteringProfiles> SelectFilteringProfiles(string listadoHoteles)
        {
            string[] ids = listadoHoteles.Split(',');
            List<FilteringProfiles> list = new List<FilteringProfiles>();

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                foreach (string id in ids)
                {
                    var elements = (from h in context.FilteringProfiles
                                    where h.IdSite.Equals(int.Parse(id))
                                    select h);

                    foreach(var element in elements)
                    {
                        FilteringProfiles obj = new FilteringProfiles();
                        Util.Map(elements, obj);
                        list.Add(obj);
                    }
                }
                return list;
            }
        }

        #endregion

        #region checkNumberOracle

        public void Insert(CheckNumberOracle obj)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                context.CheckNumberOracle.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        #endregion

        #region SocialNetworks

        public void Select(SocialNetworks obj, int id)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.SocialNetworks
                               where h.IdSocialNetwork.Equals(id)
                               select h).SingleOrDefault();

                if (element != null)
                {
                    obj.IdSocialNetwork = element.IdSocialNetwork;
                    obj.Name = element.Name;
                }

            }
        }

        public List<SocialNetworks> SelectSocialNetworks()
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.SocialNetworks
                                select h);

                List<SocialNetworks> listado = new List<SocialNetworks>();

                foreach (var element in elements)
                {
                    SocialNetworks obj = new SocialNetworks();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<LocationsSocialNetworks> SelectLocationsSocialNetworks(int idLocation)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.LocationsSocialNetworks
                               where h.IdLocation.Equals(idLocation)
                               select h);

                List<LocationsSocialNetworks> listado = new List<LocationsSocialNetworks>();

                foreach (var element in elements)
                {
                    LocationsSocialNetworks obj = new LocationsSocialNetworks();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public List<SitesSocialNetworks> SelectSitesSocialNetworks(int idSite)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.SitesSocialNetworks
                                where h.IdSite.Equals(idSite)
                                select h);

                List<SitesSocialNetworks> listado = new List<SitesSocialNetworks>();

                foreach (var element in elements)
                {
                    SitesSocialNetworks obj = new SitesSocialNetworks();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        public void Insert(SitesSocialNetworks obj)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                context.SitesSocialNetworks.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        public void Update(SitesSocialNetworks obj)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.SitesSocialNetworks
                               where h.IdSiteSocialNetwork.Equals(obj.IdSiteSocialNetwork)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(obj, element);

                context.SubmitChanges();
            }
        }

        public void Insert(LocationsSocialNetworks obj)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                context.LocationsSocialNetworks.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        public void Update(LocationsSocialNetworks obj)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.LocationsSocialNetworks
                               where h.IdLocationSocialNetwork.Equals(obj.IdLocationSocialNetwork)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(obj, element);

                context.SubmitChanges();
            }
        }


        #endregion

        #region SitePassAccess 

        public void Select(SitePassAccess obj, string mac, int idhotel, string pass)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.SitePassAccess
                               where h.MAC.Equals(mac) && h.IdHotel.Equals(idhotel) && h.Pass.Equals(pass)
                               select h).FirstOrDefault();

                if (element != null)
                {
                    Util.Map(element, obj);
                }
            }
        }

        public void Insert(SitePassAccess obj)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                context.SitePassAccess.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }


        #endregion

        #region RadiusCoa

        public List<RadiusCoa> SelectRadiusCoas()
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                List<RadiusCoa> list = (from h in context.RadiusCoa
                                                      where h.Flag.Equals(0)
                                                      select h).ToList();

                return list;
            }
        }

        public List<RadiusCoa> SelectRadiusCoasUnfinished()
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                List<RadiusCoa> list = (from h in context.RadiusCoa
                                        where !h.Flag.Equals(0) && h.Flag < 99
                                        select h).ToList();

                return list;
            }
        }

        public void Update(RadiusCoa obj)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.RadiusCoa
                               where h.IdRadiusCoa.Equals(obj.IdRadiusCoa)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(obj, element);

                context.SubmitChanges();
            }
        }

        #endregion

        #region CoA_Vendors

        public List<CoA_Vendors> SelectRadiusCoA_Vendors()
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                List<CoA_Vendors> list = (from h in context.CoA_Vendors
                                        select h).ToList();

                return list;
            }
        }

        public void Select(CoA_Vendors obj, int idCoAVendors)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.CoA_Vendors
                               where h.IdCoAVendors.Equals(idCoAVendors)
                               select h).SingleOrDefault();

                if (element != null)
                {
               //     Util.Map(obj, element);
                    obj.IdCoAVendors = element.IdCoAVendors;
                    obj.Name = element.Name;
                }

               // context.SubmitChanges();
            }
        }

        #endregion

        #region CoA_Types

        public List<CoA_Types> SelectRadiusCoA_Types()
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                List<CoA_Types> list = (from h in context.CoA_Types
                                        select h).ToList();

                return list;
            }
        }

        public void Select(CoA_Types obj, int idCoATypes)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.CoA_Types
                               where h.IdCoATypes.Equals(idCoATypes)
                               select h).SingleOrDefault();

                if (element != null)
                {
                  //  Util.Map(obj, element);
                    obj.IdCoATypes = element.IdCoATypes;
                    obj.Action = element.Action;
                }

           //     context.SubmitChanges();
            }
        }

        #endregion

        #region MerakiLogoutsUrl 

        public void Select(MerakiLogoutUrl obj, string mac, int idhotel)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.MerakiLogoutUrl
                               where h.MAC.Equals(mac) && h.Idhotel.Equals(idhotel)
                               select h).FirstOrDefault();

                if (element != null)
                {
                    Util.Map(element, obj);
                }
            }
        }

        public void Insert(MerakiLogoutUrl obj)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                context.MerakiLogoutUrl.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }


        #endregion

        #region Vendors

        public void Select(Vendors obj, int idVendor)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.Vendors
                               where h.IdVendor.Equals(idVendor)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        public void Insert(Vendors obj)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                context.Vendors.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

 
        public List<Vendors> SelectVendors()
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var elements = (from h in context.Vendors
                                select h);

                List<Vendors> listado = new List<Vendors>();

                foreach (var element in elements)
                {
                    Vendors obj = new Vendors();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }

        #endregion

        #region Currecies

        public void Select(Currencies2 obj, int id)
        {

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var element = (from h in context.Currencies2
                               where h.Id.Equals(id)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        public void Select(Currencies2 obj, string code)
        {

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var element = (from h in context.Currencies2
                               where h.Code.Equals(code)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        public void Insert(Currencies2 obj)
        {

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                context.Currencies2.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }

        public List<Currencies2> SelectCurrencies()
        {

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var elements = (from h in context.Currencies2
                                orderby h.Badge
                                select h);

                List<Currencies2> listado = new List<Currencies2>();

                foreach (var element in elements)
                {
                    Currencies2 obj = new Currencies2();
                    Util.Map(element, obj);
                    listado.Add(obj);
                }

                return listado;
            }
        }


        public void Update(Currencies2 obj)
        {
            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var element = (from h in context.Currencies2
                               where h.Id.Equals(obj.Id)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(obj, element);

                context.SubmitChanges();
            }
        }

        #endregion

        #region SessionOnLineCoordenades

        public List<UsersOnlineCoordinadesInfo> SessionOnLineCoordenades()
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                List<UsersOnlineCoordinadesInfo> list = new List<UsersOnlineCoordinadesInfo>();

                var elements = context.SP_WBB_ONLINEUSERSCOORDENADAS();

                foreach (var q in elements)
                {
                    UsersOnlineCoordinadesInfo obj = new UsersOnlineCoordinadesInfo();
                    Util.Map(q, obj);
                    obj.Longitude = q.Longitude;
                    obj.Latitude = q.Latitude;
                    obj.IdHotel = q.IdHotel;
                    obj.AcctSessionId = q.AcctSessionID;

                    list.Add(obj);
                }

                return list;
            }
        }

        #endregion

        #region SitesGMT

        public void Select(SitesGMT obj, int idSite)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.SitesGMT
                               where h.IdSite.Equals(idSite) && h.IdLocation.Equals(0)
                               select h).FirstOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        public void SelectbyLocation(SitesGMT obj, int idLocation)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.SitesGMT
                               where h.IdLocation.Equals(idLocation)
                               select h).FirstOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        public void SelectbyGroup(SitesGMT obj, int idGroup)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.SitesGMT
                               where h.IdGroup.Equals(idGroup)
                               select h).FirstOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        public void Insert(SitesGMT obj)
        {

            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                context.SitesGMT.InsertOnSubmit(obj);

                context.SubmitChanges();
            }
        }
        public void Update(SitesGMT obj)
        {
            using (DataModel context = new DataModel(Util.ConnectionString()))
            {
                var element = (from h in context.SitesGMT
                               where h.Id.Equals(obj.Id)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(obj, element);

                context.SubmitChanges();
            }
        }


        #endregion

        #region APITokens 

        public void Select(ApiTokens obj, string Token)
        {
            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var element = (from h in context.ApiTokens
                               where h.Token.Equals(Token)
                               select h).FirstOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        #endregion

        #region ErrorMessages 

        public void Select(ErrorMessages obj, string CodError, string CodLanguage )
        {
            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var element = (from h in context.ErrorMessages
                               where h.CodError.Equals(CodError) && h.CodLanguage.Equals(CodLanguage)
                               select h).FirstOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        #endregion

        #region ApiMerakiSplashLogins

        public void SelectLast(ApiMerakiSplashLogins obj)
        {

            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var element = (from h in context.ApiMerakiSplashLogins
                               orderby h.loginAt descending
                               select h).FirstOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        public void Select(ApiMerakiSplashLogins obj, string clientID, string clientMac, double loginAt)
        {

            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var element = (from h in context.ApiMerakiSplashLogins
                               where h.clientId.Equals(clientID) && h.clientMac.Equals(clientMac) && h.loginAt.Equals(loginAt)
                               select h).FirstOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        public void Select(ApiMerakiSplashLogins obj, string clientID, string clientMac, bool status)
        {

            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var element = (from h in context.ApiMerakiSplashLogins
                               where h.clientId.Equals(clientID) && h.clientMac.Equals(clientMac) && h.OnlineStatus.Equals(status)
                               select h).FirstOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        public void Insert(ApiMerakiSplashLogins obj)
        {
            try
            {
                using (DataModel2 context = new DataModel2(Util.ConnectionString()))
                {
                    context.ApiMerakiSplashLogins.InsertOnSubmit(obj);

                    context.SubmitChanges();
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        public void Update(ApiMerakiSplashLogins obj)
        {
            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var element = (from h in context.ApiMerakiSplashLogins
                               where h.Id.Equals(obj.Id)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(obj, element);

                context.SubmitChanges();
            }
        }

        public List<ApiMerakiSplashLogins> SelectApiMerakiSplashLogins()
        {
            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                List<ApiMerakiSplashLogins> list = new List<ApiMerakiSplashLogins>();

                var elements = (from h in context.ApiMerakiSplashLogins
                                select h).ToList();

                foreach (var q in elements)
                {
                    ApiMerakiSplashLogins obj = new ApiMerakiSplashLogins();
                    Util.Map(q, obj);

                    list.Add(obj);
                }

                return list;
            }
        }

        public List<ApiMerakiSplashLogins> SelectApiMerakiSplashLoginsbyStatus(bool online)
        {
            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                List<ApiMerakiSplashLogins> list = new List<ApiMerakiSplashLogins>();

                var elements = (from h in context.ApiMerakiSplashLogins
                                where h.OnlineStatus.Equals(online)
                                select h).ToList();

                foreach (var q in elements)
                {
                    ApiMerakiSplashLogins obj = new ApiMerakiSplashLogins();
                    Util.Map(q, obj);

                    list.Add(obj);
                }

                return list;
            }
        }
        #endregion

        #region MerakiLocationGPS

        public void Select(MerakiLocationsGPS obj, string NasMac)
        {

            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var element = (from h in context.MerakiLocationsGPS
                               where h.NAS_MAC.Equals(NasMac)
                               select h).FirstOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        public void SelectContains(MerakiLocationsGPS obj, string Mac)
        {

            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var element = (from h in context.MerakiLocationsGPS
                               where h.NAS_MAC.Contains(Mac)
                               select h).FirstOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        public List<MerakiLocationsGPS> SelectMerakiLocationsGPS(int idSite)
        {
            
            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var elements = (from h in context.MerakiLocationsGPS
                               where h.IdSite.Equals(idSite)
                               select h).ToList();

                List<MerakiLocationsGPS> list = new List<MerakiLocationsGPS>();

                foreach (var element in elements)
                {
                    MerakiLocationsGPS obj = new MerakiLocationsGPS();
                    Util.Map(element, obj);
                    list.Add(obj);
                }

                return list;
            }
        }

        #endregion

        #region ActiveSession

        public void Insert(ActiveSessions obj)
        {
            try
            {
                using (DataModel context = new DataModel(Util.ConnectionString()))
                {
                    context.ActiveSessions.InsertOnSubmit(obj);

                    context.SubmitChanges();
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteActiveSession(int id)
        {
            try
            {
                using (DataModel context = new DataModel(Util.ConnectionString()))
                {
                    var element = (from h in context.ActiveSessions
                                   where h.IdActiveSession.Equals(id)
                                   select h).FirstOrDefault();

                    context.ActiveSessions.DeleteOnSubmit(element);
                    context.SubmitChanges();
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }

        }

        public void Select(ActiveSessions obj, string clientMac, string actSessionId)
        {
            try
            {
                using (DataModel context = new DataModel(Util.ConnectionString()))
                {
                    var element = (from h in context.ActiveSessions
                                   where h.CallerID.Equals(clientMac) && h.AcctSessionID.Equals(actSessionId)
                                   select h).FirstOrDefault();

                    if (element != null)
                    {
                        Util.Map(element, obj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Select(ActiveSessions obj, string clientMac)
        {
            try
            {
                using (DataModel context = new DataModel(Util.ConnectionString()))
                {
                    var element = (from h in context.ActiveSessions
                                   where h.CallerID.Equals(clientMac)
                                   select h).FirstOrDefault();

                    if (element != null)
                    {
                        Util.Map(element, obj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ActiveSessions> ActiveSessionBySite(int idSite)
        {
            try
            {
                using (DataModel context = new DataModel(Util.ConnectionString()))
                {
                    List<ActiveSessions> list = new List<ActiveSessions>();

                    var elements = (from h in context.ActiveSessions
                                    where h.IdHotel.Equals(idSite)
                                    select h).ToList();

                    foreach (var q in elements)
                    {
                        ActiveSessions obj = new ActiveSessions();
                        Util.Map(q, obj);

                        list.Add(obj);
                    }

                    return list;
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public List<ActiveSessions> ActiveSessionByLocation(string identifier)
        {
            try
            {
                using (DataModel context = new DataModel(Util.ConnectionString()))
                {
                    List<ActiveSessions> list = new List<ActiveSessions>();

                    var elements = (from h in context.ActiveSessions
                                    where h.LocationIdentifier.Equals(identifier)
                                    select h).ToList();

                    foreach (var q in elements)
                    {
                        ActiveSessions obj = new ActiveSessions();
                        Util.Map(q, obj);

                        list.Add(obj);
                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ActiveSessions> ActiveSessionByAccSessionsIdString(string actSessiionID)
        {
            try
            {
                using (DataModel context = new DataModel(Util.ConnectionString()))
                {
                    List<ActiveSessions> list = new List<ActiveSessions>();

                    var elements = (from h in context.ActiveSessions
                                    where h.AcctSessionID.Contains(actSessiionID)
                                    select h).ToList();

                    foreach (var q in elements)
                    {
                        ActiveSessions obj = new ActiveSessions();
                        Util.Map(q, obj);

                        list.Add(obj);
                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

        #region ApiMerakiOrganizations

        public void Insert(ApiMerakiOrganizations obj)
        {
            try
            {
                using (DataModel2 context = new DataModel2(Util.ConnectionString()))
                {
                    context.ApiMerakiOrganizations.InsertOnSubmit(obj);
                    context.SubmitChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(ApiMerakiOrganizations obj)
        {
            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var element = (from h in context.ApiMerakiOrganizations
                               where h.IdOrganization.Equals(obj.IdOrganization)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(obj, element);

                context.SubmitChanges();
            }
        }

        public void Select(ApiMerakiOrganizations obj, int id)
        {

            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var element = (from h in context.ApiMerakiOrganizations
                               where h.id.Equals(id)
                               select h).FirstOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        public void DeleteApiMerakiOrganization(int idOrganization)
        {
            try
            {
                using (DataModel2 context = new DataModel2(Util.ConnectionString()))
                {
                    var element = (from h in context.ApiMerakiOrganizations
                                   where h.IdOrganization.Equals(idOrganization)
                                   select h).FirstOrDefault();

                    context.ApiMerakiOrganizations.DeleteOnSubmit(element);
                    context.SubmitChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<ApiMerakiOrganizations> ApiMerakiOrganizations()
        {
            try
            {
                using (DataModel2 context = new DataModel2(Util.ConnectionString()))
                {
                    List<ApiMerakiOrganizations> list = new List<ApiMerakiOrganizations>();

                    var elements = (from h in context.ApiMerakiOrganizations
                                    select h).ToList();

                    foreach (var q in elements)
                    {
                        ApiMerakiOrganizations obj = new ApiMerakiOrganizations();
                        Util.Map(q, obj);

                        list.Add(obj);
                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ApiMerakiOrganizations> ApiMerakiOrganizations(DateTime lastUpdate)
        {
            try
            {
                using (DataModel2 context = new DataModel2(Util.ConnectionString()))
                {
                    List<ApiMerakiOrganizations> list = new List<ApiMerakiOrganizations>();

                    var elements = (from h in context.ApiMerakiOrganizations
                                    where h.LastUpdate < lastUpdate
                                    select h).ToList();

                    foreach (var q in elements)
                    {
                        ApiMerakiOrganizations obj = new ApiMerakiOrganizations();
                        Util.Map(q, obj);

                        list.Add(obj);
                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region ApiMerakiNetworks

        public void Insert(ApiMerakiNetworks obj)
        {
            try
            {
                using (DataModel2 context = new DataModel2(Util.ConnectionString()))
                {
                    context.ApiMerakiNetworks.InsertOnSubmit(obj);

                    context.SubmitChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(ApiMerakiNetworks obj)
        {
            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var element = (from h in context.ApiMerakiNetworks
                               where h.idNetwork.Equals(obj.idNetwork)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(obj, element);

                context.SubmitChanges();
            }
        }

        public void Select(ApiMerakiNetworks obj, string id)
        {
            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var element = (from h in context.ApiMerakiNetworks
                               where h.id.Equals(id)
                               select h).FirstOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        public void DeleteApiMerakiNetwork(int idNetwork)
        {
            try
            {
                using (DataModel2 context = new DataModel2(Util.ConnectionString()))
                {
                    var element = (from h in context.ApiMerakiNetworks
                                   where h.idNetwork.Equals(idNetwork)
                                   select h).FirstOrDefault();

                    context.ApiMerakiNetworks.DeleteOnSubmit(element);
                    context.SubmitChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<ApiMerakiNetworks> ApiMerakiNetworks(int idOrganization)
        {
            try
            {
                using (DataModel2 context = new DataModel2(Util.ConnectionString()))
                {
                    List<ApiMerakiNetworks> list = new List<ApiMerakiNetworks>();

                    var elements = (from h in context.ApiMerakiNetworks
                                    where h.IdOrganization.Equals(idOrganization)
                                    select h).ToList();

                    foreach (var q in elements)
                    {
                        ApiMerakiNetworks obj = new ApiMerakiNetworks();
                        Util.Map(q, obj);

                        list.Add(obj);
                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ApiMerakiNetworks> ApiMerakiNetworks()
        {
            try
            {
                using (DataModel2 context = new DataModel2(Util.ConnectionString()))
                {
                    List<ApiMerakiNetworks> list = new List<ApiMerakiNetworks>();

                    var elements = (from h in context.ApiMerakiNetworks
                                    select h).ToList();

                    foreach (var q in elements)
                    {
                        ApiMerakiNetworks obj = new ApiMerakiNetworks();
                        Util.Map(q, obj);

                        list.Add(obj);
                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ApiMerakiNetworks> ApiMerakiNetworks(DateTime lastUpdate)
        {
            try
            {
                using (DataModel2 context = new DataModel2(Util.ConnectionString()))
                {
                    List<ApiMerakiNetworks> list = new List<ApiMerakiNetworks>();

                    var elements = (from h in context.ApiMerakiNetworks
                                    where h.LastUpdate < lastUpdate
                                    select h).ToList();

                    foreach (var q in elements)
                    {
                        ApiMerakiNetworks obj = new ApiMerakiNetworks();
                        Util.Map(q, obj);

                        list.Add(obj);
                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region ApiMerakiDevices

        public void Insert(ApiMerakiDevices obj)
        {
            try
            {
                using (DataModel2 context = new DataModel2(Util.ConnectionString()))
                {
                    context.ApiMerakiDevices.InsertOnSubmit(obj);

                    context.SubmitChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(ApiMerakiDevices obj)
        {
            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var element = (from h in context.ApiMerakiDevices
                               where h.Id.Equals(obj.Id)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(obj, element);

                context.SubmitChanges();
            }
        }

        public void Select(ApiMerakiDevices obj, int id)
        {

            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var element = (from h in context.ApiMerakiDevices
                               where h.Id.Equals(id)
                               select h).FirstOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        public void Select(ApiMerakiDevices obj, string mac)
        {

            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var element = (from h in context.ApiMerakiDevices
                               where h.mac.Equals(mac)
                               select h).FirstOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        public void DeleteApiMerakiDevice(int id)
        {
            try
            {
                using (DataModel2 context = new DataModel2(Util.ConnectionString()))
                {
                    var element = (from h in context.ApiMerakiDevices
                                   where h.Id.Equals(id)
                                   select h).FirstOrDefault();

                    context.ApiMerakiDevices.DeleteOnSubmit(element);
                    context.SubmitChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<ApiMerakiDevices> ApiMerakiDevices(string networkId)
        {
            try
            {
                using (DataModel2 context = new DataModel2(Util.ConnectionString()))
                {
                    List<ApiMerakiDevices> list = new List<ApiMerakiDevices>();

                    var elements = (from h in context.ApiMerakiDevices
                                    where h.networkId.Equals(networkId)
                                    select h).ToList();

                    foreach (var q in elements)
                    {
                        ApiMerakiDevices obj = new ApiMerakiDevices();
                        Util.Map(q, obj);

                        list.Add(obj);
                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ApiMerakiDevices> ApiMerakiDevices()
        {
            try
            {
                using (DataModel2 context = new DataModel2(Util.ConnectionString()))
                {
                    List<ApiMerakiDevices> list = new List<ApiMerakiDevices>();

                    var elements = (from h in context.ApiMerakiDevices
                                    select h).ToList();

                    foreach (var q in elements)
                    {
                        ApiMerakiDevices obj = new ApiMerakiDevices();
                        Util.Map(q, obj);

                        list.Add(obj);
                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ApiMerakiDevices> ApiMerakiDevices(DateTime lastUpdate)
        {
            try
            {
                using (DataModel2 context = new DataModel2(Util.ConnectionString()))
                {
                    List<ApiMerakiDevices> list = new List<ApiMerakiDevices>();

                    var elements = (from h in context.ApiMerakiDevices
                                    where h.LastUpdate < lastUpdate
                                    select h).ToList();

                    foreach (var q in elements)
                    {
                        ApiMerakiDevices obj = new ApiMerakiDevices();
                        Util.Map(q, obj);

                        list.Add(obj);
                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region ApiMerakiAccounting

        public void Insert(ApiMerakiAccounting obj)
        {
            try
            {
                using (DataModel2 context = new DataModel2(Util.ConnectionString()))
                {
                    context.ApiMerakiAccounting.InsertOnSubmit(obj);

                    context.SubmitChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ApiMerakiAccounting> SelectApiMerakiAccountings(string actSessionId, DateTime since)
        {
            try
            {
                using (DataModel2 context = new DataModel2(Util.ConnectionString()))
                {
                    List<ApiMerakiAccounting> list = new List<ApiMerakiAccounting>();

                    var elements = (from h in context.ApiMerakiAccounting
                                    where h.AcctSessionId.Equals(actSessionId) && h.DateInserted >= since
                                    orderby h.DateInserted descending
                                    select h).ToList();

                    foreach (var q in elements)
                    {
                        ApiMerakiAccounting obj = new ApiMerakiAccounting();
                        Util.Map(q, obj);

                        list.Add(obj);
                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SelectApiMerakiAccountingLastbyActSessionId(ApiMerakiAccounting obj, string actSessionId)
        {
            try
            {
                using (DataModel2 context = new DataModel2(Util.ConnectionString()))
                {
                    var element = (from h in context.ApiMerakiAccounting
                                    where h.AcctSessionId.Equals(actSessionId) 
                                    orderby h.DateInserted descending
                                    select h).FirstOrDefault();

                    if (element != null)
                        Util.Map(element, obj);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteApiMerakiAccounting(string actSessionId)
        {
            try
            {
                using (DataModel2 context = new DataModel2(Util.ConnectionString()))
                {
                    var elements = (from h in context.ApiMerakiAccounting
                                   where h.AcctSessionId.Equals(actSessionId)
                                   select h).ToList();

                    foreach(var element in elements)
                        context.ApiMerakiAccounting.DeleteOnSubmit(element);
                    context.SubmitChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region ClosedSessions

        public void Insert(ClosedSessions2 obj)
        {
            try
            {
                using (DataModel2 context = new DataModel2(Util.ConnectionString()))
                {
                    context.ClosedSessions2.InsertOnSubmit(obj);
                    context.SubmitChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region ApiMerakiGroupPolicies

        public void Insert(ApiMerakiGroupPolicies obj)
        {
            try
            {
                using (DataModel2 context = new DataModel2(Util.ConnectionString()))
                {
                    context.ApiMerakiGroupPolicies.InsertOnSubmit(obj);

                    context.SubmitChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //COMENT
        public void Update(ApiMerakiGroupPolicies obj)
        {
            using (DataModel2 context = new DataModel2(Util.ConnectionString()))
            {
                var element = (from h in context.ApiMerakiGroupPolicies
                               where h.Id.Equals(obj.Id)
                               select h).SingleOrDefault();

                if (element != null)
                    Util.Map(obj, element);

                context.SubmitChanges();
            }
        }

        public List<ApiMerakiGroupPolicies> SelectApiMerakiGroupPolicies(string NetworkId)
        {
            try
            {
                using (DataModel2 context = new DataModel2(Util.ConnectionString()))
                {
                    List<ApiMerakiGroupPolicies> list = new List<ApiMerakiGroupPolicies>();

                    var elements = (from h in context.ApiMerakiGroupPolicies
                                    where h.NetworkId.Equals(NetworkId)
                                    select h).ToList();

                    foreach (var q in elements)
                    {
                        ApiMerakiGroupPolicies obj = new ApiMerakiGroupPolicies();
                        Util.Map(q, obj);

                        list.Add(obj);
                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SelectApiMerakiGroupPolicy(ApiMerakiGroupPolicies obj, string networkId, int groupPolicyId)
        {
            try
            {
                using (DataModel2 context = new DataModel2(Util.ConnectionString()))
                {
                    var element = (from h in context.ApiMerakiGroupPolicies
                                   where h.NetworkId.Equals(networkId) && h.groupPolicyId.Equals(groupPolicyId)
                                   select h).FirstOrDefault();

                    if (element != null)
                        Util.Map(element, obj);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SelectApiMerakiGroupPolicy(ApiMerakiGroupPolicies obj, string networkId, string name)
        {
            try
            {
                using (DataModel2 context = new DataModel2(Util.ConnectionString()))
                {
                    var element = (from h in context.ApiMerakiGroupPolicies
                                   where h.NetworkId.Equals(networkId) && h.name.ToUpper().Equals(name.ToUpper())
                                   select h).FirstOrDefault();

                    if (element != null)
                        Util.Map(element, obj);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ApiMerakiGroupPolicies> SelectApiMerakiGroupPolicies(DateTime lastUpdate)
        {
            try
            {
                using (DataModel2 context = new DataModel2(Util.ConnectionString()))
                {
                    List<ApiMerakiGroupPolicies> list = new List<ApiMerakiGroupPolicies>();

                    var elements = (from h in context.ApiMerakiGroupPolicies
                                    where h.LastUpdate < lastUpdate
                                    select h).ToList();

                    foreach (var q in elements)
                    {
                        ApiMerakiGroupPolicies obj = new ApiMerakiGroupPolicies();
                        Util.Map(q, obj);

                        list.Add(obj);
                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteApiMerakiGroupPolicies(int id)
        {
            try
            {
                using (DataModel2 context = new DataModel2(Util.ConnectionString()))
                {
                    var element = (from h in context.ApiMerakiGroupPolicies
                                    where h.Id.Equals(id)
                                    select h).FirstOrDefault();

                    if (element != null)
                    {
                        context.ApiMerakiGroupPolicies.DeleteOnSubmit(element);
                        context.SubmitChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region Clone

        public bool CloneClosedSession(int idHotelToClone, int newIdHotel)
        {
            try
            {
                using (DataModel3 context = new DataModel3(Util.ConnectionString()))
                {
                    var elements = context.SP_360_CLONECLOSEDSESSIONS(idHotelToClone, newIdHotel);

                    return true;
                }
            }
            catch
            {
                throw;
            }
        }


        public bool CloneSessionLog(int idHotelToClone, int newIdHotel)
        {
            try
            {
                using (DataModel3 context = new DataModel3(Util.ConnectionString()))
                {
                    var elements = context.SP_360_CLONESESSIONSLOG(idHotelToClone, newIdHotel);

                    return true;
                }
            }
            catch
            {
                throw;
            }
        }

        public bool CloneLoyaltiesLocationIsNull(int idHotelToClone, int newIdHotel)
        {
            try
            {
                using (DataModel3 context = new DataModel3(Util.ConnectionString()))
                {
                    var elements = context.SP_360_CLONELOYALTIESLOCATIONISNULL(idHotelToClone, newIdHotel);

                    return true;
                }
            }
            catch
            {
                throw;
            }
        }

        public bool CloneLoyaltiesLocation(int idHotelToClone, int newIdHotel, int idLocationToClone, int newIdLocation)
        {
            try
            {
                using (DataModel3 context = new DataModel3(Util.ConnectionString()))
                {
                    var elements = context.SP_360_CLONELOYALTIESBYLOCATION(idHotelToClone, newIdHotel, idLocationToClone, newIdLocation);

                    return true;
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region CambiumLocationGPS

        public void Select(CambiumLocationsGPS obj, string NasMac)
        {

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var element = (from h in context.CambiumLocationsGPS
                               where h.NAS_MAC.Equals(NasMac)
                               select h).FirstOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        public void SelectContains(CambiumLocationsGPS obj, string Mac)
        {

            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var element = (from h in context.CambiumLocationsGPS
                               where h.NAS_MAC.Contains(Mac)
                               select h).FirstOrDefault();

                if (element != null)
                    Util.Map(element, obj);
            }
        }

        public List<CambiumLocationsGPS> SelectCambiumLocationsGPS(int idSite)
        {
            using (DataModel3 context = new DataModel3(Util.ConnectionString()))
            {
                var elements = (from h in context.CambiumLocationsGPS
                                where h.IdSite.Equals(idSite)
                                select h).ToList();

                List<CambiumLocationsGPS> list = new List<CambiumLocationsGPS>();

                foreach (var element in elements)
                {
                    CambiumLocationsGPS obj = new CambiumLocationsGPS();
                    Util.Map(element, obj);
                    list.Add(obj);
                }

                return list;
            }
        }

        #endregion

        #region PortalLogAttack
        public void Insert(CaptivePortalLogAttack obj)
        {
            try
            {
                using (DataModel3 context = new DataModel3(Util.ConnectionString()))
                {
                    context.CaptivePortalLogAttack.InsertOnSubmit(obj);
                    context.SubmitChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
