﻿using System;
using System.Collections.Generic;
using Wifi360.Data.Model;
using System.Globalization;
using Wifi360.Data.Common;
using Wifi360.Data.Class;

namespace Wifi360.Data.Controllers
{
    public class BillingController
    {
        /// <summary>
        /// Metodo de autenticacion
        /// </summary>
        /// <param name="username">username</param>
        /// <param name="password">password</param>
        /// <returns>true: login success, false: login failure</returns>
        static public bool login(string username, string password)
        {
            Users user = new Users();
            BillingDataAccess.Select(user, username, password);

            if (user.IdUser != 0)
                return true;

            return false;
        }

        #region GROUPS

        static public bool DeleteGroupMember(int id)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                dataAccess.DeleteGroupMember(id);

                return true;
            }
            catch
            {
                return false;
            }
        }

        static public bool SaveFDSGroups(FDSGroups obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.IdGroup.Equals(0))
                    dataAccess.Insert(obj);
                else
                    dataAccess.Update(obj);
                return true;
            }
            catch (Exception ex)
            {
                throw;
                return false;

            }
        }

        static public bool SaveFDSGroupsMember(FDSGroupsMembers obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.Id.Equals(0))
                    dataAccess.Insert(obj);
                else
                    dataAccess.Update(obj);
                return true;
            }
            catch (Exception ex)
            {
                throw;
                return false;

            }
        }

        static public List<FDSGroups> GetGroups(string name, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAcess = new BillingDataAccess();
            return dataAcess.SelectGroups(name, pageIndex, pageSize, ref PageNumber);
        }

        static public List<FDSGroups> GetGroups()
        {
            BillingDataAccess dataAcess = new BillingDataAccess();
            return dataAcess.SelectGroups();
        }

        static public List<FDSGroups> GetGroupsPorGrupo(int IdGrupo)
        {
            BillingDataAccess dataAcess = new BillingDataAccess();
            if (IdGrupo != 0)
                return dataAcess.SelectGroups(IdGrupo);
            else
                return dataAcess.SelectGroups();
        }

        #endregion

        #region HOTELES / SITES
        static public List<Hotels> GetHotelsPorGrupo(int IdGroup)
        {
            BillingDataAccess dataAcess = new BillingDataAccess();
            return dataAcess.SelectHotelesPorGrupo(IdGroup);
        }

        static public List<Hotels> GetHotels()
        {
            BillingDataAccess dataAcess = new BillingDataAccess();
            return dataAcess.SelectHoteles();
        }

        static public List<Hotels> GetHotels(string name, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAcess = new BillingDataAccess();
            return dataAcess.SelectHoteles(name, pageIndex, pageSize, ref PageNumber);
        }


        #endregion

        #region ROOMS
        static public bool SaveRooms(Rooms obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.IdRoom.Equals(0))
                    dataAccess.Insert(obj);
                else
                    dataAccess.Update(obj);

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
        }


        #endregion

        #region LOCATIONS
        static public bool SaveMerakiLocationsGPS(MerakiLocationsGPS obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.Id.Equals(0))
                    dataAccess.Insert(obj);
                 else
                    dataAccess.Update(obj);
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
        }

        static public bool SaveLocationIdentifiers(FDSLocationsIdentifier obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.Id.Equals(0))
                    dataAccess.Insert(obj);
                // else
                //     dataAccess.Update(obj);

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
        }

        static public List<Locations2> GetLocationsPorGrupo(int idGrupos)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.SelectLocationsPorGrupos(idGrupos);
        }

        static public List<Locations2> GetLocations()
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.SelectLocations();
        }

        #endregion

        #region FDSUsers
        static public FDSUsers GetFDSUser(string username, string password)
        {
            FDSUsers fdsuser = new FDSUsers();
            BillingDataAccess.Select(fdsuser, username, password);

            return fdsuser;
        }

        static public FDSUsers GetFDSUser(int idUser)
        {
            FDSUsers fdsuser = new FDSUsers();
            BillingDataAccess.Select(fdsuser, idUser);

            return fdsuser;
        }

        static public List<FDSUsers> GetFDSUsers(string name, int pageIndex, int pageSize, ref int PageNumber)
        {
            return BillingDataAccess.SelectFDSUsers(name, pageIndex, pageSize, ref PageNumber);

        }

        static public List<FDSUsers> GetFDSUsers()
        {
            return BillingDataAccess.SelectFDSUsers();

        }

        static public List<FDSUsers> GetFDSUsersLocation(int idLocation)
        {
            return BillingDataAccess.SelectFDSUsersLocation(idLocation);

        }

        static public List<FDSUsers> GetFDSUsersSite(int idSite)
        {
            return BillingDataAccess.SelectFDSUsersSite(idSite);

        }

        static public List<FDSUsers> GetFDSUsersGroup(int idGroup)
        {
            return BillingDataAccess.SelectFDSUsersGroup(idGroup);

        }

        static public List<FDSUsers> GetFDSUsersInBillings(string listadoHoteles, DateTime start, DateTime end)
        {
            return BillingDataAccess.SelectFDSUsersInBillings(listadoHoteles, start, end);

        }

        static public Rols GetFDSRol(int idRol)
        {
            Rols rol = new Wifi360.Data.Model.Rols();
            BillingDataAccess.Select(rol, idRol);

            return rol;
        }

        static public List<Rols> GetFDSRols()
        {
            return BillingDataAccess.SelectFDSRols();
        }

        static public FDSLocationsIdentifier GetFDSLocationIdentifier(string identifier)
        {
            FDSLocationsIdentifier fdsuser = new FDSLocationsIdentifier();
            BillingDataAccess.Select(fdsuser, identifier);

            return fdsuser;
        }

        static public FDSLocationsIdentifier GetFDSLocationIdentifier(int idlocation)
        {
            FDSLocationsIdentifier fdsuser = new FDSLocationsIdentifier();
            BillingDataAccess.Select(fdsuser, idlocation);

            return fdsuser;
        }

        static public FDSUsersAccess GetFDSUserAccess(int idUser)
        {
            FDSUsersAccess fdsuser = new FDSUsersAccess();
            BillingDataAccess.Select(fdsuser, idUser);

            return fdsuser;
        }

        static public List<FDSUsersAccess> GetFDSUserAccesses()
        {
            return BillingDataAccess.SelectFDSUserAccesses();

        }

        static public List<FDSUsersAccess> GetFDSUserAccessesBySite(int idsite)
        {
            return BillingDataAccess.SelectFDSUserAccessesBySite(idsite);
        }

        static public List<FDSUsersAccess> GetFDSUserAccesses(int idUser)
        {
            return BillingDataAccess.SelectFDSUserAccesses(idUser);

        }

        static public List<FDSSitesGroups> GetFDSSitesGroup(int idGroup)
        {
            return BillingDataAccess.SelectFDSSitesGroup(idGroup);

        }

        static public List<Hotels> GetSitesUser(int idUser)
        {
            return BillingDataAccess.SelectSitesUser(idUser);

        }

        static public List<FDSLocationsIdentifier> GetFDSLocationsIdentifiers(int idSite)
        {
            return BillingDataAccess.SelectFDSLocationsIdentifiers(idSite);

        }
        static public List<FDSLocationsIdentifier> GetFDSLocationsIdentifiers(int idLoc, int idSite)
        {
            return BillingDataAccess.SelectFDSLocationsIdentifiers(idLoc, idSite);

        }

        static public List<MerakiLocationsGPS> GetAllMerakiLocationsGPSSite(int idLoc, int idSite)
        {
            return BillingDataAccess.SelectAllMerakiLocationsGPSSite(idLoc, idSite);
        }

        static public FDSUsersType GetFDSUserType(int idType)
        {
            FDSUsersType fdsusertype = new FDSUsersType();
            BillingDataAccess.Select(fdsusertype, idType);

            return fdsusertype;
        }

        static public List<FDSUsersType> GetFDSUserTypes()
        {
            return BillingDataAccess.SelectFDSUserTypes();
        }

        static public RegisterUsers GetFDSUserInactive(int idUser)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            RegisterUsers userInactive = new RegisterUsers();
            dataAccess.Select(userInactive, idUser);

            return userInactive;
        }

        static public RegisterUsers GetFDSUserInactive(string username)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            RegisterUsers userInactive = new RegisterUsers();
            dataAccess.Select(userInactive, username);

            return userInactive;
        }

        #endregion

        #region PortalLogAttack
        static public bool SaveCaptivePortalLogAttack(CaptivePortalLogAttack log) {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                dataAccess.Insert(log);                
            }
            catch (Exception ex)
            {
                throw;
            }
            return true;
        }

        #endregion

        static public List<BillingInfo> produccion(int idHotel, DateTime start, DateTime end)
        {
            try
            {
                BillingDataAccess data = new BillingDataAccess();
                return data.SelectBillings(idHotel, start, end);
            }
            catch (Exception ex)
            {
                throw;
                return null;
            }

        }

        static public List<BillingInfo> produccion(string listadohoteles, DateTime start, DateTime end)
        {
            try
            {
                BillingDataAccess data = new BillingDataAccess();
                return data.SelectBillings(listadohoteles, start, end);
            }
            catch (Exception ex)
            {
                throw;
                return null;
            }

        }

        static public List<BillingInfo> produccion(string listadohoteles, DateTime start, DateTime end, int idroom, int idfdsuserfilter)
        {
            try
            {
                BillingDataAccess data = new BillingDataAccess();
                return data.SelectBillings(listadohoteles, start, end, idroom, idfdsuserfilter);
            }
            catch (Exception ex)
            {
                throw;
                return null;
            }

        }

        //static public List<Billings> produccion(int idHotel, string idroom, string start, string end)
        //{
        //    try
        //    {
        //        string format = "d";
        //        CultureInfo provider = CultureInfo.InvariantCulture;

        //        if ((idroom.Equals("0")) && ((!string.IsNullOrEmpty(start) && (!string.IsNullOrEmpty(end)))))
        //        {
        //            BillingDataAccess data = new BillingDataAccess();
        //            return data.SelectBillings(idHotel, DateTime.ParseExact(start, format,provider), DateTime.ParseExact(end, format, provider));
        //        }
        //        else if ((idroom.Equals("0")) && ((string.IsNullOrEmpty(start) && (string.IsNullOrEmpty(end)))))
        //        {
        //            BillingDataAccess data = new BillingDataAccess();
        //            return data.SelectBillings(idHotel, Int32.Parse(idroom));
        //        }
        //        else
        //        {
        //            BillingDataAccess data = new BillingDataAccess();
        //            return data.SelectBillings(idHotel, Int32.Parse(idroom), DateTime.ParseExact(start, format, provider), DateTime.ParseExact(end, format, provider));
        //        }


        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //        return null;
        //    }

        //}

        static public List<BillingInfo> produccion(int idHotel, string idroom, string start, string end, int pageIndex, int pageSize, ref int pageNumber, ref double suma)
        {
            try
            {
                string format = "d";
                CultureInfo provider = CultureInfo.InvariantCulture;

                if ((idroom.Equals("0")) && ((!string.IsNullOrEmpty(start) && (!string.IsNullOrEmpty(end)))))
                {
                    BillingDataAccess data = new BillingDataAccess();
                    return data.SelectBillings(idHotel, DateTime.Parse(start), DateTime.Parse(end), pageIndex, pageSize, ref pageNumber, ref suma);
                    //return data.SelectBillings(idHotel, DateTime.ParseExact(start, format, provider), DateTime.ParseExact(end, format, provider), pageIndex, pageSize, ref pageNumber);
                }
                else if ((!idroom.Equals("0")) && ((string.IsNullOrEmpty(start) && (string.IsNullOrEmpty(end)))))
                {
                    BillingDataAccess data = new BillingDataAccess();
                    return data.SelectBillings(idHotel, Int32.Parse(idroom), pageIndex, pageSize, ref pageNumber, ref suma);
                }
                else
                {
                    BillingDataAccess data = new BillingDataAccess();
                    return data.SelectBillings(idHotel, Int32.Parse(idroom), DateTime.Parse(start), DateTime.Parse(end), pageIndex, pageSize, ref pageNumber, ref suma);
                }

                return null;
            }
            catch (Exception ex)
            {
                throw;
                return null;
            }

        }

        static public List<BillingInfo> produccion(int idHotel, int idroom, string start, string end, int idfdsuser, int pageIndex, int pageSize, ref int pageNumber, ref double suma)
        {
            try
            {
                BillingDataAccess data = new BillingDataAccess();
                return data.SelectBillings(idHotel, DateTime.Parse(start), DateTime.Parse(end), idroom, idfdsuser, pageIndex, pageSize, ref pageNumber, ref suma);

            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static public List<BillingInfo> produccion(int idHotel, List<FDSUsers> users, int idroom, string start, string end, int idfdsuser, int pageIndex, int pageSize, ref int pageNumber, ref double suma)
        {
            try
            {
                BillingDataAccess data = new BillingDataAccess();
                return data.SelectBillings(idHotel, users, DateTime.Parse(start), DateTime.Parse(end), idroom, idfdsuser, pageIndex, pageSize, ref pageNumber, ref suma);

            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static public List<BillingInfo> produccion(string listhoteles, List<FDSUsers> users, int idroom, string start, string end, int idfdsuser, int pageIndex, int pageSize, ref int pageNumber, ref double suma)
        {
            try
            {
                BillingDataAccess data = new BillingDataAccess();
                return data.SelectBillings(listhoteles, users, DateTime.Parse(start), DateTime.Parse(end), idroom, idfdsuser, pageIndex, pageSize, ref pageNumber, ref suma);

            }
            catch (Exception ex)
            {
                throw;
            }

        }


        static public List<BillingInfo> produccion(int idHotel, int idroom, string start, string end, int idfdsuser)
        {
            try
            {
                BillingDataAccess data = new BillingDataAccess();
                return data.SelectBillings(idHotel, DateTime.Parse(start), DateTime.Parse(end), idroom, idfdsuser);

            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static public List<BillingInfo> produccion(int idHotel, string idroom, string billing, string start, string end, int pageIndex, int pageSize, ref int pageNumber, ref double suma)
        {
            try
            {
                string format = "d";
                CultureInfo provider = CultureInfo.InvariantCulture;

                if ((idroom.Equals("0")) && ((!string.IsNullOrEmpty(start) && (!string.IsNullOrEmpty(end)))))
                {
                    BillingDataAccess data = new BillingDataAccess();
                    return data.SelectBillings(idHotel, DateTime.Parse(start), DateTime.Parse(end), pageIndex, pageSize, ref pageNumber, ref suma);
                    //return data.SelectBillings(idHotel, DateTime.ParseExact(start, format, provider), DateTime.ParseExact(end, format, provider), pageIndex, pageSize, ref pageNumber);
                }
                else if ((!idroom.Equals("0")) && ((string.IsNullOrEmpty(start) && (string.IsNullOrEmpty(end)))))
                {
                    BillingDataAccess data = new BillingDataAccess();
                    return data.SelectBillings(idHotel, Int32.Parse(idroom), pageIndex, pageSize, ref pageNumber, ref suma);
                }
                else
                {
                    BillingDataAccess data = new BillingDataAccess();
                    return data.SelectBillings(idHotel, Int32.Parse(idroom), DateTime.Parse(start), DateTime.Parse(end), pageIndex, pageSize, ref pageNumber, ref suma);
                }

                return null;
            }
            catch (Exception ex)
            {
                throw;
                return null;
            }

        }

        static public List<BillingInfo> produccion(int idHotel, string idroom, string billing, string start, string end, int idfdsuser, int pageIndex, int pageSize, ref int pageNumber, ref double suma)
        {
            try
            {
                string format = "d";
                CultureInfo provider = CultureInfo.InvariantCulture;

                if ((idroom.Equals("0")) && ((!string.IsNullOrEmpty(start) && (!string.IsNullOrEmpty(end)))))
                {
                    BillingDataAccess data = new BillingDataAccess();
                    return data.SelectBillings(idHotel, DateTime.Parse(start), DateTime.Parse(end), pageIndex, pageSize, ref pageNumber, ref suma);
                    //return data.SelectBillings(idHotel, DateTime.ParseExact(start, format, provider), DateTime.ParseExact(end, format, provider), pageIndex, pageSize, ref pageNumber);
                }
                else if ((!idroom.Equals("0")) && ((string.IsNullOrEmpty(start) && (string.IsNullOrEmpty(end)))))
                {
                    BillingDataAccess data = new BillingDataAccess();
                    return data.SelectBillings(idHotel, Int32.Parse(idroom), pageIndex, pageSize, ref pageNumber, ref suma);
                }
                else
                {
                    BillingDataAccess data = new BillingDataAccess();
                    return data.SelectBillings(idHotel, Int32.Parse(idroom), DateTime.Parse(start), DateTime.Parse(end), pageIndex, pageSize, ref pageNumber, ref suma);
                }

                return null;
            }
            catch (Exception ex)
            {
                throw;
                return null;
            }

        }

        static public List<Exchanges> GetTiposDeMoneda(string Sites)
        {
            try
            {
                BillingDataAccess data = new BillingDataAccess();
                return data.SelectTiposDeMoneda(Sites);
               
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        static public List<BillingInfo> produccion(string hotels, string idroom, string billing, string start, string end, int pageIndex, int pageSize, ref int pageNumber, ref double suma)
        {
            try
            {
                if ((idroom.Equals("0")) && ((!string.IsNullOrEmpty(start) && (!string.IsNullOrEmpty(end)))))
                {
                    BillingDataAccess data = new BillingDataAccess();
                    return data.SelectBillings(hotels, DateTime.Parse(start), DateTime.Parse(end), pageIndex, pageSize, ref pageNumber, ref suma);
                    //return data.SelectBillings(idHotel, DateTime.ParseExact(start, format, provider), DateTime.ParseExact(end, format, provider), pageIndex, pageSize, ref pageNumber);
                }
                else if ((!idroom.Equals("0")) && ((string.IsNullOrEmpty(start) && (string.IsNullOrEmpty(end)))))
                {
                    BillingDataAccess data = new BillingDataAccess();
                    return data.SelectBillings(hotels, Int32.Parse(idroom), pageIndex, pageSize, ref pageNumber, ref suma);
                }
                else
                {
                    BillingDataAccess data = new BillingDataAccess();
                    return data.SelectBillings(hotels, Int32.Parse(idroom), DateTime.Parse(start), DateTime.Parse(end), pageIndex, pageSize, ref pageNumber, ref suma);
                }

            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static public List<BillingInfo> produccionLocation(int idLocation, string idroom, string billing, string start, string end, int pageIndex, int pageSize, ref int pageNumber, ref double suma)
        {
            try
            {
                string format = "d";
                CultureInfo provider = CultureInfo.InvariantCulture;

                if ((idroom.Equals("0")) && ((!string.IsNullOrEmpty(start) && (!string.IsNullOrEmpty(end)))))
                {
                    BillingDataAccess data = new BillingDataAccess();
                    return data.SelectBillingsLocation(idLocation, DateTime.Parse(start), DateTime.Parse(end), pageIndex, pageSize, ref pageNumber, ref suma);
                }
                else if ((!idroom.Equals("0")) && ((string.IsNullOrEmpty(start) && (string.IsNullOrEmpty(end)))))
                {
                    BillingDataAccess data = new BillingDataAccess();
                    return data.SelectBillingsLocation(idLocation, Int32.Parse(idroom), pageIndex, pageSize, ref pageNumber, ref suma);
                }
                else
                {
                    BillingDataAccess data = new BillingDataAccess();
                    return data.SelectBillingsLocation(idLocation, Int32.Parse(idroom), DateTime.Parse(start), DateTime.Parse(end), pageIndex, pageSize, ref pageNumber, ref suma);
                }

                return null;
            }
            catch (Exception ex)
            {
                throw;
                return null;
            }

        }

        static public List<BillingInfo> produccionLocation(int idLocation, string idroom, string billing, string start, string end)
        {
            try
            {
                string format = "d";
                CultureInfo provider = CultureInfo.InvariantCulture;

                if ((idroom.Equals("0")) && ((!string.IsNullOrEmpty(start) && (!string.IsNullOrEmpty(end)))))
                {
                    BillingDataAccess data = new BillingDataAccess();
                    return data.SelectBillingsLocation(idLocation, DateTime.Parse(start), DateTime.Parse(end));
                }
                else if ((!idroom.Equals("0")) && ((string.IsNullOrEmpty(start) && (string.IsNullOrEmpty(end)))))
                {
                    BillingDataAccess data = new BillingDataAccess();
                    return data.SelectBillingsLocation(idLocation, Int32.Parse(idroom));
                }
                else
                {
                    BillingDataAccess data = new BillingDataAccess();
                    return data.SelectBillingsInfoLocation(idLocation, Int32.Parse(idroom), DateTime.Parse(start), DateTime.Parse(end));
                }

            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static public List<BillingInfo> produccionLocation(int idLocation, List<FDSUsers> users, string idroom, string billing, string start, string end)
        {
            try
            {
                string format = "d";
                CultureInfo provider = CultureInfo.InvariantCulture;

                if ((idroom.Equals("0")) && ((!string.IsNullOrEmpty(start) && (!string.IsNullOrEmpty(end)))))
                {
                    BillingDataAccess data = new BillingDataAccess();
                    return data.SelectBillingsLocation(idLocation, users, DateTime.Parse(start), DateTime.Parse(end));
                }
                else if ((!idroom.Equals("0")) && ((string.IsNullOrEmpty(start) && (string.IsNullOrEmpty(end)))))
                {
                    BillingDataAccess data = new BillingDataAccess();
                    return data.SelectBillingsLocation(idLocation, Int32.Parse(idroom));
                }
                else
                {
                    BillingDataAccess data = new BillingDataAccess();
                    return data.SelectBillingsInfoLocation(idLocation, Int32.Parse(idroom), DateTime.Parse(start), DateTime.Parse(end));
                }

            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static public List<BillingInfo> produccionLocation(int idLocation, string idroom, string billing, int idfdsuser, string start, string end, int pageIndex, int pageSize, ref int pageNumber, ref double suma)
        {
            try
            {
                string format = "d";
                CultureInfo provider = CultureInfo.InvariantCulture;

                if ((idroom.Equals("0")) && ((!string.IsNullOrEmpty(start) && (!string.IsNullOrEmpty(end)))))
                {
                    BillingDataAccess data = new BillingDataAccess();
                    return data.SelectBillingsLocation(idLocation, DateTime.Parse(start), DateTime.Parse(end), idfdsuser, pageIndex, pageSize, ref pageNumber, ref suma);
                }
                else if ((!idroom.Equals("0")) && ((string.IsNullOrEmpty(start) && (string.IsNullOrEmpty(end)))))
                {
                    BillingDataAccess data = new BillingDataAccess();
                    return data.SelectBillingsLocation(idLocation, Int32.Parse(idroom), pageIndex, pageSize, ref pageNumber, ref suma);
                }
                else
                {
                    BillingDataAccess data = new BillingDataAccess();
                    return data.SelectBillingsLocation(idLocation, Int32.Parse(idroom), DateTime.Parse(start), DateTime.Parse(end), idfdsuser, pageIndex, pageSize, ref pageNumber, ref suma);
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static public List<BillingInfo> produccionLocation(int idLocation, List<FDSUsers> fdsusers, string idroom, string billing, int idfdsuser, string start, string end, int pageIndex, int pageSize, ref int pageNumber, ref double suma)
        {
            try
            {
                string format = "d";
                CultureInfo provider = CultureInfo.InvariantCulture;

                if ((idroom.Equals("0")) && ((!string.IsNullOrEmpty(start) && (!string.IsNullOrEmpty(end)))))
                {
                    BillingDataAccess data = new BillingDataAccess();
                    return data.SelectBillingsLocation(idLocation, fdsusers, DateTime.Parse(start), DateTime.Parse(end), idfdsuser, pageIndex, pageSize, ref pageNumber, ref suma);
                }
                else if ((!idroom.Equals("0")) && ((string.IsNullOrEmpty(start) && (string.IsNullOrEmpty(end)))))
                {
                    BillingDataAccess data = new BillingDataAccess();
                    return data.SelectBillingsLocation(idLocation, Int32.Parse(idroom), pageIndex, pageSize, ref pageNumber, ref suma);
                }
                else
                {
                    BillingDataAccess data = new BillingDataAccess();
                    return data.SelectBillingsLocation(idLocation, fdsusers, Int32.Parse(idroom), DateTime.Parse(start), DateTime.Parse(end), idfdsuser, pageIndex, pageSize, ref pageNumber, ref suma);
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static public List<BillingInfo> produccionLocation(int idLocation, int idroom, string billing, int idfdsuser, string start, string end)
        {
            try
            {
                string format = "d";
                CultureInfo provider = CultureInfo.InvariantCulture;

                if ((idroom.Equals("0")) && ((!string.IsNullOrEmpty(start) && (!string.IsNullOrEmpty(end)))))
                {
                    BillingDataAccess data = new BillingDataAccess();
                    return data.SelectBillingsLocation(idLocation, DateTime.Parse(start), DateTime.Parse(end), idfdsuser);
                }
                else if ((!idroom.Equals("0")) && ((string.IsNullOrEmpty(start) && (string.IsNullOrEmpty(end)))))
                {
                    BillingDataAccess data = new BillingDataAccess();
                    return data.SelectBillingsLocation(idLocation, idroom);
                }
                else
                {
                    BillingDataAccess data = new BillingDataAccess();
                    return data.SelectBillingsLocation(idLocation, DateTime.Parse(start), DateTime.Parse(end), idfdsuser, idroom);
                }

            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static public List<BillingInfo> produccionLocation(int idLocation, List<FDSUsers> users, int idroom, string billing, int idfdsuser, string start, string end)
        {
            try
            {
                string format = "d";
                CultureInfo provider = CultureInfo.InvariantCulture;

                if ((idroom.Equals("0")) && ((!string.IsNullOrEmpty(start) && (!string.IsNullOrEmpty(end)))))
                {
                    BillingDataAccess data = new BillingDataAccess();
                    return data.SelectBillingsLocation(idLocation, DateTime.Parse(start), DateTime.Parse(end), idfdsuser);
                }
                else if ((!idroom.Equals("0")) && ((string.IsNullOrEmpty(start) && (string.IsNullOrEmpty(end)))))
                {
                    BillingDataAccess data = new BillingDataAccess();
                    return data.SelectBillingsLocation(idLocation, idroom);
                }
                else
                {
                    BillingDataAccess data = new BillingDataAccess();
                    return data.SelectBillingsLocation(idLocation, DateTime.Parse(start), DateTime.Parse(end), idfdsuser, idroom);
                }

            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static public BillingTypes ObtainBillingType(int idBillingType)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                BillingTypes obj = new BillingTypes();
                dataAccess.Select(obj, idBillingType);

                return obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        static public BillingTypes2 ObtainBillingType2(int idBillingType)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                BillingTypes2 obj = new BillingTypes2();
                dataAccess.Select(obj, idBillingType);

                return obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        static public Billings ObtainBilling(int idBilling)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                Billings obj = new Billings();
                dataAccess.Select(obj, idBilling);

                return obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        static public Locations2 ObtainLocation(int idLocation)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                Locations2 obj = new Locations2();
                dataAccess.Select(obj, idLocation);

                return obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        static public Locations2 ObtainLocationDefault(int idsite)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                Locations2 obj = new Locations2();
                dataAccess.Select(obj, idsite, true);

                return obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //static public Locations ObtainLocation(string deviceid)
        //{
        //    try
        //    {
        //        BillingDataAccess dataAccess = new BillingDataAccess();
        //        Locations obj = new Locations();
        //        dataAccess.Select(obj, deviceid);

        //        return obj;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}

        static public Locations2 ObtainLocation(int idhotel, string value)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                Locations2 obj = new Locations2();
                dataAccess.Select(obj, idhotel, value);

                return obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        static public Priorities ObtainPriority(int idPriority)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                Priorities obj = new Priorities();
                dataAccess.Select(obj, idPriority);

                return obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        static public bool DeletePriority(int id)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                dataAccess.DeletePriority(id);

                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Devuelve los tipos de acceso creados en un hotel
        /// </summary>
        /// <param name="idHotel">Identificador del hotel</param>
        /// <returns></returns>
        static public List<BillingTypes> ObtainBillingTypes(int idHotel)
        {
            try
            {

                List<BillingTypes> billingTypes = new List<BillingTypes>();
                BillingDataAccess dataAccess = new BillingDataAccess();
                billingTypes = dataAccess.SelectBillingType(idHotel);

                return billingTypes;

            }
            catch (Exception ex)
            {
                throw;
                return null;
            }
        }

        static public List<BillingTypes2> ObtainBillingTypes2All(int idHotel)
        {
            try
            {

                List<BillingTypes2> billingTypes = new List<BillingTypes2>();
                BillingDataAccess dataAccess = new BillingDataAccess();
                billingTypes = dataAccess.SelectBillingType2All(idHotel);

                return billingTypes;

            }
            catch (Exception ex)
            {
                throw;
                return null;
            }
        }

        static public List<BillingTypes> ObtainBillingTypesAll(int idHotel)
        {
            try
            {

                List<BillingTypes> billingTypes = new List<BillingTypes>();
                BillingDataAccess dataAccess = new BillingDataAccess();
                billingTypes = dataAccess.SelectBillingTypeAll(idHotel);

                return billingTypes;

            }
            catch (Exception ex)
            {
                throw;
                return null;
            }
        }

        static public List<BillingTypes> ObtainBillingTypesModules(int idHotel, int idBillingModule)
        {
            try
            {

                List<BillingTypes> billingTypes = new List<BillingTypes>();
                BillingDataAccess dataAccess = new BillingDataAccess();
                billingTypes = dataAccess.SelectBillingTypeModule(idHotel, idBillingModule);

                return billingTypes;

            }
            catch (Exception ex)
            {
                throw;
                return null;
            }
        }

        static public List<BillingTypes> ObtainBillingTypes(int idLocation, int idBillingModule)
        {
            try
            {
                List<BillingTypes> billingTypes = new List<BillingTypes>();
                BillingDataAccess dataAccess = new BillingDataAccess();
                billingTypes = dataAccess.SelectBillingType(idLocation, idBillingModule);

                return billingTypes;
            }
            catch (Exception ex)
            {
                throw;
                return null;
            }
        }

        static public List<BillingTypes> SelectBillingTypesOnlyAllZones(int idHotel)
        {
            try
            {
                Locations2 locationAllzones = ObtainLocation(idHotel, "All_Zones");

                List<BillingTypes> billingTypes = new List<BillingTypes>();
                BillingDataAccess dataAccess = new BillingDataAccess();
                billingTypes = dataAccess.SelectBillingTypesOnlyAllZones(locationAllzones.IdLocation);

                return billingTypes;

            }
            catch (Exception ex)
            {
                throw;
                return null;
            }
        }

        static public List<BillingTypes> SelectBillingTypesAllAndAllZones(int idHotel, int idlocation)
        {
            try
            {
                Locations2 locationAllzones = ObtainLocation(idHotel, "All_Zones");
                
                List<BillingTypes> billingTypes = new List<BillingTypes>();
                BillingDataAccess dataAccess = new BillingDataAccess();
                billingTypes = dataAccess.SelectBillingTypesAllAndAllZones(idlocation, locationAllzones.IdLocation);

                return billingTypes;

            }
            catch (Exception ex)
            {
                throw;
                return null;
            }
        }
            
        static public List<BillingTypes> ObtainBillingTypesAndAllZones(int idHotel, int idlocation)
        {
            try
            {
                Locations2 locationAllzones = ObtainLocation(idHotel, "All_Zones");
                
                List<BillingTypes> billingTypes = new List<BillingTypes>();
                BillingDataAccess dataAccess = new BillingDataAccess();
                billingTypes = dataAccess.SelectBillingTypesAndAllZones(idlocation, locationAllzones.IdLocation);

                return billingTypes;

            }
            catch (Exception ex)
            {
                throw;
                return null;
            }
        }

        static public BillingTypes ObtainBillingTypeFree(int idHotel)
        {
            try
            {

                BillingTypes billingType = new BillingTypes();
                BillingDataAccess dataAccess = new BillingDataAccess();
                billingType = dataAccess.SelectBillingTypeFree(idHotel);

                return billingType;

            }
            catch (Exception ex)
            {
                throw;
                return null;
            }
        }

        static public BillingTypes ObtainBillingTypeFreeForLocation(int idLocation)
        {
            try
            {
                BillingTypes billingType = new BillingTypes();
                BillingDataAccess dataAccess = new BillingDataAccess();
                billingType = dataAccess.SelectBillingTypeFreeLocation(idLocation);

                return billingType;

            }
            catch (Exception ex)
            {
                throw;
                return null;
            }
        }

        static public BillingTypes ObtainBillingTypeDefaultFastTicket(int idHotel)
        {
            try
            {
                BillingTypes billingType = new BillingTypes();
                BillingDataAccess dataAccess = new BillingDataAccess();
                dataAccess.Select(billingType, idHotel, true);

                return billingType;

            }
            catch (Exception ex)
            {
                throw;
                return null;
            }
        }

        static public List<BillingTypes> ObtainBillingTypes(int idHotel, int PageIndex, int PageSize, ref int PageNumber)
        {
            try
            {

                List<BillingTypes> billingTypes = new List<BillingTypes>();
                BillingDataAccess dataAccess = new BillingDataAccess();
                billingTypes = dataAccess.SelectBillingType(idHotel, PageIndex, PageSize, ref PageNumber);

                return billingTypes;

            }
            catch (Exception ex)
            {
                throw;
                return null;
            }
        }

        static public List<BillingTypes> ObtainBillingTypes(int idHotel, string filter, int idmodule, bool status, int iot, int PageIndex, int PageSize, ref int PageNumber)
        {
            try
            {

                List<BillingTypes> billingTypes = new List<BillingTypes>();
                BillingDataAccess dataAccess = new BillingDataAccess();
                switch (iot)
                {
                    case 0: billingTypes = dataAccess.SelectBillingType(idHotel, filter, idmodule, status, PageIndex, PageSize, ref PageNumber); break;
                    case 1: billingTypes = dataAccess.SelectBillingType(idHotel, filter, idmodule, status, false, PageIndex, PageSize, ref PageNumber); break;
                    case 2: billingTypes = dataAccess.SelectBillingType(idHotel, filter, idmodule, status, true, PageIndex, PageSize, ref PageNumber); break;
                }
                

                return billingTypes;

            }
            catch (Exception ex)
            {
                throw;
                return null;
            }
        }

        static public List<BillingTypes> ObtainBillingTypesLocation(int idLocation)
        {
            try
            {

                List<BillingTypes> billingTypes = new List<BillingTypes>();
                BillingDataAccess dataAccess = new BillingDataAccess();
                billingTypes = dataAccess.SelectBillingTypeLocation(idLocation);

                return billingTypes;

            }
            catch (Exception ex)
            {
                throw;
                return null;
            }
        }

        static public List<BillingTypes2> ObtainAllBillingTypesLocation(int idLocation)
        {
            try
            {

                List<BillingTypes2> billingTypes = new List<BillingTypes2>();
                BillingDataAccess dataAccess = new BillingDataAccess();
                billingTypes = dataAccess.SelectAllBillingTypeLocation(idLocation);

                return billingTypes;

            }
            catch (Exception ex)
            {
                throw;
                return null;
            }
        }

        static public List<BillingTypes> ObtainBillingTypesLocation(int idLocation, int PageIndex, int PageSize, ref int PageNumber)
        {
            try
            {

                List<BillingTypes> billingTypes = new List<BillingTypes>();
                BillingDataAccess dataAccess = new BillingDataAccess();
                billingTypes = dataAccess.SelectBillingTypeLocation(idLocation, PageIndex, PageSize, ref PageNumber);

                return billingTypes;

            }
            catch (Exception ex)
            {
                throw;
                return null;
            }
        }

        static public List<BillingTypes> ObtainBillingTypesLocation(int idLocation, string filter, int idmodule, bool status, int iot, int PageIndex, int PageSize, ref int PageNumber)
        {
            try
            {

                List<BillingTypes> billingTypes = new List<BillingTypes>();
                BillingDataAccess dataAccess = new BillingDataAccess();
                switch (iot)
                {
                    case 0: billingTypes = dataAccess.SelectBillingTypeLocation(idLocation, filter, idmodule, status, PageIndex, PageSize, ref PageNumber); break;
                    case 1: billingTypes = dataAccess.SelectBillingTypeLocation(idLocation, filter, idmodule, status, false, PageIndex, PageSize, ref PageNumber); break;
                    case 2: billingTypes = dataAccess.SelectBillingTypeLocation(idLocation, filter, idmodule, status, true, PageIndex, PageSize, ref PageNumber); break;
                }
                

                return billingTypes;

            }
            catch (Exception ex)
            {
                throw;
                return null;
            }
        }

        static public List<Rooms> ObtainRooms(int idHotel)
        {
            try
            {
                List<Rooms> rooms = new List<Rooms>();
                BillingDataAccess dataAccess = new BillingDataAccess();
                rooms = dataAccess.SelectRooms(idHotel);

                return rooms;
            }
            catch(Exception ex)
            {
                throw;
                return null;
            }
        }

        static public Rooms GetRoom(int idRoom) 
        {
           try {
               BillingDataAccess dataAccess = new BillingDataAccess();
               Rooms room = new Rooms();
               dataAccess.Select(room, idRoom);

               return room;

           }
            catch(Exception ex) {
                throw;
            }
        }

        static public Rooms GetRoom(int idHotel, string name)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                Rooms room = new Rooms();
                dataAccess.Select(room, idHotel, name);

                return room;

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        static public bool SaveBilling(Billings obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.IdBilling.Equals(0))
                    dataAccess.Insert(obj);
                else
                    dataAccess.Update(obj);

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
        }

        static public bool SaveFastTicket(FastTicket obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.IdFastTicket.Equals(0))
                    dataAccess.Insert(obj);
                //else
                  //  dataAccess.Update(obj);

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
        }

        static public bool SaveCaptivePortalLog(CaptivePortalLog obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                dataAccess.Insert(obj);
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        static public bool SaveCaptivePortalLogInternal(CaptivePortalLogInternal obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                dataAccess.Insert(obj);
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
        }

        static public bool SaveUser(Users obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.IdUser.Equals(0))
                    dataAccess.Insert(obj);
                else
                    dataAccess.Update(obj);

                return true;
            }
            catch (Exception ex)
            {
                throw;
                return false;
                
            }
        }

        static public bool SaveUserInactive(RegisterUsers obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.IdUser.Equals(0))
                    dataAccess.Insert(obj);
                else
                    dataAccess.Update(obj);

                return true;
            }
            catch (Exception ex)
            {
                throw;

            }
        }

        static public bool UpdateUser(Users obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                dataAccess.Update(obj);

                return true;
            }
            catch (Exception ex)
            {
                throw;
                return false;

            }
        }

        static public bool SaveFDSUser(FDSUsers obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.IdUser.Equals(0))
                    dataAccess.Insert(obj);
                else
                    dataAccess.Update(obj);

                return true;
            }
            catch (Exception ex)
            {
                throw;
                return false;

            }
        }

        static public bool SaveFDSUserAccess(FDSUsersAccess obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.Id.Equals(0))
                    dataAccess.Insert(obj);
                else
                    dataAccess.Update(obj);

                return true;
            }
            catch (Exception ex)
            {
                throw;
                return false;

            }
        }

        static public void SaveBillingType(BillingTypes obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.IdBillingType.Equals(0))
                    dataAccess.Insert(obj);
                else
                    dataAccess.Update(obj);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        static public void SaveBillingType(BillingTypes2 obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.IdBillingType.Equals(0))
                    dataAccess.Insert(obj);
                else
                    dataAccess.Update(obj);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        static public void SaveHotel(Hotels obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.IdHotel.Equals(0))
                    dataAccess.Insert(obj);
                else
                    dataAccess.Update(obj);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        static public void SaveLocationSocialNetwork(LocationsSocialNetworks obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.IdLocationSocialNetwork.Equals(0))
                    dataAccess.Insert(obj);
                else
                    dataAccess.Update(obj);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        static public void SaveSiteSocialNetwork(SitesSocialNetworks obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.IdSiteSocialNetwork.Equals(0))
                    dataAccess.Insert(obj);
                else
                    dataAccess.Update(obj);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        static public void SaveDisclaimer(Disclaimers obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                //if (obj.IdDisclaimer.Equals(0))
                    dataAccess.Insert(obj);
                //else
                //    dataAccess.Update(obj);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static List<Billings> ObtainBillings(int idSite)
        {
            List<Billings> Billings = new List<Billings>();
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                return dataAccess.SelectBillings(idSite);
            }
            catch (Exception ex)
            {
                throw;
            }          
        }

        public static List<Billings> ObtainBillingFromSite(int idSite, int idLocation)
        {
            List<Billings> Billings = new List<Billings>();
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                return dataAccess.SelectBillingsSiteLocation(idSite, idLocation);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        static public void SaveFacebookPromotion(FacebookPromotions obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.IdFacebookPromotion.Equals(0))
                    dataAccess.Insert(obj);
                else
                    dataAccess.Update(obj);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        static public void SaveLocation(Locations2 obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.IdLocation.Equals(0))
                    dataAccess.Insert(obj);
                else
                    dataAccess.Update(obj);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        
        static public void SaveLocationText(LocationsTexts obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.IdLocationText.Equals(0))
                    dataAccess.Insert(obj);
                else
                    dataAccess.Update(obj);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        static public void SavePriority(Priorities obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.IdPriority.Equals(0))
                    dataAccess.Insert(obj);
                else
                    dataAccess.Update(obj);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        static public void SaveSurvey(Surveys2 obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.IdSurvey.Equals(0))
                    dataAccess.Insert(obj);
                else
                    dataAccess.Update(obj);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        static public void SaveCheckNumberOracle(CheckNumberOracle obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                dataAccess.Insert(obj);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        
        static public void SaveLocationModuleText(LocationModuleText obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.IdLocationModuleText.Equals(0))
                    dataAccess.Insert(obj);
                else
                    dataAccess.Update(obj);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        static public void SaveParametroConfiguracion(ParametrosConfiguracion obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.IdParameter.Equals(0))
                    dataAccess.Insert(obj);
                else
                    dataAccess.Update(obj);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        static public ActiveSessions GetActiveSession(string AcctSessionID)
        {
            ActiveSessions obj = new ActiveSessions();
            BillingDataAccess dataAccess = new BillingDataAccess();
            dataAccess.Select(AcctSessionID, obj);

            return obj;
        }

        static public ActiveSessions GetActiveSessionMAC(string mac)
        {
            ActiveSessions obj = new ActiveSessions();
            BillingDataAccess dataAccess = new BillingDataAccess();
            dataAccess.Select(obj, mac);

            return obj;
        }

        static public ActiveSessions GetActiveSession(int idActiveSession)
        {
            ActiveSessions obj = new ActiveSessions();
            BillingDataAccess dataAccess = new BillingDataAccess();
            dataAccess.Select(idActiveSession, obj);

            return obj;
        }

        static public ActiveSessions GetActiveSession(string MAC, int idhotel)
        {
            ActiveSessions obj = new ActiveSessions();
            BillingDataAccess dataAccess = new BillingDataAccess();
            dataAccess.Select(MAC, idhotel, obj);

            return obj;
        }

        static public List<ActiveSessions> GetActiveSessions(int idHotel, DateTime start)
        {
            List<ActiveSessions> list = new List<ActiveSessions>();
            BillingDataAccess dataAccess = new BillingDataAccess();
            list = dataAccess.SelectActiveSession(idHotel, start);

            return list;
        }

        static public List<Users> GetAllUsersLocation(int idLocation) ///SEGUIR POR AQUI
        {
            List<Users> list = new List<Users>();
            BillingDataAccess dataAccess = new BillingDataAccess();
            list = dataAccess.SelectAllUsersLocation(idLocation);

            return list;
        }

        static public Users GetRadiusUser(string username)
        {
            Users user = new Users();
            BillingDataAccess dataAccess = new BillingDataAccess();
            dataAccess.Select(user, username);

            return user;
        }

        static public Users GetRadiusUser(string username, int idHotel)
        {
            Users user = new Users();
            BillingDataAccess dataAccess = new BillingDataAccess();
            dataAccess.Select(user, username, idHotel);

            return user;
        }

        static public Users GetRadiusUsermac(string mac)
        {
            Users user = new Users();
            BillingDataAccess dataAccess = new BillingDataAccess();
            dataAccess.SelectMac(user, mac);

            return user;
        }

        static public Users GetRadiusUsermac(string mac, int idhotel)
        {
            Users user = new Users();
            BillingDataAccess dataAccess = new BillingDataAccess();
            dataAccess.SelectMac(user, mac, idhotel);

            return user;
        }

        static public Users GetRadiusUsermac(string mac, int idhotel, int idbillingmodule)
        {
            Users user = new Users();
            BillingDataAccess dataAccess = new BillingDataAccess();
            dataAccess.SelectMac(user, mac, idhotel, idbillingmodule);

            return user;
        }

        static public Users GetRadiusUser(int idUser)
        {
            Users user = new Users();
            BillingDataAccess dataAccess = new BillingDataAccess();
            dataAccess.Select(user, idUser);

            return user;
        }

        static public UserAll GetRadiusUserAll(int idUser)
        {
            UserAll user = new UserAll();
            BillingDataAccess dataAccess = new BillingDataAccess();
            dataAccess.Select(user, idUser);

            return user;
        }

        static public List<UserAll> GetRadiusUsers(int idhotel, string username, DateTime validsince)
        {
            BillingDataAccess dataAcccess = new BillingDataAccess();
            return dataAcccess.SelectRadiusUsers(idhotel, username, validsince);
        }

        static public RememberMeTable GetRememberMeTableUser(string mac, int idhotel)
        {
            RememberMeTable user = new RememberMeTable();
            BillingDataAccess dataAccess = new BillingDataAccess();
            dataAccess.Select(user, mac, idhotel);

            return user;
        }

        static public bool DeleteRememberMeTableUser(RememberMeTable obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                dataAccess.DeleteRememberMeTable(obj.IdRememberMe);

                return true;
            }
            catch
            {
                return false;
            }
        }

        static public bool DeleteRememberMeTableUser(string mac, int idhotel)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                dataAccess.DeleteRememberMeTable(mac, idhotel);

                return true;
            }
            catch
            {
                return false;
            }
        }

        static public List<Accounting> OnlineUsers(int idHotel)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.OnlineUsers(idHotel);
        }

        static public List<UsersOnlineInfo> OnlineUsers(int idHotel, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.OnlineUsers(idHotel, pageIndex, pageSize, ref PageNumber);
        }

        static public List<UsersOnlineInfo> OnlineUsersFilter(int idHotel, bool? iot, string username, int? idbillingtype, int? idprioriy, int? idfilter, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.OnlineUsersFilter(idHotel, username, iot, idbillingtype, idprioriy, idfilter, pageIndex, pageSize, ref PageNumber);
        }

        static public List<UsersOnlineInfo> OnlineUsersFilter(int idHotel, bool? iot, string username, int? idbillingtype, int? idprioriy, int? idfilter, int idfdsuser, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.OnlineUsersFilter(idHotel, username, iot, idbillingtype, idprioriy, idfilter, idfdsuser, pageIndex, pageSize, ref PageNumber);
        }

        static public List<UsersOnlineInfo> OnlineUsersFilter(int idHotel, List<FDSUsers> users, bool? iot, string username, int? idbillingtype, int? idprioriy, int? idfilter, int idfdsuser, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.OnlineUsersFilter(idHotel, users, username, iot, idbillingtype, idprioriy, idfilter, idfdsuser, pageIndex, pageSize, ref PageNumber);
        }

        static public List<UsersOnlineInfo> OnlineUsersFilter(string listhoteles, bool? iot, string username, int? idbillingtype, int? idprioriy, int? idfilter, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.OnlineUsersFilter(listhoteles, username, iot, idbillingtype, idprioriy, idfilter, pageIndex, pageSize, ref PageNumber);
        }

        static public List<UsersOnlineInfo> OnlineUsersFilter(string locationIdentifier, bool? iot, string username, int? idbillingtype, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.OnlineUsersFilter(locationIdentifier, username, iot, idbillingtype, pageIndex, pageSize, ref PageNumber);
        }

        static public List<UsersOnlineInfo> OnlineUsersFilter(string locationIdentifier, bool? iot, string username, int? idbillingtype, int idfdsuser, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.OnlineUsersFilter(locationIdentifier, username, iot, idbillingtype, idfdsuser, pageIndex, pageSize, ref PageNumber);
        }

        static public List<UsersOnlineInfo> OnlineUsersFilter(string locationIdentifier, List<FDSUsers> users, bool? iot, string username, int? idbillingtype, int idfdsuser, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.OnlineUsersFilter(locationIdentifier, users, username, iot, idbillingtype, idfdsuser, pageIndex, pageSize, ref PageNumber);
        }


        static public List<UsersOnlineInfo> OnlineUsers(string locationIdentifier, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.OnlineUsers(locationIdentifier, pageIndex, pageSize, ref PageNumber);
        }

        static public List<UsersOnlineInfo> OnlineUsersIoT(int idHotel, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.OnlineUsersIoT(idHotel, pageIndex, pageSize, ref PageNumber);
        }

        static public List<UsersOnlineInfo> OnlineUsersIoT(string locationIdentifier, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.OnlineUsersIoT(locationIdentifier, pageIndex, pageSize, ref PageNumber);
        }

        static public List<ClosedSessions> Activity(int idHotel, string username, int idRoom, DateTime start, DateTime end, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();

            List<ClosedSessions> sessions = new List<ClosedSessions>();
            List<ClosedSessions> sessionsResponse = new List<ClosedSessions>();

            if (idRoom.Equals(0))
            {
                List<Users> users = dataAccess.UsuariosPeriodo(idHotel,username, start, end);
                foreach (Users u in users)
                {
                    List<ClosedSessions> aux = dataAccess.SessionUser(u.Name);

                    foreach (ClosedSessions c in aux)
                    {
                        if (c.SessionStarted >= start)
                        {
                            ClosedSessions obj = new ClosedSessions();
                            Util.Map(c, obj);
                            sessions.Add(obj);
                        }
                    }
                }
            }
            else
            {
                List<Users> users = dataAccess.UsuariosPeriodoHabitacion(idHotel, idRoom, username, start, end);
                foreach (Users u in users)
                {
                    List<ClosedSessions> aux = dataAccess.SessionUser(u.Name);

                    foreach (ClosedSessions c in aux)
                    {
                        if (c.SessionStarted >= start)
                        {
                            ClosedSessions obj = new ClosedSessions();
                            Util.Map(c, obj);
                            sessions.Add(obj);
                        }
                    }
                }
            }
            return sessions;
        }

        static public List<Users> GetActiveUsers(int idHotel)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetActiveUsers(idHotel);
        }

        static public List<Users> GetActiveUsersLocation(int idLocation)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetActiveUsersLocation(idLocation);
        }

        static public List<Users> GetDisabledUsers(int idHotel, DateTime date, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetDisabledUsers(idHotel, date, pageIndex, pageSize, ref PageNumber);
        }

        static public List<Users> GetDisabledUsers(int idHotel, string username, DateTime start, DateTime end, int billing, int room, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetDisabledUsers(idHotel, username, start, end, billing, room, pageIndex, pageSize, ref PageNumber);
        }

        //static public List<Users> GetDisabledUsers(int idHotel, string username, DateTime start, DateTime end, int billing, int room, int idfdsuser, int pageIndex, int pageSize, ref int PageNumber)
        //{
        //    BillingDataAccess dataAccess = new BillingDataAccess();
        //    return dataAccess.GetDisabledUsers(idHotel, username, start, end, billing, room, idfdsuser, pageIndex, pageSize, ref PageNumber);
        //}

        static public List<Users> GetDisabledUsers(int idHotel, List<FDSUsers> users, string username, DateTime start, DateTime end, int billing, int room, int idfdsuser, int iot, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetDisabledUsers(idHotel, users, username, start, end, billing, room, idfdsuser, iot, pageIndex, pageSize, ref PageNumber);
        }

        static public List<Users> GetDisabledUsers(string listadohoteles, string username, DateTime start, DateTime end, int billing, int room, int iot, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetDisabledUsers(listadohoteles, username, start, end, billing, room, iot, pageIndex, pageSize, ref PageNumber);
        }

        static public List<Users> GetDisabledUsersIoT(int idHotel, string username, DateTime start, DateTime end, int billing, int room, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetDisabledUsersIoT(idHotel, username, start, end, billing, room, pageIndex, pageSize, ref PageNumber);
        }

        static public List<Users> GetDisabledUsersIoT(string listHoteles, string username, DateTime start, DateTime end, int billing, int room, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetDisabledUsersIoT(listHoteles, username, start, end, billing, room, pageIndex, pageSize, ref PageNumber);
        }

        static public List<Users> GetDisabledUsersLocations(int idLocation, string username, DateTime start, DateTime end, int billing, int room, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetDisabledUsersLocation(idLocation, username, start, end, billing, room, pageIndex, pageSize, ref PageNumber);
        }

        static public List<Users> GetDisabledUsersLocations(int idLocation, string username, DateTime start, DateTime end, int billing, int room, int idfdsuser, int iot, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetDisabledUsersLocation(idLocation, username, start, end, billing, room, idfdsuser, iot, pageIndex, pageSize, ref PageNumber);
        }

        static public List<Users> GetDisabledUsersLocations(int idLocation, List<FDSUsers> users, string username, DateTime start, DateTime end, int billing, int room, int idfdsuser, int iot, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetDisabledUsersLocation(idLocation, users, username, start, end, billing, room, idfdsuser, iot, pageIndex, pageSize, ref PageNumber);
        }


        static public List<Users> GetDisabledUsersLocationsIoT(int idLocation, string username, DateTime start, DateTime end, int billing, int room, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetDisabledUsersLocationIoT(idLocation, username, start, end, billing, room, pageIndex, pageSize, ref PageNumber);
        }

        static public List<UserInfo> GetActiveUsers(int idHotel, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetActiveUsers(idHotel, pageIndex, pageSize, ref PageNumber);
        }

        static public List<UserInfo> GetActiveUsers(int idHotel, string username, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetActiveUsers(idHotel, username, pageIndex, pageSize, ref PageNumber);
        }

        static public List<UserInfo> GetActiveUsers(int idHotel, string username, DateTime start, DateTime end, int billing, int room, int priority, int filterprofile, int? iot, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetActiveUsers(idHotel, username, start, end, billing, room, priority, filterprofile, iot, pageIndex, pageSize, ref PageNumber);
        }

        static public List<UserInfo> GetActiveUsers(int idHotel, string username, DateTime start, DateTime end, int billing, int room, int priority, int filterprofile, int? iot, int idfdsuser, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetActiveUsers(idHotel, username, start, end, billing, room, priority, filterprofile, iot, idfdsuser, pageIndex, pageSize, ref PageNumber);
        }

        static public List<UserInfo> GetActiveUsers(int idHotel, List<FDSUsers> users,  string username, DateTime start, DateTime end, int billing, int room, int priority, int filterprofile, int? iot, int idfdsuser, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetActiveUsers(idHotel, users, username, start, end, billing, room, priority, filterprofile, iot, idfdsuser, pageIndex, pageSize, ref PageNumber);
        }

        static public List<UserInfo> GetActiveUsers(string listadohoteles, string username, DateTime start, DateTime end, int billing, int room, int priority, int filterprofile, int? iot, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetActiveUsers(listadohoteles, username, start, end, billing, room, priority, filterprofile, iot, pageIndex, pageSize, ref PageNumber);
        }

        static public List<UserInfo> GetActiveUsersIoT(int idHotel, string username, DateTime start, DateTime end, int billing, int room, int priority, int filterprofile, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetActiveUsersIoT(idHotel, username, start, end, billing, room, priority, filterprofile, pageIndex, pageSize, ref PageNumber);
        }

        static public List<UserInfo> GetActiveUsersLocation(int idlocation, string username, DateTime start, DateTime end, int billing, int room, int priority, int filterprofile, int? iot, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetActiveUsersLocation(idlocation, username, start, end, billing, room, priority, filterprofile, iot, pageIndex, pageSize, ref PageNumber);
        }

        static public List<UserInfo> GetActiveUsersLocation(int idlocation, string username, DateTime start, DateTime end, int billing, int room, int priority, int filterprofile, int? iot, int idfdsuser, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetActiveUsersLocation(idlocation, username, start, end, billing, room, priority, filterprofile, iot, idfdsuser, pageIndex, pageSize, ref PageNumber);
        }

        static public List<UserInfo> GetActiveUsersLocation(int idlocation, List<FDSUsers> users, string username, DateTime start, DateTime end, int billing, int room, int priority, int filterprofile, int? iot, int idfdsuser, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetActiveUsersLocation(idlocation, users, username, start, end, billing, room, priority, filterprofile, iot, idfdsuser, pageIndex, pageSize, ref PageNumber);
        }

        static public List<UserInfo> GetUsersLocation(int idlocation, bool enabled, string username, DateTime start, DateTime end, int billing, int room, int priority, int filterprofile, int? iot, int idfdsuser, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetUsersLocation(idlocation, enabled, username, start, end, billing, room, priority, filterprofile, iot, idfdsuser, pageIndex, pageSize, ref PageNumber);
        }

        static public List<UserInfo> GetActiveUsersLocationIoT(int idlocation, string username, DateTime start, DateTime end, int billing, int room, int priority, int filterprofile, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetActiveUsersLocationIoT(idlocation, username, start, end, billing, room, priority, filterprofile, pageIndex, pageSize, ref PageNumber);
        }

        static public List<UserInfo> GetActiveUsers(int idHotel, int idbillingtype, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetActiveUsers(idHotel, idbillingtype, pageIndex, pageSize, ref PageNumber);
        }

        static public List<UserInfo> GetActiveUsers(int idHotel, string username, int idbillingtype, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetActiveUsers(idHotel, username, idbillingtype, pageIndex, pageSize, ref PageNumber);
        }

        static public List<UserInfo> GetVouchersUsers(int idHotel, string username, int billing, DateTime start, DateTime end, bool? enabled, bool? billable, int idfdsuser, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetVouchersUsers(idHotel, username, start, end, billing, enabled, billable, idfdsuser, pageIndex, pageSize, ref PageNumber);
        }

        static public List<UserInfo> GetVouchersUsersLocation(int idlocation, string username, int billing, DateTime start, DateTime end, bool? enabled, bool? billable, int idfdsuser, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetVouchersUsersLocation(idlocation, username, start, end, billing, enabled, billable, idfdsuser, pageIndex, pageSize, ref PageNumber);
        }

        static public List<UserInfo> GetVouchersUsers(string listadohoteles, string username, int billing, DateTime start, DateTime end, bool? enabled, bool? billable, int idfdsuser, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetVouchersUsers(listadohoteles, username, start, end, billing, enabled, billable, idfdsuser, pageIndex, pageSize, ref PageNumber);
        }

        static public List<FailedRequests> GetFailedRequest(int idHotel, string start, string end, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetFailedRequests(idHotel, start, end, pageIndex, pageSize, ref PageNumber);
        }

        static public FailedRequests GetFailedRequest(int idHotel, string username, string mac)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetFailedRequests(idHotel, username, mac);
        }

        static public List<FailedRequests> GetFailedRequestHoteles(string listadoHoteles, string start, string end, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetFailedRequestsHoteles(listadoHoteles, start, end, pageIndex, pageSize, ref PageNumber);
        }

        static public List<FailedRequests> GetFailedRequest(string locationIdentifier, string start, string end, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetFailedRequests(locationIdentifier, start, end, pageIndex, pageSize, ref PageNumber);
        }

        static public List<Users> GetUsersPeriod(int idHotel, DateTime start, DateTime end) 
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetUsuariosPeriodo(idHotel, start, end);
        }

        static public List<Users> GetUsersPeriod(int idHotel, DateTime start, DateTime end, string username, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetUsuariosPeriodo(idHotel, start, end, username, pageIndex, pageSize, ref PageNumber);
        }

        static public List<Users> GetUsersPeriod(string listadoHoteles, DateTime start, DateTime end, string username, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetUsuariosPeriodo(listadoHoteles, start, end, username, pageIndex, pageSize, ref PageNumber);
        }

        static public List<Users> GetUsersPeriodLocation(int idlocation, int idhotel, DateTime start, DateTime end, string username, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetUsuariosPeriodoLocation(idlocation, idhotel, start, end, username, pageIndex, pageSize, ref PageNumber);
        }

        static public List<Accounting> AccountingStart(int idHotel, DateTime start)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetAccountingStart(idHotel, start);
        }

        static public List<Accounting> Accounting(string user, DateTime start)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetAccounting(user, start);
        }

        static public List<Accounting> Accounting(string user, DateTime start, string mac)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetAccounting(user, start, mac);
        }

        static public ActiveSessions GetActiveSession(string mac, string username)
        {
            ActiveSessions active = new ActiveSessions();
            BillingDataAccess data = new BillingDataAccess();
            return data.GetActiveSession(mac, username);
        }

        static public FailedRequests GetFailedRequest(string mac, string username, DateTime time)
        {
            FailedRequests active = new FailedRequests();
            BillingDataAccess data = new BillingDataAccess();
            return data.GetFailedRequest(mac, username, time);
        }

        static public bool SaveSessionLog(SessionsLog obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.IdCount.Equals(0))
                    dataAccess.Insert(obj);
                else
                    dataAccess.Update(obj);

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
        }

        static public List<SessionsLog> GetSessionsLog(int idHotel)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetSessionLogs(idHotel);
        }

        static public List<SessionsLog> GetLogPeriod(int idHotel, DateTime start, DateTime end)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetSessionLogs(idHotel, start, end);
        }

        static public List<SessionsLog> GetLogPeriodLocation(int idlocation, DateTime start, DateTime end)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.GetSessionLogsLocation(idlocation, start, end);
        }

        static public Hotels GetHotel(string nseid)
        {
            Hotels hotel = new Hotels();
            BillingDataAccess dataAcess = new BillingDataAccess();
            dataAcess.Select(hotel, nseid);

            return hotel;
        }

        static public Hotels GetHotel(int idHotel)
        {
            Hotels hotel = new Hotels();
            BillingDataAccess dataAcess = new BillingDataAccess();
            dataAcess.Select(hotel, idHotel);

            return hotel;
        }

        static public FDSGroups GetGroup(int idGroup)
        {
            FDSGroups group = new FDSGroups();
            BillingDataAccess dataAcess = new BillingDataAccess();
            dataAcess.Select(group, idGroup);

            return group;
        }

        static public FDSGroups GetGroupShareRealm(int idSite)
        {
            FDSGroups group = new FDSGroups();
            BillingDataAccess dataAcess = new BillingDataAccess();
            dataAcess.SelectRealmShareGroup(group, idSite);

            return group;
        }

        static public List<Hotels> GetHotels(string nseid)
        {
            BillingDataAccess dataAcess = new BillingDataAccess();
            return dataAcess.SelectHoteles(nseid);
        }

        static public List<Hotels> GetSitesByGroup(int idGroup)
        {
            BillingDataAccess dataAcess = new BillingDataAccess();
            return dataAcess.SelectSitesGroup(idGroup);
        }

        static public List<Hotels> GetSitesByParent(int idParent)
        {
            BillingDataAccess dataAcess = new BillingDataAccess();
            return dataAcess.SelectSitesByParent(idParent);
        }

        static public Priorities GetPriority(int idPriority)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            Priorities p = new Priorities();
            dataAccess.Select(p, idPriority);
            return p;
        }
        
        static public List<Priorities> GetPriorities(int idHotel)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.SelectPriorities(idHotel);
        }

        static public List<Priorities> GetPriorities(string listadoHoteles)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.SelectPriorities(listadoHoteles);
        }

        static public List<Locations2> GetLocations(int idHotel)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.SelectLocations(idHotel);
        }

        static public Locations2 GetLocation(int idLocation)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.SelectLocation(idLocation);
        }

        static public List<Loyalties2> GetLoyaties(int idHotel, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.SelectLoyalties(idHotel, pageIndex, pageSize, ref PageNumber);
        }

        static public List<Loyalties2> GetLoyaties(int idHotel, DateTime start, DateTime end)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.SelectLoyalties(idHotel, start, end);
        }

        static public List<Loyalties2> GetLoyaties(string listadoHoteles, DateTime start, DateTime end)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.SelectLoyalties(listadoHoteles, start, end);
        }

        static public List<Loyalties2> GetLoyatiesLocation(int idLocation, DateTime start, DateTime end)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.SelectLoyaltiesLocation(idLocation, start, end);
        }

        static public List<Loyalties2> GetAllLoyatiesLocation(int idLocation)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.SelectAllLoyaltiesLocation(idLocation);
        }

        static public List<Loyalties2> GetLoyaties(int idHotel, DateTime start, DateTime end, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.SelectLoyalties(idHotel, start, end, pageIndex, pageSize, ref PageNumber);
        }

        static public List<Loyalties2> GetLoyaties(string listadoHoteles, DateTime start, DateTime end, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.SelectLoyalties(listadoHoteles, start, end, pageIndex, pageSize, ref PageNumber);
        }

        static public List<Loyalties2> GetLoyatiesLocation(int idLocation, DateTime start, DateTime end, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.SelectLoyaltiesLocation(idLocation, start, end, pageIndex, pageSize, ref PageNumber);
        }

        static public List<Loyalties2> GetLoyatiesMacLocation(string mac, int idLocation)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.SelectLoyaltiesMACLocation(mac, idLocation);
        }


        static public List<ClosedSessions2> GetClosedSession(int idHotel, DateTime time)
        {
            BillingDataAccess dataAcess = new BillingDataAccess();
            return dataAcess.SelectClossedSession(idHotel, time);
        }

        static public List<ClosedSessions2> GetClosedSessionLocation(int idLocation, DateTime time)
        {
            BillingDataAccess dataAcess = new BillingDataAccess();
            return dataAcess.SelectClossedSessionLocation(idLocation, time);
        }

        static public List<ClosedSessions2> GetAllClosedSessionSite(int idSite)
        {
            BillingDataAccess dataAcess = new BillingDataAccess();
            return dataAcess.SelectAllClossedSessionSite(idSite);
        }

        static public List<ClosedSessions2> GetClosedSession(string mac, DateTime time)
        {
            BillingDataAccess dataAcess = new BillingDataAccess();
            return dataAcess.SelectClossedSession(mac, time);
        }

        static public List<ClosedSessions2> GetClosedSession(int idHotel, string username, DateTime start, DateTime end, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAcess = new BillingDataAccess();
            return dataAcess.SelectClossedSession(idHotel, username, start, end, pageIndex, pageSize, ref PageNumber);
        }

        static public List<ClosedSessions2> GetClosedSessionListadoHoteles(string listadoHoteles, string username, DateTime start, DateTime end, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAcess = new BillingDataAccess();
            return dataAcess.SelectClossedSessionListadoHoteles(listadoHoteles, username, start, end, pageIndex, pageSize, ref PageNumber);
        }

        static public List<ClosedSessions2> GetClosedSessionListadoHoteles(string listadoHoteles, string username, DateTime start, DateTime end)
        {
            BillingDataAccess dataAcess = new BillingDataAccess();
            return dataAcess.SelectClossedSessionListadoHoteles(listadoHoteles, username, start, end);
        }


        static public List<ClosedSessions2> GetClosedSession(string locationIdentifier, int idhotel, string username, DateTime start, DateTime end, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAcess = new BillingDataAccess();
            return dataAcess.SelectClossedSession(locationIdentifier, idhotel , username, start, end, pageIndex, pageSize, ref PageNumber);
        }

        static public List<ClosedSessions2> GetClosedSession(string locationIdentifier, int idhotel, string username, DateTime start, DateTime end)
        {
            BillingDataAccess dataAcess = new BillingDataAccess();
            return dataAcess.SelectClossedSession(locationIdentifier, idhotel, username, start, end);
        }

        static public List<ClosedSessions2> GetClosedSession(int idHotel, string username, DateTime start, DateTime end)
        {
            BillingDataAccess dataAcess = new BillingDataAccess();
            return dataAcess.SelectClossedSession(idHotel, username, start, end);
        }

        static public List<CaptivePortalLogInfo> GetCaptivePortalLog(int idHotel, DateTime start, DateTime end, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAcess = new BillingDataAccess();
            return dataAcess.ObtainCaptivePortalLog(idHotel, start, end, pageIndex, pageSize, ref PageNumber);
        }

        static public List<CaptivePortalLogInfo> GetCaptivePortalLog(string listhoteles, DateTime start, DateTime end, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAcess = new BillingDataAccess();
            return dataAcess.ObtainCaptivePortalLog(listhoteles, start, end, pageIndex, pageSize, ref PageNumber);
        }

        static public List<CaptivePortalLogInfo> GetCaptivePortalLogLocation(int idLocation, DateTime start, DateTime end, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAcess = new BillingDataAccess();
            return dataAcess.ObtainCaptivePortalLogLocation(idLocation, start, end, pageIndex, pageSize, ref PageNumber);
        }

        static public PayPalTransactions GetPayPalTransaction(int idTransaction)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            PayPalTransactions obj = new PayPalTransactions();
            dataAccess.Select(obj, idTransaction);

            return obj;
        }

        static public PayPalTransactions GetLastTransaction()
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            PayPalTransactions obj = new PayPalTransactions();
            dataAccess.SelectLast(obj);

            return obj;
        }

        static public bool SavePayPalTransaction(PayPalTransactions obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.IdTransaction.Equals(0))
                    dataAccess.Insert(obj);
                else
                    dataAccess.Update(obj);

                return true;
            }
            catch
            {
                return false;
            }
        }

        static public bool SaveLoyalty(Loyalties2 obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.IdLoyalty.Equals(0))
                    dataAccess.Insert(obj);
                else
                    dataAccess.Update(obj);

                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Devuelve los cargos nuevos realizados, tienen el flag a 0
        /// </summary>
        /// <returns></returns>
        static public List<Billings> SelectNewBillings()
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.SelectNewBillings();
        }

        /// <summary>
        /// Devuelve los cargos que han sido leidos pero no tenemos respuesta de la PMS
        /// </summary>
        /// <returns></returns>
        static public List<Billings> SelectReadBillings()
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.SelectReadBillings();
        }

        static public int BWUsed(int idHotel)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.BWUsed(idHotel);
        }

        static public int BWUsedDown(int idHotel)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.BWUsedDown(idHotel);
        }

        static public int BWUsedLocation(int idLocation)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.BWUsedLocation(idLocation);
        }

        static public int BWUsedDownLocation(int idLocation)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.BWUsedDownLocation(idLocation);
        }

        static public List<Languages> GetLanguages()
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.SelectLanguages();
        }

        static public Languages GetLanguage(string code)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            Languages obj = new Languages();
            dataAccess.Select(obj, code);
            return obj;
        }

        static public Languages GetLanguage(int idlanguaje)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            Languages obj = new Languages();
            dataAccess.Select(obj, idlanguaje);
            return obj;
        }

        static public Disclaimers GetDisclaimer(int idDisclaimer)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            Disclaimers obj = new Disclaimers();
            dataAccess.Select(obj, idDisclaimer);
            return obj;
        }
        
        static public Disclaimers GetDisclaimer(int idHotel, int idLanguage)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            Disclaimers obj = new Disclaimers();
            dataAccess.Select(obj, idHotel, idLanguage);
            return obj;
        }

        static public Disclaimers GetDisclaimer(int idHotel, int idlocation, int idLanguage)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            Disclaimers obj = new Disclaimers();
            dataAccess.Select(obj, idHotel, idlocation, idLanguage);
            return obj;
        }

        static public Disclaimers GetDisclaimer(int idHotel, int idlocation, int idLanguage, int idtype)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            Disclaimers obj = new Disclaimers();
            dataAccess.Select(obj, idHotel, idlocation, idLanguage, idtype);
            return obj;
        }

        static public List<Disclaimers> GetDisclaimers(int idLocation)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            List<Disclaimers> obj = new List<Disclaimers>();
            obj = dataAccess.SelectDisclaimers(idLocation);
            return obj;
        }

        static public Surveys2 GetSurvey(int idSurvey)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            Surveys2 obj = new Surveys2();
            dataAccess.Select(obj, idSurvey);
            return obj;
        }
        
        static public List<Surveys2> GetSurveys(int idHotel)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.SelectSurvey(idHotel);
        }

        static public List<Surveys2> GetSurveys(int idHotel, int idlanguage)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.SelectSurvey(idHotel, idlanguage);
        }

        static public List<Surveys2> GetSurveyLocation(int idLocation)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.SelectSurveyLocation(idLocation);
        }

        static public List<Surveys2> GetSurveyLocation(int idLocation, int idlanguage)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.SelectSurveyLocation(idLocation, idlanguage);
        }

        static public UserAgents GetUserAgent(string cadena)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            UserAgents obj = new UserAgents();
            dataAccess.Select(obj, cadena);
            return obj;
        }

        static public UserAgents SalvarTransaccionado(string cadena)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            UserAgents obj = new UserAgents();
            dataAccess.ActualizarTransaccionado(obj, cadena);
            return obj;
        }

        static public UserAgentsTemp GetUserAgentTemp(string cadena)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            UserAgentsTemp obj = new UserAgentsTemp();
            dataAccess.Select(obj, cadena);
            return obj;
        }

        static public UserAgents GetUserAgent(int idUserAgent)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            UserAgents obj = new UserAgents();
            dataAccess.Select(obj, idUserAgent);
            return obj;
        }

        static public bool SaveUserAgent(UserAgents obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.IdUserAgent.Equals(0))
                    dataAccess.Insert(obj);
                else
                    dataAccess.Update(obj);

                return true;
            }
            catch
            {
                return false;
            }
        }

        static public bool SaveUserAgentTemp(UserAgentsTemp obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.IdTemp.Equals(0))
                    dataAccess.Insert(obj);

                return true;
            }
            catch
            {
                return false;
            }
        }

        static public bool DeleteUserAgentTemp(UserAgentsTemp obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                dataAccess.Delete(obj);

                return true;
            }
            catch
            {
                return false;
            }
        }

        static public bool DeleteSurveyQuestion(Surveys2 obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                dataAccess.Delete(obj);

                return true;
            }
            catch
            {
                return false;
            }
        }

        static public BlackListUserAgents GetBlackUserAgent(string cadena)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            BlackListUserAgents obj = new BlackListUserAgents();
            dataAccess.Select(obj, cadena);
            return obj;
        }

        static public bool SaveBlackListUserAgent(BlackListUserAgents obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                dataAccess.Insert(obj);

                return true;
            }
            catch
            {
                return false;
            }
        }

        static public BlackListMACs GetBlackListMac(string mac, DateTime now)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            BlackListMACs obj = new BlackListMACs();
            dataAccess.Select(obj, mac, now);
            return obj;
        }

        static public bool SaveBlackListMac(BlackListMACs obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                dataAccess.Insert(obj);

                return true;
            }
            catch
            {
                return false;
            }
        }

        static public int GetUserAgentsTemps(string mac, int interval)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.SelectUserAgentTemps(mac,interval);
        }

        static public List<UserAgents> GetUserAgents(string cadena)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.SelectUserAgents(cadena);
        }

        static public List<UserAgents> GetUserAgentsToBlackList(int max)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.SelectUserAgentsToBlackList(max);
        }

        static public List<BlackListMACs> GetBlacListMac(int idHotel, DateTime start, DateTime end, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.SelectBlackListMACs(idHotel, start, end, pageIndex, pageSize, ref PageNumber);
        }

        static public List<BlackListMACs> GetBlacListMac(string listadoHoteles, DateTime start, DateTime end, int pageIndex, int pageSize, ref int PageNumber)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.SelectBlackListMACs(listadoHoteles, start, end, pageIndex, pageSize, ref PageNumber);
        }

        static public bool DelteBlackListMac(int idMac)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                dataAccess.DeleteMACBlackList(idMac);

                return true;
            }
            catch
            {
                return false;
            }
        }

        static public bool DeleteBill(int id)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                dataAccess.DeleteBilling(id);

                return true;
            }
            catch
            {
                return false;
            }
        }

        static public BillingModules ObtainBillingModule(int idBillingModule)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            BillingModules obj = new BillingModules();
            dataAccess.Select(obj, idBillingModule);

            return obj;
        }
        
        static public List<BillingModules> ObtainBillingModules()
        {
            try
            {

                List<BillingModules> billingModules = new List<BillingModules>();
                BillingDataAccess dataAccess = new BillingDataAccess();
                billingModules = dataAccess.SelectBillingModule();

                return billingModules;

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        static public LocationsTexts GetLocationText(int idLocationText)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            LocationsTexts obj = new LocationsTexts();
            dataAccess.Select(obj, idLocationText);
            return obj;
        }
        
        static public LocationsTexts GetLocationText(int idLocation, int idLanguage)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            LocationsTexts obj = new LocationsTexts();
            dataAccess.Select(obj, idLocation, idLanguage);
            return obj;
        }

        static public List<LocationsTexts> GetLocationsText(int idLocation)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            List<LocationsTexts> obj = new List<LocationsTexts>();
            obj = dataAccess.SelectLocationsText(idLocation);
            return obj;
        }

        static public LocationModuleText GetLocationModuleText(int idLocationModuleText)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            LocationModuleText obj = new LocationModuleText();
            dataAccess.Select(obj, idLocationModuleText);
            return obj;
        }
        
        static public LocationModuleText GetLocationModuleText(int idLocation, int idBillingModule, int idLanguage)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            LocationModuleText obj = new LocationModuleText();
            dataAccess.Select(obj, idLocation,idBillingModule, idLanguage);
            return obj;
        }

        static public List<LocationModuleText> GetLocationModuleTexts(int idLocation, int idLanguage)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            List<LocationModuleText> obj = new List<LocationModuleText>();

            return dataAccess.SelectLocationModuleTexts(idLocation, idLanguage);
        }

        static public List<LocationModuleText> GetLocationModuleTexts(int idLocation)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            List<LocationModuleText> obj = new List<LocationModuleText>();

            return dataAccess.SelectLocationModuleTexts(idLocation);
        }

        static public FacebookPromotions GetFacebookPromotion(int idPromotion)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                FacebookPromotions obj = new FacebookPromotions();
                dataAccess.Select(obj, idPromotion);
                return obj;
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static public List<FacebookPromotions> GetFacebookPromotions(int idHotel, int idLocation, bool visible)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                List<FacebookPromotions> list = new List<FacebookPromotions>();
                return dataAccess.SelectFacebookPromotions(idHotel, idLocation,visible);
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static public List<FacebookPromotions> GetFacebookPromotions(int idHotel)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                List<FacebookPromotions> list = new List<FacebookPromotions>();
                return dataAccess.SelectFacebookPromotions(idHotel);
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static public List<FacebookPromotionsLog> GetFacebookPromotionsLog(int idpromotion)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                List<FacebookPromotionsLog> list = new List<FacebookPromotionsLog>();
                return dataAccess.SelectFacebookPromotionsLog(idpromotion);
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static public List<FacebookPromotionsLog> GetFacebookPromotionsLog(string mail)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                List<FacebookPromotionsLog> list = new List<FacebookPromotionsLog>();
                return dataAccess.SelectFacebookPromotionsLog(mail);
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static public FacebookPromotionsLog GetFacebookPromotionLog(string mail, int idpromotion)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                FacebookPromotionsLog obj = new FacebookPromotionsLog();
                dataAccess.Select(obj, idpromotion, mail);
                return obj;
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static public bool SavePromotionLog(FacebookPromotionsLog obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.IdPromotionLog.Equals(0))
                    dataAccess.Insert(obj);
                else
                    dataAccess.Update(obj);

                return true;
            }
            catch
            {
                return false;
            }
        }

        static public CustomAccess GetCustomAccess(int idhotel)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                CustomAccess obj = new CustomAccess();
                dataAccess.Select(obj, idhotel);
                return obj;
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static public bool SaveAppleUrl(AppleUrls obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                    dataAccess.Insert(obj);

                return true;
            }
            catch (Exception ex)
            {
                return false;

            }
        }

        static public double AverageUserUsage(int idhotel)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.AveraUserUsage(idhotel);
        }

        static public double AverageUserUsageLocation(int idLocation)
        {
            BillingDataAccess dataAccess = new BillingDataAccess();
            return dataAccess.AveraUserUsageLocation(idLocation);
        }


        #region ParametrosConfiguracion

        static public List<ParametrosConfiguracion> GetParametrosConfiguracion(int idSite)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                List<ParametrosConfiguracion> list = new List<ParametrosConfiguracion>();
                return dataAccess.SelectParametrosConfiguracion(idSite);
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static public ParametrosConfiguracion GetParametroConfiguracion(string value)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                ParametrosConfiguracion obj = new ParametrosConfiguracion();
                dataAccess.Select(obj, value);
                return obj;
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        #endregion

        #region FilteringProfile

        static public FilteringProfiles GetFilteringProfile(int idFilter)
        {
            FilteringProfiles obj = new FilteringProfiles();
            BillingDataAccess dataAcess = new BillingDataAccess();
            dataAcess.Select(obj, idFilter);

            return obj;
        }

        static public List<FilteringProfiles> GetFilteringProfiles(int idSite)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                List<FilteringProfiles> list = new List<FilteringProfiles>();
                return dataAccess.SelectFilteringProfiles(idSite);
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static public List<FilteringProfiles> GetFilteringProfiles(string listadoHoteles)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                List<FilteringProfiles> list = new List<FilteringProfiles>();
                return dataAccess.SelectFilteringProfiles(listadoHoteles);
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static public bool DeleteFilterProfile(int id)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                dataAccess.DeleteFilterProfile(id);

                return true;
            }
            catch
            {
                return false;
            }
        }

        static public void SaveFilterProfile(FilteringProfiles obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.IdFilter.Equals(0))
                    dataAccess.Insert(obj);
                else
                    dataAccess.Update(obj);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion

        #region SocialNetworks

        static public List<SocialNetworks> GetSocialNetwoks()
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                List<SocialNetworks> list = new List<SocialNetworks>();
                return dataAccess.SelectSocialNetworks();
            }
            catch (Exception ex)
            {
                throw;
            }

        }
        
        static public List<SitesSocialNetworks> GetSiteSocialNetwoks(int idSite)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                List<SitesSocialNetworks> list = new List<SitesSocialNetworks>();
                return dataAccess.SelectSitesSocialNetworks(idSite);
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static public List<LocationsSocialNetworks> GetLocationSocialNetwoks(int idLocation)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                List<LocationsSocialNetworks> list = new List<LocationsSocialNetworks>();
                return dataAccess.SelectLocationsSocialNetworks(idLocation);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion

        #region SitePassAccess

        static public SitePassAccess GetSitePassAccess(string mac, int idhotel, string pass)
        {
            SitePassAccess obj = new SitePassAccess();
            BillingDataAccess dataAcess = new BillingDataAccess();
            dataAcess.Select(obj, mac, idhotel, pass);

            return obj;
        }

        static public bool SaveSitePassAccess(SitePassAccess obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                dataAccess.Insert(obj);

                return true;
            }
            catch (Exception ex)
            {
                return false;

            }
        }

        #endregion

        #region RadiusCoa

        static public bool UpdateRadiusCoa(RadiusCoa obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                dataAccess.Update(obj);

                return true;
            }
            catch (Exception ex)
            {
                return false;

            }
        }

        static public List<RadiusCoa> SelectRadiusCoaUnProcessed()
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                List<RadiusCoa> list = new List<RadiusCoa>();
                return dataAccess.SelectRadiusCoas();
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static public List<RadiusCoa> SelectRadiusCoaUnFinished()
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                List<RadiusCoa> list = new List<RadiusCoa>();
                return dataAccess.SelectRadiusCoasUnfinished();
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        #endregion

        #region CoA_Types

        static public CoA_Types SelectCoA_Type(int idCoA_Types)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                CoA_Types obj = new CoA_Types();
                dataAccess.Select(obj, idCoA_Types);

                return obj;

            }
            catch (Exception ex)
            {
                throw;

            }
        }

        static public List<CoA_Types> SelectRadiusCoA_Types()
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                List<CoA_Types> list = new List<CoA_Types>();
                return dataAccess.SelectRadiusCoA_Types();
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        #endregion

        #region CoA_Vendors

        static public CoA_Vendors SelectCoA_Vendor(int idCoAVendors)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                CoA_Vendors obj = new CoA_Vendors();
                dataAccess.Select(obj, idCoAVendors);

                return obj;
            }
            catch (Exception ex)
            {
                throw;

            }
        }

        static public List<CoA_Vendors> SelectRadiusCoA_Vendors()
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                List<CoA_Vendors> list = new List<CoA_Vendors>();
                return dataAccess.SelectRadiusCoA_Vendors();
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        #endregion

        #region MerakiLogoutUrl

        static public MerakiLogoutUrl GetSiteMerakiLogoutUrl(string mac, int idhotel)
        {
            MerakiLogoutUrl obj = new MerakiLogoutUrl();
            BillingDataAccess dataAcess = new BillingDataAccess();
            dataAcess.Select(obj, mac, idhotel);

            return obj;
        }

        static public bool SaveMerakiLogoutUrl(MerakiLogoutUrl obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                dataAccess.Insert(obj);

                return true;
            }
            catch (Exception ex)
            {
                return false;

            }
        }

        #endregion

        #region Vendors

        static public Vendors Vendors(int idVendors)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                Vendors obj = new Vendors();
                dataAccess.Select(obj, idVendors);

                return obj;
            }
            catch (Exception ex)
            {
                throw;

            }
        }

        static public List<Vendors> SelectVendors()
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                List<Vendors> list = new List<Vendors>();
                return dataAccess.SelectVendors();
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        #endregion

        #region FDSGroupsMembers

        static public List<FDSGroupsMembers> SelectFDSGroupsMembers(int idgroup)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                List<FDSGroupsMembers> list = new List<FDSGroupsMembers>();
                return dataAccess.SelectFDSGroupMembers(idgroup);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        static public FDSGroupsMembers SelectFDSGroupsMemberByIdMember(int idMember)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                FDSGroupsMembers obj = new FDSGroupsMembers();
                dataAccess.SelectFDSGroupMemberByIdMember(obj, idMember);

                return obj;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion

        #region Currencies
        static public bool SaveCurrencie(Currencies2 obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.Id.Equals(0))
                    dataAccess.Insert(obj);
                else
                    dataAccess.Update(obj);

                return true;
            }
            catch (Exception ex)
            {
                return false;

            }
        }

        static public Currencies2 Currency(string code)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                Currencies2 obj = new Currencies2();
                dataAccess.Select(obj, code);

                return obj;
            }
            catch (Exception ex)
            {
                throw;

            }
        }

        static public List<Currencies2> SelectCurrencies()
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                List<Currencies2> list = new List<Currencies2>();
                return dataAccess.SelectCurrencies();
            }
            catch (Exception ex)
            {
                throw;
            }

        }


        #endregion

        #region SessionOnLineCoordenades

        static public List<UsersOnlineCoordinadesInfo> SelectUsersOnlineCoordinades()
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                List<UsersOnlineCoordinadesInfo> list = new List<UsersOnlineCoordinadesInfo>();
                return dataAccess.SessionOnLineCoordenades();
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        #endregion

        #region SiteGMT

        static public SitesGMT SiteGMTSitebyGroup(int idGroup)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                SitesGMT obj = new SitesGMT();
                dataAccess.SelectbyGroup(obj, idGroup);

                return obj;
            }
            catch (Exception ex)
            {
                throw;

            }
        }

        static public SitesGMT SiteGMTSitebySite(int idSite)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                SitesGMT obj = new SitesGMT();
                dataAccess.Select(obj, idSite);

                return obj;
            }
            catch (Exception ex)
            {
                throw;

            }
        }

        static public SitesGMT SiteGMTSitebyLocation(int idLocation)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                SitesGMT obj = new SitesGMT();
                dataAccess.SelectbyLocation(obj, idLocation);

                return obj;
            }
            catch (Exception ex)
            {
                throw;

            }
        }

        static public bool SaveSiteGMT(SitesGMT obj)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.Id.Equals(0))
                    dataAccess.Insert(obj);
                else
                    dataAccess.Update(obj);

                return true;
            }
            catch (Exception ex)
            {
                return false;

            }
        }

        


        #endregion

        #region ApiToken 

        static public ApiTokens SelectAccessWithToken(string token)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                ApiTokens obj = new ApiTokens();
                dataAccess.Select(obj, token);

                return obj;
            }
            catch (Exception ex)
            {
                throw;

            }
        }

        #endregion

        #region ErrorMessages 

        static public ErrorMessages SelectErrorMessage(string codError, string codLanguage)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                ErrorMessages obj = new ErrorMessages();
                dataAccess.Select(obj, codError, codLanguage);

                return obj;
            }
            catch (Exception ex)
            {
                throw;

            }
        }

        #endregion


        #region ApiMerakiSplashLogin

        static public bool SaveApiMerakiSplashLogin(ApiMerakiSplashLogins obj)
        {
            try
            {

                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.Id.Equals(0))
                    dataAccess.Insert(obj);
                else
                    dataAccess.Update(obj);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        static public ApiMerakiSplashLogins SelectApiMerakiLastSplashLogin()
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                ApiMerakiSplashLogins obj = new ApiMerakiSplashLogins();
                dataAccess.SelectLast(obj);

                return obj;
            }
            catch (Exception ex)
            {
                throw;

            }
        }
        static public ApiMerakiSplashLogins SelectApiMerakiSplashLogin(string clientID, string clientMac, double loginAt)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                ApiMerakiSplashLogins obj = new ApiMerakiSplashLogins();
                dataAccess.Select(obj, clientID, clientMac, loginAt);

                return obj;
            }
            catch (Exception ex)
            {
                throw;

            }
        }

        static public ApiMerakiSplashLogins SelectApiMerakiSplashLogin(string clientID, string clientMac, bool status)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                ApiMerakiSplashLogins obj = new ApiMerakiSplashLogins();
                dataAccess.Select(obj, clientID, clientMac, status);

                return obj;
            }
            catch (Exception ex)
            {
                throw;

            }
        }

        static public List<ApiMerakiSplashLogins> SelectApiMerakiSplashLogins()
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                List<ApiMerakiSplashLogins> list = new List<ApiMerakiSplashLogins>();
                return dataAccess.SelectApiMerakiSplashLogins();
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static public List<ApiMerakiSplashLogins> SelectApiMerakiSplashLogins(bool status)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                List<ApiMerakiSplashLogins> list = new List<ApiMerakiSplashLogins>();
                return dataAccess.SelectApiMerakiSplashLoginsbyStatus(status);
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        #endregion

        #region MerakiLocationsGPS

        static public MerakiLocationsGPS SelectMerakiLocationsGPS(string NasMac)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                MerakiLocationsGPS obj = new MerakiLocationsGPS();
                dataAccess.Select(obj, NasMac);

                return obj;
            }
            catch (Exception ex)
            {
                throw;

            }
        }

        static public MerakiLocationsGPS SelectMerakiLocationsGPSContains(string Mac)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                MerakiLocationsGPS obj = new MerakiLocationsGPS();
                dataAccess.SelectContains(obj, Mac);

                return obj;
            }
            catch (Exception ex)
            {
                throw;

            }
        }

        static public List<MerakiLocationsGPS> SelectMerakiLocationsGPS(int idSite)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                return dataAccess.SelectMerakiLocationsGPS(idSite);
            }
            catch (Exception ex)
            {
                throw;

            }
        }

        #endregion

        #region ActiveSessions

        static public bool SaveActiveSession(ActiveSessions obj)
        {
            try
            {

                BillingDataAccess dataAccess = new BillingDataAccess();
                dataAccess.Insert(obj);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        static public void DeleteActiveSession(int id)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                dataAccess.DeleteActiveSession(id);
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        static public ActiveSessions SelectActiveSessions(string clientMac, string actSessionId)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                ActiveSessions obj = new ActiveSessions();
                dataAccess.Select(obj, clientMac, actSessionId);

                return obj;
            }
            catch (Exception ex)
            {
                throw;

            }
        }

        static public List<ActiveSessions> SelectActiveSessionsByAccSessionId(string pattern)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                List<ActiveSessions> list = new List<ActiveSessions>();
                return dataAccess.ActiveSessionByAccSessionsIdString(pattern);
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        #endregion

        #region ApiMerakiOrganizations

        static public bool SaveApiMerakiOrganization(ApiMerakiOrganizations obj)
        {
            try
            {

                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.IdOrganization.Equals(0))
                    dataAccess.Insert(obj);
                else
                    dataAccess.Update(obj);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        static public void DeleteApiMerakiOrganization(int id)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                dataAccess.DeleteApiMerakiOrganization(id);

            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        static public List<ApiMerakiOrganizations> SelectApiMerakiOrganizations()
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                List<ApiMerakiOrganizations> list = new List<ApiMerakiOrganizations>();
                return dataAccess.ApiMerakiOrganizations();
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static public List<ApiMerakiOrganizations> SelectApiMerakiOrganizations(DateTime lastUpdate)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                List<ApiMerakiOrganizations> list = new List<ApiMerakiOrganizations>();
                return dataAccess.ApiMerakiOrganizations(lastUpdate);
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static public ApiMerakiOrganizations SelectApiMerakiOrganization(int id)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                ApiMerakiOrganizations obj = new ApiMerakiOrganizations();
                dataAccess.Select(obj, id);
                return obj;
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        #endregion

        #region ApiMerakiNetworks

        static public bool SaveApiMerakiNetwork(ApiMerakiNetworks obj)
        {
            try
            {

                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.idNetwork.Equals(0))
                    dataAccess.Insert(obj);
                else
                    dataAccess.Update(obj);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        static public void DeleteApiMerakiNetwork(int id)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                dataAccess.DeleteApiMerakiNetwork(id);

            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        static public ApiMerakiNetworks SelectApiMerakiNetwork(string id)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                ApiMerakiNetworks obj = new ApiMerakiNetworks();
                dataAccess.Select(obj, id);
                return obj;
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static public List<ApiMerakiNetworks> SelectApiMerakiNetworks()
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                List<ApiMerakiNetworks> list = new List<ApiMerakiNetworks>();
                return dataAccess.ApiMerakiNetworks();
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static public List<ApiMerakiNetworks> SelectApiMerakiNetworks(int idOrganization)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                List<ApiMerakiNetworks> list = new List<ApiMerakiNetworks>();
                return dataAccess.ApiMerakiNetworks(idOrganization);
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static public List<ApiMerakiNetworks> SelectApiMerakiNetworks(DateTime lastUpdate)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                List<ApiMerakiNetworks> list = new List<ApiMerakiNetworks>();
                return dataAccess.ApiMerakiNetworks(lastUpdate);
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        #endregion

        #region ApiMerakiDevices

        static public bool SaveApiMerakiDevice(ApiMerakiDevices obj)
        {
            try
            {

                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.Id.Equals(0))
                    dataAccess.Insert(obj);
                else
                    dataAccess.Update(obj);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        static public ApiMerakiDevices SelectApiMerakiDevice(string mac)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                ApiMerakiDevices obj = new ApiMerakiDevices();
                dataAccess.Select(obj, mac);
                return obj;
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static public List<ApiMerakiDevices> SelectApiMerakiDevices()
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                List<ApiMerakiDevices> list = new List<ApiMerakiDevices>();
                return dataAccess.ApiMerakiDevices();
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static public List<ApiMerakiDevices> SelectApiMerakiDevices(string networkID)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                List<ApiMerakiDevices> list = new List<ApiMerakiDevices>();
                return dataAccess.ApiMerakiDevices(networkID);
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static public List<ApiMerakiDevices> SelectApiMerakiDevices(DateTime lastUpdate)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                List<ApiMerakiDevices> list = new List<ApiMerakiDevices>();
                return dataAccess.ApiMerakiDevices(lastUpdate);
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static public void DeleteApiMerakiDevice(int id)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                dataAccess.DeleteApiMerakiDevice(id);

            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        #endregion

        #region ApiMerakiAccounting

        static public bool SaveApiMerakiAccounting(ApiMerakiAccounting obj)
        {
            try
            {

                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.Id.Equals(0))
                    dataAccess.Insert(obj);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        static public void DeleteApiMerakiAccounting(string actSessionId)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                dataAccess.DeleteApiMerakiAccounting(actSessionId);

            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        static public ApiMerakiAccounting SelectApiMerakiAccountingLastbyActSessionId(string actSessionId)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                ApiMerakiAccounting obj = new ApiMerakiAccounting();
                dataAccess.SelectApiMerakiAccountingLastbyActSessionId(obj, actSessionId);
                return obj;
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static public List<ApiMerakiAccounting> SelectApiMerakiAccounting(string actSessionId, DateTime since)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                List<ApiMerakiAccounting> list = new List<ApiMerakiAccounting>();
                return dataAccess.SelectApiMerakiAccountings(actSessionId, since);
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        #endregion

        #region ClosedSessions

        static public bool SaveClosedSessions(ClosedSessions2 obj)
        {
            try
            {

                BillingDataAccess dataAccess = new BillingDataAccess();
                dataAccess.Insert(obj);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        #endregion

        #region ApiMerakiGroupPolicies

        static public bool SaveApiMerakiGroupPolicies(ApiMerakiGroupPolicies obj)
        {
            try
            {

                BillingDataAccess dataAccess = new BillingDataAccess();
                if (obj.Id.Equals(0))
                    dataAccess.Insert(obj);
                else
                    dataAccess.Update(obj);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        static public void DeleteApiMerakiGroupPolicies(int id)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                dataAccess.DeleteApiMerakiGroupPolicies(id);

            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        static public ApiMerakiGroupPolicies SelectApiMerakiGroupPolicy(string networkId, int groupPolicyId)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                ApiMerakiGroupPolicies obj = new ApiMerakiGroupPolicies();
                dataAccess.SelectApiMerakiGroupPolicy(obj, networkId, groupPolicyId);
                return obj;
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static public ApiMerakiGroupPolicies SelectApiMerakiGroupPolicy(string networkId, string name)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                ApiMerakiGroupPolicies obj = new ApiMerakiGroupPolicies();
                dataAccess.SelectApiMerakiGroupPolicy(obj, networkId, name);
                return obj;
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static public List<ApiMerakiGroupPolicies> SelectApiMerakiGroupPolicies(string networkId)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                List<ApiMerakiGroupPolicies> list = new List<ApiMerakiGroupPolicies>();
                return dataAccess.SelectApiMerakiGroupPolicies(networkId);
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static public List<ApiMerakiGroupPolicies> SelectApiMerakiGroupPolicies(DateTime lastUpdate)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                List<ApiMerakiGroupPolicies> list = new List<ApiMerakiGroupPolicies>();
                return dataAccess.SelectApiMerakiGroupPolicies(lastUpdate);
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        #endregion

        #region ClonedClosedSessions

        

        static public bool CloneClosedSession(int idHotelToClone, int newIdHotel)
        {
            try
            {

                BillingDataAccess dataAccess = new BillingDataAccess();

                return dataAccess.CloneClosedSession(idHotelToClone, newIdHotel);
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        static public bool CloneSessionsLog(int idHotelToClone, int newIdHotel)
        {
            try
            {

                BillingDataAccess dataAccess = new BillingDataAccess();

                return dataAccess.CloneSessionLog(idHotelToClone, newIdHotel);
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }


        static public bool CloneLoyaltiesLocationIsNull(int idHotelToClone, int newIdHotel)
        {
            try
            {

                BillingDataAccess dataAccess = new BillingDataAccess();

                return dataAccess.CloneLoyaltiesLocationIsNull(idHotelToClone, newIdHotel);
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        static public bool CloneLoyaltiesByLocation(int idHotelToClone, int newIdHotel, int idLocationToClone, int newIdLocation)
        {
            try
            {

                BillingDataAccess dataAccess = new BillingDataAccess();

                return dataAccess.CloneLoyaltiesLocation(idHotelToClone, newIdHotel, idLocationToClone, newIdLocation);
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        #endregion

        #region CambiumLocationsGPS

        static public CambiumLocationsGPS SelectCambiumLocationsGPS(string NasMac)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                CambiumLocationsGPS obj = new CambiumLocationsGPS();
                dataAccess.Select(obj, NasMac);

                return obj;
            }
            catch (Exception ex)
            {
                throw;

            }
        }

        static public CambiumLocationsGPS SelectCambiumLocationsGPSContains(string Mac)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                CambiumLocationsGPS obj = new CambiumLocationsGPS();
                dataAccess.SelectContains(obj, Mac);

                return obj;
            }
            catch (Exception ex)
            {
                throw;

            }
        }

        static public List<CambiumLocationsGPS> SelectCambiumLocationsGPS(int idSite)
        {
            try
            {
                BillingDataAccess dataAccess = new BillingDataAccess();
                return dataAccess.SelectCambiumLocationsGPS(idSite);
            }
            catch (Exception ex)
            {
                throw;

            }
        }

        #endregion

    }
}

