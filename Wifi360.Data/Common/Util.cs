﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Reflection;

namespace Wifi360.Data.Common
{
    public class Util
    {
        public static string ConnectionString()
        {
            string _connectionString = string.Empty;
            if (ConfigurationManager.AppSettings["ConnectionStringName"] != null)
                _connectionString = ConfigurationManager.AppSettings["ConnectionStringName"].ToString();

            if (!string.IsNullOrEmpty(_connectionString))
                _connectionString = ConfigurationManager.ConnectionStrings[_connectionString].ConnectionString.ToString();
            
            return _connectionString;
        }

        public static void Map(object source, object destiny)
        {
            foreach (PropertyInfo infoSource in source.GetType().GetProperties())
            {
                if (infoSource.CanRead)
                {
                    PropertyInfo infoDestiny = destiny.GetType().GetProperty(infoSource.Name);

                    if (infoDestiny != null)
                    {
                        if ((infoDestiny.CanWrite) && (infoSource.PropertyType == infoDestiny.PropertyType))
                        {
                            object value = infoSource.GetValue(source, null);
                            infoDestiny.SetValue(destiny, value, null);
                        }
                    }
                }
            }
        }
    }
}
