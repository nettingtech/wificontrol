﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wifi360.Data.Class
{
    public class UserAll
    {
        private int _iduser;
        private int _idHotel;
        private int _idRoom;
        private string _roomName;
        private int _idBillingType;
        private string _billingType;
        private string _name;
        private string _password;
        private Boolean _enabled;
        private DateTime _validSince;
        private DateTime _validTill;
        private int _timeCredit;
        private int _maxDevices;
        private Boolean _chko;
        private long _volumeUp;
        private long _volumeDown;
        private long _bWUp;
        private long _bWDown;
        private int _priority;
        private string _priorityName;
        private string _buyingCallerID;
        private int _buyingFrom;
        private int _filterId;
        private int _idlocation;
        private string _comment;

        public int IdUser
        {
            get { return _iduser; }
            set { _iduser = value; }
        }
        
        public int IdHotel
        {
            get { return _idHotel; }
            set { _idHotel = value; }
        }

        public int IdRoom
        {
            get { return _idRoom; }
            set { _idRoom = value; }
        }

        public string RoomName
        {
            get { return _roomName; }
            set { _roomName = value; }
        }

        public int IdBillingType
        {
            get { return _idBillingType; }
            set { _idBillingType = value; }
        }

        public string BillingType
        {
            get { return _billingType; }
            set { _billingType = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        public Boolean Enabled
        {
            get { return _enabled; }
            set { _enabled = value; }
        }

        public DateTime ValidSince
        {
            get { return _validSince; }
            set { _validSince = value; }
        }

        public DateTime ValidTill
        {
            get { return _validTill; }
            set { _validTill = value; }
        }

        public int TimeCredit
        {
            get { return _timeCredit; }
            set { _timeCredit = value; }
        }

        public int MaxDevices
        {
            get { return _maxDevices; }
            set { _maxDevices = value; }
        }

        public int Priority
        {
            get { return _priority; }
            set { _priority = value; }
        }

        public string PriorityName
        {
            get { return _priorityName; }
            set { _priorityName = value; }
        }

        public Boolean CHKO
        {
            get { return _chko; }
            set { _chko = value; }
        }

        public long VolumeUp
        {
            get { return _volumeUp; }
            set { _volumeUp = value; }
        }

        public long VolumeDown
        {
            get { return _volumeDown; }
            set { _volumeDown = value; }
        }

        public long BWUp
        {
            get { return _bWUp; }
            set { _bWUp = value; }
        }

        public long BWDown
        {
            get { return _bWDown; }
            set { _bWDown = value; }
        }

        public int BuyingFrom
        {
            get { return _buyingFrom; }
            set { _buyingFrom = value; }
        }

        public string BuyingCallerID
        {
            get { return _buyingCallerID; }
            set { _buyingCallerID = value; }
        }

        public int IdLocation
        {
            get { return _idlocation; }
            set { _idlocation = value; }
        }

        public int FilterId
        {
            get { return _filterId; }
            set { _filterId = value; }
        }

        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }
    }
}
