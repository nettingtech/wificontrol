﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wifi360.Data.Class
{
    public class FDSMemberGroupInfo
    {

        public int Id { get; set; }
        public int IdGroup { get; set; }
        public int IdMember { get; set; }
        public int IdMemberType { get; set; }
        public bool IsParent { get; set; }
        public string Name { get; set; }
    }

    public class FDSGroupLevel
    {
        public int idGroup {get; set;}
        public int level { get; set; }
    }

    public class FDSMemberInGroup    {
        public int idGroup { get; set; }
        public int idMember { get; set; }
    }
}
