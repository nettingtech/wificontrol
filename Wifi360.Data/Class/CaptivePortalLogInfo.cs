﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wifi360.Data.Class
{
    public class CaptivePortalLogInfo
    {
       private string _location;
        private string _userName;
        private string _password;
        private string _nomadixResponse;
        private string _radiusResponse;
        private DateTime _date;
        private string _callerID;
        private string _page;

        public DateTime Date
        {
            get { return _date; }
            set { _date = value; }
        }


        public string Location
        {
            get { return _location; }
            set { _location = value; }
        }

        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        public string NomadixResponse
        {
            get { return _nomadixResponse; }
            set { _nomadixResponse = value; }
        }

        public string RadiusResponse
        {
            get { return _radiusResponse; }
            set { _radiusResponse = value; }
        }

        public string CallerID
        {
            get { return _callerID; }
            set { _callerID = value; }
        }

        public string Page
        {
            get { return _page; }
            set { _page = value; }
        }

    }
}
