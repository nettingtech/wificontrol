﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wifi360.Data.Class
{
    public class UsersOnlineCoordinadesInfo
    {
        private string _latitude;
        private string _longitude;
        private string _CallerID;
        private string _AcctSessionId;
        private int _idHotel;

        public string Latitude
        {
            get { return _latitude; }
            set { _latitude = value; }
        }
        public string Longitude
        {
            get { return _longitude; }
            set { _longitude = value; }
        }
        public string CallerID
        {
            get { return _CallerID; }
            set { _CallerID = value; }
        }
        public string AcctSessionId
        {
            get { return _AcctSessionId; }
            set { _AcctSessionId = value; }
        }
        public int IdHotel
        {
            get { return _idHotel; }
            set { _idHotel = value; }
        }
    }
}
