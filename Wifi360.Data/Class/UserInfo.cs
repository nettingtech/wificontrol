﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wifi360.Data.Class
{
    public class UserInfo
    {
        private int _iduser;
        private string _room;
        private string _userName;
        private string _password;
        private string _billingType;
        private DateTime _validSince;
        private DateTime _validTill;
        private int _timeCredit;
        private int _idlocation;
        private int _idPriority;
        private int _idFilterProfile;
        private string _comment;
        private int _idFDSUSer;
        private bool _billable;
        private bool _enabled;
        private int  _validAfterFirstUse;
        private DateTime _billingCharge;

        public int IdUser
        {
            get { return _iduser; }
            set { _iduser = value; }
        }

        public int IdLocation
        {
            get { return _idlocation; }
            set { _idlocation = value; }
        }

        public string Room
        {
            get { return _room; }
            set { _room = value; }
        }

        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        public string BillingType
        {
            get { return _billingType; }
            set { _billingType = value; }
        }

        public DateTime ValidSince
        {
            get { return _validSince; }
            set { _validSince = value; }
        }

        public DateTime ValidTill
        {
            get { return _validTill; }
            set { _validTill = value; }
        }

        public int TimeCredit
        {
            get { return _timeCredit; }
            set { _timeCredit = value; }
        }

        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        public int IdPriority
        {
            get { return _idPriority; }
            set { _idPriority = value; }
        }

        public int IdFilterProfile
        {
            get { return _idFilterProfile; }
            set { _idFilterProfile = value; }
        }

        public int IdFDSUser
        {
            get { return _idFDSUSer; }
            set { _idFDSUSer = value; }
        }

        public bool Enabled
        {
            get { return _enabled; }
            set { _enabled = value; }
        }

        public bool Billable
        {
            get { return _billable; }
            set { _billable = value; }
        }

        public int ValidAfterFirstUse
        {
            get { return _validAfterFirstUse; }
            set { _validAfterFirstUse = value; }
        }

        public DateTime BillingCharge
        {
            get { return _billingCharge; }
            set { _billingCharge = value; }
        }

    }
}
