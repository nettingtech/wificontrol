﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace Wifi360.Data.Class
{
    /// <summary>
    /// Enumerado con las diferentes categorías de Log
    /// </summary>
    public enum ELogType { Error, Warning, Inform };

    /// <summary>
    /// Clase que implementa un log diario en archivo de texto plano. También incorpora métodos alternativos para crear
    /// un log a través del Visor de Eventos de Windows. Si el log no se puede escribir en disco por algún error,
    /// entonces el mismo log se escribirá en el Visor de Eventos.
    /// </summary>
    public class LogDaily
    {
        #region Variables Privadas

        private string appName;
        private string logDir;

        #endregion Variables Privadas

        #region Propiedades

        public string AppName
        {
            get { return appName; }
            set { appName = value; }
        }

        public string LogDir
        {
            get { return logDir; }
            set { logDir = value; }
        }

        #endregion Propiedades

        #region Constructores

        /// <summary>
        /// Constructor por defecto de la clase LogDaily
        /// </summary>
        /// <param name="appName">Nombre de la aplicación con la que "firmará" el log (string)</param>
        /// <param name="logDir">Ruta del directorio en donde se generarán los archivos log (string)</param>
        public LogDaily(string appName, string logDir)
        {
            this.appName = appName;
            this.logDir = logDir;
        }

        #endregion Constructores

        #region Métodos privados

        /// <summary>
        /// Comprueba que el directorio 'Log' existe y si no lo crea
        /// </summary>
        /// <param name="logDir">Ruta del directorio (string)</param>
        private void CheckDirLog(string logDir)
        {
            try
            {
                if (!Directory.Exists(logDir))
                {
                    Directory.CreateDirectory(logDir);
                }
            }
            catch (Exception ex)
            {
                VisorWriteEntry(ELogType.Error, "Error accediendo al directorio del Log: '" + logDir + "'.", ex);
            }
        }

        /// <summary>
        /// Genera un string el nombre de un fichero Log con formato "appname_yyyy-mm-dd.log"
        /// </summary>
        /// <param name="fullPath">Indica si se incluye la ruta completa o no en el nombre del fichero (bool)</param>
        /// <returns>El nombre del fichero (string)</returns>
        private string GenerateDailyFilename(bool fullPath)
        {
            string fileName = appName.ToLower() + "-" + DateTime.Now.ToString("yyyy-MM-dd") + ".log";
            return (fullPath) ? Path.GetFullPath(Misc.NormalizePath(logDir) + @"\" + fileName) : fileName;
        }

        /// <summary>
        /// Convierte un tipo enumerado de error propio (ELogType) a un tipo enumerado de EventLogEntryType
        /// </summary>
        /// <param name="entryType">Tipo de error (ELogType)</param>
        /// <returns>Tipo de error(EventLogEntryType)</returns>
        private EventLogEntryType ToEventLogEntryType(ELogType entryType)
        {
            switch (entryType)
            {
                case ELogType.Error:
                    return EventLogEntryType.Error;

                case ELogType.Warning:
                    return EventLogEntryType.Warning;

                case ELogType.Inform:
                    return EventLogEntryType.Information;

                default:
                    return EventLogEntryType.Error;
            }
        }

        /// <summary>
        /// Crea un Visor de Eventos con el nombre del programa en la categoría "Application"
        /// </summary>
        private void CreateEventSource()
        {
            if (!EventLog.SourceExists(appName))
            {
                EventLog.CreateEventSource(appName, "Application");
            }
        }

        /// <summary>
        /// Convierte un string multilinea a una monolinea
        /// Se hace un 'Trim()' de cada línea y los retornos de carro se sustituyen por 'sep'
        /// </summary>
        /// <param name="input">El texto de entrada (string)</param>
        /// <param name="sep">Separador por el que se sustituirán los retornos de carro (string)</param>
        /// <returns>El texto en una sóla línea (string)</returns>
        private string ToMonoLine(string input, string sep)
        {
            return string.Join(sep, input.Split(new[] { Environment.NewLine }, StringSplitOptions.None).Select(l => l.Trim()));
        }

        /// <summary>
        /// Genera un string (monolinea) con: fecha_hora, tipo y mensaje
        /// </summary>
        /// <param name="logType">Tipo de Log (ELogType)</param>
        /// <param name="message">Mensaje (string)</param>
        /// <returns>El texto del error parseado (string)</returns>
        private string ParseLogInfo(ELogType logType, string message)
        {
            return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff|") + appName.ToUpper() + "|" + logType.ToString().ToUpper() + ToMonoLine("|" + message, "|");
        }

        /// <summary>
        /// Genera un string (monolinea o no) con: fecha_hora, tipo y excepción
        /// </summary>
        /// <param name="logType">Tipo de Log (ELogType)</param>
        /// <param name="eex">Excepción (Exception)</param>
        /// <param name="oneLine">'Flag' para que la salida sea monolínea o no (bool)</param>
        /// <returns>El texto del error parseado (string)</returns>
        private string ParseLogInfo(ELogType logType, Exception ex, bool oneLine)
        {
            if (oneLine)
                return ParseLogInfo(logType, ex.Message) + "   { " + ToMonoLine(ex.StackTrace, "; ") + " }";
            else
                return ParseLogInfo(logType, ex.Message) + Environment.NewLine + ex.StackTrace;
        }

        /// <summary>
        /// Genera un string (monolinea o no) con: fecha_hora, tipo, mensaje y excepción
        /// </summary>
        /// <param name="logType">Tipo de Log (ELogType)</param>
        /// <param name="message">Mensaje (string)</param>
        /// <param name="eex">Excepción (Exception)</param>
        /// <param name="oneLine">'Flag' para que la salida sea monolínea o no (bool)</param>
        /// <returns>El texto del error parseado (string)</returns>
        private string ParseLogInfo(ELogType logType, string message, Exception ex, bool oneLine)
        {
            if (oneLine)
                return ParseLogInfo(logType, message) + "   " + ToMonoLine(ex.Message, " ") + "   { " + ToMonoLine(ex.StackTrace, "; ") + " }";
            else
                return ParseLogInfo(logType, message) + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace;
        }

        #endregion Métodos privados

        #region Métodos públicos Log en fichero diario

        /// <summary>
        /// Crea una entrada del log en el fichero diario 'yyyy-mm-dd.log' con: fecha_hora, tipo y mensaje
        /// </summary>
        /// <param name="logType">Tipo de Log (ELogType)</param>
        /// <param name="message">Mensaje (string)</param>
        public void FileWriteEntry(ELogType logType, string message)
        {
            try
            {
                string fullFileName = GenerateDailyFilename(true);

                CheckDirLog(Path.GetDirectoryName(fullFileName));

                using (StreamWriter sw = new StreamWriter(fullFileName, true))
                {
                    sw.WriteLine(ParseLogInfo(logType, message));
                }
            }
            catch (Exception iex)
            {
                VisorWriteEntry(
                    ELogType.Error,
                    "Error creando un log en el fichero: '" + GenerateDailyFilename(false) + "'.",
                    iex,
                    ParseLogInfo(logType, message)
                );
            }
        }

        /// <summary>
        /// Crea una entrada del log en el fichero diario 'yyyy-mm-dd.log' con: fecha_hora, tipo y excepción
        /// </summary>
        /// <param name="logType">Tipo de Log (ELogType)</param>
        /// <param name="eex">Excepción (Exception)</param>
        public void FileWriteEntry(ELogType logType, Exception eex)
        {
            try
            {
                string fullFileName = GenerateDailyFilename(true);

                CheckDirLog(Path.GetDirectoryName(fullFileName));

                using (StreamWriter sw = new StreamWriter(fullFileName, true))
                {
                    sw.WriteLine(ParseLogInfo(logType, eex, true));
                }
            }
            catch (Exception iex)
            {
                VisorWriteEntry(
                    ELogType.Error,
                    "Error creando un log en el fichero: '" + GenerateDailyFilename(false) + "'.",
                    iex,
                    ParseLogInfo(logType, eex, false)
                );
            }
        }

        /// <summary>
        /// Crea una entrada del log en el fichero diario 'yyyy-mm-dd.log' con: fecha_hora, tipo, mensaje y excepción
        /// </summary>
        /// <param name="logType">Tipo de Log (ELogType)</param>
        /// <param name="message">Mensaje (string)</param>
        /// <param name="eex">Excepción (Exception)</param>
        public void FileWriteEntry(ELogType logType, string message, Exception eex)
        {
            try
            {
                string fullFileName = GenerateDailyFilename(true);

                CheckDirLog(Path.GetDirectoryName(fullFileName));

                using (StreamWriter sw = new StreamWriter(fullFileName, true))
                {
                    sw.WriteLine(ParseLogInfo(logType, message, eex, true));
                }
            }
            catch (Exception iex)
            {
                VisorWriteEntry(
                    ELogType.Error,
                    "Error creando un log en el fichero: '" + GenerateDailyFilename(false) + "'.",
                    iex,
                    ParseLogInfo(logType, message, eex, false)
                );
            }
        }

        #endregion Métodos públicos Log en fichero diario

        #region Métodos públicos Log en visor de eventos

        /// <summary>
        /// Crea una entrada del log en el visor de eventos con: fecha_hora, tipo y mensaje
        /// </summary>
        /// <param name="logType">Tipo de Log (ELogType)</param>
        /// <param name="message">Mensaje (string)</param>
        public void VisorWriteEntry(ELogType logType, string message)
        {
            try
            {
                CreateEventSource();
                EventLog.WriteEntry(appName, ParseLogInfo(logType, message), ToEventLogEntryType(logType));
            }
            catch (Exception iex)
            {
                Console.WriteLine(ParseLogInfo(ELogType.Error, "Error creando log en visor de eventos.", iex, false));
            }
        }

        /// <summary>
        /// Crea una entrada del log en el visor de eventos con: fecha_hora, tipo y excepción
        /// </summary>
        /// <param name="logType">Tipo de Log (ELogType)</param>
        /// <param name="eex">Excepción (Exception)</param>
        public void VisorWriteEntry(ELogType logType, Exception eex)
        {
            try
            {
                CreateEventSource();
                EventLog.WriteEntry(appName, ParseLogInfo(logType, eex, false), ToEventLogEntryType(logType));
            }
            catch (Exception iex)
            {
                Console.WriteLine(ParseLogInfo(ELogType.Error, "Error creando log en visor de eventos.", iex, false));
            }
        }

        /// <summary>
        /// Crea una entrada del log en el visor de eventos con: fecha_hora, tipo, mensaje y excepción
        /// </summary>
        /// <param name="logType">Tipo de Log (ELogType)</param>
        /// <param name="message">Mensaje (string)</param>
        /// <param name="eex">Excepción (Exception)</param>
        public void VisorWriteEntry(ELogType logType, string message, Exception eex)
        {
            try
            {
                CreateEventSource();
                EventLog.WriteEntry(appName, ParseLogInfo(logType, message, eex, false), ToEventLogEntryType(logType));
            }
            catch (Exception iex)
            {
                Console.WriteLine(ParseLogInfo(ELogType.Error, "Error creando log en visor de eventos.", iex, false));
            }
        }

        /// <summary>
        /// Crea una entrada del log en el visor de eventos con: fecha_hora, tipo, mensaje, excepción y log original
        /// </summary>
        /// <param name="logType">Tipo de Log (ELogType)</param>
        /// <param name="message">Mensaje (string)</param>
        /// <param name="eex">Excepción (Exception)</param>
        /// <param name="origLog">Log original (string)</param>
        public void VisorWriteEntry(ELogType logType, string message, Exception eex, string origLog)
        {
            try
            {
                CreateEventSource();
                EventLog.WriteEntry(
                    appName,
                    ParseLogInfo(logType, message, eex, false) + Environment.NewLine + Environment.NewLine
                        + "=||=    =||=    =||=    =||=    =||=     LOG ORIGINAL:     =||=    =||=    =||=    =||=    =||="
                        + Environment.NewLine + origLog,
                    ToEventLogEntryType(logType)
                );
            }
            catch (Exception iex)
            {
                Console.WriteLine(ParseLogInfo(ELogType.Error, "Error creando log en visor de eventos.", iex, false));
            }
        }

        #endregion Métodos públicos Log en visor de eventos
    }
}
