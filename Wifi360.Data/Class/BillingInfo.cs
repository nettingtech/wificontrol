﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wifi360.Data.Class
{
    public class BillingInfo
    {
        private int _idBilling;
        private string _room;
        private string _billingType;
        private DateTime _billingCharge;
        private double _amount;
        private int _flag;
        private int _idLocation;
        private int _idUser;
        private string _name;
        private int _idFDSUser;

        public int IdBilling
        {
            get { return _idBilling; }
            set { _idBilling = value; }
        }

        public int IdLocation
        {
            get { return _idLocation; }
            set { _idLocation = value; }
        }


        public string Room
        {
            get { return _room; }
            set { _room = value; }
        }

        public string BillingType
        {
            get { return _billingType; }
            set { _billingType = value; }
        }

        public DateTime BillingCharge
        {
            get { return _billingCharge; }
            set { _billingCharge = value; }
        }

        public double Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }


        public int Flag
        {
            get { return _flag; }
            set { _flag = value; }
        }

        public int IdUser
        {
            get { return _idUser; }
            set { _idUser = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public int IdFDSUser
        {
            get { return _idFDSUser; }
            set { _idFDSUser = value; }
        }
    }
}
