﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wifi360.Data.Class
{
    public class UsersOnlineInfo
    {
        private DateTime _sesionStarted;
        private string _room;
        private string _accessType;
        private string _userName;
        private string _CallerID;
        private long _bytesin;
        private long _bytesout;
        private string _acctSessionID;
        private string _comment;
        private int _idUser;
        private int _idPriority;
        private string _priority;
        private int _idFilter;
        private string _filter;
        private int _idFDSUser;

        public DateTime SessionStarted
        {
            get { return _sesionStarted; }
            set { _sesionStarted = value; }
        }
        public string Room
        {
            get { return _room; }
            set { _room = value; }
        }
        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }
        public string AccessType
        {
            get { return _accessType; }
            set { _accessType = value; }
        }
        public long BytesIn
        {
            get { return _bytesin; }
            set { _bytesin = value; }
        }
        public long BytesOut
        {
            get { return _bytesout; }
            set { _bytesout = value; }
        }
        public string AcctSessionID
        {
            get { return _acctSessionID; }
            set { _acctSessionID = value; }
        }
        public string CallerID
        {
            get { return _CallerID; }
            set { _CallerID = value; }
        }
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }
        public int IdUser
        {
            get { return _idUser; }
            set { _idUser = value; }
        }
        public int IdPriority
        {
            get { return _idPriority; }
            set { _idPriority = value; }
        }
        public int IdFilter
        {
            get { return _idFilter; }
            set { _idFilter = value; }
        }
        public string Priority
        {
            get { return _priority; }
            set { _priority = value; }
        }
        public string Filter
        {
            get { return _filter; }
            set { _filter = value; }
        }
        public int IdFDSUser
        {
            get { return _idFDSUser; }
            set { _idFDSUser = value; }
        }

    }
}
