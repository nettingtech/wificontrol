﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Wifi360.Data.Class
{
    /// <summary>
    /// Clase "Miscellaneous" en donde se se encuentran diferentes métodos estáticos
    /// </summary>
    public static class Misc
    {
        private static DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        /// <summary>
        /// Convierte un "string" a un "int" devolviendo 0 en caso de error
        /// </summary>
        /// <param name="inputString">Cadena de entrada a convertir (string)</param>
        /// <returns>Valor convertido o 0 en caso de error (int)</returns>
        public static int StringToInt(string inputString)
        {
            try
            {
                return int.Parse(inputString);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary>
        /// Convierte un "string" a un "int" devolviendo un valor por defecto (defaultValue) en caso de error
        /// </summary>
        /// <param name="inputString">Cadena de entrada a convertir (string)</param>
        /// <param name="defaultValue">Valor por defecto devuelto en caso de error (int)</param>
        /// <returns>Valor convertido o 'defaultValue' en caso de error (int)</returns>
        public static int StringToInt(string inputString, int defaultValue)
        {
            try
            {
                return int.Parse(inputString);
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// Convierte un "string" a un "float" devolviendo 0 en caso de error
        /// </summary>
        /// <param name="inputString">Cadena de entrada a convertir (string)</param>
        /// <returns>Valor convertido o 0 en caso de error (float)</returns>
        public static float StringToFloat(string inputString)
        {
            try
            {
                return float.Parse(inputString);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary>
        /// Convierte un "string" a un "float" devolviendo un valor por defecto (defaultValue) en caso de error
        /// </summary>
        /// <param name="inputString">Cadena de entrada a convertir (string)</param>
        /// <param name="defaultValue">Valor por defecto devuelto en caso de error (int)</param>
        /// <returns>Valor convertido o 'defaultValue' en caso de error (float)</returns>
        public static float StringToFloat(string inputString, float defaultValue)
        {
            try
            {
                return float.Parse(inputString);
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// Convierte un "string" a un "DateTime" dado el formato dado. Devuelve la fecha del sistema en caso de error
        /// </summary>
        /// <param name="inputString">Cadena de entrada a convertir (string)</param>
        /// <param name="dateFormat">Formato de la fecha a convertir, por ejemplo "yyyy-MM-dd" (string)</param>
        /// <returns>Fecha convertida o la fecha del sistema en caso de error (DateTime)</returns>
        public static DateTime StringToDateTime(string inputString, string dateFormat)
        {
            try
            {
                return DateTime.ParseExact(inputString, dateFormat, System.Globalization.CultureInfo.InvariantCulture);
            }
            catch (Exception)
            {
                return DateTime.Now;
            }
        }

        /// <summary>
        /// Elimina la "\" del final de un path en caso de que la tenga
        /// </summary>
        /// <param name="inputPath">El path de entrada (string)</param>
        /// <returns>El path sin la "\" del final</returns>
        public static string NormalizePath(string inputPath)
        {
            string lastChar = inputPath.Substring(inputPath.Length - 1, 1);

            if ((inputPath.Length > 2) && (lastChar == @"\"))
                return inputPath.Remove(inputPath.Length - 1);
            else if ((inputPath.Length > 1) && (inputPath.Substring(0, 1) == ".") && (lastChar == @"\"))
                return inputPath.Remove(inputPath.Length - 1);
            else
                return inputPath;
        }

        public static string Encrypt(string text, string key)
        {
            using (var md5 = new MD5CryptoServiceProvider())
            {
                using (var tdes = new TripleDESCryptoServiceProvider())
                {
                    tdes.Key = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                    tdes.Mode = CipherMode.ECB;
                    tdes.Padding = PaddingMode.PKCS7;

                    using (var transform = tdes.CreateEncryptor())
                    {
                        byte[] textBytes = UTF8Encoding.UTF8.GetBytes(text);
                        byte[] bytes = transform.TransformFinalBlock(textBytes, 0, textBytes.Length);
                        return Convert.ToBase64String(bytes, 0, bytes.Length);
                    }
                }
            }
        }

        public static string Decrypt(string cipher, string key)
        {
            using (var md5 = new MD5CryptoServiceProvider())
            {
                using (var tdes = new TripleDESCryptoServiceProvider())
                {
                    tdes.Key = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                    tdes.Mode = CipherMode.ECB;
                    tdes.Padding = PaddingMode.PKCS7;

                    using (var transform = tdes.CreateDecryptor())
                    {
                        byte[] cipherBytes = Convert.FromBase64String(cipher);
                        byte[] bytes = transform.TransformFinalBlock(cipherBytes, 0, cipherBytes.Length);
                        return UTF8Encoding.UTF8.GetString(bytes);
                    }
                }
            }
        }

        public static long DateTimeToUnixTime(DateTime inputDateTme)
        {
            try
            {
                return Convert.ToInt64((inputDateTme.ToUniversalTime() - epoch).TotalSeconds);
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
}
