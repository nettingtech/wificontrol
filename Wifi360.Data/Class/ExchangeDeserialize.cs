﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wifi360.Data.Class
{
    public class ExchangeDeserialize
    {
        public bool success { get; set; }
        public int timestamp { get; set; }
        public string @base { get; set; }
        public string date { get; set; }
        public ListRates rates { get; set; }
    }

    public class ListRates
    {
        public string Currencie { get; set; }
        public float rate { get; set; }

    }
}
