﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wifi360.Data.Class
{
    /// Clase anexa a la clase LogDaily que define una excepción compuesta de:
    ///    - Un tipo de log (enmerado ELogType)
    ///    - Un mensaje (string)
    ///    - Una excepción original (Exception)
    /// </summary>
    public class LogDailyException : Exception
    {
        #region Variables privadas

        private ELogType logType;
        private string internalMessage;
        private Exception originalException;

        #endregion Variables privadas

        #region Propiedades

        public ELogType LogType
        {
            get { return logType; }
        }

        public string InternalMessage
        {
            get { return internalMessage; }
        }

        public Exception OriginalException
        {
            get { return originalException; }
        }

        #endregion Propiedades

        #region Constructores

        public LogDailyException(ELogType logType, string internalMessage, Exception originalException)
        {
            this.logType = logType;
            this.internalMessage = internalMessage;
            this.originalException = originalException;
        }

        #endregion Constructores
    }
}
