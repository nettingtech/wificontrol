﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wifi360.Data.Class
{
    public class Exchanges
    {
        private int _idexchange;
        private string _currencie;
        private string _base;
        private double _exchange;
        private double _ReverseExchange;
        private DateTime _lastupdate;


        public DateTime LastUpdate
        {
            get
            {
                return _lastupdate;
            }

            set
            {
                _lastupdate = value;
            }
        }

        public int Idexchange
        {
            get
            {
                return _idexchange;
            }

            set
            {
                _idexchange = value;
            }
        }

        public string Currency
        {
            get
            {
                return _currencie;
            }

            set
            {
                _currencie = value;
            }
        }

        public string Base
        {
            get
            {
                return _base;
            }

            set
            {
                _base = value;
            }
        }

        public double Exchange
        {
            get
            {
                return _exchange;
            }

            set
            {
                _exchange = value;
            }
        }

        public double ReverseExchange
        {
            get
            {
                return _ReverseExchange;
            }

            set
            {
                _ReverseExchange = value;
            }
        }
    }
}
