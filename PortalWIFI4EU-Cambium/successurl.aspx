﻿<%@ Page Title="" Language="C#" MasterPageFile="~/resources/master/MasterSecurityPage.Master" AutoEventWireup="true" CodeBehind="successurl.aspx.cs" Inherits="PortalWIFI4EU_Cambium.successurl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row" id="principal">
            <div class="col-xs-12 col-md-8 col-lg-8 col-lg-offset-1 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="text-center" id="logohead"> </div>
                        <div class="alert alert-success text-center">
                            <label id="labelusername">Código de Acceso: <span style='color:#004085;'><%= username %> </span></label><br />
                            <label id="labelvalidtill">Vádido hasta: <span style='color:#004085;'><%= validtill %> </span></label><br />
                            <label id="labeltimecredit">Crédito de tiempo: <span style='color:#004085;'><%= timecredit %> </span></label><br />
                            <label id="labelvolumedown">Crédito de volumen: <span style='color:#004085;'><%= volumecredit %> </span></label><br />
                            <label id="labelaccesstype">Tipo de acceso: <span style='color:#004085;'><%= accesstype %> </span></label><br />
                        </div>

                        <asp:Button runat="server" ID="logout_asp_button" CssClass="btn btn-danger btn-block" Text="Desconectar" OnClick="logout_asp_button_Click" /><br />
                        <asp:Button runat="server" ID="change_asp_button" CssClass="btn btn-warning btn-block" Text="Usar otro código de acceso" OnClick="change_asp_button_Click" /><br />
                        <a href="<%= continueurl %>" id="buttonContinue" target="_blank" class="btn btn-default btn-block">Continuar a internet</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
