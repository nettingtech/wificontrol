﻿<%@ Page Title="" Language="C#" MasterPageFile="~/resources/master/MasterSecurityPage.Master" AutoEventWireup="true" CodeBehind="welcome.aspx.cs" Inherits="PortalWIFI4EU_Cambium.welcome" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        $(document).ready(function (e) {

            if ($("#cloudre").val() != '') {
                //$("#principal").hide();
                //$("#myModal").modal('hide');
                //$("#MyModalWait").modal('show');

                var continue_url = window.location.origin + "/successurl.aspx?username=" + $("#username").val();

                var action = "http://" + $("#origin").val() + ":880/cgi-bin/hotspot_login.cgi";

                var newForm = jQuery('<form>', {
                    'action': action,
                    'method': 'POST',
                }).append(jQuery('<input>', {
                    'name': 'ga_user',
                    'value': $("#username").val(),
                    'type': 'hidden'
                })).append(jQuery('<input>', {
                    'name': 'ga_pass',
                    'value': $("#password").val(),
                    'type': 'hidden'
                })).append(jQuery('<input>', {
                    'name': 'ga_Qv',
                    'value': $("#token").val(),
                    'type': 'hidden'
                })).append(jQuery('<input>', {
                    'name': 'ga_orig_url',
                    'value': continue_url,
                    'type': 'hidden'

                }));
                $(document.body).append(newForm);
                newForm.submit();
            }
            else {
                var scrollcomplete = false;
                var parameters = getParameters();
                var val = "idhotel=" + $("#idhotel").val() + "&idlocation=" + $("#idlocation").val() + "&mac=" + $("#mac").val() + "&origin=" + $("#origin").val();
                createCookie("cambium_wifi4eu", val, 1);

                var lang = navigator.browserLanguage ? navigator.browserLanguage : navigator.language;
                var langcode = lang.substring(0, 2);

                if ((langcode != 'es') && (langcode != 'en'))
                    langcode = $("#lang").val();

                var dataString = 'action=fullsite&idhotel=' + $("#idhotel").val() + "&idlocation=" + $("#idlocation").val() + "&lang=" + langcode + '&mac=' + $("#mac").val();
                $.ajax({
                    url: "../resources/handlers/Handler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    success: function (pReturn) {
                        var lReturn = JSON.parse(pReturn);
                        $("#logofinish").attr("src", "resources/images/" + $("#idhotel").val() + "/" + $("#idlocation").val() + "-logo.png");
                        $("#freeimage").attr("src", "resources/images/" + $("#idhotel").val() + "/" + $("#idlocation").val() + "-logo.png");
                        $("#disclaimerimage").attr("src", "resources/images/" + $("#idhotel").val() + "/" + $("#idlocation").val() + "-logo.png");
                        $("#noacceptimage").attr("src", "resources/images/" + $("#idhotel").val() + "/" + $("#idlocation").val() + "-logo.png");
                        $("#freetext").html(lReturn.FreeText);
                        $("#disclaimertext").html(lReturn.Disclaimer);
                        $("#disclaimermandatorytext").html(lReturn.Disclaimer);

                        if (lReturn.FreeAccessModule) {
                            $("#freeaccess").val(lReturn.freeAccess.IdBillingType);
                            $("#tab-free").show();
                            if (langcode == 'es') {
                                $("#freeaccessinformation").html('<b>Tiempo de Conexión:</b> ' + lReturn.freeAccess.TimeCredit + ' <br/><b>Validez del Ticket:</b> ' + lReturn.freeAccess.ValidTill + ' horas<br/><b>Número máximo de dispositivos:</b> ' + lReturn.freeAccess.MaxDevices + '<br/><b>Velocidad de Subida:</b> ' + lReturn.freeAccess.BWUp + ' kbps<br/><b>Velocidad de Bajada:</b> ' + lReturn.freeAccess.BWDown + ' kbps<br/>');
                            }
                            else {
                                $("#freeaccessinformation").html('<b>Time Credit:</b> ' + lReturn.freeAccess.TimeCredit + ' <br/><b>Valid Till:</b> ' + lReturn.freeAccess.ValidTill + ' hours<br/><b>Max devices:</b> ' + lReturn.freeAccess.MaxDevices + '<br/><b>Bandwidth Up:</b> ' + lReturn.freeAccess.BWUp + ' kbps<br/><b>Bandwidth Down:</b> ' + lReturn.freeAccess.BWDown + ' kbps<br/>');
                            }
                        }

                        if ($("#error").val() != "") {
                            if (langcode == 'es') {
                                switch ($("#error").val().toUpperCase()) {
                                    case 'UNKNOWN USER': $("#message").html('Usuario Desconocido'); break;
                                    case 'NO TIME CREDIT': $("#message").html('Tiempo de conexión agotado'); break;
                                    case 'TICKET EXPIRED': $("#message").html('Ticket Caducado'); break;
                                    case 'TOO MANY DEVICES': $("#message").html('Demasiados dispositivos'); break;
                                    case 'SORRY, NO MORE USERS ALLOWED': $("#message").html('Lo sentimos, no se permiten más usuarios'); break;
                                    case 'WRONG PASSWORD': $("#message").html('Contraseña incorrecta'); break;
                                    case 'UPLOAD QUOTA EXCEEDED': $("#message").html('Cuota de datos de subida agotada'); break;
                                    case 'DOWNLOAD QUOTA EXCEEDED': $("#message").html('Cuota de datos de bajada agotada'); break;

                                    default: $("#message").html($("#error").val());
                                }
                            }
                            else
                                $("#message").html($("#error").val());

                            $("#myModalError").modal('show');
                            $("#myModal").modal('hide');
                            $("#noaccept").fadeOut("fast");
                            $("#principal").fadeIn("slow");
                            $("#wait").fadeOut("slow");
                        }

                        $("#wait").hide();
                        $("#principal").show();

                        x = $('.container').height() + 20; // +20 gives space between div and footer
                        $('#footer').css('top', x + 20 + 'px');// again 100 is the height of your footer
                        $('#footer').css('display', 'block');

                        $('#modalscroll').css('overflow-y', 'auto');
                        $('#modalscroll').css('max-height', $(window).height() * 0.25);
                        $("#modalscroll").bind('scroll', chk_scroll);

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#loading").html("Se ha producido un error durante el envio. Por favor inténtelo más tarde.");
                        alert('error: ' + xhr.statusText);
                    }
                });

            }

            $('#welcome_freebutton').click(function (e) {
                var dataString = 'action=allow&idhotel=' + $("#idhotel").val() + '&idlocation=' + $("#idlocation").val() + '&mac=' + $("#mac").val() + '&origin=' + $("#origin").val();
                $.ajax({
                    url: "../resources/handlers/Handler.ashx",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    beforeSend: function () {
                        $("#MyModalWait").modal('show');
                    },
                    complete: function () {
                        $("#MyModalWait").modal('hide');
                    },
                    success: function (pResult) {
                        var lResult = JSON.parse(pResult);

                        if (lResult.allow == 1) {
                            $("#finishbutton").attr("href", lResult.Url);
                            $("#finishInformation").append($("#freeaccessinformation").html());

                            var continue_url = window.location.origin + "/successurl.aspx?username=" + lResult.UserName.toUpperCase();

                            var action = "http://" + $("#origin").val() + ":880/cgi-bin/hotspot_login.cgi";

                            var newForm = jQuery('<form>', {
                                'action': action,
                                'method': 'POST',
                            }).append(jQuery('<input>', {
                                'name': 'ga_user',
                                'value': lResult.UserName.toUpperCase(),
                                'type': 'hidden'
                            })).append(jQuery('<input>', {
                                'name': 'ga_pass',
                                'value': lResult.Password.toUpperCase(),
                                'type': 'hidden'
                            })).append(jQuery('<input>', {
                                'name': 'ga_Qv',
                                'value': $("#token").val(),
                                'type': 'hidden'
                            })).append(jQuery('<input>', {
                                'name': 'ga_orig_url',
                                'value': continue_url,
                                'type': 'hidden'

                            }));
                            $(document.body).append(newForm);
                            newForm.submit();
                        }
                        else if (lResult.allow == 2) {
                            if (langcode != 'es') {
                                $("#message").html("No more free access allowed now.");
                            }
                            else {
                                $("#message").html("No se permiten más accesos gratuitos en este periodo.");
                            }
                            $("#myModalError").modal('show');
                        }
                        else if (lResult.allow == 0) {
                            if ($("#mandatorysurvey").val() != "true") {
                                e.preventDefault();

                                var dataString = 'action=freeaccessnosurvey&idhotel=' + $("#idhotel").val() + '&idlocation=' + $("#idlocation").val() + '&mac=' + $("#mac").val() + '&origin=' + $("#origin").val() + "&idbilling=" + $("#freeaccess").val();
                                $.ajax({
                                    url: "../resources/handlers/Handler.ashx",
                                    data: dataString,
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "text",
                                    beforeSend: function () {
                                        $("#MyModalWait").modal('show');
                                    },
                                    complete: function () {
                                        $("#MyModalWait").modal('hide');
                                    },
                                    success: function (pResult) {
                                        var lResult = JSON.parse(pResult);

                                        if (lResult.IdUser == -1) {
                                            if (langcode != 'es') {
                                                $("#message").html("No more free access are allowed now.");
                                            }
                                            else {
                                                $("#message").html("No se permiten más accesos gratuitos en este periodo.");
                                            }
                                            $("#myModalError").modal('show');
                                        }
                                        else if (lResult.IdUser == -3) {
                                            if (langcode != 'es') {
                                                $("#message").html("We are currently experiencing a problem, please try again later.");
                                            }
                                            else {
                                                $("#message").html("Tenemos un problema temporal, intentelo de nuevo más tarde.");
                                            }
                                            $("#myModalError").modal('show');
                                        }
                                        else if (lResult.IdUser == 0) {
                                            if (langcode != 'es') {
                                                $("#message").html("We are currently experiencing a problem, please try again later.");
                                            }
                                            else {
                                                $("#message").html("Tenemos un problema temporal, intentelo de nuevo más tarde.");
                                            }
                                            $("#myModalError").modal('show');
                                        }
                                        else {
                                            $("#finishbutton").attr("href", lResult.Url);
                                            $("#finishInformation").append($("#freeaccessinformation").html());

                                            var continue_url = window.location.origin + "/successurl.aspx?username=" + lResult.UserName.toUpperCase();

                                            var action = "http://" + $("#origin").val() + ":880/cgi-bin/hotspot_login.cgi";

                                            var newForm = jQuery('<form>', {
                                                'action': action,
                                                'method': 'POST',
                                            }).append(jQuery('<input>', {
                                                'name': 'ga_user',
                                                'value': lResult.UserName.toUpperCase(),
                                                'type': 'hidden'
                                            })).append(jQuery('<input>', {
                                                'name': 'ga_pass',
                                                'value': lResult.Password.toUpperCase(),
                                                'type': 'hidden'
                                            })).append(jQuery('<input>', {
                                                'name': 'ga_Qv',
                                                'value': $("#token").val(),
                                                'type': 'hidden'
                                            })).append(jQuery('<input>', {
                                                'name': 'ga_orig_url',
                                                'value': continue_url,
                                                'type': 'hidden'

                                            }));
                                            $(document.body).append(newForm);
                                            newForm.submit();

                                            if (langcode == 'es') {
                                                $("#finishlabel").html("Enhorabuena");
                                                $("#finishInformation").append('<b>Nombre de usuario: </b>' + lResult.UserName + '<br />');
                                                $("#finishInformation").append('<b>Contraseña: </b>' + lResult.Password + '<br />');
                                                $("#finishInformation").append('<b>Fecha de expiración: </b>' + lResult.ValidTill + '<br />');
                                                $("#finishbutton").val("Continuar navegando");
                                            }
                                            else {
                                                $("#finishInformation").append('<b>UserName: </b>' + lResult.UserName + '<br />');
                                                $("#finishInformation").append('<b>Password: </b>' + lResult.Password + '<br />');
                                                $("#finishInformation").append('<b>Valid Till: </b>' + lResult.ValidTill + '<br />');
                                                $("#finishbutton").val("Continue browsing");
                                            }
                                        }
                                    },
                                    error: function (xhr, ajaxOptions, thrownError) {
                                        $("#loading").html("Se ha producido un error durante el envio. Por favor inténtelo más tarde.");
                                        alert('error: ' + xhr.statusText);
                                        $("#nextstep3").show();
                                        $("#backstep3").show();

                                    }
                                });
                            }
                            else {
                                $("#myModalSurvey").modal('show');
                            }

                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#loading").html("Se ha producido un error durante el envio. Por favor inténtelo más tarde.");
                        alert('error: ' + xhr.statusText);
                        $("#nextstep3").show();
                        $("#backstep3").show();

                    }
                });
            });

            $("#finishbutton").click(function (e) {
                e.preventDefault();
                var url = $("#finishbutton").attr("href");
                window.location.href = url;

            });

            function chk_scroll(e) {
                var elem = $(e.currentTarget);

                if (elem.scrollTop() >= (elem[0].scrollHeight - elem.outerHeight()) * 0.8) {
                    scrollcomplete = true;
                    $("#welcome_freebutton").removeAttr('disabled');
                    $("#welcome_freebutton").removeClass().addClass("btn btn-primary btn-lg btn-block");
                }

            }

            $('#myModal').on('show.bs.modal', function () {

            });

            $("#errorbutton").click(function (e) {
                e.preventDefault();
                $("#myModalError").modal('hide');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="container">
        <div class="row" id="row_wifi4eu">
            <div class="col-sm-12">
                <img id="wifi4eulogo">
             </div>
         </div>
<%--        <div class="row" id="wait">
            <div class="col-xs-12 col-sm-12 col-md-12">
                  <div id="borderbox" class="panel panel-default">
                  <div class="panel-body">
                    <div class="progress">
                            <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                <span class="sr-only">Loading - Waiting please</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>--%>
        <div class="row" id="principal" style="margin-top: 10px; display:none;">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <p class="text-center"> <img src="resources/images/no-disponible.png" style="width:150px" id="freeimage"/></p> 
                        <div class="panel panel-info"><div class="panel-body" id="freetext"></div></div>
                        <div class="panel panel-info">
                            <div id="modalscroll">
                                <div class="panel-body" id="disclaimertext"></div>
                            </div>
                        </div>
                        <button type="button" class="btn btn-default btn-lg btn-block" id="welcome_freebutton" disabled="disabled">Continue to internet!</button>
                    </div>
                  </div>
                </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" id="disclaimermandatory" style="display:none;">
                <a href="#" id="disclaimerMandatoryButton" class="btn btn-default">Legal Advice</a>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModalError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title text-center" id="H2">Error</h4>
                </div>
                <div class="modal-body">
                    <div id="message" class="alert alert-danger text-center"></div> 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="errorbutton">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModalFinish" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center" id="finishlabel">Congratulations</h4>
                </div>
                <div class="modal-body">
                    <div id="finishInformation" style="text-align:center" ><img src="resources/images/no-disponible.png" alt="logo hotel" id="logofinish" style="width:150px"  /><br /><br /></div>
                    <br />
                    <input type="button" id="finishbutton" class="btn btn-success btn-block" value="Continue browsing" /> 
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="MyModalWait" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center" id="waitheader">Please, wait a moment</h4>
                </div>
                <div class="modal-body">
                    <div class="progress">
                        <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                            <span class="sr-only">Waiting please</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="accesstypes" style="display:none"></div>
        <div id="freeaccessinformation" style="display:none"></div>
        <input type="text" hidden="hidden" id="freeaccess" />
</asp:Content>
