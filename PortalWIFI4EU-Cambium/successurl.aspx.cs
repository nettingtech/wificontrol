﻿using PortalWIFI4EU_Cambium.resources.master;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Wifi360.Data.Class;
using Wifi360.Data.Controllers;
using Wifi360.Data.Model;

namespace PortalWIFI4EU_Cambium
{
    public partial class successurl : System.Web.UI.Page
    {
        protected string username = string.Empty;
        protected string validtill = string.Empty;
        protected string timecredit = string.Empty;
        protected string volumecredit = string.Empty;
        protected string accesstype = string.Empty;
        protected string continueurl = string.Empty;
        protected string logout_url = string.Empty;
        protected int nt_st = 0;
        protected int nt_lt = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                try
                {
                    var isLog = Boolean.Parse(ConfigurationManager.AppSettings["isLog"].ToString());
                    var idproceso = Guid.NewGuid();

                    if (Context.Request.Params["username"] != null)
                        username = Context.Request.Params["username"].ToString();

                    nt_st = int.Parse(((MasterSecurityPage)this.Master).idhotel);
                    nt_lt = int.Parse(((MasterSecurityPage)Master).idlocation);

                    SitesGMT siteGMT = GetSiteGMt(nt_lt, nt_st);

                    Users user = BillingController.GetRadiusUser(username, nt_st);
                    if (user.IdUser != 0)
                    {
                        bool little_joke = false;
                        var little_joke_info = "Intento fallido: ";
                        string Real_session_nas_mac = string.Empty;
                        //Tenemos mac obtenemos activesessions con GetActiveSessionMAC
                        ActiveSessions Real_session = new ActiveSessions();
                        //Real_session = BillingController.GetActiveSessionMAC(((MerakiSecureMaster)Master).mac);
                        Real_session = BillingController.GetActiveSession(((MasterSecurityPage)Master).mac, nt_st);
                        //con activesession obtenemos nasMAc
                        if (Real_session.AcctSessionID != null)
                        {
                            Real_session_nas_mac = Real_session.NAS_MAC;     // E0-CB-BC-35-D5-67:ON_SPOT  (idhotel 234) o tb hotspot_cunef_2019
                            Real_session_nas_mac.Split(':');
                            //con nasMAc location y site y comparamos con los encryptados (ya están en OBJ)

                            if (!(Real_session_nas_mac[0].Equals(((MasterSecurityPage)Master).ga_mac)) || !(Real_session_nas_mac[1].Equals(((MasterSecurityPage)Master).ga_ssid)))
                            {
                                little_joke = true;
                                little_joke_info += "NAS_MAC ";
                            }

                            BillingController.GetLocation(nt_lt);
                            if (!Real_session.IdHotel.Equals(nt_st))
                            {
                                little_joke = true;
                                little_joke_info += " LOCATION.";
                            }
                        }

                        if (little_joke)
                        {
                            //POCA BROMA SARDINAS CUCA!
                            CaptivePortalLogAttack new_attack = new CaptivePortalLogAttack();
                            new_attack.idLog = 0;
                            new_attack.device_mac = ((MasterSecurityPage)Master).mac;
                            new_attack.ip = string.Empty;
                            new_attack.info = little_joke_info;
                            new_attack.info += "ActiveSession ID: " + Real_session.IdActiveSession;
                            new_attack.nas_mac_correcta = Real_session_nas_mac[0].ToString() + ":" + Real_session_nas_mac[0].ToString();
                            new_attack.username = username;
                            new_attack.nas_mac_origen = ((MasterSecurityPage)Master).ga_mac + ":" + ((MasterSecurityPage)Master).ga_ssid;
                            new_attack.portal = "CAMBIUM WIFI4EU";
                            new_attack.date = DateTime.Now.ToUniversalTime();

                            BillingController.SaveCaptivePortalLogAttack(new_attack);

                            //redireccionamos a LOGOUT_URL (RADIUOS COA). Lo dejamos comentado porque para detecta atque mi MAC con otro usuario en otra RED

                            string ga_ssid = ((MasterSecurityPage)Master).ga_ssid;
                            string ga_nas_id = ((MasterSecurityPage)Master).ga_nas_id;
                            string ga_ap_mac = ((MasterSecurityPage)Master).ga_mac;
                            string ga_cmac = ((MasterSecurityPage)Master).mac;
                            string token = ((MasterSecurityPage)Master).token;
                            string ga_srvr = ((MasterSecurityPage)Master).origin;

                            logout_url = string.Format("http://{0}:880/1/welcome.html?ga_ap_mac={1}&ga_nas_id={2}&ga_srvr={0}&ga_cmac={3}&ga_Qv={4}", ga_srvr, ga_ap_mac, ga_nas_id, ga_cmac, token);
                            //      Response.Redirect(logout_url, false);
                            //      Response.End();
                        }

                        BillingTypes2 billingtype = BillingController.ObtainBillingType2(user.IdBillingType);

                        DateTime validTill = DateTime.MinValue;
                        List<ClosedSessions2> closedSessions = BillingController.GetClosedSession(nt_st, username, user.ValidSince, user.ValidTill);
                        if (closedSessions.Count > 0)
                        {
                            ClosedSessions2 closed = (from d in closedSessions orderby d.SessionTerminated descending select d).FirstOrDefault();
                            if (closed != null)
                                validTill = closed.SessionTerminated.Value.AddHours(billingtype.ValidAfterFirstUse);
                        }
                        else
                        {
                            ActiveSessions active = BillingController.GetActiveSession(((MasterSecurityPage)Master).mac, nt_st);
                            if (!active.IdActiveSession.Equals(0))
                                validTill = active.SessionStarted.Value.AddHours(billingtype.ValidAfterFirstUse);
                            else
                                validTill = DateTime.Now.ToUniversalTime().AddHours(billingtype.ValidAfterFirstUse);
                        }

                        if (validTill > user.ValidTill)
                            validtill = user.ValidTill.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                        else
                            validtill = validTill.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");

                        TimeSpan t = TimeSpan.FromSeconds(user.TimeCredit / user.MaxDevices);
                        string time = string.Empty;
                        if (user.TimeCredit >= 86400)
                            time = string.Format("{0} d: {1:D2} h: {2:D2} m: {3:D2} s", t.Days, t.Hours, t.Minutes, t.Seconds);
                        else
                            time = string.Format("{0:D2} h: {1:D2} m: {2:D2} s", t.Hours, t.Minutes, t.Seconds);

                        timecredit = time;
                        volumecredit = string.Format("{0:0.00} MB", Math.Round(double.Parse((user.VolumeDown / 1048576).ToString()), 2));
                        accesstype = billingtype.Description;
                        continueurl = ((MasterSecurityPage)Master).urlDest;
                    }
                }
                catch (Exception ex)
                {
                    CaptivePortalLogInternal internallog = new CaptivePortalLogInternal();
                    try
                    {
                        internallog.IdHotel = int.Parse(((MasterSecurityPage)Master).idhotel);
                        internallog.IdLocation = int.Parse(((MasterSecurityPage)Master).idlocation);
                        internallog.CallerID = ((MasterSecurityPage)Master).mac;
                        internallog.NomadixResponse = ((MasterSecurityPage)Master).urlDest;
                        if (ex.StackTrace == null)
                        {
                            internallog.RadiusResponse = ex.InnerException.Message + " trace: " + ex.StackTrace;
                        }
                        internallog.UserName = ((MasterSecurityPage)Master).username;
                        internallog.Page = "Success.aspx";
                        internallog.NSEId = string.Empty;
                        internallog.Password = string.Empty;
                        BillingController.SaveCaptivePortalLogInternal(internallog);
                    }
                    catch {; }
                }
            }
        }

        private static SitesGMT GetSiteGMt(int idlocation, int idhotel)
        {
            SitesGMT siteGMT = null;
            Locations2 location = null;
            Hotels hotel = null;
            try
            {
                if (idlocation != 0)
                {
                    location = BillingController.GetLocation(idlocation);
                    hotel = BillingController.GetHotel(location.IdHotel);
                    siteGMT = BillingController.SiteGMTSitebyLocation(location.IdLocation);
                    if (siteGMT.Id.Equals(0))
                        siteGMT = BillingController.SiteGMTSitebySite(location.IdHotel);
                }
                else
                {
                    siteGMT = BillingController.SiteGMTSitebySite(idhotel);
                }

                return siteGMT;
            }
            catch (Exception ex)
            {
                return null;
            }


        }

        protected void logout_asp_button_Click(object sender, EventArgs e)
        {
            try
            {
                string nt_token = Session["nt_crc"].ToString();
                List<ParametrosConfiguracion> parametros = BillingController.GetParametrosConfiguracion(0);
                ParametrosConfiguracion key = (from t in parametros where t.Key.ToUpper().Equals("ENCRYPTIONKEY") select t).FirstOrDefault();
                string[] nt_token_decrypt = Misc.Decrypt(nt_token, key.value).Split('|');

                string[] nt_token_decrypt_A = nt_token_decrypt[0].Split('=');
                string[] nt_token_decrypt_B = nt_token_decrypt[1].Split('=');
                string[] nt_token_decrypt_C = nt_token_decrypt[2].Split('=');
                string[] nt_token_decrypt_D = nt_token_decrypt[3].Split('=');
                string[] nt_token_decrypt_E = nt_token_decrypt[4].Split('=');
                string[] nt_token_decrypt_F = nt_token_decrypt[5].Split('=');
                string[] nt_token_decrypt_G = nt_token_decrypt[6].Split('=');
                string[] nt_token_decrypt_H = nt_token_decrypt[7].Split('=');
                string[] nt_token_decrypt_I = nt_token_decrypt[8].Split('=');
                string[] nt_token_decrypt_J = nt_token_decrypt[9].Split('=');
                string[] nt_token_decrypt_K = nt_token_decrypt[10].Split('=');

                if (nt_token_decrypt_A[0].Equals("nt_st") && nt_token_decrypt_B[0].Equals("nt_lt") && nt_token_decrypt_C[0].Equals("nt_client_mac") &&
                    nt_token_decrypt_D[0].Equals("nt_origin") && nt_token_decrypt_E[0].Equals("nt_urlDest") && nt_token_decrypt_F[0].Equals("nt_ap_mac") &&
                    nt_token_decrypt_G[0].Equals("nt_ap_ssid") && nt_token_decrypt_H[0].Equals("nt_ap_nas_id") && nt_token_decrypt_I[0].Equals("nt_token") &&
                    nt_token_decrypt_J[0].Equals("nt_error") && nt_token_decrypt_K[0].Equals("epoch"))
                {

                    string ga_cmac = nt_token_decrypt_C[1];
                    string ga_srvr = nt_token_decrypt_D[1];
                    string ap_urlDest = nt_token_decrypt_E[1];
                    string ap_error = nt_token_decrypt_J[1];
                    string ga_ap_mac = nt_token_decrypt_F[1];
                    string ga_ssid = nt_token_decrypt_G[1];
                    string ga_nas_id = nt_token_decrypt_H[1];
                    string token = nt_token_decrypt_I[1];

                    logout_url = string.Format("http://{0}:880/1/welcome.html?ga_ap_mac={1}&ga_nas_id={2}&ga_srvr={0}&ga_cmac={3}&ga_Qv={4}",
                                    ga_srvr, ga_ap_mac, ga_nas_id, ga_cmac, token);

                    logout_asp_button.Text = logout_url;
                     Response.Redirect(logout_url, false);
                }

            }
            catch (Exception ex)
            {
                ;
            }

        }

        protected void change_asp_button_Click(object sender, EventArgs e)
        {
            try
            {
                string nt_token = Session["nt_crc"].ToString();
                List<ParametrosConfiguracion> parametros = BillingController.GetParametrosConfiguracion(0);
                ParametrosConfiguracion key = (from t in parametros where t.Key.ToUpper().Equals("ENCRYPTIONKEY") select t).FirstOrDefault();
                string[] nt_token_decrypt = Misc.Decrypt(nt_token, key.value).Split('|');

                string[] nt_token_decrypt_A = nt_token_decrypt[0].Split('=');
                string[] nt_token_decrypt_B = nt_token_decrypt[1].Split('=');
                string[] nt_token_decrypt_C = nt_token_decrypt[2].Split('=');
                string[] nt_token_decrypt_D = nt_token_decrypt[3].Split('=');
                string[] nt_token_decrypt_E = nt_token_decrypt[4].Split('=');
                string[] nt_token_decrypt_F = nt_token_decrypt[5].Split('=');
                string[] nt_token_decrypt_G = nt_token_decrypt[6].Split('=');
                string[] nt_token_decrypt_H = nt_token_decrypt[7].Split('=');
                string[] nt_token_decrypt_I = nt_token_decrypt[8].Split('=');
                string[] nt_token_decrypt_J = nt_token_decrypt[9].Split('=');
                string[] nt_token_decrypt_K = nt_token_decrypt[10].Split('=');

                if (nt_token_decrypt_A[0].Equals("nt_st") && nt_token_decrypt_B[0].Equals("nt_lt") && nt_token_decrypt_C[0].Equals("nt_client_mac") &&
                    nt_token_decrypt_D[0].Equals("nt_origin") && nt_token_decrypt_E[0].Equals("nt_urlDest") && nt_token_decrypt_F[0].Equals("nt_ap_mac") &&
                    nt_token_decrypt_G[0].Equals("nt_ap_ssid") && nt_token_decrypt_H[0].Equals("nt_ap_nas_id") && nt_token_decrypt_I[0].Equals("nt_token") &&
                    nt_token_decrypt_J[0].Equals("nt_error") && nt_token_decrypt_K[0].Equals("epoch"))
                {

                    string ga_cmac = nt_token_decrypt_C[1];
                    string ga_srvr = nt_token_decrypt_D[1];
                    string ap_urlDest = nt_token_decrypt_E[1];
                    string ap_error = nt_token_decrypt_J[1];
                    string ga_ap_mac = nt_token_decrypt_F[1];
                    string ga_ssid = nt_token_decrypt_G[1];
                    string ga_nas_id = nt_token_decrypt_H[1];
                    string token = nt_token_decrypt_I[1];

                    int idhotel = int.Parse(nt_token_decrypt_A[1]);
                    int idlocation = int.Parse(nt_token_decrypt_B[1]);

                    BillingController.DeleteRememberMeTableUser(ga_cmac, idhotel);

                    logout_url = string.Format("http://{0}:880/1/welcome.html?ga_ap_mac={1}&ga_nas_id={2}&ga_srvr={0}&ga_cmac={3}&ga_Qv={4}",
                                        ga_srvr, ga_ap_mac, ga_nas_id, ga_cmac, token);

                    Response.Redirect(logout_url, false);
                }
            }
            catch (Exception ex)
            {
                ;
            }
        }
    }
}