﻿using PortalWIFI4EU_Cambium.resources.response;
using PortalWIFI4EU_Cambium.resources.utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using Wifi360.Data.Controllers;
using Wifi360.Data.Model;

namespace PortalWIFI4EU_Cambium.resources.handlers
{
    /// <summary>
    /// Descripción breve de Handler
    /// </summary>
    public class Handler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            if (string.IsNullOrEmpty(context.Request.Params["action"])) ProcessEmtpy(context);
            var jss = new JavaScriptSerializer();
            string json = new StreamReader(context.Request.InputStream).ReadToEnd();
            Dictionary<string, string> sData = jss.Deserialize<Dictionary<string, string>>(json);
            if (string.IsNullOrEmpty(json))
            {
                switch (context.Request.Params["action"].ToUpper())
                {
                    case "ALLOW": allowfree(context); break;
                    case "FREEACCESSNOSURVEY": freeaccessnosurvey(context); break;
                    case "FULLSITE": fullsite(context); break;
                    default: ProcessEmtpy(context); break;
                }
            }
        }

        private UserResponse allowfree(HttpContext context)
        {
            CaptivePortalLog log = new CaptivePortalLog();
            UserResponse response = new UserResponse();
            try
            {
                string idhotel = (context.Request["idhotel"] == null ? string.Empty : context.Request["idhotel"].ToString());
                string mac = (context.Request["mac"] == null ? string.Empty : context.Request["mac"].ToString());
                string idlocation = (context.Request["idlocation"] == null ? string.Empty : context.Request["idlocation"].ToString());
                string mailing = (context.Request["mailing"] == null ? string.Empty : context.Request["mailing"].ToString());
                string lang = (context.Request["lang"] == null ? string.Empty : context.Request["lang"].ToString());
                string origin = (context.Request["origin"] == null ? string.Empty : context.Request["origin"].ToString());

                Hotels hotel = BillingController.GetHotel(Int32.Parse(idhotel));
                Locations2 location = BillingController.ObtainLocation(Int32.Parse(idlocation));

                log.IdHotel = hotel.IdHotel;
                log.CallerID = mac;
                log.Date = DateTime.Now.ToUniversalTime();
                log.UserName = "";
                log.Password = "";
                log.Page = "freeaccess reconnect";
                log.IdLocation = Int32.Parse(idlocation);
                log.IdHotel = hotel.IdHotel;
                log.NSEId = "Cambium Wifi4EU - IdHotel: " + hotel.IdHotel;
                log.RadiusResponse = "NOT APPLICABLE";
                log.NomadixResponse = "NOT APPLICABLE";

                Users lastUser = BillingController.GetRadiusUsermac(mac, hotel.IdHotel);
                if (!lastUser.IdUser.Equals(0))
                {
                    BillingTypes bt = BillingController.ObtainBillingType(lastUser.IdBillingType);
                    log.IdLocation = bt.IdLocation;
                    if ((lastUser.Enabled))
                    {
                        response.allow = 1; //PASA DIRECTAMENTE
                        response.IdUser = 1;
                        response.UserName = lastUser.Name;
                        response.Password = lastUser.Password;
                        log.UserName = lastUser.Name.ToUpper();
                        log.Password = lastUser.Password.ToUpper();
                        response.ValidTill = lastUser.ValidTill.AddHours(hotel.GMT).ToString("dd/MM/yyyy HH:mm:ss");
                        if (string.IsNullOrEmpty(bt.UrlLanding))
                        {
                            if (string.IsNullOrEmpty(location.UrlLanding))
                                response.Url = hotel.UrlHotel;
                            else
                                response.Url = location.UrlLanding;
                        }
                        else
                        {
                            response.Url = bt.UrlLanding;
                        }
                        //DEMO
                        //response.Url = location.UrlLanding;
                    }
                    else if (lastUser.ValidSince.AddHours(hotel.FreeAccessRepeatTime) > DateTime.Now.ToUniversalTime())
                    {
                        response.allow = 2; //SI RESPONSE ALLOW = 2, NO PUEDE PASAR

                        log.Page = "No more free access allowed";
                        log.RadiusResponse = "No more free access allowed";
                        log.NomadixResponse = "NOT APPLICABLE";
                        log.UserName = lastUser.Name.ToUpper();
                        log.Password = lastUser.Password.ToUpper();
                        log.NSEId = "Cambium Wifi4EU - IdHotel: " + hotel.IdHotel;
                        log.CallerID = mac;
                        log.Date = DateTime.Now.ToUniversalTime();
                    }
                    else
                    {
                        response.allow = 0; //SI RESPONSE ALLOW = 0, HAY USUARIO ANTERIOR PERO ESTA FUERA DEL PLAZO O NO ES GRATUITO
                    }
                }
                else
                {
                    response.allow = 0; //ALLOW = 0, NO HAY UN USUARIO ANTERIOR
                }
            }
            catch
            {

            }

            BillingController.SaveCaptivePortalLog(log);

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private UserResponse freeaccessnosurvey(HttpContext context)
        {
            CaptivePortalLog log = new CaptivePortalLog();
            UserResponse response = new UserResponse();
            try
            {
                string name = (context.Request["name"] == null ? string.Empty : context.Request["name"].ToString());
                string surname = (context.Request["surname"] == null ? string.Empty : context.Request["surname"].ToString());

                string idhotel = (context.Request["idhotel"] == null ? string.Empty : context.Request["idhotel"].ToString());
                string mac = (context.Request["mac"] == null ? string.Empty : context.Request["mac"].ToString());
                string idlocation = (context.Request["idlocation"] == null ? string.Empty : context.Request["idlocation"].ToString());
                string mailing = (context.Request["mailing"] == null ? string.Empty : context.Request["mailing"].ToString());
                string origin = (context.Request["origin"] == null ? string.Empty : context.Request["origin"].ToString());
                string idbillingtype = (context.Request["idbilling"] == null ? string.Empty : context.Request["idbilling"].ToString());

                Hotels hotel = BillingController.GetHotel(Int32.Parse(idhotel));
                Locations2 location = BillingController.ObtainLocation(Int32.Parse(idlocation));

                SitesGMT siteGMT = BillingController.SiteGMTSitebyLocation(location.IdLocation);
                if (siteGMT.Id.Equals(0))
                    siteGMT = BillingController.SiteGMTSitebySite(location.IdHotel);

                log.NSEId = string.Format("Cambium Wifi4EU - Idhotel:{0}", idhotel);
                log.CallerID = mac;
                log.Date = DateTime.Now.ToUniversalTime();
                log.UserName = "";
                log.Password = "";
                log.Page = "freeaccess no survey";
                log.IdLocation = Int32.Parse(idlocation);
                log.RadiusResponse = "NOT APPLICABLE";
                log.NomadixResponse = "NOT APPLICABLE";

                log.IdHotel = hotel.IdHotel;
                BillingTypes billingType = BillingController.ObtainBillingType(Int32.Parse(idbillingtype));

                bool allow = true;
                Users lastUser = BillingController.GetRadiusUsermac(mac, hotel.IdHotel);
                if (!lastUser.IdUser.Equals(0))
                {
                    BillingTypes bt = BillingController.ObtainBillingType(lastUser.IdBillingType);
                    if ((bt.IdBillingModule.Equals(1) || bt.IdBillingModule.Equals(3)) && lastUser.ValidSince > DateTime.Now.ToUniversalTime().AddHours(-hotel.FreeAccessRepeatTime))
                        allow = false;
                }

                if (allow)
                {
                    Rooms room = BillingController.GetRoom(Int32.Parse(idhotel), "DEFAULT");
                    List<ParametrosConfiguracion> parametros = BillingController.GetParametrosConfiguracion(hotel.IdHotel);
                    int longUserName = 3;
                    int longPassword = 3;
                    string prefixUserName = string.Empty;

                    foreach (ParametrosConfiguracion obj in parametros)
                    {
                        switch (obj.Key.ToUpper())
                        {
                            case "LONGUSERNAME": longUserName = Int32.Parse(obj.value); break;
                            case "LONGPASSWORD": longPassword = Int32.Parse(obj.value); break;
                            case "USERNAMEPREFIX": prefixUserName = obj.value; break;
                        }
                    }

                    if (longPassword < 3)
                        longPassword = 3;
                    if (longUserName < 3)
                        longUserName = 3;

                    Users existuser = null;
                    string username = string.Empty;
                    do
                    {
                        username = string.Format("{0}{1}", prefixUserName, util.createRandomUserNumber(longUserName)).ToUpper();
                        existuser = BillingController.GetRadiusUser(username.ToUpper(), hotel.IdHotel);

                    } while (existuser.Enabled);

                    string password = util.CreateRandomPassword(longPassword).ToUpper();

                    log.UserName = username.ToUpper();
                    log.Password = password.ToUpper();

                    Users user = new Users();
                    user.IdHotel = Int32.Parse(idhotel);
                    user.IdRoom = room.IdRoom;
                    user.IdBillingType = billingType.IdBillingType;
                    user.BWDown = billingType.BWDown;
                    user.BWUp = billingType.BWUp;
                    user.ValidSince = DateTime.Now.ToUniversalTime(); // bill.BillingDate;
                    user.ValidTill = DateTime.Now.ToUniversalTime().AddHours(billingType.ValidTill); // bill.BillingDate.AddHours(billingType.ValidTill);
                    user.TimeCredit = billingType.TimeCredit;
                    user.MaxDevices = billingType.MaxDevices;
                    user.Priority = billingType.IdPriority;
                    user.VolumeDown = billingType.VolumeDown;
                    user.VolumeUp = billingType.VolumeUp;
                    user.Enabled = true;
                    user.Name = username.ToUpper();
                    user.Password = password.ToUpper();
                    user.CHKO = true;
                    user.BuyingFrom = 1;
                    user.BuyingCallerID = mac;
                    user.Comment = string.Empty;
                    user.IdLocation = location.IdLocation;
                    user.Filter_Id = billingType.Filter_Id;

                    BillingController.SaveUser(user);

                    response.IdUser = 1;
                    response.UserName = user.Name;
                    response.Password = user.Password;

                    TimeSpan diferencia = user.ValidTill - user.ValidSince;

                    if (diferencia.TotalSeconds > user.TimeCredit)
                        response.ValidTill = user.ValidSince.AddSeconds(user.TimeCredit).AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                    else
                        response.ValidTill = user.ValidSince.AddSeconds(diferencia.TotalSeconds).AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");


                    if (string.IsNullOrEmpty(billingType.UrlLanding))
                    {
                        if (string.IsNullOrEmpty(location.UrlLanding))
                            response.Url = hotel.UrlHotel;
                        else
                            response.Url = location.UrlLanding;
                    }
                    else
                    {
                        response.Url = billingType.UrlLanding;
                    }
                    //DEMO
                    //response.Url = location.UrlLanding;

                    log.RadiusResponse = "OK";

                    //VALIDAMOS EL USERAGENT
                    UserAgentsTemp temp = BillingController.GetUserAgentTemp(mac);
                    if (temp.IdTemp != 0)
                    {
                        UserAgents userAgent = BillingController.GetUserAgent(temp.IdUserAgent);
                        List<UserAgents> listUA = BillingController.GetUserAgents(userAgent.String);

                        userAgent.Valid = userAgent.Valid + 1; ;
                        BillingController.SaveUserAgent(userAgent);
                        foreach (UserAgents x in listUA)
                        {
                            x.Valid = x.Valid + 1; ;
                            BillingController.SaveUserAgent(x);
                        }
                    }

                    List<UserAgents> uAtoBlackList = BillingController.GetUserAgentsToBlackList(Int32.Parse(ConfigurationManager.AppSettings["MaxAttemps"].ToString()));
                    foreach (UserAgents uA in uAtoBlackList)
                    {
                        if (uA.Valid.Equals(0))
                        {
                            BlackListUserAgents bUA = BillingController.GetBlackUserAgent(uA.String);
                            if (bUA.IdBlackUserAgent.Equals(0))
                            {
                                BlackListUserAgents obj = new BlackListUserAgents();
                                obj.String = uA.String;
                                BillingController.SaveBlackListUserAgent(obj);
                            }
                        }
                    }
                }
                else
                {
                    response.IdUser = -1;
                    log.NomadixResponse = "Only one free access in 24 hours";
                    log.RadiusResponse = "Only one free access in 24 hours";
                }
            }
            catch (Exception ex)
            {
                log.NomadixResponse = "OK";
                log.RadiusResponse = "OK";

                CaptivePortalLogInternal internalLog = new CaptivePortalLogInternal();
                internalLog.NSEId = log.NSEId;
                internalLog.CallerID = log.CallerID;
                internalLog.Date = DateTime.Now.ToUniversalTime();
                internalLog.UserName = "";
                internalLog.Password = "";
                internalLog.Page = "freeaccess";
                internalLog.IdLocation = 0;
                internalLog.NomadixResponse = "ERROR NO CONTEMPLADO - " + ex.Message;
                internalLog.RadiusResponse = "OK";

                BillingController.SaveCaptivePortalLogInternal(internalLog);

            }

            BillingController.SaveCaptivePortalLog(log);

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        private FullSiteResponse fullsite(HttpContext context)
        {
            FullSiteResponse response = new FullSiteResponse();
            try
            {
                string idhotel = (context.Request["idhotel"] == null ? string.Empty : context.Request["idhotel"].ToString());
                string idlocation = (context.Request["idlocation"] == null ? string.Empty : context.Request["idlocation"].ToString());
                string lang = (context.Request["lang"] == null ? string.Empty : context.Request["lang"].ToString());
                string origin = (context.Request["origin"] == null ? string.Empty : context.Request["origin"].ToString());
                string mac = (context.Request["mac"] == null ? string.Empty : context.Request["mac"].ToString());

                // OBTENEMOS LOS DATOS DEL HOTEL Y EL IDIOMA QUE TENEMOS
                Hotels hotel = BillingController.GetHotel(Int32.Parse(idhotel));
                response.IdHotel = hotel.IdHotel;
                Languages language = BillingController.GetLanguage(lang);
                if (language.IdLanguage.Equals(0))
                    language.IdLanguage = hotel.IdLanguage;

                response.IdLanguageDefault = hotel.IdLanguage;

                //OBTENEMOS LOS DATOS DE PRIVACIDAD Y AVISO LEGAL Y LOS CONCATENAMOS
                Disclaimers dis1 = BillingController.GetDisclaimer(response.IdHotel, Int32.Parse(idlocation), language.IdLanguage, 1);
                Disclaimers dis2 = BillingController.GetDisclaimer(response.IdHotel, Int32.Parse(idlocation), language.IdLanguage, 2);
                if (dis1.IdDisclaimer != 0)
                    response.Disclaimer = dis1.Text;
                if (dis2.IdDisclaimer != 0)
                    response.Disclaimer += dis2.Text;

                //ACTIVAMOS LOS DIFERENTES MÓDULOS DE ACCESO
                response.MandatorySurvey = hotel.Survey;
                response.LoginModule = hotel.LoginModule;
                response.FreeAccessModule = hotel.FreeAccessModule;
                response.PayAccessModule = hotel.PayAccessModule;
                response.SocialNetworksModule = hotel.SocialNetworksModule;
                response.CustomAccessModule = hotel.CustomAccessModule;
                response.VoucherModule = hotel.VoucherModule;

                //OBTENEMOS LA LOCALIZACION Y EMPEZAMOS A CONFIGURAR LA MISMA
                Locations2 location = location = BillingController.ObtainLocation(Int32.Parse(idlocation));
                response.DefaultModule = location.DefaultModule;
                response.DisclaimerMandatory = location.Disclaimer;

                //ASIGNAMOS LOS TEXTOS POR MODULO E IDIOMA
                List<LocationModuleText> locationTexts = BillingController.GetLocationModuleTexts(location.IdLocation, language.IdLanguage);
                foreach (LocationModuleText obj in locationTexts)
                {
                    switch (obj.IdBillingModule)
                    {
                        case 1: response.FreeText = obj.Text; break;
                        case 2: response.PremiumText = obj.Text; break;
                        case 3: response.SocialText = obj.Text; break;
                        case 5: response.LoginText = obj.Text; break;
                        case 6: response.VoucherText = obj.Text; break;
                        case 4: response.CustomText = obj.Text; break;

                    }
                }

                Locations2 locationAllZones = BillingController.ObtainLocation(hotel.IdHotel, "All_Zones");

                #region freeaccess
                if (hotel.FreeAccessModule)
                {
                    List<BillingTypes> list = BillingController.ObtainBillingTypes(location.IdLocation, 1);
                    //SI NO HAY TIPO DE ACCESO PARA EL MÓDULO EN LA LOCALIZACIÓN
                    if (list.Count.Equals(0))
                    {
                        //COMPROBAMOS QUE HAYA TIPO PARA EL MÓDULO EN ALL LOCATIONS
                        List<BillingTypes> listAllZones = BillingController.ObtainBillingTypes(locationAllZones.IdLocation, 1);
                        if (listAllZones.Count.Equals(0))
                        {
                            //COMPROBAMOS QUE TENGA PADRE Y HEREDE DEL MISMO.
                            if (hotel.IdParent.Equals(0) || (!hotel.IdParent.Equals(0) && !hotel.Inherit)) //SI NO TIENE PADRE O BIEN TIENE PERO NO HEREDA
                                response.FreeAccessModule = false;                                         // SE DESHABILITA EL MODULO
                            else
                            {
                                Hotels parent = null;
                                Hotels parentaux = hotel;
                                Locations2 allzonesParent = null;
                                bool end_bucle = false;

                                do
                                {
                                    parent = BillingController.GetHotel(parentaux.IdParent);
                                    allzonesParent = BillingController.ObtainLocation(parent.IdHotel, "All_Zones");
                                    List<BillingTypes> parentBillingAllzones = BillingController.ObtainBillingTypes(allzonesParent.IdLocation, 1);
                                    if (parentBillingAllzones.Count.Equals(0)) //EN EL PADRE TAMPOCO HAY TIPOS DE ACCESO, COMPROBAMOS QUE PODAMOS SEGUIR SUBIENDO
                                    {
                                        if (!parent.IdParent.Equals(0) && parent.Inherit)
                                        {
                                            parentaux = parent;
                                        }
                                        else
                                        {
                                            response.FreeAccessModule = false;
                                            end_bucle = true;
                                        }
                                    }
                                    else
                                    {
                                        BillingTypeResponse btfree = new BillingTypeResponse();
                                        btfree.IdBillingType = parentBillingAllzones[0].IdBillingType;
                                        btfree.Description = parentBillingAllzones[0].Description;
                                        btfree.Price = parentBillingAllzones[0].Price;
                                        btfree.ValidTill = parentBillingAllzones[0].ValidTill;
                                        btfree.BWDown = parentBillingAllzones[0].BWDown;
                                        btfree.BWUp = parentBillingAllzones[0].BWUp;
                                        btfree.VolumeDown = parentBillingAllzones[0].VolumeDown;
                                        btfree.VolumeUp = parentBillingAllzones[0].VolumeUp;
                                        btfree.MaxDevices = parentBillingAllzones[0].MaxDevices;
                                        TimeSpan t = TimeSpan.FromSeconds(parentBillingAllzones[0].TimeCredit / parentBillingAllzones[0].MaxDevices);
                                        if (parentBillingAllzones[0].TimeCredit >= 86400)
                                            btfree.TimeCredit += string.Format("{0}d ", t.Days);
                                        if (t.Hours > 0)
                                            btfree.TimeCredit += string.Format("{0:D2}h ", t.Hours);
                                        if (t.Minutes > 0)
                                            btfree.TimeCredit += string.Format("{0:D2}m ", t.Minutes);
                                        if (t.Seconds > 0)
                                            btfree.TimeCredit += string.Format("{0:D2}s ", t.Seconds);

                                        if (parentBillingAllzones[0].VolumeUp.Equals(0))
                                            btfree.LimitUp = "--";
                                        else
                                            btfree.LimitUp = string.Format("{0} Mb", (parentBillingAllzones[0].VolumeUp / (1024 * 1024)));

                                        if (parentBillingAllzones[0].VolumeDown.Equals(0))
                                            btfree.LimitDown = "--";
                                        else
                                            btfree.LimitDown = string.Format("{0} Mb", (parentBillingAllzones[0].VolumeDown / (1024 * 1024)));

                                        response.freeAccess = btfree;

                                        end_bucle = true;
                                    }

                                } while (!end_bucle);
                            }
                        }
                        else
                        {
                            BillingTypeResponse btfree = new BillingTypeResponse();
                            btfree.IdBillingType = listAllZones[0].IdBillingType;
                            btfree.Description = listAllZones[0].Description;
                            btfree.Price = listAllZones[0].Price;
                            btfree.ValidTill = listAllZones[0].ValidTill;
                            btfree.BWDown = listAllZones[0].BWDown;
                            btfree.BWUp = listAllZones[0].BWUp;
                            btfree.VolumeDown = listAllZones[0].VolumeDown;
                            btfree.VolumeUp = listAllZones[0].VolumeUp;
                            btfree.MaxDevices = listAllZones[0].MaxDevices;
                            TimeSpan t = TimeSpan.FromSeconds(listAllZones[0].TimeCredit / listAllZones[0].MaxDevices);
                            if (listAllZones[0].TimeCredit >= 86400)
                                btfree.TimeCredit += string.Format("{0}d ", t.Days);
                            if (t.Hours > 0)
                                btfree.TimeCredit += string.Format("{0:D2}h ", t.Hours);
                            if (t.Minutes > 0)
                                btfree.TimeCredit += string.Format("{0:D2}m ", t.Minutes);
                            if (t.Seconds > 0)
                                btfree.TimeCredit += string.Format("{0:D2}s ", t.Seconds);

                            if (listAllZones[0].VolumeUp.Equals(0))
                                btfree.LimitUp = "--";
                            else
                                btfree.LimitUp = string.Format("{0} Mb", (listAllZones[0].VolumeUp / (1024 * 1024)));

                            if (listAllZones[0].VolumeDown.Equals(0))
                                btfree.LimitDown = "--";
                            else
                                btfree.LimitDown = string.Format("{0} Mb", (listAllZones[0].VolumeDown / (1024 * 1024)));

                            response.freeAccess = btfree;
                        }
                    }
                    else
                    {
                        BillingTypeResponse btfree = new BillingTypeResponse();
                        btfree.IdBillingType = list[0].IdBillingType;
                        btfree.Description = list[0].Description;
                        btfree.Price = list[0].Price;
                        btfree.ValidTill = list[0].ValidTill;
                        btfree.BWDown = list[0].BWDown;
                        btfree.BWUp = list[0].BWUp;
                        btfree.VolumeDown = list[0].VolumeDown;
                        btfree.VolumeUp = list[0].VolumeUp;
                        btfree.MaxDevices = list[0].MaxDevices;
                        TimeSpan t = TimeSpan.FromSeconds(list[0].TimeCredit / list[0].MaxDevices);
                        if (list[0].TimeCredit >= 86400)
                            btfree.TimeCredit += string.Format("{0}d ", t.Days);
                        if (t.Hours > 0)
                            btfree.TimeCredit += string.Format("{0:D2}h ", t.Hours);
                        if (t.Minutes > 0)
                            btfree.TimeCredit += string.Format("{0:D2}m ", t.Minutes);
                        if (t.Seconds > 0)
                            btfree.TimeCredit += string.Format("{0:D2}s ", t.Seconds);

                        if (list[0].VolumeUp.Equals(0))
                            btfree.LimitUp = "--";
                        else
                            btfree.LimitUp = string.Format("{0} Mb", (list[0].VolumeUp / (1024 * 1024)));

                        if (list[0].VolumeDown.Equals(0))
                            btfree.LimitDown = "--";
                        else
                            btfree.LimitDown = string.Format("{0} Mb", (list[0].VolumeDown / (1024 * 1024)));

                        response.freeAccess = btfree;
                    }
                }

                #endregion

                #region SOCIAL ACCESS
                //ACCESO SOCIAL
                if (hotel.SocialNetworksModule)
                {
                    List<BillingTypes> fbpromotions = BillingController.ObtainBillingTypes(location.IdLocation, 3);
                    if (fbpromotions.Count.Equals(0))
                    {
                        if (hotel.IdParent.Equals(0) || (!hotel.IdParent.Equals(0) && !hotel.Inherit)) //SI NO TIENE PADRE O BIEN TIENE PERO NO HEREDA
                            response.SocialNetworksModule = false;                                     // SE DESHABILITA EL MODULO
                        else
                        {
                            Hotels parent = null;
                            Hotels parentaux = hotel;
                            Locations2 allzonesParent = null;
                            bool end_bucle = false;

                            do
                            {
                                parent = BillingController.GetHotel(parentaux.IdParent);
                                allzonesParent = BillingController.ObtainLocation(parent.IdHotel, "All_Zones");
                                List<BillingTypes> parentBillingAllzones = BillingController.ObtainBillingTypes(allzonesParent.IdLocation, 3);
                                if (parentBillingAllzones.Count.Equals(0)) //EN EL PADRE TAMPOCO HAY TIPOS DE ACCESO, COMPROBAMOS QUE PODAMOS SEGUIR SUBIENDO
                                {
                                    if (!parent.IdParent.Equals(0) && parent.Inherit)
                                    {
                                        parentaux = parent;
                                    }
                                    else
                                    {
                                        response.SocialNetworksModule = false;
                                        end_bucle = true;
                                    }
                                }
                                else
                                {
                                    BillingTypeResponse btfree = BindBillingTypeResponse(hotel, parentBillingAllzones[0]);
                                    //btfree.IdBillingType = parentBillingAllzones[0].IdBillingType;
                                    //btfree.Description = parentBillingAllzones[0].Description;
                                    //btfree.Price = parentBillingAllzones[0].Price;
                                    //btfree.ValidTill = parentBillingAllzones[0].ValidTill;
                                    //btfree.BWDown = parentBillingAllzones[0].BWDown;
                                    //btfree.BWUp = parentBillingAllzones[0].BWUp;
                                    //btfree.VolumeDown = parentBillingAllzones[0].VolumeDown;
                                    //btfree.VolumeUp = parentBillingAllzones[0].VolumeUp;
                                    //btfree.MaxDevices = parentBillingAllzones[0].MaxDevices;
                                    //TimeSpan t = TimeSpan.FromSeconds(parentBillingAllzones[0].TimeCredit / parentBillingAllzones[0].MaxDevices);
                                    //if (parentBillingAllzones[0].TimeCredit >= 86400)
                                    //    btfree.TimeCredit += string.Format("{0}d ", t.Days);
                                    //if (t.Hours > 0)
                                    //    btfree.TimeCredit += string.Format("{0:D2}h ", t.Hours);
                                    //if (t.Minutes > 0)
                                    //    btfree.TimeCredit += string.Format("{0:D2}m ", t.Minutes);
                                    //if (t.Seconds > 0)
                                    //    btfree.TimeCredit += string.Format("{0:D2}s ", t.Seconds);

                                    //if (parentBillingAllzones[0].VolumeUp.Equals(0))
                                    //    btfree.LimitUp = "--";
                                    //else
                                    //    btfree.LimitUp = string.Format("{0} Mb", (parentBillingAllzones[0].VolumeUp / (1024 * 1024)));

                                    //if (parentBillingAllzones[0].VolumeDown.Equals(0))
                                    //    btfree.LimitDown = "--";
                                    //else
                                    //    btfree.LimitDown = string.Format("{0} Mb", (parentBillingAllzones[0].VolumeDown / (1024 * 1024)));

                                    response.socialAccess = btfree;

                                    end_bucle = true;
                                }

                            } while (!end_bucle);
                        }
                    }
                    else
                    {

                        BillingTypes b = BillingController.ObtainBillingType(fbpromotions[0].IdBillingType);
                        BillingTypeResponse btsocial = BindBillingTypeResponse(hotel, b);

                        btsocial.IdBillingType = b.IdBillingType;
                        btsocial.Description = b.Description;
                        btsocial.Price = b.Price;
                        btsocial.ValidTill = b.ValidTill;
                        btsocial.BWDown = b.BWDown;
                        btsocial.BWUp = b.BWUp;
                        btsocial.VolumeDown = b.VolumeDown;
                        btsocial.VolumeUp = b.VolumeUp;
                        btsocial.MaxDevices = b.MaxDevices;
                        TimeSpan t = TimeSpan.FromSeconds(b.TimeCredit / b.MaxDevices);
                        if (b.TimeCredit >= 86400)
                            btsocial.TimeCredit += string.Format("{0}d ", t.Days);
                        if (t.Hours > 0)
                            btsocial.TimeCredit += string.Format("{0:D2}h ", t.Hours);
                        if (t.Minutes > 0)
                            btsocial.TimeCredit += string.Format("{0:D2}m ", t.Minutes);
                        if (t.Seconds > 0)
                            btsocial.TimeCredit += string.Format("{0:D2}s ", t.Seconds);

                        if (b.VolumeUp.Equals(0))
                            btsocial.LimitUp = "--";
                        else
                            btsocial.LimitUp = string.Format("{0} Mb", (b.VolumeUp / (1024 * 1024)));

                        if (b.VolumeDown.Equals(0))
                            btsocial.LimitDown = "--";
                        else
                            btsocial.LimitDown = string.Format("{0} Mb", (b.VolumeDown / (1024 * 1024)));

                        response.socialAccess = btsocial;

                    }
                }

                #endregion

                #region Custom Access

                if (hotel.CustomAccessModule)
                {
                    List<BillingTypes> list = BillingController.ObtainBillingTypes(location.IdLocation, 4);
                    if (list.Count.Equals(0))
                    {
                        List<BillingTypes> listAllZones = BillingController.ObtainBillingTypes(locationAllZones.IdLocation, 4);
                        if (listAllZones.Count.Equals(0))
                            response.CustomAccessModule = false;
                    }
                }

                #endregion
            }
            catch
            {
                ;
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            HttpContext.Current.Response.ContentType = "text";
            HttpContext.Current.Response.Write(serializer.Serialize(response));
            return response;
        }

        public void ProcessEmtpy(HttpContext context)
        {
            // Set the content type and encoding for JSON
            context.Response.ContentType = "application/json";
            context.Response.ContentEncoding = Encoding.UTF8;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private static BillingTypeResponse BindBillingTypeResponse(Hotels hotel, BillingTypes b)
        {
            BillingTypeResponse br = new BillingTypeResponse();
            br.IdBillingType = b.IdBillingType;
            br.Description = b.Description;
            br.Price = b.Price;
            br.ValidTill = b.ValidTill;
            br.BWDown = b.BWDown;
            br.BWUp = b.BWUp;
            br.VolumeDown = b.VolumeDown;
            br.VolumeUp = b.VolumeUp;
            br.MaxDevices = b.MaxDevices;
            TimeSpan t = TimeSpan.FromSeconds(b.TimeCredit / b.MaxDevices);
            if (b.TimeCredit >= 86400)
                br.TimeCredit += string.Format("{0}d ", t.Days);
            if (t.Hours > 0)
                br.TimeCredit += string.Format("{0:D2}h ", t.Hours);
            if (t.Minutes > 0)
                br.TimeCredit += string.Format("{0:D2}m ", t.Minutes);
            if (t.Seconds > 0)
                br.TimeCredit += string.Format("{0:D2}s ", t.Seconds);
            Currencies2 currency = BillingController.Currency(hotel.CurrencyCode);
            br.Currency = string.Format("{0} {1}", b.Price, currency.Symbol);


            if (b.VolumeUp.Equals(0))
                br.LimitUp = "--";
            else
                br.LimitUp = string.Format("{0} Mb", (b.VolumeUp / (1024 * 1024)));

            if (b.VolumeDown.Equals(0))
                br.LimitDown = "--";
            else
                br.LimitDown = string.Format("{0} Mb", (b.VolumeDown / (1024 * 1024)));
            return br;
        }

        private static SitesGMT GetSiteGMt(string idlocation, string idhotel)
        {
            SitesGMT siteGMT = null;
            Locations2 location = null;
            Hotels hotel = null;
            try
            {
                if (!string.IsNullOrEmpty(idlocation))
                {
                    location = BillingController.GetLocation(int.Parse(idlocation));
                    hotel = BillingController.GetHotel(location.IdHotel);
                    siteGMT = BillingController.SiteGMTSitebyLocation(location.IdLocation);
                    if (siteGMT.Id.Equals(0))
                        siteGMT = BillingController.SiteGMTSitebySite(location.IdHotel);
                }
                else
                {
                    siteGMT = BillingController.SiteGMTSitebySite(int.Parse(idhotel));
                }

                return siteGMT;
            }
            catch (Exception ex)
            {
                return null;
            }


        }
    }
}