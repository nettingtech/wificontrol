﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PortalWIFI4EU_Cambium.resources.response
{
    public class BillingTypeResponse
    {
        public int IdBillingType { get; set; }
        public string Description { get; set; }
        public string PayPalDescription { get; set; }
        public double Price { get; set; }
        public string TimeCredit { get; set; }
        public int ValidTill { get; set; }
        public long BWUp { get; set; }
        public long BWDown { get; set; }
        public long VolumeUp { get; set; }
        public long VolumeDown { get; set; }
        public int MaxDevices { get; set; }
        public string Currency { get; set; }
        public string LimitDown { get; set; }
        public string LimitUp { get; set; }
    }
}