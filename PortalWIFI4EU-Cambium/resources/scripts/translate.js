﻿$(document).ready(function (e) {
    var lang = navigator.browserLanguage ? navigator.browserLanguage : navigator.language;
    var langcode = lang.substring(0, 2);

    if (langcode == 'es') {
        //FORMULARIOS
        $("#password").attr('placeholder', 'Contraseña');
        $("#username").attr('placeholder', 'Nombre de usuario');
        $("#usernamelabel").html("Nombre de usuario");
        $("#usernamevoucherlabel").html("Código de acceso");
        $("#passwordlabel").html("Contraseña");
        $("#email_field_voucher").attr('placeholder', 'Código de acceso');
        $("#premiummail").attr("placeholder", "Correo electrónico");
        $("#labelpremiumemail").html("Correo electrónico");

        //BOTONES
        $("#welcome_freebutton").html("¡Continuar a internet!");
        $("#loginbutton").html("Acceder");
        $("#premiumbutton").html("Comprar");
        $("#surveyfreebutton").html("Datos de Usuario");
        $("#surveypremiumbutton").html("Datos de Usuario");
        $("#acceptconditionsbutton").html("Aceptar");
        $("#rejectconditions").html("Cancelar");
        $("#retrybutton").html("Reintentar");
        $("#disclaimerButton").html("Cerrar");
        $("#ContinuePremium").html("Continuar");
        $("#CancelPremium").html("Cancelar");
        $("#buttonbacklegal").html("Volver");
        $("#voucherbutton").html("Acceder");

        //TABS
        $("#freelabel").html('Gratuito');
        $("#voucherlabel").html('Vale');

        //ACEPTAR CONDICIONES
        $("#disclaimeraccept").html("Acepto las condiciones de uso.");
        $("#labeloltherthan").html("Acepto la <a href='legal.aspx?iddisclaimertype=1' target='_blank'>Política de Privacidad</a> y soy mayor de 16 años o estoy autorizado por mi tutor legal.");
        $("#labelacceptcheck").html("Acepto los <a href='legal.aspx?iddisclaimertype=2' target='_blank'>Términos y Condiciones de Uso</a>.");

        //ETIQUETAS GENERAL
        $("#premiumaccesstype").html("Seleccione su tipo de acceso.");

        //MODALES
        $("#disclaimerheader").html("Condiciones de Uso");
        $("#disclaimermandatoryheader").html("Condiciones de Uso");
        $("#disclaimerMandatoryButton").html("Condiciones de Uso");
        $("#surveyheader").html("Datos Personales");
        $("#disclaimerButton").html("Cerrar");
        $("#surveyclose").html("Cerrar");
        $("#errorbutton").html("Cerrar");
        $("#surveysave").html("Continuar");
        $("#surveyclosepaypal").html("Cerrar");
        $("#surveysavepaypal").html("Continuar");

        //TEXTOS GENERALES
        $("#noaccepttext").html("Para poder utilizar el servicio, debe aceptar las Condiciones de Uso.");
        $("#welcometext").html("Bienvenido");
        $("#sorrytext").html("Lo sentimos");
        $("#disclaimerwelcome").html("Por favor, lea el texto a continuación y acepte si está de acuerdo.");
        $("#waitheader").html("Espere un momento por favor");
        $("#premiummailtext").html("Por favor, introduzca su dirección de correo para recibir sus datos de acceso.");
        $("#premiummailHeader").html("Correo electrónico");
    }
});