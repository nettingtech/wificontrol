﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Wifi360.Data.Class;
using Wifi360.Data.Controllers;
using Wifi360.Data.Model;

namespace PortalWIFI4EU_Cambium
{
    public partial class landing : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            ParametrosConfiguracion Parametro_UrlSite = BillingController.GetParametroConfiguracion(HttpContext.Current.Request.Url.Host);
            if (Parametro_UrlSite.IdParameter != 0)
            {
                Locations2 location = BillingController.ObtainLocationDefault(Parametro_UrlSite.IdSite);
                List<ParametrosConfiguracion> parametros = BillingController.GetParametrosConfiguracion(0);
                ParametrosConfiguracion key = (from t in parametros where t.Key.ToUpper().Equals("ENCRYPTIONKEY") select t).FirstOrDefault();
                try
                {
                    var ga_mac = Request.Params["ga_ap_mac"];           //mac del ap
                    var ga_ssid = Request.Params["ga_ssid"];            //ssid del ap
                    var ga_nas_id = Request.Params["ga_nas_id"];        //nas-id o hostname del ap
                    var macs = Request.Params["ga_cmac"];               //mac del dispositivo del cliente
                    var origin = Request.Params["ga_srvr"];             //dirección ip del AP de cambium contra el que lanzar la autenticación
                    var urlDest = Request.Params["ga_orig_url"];        //url de destino del dispositivo
                    var token = Request.Params["ga_Qv"];                //token de autorización del AP Cambiuem
                    var error = Request.Params["ga_error_code"];        //error (en caso de devolver el radius (password wrong, many devices, etc...)
                    var nt_crc_plaint = "nt_st=" + location.IdHotel +
                        "|nt_lt=" + location.IdLocation +
                        "|nt_client_mac=" + macs +
                        "|nt_origin=" + origin +
                        "|nt_urlDest=" + urlDest +
                        "|nt_ap_mac=" + ga_mac +
                        "|nt_ap_ssid=" + ga_ssid +
                        "|nt_ap_nas_id=" + ga_nas_id +
                        "|nt_token=" + token +
                        "|nt_error=" + error +
                        "|epoch=" + DateTime.Now.ToUniversalTime();
                    var nt_crc = Misc.Encrypt(nt_crc_plaint, key.value);

                    Session["nt_crc"] = nt_crc;
                    Response.Redirect("~/welcome.aspx", false);
                }
                catch
                {
                    Response.Redirect("error.aspx");
                }
            }
            else
            {
                Response.Redirect("error.aspx");
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //17% of people see this
        }
    }
}