﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Portal_Cambium
{
    public partial class GoogleAuth : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Get the user id from query string  
            string UserId = Request.QueryString["UserId"];
            AuthorizeUser("info_to_move");
            
        }

        //private string GetGmailId(string userId)
        //{
        //    SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["DbConnection"].ConnectionString);
        //    string Query = "select GmailId from Member where GmailId=" + userId;
        //    SqlCommand Cmd = new SqlCommand(Query, Con);
        //    Con.Open();
        //    string Result = Cmd.ExecuteScalar().ToString();
        //    Con.Close();
        //    return Result;
        //}

        //private bool IsAuthorized(string userId)
        //{
        //    SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["PORTAL"].ConnectionString);
        //    string Query = "select count(*) from GoogleMember where UserId=" + userId;
        //    SqlCommand Cmd = new SqlCommand(Query, Con);
        //    Con.Open();
        //    try { 
        //        int Result = (int)Cmd.ExecuteScalar();
        //        Con.Close();
        //        return Result > 0 ? true : false;
        //    }
        //    catch
        //    {
        //        return false;
        //    }
           
        //}
        private void AuthorizeUser(string data)
        {
            string Url = GetAuthorizationUrl(data);
            HttpContext.Current.Response.Redirect(Url, false);
        }
        /// <summary>    
        ///     
        /// </summary>    
        /// <param name="data"></param>    
        /// <returns></returns>    
        private string GetAuthorizationUrl(string data)
        {
            string ClientId = ConfigurationManager.AppSettings["GoogleAppId"];
            string Scopes = "email";
            //get this value by opening your web app in browser.    
            string RedirectUrl = string.Format("https://{0}/GoogleCallBack.aspx", HttpContext.Current.Request.Url.Host); 
            string Url = "https://accounts.google.com/o/oauth2/auth?";
            StringBuilder UrlBuilder = new StringBuilder(Url);
            UrlBuilder.Append("client_id=" + ClientId);
            UrlBuilder.Append("&redirect_uri=" + RedirectUrl);
            UrlBuilder.Append("&response_type=" + "code");
            UrlBuilder.Append("&scope=" + Scopes);
            UrlBuilder.Append("&access_type=" + "offline");
            UrlBuilder.Append("&state=" + data); //setting the user id in state  
            return UrlBuilder.ToString();
        }
    }
}