﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Wifi360.Data.Controllers;
using Wifi360.Data.Model;

namespace Portal_Cambium
{
    public partial class Success : System.Web.UI.Page
    {

        protected string username = string.Empty;
        protected string validtill = string.Empty;
        protected string timecredit = string.Empty;
        protected string volumecredit = string.Empty;
        protected string accesstype = string.Empty;
        protected string continueurl = string.Empty;
        protected string logout_url = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //HttpCookie cambium_cookie = Context.Request.Cookies["Cambium"];

                int idhotel = int.Parse(Context.Request.Params["idhotel"].ToString());
                int idlocation = int.Parse(Context.Request.Params["idlocation"].ToString());
                string mac = Context.Request.Params["mac"].ToString();
                string user_name = Context.Request.Params["username"].ToString();
                string continue_url = Context.Request.Params["continue_url"].ToString();
                string ga_ssid = Context.Request.Params["ga_ssid"].ToString();
                string ga_nas_id = Context.Request.Params["ga_nas_id"].ToString();
                string ga_ap_mac = Context.Request.Params["ga_ap_mac"].ToString();
                string ga_cmac = Context.Request.Params["ga_cmac"].ToString();
                string token = Context.Request.Params["token"].ToString();
                string ga_srvr = Context.Request.Params["origin"].ToString();

                logout_url = string.Format("http://{0}:880/1/welcome.html?ga_ap_mac={1}&ga_nas_id={2}&ga_srvr={0}&ga_cmac={3}&ga_Qv={4}",
                    ga_srvr, ga_ap_mac, ga_nas_id, ga_cmac, token);

                SitesGMT siteGMT = GetSiteGMt(idlocation, idhotel);

                Users user = BillingController.GetRadiusUser(user_name, idhotel);
                if (user.IdUser != 0)
                {
                    BillingTypes2 billingtype = BillingController.ObtainBillingType2(user.IdBillingType);
                    username = user.Name;

                    DateTime validTill = DateTime.MinValue;
                    List<ClosedSessions2> closedSessions = BillingController.GetClosedSession(idhotel, username, user.ValidSince, user.ValidTill);
                    if (closedSessions.Count > 0)
                    {
                        ClosedSessions2 closed = (from d in closedSessions orderby d.SessionTerminated descending select d).FirstOrDefault();
                        if (closed != null)
                            validTill = closed.SessionTerminated.Value.AddHours(billingtype.ValidAfterFirstUse);
                    }
                    else
                    {
                        ActiveSessions active = BillingController.GetActiveSession(mac, idhotel);
                        if (!active.IdActiveSession.Equals(0))
                            validTill = active.SessionStarted.Value.AddHours(billingtype.ValidAfterFirstUse);
                        else
                            validTill = DateTime.Now.ToUniversalTime().AddHours(billingtype.ValidAfterFirstUse);
                    }

                    if (validTill > user.ValidTill)
                        validtill = user.ValidTill.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");
                    else
                        validtill = validTill.AddSeconds(siteGMT.gmtoffset).ToString("dd/MM/yyyy HH:mm:ss");



                    TimeSpan t = TimeSpan.FromSeconds(user.TimeCredit / user.MaxDevices);
                    string time = string.Empty;
                    if (user.TimeCredit >= 86400)
                        time = string.Format("{0} d: {1:D2} h: {2:D2} m: {3:D2} s", t.Days, t.Hours, t.Minutes, t.Seconds);
                    else
                        time = string.Format("{0:D2} h: {1:D2} m: {2:D2} s", t.Hours, t.Minutes, t.Seconds);
                    timecredit = time;
                    volumecredit = string.Format("{0:0.00} MB", Math.Round(double.Parse((user.VolumeDown / 1048576).ToString()), 2));
                    accesstype = billingtype.Description;
                    continueurl = continue_url;
                }
            }
            catch (Exception ex)
            {
                CaptivePortalLogInternal internallog = new CaptivePortalLogInternal();

                int idhotel = int.Parse(Context.Request.Params["idhotel"].ToString());
                int idlocation = int.Parse(Context.Request.Params["idlocation"].ToString());
                string mac = Context.Request.Params["mac"].ToString();
                string user_name = Context.Request.Params["username"].ToString();
                string continue_url = Context.Request.Params["continue_url"].ToString();
                internallog.IdHotel = idhotel;
                internallog.IdLocation = idlocation;
                internallog.CallerID = mac;
                internallog.NomadixResponse = continue_url;
                internallog.RadiusResponse = ex.Message + " trace: " + ex.StackTrace;
                internallog.UserName = user_name;
                internallog.Page = "Success.aspx";
                internallog.NSEId = string.Empty;
                internallog.Password = string.Empty;

                BillingController.SaveCaptivePortalLogInternal(internallog);
            }
        }

        private static SitesGMT GetSiteGMt(int idlocation, int idhotel)
        {
            SitesGMT siteGMT = null;
            Locations2 location = null;
            Hotels hotel = null;
            try
            {
                if (idlocation != 0)
                {
                    location = BillingController.GetLocation(idlocation);
                    hotel = BillingController.GetHotel(location.IdHotel);
                    siteGMT = BillingController.SiteGMTSitebyLocation(location.IdLocation);
                    if (siteGMT.Id.Equals(0))
                        siteGMT = BillingController.SiteGMTSitebySite(location.IdHotel);
                }
                else
                {
                    siteGMT = BillingController.SiteGMTSitebySite(idhotel);
                }

                return siteGMT;
            }
            catch (Exception ex)
            {
                return null;
            }


        }

        protected void logout_asp_button_Click(object sender, EventArgs e)
        {
            try
            {
                string ga_ssid = Context.Request.Params["ga_ssid"].ToString();
                string ga_nas_id = Context.Request.Params["ga_nas_id"].ToString();
                string ga_ap_mac = Context.Request.Params["ga_ap_mac"].ToString();
                string ga_cmac = Context.Request.Params["ga_cmac"].ToString();
                string token = Context.Request.Params["token"].ToString();
                string ga_srvr = Context.Request.Params["origin"].ToString();

                logout_url = string.Format("http://{0}:880/1/welcome.html?ga_ap_mac={1}&ga_nas_id={2}&ga_srvr={0}&ga_cmac={3}&ga_Qv={4}",
                                    ga_srvr, ga_ap_mac, ga_nas_id, ga_cmac, token);


                Response.Redirect(logout_url, false);
            }
            catch (Exception ex)
            {
                ;
            }

        }

        protected void change_asp_button_Click(object sender, EventArgs e)
        {
            try
            {
                int idhotel = int.Parse(Context.Request.Params["idhotel"].ToString());
                int idlocation = int.Parse(Context.Request.Params["idlocation"].ToString());
                string mac = Context.Request.Params["mac"].ToString();

                BillingController.DeleteRememberMeTableUser(mac, idhotel);

                string ga_ssid = Context.Request.Params["ga_ssid"].ToString();
                string ga_nas_id = Context.Request.Params["ga_nas_id"].ToString();
                string ga_ap_mac = Context.Request.Params["ga_ap_mac"].ToString();
                string ga_cmac = Context.Request.Params["ga_cmac"].ToString();
                string token = Context.Request.Params["token"].ToString();
                string ga_srvr = Context.Request.Params["origin"].ToString();

                logout_url = string.Format("http://{0}:880/1/welcome.html?ga_ap_mac={1}&ga_nas_id={2}&ga_srvr={0}&ga_cmac={3}&ga_Qv={4}",
                                    ga_srvr, ga_ap_mac, ga_nas_id, ga_cmac, token);

                Response.Redirect(logout_url, false);
            }
            catch (Exception ex)
            {
                ;
            }
        }
    }
}