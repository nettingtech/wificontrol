﻿<%@ Page Title="" Language="C#" MasterPageFile="~/resources/master/CambiumMaster.Master" AutoEventWireup="true" CodeBehind="instagram.aspx.cs" Inherits="Portal_Cambium.instagram" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script type="text/javascript">

          $(document).ready(function (e) {

              var lang = navigator.browserLanguage ? navigator.browserLanguage : navigator.language;
              var langcode = lang.substring(0, 2);
              if ((langcode != 'es') && (langcode != 'en'))
                  langcode = $("#languagecode").val();

              var cookie = getCookie("Cambium");
              if (cookie != "") {
                  values = cookie.split('&');

                  $("#idhotel").val(values[0].split('=')[1]);
                  $("#idlocation").val(values[1].split('=')[1]);
                  $("#mac").val(values[2].split('=')[1]);
                  $("#origin").val(values[3].split('=')[1]);
                  $("#idsocialbillintype").val(values[4].split('=')[1]);
                  $("#token").val(values[5].split('=')[1]);
                  $("#ga_ssid").val(values[6].split('=')[1]);
                  $("#ga_nas_id").val(values[7].split('=')[1]);
                  $("#ga_ap_mac").val(values[8].split('=')[1]);
                  $("#ga_cmac").val(values[9].split('=')[1]);
              }
              else
                  alert("No hay cookie");

              var access_token = window.location.hash.split('=')[1];
              $("#logomodalfinish").attr("src", "/resources/images/" + $("#idhotel").val() + "/" + $("#idlocation").val() + "-logo.png");

              $.ajax({
                  url: 'https://api.instagram.com/v1/users/self/?access_token=' + access_token,
                  dataType: 'jsonp',
                  type: 'GET',
                  data: { client_id: access_token },
                  success: function (data) {
                      var survey = 'instagram: nombre=' + data.data.full_name + '|id=' + data.data.id + '|username=' + data.data.username + '|language=' + navigator.language;
                      var dataStringm = 'action=instagramaccess&id=' + data.data.id + '&mac=' + $("#mac").val() + '&idhotel=' + $("#idhotel").val() + '&idlocation=' + $("#idlocation").val() + '&origin=' + $("#origin").val() + '&survey=' + survey + "&lang=" + langcode + "&idbillingtype=" + $("#idsocialbillintype").val();
                      $.ajax({
                          url: "../resources/handlers/CambiumHandler.ashx",
                          data: dataStringm,
                          contentType: "application/json; charset=utf-8",
                          dataType: "text",
                          beforeSend: function () {
                              $("#MyModalWait").modal('show');
                          },
                          complete: function () {
                              $("#MyModalWait").modal('hide');
                          },
                          success: function (pResult) {
                              var lResult = JSON.parse(pResult);
                              if (lResult.IdUser == -1) {
                                  if (langcode != 'es') {
                                      $("#message").html("No more free access are allowed now.");
                                  }
                                  else {
                                      $("#message").html("No se permiten más accesos gratuitos en este periodo.");
                                  }
                                  $("#myModalError").modal('show');
                              }
                              else if (lResult.IdUser == -3) {
                                  if (langcode != 'es') {
                                      $("#message").html("We are currently experiencing a problem, please try again later.");
                                  }
                                  else {
                                      $("#message").html("Tenemos un problema temporal, intentelo de nuevo más tarde.");
                                  }
                                  $("#myModalError").modal('show');
                              }
                              else if (lResult.IdUser == 0) {
                                  if (langcode != 'es') {
                                      $("#message").html(lResult.UserName);
                                  }
                                  else {
                                      $("#message").html(lResult.UserName);
                                  }
                                  $("#myModalError").modal('show');
                              }
                              else {
                                  $("#finishbutton").attr("href", lResult.Url);
                                  $("#finishInformation").append($("#socialaccessinformation").html());

                                  if (langcode == 'es') {
                                      $("#finishlabel").html("Bienvenido " + data.data.full_name);
                                      $("#finishInformation").append('<b>Nombre de usuario: </b>' + lResult.UserName + '<br />');
                                      $("#finishInformation").append('<b>Contraseña: </b>' + lResult.Password + '<br />');
                                      $("#finishInformation").append('<b>Fecha de expiración: </b>' + lResult.ValidTill + '<br />');
                                      $("#finishbutton").html("Continuar navegando");
                                  }
                                  else {
                                      $("#finishlabel").html("Welcome " + data.data.full_name);
                                      $("#finishInformation").append('<b>UserName: </b>' + lResult.UserName + '<br />');
                                      $("#finishInformation").append('<b>Password: </b>' + lResult.Password + '<br />');
                                      $("#finishInformation").append('<b>Valid Till: </b>' + lResult.ValidTill + '<br />');
                                      $("#finishbutton").html("Continue browsing");
                                  }

                                  var val = "username=" + lResult.UserName + "&password=" + lResult.Password + "&validtill=" + lResult.ValidTill + "&continue_url=" + lResult.Url;
                                  createCookie("Cambiumuser", val, 1);

                                  var parameters = getParameters();
                                  var continue_url = window.location.origin + "/success.aspx?idhotel=" + $("#idhotel").val() + '&idlocation=' + $("#idlocation").val() + '&mac=' + $("#mac").val() + "&username=" + lResult.UserName + "&continue_url=" + lResult.Url
                                    + '&ga_ssid=' + $("#ga_ssid").val() + '&ga_nas_id=' + $("#ga_nas_id").val() + '&ga_ap_mac=' + $("#ga_ap_mac").val() + '&ga_cmac=' + $("#ga_cmac").val() + '&token=' + $("#token").val() + '&origin=' + $("#origin").val();
                                  var action = "http://" + $("#origin").val() + ":880/cgi-bin/hotspot_login.cgi";
                                  var newForm = jQuery('<form>', {
                                      'action': action,
                                      'method': 'POST',
                                  }).append(jQuery('<input>', {
                                      'name': 'ga_user',
                                      'value': lResult.UserName,
                                      'type': 'hidden'
                                  })).append(jQuery('<input>', {
                                      'name': 'ga_pass',
                                      'value': lResult.Password,
                                      'type': 'hidden'
                                  })).append(jQuery('<input>', {
                                      'name': 'ga_Qv',
                                      'value': $("#token").val(),
                                      'type': 'hidden'
                                  })).append(jQuery('<input>', {
                                      'name': 'ga_orig_url',
                                      'value': continue_url,
                                      'type': 'hidden'

                                  }));
                                  $(document.body).append(newForm);
                                  newForm.submit();
                              }

                          },
                          error: function (xhr, ajaxOptions, thrownError) {
                              $("#message2").html("We have a problem, please try again later.");
                              alert('error: ' + xhr.statusText);
                          }
                      });

                  },
                  error: function (data) {
                      console.log(data);
                      $("#message2").html(data).removeClass().addClass('alert alert-danger text-center').show();
                      $("#myModalMandatory").modal('show');
                  }
              });

              $("#finishbutton").click(function (e) {
                  e.preventDefault();
                  var url = $("#finishbutton").attr("href");
                  window.location.href = url;

              });
          });
       </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="modal fade" id="myModalError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title text-center" id="H2">Error</h4>
          </div>
          <div class="modal-body">
            <div id="message" class="alert alert-danger text-center"></div> 
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default round" data-dismiss="modal" id="errorbutton">Close</button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="myModalFinish" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title text-center" id="finishlabel">Congratulations</h4>
          </div>
          <div class="modal-body">
                <div style="text-align:center">
                        <img src="" id="logomodalfinish" alt="" />
                </div>
                <div id="finishInformation" style="text-align:center" ><br /><br /></div>
                            <br />
                <input type="button" id="finishbutton" class="btn btn-success btn-block round" value="Continue browsing" /> 
          </div>
            <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="MyModalWait" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center" id="waitheader">Please, wait a moment</h4>
                </div>
                <div class="modal-body">
                    <div class="progress">
                        <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                            <span class="sr-only">Waiting please</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <input type="text" hidden="hidden" id="idsocialbillintype" />
        <input type="text" hidden="hidden" id="ga_ssid" />
        <input type="text" hidden="hidden" id="ga_nas_id" />
        <input type="text" hidden="hidden" id="ga_ap_mac" />
        <input type="text" hidden="hidden" id="ga_cmac" />
        <input type="text" hidden="hidden" id="token" />

</asp:Content>
