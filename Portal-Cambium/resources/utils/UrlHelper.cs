﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Portal_Cambium.resources.utils
{
    public class UrlHelper
    {
        #region URL Handling (BlogEngine.NET)

        private static string _virtualPath = string.Empty;

        public static string VirtualPath
        {
            get
            {
                if (string.IsNullOrEmpty(_virtualPath))
                {
                    _virtualPath = "~/"; // Default value
                    if (ConfigurationManager.AppSettings["VirtualPath"] != null)
                        _virtualPath = ConfigurationManager.AppSettings["VirtualPath"].ToString();
                }
                return _virtualPath;
            }
        }

        private static string _relativeWebRoot;

        /// <summary>
        /// Gets the relative root of the website.
        /// </summary>
        /// <value>A string that ends with a '/'.</value>
        public static string RelativeWebRoot
        {
            get
            {
                if (_relativeWebRoot == null)
                    _relativeWebRoot = VirtualPathUtility.ToAbsolute(VirtualPath);

                return _relativeWebRoot;
            }
        }

        /// <summary>
        /// Gets the absolute root of the website.
        /// </summary>
        /// <value>A string that ends with a '/'.</value>
        public static Uri AbsoluteWebRoot
        {
            get
            {

                HttpContext context = HttpContext.Current;
                if (context == null)
                    throw new System.Net.WebException("The current HttpContext is null");

                if (context.Items["absoluteurl"] == null)
                    context.Items["absoluteurl"] = new Uri(context.Request.Url.GetLeftPart(UriPartial.Authority) + RelativeWebRoot);

                return context.Items["absoluteurl"] as Uri;
            }
        }

        /// <summary>
        /// Converts a relative URL to an absolute one.
        /// </summary>
        public static Uri ConvertToAbsolute(Uri relativeUri)
        {
            return ConvertToAbsolute(relativeUri.ToString()); ;
        }

        /// <summary>
        /// Converts a relative URL to an absolute one.
        /// </summary>
        public static Uri ConvertToAbsolute(string relativeUri)
        {
            if (String.IsNullOrEmpty(relativeUri))
                throw new ArgumentNullException("relativeUri");

            string absolute = AbsoluteWebRoot.ToString();
            int index = absolute.LastIndexOf(RelativeWebRoot.ToString());

            return new Uri(absolute.Substring(0, index) + relativeUri);
        }

        public static string Combine(string url1, string url2)
        {
            string p1 = url1;
            if (p1.EndsWith("/"))
                p1 = p1.Substring(0, p1.Length - 1);

            string p2 = url2;
            if (p2.StartsWith("/"))
                p2 = p2.Substring(1);

            return string.Format("{0}/{1}", p1, p2);
        }

        #endregion

        public static string ClearInvalidChar(string text)
        {
            if (string.IsNullOrEmpty(text))
                return text;

            text = text.Replace(":", string.Empty);
            text = text.Replace("/", string.Empty);
            text = text.Replace("?", string.Empty);
            text = text.Replace("¿", string.Empty);
            text = text.Replace("!", string.Empty);
            text = text.Replace("¡", string.Empty);
            text = text.Replace("#", string.Empty);
            text = text.Replace("[", string.Empty);
            text = text.Replace("]", string.Empty);
            text = text.Replace("@", string.Empty);
            text = text.Replace("€", string.Empty);
            text = text.Replace("$", string.Empty);
            text = text.Replace("*", string.Empty);
            text = text.Replace(".", string.Empty);
            text = text.Replace(",", string.Empty);
            text = text.Replace("\"", string.Empty);
            text = text.Replace("&", string.Empty);
            text = text.Replace("'", string.Empty);
            text = text.Replace("+", string.Empty);
            text = text.Replace("º", string.Empty);
            text = text.Replace("ª", string.Empty);
            text = text.Replace("%", string.Empty);
            text = text.Replace("<", string.Empty);
            text = text.Replace(">", string.Empty);

            text = text.Replace("(", string.Empty);
            text = text.Replace(")", string.Empty);

            String normalized = text.Normalize(System.Text.NormalizationForm.FormD);
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            for (int i = 0; i < normalized.Length; i++)
            {
                Char c = normalized[i];
                if (System.Globalization.CharUnicodeInfo.GetUnicodeCategory(c) != System.Globalization.UnicodeCategory.NonSpacingMark)
                    sb.Append(c);
            }

            text = sb.ToString();

            return text;
        }

        public static string Encode(string url)
        {
            string _tmp = url;
            _tmp = _tmp.ToLower();
            _tmp = ClearInvalidChar(_tmp);
            _tmp = HttpUtility.UrlEncode(_tmp);
            _tmp = _tmp.Replace("+", "-");

            return _tmp;
        }
    }
}