﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal_Cambium.resources.response
{
    public class ResponseSocialNetwork
    {
        public int IdSocialNetwork { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
    }
}