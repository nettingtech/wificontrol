﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal_Cambium.resources.response
{
    public class ListSurveyResponse
    {
        public List<SurveyResponse> list { get; set; }
        public int hotel { get; set; }
    }
}