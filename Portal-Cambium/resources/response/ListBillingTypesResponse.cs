﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal_Cambium.resources.response
{
    public class ListBillingTypesResponse
    {
        public List<BillingTypeResponse> list { get; set; }
        public int hotel { get; set; }
    }
}