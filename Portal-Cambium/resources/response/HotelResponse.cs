﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal_Cambium.resources.response
{
    public class HotelResponse
    {
        public int IdHotel { get; set; }
        public string Name { get; set; }
        public string Disclaimer { get; set; }
        public string LocationText { get; set; }
        public bool FreeAccessModule { get; set; }
        public bool PayAccessModule { get; set; }
        public bool SocialNetworksModule { get; set; }
        public bool CustomAccessModule { get; set; }
        public int IdLanguageDefault { get; set; }
    }
}