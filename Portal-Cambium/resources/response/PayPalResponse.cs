﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal_Cambium.resources.response
{
    public class PayPalResponse
    {
        public int IdTransaction { get; set; }
        public string Description { get; set; }
        public string Price { get; set; }
    }
}