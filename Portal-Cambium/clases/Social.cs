﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;


namespace Portal_Cambium.clases
{
    public class Social
    {
        public class User
            {
                public string id { get; set; }
                public string first_name { get; set; }
                public string last_name { get; set; }
                public string link { get; set; }
                public string username { get; set; }
                public string gender { get; set; }
                public string locale { get; set; }
                public string email { get; set; }
                public string birthday { get; set; }
            }

        public class Token
        {
            public string access_token { get; set; }
            public string expires_in { get; set; }
        }

        public class LinkedinUser
        {
            public partial class Welcome
            {
                [JsonProperty("localizedLastName")]
                public string LocalizedLastName { get; set; }

                [JsonProperty("lastName")]
                public StName LastName { get; set; }

                [JsonProperty("firstName")]
                public StName FirstName { get; set; }

                [JsonProperty("profilePicture")]
                public ProfilePicture ProfilePicture { get; set; }

                [JsonProperty("id")]
                public string Id { get; set; }

                [JsonProperty("localizedFirstName")]
                public string LocalizedFirstName { get; set; }
            }

            public partial class StName
            {
                [JsonProperty("localized")]
                public Localized Localized { get; set; }

                [JsonProperty("preferredLocale")]
                public PreferredLocale PreferredLocale { get; set; }
            }

            public partial class Localized
            {
                [JsonProperty("es_ES")]
                public string EsEs { get; set; }
            }

            public partial class PreferredLocale
            {
                [JsonProperty("country")]
                public string Country { get; set; }

                [JsonProperty("language")]
                public string Language { get; set; }
            }

            public partial class ProfilePicture
            {
                [JsonProperty("displayImage")]
                public string DisplayImage { get; set; }
            }

            public partial class Welcome_mail
            {
                [JsonProperty("elements")]
                public List<Element> Elements { get; set; }
            }

            public partial class Element
            {
                [JsonProperty("handle")]
                public string Handle { get; set; }

                [JsonProperty("type")]
                public string Type { get; set; }

                [JsonProperty("handle~")]
                public Handle ElementHandle { get; set; }

                [JsonProperty("primary")]
                public bool Primary { get; set; }
            }

            public partial class Handle
            {
                [JsonProperty("emailAddress")]
                public string EmailAddress { get; set; }
            }
        }
    }    
}