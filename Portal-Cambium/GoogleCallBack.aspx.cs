﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Portal_Cambium
{
    public partial class GoogleCallBack : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //you will get this, when any error will occur while authorization otherwise null    
            string Error = Request.QueryString["error"];
            //authorization code after successful authorization    
            string Code = Request.QueryString["code"];
            if (Error != null) { }
            else if (Code != null)
            {
                string AccessToken = AuthorizationCode(Code);
                string JsonString = FetchGoogleInfo(AccessToken); //AccessToken
                JavaScriptSerializer js = new JavaScriptSerializer();
                var obj = js.Deserialize<dynamic>(JsonString);

                string mail = obj["email"];
                string name = obj["name"];
                string Url = "google.aspx?mail=" + mail + "&name="+ name;
                Response.Redirect(Url, true);
            }
            else {
                string Url = GetAuthorizationUrl("test");
                HttpContext.Current.Response.Redirect(Url, false);
            }
        }

        private string GetAuthorizationUrl(string data)
        {
            string ClientId = ConfigurationManager.AppSettings["GoogleAppId"];
            string Scopes = "email profile";
            //get this value by opening your web app in browser.    
            string RedirectUrl = string.Format("https://{0}/GoogleCallBack.aspx", HttpContext.Current.Request.Url.Host);
            string Url = "https://accounts.google.com/o/oauth2/auth?";
            StringBuilder UrlBuilder = new StringBuilder(Url);
            UrlBuilder.Append("client_id=" + ClientId);
            UrlBuilder.Append("&redirect_uri=" + RedirectUrl);
            UrlBuilder.Append("&response_type=" + "code");
            UrlBuilder.Append("&scope=" + Scopes);
            UrlBuilder.Append("&access_type=" + "offline");
            UrlBuilder.Append("&state=" + data); //setting the user id in state  
            return UrlBuilder.ToString();
        }

        private string AuthorizationCode(string code)
        {
            string ClientSecret = ConfigurationManager.AppSettings["GoogleAppSecret"];
            string ClientId = ConfigurationManager.AppSettings["GoogleAppId"];
            //get this value by opening your web app in browser.    
            string RedirectUrl = string.Format("https://{0}/GoogleCallBack.aspx", HttpContext.Current.Request.Url.Host);
            var Content = "code=" + code + "&client_id=" + ClientId + "&client_secret=" + ClientSecret + "&redirect_uri=" + RedirectUrl + "&grant_type=authorization_code";

            var request = WebRequest.Create("https://accounts.google.com/o/oauth2/token");
            request.Method = "POST";
            //request.UserAgent = "Mozilla/5.0 Google"; //Sobreescribe el useragent

            byte[] byteArray = Encoding.UTF8.GetBytes(Content);
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = byteArray.Length;
            using (Stream dataStream = request.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();
            }
            var Response = (HttpWebResponse)request.GetResponse();
            Stream responseDataStream = Response.GetResponseStream();
            StreamReader reader = new StreamReader(responseDataStream);
            string ResponseData = reader.ReadToEnd();
            reader.Close();
            responseDataStream.Close();
            Response.Close();
            if (Response.StatusCode == HttpStatusCode.OK)
            {
                var ReturnedToken = JsonConvert.DeserializeObject<Token>(ResponseData);
                return ReturnedToken.access_token;
            }
            else
            {
                return string.Empty;
            }
        }

        private string FetchGoogleInfo(string accessToken)
        {
            var EmailRequest = "https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=" + accessToken;
            // Create a request for the URL.    
            var Request = WebRequest.Create(EmailRequest);
            //Request.UserAgent = "Mozilla/5.0 Google"; //Sobreescribe el useragent
            // Get the response.    
            var Response = (HttpWebResponse)Request.GetResponse();
            // Get the stream containing content returned by the server.    
            var DataStream = Response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.    
            var Reader = new StreamReader(DataStream);
            // Read the content.    
            var JsonString = Reader.ReadToEnd();
            // Cleanup the streams and the response.    
            Reader.Close();
            DataStream.Close();
            Response.Close();
            return JsonString;            
        }

    }
    public class Token
    {
        public string access_token
        {
            get;
            set;
        }
        public string token_type
        {
            get;
            set;
        }
        public string expires_in
        {
            get;
            set;
        }
        public string refresh_token
        {
            get;
            set;
        }
    }
}