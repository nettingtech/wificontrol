﻿<%@ Page Title="" Language="C#" MasterPageFile="~/resources/master/CambiumMaster.Master" AutoEventWireup="true" CodeBehind="paypalsuccess.aspx.cs" Inherits="Portal_Cambium.paypalsuccess" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript">
        $(document).ready(function (e) {

            var cookie = getCookie("Cambium");
            if (cookie != "") {
                values = cookie.split('&');

                $("#token").val(values[5].split('=')[1]);
                $("#ga_ssid").val(values[6].split('=')[1]);
                $("#ga_nas_id").val(values[7].split('=')[1]);
                $("#ga_ap_mac").val(values[8].split('=')[1]);
                $("#ga_cmac").val(values[9].split('=')[1]);
            }
            else

            var parameters = getParameters();
            var continue_url = window.location.origin + "/success.aspx?idhotel=" + $("#idhotel").val() + '&idlocation=' + $("#idlocation").val() + '&mac=' + $("#mac").val() + "&username=" + $("#usernamepay").val() + "&continue_url=" + $("#continue_urlpay").val()
              + '&ga_ssid=' + $("#ga_ssid").val() + '&ga_nas_id=' + $("#ga_nas_id").val() + '&ga_ap_mac=' + $("#ga_ap_mac").val() + '&ga_cmac=' + $("#ga_cmac").val() + '&token=' + $("#token").val() + '&origin=' + $("#origin").val();


            var action = "http://" + $("#origin").val() + ":880/cgi-bin/hotspot_login.cgi";

            var newForm = jQuery('<form>', {
                'action': action,
                'method': 'POST',
            }).append(jQuery('<input>', {
                'name': 'ga_user',
                'value': $("#usernamepay").val(),
                'type': 'hidden'
            })).append(jQuery('<input>', {
                'name': 'ga_pass',
                'value': $("#passwordpay").val(),
                'type': 'hidden'
            })).append(jQuery('<input>', {
                'name': 'ga_Qv',
                'value': $("#token").val(),
                'type': 'hidden'
            })).append(jQuery('<input>', {
                'name': 'ga_orig_url',
                'value': continue_url,
                'type': 'hidden'

            }));
            $(document.body).append(newForm);
            newForm.submit();
        });
    </script>
        <input type="text" hidden="hidden" id="ga_ssid" />
        <input type="text" hidden="hidden" id="ga_nas_id" />
        <input type="text" hidden="hidden" id="ga_ap_mac" />
        <input type="text" hidden="hidden" id="ga_cmac" />
        <input type="hidden" id="usernamepay" value="<%Response.Write(username);%>" />
        <input type="hidden" id="passwordpay" value="<%Response.Write(password);%>" />
        <input type="hidden" id="validtillpay" value="<%Response.Write(validtill);%>" />
        <input type="hidden" id="continue_urlpay" value="<%Response.Write(continue_url);%>" />
        <input type="hidden" id="login_urlpay" value="<%Response.Write(login_url);%>" />
    
</asp:Content>
