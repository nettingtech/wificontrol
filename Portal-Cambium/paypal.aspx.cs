﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Wifi360.Data.Controllers;
using Wifi360.Data.Model;

namespace Portal_Cambium
{
    public partial class paypal : System.Web.UI.Page
    {
        protected string item_name = string.Empty;
        protected string amount = string.Empty;
        protected string idtransaction = string.Empty;
        protected string PayPalUser = string.Empty;
        protected string currency_code = string.Empty;
        protected string return_url = string.Empty;
        protected string notify_url = string.Empty;
        protected string error_url = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            item_name = (Request["item_name"] == null ? string.Empty : Request["item_name"].ToString());
            amount = (Request["amount"] == null ? string.Empty : Request["amount"].ToString());
            idtransaction = (Request["IdTransaction"] == null ? string.Empty : Request["IdTransaction"].ToString());

            string idhotel = (Request["idhotel"] == null ? string.Empty : Request["idhotel"].ToString());
            Hotels h = BillingController.GetHotel(Int32.Parse(idhotel));
            PayPalUser = h.PayPalUser;
            currency_code = h.CurrencyCode;
            return_url = "http://" + Request.Url.Host;
            return_url += string.Format("/paypalsuccess.aspx?custom={0}", idtransaction);
            notify_url = "http://" + Request.Url.Host + "/paypalsuccess.aspx";
            error_url = "http://" + Request.Url.Host + "/error.aspx";
        }
    }
}